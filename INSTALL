
0.  BOOST
    Ensure that a version of boost 1.51 -- 1.61 (except 1.54) is 
    installed on your system.  You might be able to find it with 
    the following:
    
        ls /usr/local/boost
        
    Set the BOOSTHOME environment variable to point to the BOOST
    installation directories.
        
        export BOOSTHOME=/usr/local/boost

1.  Install the Rose Compiler infrastructure, rosecompiler.org

    Please use the github version of ROSE

        git clone https://github.com/rose-compiler/rose
        
    We tested with Rose version 0.9.9.13.

    Set the $ROSE_SOURCE environment variable to the top level rose/ dir.

        export ROSE_SOURCE=/path/to/top/dir/for/rose/
        
    Set the ROSEHOME environment variable to where ROSE will be installed.

       export ROSEHOME=/path/to/top/dir/for/roseBuild/
    
    --------
    NOTE:
    If you are using OpenJDK for Java, please download the patch that 
    we provide and apply the patch in the top level rose/ directory.

        cd $ROSE_SOURCE
        patch -p1 < 0001-Adds-openjdk-support.patch
    --------

    Configure and build the Rose Compiler.

        cd $ROSE_SOURCE 
        ./build 
        cd $ROSEHOME  
        $ROSE_SOURCE/configure --prefix=$ROSEHOME --enable-binary-analysis=no --with-boost=$BOOSTHOME --with-boost-libdir=$BOOSTHOME/lib

        export LD_LIBRARY_PATH=$BOOSTHOME/lib:$ROSEHOME/lib

        make
        make install
        
   
   
2.  Install IEGenLib from https://github.com/taknevski/IEGenLib.git.

        git clone https://github.com/taknevski/IEGenLib.git

    The README file for IEGenLib is old.  Please do the following:
    
        cd IEGenLib
        ./configure
        make
        make install

    This will install into a directory IEGenLib/iegenlib
    Set IEGEN_HOME to /full/path/to/IEGenLib/iegen, which will
    be provided at the end of the "make install".
        
        export IEGEN_HOME=/path/to/IEGenLib/iegen

3.  Install LUA version 5.1.5.

        wget https://www.lua.org/ftp/lua-5.1.5.tar.gz
        
        gunzip lua-5.1.5.tar.gz
        tar xvf lua-5.1.5.tar.gz
        
        cd lua-5.1.5
        make generic

    Set the LUAHOME environment variable to the appropriate directory.

        export LUAHOME=/path/to/lua-5.1.5/src


4. Now go back into the pldi15 project.
   Install the included omega by doing the following commands:

        cd pldi15
        cd omega
        make clean
        make veryclean
        make depend
        make

    As long as the libcodegen library was built, you have enough.
        ar -rs libcodegen.a codegen.o CG_stringBuilder.o CG.o CG_utils.o CG_roseRepr.o CG_roseBuilder.o
        ar: creating libcodegen.a


    Set your OMEGAHOME environment variable to the appropriate directory.

        export OMEGAHOME=/path/to/omega


5.  If you are installing for CUDA-CHILL set the CUDACHILL environment
    variable to true else false.

        export CUDACHILL=false

    NOTE that CUDA and CUDACHILL need to be installed for testing ELL and DIA.


6.  Install cuda-chill or chill (BCSR results use chill, ELL and DIA results
    use cuda-chill) by doing the following commands:

        cd chill
        make clean
        make depend-cuda-chill
        make cuda-chill

    else if you are installing just plain chill
    
        export CUDACHILL=false;
        cd chill
        make clean
        make depend
        make

    ***IMPORTANT NOTE: If you want both cuda-chill and chill executables
    to co-exist do a make clean after building one and prior to building
    the other.  However do not do "make veryclean"


7.  To reproduce PLDI 15 experiments (BCSR, ELL, and DIA results)
    please refer to README in the chill/tests/ subdirectory.
        more chill/tests/README



