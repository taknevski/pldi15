/*
 * sparse_partition.cc
 *
 *  Created on: Oct 14, 2015
 *      Author: anand
 */




void Loop::sparse_partition(int stmt_num, int level, std::string name) {

	// get stmt IS and XFORM

	// check that level is the first loop level carrying a dependence
	if (level != 1)
		throw loop_error(
				"Sparse partition only supports outer most level for now!");

	//
	apply_xform(stmt_num);

	//Relation new_IS(stmt[stmt_num].IS.n_set() + 1);
	//Relation new_xform(stmt[stmt_num].xform.n_inp() + 1, stmt[stmt_num].xform.n_out() + 2);

	//Dependence testing and call to schedule construction
	//goes here



	//Create Executor IS
	Relation outer_loop(1);
	F_And *f_root = outer_loop.add_and();
	GEQ_Handle h = f_root->add_GEQ();
	h.update_coef(outer_loop.set_var(1), 1);
	GEQ_Handle h2 = f_root->add_GEQ();
	h2.update_coef(outer_loop.set_var(1), -1);



	Free_Var_Decl *ub = new Free_Var_Decl("ub");

	Variable_ID csr_ub = outer_loop.get_local(ub);

	h2.update_coef(csr_ub, 1);
	h2.update_const(-1);

	stmt[stmt_num].IS.get_local(ub);
	Relation partial_IS = replicate_IS_and_add_at_pos(stmt[stmt_num].IS, 1,
			outer_loop);

	Relation outer_loops(2);
	F_And *f_root2 = outer_loops.add_and();
	GEQ_Handle h3 = f_root2->add_GEQ();
	h3.update_coef(outer_loops.set_var(1), 1);
	GEQ_Handle h4 = f_root2->add_GEQ();
	h4.update_coef(outer_loops.set_var(1), -1);

	Variable_ID csr_ub2 = outer_loops.get_local(ub);

	h4.update_coef(csr_ub2, 1);
	h4.update_const(-1);

	Free_Var_Decl *ub2 = new Free_Var_Decl(name, 1);
	Variable_ID insp = outer_loops.get_local(ub2, Input_Tuple);

	Free_Var_Decl *ub3 = new Free_Var_Decl(name + "_", 1);
	Variable_ID insp2 = outer_loops.get_local(ub3, Input_Tuple);

	partial_IS.get_local(ub2, Input_Tuple);
	partial_IS.get_local(ub3, Input_Tuple);

	GEQ_Handle h5 = f_root2->add_GEQ();
	h5.update_coef(outer_loops.set_var(2), 1);
	h5.update_coef(insp, -1);

	GEQ_Handle h6 = f_root2->add_GEQ();
	h6.update_coef(outer_loops.set_var(2), -1);
	h6.update_coef(insp2, 1);
	h6.update_const(-1);

	Relation new_IS = and_with_relation_and_replace_var(partial_IS,
			partial_IS.set_var(2), outer_loops);

	Relation new_xform(stmt[stmt_num].xform.n_inp() + 1,
			stmt[stmt_num].xform.n_out() + 2);
	F_And *froot4 = new_xform.add_and();

	for (int i = 1; i <= new_IS.n_set(); i++) {
		EQ_Handle h = froot4->add_EQ();
		h.update_coef(new_xform.input_var(i), 1);
		h.update_coef(new_xform.output_var(2 * i), -1);
	}

	for (int i = 1; i <= new_xform.n_out(); i += 2) {
		EQ_Handle h = froot4->add_EQ();
		h.update_coef(new_xform.output_var(i), 1);
	}

	new_IS.get_local(ub2, Input_Tuple);
	new_IS.get_local(ub3, Input_Tuple);

	this->known.get_local(ub2, Input_Tuple);
	this->known.get_local(ub3, Input_Tuple);

	stmt[stmt_num].IS = new_IS;
	stmt[stmt_num].xform = new_xform;
	freevar.push_back(ub2);
	freevar.push_back(ub3);
	LoopLevel ll;
	ll.type = LoopLevelOriginal;
	ll.payload = 1;
	ll.parallel_level = 0;
	ll.segreducible = false;
	ll.made_dense = false;

	std::vector<LoopLevel>::iterator it = stmt[stmt_num].loop_level.begin();
	stmt[stmt_num].loop_level.insert(it, ll);

}
