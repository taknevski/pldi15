/*****************************************************************************
 Copyright (C) 2009-2010 University of Utah
 All Rights Reserved.

 Purpose:
 CHiLL's rose interface.

 Notes:
 Array supports mixed pointer and array type in a single declaration.
 
 History:
 02/23/2009 Created by Chun Chen.
 *****************************************************************************/
#include <string>
#include "ir_rose.hh"

#include <code_gen/CG_roseBuilder.h>

using namespace SageBuilder;
using namespace SageInterface;
using namespace omega;

std::vector<IR_CONDITION_TYPE> recurseOnTestExp(SgExpression *test_expr) {

	std::vector<IR_CONDITION_TYPE> a;

	if (isSgGreaterOrEqualOp(test_expr))
		a.push_back(IR_COND_GE);

	else if (isSgGreaterThanOp(test_expr))
		a.push_back(IR_COND_GT);
	else if (isSgLessThanOp(test_expr))
		a.push_back(IR_COND_LT);
	else if (isSgLessOrEqualOp(test_expr))
		a.push_back(IR_COND_LE);
	else if (isSgAndOp(test_expr) || isSgOrOp(test_expr)) {

		std::vector<IR_CONDITION_TYPE> b = recurseOnTestExp(
				isSgBinaryOp(test_expr)->get_lhs_operand());
		std::copy(b.begin(), b.end(), back_inserter(a));
		b = recurseOnTestExp(isSgBinaryOp(test_expr)->get_rhs_operand());
		std::copy(b.begin(), b.end(), back_inserter(a));
	}

	return a;

}

SgExpression * getMatchingUpperBound(std::string name, SgExpression *cond) {

	if (isSgAndOp(cond) || isSgOrOp(cond)) {
		SgExpression *b;
		b = getMatchingUpperBound(name, isSgBinaryOp(cond)->get_lhs_operand());
		if (b)
			return b;
		b = getMatchingUpperBound(name, isSgBinaryOp(cond)->get_rhs_operand());
		if (b)
			return b;
	} else if (isSgGreaterOrEqualOp(cond) || isSgGreaterThanOp(cond)
			|| isSgLessThanOp(cond) || isSgLessOrEqualOp(cond)) {
		SgExpression *b;
		b = isSgBinaryOp(cond)->get_lhs_operand();
		if (isSgVarRefExp(b))
			if (isSgVarRefExp(b)->get_symbol()->get_name().getString() == name)
				return isSgBinaryOp(cond)->get_rhs_operand();

		return NULL;
	}

	return NULL;

}

std::vector<SgVariableSymbol *> collectVarSymsRecursive(
		SgExpression* stop_condition) {
	std::vector<SgVariableSymbol *> a;

	if (isSgVarRefExp(stop_condition)) {
		a.push_back(isSgVarRefExp(stop_condition)->get_symbol());

	} else if (isSgGreaterOrEqualOp(stop_condition)
			|| isSgGreaterThanOp(stop_condition)
			|| isSgLessThanOp(stop_condition)
			|| isSgLessOrEqualOp(stop_condition)) {
		std::vector<SgVariableSymbol *> b = collectVarSymsRecursive(
				isSgBinaryOp(stop_condition)->get_lhs_operand());

		a = b;
	} else if (isSgAndOp(stop_condition) || isSgOrOp(stop_condition)) {

		std::vector<SgVariableSymbol *> b = collectVarSymsRecursive(
				isSgBinaryOp(stop_condition)->get_lhs_operand());
		std::copy(b.begin(), b.end(), back_inserter(a));
		b = collectVarSymsRecursive(
				isSgBinaryOp(stop_condition)->get_rhs_operand());
		std::copy(b.begin(), b.end(), back_inserter(a));
	}

	return a;

}

// ----------------------------------------------------------------------------
// Class: IR_roseScalarSymbol
// ----------------------------------------------------------------------------

std::string IR_roseScalarSymbol::name() const {
	if (scalar_within_struct == NULL)
		return vs_->get_name().getString();
	else
		return isSgNode(scalar_within_struct)->unparseToString();
}

int IR_roseScalarSymbol::size() const {
	return (vs_->get_type()->memoryUsage()) / (vs_->get_type()->numberOfNodes());
}

bool IR_roseScalarSymbol::operator==(const IR_Symbol &that) const {
	if (typeid(*this) != typeid(that))
		return false;

	const IR_roseScalarSymbol *l_that =
			static_cast<const IR_roseScalarSymbol *>(&that);
	return this->vs_ == l_that->vs_;
}

IR_Symbol *IR_roseScalarSymbol::clone() const {
	return NULL;
}

// ----------------------------------------------------------------------------
// Class: IR_roseScalarSymbol
// ----------------------------------------------------------------------------

std::string IR_roseFunctionSymbol::name() const {

	return fs_->get_name().getString();

}

bool IR_roseFunctionSymbol::operator==(const IR_Symbol &that) const {
	if (typeid(*this) != typeid(that))
		return false;

	const IR_roseFunctionSymbol *l_that =
			static_cast<const IR_roseFunctionSymbol *>(&that);
	return this->fs_ == l_that->fs_;
}

IR_Symbol *IR_roseFunctionSymbol::clone() const {
	return NULL;
}
// ----------------------------------------------------------------------------
// Class: IR_rosePointerSymbol
// ----------------------------------------------------------------------------

std::string IR_rosePointerSymbol::name() const {
	return name_;
}

IR_CONSTANT_TYPE IR_rosePointerSymbol::elem_type() const {

	SgType *tn = vs_->get_type();

	while (isSgPointerType(tn))
		tn = isSgPointerType(tn)->findBaseType();

	if (isSgTypeInt(tn))
		return IR_CONSTANT_INT;
	else if (isSgTypeUnsignedShort(tn))
		return IR_CONSTANT_SHORT;
	else if (isSgTypeFloat(tn))
		return IR_CONSTANT_FLOAT;
	else if (isSgClassType(tn)) {
		SgTemplateInstantiationDecl *decl;
		if (decl = isSgTemplateInstantiationDecl(
				isSgClassType(tn)->get_declaration())) {
			//SgTemplateDeclaration *cls = decl->get_templateDeclaration();
			  SgTemplateClassDeclaration *cls = decl->get_templateDeclaration();    
                        if (cls->get_name().getString() == "vector") {
				if (isSgNamespaceDefinitionStatement(cls->get_scope()))
					if (isSgNamespaceDefinitionStatement(cls->get_scope())->get_namespaceDeclaration()->search_for_symbol_from_symbol_table()->get_name().getString()
							== "std") {
						SgTemplateArgumentPtrList args =
								decl->get_templateArguments();
						assert(args.size() == 1);
						SgTemplateArgument *arg = *(args.begin());
					if(isSgTypeInt(arg->get_type()))
						return IR_CONSTANT_INT;
					else if(isSgTypeFloat(arg->get_type()))
						return IR_CONSTANT_FLOAT;

				}

		}

	}

}

return IR_CONSTANT_UNKNOWN;

}

int IR_rosePointerSymbol::n_dim() const {

return dim_;
}

omega::CG_outputRepr *IR_rosePointerSymbol::size(int dim) const {

return dims[dim];
}

void IR_rosePointerSymbol::set_size(int dim, omega::CG_outputRepr * repr) {

dims[dim] = repr;
}

bool IR_rosePointerSymbol::operator==(const IR_Symbol &that) const {

if (typeid(*this) != typeid(that))
	return false;

const IR_rosePointerSymbol *l_that =
		static_cast<const IR_rosePointerSymbol *>(&that);
return this->vs_ == l_that->vs_;

}

IR_Symbol *IR_rosePointerSymbol::clone() const {
return new IR_rosePointerSymbol(ir_, vs_, dim_);
}

// ----------------------------------------------------------------------------
// Class: IR_roseArraySymbol
// ----------------------------------------------------------------------------

std::string IR_roseArraySymbol::name() const {
if (name_ == "")
	return (vs_->get_declaration()->get_name().getString());
else
	return name_;
}

int IR_roseArraySymbol::elem_size() const {

SgType *tn = vs_->get_type();
SgType* arrType;

int elemsize;

if (arrType = isSgArrayType(tn)) {
	while (isSgArrayType(arrType)) {
		arrType = arrType->findBaseType();
	}
} else if (arrType = isSgPointerType(tn)) {
	while (isSgPointerType(arrType)) {
		arrType = arrType->findBaseType();
	}
}

elemsize = (int) arrType->memoryUsage() / arrType->numberOfNodes();
return elemsize;
}

int IR_roseArraySymbol::n_dim() const {
int dim = 0;
SgType* arrType = isSgArrayType(vs_->get_type());
SgType* ptrType = isSgPointerType(vs_->get_type());
if (arrType != NULL) {
	while (isSgArrayType(arrType)) {
		arrType = isSgArrayType(arrType)->get_base_type();
		dim++;
	}
} else if (ptrType != NULL) {
	while (isSgPointerType(ptrType) || isSgArrayType(ptrType)) {
		if (isSgPointerType(ptrType))
			ptrType = isSgPointerType(ptrType)->get_base_type();
		else if (isSgArrayType(ptrType))
			ptrType = isSgArrayType(ptrType)->get_base_type();

		dim++;
	}
}

return dim;
}

omega::CG_outputRepr *IR_roseArraySymbol::size(int dim) const {

SgArrayType* arrType = isSgArrayType(vs_->get_type());
// SgExprListExp* dimList = arrType->get_dim_info();
int count = 0;
SgExpression* expr;
SgType* pntrType = isSgPointerType(vs_->get_type());

if (arrType != NULL) {
	SgExprListExp* dimList = arrType->get_dim_info();

	if (dimList) {
		if (!static_cast<const IR_roseCode *>(ir_)->is_fortran_) {
			SgExpressionPtrList::iterator it =
					dimList->get_expressions().begin();

			while ((it != dimList->get_expressions().end()) && (count < dim)) {
				it++;
				count++;
			}

			expr = *it;
		} else {
			SgExpressionPtrList::reverse_iterator i =
					dimList->get_expressions().rbegin();
			for (; (i != dimList->get_expressions().rend()) && (count < dim);
					i++) {

				count++;
			}

			expr = *i;
		}
	} else if (dim == 0)
		expr = arrType->get_index();

} else if (pntrType != NULL) {

	while (count < dim) {
		pntrType = (isSgPointerType(pntrType))->get_base_type();
		count++;
	}
	if (isSgPointerType(pntrType))
		expr = new SgExpression;
}

if (!expr)
	throw ir_error("Index variable is NULL!!");

return new omega::CG_roseRepr(expr);

}

IR_ARRAY_LAYOUT_TYPE IR_roseArraySymbol::layout_type() const {
if (static_cast<const IR_roseCode *>(ir_)->is_fortran_)
	return IR_ARRAY_LAYOUT_COLUMN_MAJOR;
else
	return IR_ARRAY_LAYOUT_ROW_MAJOR;

}

IR_CONSTANT_TYPE IR_roseArraySymbol::elem_type() const {

SgType *tn = vs_->get_type();

while (isSgArrayType(tn))
	tn = isSgArrayType(tn)->findBaseType();

if (isSgTypeInt(tn))
	return IR_CONSTANT_INT;
else if (isSgTypeFloat(tn))
	return IR_CONSTANT_FLOAT;
else {

	while (isSgPointerType(tn))
		tn = isSgPointerType(tn)->findBaseType();

	if (isSgTypeInt(tn))
		return IR_CONSTANT_INT;
	else if (isSgTypeFloat(tn))
		return IR_CONSTANT_FLOAT;
		else if (isSgClassType(tn)) {
			SgTemplateInstantiationDecl *decl;
			if (decl = isSgTemplateInstantiationDecl(
					isSgClassType(tn)->get_declaration())) {
				//SgTemplateDeclaration *cls = decl->get_templateDeclaration();
				 SgTemplateClassDeclaration *cls = decl->get_templateDeclaration();
                                 if (cls->get_name().getString() == "vector") {
					if (isSgNamespaceDefinitionStatement(cls->get_scope()))
						if (isSgNamespaceDefinitionStatement(cls->get_scope())->get_namespaceDeclaration()->search_for_symbol_from_symbol_table()->get_name().getString()
								== "std") {
							SgTemplateArgumentPtrList args =
									decl->get_templateArguments();
							assert(args.size() == 1);
							SgTemplateArgument *arg = *(args.begin());
						if(isSgTypeInt(arg->get_type()))
							return IR_CONSTANT_INT;
						else if(isSgTypeFloat(arg->get_type()))
							return IR_CONSTANT_FLOAT;

					}

			}

		}

	}




	//return IR_CONSTANT_UNKNOWN;
}
return IR_CONSTANT_UNKNOWN;
}

bool IR_roseArraySymbol::operator==(const IR_Symbol &that) const {

if (typeid(*this) != typeid(that))
	return false;

const IR_roseArraySymbol *l_that =
		static_cast<const IR_roseArraySymbol *>(&that);
return this->vs_ == l_that->vs_;

}

IR_Symbol *IR_roseArraySymbol::clone() const {
return new IR_roseArraySymbol(ir_, vs_, "", NULL);
}

// ----------------------------------------------------------------------------
// Class: IR_roseConstantRef
// ----------------------------------------------------------------------------

bool IR_roseConstantRef::operator==(const IR_Ref &that) const {

if (typeid(*this) != typeid(that))
	return false;

const IR_roseConstantRef *l_that =
		static_cast<const IR_roseConstantRef *>(&that);

if (this->type_ != l_that->type_)
	return false;

if (this->type_ == IR_CONSTANT_INT)
	return this->i_ == l_that->i_;
else
	return this->f_ == l_that->f_;

}

omega::CG_outputRepr *IR_roseConstantRef::convert() {
if (type_ == IR_CONSTANT_INT) {
	omega::CG_roseRepr *result = new omega::CG_roseRepr(
			isSgExpression(buildIntVal(static_cast<int>(i_))));
	delete this;
	return result;
} else
	throw ir_error("constant type not supported");

}

IR_Ref *IR_roseConstantRef::clone() const {
if (type_ == IR_CONSTANT_INT)
	return new IR_roseConstantRef(ir_, i_);
else if (type_ == IR_CONSTANT_FLOAT)
	return new IR_roseConstantRef(ir_, f_);
else
	throw ir_error("constant type not supported");

}

// ----------------------------------------------------------------------------
// Class: IR_roseScalarRef
// ----------------------------------------------------------------------------

bool IR_roseScalarRef::is_write() const {
/*	if (ins_pos_ != NULL && op_pos_ == -1)
 return true;
 else
 return false;
 */

if (is_write_ == 1)
	return true;

return false;
}

IR_ScalarSymbol *IR_roseScalarRef::symbol() const {
if (struct_exp == NULL)
	return new IR_roseScalarSymbol(ir_, vs_->get_symbol());
else
	return new IR_roseScalarSymbol(ir_, vs_->get_symbol(), struct_exp);
}

bool IR_roseScalarRef::operator==(const IR_Ref &that) const {
if (typeid(*this) != typeid(that))
	return false;

const IR_roseScalarRef *l_that = static_cast<const IR_roseScalarRef *>(&that);

if (this->ins_pos_ == NULL)
	return this->vs_ == l_that->vs_;
else
	return this->ins_pos_ == l_that->ins_pos_
			&& this->op_pos_ == l_that->op_pos_;
}

omega::CG_outputRepr *IR_roseScalarRef::convert() {
omega::CG_roseRepr *result = new omega::CG_roseRepr(isSgExpression(vs_));
delete this;
return result;

}

IR_Ref * IR_roseScalarRef::clone() const {
//if (ins_pos_ == NULL)
return new IR_roseScalarRef(ir_, vs_, this->is_write_);
//else
// 		return new IR_roseScalarRef(ir_, , op_pos_);

}

// ----------------------------------------------------------------------------
// Class: IR_roseFunctionRef
// ----------------------------------------------------------------------------

bool IR_roseFunctionRef::is_write() const {
/*	if (ins_pos_ != NULL && op_pos_ == -1)
 return true;
 else
 return false;
 */

if (is_write_ == 1)
	return true;

return false;
}

IR_FunctionSymbol *IR_roseFunctionRef::symbol() const {

return new IR_roseFunctionSymbol(ir_, vs_->get_symbol());

}

bool IR_roseFunctionRef::operator==(const IR_Ref &that) const {
if (typeid(*this) != typeid(that))
	return false;

const IR_roseFunctionRef *l_that =
		static_cast<const IR_roseFunctionRef *>(&that);

return this->vs_ == l_that->vs_;

}

omega::CG_outputRepr *IR_roseFunctionRef::convert() {
omega::CG_roseRepr *result = new omega::CG_roseRepr(isSgExpression(vs_));
delete this;
return result;

}

IR_Ref * IR_roseFunctionRef::clone() const {
//if (ins_pos_ == NULL)
return new IR_roseFunctionRef(ir_, vs_);
//else
// 		return new IR_roseScalarRef(ir_, , op_pos_);

}
// ----------------------------------------------------------------------------
// Class: IR_roseArrayRef
// ----------------------------------------------------------------------------

bool IR_rosePointerArrayRef::is_write() const {
SgAssignOp* assignment;

if (is_write_ == 1 || is_write_ == 0)
	return is_write_;
if (assignment = isSgAssignOp(ia_->get_parent())) {
	if (assignment->get_lhs_operand() == ia_)
		return true;
} else if (SgExprStatement* expr_stmt = isSgExprStatement(ia_->get_parent())) {
	SgExpression* exp = expr_stmt->get_expression();

	if (exp) {
		if (assignment = isSgAssignOp(exp)) {
			if (assignment->get_lhs_operand() == ia_)
				return true;

		}
	}

}
return false;
}

omega::CG_outputRepr *IR_rosePointerArrayRef::index(int dim) const {

SgExpression *current = isSgExpression(ia_);
SgExpression* expr;
int count = 0;

while (isSgPntrArrRefExp(current)) {
	current = isSgPntrArrRefExp(current)->get_lhs_operand();
	count++;
}

current = ia_;

while (count > dim) {
	expr = isSgPntrArrRefExp(current)->get_rhs_operand();
	current = isSgPntrArrRefExp(current)->get_lhs_operand();
	count--;
}

if (!expr)
	throw ir_error("Index variable is NULL!!");

omega::CG_roseRepr* ind = new omega::CG_roseRepr(expr);

//Anand changing clone below:
//return ind->clone();
return ind;
}

IR_PointerSymbol *IR_rosePointerArrayRef::symbol() const {

return sym_;

}

bool IR_rosePointerArrayRef::operator==(const IR_Ref &that) const {
if (typeid(*this) != typeid(that))
	return false;

const IR_rosePointerArrayRef *l_that =
		static_cast<const IR_rosePointerArrayRef *>(&that);

return this->ia_ == l_that->ia_;
}

omega::CG_outputRepr *IR_rosePointerArrayRef::convert() {
omega::CG_roseRepr *temp = new omega::CG_roseRepr(isSgExpression(this->ia_));
//	std::cout<< "Manu:: "<< (isSgExpression(this->ia_))->unparseToString() << "\n";
omega::CG_outputRepr *result = temp->clone();
//	delete this;   // Commented by Manu
return result;
}

IR_Ref *IR_rosePointerArrayRef::clone() const {
return new IR_roseArrayRef(ir_, ia_, is_write_);
}

// ----------------------------------------------------------------------------
// Class: IR_roseArrayRef
// ----------------------------------------------------------------------------

bool IR_roseArrayRef::is_write() const {
SgAssignOp* assignment;

if (is_write_ == 1 || is_write_ == 0)
	return is_write_;
if (assignment = isSgAssignOp(ia_->get_parent())) {
	if (assignment->get_lhs_operand() == ia_)
		return true;
} else if (SgExprStatement* expr_stmt = isSgExprStatement(ia_->get_parent())) {
	SgExpression* exp = expr_stmt->get_expression();

	if (exp) {
		if (assignment = isSgAssignOp(exp)) {
			if (assignment->get_lhs_operand() == ia_)
				return true;

		}
	}

}
return false;
}

omega::CG_outputRepr *IR_roseArrayRef::index(int dim) const {

SgExpression *current = isSgExpression(ia_);
SgExpression* expr;
int count = 0;

while (isSgPntrArrRefExp(current)) {
	current = isSgPntrArrRefExp(current)->get_lhs_operand();
	count++;
}

current = ia_;

while (count > dim) {
	expr = isSgPntrArrRefExp(current)->get_rhs_operand();
	current = isSgPntrArrRefExp(current)->get_lhs_operand();
	count--;
}

if (!expr)
	throw ir_error("Index variable is NULL!!");

omega::CG_roseRepr* ind = new omega::CG_roseRepr(expr);

//Anand:commenting out clone
//return ind->clone();
return ind;
}

IR_ArraySymbol *IR_roseArrayRef::symbol() const {

SgExpression *current = isSgExpression(ia_);
SgDotExp *dot_;
SgVarRefExp* base;
SgVariableSymbol *arrSymbol;
while (isSgPntrArrRefExp(current) || isSgUnaryOp(current)) {
	if (isSgPntrArrRefExp(current))
		current = isSgPntrArrRefExp(current)->get_lhs_operand();
	else if (isSgUnaryOp(current))
		/* To handle support for addressof operator and pointer dereference
		 * both of which are unary ops
		 */
		current = isSgUnaryOp(current)->get_operand();
}
if (base = isSgVarRefExp(current)) {
	arrSymbol = (SgVariableSymbol*) (base->get_symbol());
	std::string x = arrSymbol->get_name().getString();
}
//Anand : Adding support for expressions like a.i[k]
else if (dot_ = isSgDotExp(current)) {
	base = isSgVarRefExp(dot_->get_rhs_operand());
	arrSymbol = (SgVariableSymbol*) (base->get_symbol());
	std::string struct_name =
			isSgVarRefExp(dot_->get_lhs_operand())->get_symbol()->get_name().getString();
	std::string name = struct_name + "." + arrSymbol->get_name().getString();
	//arr_within_struct = dot_;
	if (!base)
		throw ir_error("Array Symbol is not a variable?!");

	return new IR_roseArraySymbol(ir_, arrSymbol, name, dot_);

} else
	throw ir_error("Array Symbol is not a variable?!");

return new IR_roseArraySymbol(ir_, arrSymbol, "", NULL);

}

bool IR_roseArrayRef::operator==(const IR_Ref &that) const {
if (typeid(*this) != typeid(that))
	return false;

const IR_roseArrayRef *l_that = static_cast<const IR_roseArrayRef *>(&that);

return this->ia_ == l_that->ia_;
}

omega::CG_outputRepr *IR_roseArrayRef::convert() {
omega::CG_roseRepr *temp = new omega::CG_roseRepr(isSgExpression(this->ia_));
//	std::cout<< "Manu:: "<< (isSgExpression(this->ia_))->unparseToString() << "\n";
omega::CG_outputRepr *result = temp->clone();
//	delete this;   // Commented by Manu
return result;
}

IR_Ref *IR_roseArrayRef::clone() const {
return new IR_roseArrayRef(ir_, ia_, is_write_);
}

// ----------------------------------------------------------------------------
// Class: IR_roseLoop
// ----------------------------------------------------------------------------

std::vector<IR_ScalarSymbol *> IR_roseLoop::index() const {
std::vector<IR_ScalarSymbol *> a;
SgForStatement *tf = isSgForStatement(tf_);
SgFortranDo *tfortran = isSgFortranDo(tf_);
SgWhileStmt *twh = isSgWhileStmt(tf_);
SgVariableSymbol* vs = NULL;
if (tf) {
	SgForInitStatement* list = tf->get_for_init_stmt();
	SgStatementPtrList& initStatements = list->get_init_stmt();
	SgStatementPtrList::const_iterator j = initStatements.begin();

	if (SgExprStatement *expr = isSgExprStatement(*j))
		if (SgAssignOp* op = isSgAssignOp(expr->get_expression()))
			if (SgVarRefExp* var_ref = isSgVarRefExp(op->get_lhs_operand()))
				vs = var_ref->get_symbol();
} else if (tfortran) {
	SgExpression* init = tfortran->get_initialization();

	if (SgAssignOp* op = isSgAssignOp(init))
		if (SgVarRefExp* var_ref = isSgVarRefExp(op->get_lhs_operand()))
			vs = var_ref->get_symbol();

} else if (twh) {

	std::vector<SgVariableSymbol *> syms = collectVarSymsRecursive(
			stop_condition);
	//need to verify syms are incremented within while body 8th March 2016 Anand
	// not done now
	//
	for (int i = 0; i < syms.size(); i++)
		a.push_back(new IR_roseScalarSymbol(ir_, syms[i]));

	return a;

}

if (vs == NULL)
	throw ir_error("Index variable is NULL!!");
a.push_back(new IR_roseScalarSymbol(ir_, vs));
return a;
}

std::vector<omega::CG_outputRepr *> IR_roseLoop::lower_bound() const {
SgForStatement *tf = isSgForStatement(tf_);
SgFortranDo *tfortran = isSgFortranDo(tf_);
SgWhileStmt *twh = isSgWhileStmt(tf_);
SgExpression* lowerBound = NULL;
std::vector<omega::CG_outputRepr *> to_return;
if (tf) {
	SgForInitStatement* list = tf->get_for_init_stmt();
	SgStatementPtrList& initStatements = list->get_init_stmt();
	SgStatementPtrList::const_iterator j = initStatements.begin();

	if (SgExprStatement *expr = isSgExprStatement(*j))
		if (SgAssignOp* op = isSgAssignOp(expr->get_expression()))
			lowerBound = op->get_rhs_operand();
} else if (tfortran) {
	SgExpression* init = tfortran->get_initialization();

	if (SgAssignOp* op = isSgAssignOp(init))
		lowerBound = op->get_rhs_operand();
} else if (twh) {
	std::vector<IR_ScalarSymbol *> vars = index();

	//ssa_unfiltered_cfg::SSA_UnfilteredCfg::NodeReachingDefTable to_compare =
	//		main_ssa->getReachingDefsBefore(
	//					isSgNode(twh));

	std::set<SgVarRefExp *> refs = main_ssa->getUsesAtNode(
			isSgNode(stop_condition));

	for (int i = 0; i < vars.size(); i++) {
		int found = 0;
		for (std::set<SgVarRefExp *>::iterator it4 = refs.begin();
				it4 != refs.end(); it4++) {

			if ((*it4)->get_symbol()->get_name().getString()
					== vars[i]->name()) {

				ssa_unfiltered_cfg::ReachingDef::ReachingDefPtr to_compare_2 =
						main_ssa->getDefinitionForUse(*it4);

				std::set<VirtualCFG::CFGNode> to_compare_set =
						to_compare_2->getActualDefinitions();
				for (std::set<VirtualCFG::CFGNode>::iterator cfg_it =
						to_compare_set.begin(); cfg_it != to_compare_set.end();
						cfg_it++) {
                                        if(isSgVariableDeclaration(cfg_it->getNode())){
                                             SgVariableDeclaration * varDeclStmt=isSgVariableDeclaration(cfg_it->getNode());
                                             SgVariableDefinition * varDef=varDeclStmt->get_definition();
                                             SgInitializedName *iniName=isSgInitializedName(varDef->get_vardefn());
                                             if (iniName->get_initializer () !=NULL){
                                               if(iniName->get_name().getString() == vars[i]->name()){
                                                //SgType * type=isSgAssignInitializer(iniName->get_initializer())->get_type ();
                                                SgAssignInitializer *sai=isSgAssignInitializer(iniName->get_initializer());
				                   if(sai){
                                                      to_return.push_back(new CG_roseRepr(sai->get_operand()));
                                                      found++;
                                                      break;            
                                                   }      
                                                 }	
                                             }
                                       }else if(SgInitializedName *in = isSgInitializedName(cfg_it->getNode())){
                                             if(in->get_name().getString() == vars[i]->name()){
                                                SgAssignInitializer *sai=isSgAssignInitializer(in->get_initializer());
                                                   if(sai){
                                                      to_return.push_back(new CG_roseRepr(sai->get_operand()));
                                                      found++;
                                                      break;
                                                   }
                      

                                             }
                                          } 

                                       else if (isSgAssignOp(cfg_it->getNode())
							|| isSgCompoundAssignOp(cfg_it->getNode()))
						if (SgVarRefExp* variable =
								isSgVarRefExp(
										isSgBinaryOp(cfg_it->getNode())->get_lhs_operand())) {
							if (variable->get_symbol()->get_name().getString()
									== vars[i]->name()) {
								to_return.push_back(
										new CG_roseRepr(
												isSgBinaryOp(cfg_it->getNode())->get_rhs_operand()));
								found++;
								break;
							}
						}

				}
			}

			if (found)
				break;
		}
		assert(found == 1);
	}
	return to_return;
}

if (lowerBound == NULL)
	throw ir_error("Lower Bound is NULL!!");
to_return.push_back(new omega::CG_roseRepr(lowerBound));
return to_return;
}

std::vector<omega::CG_outputRepr *> IR_roseLoop::upper_bound() const {
SgForStatement *tf = isSgForStatement(tf_);
SgFortranDo *tfortran = isSgFortranDo(tf_);
std::vector<omega::CG_outputRepr *> to_return;
SgExpression* upperBound = NULL;
if (tf) {
	SgBinaryOp* cond = isSgBinaryOp(tf->get_test_expr());
	if (cond == NULL)
		throw ir_error("Test Expression is NULL!!");

	if (isSgAndOp(cond) || isSgOrOp(cond)) {
		SgExpression *a, *b;
		a = isSgBinaryOp(cond)->get_lhs_operand();
		b = isSgBinaryOp(cond)->get_rhs_operand();
		if ((isSgLessThanOp(a) || isSgLessOrEqualOp(a))
				&& ((isSgLessThanOp(b) || isSgLessOrEqualOp(b)))) {
			to_return.push_back(new CG_roseRepr(isSgExpression(a)));
			to_return.push_back(new CG_roseRepr(isSgExpression(b)));
			return to_return;
		} else
			throw ir_error("Test Expression is NULL!!");
	} else
		upperBound = cond->get_rhs_operand();

	if (upperBound == NULL)
		throw ir_error("Upper Bound is NULL!!");
} else if (tfortran) {

	upperBound = tfortran->get_bound();

} else if (isSgWhileStmt(tf_)) {
	std::vector<IR_ScalarSymbol *> vars = index();
	SgExpression *ub;
	for (int i = 0; i < vars.size(); i++) {

		ub = getMatchingUpperBound(vars[i]->name(), stop_condition);
		to_return.push_back(new omega::CG_roseRepr(ub));

	}
	return to_return;
}

to_return.push_back(new omega::CG_roseRepr(upperBound));

return to_return;

}

std::vector<IR_CONDITION_TYPE> IR_roseLoop::stop_cond() const {
SgForStatement *tf = isSgForStatement(tf_);
SgFortranDo *tfortran = isSgFortranDo(tf_);
std::vector<IR_CONDITION_TYPE> to_return;
if (tf) {
	SgExpression* stopCond = NULL;
	SgExpression* test_expr = tf->get_test_expr();

	if (isSgLessThanOp(test_expr))
		to_return.push_back(IR_COND_LT);		//return IR_COND_LT;
	else if (isSgLessOrEqualOp(test_expr))
		to_return.push_back(IR_COND_LE);		//return IR_COND_LE;
	else if (isSgGreaterThanOp(test_expr))
		to_return.push_back(IR_COND_GT);		//return IR_COND_GT;
	else if (isSgGreaterOrEqualOp(test_expr))
		to_return.push_back(IR_COND_GE);		//return IR_COND_GE;
	else if (isSgAndOp(test_expr))
		to_return = recurseOnTestExp(test_expr);
	else
		throw ir_error("loop stop condition unsupported");
} else if (tfortran) {
	SgExpression* increment = tfortran->get_increment();
	if (!isSgNullExpression(increment)) {
		if (isSgMinusOp(increment)
				&& !isSgBinaryOp(isSgMinusOp(increment)->get_operand()))
			to_return.push_back(IR_COND_GE);		//return IR_COND_GE;
		else
			to_return.push_back(IR_COND_LE);		//return IR_COND_LE;
	} else {
		SgExpression* lowerBound = NULL;
		SgExpression* upperBound = NULL;
		SgExpression* init = tfortran->get_initialization();
		SgIntVal* ub;
		SgIntVal* lb;
		if (SgAssignOp* op = isSgAssignOp(init))
			lowerBound = op->get_rhs_operand();

		upperBound = tfortran->get_bound();

		if ((upperBound != NULL) && (lowerBound != NULL)) {

			if ((ub = isSgIntVal(isSgValueExp(upperBound)))
					&& (lb = isSgIntVal(isSgValueExp(lowerBound)))) {
				if (ub->get_value() > lb->get_value())
					to_return.push_back(IR_COND_LE);	//return IR_COND_LE;
				else
					to_return.push_back(IR_COND_GE);	//return IR_COND_GE;
			} else
				throw ir_error("loop stop condition unsupported");

		} else
			throw ir_error("malformed fortran loop bounds!!");

	}
} else if (isSgWhileStmt(tf_)) {
	SgExpression *test_expr = isSgExprStatement(
			isSgWhileStmt(tf_)->get_condition())->get_expression();

	to_return = recurseOnTestExp(test_expr);

}
return to_return;
}

IR_Block *IR_roseLoop::body() const {
SgForStatement *tf = isSgForStatement(tf_);
SgFortranDo *tfortran = isSgFortranDo(tf_);
SgNode* loop_body = NULL;
SgStatement* body_statements = NULL;

if (tf) {
	body_statements = tf->get_loop_body();
} else if (tfortran) {
	body_statements = isSgStatement(tfortran->get_body());

}

loop_body = isSgNode(body_statements);

SgStatementPtrList list;
if (isSgBasicBlock(loop_body)) {
	list = isSgBasicBlock(loop_body)->get_statements();

	if (list.size() == 1)
		loop_body = isSgNode(*(list.begin()));
}

if (loop_body == NULL)
	throw ir_error("for loop body is NULL!!");

return new IR_roseBlock(ir_, loop_body);
}

int IR_roseLoop::step_size() const {

SgForStatement *tf = isSgForStatement(tf_);
SgFortranDo *tfortran = isSgFortranDo(tf_);

if (tf) {
	SgExpression *increment = tf->get_increment();

	if (isSgPlusPlusOp(increment))
		return 1;
	if (isSgMinusMinusOp(increment))
		return -1;
	else if (SgAssignOp* assignment = isSgAssignOp(increment)) {
		SgBinaryOp* stepsize = isSgBinaryOp(assignment->get_lhs_operand());
		if (stepsize == NULL)
			throw ir_error("Step size expression is NULL!!");
		SgIntVal* step = isSgIntVal(stepsize->get_lhs_operand());
		return step->get_value();
	} else if (SgBinaryOp* inc = isSgPlusAssignOp(increment)) {
		SgIntVal* step = isSgIntVal(inc->get_rhs_operand());
		return (step->get_value());
	} else if (SgBinaryOp * inc = isSgMinusAssignOp(increment)) {
		SgIntVal* step = isSgIntVal(inc->get_rhs_operand());
		return -(step->get_value());
	} else if (SgBinaryOp * inc = isSgCompoundAssignOp(increment)) {
		SgIntVal* step = isSgIntVal(inc->get_rhs_operand());
		return (step->get_value());
	}

} else if (tfortran) {

	SgExpression* increment = tfortran->get_increment();

	if (!isSgNullExpression(increment)) {
		if (isSgMinusOp(increment)) {
			if (SgValueExp *inc = isSgValueExp(
					isSgMinusOp(increment)->get_operand()))
				if (isSgIntVal(inc))
					return -(isSgIntVal(inc)->get_value());
		} else {
			if (SgValueExp* inc = isSgValueExp(increment))
				if (isSgIntVal(inc))
					return isSgIntVal(inc)->get_value();
		}
	} else {

		SgExpression* lowerBound = NULL;
		SgExpression* upperBound = NULL;
		SgExpression* init = tfortran->get_initialization();
		SgIntVal* ub;
		SgIntVal* lb;
		if (SgAssignOp* op = isSgAssignOp(init))
			lowerBound = op->get_rhs_operand();

		upperBound = tfortran->get_bound();

		if ((upperBound != NULL) && (lowerBound != NULL)) {

			if ((ub = isSgIntVal(isSgValueExp(upperBound)))
					&& (lb = isSgIntVal(isSgValueExp(lowerBound)))) {
				if (ub->get_value() > lb->get_value())
					return 1;
				else
					return -1;
			} else
				throw ir_error("loop stop condition unsupported");

		} else
			throw ir_error("loop stop condition unsupported");

	}

} else if (isSgWhileStmt(tf_))
	return 1;		 // approximation, needs work Anand: 03/08/2016

}

IR_Block *IR_roseLoop::convert() {
const IR_Code *ir = ir_;
SgNode *tnl = isSgNode(tf_);
delete this;
return new IR_roseBlock(ir, tnl);
}

IR_Control *IR_roseLoop::clone() const {

return new IR_roseLoop(ir_, tf_);

}

// ----------------------------------------------------------------------------
// Class: IR_roseBlock
// ----------------------------------------------------------------------------

omega::CG_outputRepr *IR_roseBlock::original() const {

omega::CG_outputRepr * tnl;

if (isSgBasicBlock(tnl_)) {

	SgStatementPtrList *bb = new SgStatementPtrList();
	SgStatementPtrList::iterator it;
	for (it = (isSgBasicBlock(tnl_)->get_statements()).begin();
			it != (isSgBasicBlock(tnl_)->get_statements()).end()
					&& (*it != start_); it++)
		;

	if (it != (isSgBasicBlock(tnl_)->get_statements()).end()) {
		for (; it != (isSgBasicBlock(tnl_)->get_statements()).end(); it++) {
			bb->push_back(*it);
			if ((*it) == end_)
				break;
		}
	}
	tnl = new omega::CG_roseRepr(bb);
	//block = tnl->clone();

} else {
	tnl = new omega::CG_roseRepr(tnl_);

	//block = tnl->clone();
}

return tnl;

}
omega::CG_outputRepr *IR_roseBlock::extract() const {

std::string x = tnl_->unparseToString();

omega::CG_roseRepr * tnl;

omega::CG_outputRepr* block;

if (isSgBasicBlock(tnl_)) {

	SgStatementPtrList *bb = new SgStatementPtrList();
	SgStatementPtrList::iterator it;
	for (it = (isSgBasicBlock(tnl_)->get_statements()).begin();
			it != (isSgBasicBlock(tnl_)->get_statements()).end()
					&& (*it != start_); it++)
		;

	if (it != (isSgBasicBlock(tnl_)->get_statements()).end()) {
		for (; it != (isSgBasicBlock(tnl_)->get_statements()).end(); it++) {
			bb->push_back(*it);
			if ((*it) == end_)
				break;
		}
	}
	tnl = new omega::CG_roseRepr(bb);
	block = tnl;		 //->clone();

} else {
	tnl = new omega::CG_roseRepr(tnl_);

	block = tnl;		 //->clone();
}

//delete tnl;
return block;
}

IR_Control *IR_roseBlock::clone() const {
return new IR_roseBlock(ir_, tnl_, start_, end_);

}
// ----------------------------------------------------------------------------
// Class: IR_roseIf
// ----------------------------------------------------------------------------
omega::CG_outputRepr *IR_roseIf::condition() const {
SgNode *tnl = isSgNode(isSgIfStmt(ti_)->get_conditional());
SgExpression* exp = NULL;
if (SgExprStatement* stmt = isSgExprStatement(tnl))
	exp = stmt->get_expression();
/*
 SgExpression *op = iter(tnl);
 if (iter.is_empty())
 throw ir_error("unrecognized if structure");
 tree_node *tn = iter.step();
 if (!iter.is_empty())
 throw ir_error("unrecognized if structure");
 if (!tn->is_instr())
 throw ir_error("unrecognized if structure");
 instruction *ins = static_cast<tree_instr *>(tn)->instr();
 if (!ins->opcode() == io_bfalse)
 throw ir_error("unrecognized if structure");
 operand op = ins->src_op(0);*/
if (exp == NULL)
	return new omega::CG_roseRepr(tnl);
else
	return new omega::CG_roseRepr(exp);
}

IR_Block *IR_roseIf::then_body() const {
SgNode *tnl = isSgNode(isSgIfStmt(ti_)->get_true_body());

//tree_node_list *tnl = ti_->then_part();
if (tnl == NULL)
	return NULL;
/*
 tree_node_list_iter iter(tnl);
 if (iter.is_empty())
 return NULL; */

return new IR_roseBlock(ir_, tnl);
}

IR_Block *IR_roseIf::else_body() const {
SgNode *tnl = isSgNode(isSgIfStmt(ti_)->get_false_body());

//tree_node_list *tnl = ti_->else_part();

if (tnl == NULL)
	return NULL;
/*
 tree_node_list_iter iter(tnl);
 if (iter.is_empty())
 return NULL;*/

return new IR_roseBlock(ir_, tnl);
}

IR_Block *IR_roseIf::convert() {
const IR_Code *ir = ir_;
/* SgNode *tnl = ti_->get_parent();
 SgNode *start, *end;
 start = end = ti_;

 //tree_node_list *tnl = ti_->parent();
 //tree_node_list_e *start, *end;
 //start = end = ti_->list_e();
 */
delete this;
return new IR_roseBlock(ir, ti_);
}

IR_Control *IR_roseIf::clone() const {
return new IR_roseIf(ir_, ti_);
}

// -----------------------------------------------------------y-----------------
// Class: IR_roseCode_Global_Init
// ----------------------------------------------------------------------------

IR_roseCode_Global_Init *IR_roseCode_Global_Init::pinstance = 0;

IR_roseCode_Global_Init * IR_roseCode_Global_Init::Instance(char** argv) {
if (pinstance == 0) {
	pinstance = new IR_roseCode_Global_Init;
	pinstance->project = frontend(2, argv);

}
return pinstance;
}

// ----------------------------------------------------------------------------
// Class: IR_roseCode
// ----------------------------------------------------------------------------

//Protonu
SgGlobal *IR_roseCode::getroot() const {
return root;

}

std::vector<SgSymbolTable *> IR_roseCode::getsymtabs() const {
std::vector<SgSymbolTable *> symtabs;
symtabs.push_back(symtab_);
symtabs.push_back(symtab2_);
symtabs.push_back(symtab3_);
return symtabs;
}
//end--Protonu
IR_roseCode::IR_roseCode(const char *filename, const char* proc_name) :
	IR_Code() {

SgProject* project;

char* argv[2];
int counter = 0;
argv[0] = (char*) malloc(5 * sizeof(char));
argv[1] = (char*) malloc((strlen(filename) + 1) * sizeof(char));
strcpy(argv[0], "rose");
strcpy(argv[1], filename);

project = (IR_roseCode_Global_Init::Instance(argv))->project;
main_ssa = new ssa_unfiltered_cfg::SSA_UnfilteredCfg(project);
main_ssa->run();
firstScope = getFirstGlobalScope(project);
SgFilePtrList& file_list = project->get_fileList();

std::string orig_name = rose::StringUtility::stripPathFromFileName(filename);

for (SgFilePtrList::iterator it = file_list.begin(); it != file_list.end();
		it++) {
	file = isSgSourceFile(*it);
	if (file->get_outputLanguage() == SgFile::e_Fortran_output_language)
		is_fortran_ = true;
	else
		is_fortran_ = false;
	file->set_unparse_output_filename("rose_" + orig_name);
	root = file->get_globalScope();
	buildCpreprocessorDefineDeclaration(root,
			"#define __rose_lt(x,y) ((x)<(y)?(x):(y))",
			PreprocessingInfo::before);
	buildCpreprocessorDefineDeclaration(root,
			"#define __rose_gt(x,y) ((x)>(y)?(x):(y))",
			PreprocessingInfo::before);

	symtab_ = isSgScopeStatement(root)->get_symbol_table();
	SgDeclarationStatementPtrList& declList = root->get_declarations();

	p = declList.begin();

	while (p != declList.end()) {
		func = isSgFunctionDeclaration(*p);
		if (func) {
			if (!strcmp((func->get_name().getString()).c_str(), proc_name)) {
				proc_name_ = func->get_name().getString();
				break;
			}
		}
		p++;
		counter++;
	}
	if (p != declList.end())
		break;

}

symtab2_ = func->get_definition()->get_symbol_table();
symtab3_ = func->get_definition()->get_body()->get_symbol_table();
// ocg_ = new omega::CG_roseBuilder(func->get_definition()->get_body()->get_symbol_table() , isSgNode(func->get_definition()->get_body()));
ocg_ = new omega::CG_roseBuilder(root, firstScope,
		func->get_definition()->get_symbol_table(),
		func->get_definition()->get_body()->get_symbol_table(),
		isSgNode(func->get_definition()->get_body()));

i_ = 0; /*i_ handling may need revision */
rose_array_counter = 1;
rose_pointer_counter = 1;
free(argv[1]);
free(argv[0]);

}

IR_roseCode::~IR_roseCode() {

for (std::map<std::string, std::pair<std::string, SgNode *> >::iterator i =
		defined_macros.begin(); i != defined_macros.end(); i++) {

	if (i->second.second != NULL)
		buildCpreprocessorDefineDeclaration(root,
				"#define " + i->first + i->second.first + " "
						+ (i->second.second)->unparseToString(),
				PreprocessingInfo::before);
	//else
	//	buildCpreprocessorDefineDeclaration(root, "#define " + i->first,
	//			PreprocessingInfo::before);
}

(IR_roseCode_Global_Init::Instance(NULL))->project->unparse();
//backend((IR_roseCode_Global_Init::Instance(NULL))->project);
}

IR_ScalarSymbol *IR_roseCode::CreateScalarSymbol(IR_CONSTANT_TYPE type, int,
	std::string name, bool is_static, CG_outputRepr *initializer){
char str1[14];

SgType *tn;
if (type == IR_CONSTANT_INT)
	tn = buildIntType();
else if (type == IR_CONSTANT_FLOAT)
	tn = buildFloatType();
else if (type == IR_CONSTANT_DOUBLE)
	tn = buildDoubleType();
else
	throw ir_error("unrecognizd type");

SgVariableDeclaration *defn;
if (name == "") {
	sprintf(str1, "newVariable%i\0", i_);
      if(initializer == NULL)
	defn = buildVariableDeclaration(str1, tn);
   else
     
   defn  = buildVariableDeclaration(str1,
                      tn, buildAssignInitializer(static_cast<CG_roseRepr*>(initializer)->GetExpression()));

} else
{
      

       SgVariableSymbol *vs = symtab_->find_variable(SgName(name.c_str()));
        SgVariableSymbol *vs2 = symtab2_->find_variable(SgName(name.c_str()));

      if(vs== NULL && vs2  == NULL){

         if (initializer == NULL)
            defn = buildVariableDeclaration(name, tn);
         else

            defn  = buildVariableDeclaration(name,
                      tn, buildAssignInitializer(static_cast<CG_roseRepr*>(initializer)->GetExpression()));
      }else if (vs != NULL)
       return  new IR_roseScalarSymbol(this, vs);
      else 
        return  new IR_roseScalarSymbol(this, vs2);

         
}
i_++;

if(is_static)
   defn->get_declarationModifier().get_storageModifier().setStatic();


SgInitializedNamePtrList& variables = defn->get_variables();
SgInitializedNamePtrList::const_iterator i = variables.begin();
SgInitializedName* initializedName = *i;
SgVariableSymbol* vs = new SgVariableSymbol(initializedName);

prependStatement(defn, isSgScopeStatement(func->get_definition()->get_body()));
vs->set_parent(symtab_);

if(name == "")
symtab_->insert(str1, vs);
else
symtab_->insert(name, vs); 
if (vs == NULL)
	throw ir_error("in CreateScalarSymbol: vs is NULL!!");

return new IR_roseScalarSymbol(this, vs);
/*else if (typeid(*sym) == typeid(IR_roseArraySymbol)) {
 SgType *tn1 =
 static_cast<const IR_roseArraySymbol *>(sym)->vs_->get_type();
 while (isSgArrayType(tn1) || isSgPointerType(tn1)) {
 if (isSgArrayType(tn1))
 tn1 = isSgArrayType(tn1)->get_base_type();
 else if (isSgPointerType(tn1))
 tn1 = isSgPointerType(tn1)->get_base_type();
 else
 throw ir_error(
 "in CreateScalarSymbol: symbol not an array nor a pointer!");
 }

 sprintf(str1, "newVariable%i\0", i_);
 i_++;

 SgVariableDeclaration* defn1 = buildVariableDeclaration(str1, tn1);
 SgInitializedNamePtrList& variables1 = defn1->get_variables();

 SgInitializedNamePtrList::const_iterator i1 = variables1.begin();
 SgInitializedName* initializedName1 = *i1;

 SgVariableSymbol *vs1 = new SgVariableSymbol(initializedName1);
 prependStatement(defn1,
 isSgScopeStatement(func->get_definition()->get_body()));

 vs1->set_parent(symtab_);
 symtab_->insert(str1, vs1);

 if (vs1 == NULL)
 throw ir_error("in CreateScalarSymbol: vs1 is NULL!!");

 return new IR_roseScalarSymbol(this, vs1);
 } else
 throw std::bad_typeid();
 */

}

IR_ScalarSymbol *IR_roseCode::CreateScalarSymbol(const IR_Symbol *sym, int) {
char str1[14];
if (typeid(*sym) == typeid(IR_roseScalarSymbol)) {
	SgType *tn = static_cast<const IR_roseScalarSymbol *>(sym)->vs_->get_type();
	sprintf(str1, "newVariable%i\0", i_);
	SgVariableDeclaration* defn = buildVariableDeclaration(str1, tn);
	i_++;

	SgInitializedNamePtrList & variables = defn->get_variables();
	SgInitializedNamePtrList::const_iterator i = variables.begin();
	SgInitializedName* initializedName = *i;
	SgVariableSymbol* vs = new SgVariableSymbol(initializedName);

	prependStatement(defn,
			isSgScopeStatement(func->get_definition()->get_body()));
	vs->set_parent(symtab_);
	symtab_->insert(str1, vs);

	if (vs == NULL)
		throw ir_error("in CreateScalarSymbol: vs is NULL!!");

	return new IR_roseScalarSymbol(this, vs);
} else if (typeid(*sym) == typeid(IR_roseArraySymbol)) {
	SgType *tn1 = static_cast<const IR_roseArraySymbol *>(sym)->vs_->get_type();
	while (isSgArrayType(tn1) || isSgPointerType(tn1)) {
		if (isSgArrayType(tn1))
			tn1 = isSgArrayType(tn1)->get_base_type();
		else if (isSgPointerType(tn1))
			tn1 = isSgPointerType(tn1)->get_base_type();
		else
			throw ir_error(
					"in CreateScalarSymbol: symbol not an array nor a pointer!");
	}

	sprintf(str1, "newVariable%i\0", i_);
	i_++;

	SgVariableDeclaration* defn1 = buildVariableDeclaration(str1, tn1);
	SgInitializedNamePtrList& variables1 = defn1->get_variables();

	SgInitializedNamePtrList::const_iterator i1 = variables1.begin();
	SgInitializedName* initializedName1 = *i1;

	SgVariableSymbol *vs1 = new SgVariableSymbol(initializedName1);
	prependStatement(defn1,
			isSgScopeStatement(func->get_definition()->get_body()));

	vs1->set_parent(symtab_);
	symtab_->insert(str1, vs1);

	if (vs1 == NULL)
		throw ir_error("in CreateScalarSymbol: vs1 is NULL!!");

	return new IR_roseScalarSymbol(this, vs1);
} else
	throw std::bad_typeid();

}


CG_outputRepr *IR_roseCode::CreateDelete(CG_outputRepr *exp) {
                     
SgExpression *arg =
                static_cast<const omega::CG_roseRepr *>(exp)->GetExpression();

SgDeleteExp *exp2 = buildDeleteExp(arg);

return new CG_roseRepr(isSgNode(buildExprStatement(exp2)));


}

CG_outputRepr *IR_roseCode::CreateReturn(CG_outputRepr *exp) {

SgExpression *arg =
                static_cast<const omega::CG_roseRepr *>(exp)->GetExpression();

SgReturnStmt *exp2 = buildReturnStmt(arg);

return new CG_roseRepr(isSgNode(exp2));


}



CG_outputRepr *IR_roseCode::CreateFree(CG_outputRepr *exp) {

SgExpression *arg =
		static_cast<const omega::CG_roseRepr *>(exp)->GetExpression();

SgName name_free("free");

SgFunctionDeclaration * decl_free = buildNondefiningFunctionDeclaration(
		name_free, buildVoidType(), buildFunctionParameterList(), root);

SgExprListExp* args = buildExprListExp();
args->append_expression(arg);

//    decl_cuda_malloc->get_parameterList()->append_arg
SgFunctionCallExp *the_call = buildFunctionCallExp(
		buildFunctionRefExp(decl_free), args);

SgExprStatement* stmt = buildExprStatement(the_call);

return new CG_roseRepr(isSgNode(stmt));

}

CG_outputRepr *IR_roseCode::CreateMalloc(CG_outputRepr *type, std::string lhs,
	CG_outputRepr * size_repr) {
SgType *tn = isSgType(static_cast<const omega::CG_roseRepr *>(type)->GetCode());
char str1[14];

//	CG_outputRepr *size_of_op = new CG_roseRepr(buildSizeOfOp(tn));

SgExpression *size =
		static_cast<const omega::CG_roseRepr *>(size_repr)->GetExpression();

SgExpression *arg = isSgExpression(buildMultiplyOp(buildSizeOfOp(tn), size));

SgName name_malloc("malloc");

SgFunctionDeclaration * decl_malloc = buildNondefiningFunctionDeclaration(
		name_malloc, tn, buildFunctionParameterList(), root);

SgExprListExp* args = buildExprListExp();
args->append_expression(arg);

SgVariableSymbol *temp = symtab3_->find_variable(SgName(lhs.c_str()));

//    decl_cuda_malloc->get_parameterList()->append_arg
SgFunctionCallExp *the_call = buildFunctionCallExp(
		buildFunctionRefExp(decl_malloc), args);

SgAssignOp* stmt_ = buildAssignOp(buildVarRefExp(temp),
		buildCastExp(the_call, buildPointerType(tn)));

SgExprStatement* stmt = buildExprStatement(stmt_);

return new CG_roseRepr(isSgNode(stmt));

}

CG_outputRepr *IR_roseCode::CreateMalloc(const IR_CONSTANT_TYPE type,
	std::string lhs, CG_outputRepr * size_repr) {
SgType *tn;
char str1[14];

if (type == IR_CONSTANT_INT)
	tn = buildIntType();
else if (type == IR_CONSTANT_FLOAT)
	tn = buildFloatType();
else if (type == IR_CONSTANT_DOUBLE)
	tn = buildDoubleType();
else if (type == IR_CONSTANT_SHORT)
	tn = buildUnsignedShortType();
else
	throw ir_error("Unrecognized type");

//	CG_outputRepr *size_of_op = new CG_roseRepr(buildSizeOfOp(tn));

SgExpression *size =
		static_cast<const omega::CG_roseRepr *>(size_repr)->GetExpression();

SgExpression *arg = isSgExpression(buildMultiplyOp(buildSizeOfOp(tn), size));

SgName name_malloc("malloc");

SgFunctionDeclaration * decl_malloc = buildNondefiningFunctionDeclaration(
		name_malloc, tn, buildFunctionParameterList(), root);

SgExprListExp* args = buildExprListExp();
args->append_expression(arg);

SgVariableSymbol *temp = symtab3_->find_variable(SgName(lhs.c_str()));

//    decl_cuda_malloc->get_parameterList()->append_arg
SgFunctionCallExp *the_call = buildFunctionCallExp(
		buildFunctionRefExp(decl_malloc), args);

SgAssignOp* stmt_ = buildAssignOp(buildVarRefExp(temp),
		buildCastExp(the_call, buildPointerType(tn)));

SgExprStatement* stmt = buildExprStatement(stmt_);

return new CG_roseRepr(isSgNode(stmt));

}
CG_outputRepr *IR_roseCode::CreateMalloc(const IR_CONSTANT_TYPE type,
	CG_outputRepr * lhs, CG_outputRepr * size_repr) {
SgType *tn;
char str1[14];

if (type == IR_CONSTANT_INT)
	tn = buildIntType();
else if (type == IR_CONSTANT_FLOAT)
	tn = buildFloatType();
else if (type == IR_CONSTANT_DOUBLE)
	tn = buildDoubleType();
else if (type == IR_CONSTANT_SHORT)
	tn = buildUnsignedShortType();
else
	throw ir_error("Unrecognized type");

//	CG_outputRepr *size_of_op = new CG_roseRepr(buildSizeOfOp(tn));

SgExpression *size =
		static_cast<const omega::CG_roseRepr *>(size_repr)->GetExpression();

SgExpression *arg = isSgExpression(buildMultiplyOp(buildSizeOfOp(tn), size));

SgName name_malloc("malloc");

SgFunctionDeclaration * decl_malloc = buildNondefiningFunctionDeclaration(
		name_malloc, tn, buildFunctionParameterList(), root);

SgExprListExp* args = buildExprListExp();
args->append_expression(arg);

//SgVariableSymbol *temp = symtab3_->find_variable(SgName(lhs.c_str()));

//    decl_cuda_malloc->get_parameterList()->append_arg
SgFunctionCallExp *the_call = buildFunctionCallExp(
		buildFunctionRefExp(decl_malloc), args);

SgAssignOp* stmt_ = buildAssignOp(
		static_cast<CG_roseRepr *>(lhs)->GetExpression(),
		buildCastExp(the_call, buildPointerType(tn)));

SgExprStatement* stmt = buildExprStatement(stmt_);

return new CG_roseRepr(isSgNode(stmt));

}
IR_PointerSymbol *IR_roseCode::CreatePointerSymbol(const IR_CONSTANT_TYPE type,
	std::vector<CG_outputRepr *> &size_repr, std::string name) {
SgType *tn;
char str1[14];

if (type == IR_CONSTANT_INT)
	tn = buildIntType();
else if (type == IR_CONSTANT_FLOAT)
	tn = buildFloatType();
else if (type == IR_CONSTANT_DOUBLE)
	tn = buildDoubleType();
else if (type == IR_CONSTANT_SHORT)
	tn = buildUnsignedShortType();
for (int i = 0; i < size_repr.size(); i++) {

	tn = buildPointerType(tn);

}
//	static int rose_array_counter = 1;

std::string s;
if (name == "")
	s = std::string("_P_DATA") + omega::to_string(rose_pointer_counter++);
else
	s = name;

SgVariableDeclaration* defn2 = buildVariableDeclaration(
		const_cast<char *>(s.c_str()), tn);
SgInitializedNamePtrList& variables2 = defn2->get_variables();

SgInitializedNamePtrList::const_iterator i2 = variables2.begin();
SgInitializedName* initializedName2 = *i2;
SgVariableSymbol *vs = new SgVariableSymbol(initializedName2);

prependStatement(defn2, isSgScopeStatement(func->get_definition()->get_body()));

vs->set_parent(symtab_);
symtab_->insert(SgName(s.c_str()), vs);

return new IR_rosePointerSymbol(this, vs, size_repr.size());
}

IR_PointerSymbol *IR_roseCode::CreatePointerSymbol(CG_outputRepr *type,
	std::vector<CG_outputRepr *> &size_repr) {
SgType *tn = isSgType(static_cast<const omega::CG_roseRepr *>(type)->GetCode());
char str1[14];

for (int i = 0; i < size_repr.size(); i++) {

	tn = buildPointerType(tn);

}
//	static int rose_array_counter = 1;

std::string name;

name = std::string("_P_DATA") + omega::to_string(rose_pointer_counter++);

SgVariableDeclaration* defn2 = buildVariableDeclaration(
		const_cast<char *>(name.c_str()), tn);
SgInitializedNamePtrList& variables2 = defn2->get_variables();

SgInitializedNamePtrList::const_iterator i2 = variables2.begin();
SgInitializedName* initializedName2 = *i2;
SgVariableSymbol *vs = new SgVariableSymbol(initializedName2);

prependStatement(defn2, isSgScopeStatement(func->get_definition()->get_body()));

vs->set_parent(symtab_);
symtab_->insert(SgName(name.c_str()), vs);

return new IR_rosePointerSymbol(this, vs, size_repr.size());
}

IR_PointerSymbol *IR_roseCode::CreatePointerSymbol(CG_outputRepr *type,
	std::string name, bool is_static) {
SgType *tn = isSgType(static_cast<const omega::CG_roseRepr *>(type)->GetCode());
char str1[14];

tn = buildPointerType(tn);

//	static int rose_array_counter = 1;

SgVariableDeclaration* defn2 = buildVariableDeclaration(
		const_cast<char *>(name.c_str()), tn);

if(is_static)
   defn2->get_declarationModifier().get_storageModifier().setStatic();

SgInitializedNamePtrList& variables2 = defn2->get_variables();

SgInitializedNamePtrList::const_iterator i2 = variables2.begin();
SgInitializedName* initializedName2 = *i2;
SgVariableSymbol *vs = new SgVariableSymbol(initializedName2);

prependStatement(defn2, isSgScopeStatement(func->get_definition()->get_body()));

vs->set_parent(symtab_);
symtab_->insert(SgName(name.c_str()), vs);
return new IR_rosePointerSymbol(this, vs, 1);
}

IR_ArraySymbol *IR_roseCode::CreateArraySymbol(CG_outputRepr *type,
	std::vector<omega::CG_outputRepr *> &size) {
SgType *tn = isSgType(static_cast<const omega::CG_roseRepr *>(type)->GetCode());
char str1[14];

for (int i = size.size() - 1; i >= 0; i--)
	tn = buildArrayType(tn,
			static_cast<omega::CG_roseRepr *>(size[i])->GetExpression());

//	static int rose_array_counter = 1;
std::string s = std::string("_P") + omega::to_string(rose_array_counter++);
SgVariableDeclaration* defn2 = buildVariableDeclaration(
		const_cast<char *>(s.c_str()), tn);
SgInitializedNamePtrList& variables2 = defn2->get_variables();

SgInitializedNamePtrList::const_iterator i2 = variables2.begin();
SgInitializedName* initializedName2 = *i2;
SgVariableSymbol *vs = new SgVariableSymbol(initializedName2);

prependStatement(defn2, isSgScopeStatement(func->get_definition()->get_body()));

vs->set_parent(symtab_);
symtab_->insert(SgName(s.c_str()), vs);

return new IR_roseArraySymbol(this, vs, s, NULL);
}

IR_PointerSymbol *IR_roseCode::CreatePointerSymbol(const IR_Symbol *sym,
	std::vector<CG_outputRepr *> &size_repr) {
SgType *tn;
char str1[14];

if (typeid(*sym) == typeid(IR_roseScalarSymbol)) {
	tn = static_cast<const IR_roseScalarSymbol *>(sym)->vs_->get_type();
} else if (typeid(*sym) == typeid(IR_roseArraySymbol)) {
	tn = static_cast<const IR_roseArraySymbol *>(sym)->vs_->get_type();
	while (isSgArrayType(tn) || isSgPointerType(tn)) {
		if (isSgArrayType(tn))
			tn = isSgArrayType(tn)->get_base_type();
		else if (isSgPointerType(tn))
			tn = isSgPointerType(tn)->get_base_type();
		else
			throw ir_error(
					"in CreatePointerSymbol: symbol not an array nor a pointer!");
	}
} else if (typeid(*sym) == typeid(IR_rosePointerSymbol)) {
	tn = static_cast<const IR_rosePointerSymbol *>(sym)->vs_->get_type();
	while (isSgArrayType(tn) || isSgPointerType(tn)) {
		if (isSgArrayType(tn))
			tn = isSgArrayType(tn)->get_base_type();
		else if (isSgPointerType(tn))
			tn = isSgPointerType(tn)->get_base_type();
		else
			throw ir_error(
					"in CreatePointerSymbol: symbol not an array nor a pointer!");
	}
}

else
	throw std::bad_typeid();

for (int i = 0; i < size_repr.size(); i++) {
	tn = buildPointerType(tn);

}
//	static int rose_array_counter = 1;
std::string s = std::string("_P_DATA")
		+ omega::to_string(rose_pointer_counter++);
SgVariableDeclaration* defn2 = buildVariableDeclaration(
		const_cast<char *>(s.c_str()), tn);
SgInitializedNamePtrList& variables2 = defn2->get_variables();

SgInitializedNamePtrList::const_iterator i2 = variables2.begin();
SgInitializedName* initializedName2 = *i2;
SgVariableSymbol *vs = new SgVariableSymbol(initializedName2);

prependStatement(defn2, isSgScopeStatement(func->get_definition()->get_body()));

vs->set_parent(symtab_);
symtab_->insert(SgName(s.c_str()), vs);

return new IR_rosePointerSymbol(this, vs, size_repr.size());
}

IR_ArraySymbol *IR_roseCode::CreateArraySymbol(const IR_Symbol *sym,
	std::vector<omega::CG_outputRepr *> &size, int) {
SgType *tn;
char str1[14];

if (typeid(*sym) == typeid(IR_roseScalarSymbol)) {
	tn = static_cast<const IR_roseScalarSymbol *>(sym)->vs_->get_type();
} else if (typeid(*sym) == typeid(IR_roseArraySymbol)) {
	tn = static_cast<const IR_roseArraySymbol *>(sym)->vs_->get_type();
	while (isSgArrayType(tn) || isSgPointerType(tn)) {
		if (isSgArrayType(tn))
			tn = isSgArrayType(tn)->get_base_type();
		else if (isSgPointerType(tn))
			tn = isSgPointerType(tn)->get_base_type();
		else
			throw ir_error(
					"in CreateScalarSymbol: symbol not an array nor a pointer!");
	}
} else
	throw std::bad_typeid();

for (int i = size.size() - 1; i >= 0; i--)
	tn = buildArrayType(tn,
			static_cast<omega::CG_roseRepr *>(size[i])->GetExpression());

//	static int rose_array_counter = 1;
std::string s = std::string("_P") + omega::to_string(rose_array_counter++);
SgVariableDeclaration* defn2 = buildVariableDeclaration(
		const_cast<char *>(s.c_str()), tn);
SgInitializedNamePtrList& variables2 = defn2->get_variables();

SgInitializedNamePtrList::const_iterator i2 = variables2.begin();
SgInitializedName* initializedName2 = *i2;
SgVariableSymbol *vs = new SgVariableSymbol(initializedName2);

prependStatement(defn2, isSgScopeStatement(func->get_definition()->get_body()));

vs->set_parent(symtab_);
symtab_->insert(SgName(s.c_str()), vs);

return new IR_roseArraySymbol(this, vs, s, NULL);
}

// Manu:: added as a part of scalar expansion
IR_ArraySymbol *IR_roseCode::CreateArraySymbol(omega::CG_outputRepr *size,
	const IR_Symbol *sym) {

SgType *tn;
tn = static_cast<const IR_roseArraySymbol *>(sym)->vs_->get_type();
tn = isSgArrayType(tn)->get_base_type();
tn = buildArrayType(tn,
		static_cast<omega::CG_roseRepr *>(size)->GetExpression());

//	static int rose_array_counter = 1;
std::string s = std::string("_P") + omega::to_string(rose_array_counter++);
SgVariableDeclaration* defn2 = buildVariableDeclaration(
		const_cast<char *>(s.c_str()), tn);
SgInitializedNamePtrList& variables2 = defn2->get_variables();

SgInitializedNamePtrList::const_iterator i2 = variables2.begin();
SgInitializedName* initializedName2 = *i2;
SgVariableSymbol *vs = new SgVariableSymbol(initializedName2);

prependStatement(defn2, isSgScopeStatement(func->get_definition()->get_body()));

vs->set_parent(symtab_);
symtab_->insert(SgName(s.c_str()), vs);

std::cout << "temp array:: " << isSgNode(defn2)->unparseToString().c_str()
		<< "\n";

return new IR_roseArraySymbol(this, vs, s, NULL);
}

// Manu:: replaces the RHS with a temporary array reference - part of scalar expansion
bool IR_roseCode::ReplaceRHSExpression(omega::CG_outputRepr *code,
	IR_Ref *ref) {
SgNode *tnl = static_cast<const omega::CG_roseRepr *>(code)->GetCode();
SgStatement *stmt;
SgExpression *rhs =
		static_cast<const omega::CG_roseRepr *>(ref->convert())->GetExpression();

if (tnl != NULL) {
	if (stmt = isSgStatement(tnl)) {
		if (isSgBasicBlock(stmt)) {
			SgStatementPtrList& stmts = isSgBasicBlock(stmt)->get_statements();
			if (stmts.size() > 1) {
				std::cout
						<< "Scalar expansion not supported for multiple statements\n";
				return false;
			}
			omega::CG_roseRepr *r = new omega::CG_roseRepr(isSgNode(stmts[0]));
		} else if (isSgExprStatement(stmt)) {
			omega::CG_roseRepr *r = new omega::CG_roseRepr(
					isSgExpression(isSgExprStatement(stmt)->get_expression()));

			SgExpression* op = r->GetExpression();
			if (isSgCompoundAssignOp(op)) {
				SgCompoundAssignOp *op1 = isSgCompoundAssignOp(op);
				op1->set_rhs_operand(rhs);
			}
		} else if (isSgIfStmt(stmt)) {
			SgStatement *body = isSgIfStmt(stmt)->get_true_body();
			if (body) {

				if (isSgExprStatement(body)) {
					omega::CG_roseRepr *r = new omega::CG_roseRepr(
							isSgExpression(
									isSgExprStatement(body)->get_expression()));

					SgExpression* op = r->GetExpression();
					if (isSgCompoundAssignOp(op)) {
						SgCompoundAssignOp *op1 = isSgCompoundAssignOp(op);

						op1->set_rhs_operand(rhs);
					}
					if (isSgAssignOp(op)) {
						SgAssignOp *op1 = isSgAssignOp(op);

						op1->set_rhs_operand(rhs);
					} else {
						std::cout
								<< "Scalar expansion not supported for multiple statements\n";
						//return NULL;
						return false;
					}

				}

			}
			std::cout
					<< "Scalar expansion not supported for multiple statements\n";
			return false;
		} else {
			std::cout
					<< "Scalar expansion not supported for multiple statements\n";
			return false;
		}
	} else {
		std::cout << "Scalar expansion not supported for multiple statements\n";
		return false;
	}
	return true;
} else
	return false;
}

// Manu:: replaces the RHS with a temporary array reference - part of scalar expansion
omega::CG_outputRepr * IR_roseCode::GetRHSExpression(
	omega::CG_outputRepr *code) {
SgNode *tnl = static_cast<const omega::CG_roseRepr *>(code)->GetCode();
SgStatement *stmt;

if (tnl != NULL) {
	if (stmt = isSgStatement(tnl)) {
		if (isSgBasicBlock(stmt)) {
			SgStatementPtrList& stmts = isSgBasicBlock(stmt)->get_statements();
			if (stmts.size() > 1) {
				std::cout
						<< "Scalar expansion not supported for multiple statements\n";
				return NULL;
			}
			omega::CG_roseRepr *r = new omega::CG_roseRepr(isSgNode(stmts[0]));
		} else if (isSgExprStatement(stmt)) {
			omega::CG_roseRepr *r = new omega::CG_roseRepr(
					isSgExpression(isSgExprStatement(stmt)->get_expression()));

			SgExpression* op = r->GetExpression();
			if (isSgCompoundAssignOp(op)) {
				SgCompoundAssignOp *op1 = isSgCompoundAssignOp(op);

				CG_outputRepr *to_return = new omega::CG_roseRepr(
						isSgExpression(op1->get_rhs_operand()));
				return to_return->clone();
			}
			if (isSgAssignOp(op)) {
				SgAssignOp *op1 = isSgAssignOp(op);

				CG_outputRepr *to_return = new omega::CG_roseRepr(
						isSgExpression(op1->get_rhs_operand()));
				return to_return->clone();
			}

		} else if (isSgIfStmt(stmt)) {
			SgStatement *body = isSgIfStmt(stmt)->get_true_body();
			if (body) {

				if (isSgExprStatement(body)) {
					omega::CG_roseRepr *r = new omega::CG_roseRepr(
							isSgExpression(
									isSgExprStatement(body)->get_expression()));

					SgExpression* op = r->GetExpression();
					if (isSgCompoundAssignOp(op)) {
						SgCompoundAssignOp *op1 = isSgCompoundAssignOp(op);

						CG_outputRepr *to_return = new omega::CG_roseRepr(
								isSgExpression(op1->get_rhs_operand()));
						return to_return->clone();
					}
					if (isSgAssignOp(op)) {
						SgAssignOp *op1 = isSgAssignOp(op);

						CG_outputRepr *to_return = new omega::CG_roseRepr(
								isSgExpression(op1->get_rhs_operand()));
						return to_return->clone();
					} else {
						std::cout
								<< "Scalar expansion not supported for multiple statements\n";
						return NULL;

					}

				}

			}
			std::cout
					<< "Scalar expansion not supported for multiple statements\n";
			return NULL;
		} else {
			std::cout
					<< "Scalar expansion not supported for multiple statements\n";
			return NULL;
		}
	} else {
		std::cout << "Scalar expansion not supported for multiple statements\n";
		return NULL;
	}
} else
	return NULL;
}

omega::CG_outputRepr * IR_roseCode::GetLHSExpression(
	omega::CG_outputRepr *code) {
SgNode *tnl = static_cast<const omega::CG_roseRepr *>(code)->GetCode();
SgStatementPtrList *list_ =
		static_cast<const omega::CG_roseRepr *>(code)->GetList();
SgStatement *stmt;

if (tnl == NULL && list_ != NULL) {

	assert(list_->size() == 1);
	SgStatementPtrList::iterator it = list_->begin();

	tnl = isSgNode(*it);

}

if (tnl != NULL) {
	if (stmt = isSgStatement(tnl)) {
		if (isSgBasicBlock(stmt)) {
			SgStatementPtrList& stmts = isSgBasicBlock(stmt)->get_statements();
			if (stmts.size() > 1) {
				std::cout
						<< "Scalar expansion not supported for multiple statements\n";
				return NULL;
			}
			omega::CG_roseRepr *r = new omega::CG_roseRepr(isSgNode(stmts[0]));
		} else if (isSgExprStatement(stmt)) {
			omega::CG_roseRepr *r = new omega::CG_roseRepr(
					isSgExpression(isSgExprStatement(stmt)->get_expression()));

			SgExpression* op = r->GetExpression();
			if (isSgCompoundAssignOp(op)) {
				SgCompoundAssignOp *op1 = isSgCompoundAssignOp(op);

				CG_outputRepr *to_return = new omega::CG_roseRepr(
						isSgExpression(op1->get_lhs_operand()));
				return to_return->clone();
			}
			if (isSgAssignOp(op)) {
				SgAssignOp *op1 = isSgAssignOp(op);

				CG_outputRepr *to_return = new omega::CG_roseRepr(
						isSgExpression(op1->get_lhs_operand()));
				return to_return->clone();
			}
		} else {
			std::cout
					<< "Scalar expansion not supported for multiple statements\n";
			return NULL;
		}
	} else if (isSgIfStmt(stmt)) {
		SgStatement *body = isSgIfStmt(stmt)->get_true_body();
		if (body) {

			if (isSgExprStatement(body)) {
				omega::CG_roseRepr *r = new omega::CG_roseRepr(
						isSgExpression(
								isSgExprStatement(body)->get_expression()));

				SgExpression* op = r->GetExpression();
				if (isSgCompoundAssignOp(op)) {
					SgCompoundAssignOp *op1 = isSgCompoundAssignOp(op);

					CG_outputRepr *to_return = new omega::CG_roseRepr(
							isSgExpression(op1->get_lhs_operand()));
					return to_return->clone();
				}
				if (isSgAssignOp(op)) {
					SgAssignOp *op1 = isSgAssignOp(op);

					CG_outputRepr *to_return = new omega::CG_roseRepr(
							isSgExpression(op1->get_lhs_operand()));
					return to_return->clone();
				} else {
					std::cout
							<< "Scalar expansion not supported for multiple statements\n";
					return NULL;

				}

			}

		}
		std::cout << "Scalar expansion not supported for multiple statements\n";
		return NULL;
	} else {
		std::cout << "Scalar expansion not supported for multiple statements\n";
		return NULL;
	}
}
return NULL;
}

// Manu:: replaces the RHS with a temporary array reference - part of scalar expansion
bool IR_roseCode::ReplaceLHSExpression(omega::CG_outputRepr *code,
	IR_ArrayRef *ref) {
SgNode *tnl = static_cast<const omega::CG_roseRepr *>(code)->GetCode();
SgStatement *stmt;
SgExpression *lhs =
		static_cast<const omega::CG_roseRepr *>(ref->convert())->GetExpression();
SgAssignOp *newStmt;

std::cout << "Expression is --> " << isSgNode(lhs)->unparseToString() << "\n";

if (tnl != NULL) {
	if (stmt = isSgStatement(tnl)) {
		if (isSgBasicBlock(stmt)) {
			SgStatementPtrList& stmts = isSgBasicBlock(stmt)->get_statements();
			if (stmts.size() > 1) {
				std::cout
						<< "Scalar expansion not supported for multiple statements\n";
				return false;
			}
			omega::CG_roseRepr *r = new omega::CG_roseRepr(isSgNode(stmts[0]));
		} else if (isSgExprStatement(stmt)) {
			omega::CG_roseRepr *r = new omega::CG_roseRepr(
					isSgExpression(isSgExprStatement(stmt)->get_expression()));

			SgExpression* op = r->GetExpression();
			if (isSgCompoundAssignOp(op)) {
				SgCompoundAssignOp *op1 = isSgCompoundAssignOp(op);
				op1->set_lhs_operand(lhs);
				newStmt = new SgAssignOp(op1->get_lhs_operand(),
						op1->get_rhs_operand(), op1->get_type());
//std::cout << "Expression is --> " << isSgNode(newStmt)->unparseToString() << "\n";
				code = (new omega::CG_roseRepr(isSgNode(newStmt)))->clone();
			}
		} else {
			std::cout
					<< "Scalar expansion not supported for multiple statements\n";
			return false;
		}
	} else {
		std::cout << "Scalar expansion not supported for multiple statements\n";
		return false;
	}
	return true;
} else
	return false;
}

IR_ScalarRef *IR_roseCode::CreateScalarRef(const IR_ScalarSymbol *sym) {
return new IR_roseScalarRef(this,
		buildVarRefExp(static_cast<const IR_roseScalarSymbol *>(sym)->vs_));

}

IR_ArrayRef *IR_roseCode::CreateArrayRef(const IR_ArraySymbol *sym,
	std::vector<omega::CG_outputRepr *> &index) {

int t;

if (sym->n_dim() != index.size())
	throw std::invalid_argument("incorrect array symbol dimensionality");

const IR_roseArraySymbol *l_sym = static_cast<const IR_roseArraySymbol *>(sym);

SgVariableSymbol *vs = l_sym->vs_;
SgExpression* ia1 = buildVarRefExp(vs);

//std::string x = isSgNode(ia1)->unparseToString();

for (int i = 0; i < index.size(); i++) {

	/* if (is_fortran_)
	 t = index.size() - i - 1;
	 else
	 t = i;
	 */
	//	std::string y =
	//			isSgNode(
	//					static_cast<omega::CG_roseRepr *>(index[i])->GetExpression())->unparseToString();
	ia1 = buildPntrArrRefExp(ia1,
			static_cast<omega::CG_roseRepr *>(index[i])->GetExpression());

}

SgPntrArrRefExp *ia = isSgPntrArrRefExp(ia1);
//std::string z = isSgNode(ia)->unparseToString();

return new IR_roseArrayRef(this, ia, -1);

}

IR_PointerArrayRef *IR_roseCode::CreatePointerArrayRef(IR_PointerSymbol *sym,
	std::vector<omega::CG_outputRepr *> &index) {

int t;

if (sym->n_dim() != index.size())
	throw std::invalid_argument("incorrect array symbol dimensionality");

const IR_rosePointerSymbol *l_sym =
		static_cast<const IR_rosePointerSymbol *>(sym);

SgVariableSymbol *vs = l_sym->vs_;
SgExpression* ia1 = buildVarRefExp(vs);

//std::string x = isSgNode(ia1)->unparseToString();

for (int i = 0; i < index.size(); i++) {

	/* if (is_fortran_)
	 t = index.size() - i - 1;
	 else
	 t = i;
	 */
	//	std::string y =
	//			isSgNode(
	//					static_cast<omega::CG_roseRepr *>(index[i])->GetExpression())->unparseToString();
	ia1 = buildPntrArrRefExp(ia1,
			static_cast<omega::CG_roseRepr *>(index[i])->GetExpression());

}

SgPntrArrRefExp *ia = isSgPntrArrRefExp(ia1);
//std::string z = isSgNode(ia)->unparseToString();

return new IR_rosePointerArrayRef(this, ia, -1, sym);

}

std::vector<IR_Loop *> IR_roseCode::FindLoops(omega::CG_outputRepr *repr) {
std::vector<IR_Loop*> a;
SgNode *tnl = static_cast<CG_roseRepr *>(repr)->GetCode();
SgStatementPtrList *list = static_cast<CG_roseRepr *>(repr)->GetList();
if (!tnl && list) {
	int paths = 0;
	for (SgStatementPtrList::iterator it = (*list).begin(); it != (*list).end();
			it++) {

		std::vector<IR_Loop *> b;

		b = FindLoops(new CG_roseRepr(*it));
		if (b.size() > 0) {
			paths++;
			if (paths > 1)
				return std::vector<IR_Loop*>();
			std::copy(b.begin(), b.end(), back_inserter(a));
		}

	}
	return a;

}

if (isSgForStatement(tnl)) {
	std::vector<IR_Loop *> b;
	a.push_back(new IR_roseLoop(this, isSgForStatement(tnl), main_ssa));
	b = FindLoops(new CG_roseRepr(isSgForStatement(tnl)->get_loop_body()));

	std::copy(b.begin(), b.end(), back_inserter(a));
	//break;
} else if (isSgIfStmt(tnl)) {
	std::vector<IR_Loop *> b;
	int paths = 0;
	b = FindLoops(new CG_roseRepr(isSgIfStmt(tnl)->get_true_body()));
	if (b.size() > 0) {
		paths++;
		std::copy(b.begin(), b.end(), back_inserter(a));
	}
	if (isSgIfStmt(tnl)->get_false_body()) {
		b = FindLoops(new CG_roseRepr(isSgIfStmt(tnl)->get_false_body()));
		if (b.size() > 0)
			paths++;
		if (paths < 2)
			std::copy(b.begin(), b.end(), back_inserter(a));
		else
			return std::vector<IR_Loop*>();

	}
} else if (isSgBasicBlock(tnl)) {
	std::vector<IR_Loop*> b;
	SgStatementPtrList stmts = isSgBasicBlock(tnl)->get_statements();
	int paths = 0;
	for (SgStatementPtrList::iterator it = stmts.begin(); it != stmts.end();
			it++) {
		b = FindLoops(new CG_roseRepr(*it));
		if (b.size() > 0)
			paths++;
		if (paths < 2) {
			if (b.size() > 0)
				std::copy(b.begin(), b.end(), back_inserter(a));
		} else
			return std::vector<IR_Loop*>();

	}

} else if (isSgWhileStmt(tnl)) {
	std::vector<IR_Loop *> b;
	a.push_back(new IR_roseLoop(this, isSgWhileStmt(tnl), main_ssa));
	b = FindLoops(new CG_roseRepr(isSgWhileStmt(tnl)->get_body()));
	std::copy(b.begin(), b.end(), back_inserter(a));

} else if (isSgStatement(tnl)) {

} else
	throw loop_error(
			"Unrecognized Statement in compute_loop_indices_recursive!\n");

return a;

}

void IR_roseCode::CreateDefineMacro(std::string s, std::string args,
	omega::CG_outputRepr *repr) {
SgNode *tnl = static_cast<const omega::CG_roseRepr *>(repr)->GetExpression();
//	buildCpreprocessorDefineDeclaration(root,
//			"#define " + s +  args + " " + tnl->unparseToString(),
//			PreprocessingInfo::before);

std::pair<std::string, SgNode *> second = std::pair<std::string, SgNode *>(args,
		tnl);
defined_macros.insert(
		std::pair<std::string, std::pair<std::string, SgNode *> >(s, second));

SgName name_macro(s);
SgFunctionDeclaration * decl_macro = buildNondefiningFunctionDeclaration(
		name_macro, buildVoidType(), buildFunctionParameterList(), root);

}

CG_outputRepr *IR_roseCode::RetrieveMacro(std::string s) {

//	buildCpreprocessorDefineDeclaration(root,
//			"#define " + s +  args + " " + tnl->unparseToString(),
//			PreprocessingInfo::before);
std::map<std::string, std::pair<std::string, SgNode*> >::iterator it =
		defined_macros.find(s);
return new CG_roseRepr(isSgExpression(it->second.second));

}

/*void IR_roseCode::CreateDefineMacro(std::string s, std::string args,
 std::string repr) {
 //	SgNode *tnl =
 //			static_cast<const omega::CG_roseRepr *>(repr)->GetExpression();
 ///	buildCpreprocessorDefineDeclaration(root,
 //			"#define " + s +  args + " " + tnl->unparseToString(),
 //			PreprocessingInfo::before);

 defined_macros.insert(
 std::pair<std::string, SgNode *>(s + args + " " + repr, NULL));

 }*/

std::vector<IR_ScalarRef *> IR_roseCode::FindScalarRef(
	const omega::CG_outputRepr *repr) const {
std::vector<IR_ScalarRef *> scalars;
SgNode *tnl = static_cast<const omega::CG_roseRepr *>(repr)->GetCode();
SgStatementPtrList *list =
		static_cast<const omega::CG_roseRepr *>(repr)->GetList();
SgStatement* stmt;
SgExpression * exp;

if (list != NULL) {
	for (SgStatementPtrList::iterator it = (*list).begin(); it != (*list).end();
			it++) {
		omega::CG_roseRepr *r = new omega::CG_roseRepr(isSgNode(*it));
		std::vector<IR_ScalarRef *> a = FindScalarRef(r);
		delete r;
		std::copy(a.begin(), a.end(), back_inserter(scalars));
	}
}

else if (tnl != NULL) {
	if (stmt = isSgStatement(tnl)) {
		if (isSgBasicBlock(stmt)) {
			SgStatementPtrList& stmts = isSgBasicBlock(stmt)->get_statements();
			for (int i = 0; i < stmts.size(); i++) {
				omega::CG_roseRepr *r = new omega::CG_roseRepr(
						isSgNode(stmts[i]));
				std::vector<IR_ScalarRef *> a = FindScalarRef(r);
				delete r;
				std::copy(a.begin(), a.end(), back_inserter(scalars));
			}

		} else if (isSgForStatement(stmt)) {

			SgForStatement *tnf = isSgForStatement(stmt);
			omega::CG_roseRepr *s = new omega::CG_roseRepr(
					isSgStatement(tnf->get_test()));
			std::vector<IR_ScalarRef *> b = FindScalarRef(s);
			delete s;
			std::copy(b.begin(), b.end(), back_inserter(scalars));
			omega::CG_roseRepr *r = new omega::CG_roseRepr(
					isSgStatement(tnf->get_loop_body()));
			std::vector<IR_ScalarRef *> a = FindScalarRef(r);
			delete r;
			std::copy(a.begin(), a.end(), back_inserter(scalars));
		} else if (isSgFortranDo(stmt)) {
			SgFortranDo *tfortran = isSgFortranDo(stmt);
			omega::CG_roseRepr *r = new omega::CG_roseRepr(
					isSgStatement(tfortran->get_body()));
			std::vector<IR_ScalarRef *> a = FindScalarRef(r);
			delete r;
			std::copy(a.begin(), a.end(), back_inserter(scalars));
		} else if (isSgIfStmt(stmt)) {
			SgIfStmt* tni = isSgIfStmt(stmt);
			omega::CG_roseRepr *r = new omega::CG_roseRepr(
					isSgNode(tni->get_conditional()));
			std::vector<IR_ScalarRef *> a = FindScalarRef(r);
			delete r;
			std::copy(a.begin(), a.end(), back_inserter(scalars));
			r = new omega::CG_roseRepr(isSgNode(tni->get_true_body()));
			a = FindScalarRef(r);
			delete r;
			std::copy(a.begin(), a.end(), back_inserter(scalars));
			r = new omega::CG_roseRepr(isSgNode(tni->get_false_body()));
			a = FindScalarRef(r);
			delete r;
			std::copy(a.begin(), a.end(), back_inserter(scalars));
		} else if (isSgExprStatement(stmt)) {
			omega::CG_roseRepr *r = new omega::CG_roseRepr(
					isSgExpression(isSgExprStatement(stmt)->get_expression()));
			std::vector<IR_ScalarRef *> a = FindScalarRef(r);
			delete r;
			std::copy(a.begin(), a.end(), back_inserter(scalars));

		}
	}
} else {
	SgExpression* op =
			static_cast<const omega::CG_roseRepr *>(repr)->GetExpression();

	if (isSgPntrArrRefExp(op)) {
		omega::CG_roseRepr *r1 = new omega::CG_roseRepr(
				isSgPntrArrRefExp(op)->get_lhs_operand());
		std::vector<IR_ScalarRef *> a1 = FindScalarRef(r1);
		delete r1;
		std::copy(a1.begin(), a1.end(), back_inserter(scalars));
		omega::CG_roseRepr *r2 = new omega::CG_roseRepr(
				isSgPntrArrRefExp(op)->get_rhs_operand());
		std::vector<IR_ScalarRef *> a2 = FindScalarRef(r2);
		delete r2;
		std::copy(a2.begin(), a2.end(), back_inserter(scalars));

	} else if (isSgVarRefExp(op)
			&& !isSgPointerType(isSgVarRefExp(op)->get_type())
			&& !isSgClassType(isSgVarRefExp(op)->get_type())
			&& (!isSgArrayType(isSgVarRefExp(op)->get_type()))) {
		/*	if ((isSgAssignOp(isSgNode(op)->get_parent()))
		 && ((isSgAssignOp(isSgNode(op)->get_parent())->get_lhs_operand())
		 == op))
		 scalars.push_back(
		 new IR_roseScalarRef(this,
		 isSgAssignOp(isSgNode(op)->get_parent()), -1));
		 else
		 */
		if (SgCompoundAssignOp* op_ = isSgCompoundAssignOp(
				isSgVarRefExp(op)->get_parent())) {
			//if (SgCompoundAssignOp * op_ = isSgCompoundAssignOp(op_)) {
			if (op_->get_lhs_operand() == isSgVarRefExp(op)) {
				scalars.push_back(
						new IR_roseScalarRef(this, isSgVarRefExp(op), 1));
				scalars.push_back(
						new IR_roseScalarRef(this, isSgVarRefExp(op), 0));
			}
			//}

		} else if (SgAssignOp* assmt = isSgAssignOp(
				isSgVarRefExp(op)->get_parent())) {

			if (assmt->get_lhs_operand() == isSgVarRefExp(op))
				scalars.push_back(
						new IR_roseScalarRef(this, isSgVarRefExp(op), 1));
		} else if (SgAssignOp * assmt = isSgAssignOp(
				isSgVarRefExp(op)->get_parent())) {

			if (assmt->get_rhs_operand() == isSgVarRefExp(op))
				scalars.push_back(
						new IR_roseScalarRef(this, isSgVarRefExp(op), 0));
			//Anand; Adding support for scalar variables within structures:
			// eg. struct a{int i}; a.i
		} else if (SgDotExp *dotexp = isSgDotExp(
				isSgVarRefExp(op)->get_parent())) {

			assert(dotexp->get_rhs_operand() == op);

			if (SgAssignOp *assn = isSgAssignOp(dotexp->get_parent())) {
				if (assn->get_lhs_operand() == dotexp)
					scalars.push_back(
							new IR_roseScalarRef(this, isSgVarRefExp(op), 1,
									dotexp));
				else
					scalars.push_back(
							new IR_roseScalarRef(this, isSgVarRefExp(op), 0,
									dotexp));
			} else
				scalars.push_back(
						new IR_roseScalarRef(this, isSgVarRefExp(op), 0,
								dotexp));

		} else
			scalars.push_back(new IR_roseScalarRef(this, isSgVarRefExp(op), 0));
	} else if (isSgAssignOp(op)) {
		omega::CG_roseRepr *r1 = new omega::CG_roseRepr(
				isSgAssignOp(op)->get_lhs_operand());
		std::vector<IR_ScalarRef *> a1 = FindScalarRef(r1);
		delete r1;
		std::copy(a1.begin(), a1.end(), back_inserter(scalars));
		omega::CG_roseRepr *r2 = new omega::CG_roseRepr(
				isSgAssignOp(op)->get_rhs_operand());
		std::vector<IR_ScalarRef *> a2 = FindScalarRef(r2);
		delete r2;
		std::copy(a2.begin(), a2.end(), back_inserter(scalars));

	} else if (isSgBinaryOp(op)) {
		omega::CG_roseRepr *r1 = new omega::CG_roseRepr(
				isSgBinaryOp(op)->get_lhs_operand());
		std::vector<IR_ScalarRef *> a1 = FindScalarRef(r1);
		delete r1;
		std::copy(a1.begin(), a1.end(), back_inserter(scalars));
		omega::CG_roseRepr *r2 = new omega::CG_roseRepr(
				isSgBinaryOp(op)->get_rhs_operand());
		std::vector<IR_ScalarRef *> a2 = FindScalarRef(r2);
		delete r2;
		std::copy(a2.begin(), a2.end(), back_inserter(scalars));
	} else if (isSgUnaryOp(op)) {
		omega::CG_roseRepr *r1 = new omega::CG_roseRepr(
				isSgUnaryOp(op)->get_operand());
		std::vector<IR_ScalarRef *> a1 = FindScalarRef(r1);
		delete r1;
		std::copy(a1.begin(), a1.end(), back_inserter(scalars));
	} else if (SgCallExpression *call = isSgCallExpression(op)) {

		SgExprListExp* arg_list = call->get_args();

		SgExpressionPtrList & expressionPtrList = arg_list->get_expressions();

		for (SgExpressionPtrList::iterator i = expressionPtrList.begin();
				i != expressionPtrList.end(); i++) {
			omega::CG_roseRepr *r1 = new omega::CG_roseRepr(*i);
			std::vector<IR_ScalarRef *> a1 = FindScalarRef(r1);
			delete r1;
			std::copy(a1.begin(), a1.end(), back_inserter(scalars));
		}
	}

}
return scalars;

}

omega::CG_outputRepr *IR_roseCode::CreateArrayType(IR_CONSTANT_TYPE type,
	omega::CG_outputRepr *size) {

SgArrayType *arrType;

SgExpression *size_ = static_cast<CG_roseRepr *>(size)->GetExpression();
assert(size_);
if (type == IR_CONSTANT_INT)
	arrType = buildArrayType(buildIntType(), size_);
else if (type == IR_CONSTANT_FLOAT)
	arrType = buildArrayType(buildFloatType(), size_);
else if (type == IR_CONSTANT_SHORT)
	arrType = buildArrayType(buildUnsignedShortType(), size_);
else
	throw ir_error("unrecognized base type for array");

return new CG_roseRepr(isSgNode(arrType));

}

omega::CG_outputRepr *IR_roseCode::CreateArrayType(IR_CONSTANT_TYPE type,
	std::vector<omega::CG_outputRepr *> size) {

SgType *arrType;
if (type == IR_CONSTANT_INT)
	arrType = buildIntType();
else if (type == IR_CONSTANT_FLOAT)
	arrType = buildFloatType();
else if (type == IR_CONSTANT_SHORT)
	arrType = buildUnsignedShortType();
else
	throw ir_error("unrecognized base type for array");

for (int i = size.size() - 1; i >= 0; i--)
	arrType = buildArrayType(arrType,
			static_cast<omega::CG_roseRepr *>(size[i])->GetExpression());

return new CG_roseRepr(isSgNode(arrType));

}

omega::CG_outputRepr *IR_roseCode::CreatePointerType(IR_CONSTANT_TYPE type) {
SgPointerType *ptrType;
if (type == IR_CONSTANT_INT)
	ptrType = buildPointerType(buildIntType());
else if (type == IR_CONSTANT_FLOAT)
	ptrType = buildPointerType(buildFloatType());
else if (type == IR_CONSTANT_SHORT)
	ptrType = buildPointerType(buildUnsignedShortType());
else
	throw ir_error("unrecognized base type for pointer");

return new CG_roseRepr(isSgNode(ptrType));

}

omega::CG_outputRepr *IR_roseCode::CreatePointerType(CG_outputRepr *type) {
SgPointerType *ptrType = buildPointerType(
		isSgType(static_cast<CG_roseRepr *>(type)->GetCode()));

return new CG_roseRepr(isSgNode(ptrType));

}
omega::CG_outputRepr *IR_roseCode::CreateScalarType(IR_CONSTANT_TYPE type) {

if (type == IR_CONSTANT_INT)
	return new CG_roseRepr(buildIntType());
else if (type == IR_CONSTANT_FLOAT)
	return new CG_roseRepr(buildFloatType());
else if (type == IR_CONSTANT_SHORT)
	return new CG_roseRepr(buildUnsignedShortType());
else
	throw ir_error("unrecognized base type for pointer");

return NULL;

}
std::vector<IR_ArrayRef *> IR_roseCode::FindArrayRef(
	const omega::CG_outputRepr *repr, bool ignore_while) const {
std::vector<IR_ArrayRef *> arrays;
SgNode *tnl = static_cast<const omega::CG_roseRepr *>(repr)->GetCode();
SgStatementPtrList* list =
		static_cast<const omega::CG_roseRepr *>(repr)->GetList();
SgStatement* stmt;
SgExpression * exp;

if (list != NULL) {
	for (SgStatementPtrList::iterator it = (*list).begin(); it != (*list).end();
			it++) {
		omega::CG_roseRepr *r = new omega::CG_roseRepr(isSgNode(*it));
		std::vector<IR_ArrayRef *> a = FindArrayRef(r, ignore_while);
		delete r;
		std::copy(a.begin(), a.end(), back_inserter(arrays));
	}
} else if (tnl != NULL) {
	if (stmt = isSgStatement(tnl)) {
		if (isSgBasicBlock(stmt)) {
			SgStatementPtrList& stmts = isSgBasicBlock(stmt)->get_statements();
			for (int i = 0; i < stmts.size(); i++) {
				omega::CG_roseRepr *r = new omega::CG_roseRepr(
						isSgNode(stmts[i]));
				std::vector<IR_ArrayRef *> a = FindArrayRef(r, ignore_while);
				delete r;
				std::copy(a.begin(), a.end(), back_inserter(arrays));
			}

		} else if (isSgForStatement(stmt)) {

			SgForStatement *tnf = isSgForStatement(stmt);
			omega::CG_roseRepr *r = new omega::CG_roseRepr(
					isSgStatement(tnf->get_loop_body()));
			std::vector<IR_ArrayRef *> a = FindArrayRef(r, ignore_while);
			delete r;
			std::copy(a.begin(), a.end(), back_inserter(arrays));
		} else if (isSgFortranDo(stmt)) {
			SgFortranDo *tfortran = isSgFortranDo(stmt);
			omega::CG_roseRepr *r = new omega::CG_roseRepr(
					isSgStatement(tfortran->get_body()));
			std::vector<IR_ArrayRef *> a = FindArrayRef(r, ignore_while);
			delete r;
			std::copy(a.begin(), a.end(), back_inserter(arrays));
		} else if (isSgIfStmt(stmt)) {
			SgIfStmt* tni = isSgIfStmt(stmt);
			omega::CG_roseRepr *r = new omega::CG_roseRepr(
					isSgNode(tni->get_conditional()));
			std::vector<IR_ArrayRef *> a = FindArrayRef(r, ignore_while);
			delete r;
			std::copy(a.begin(), a.end(), back_inserter(arrays));
			r = new omega::CG_roseRepr(isSgNode(tni->get_true_body()));
			a = FindArrayRef(r, ignore_while);
			delete r;
			std::copy(a.begin(), a.end(), back_inserter(arrays));
			r = new omega::CG_roseRepr(isSgNode(tni->get_false_body()));
			a = FindArrayRef(r, ignore_while);
			delete r;
			std::copy(a.begin(), a.end(), back_inserter(arrays));
		} else if (isSgWhileStmt(stmt) && !ignore_while) {
			SgWhileStmt* tni = isSgWhileStmt(stmt);
			omega::CG_roseRepr *r = new omega::CG_roseRepr(
					isSgNode(tni->get_condition()));
			std::vector<IR_ArrayRef *> a = FindArrayRef(r, ignore_while);
			delete r;
			std::copy(a.begin(), a.end(), back_inserter(arrays));
			r = new omega::CG_roseRepr(isSgNode(tni->get_body()));
			a = FindArrayRef(r, ignore_while);
			delete r;
			std::copy(a.begin(), a.end(), back_inserter(arrays));

		} else if (isSgExprStatement(stmt)) {
			omega::CG_roseRepr *r = new omega::CG_roseRepr(
					isSgExpression(isSgExprStatement(stmt)->get_expression()));
			std::vector<IR_ArrayRef *> a = FindArrayRef(r, ignore_while);
			delete r;
			std::copy(a.begin(), a.end(), back_inserter(arrays));

		}
	}
} else {
	SgExpression* op =
			static_cast<const omega::CG_roseRepr *>(repr)->GetExpression();
	if (isSgPntrArrRefExp(op)) {

		SgVarRefExp* base;
		SgExpression* op2;
		if (isSgCompoundAssignOp(isSgPntrArrRefExp(op)->get_parent())
				&& isSgCompoundAssignOp(isSgPntrArrRefExp(op)->get_parent())->get_lhs_operand()
						== isSgPntrArrRefExp(op)) {
			IR_roseArrayRef *ref1 = new IR_roseArrayRef(this,
					isSgPntrArrRefExp(op), 1);
			arrays.push_back(ref1);
			CG_outputRepr *clone_ia =
					(new CG_roseRepr(isSgPntrArrRefExp(op)))->clone();

			IR_roseArrayRef *ref2 = new IR_roseArrayRef(this,
					isSgPntrArrRefExp(op),
					//isSgPntrArrRefExp(
					//		static_cast<CG_roseRepr *>(clone_ia)->GetExpression()),
					0);
			arrays.push_back(ref2);
		} else if (isSgCompoundAssignOp(isSgPntrArrRefExp(op)->get_parent())
				&& isSgCompoundAssignOp(isSgPntrArrRefExp(op)->get_parent())->get_rhs_operand()
						== isSgPntrArrRefExp(op)) {
			IR_roseArrayRef *ref2 = new IR_roseArrayRef(this,
					isSgPntrArrRefExp(op), 0);
			arrays.push_back(ref2);
		}

		else {
			IR_roseArrayRef *ref3 = new IR_roseArrayRef(this,
					isSgPntrArrRefExp(op), -1);
			arrays.push_back(ref3);
		}
		while (isSgPntrArrRefExp(op)) {
			op2 = isSgPntrArrRefExp(op)->get_rhs_operand();
			op = isSgPntrArrRefExp(op)->get_lhs_operand();
			omega::CG_roseRepr *r = new omega::CG_roseRepr(op2);
			std::vector<IR_ArrayRef *> a = FindArrayRef(r, ignore_while);
			delete r;
			std::copy(a.begin(), a.end(), back_inserter(arrays));

		}

		/* base = isSgVarRefExp(op);
		 SgVariableSymbol *arrSymbol = (SgVariableSymbol*)(base->get_symbol());
		 SgArrayType *arrType = isSgArrayType(arrSymbol->get_type());

		 SgExprListExp* dimList = arrType->get_dim_info();

		 if(dimList != NULL){
		 SgExpressionPtrList::iterator it = dimList->get_expressions().begin();
		 SgExpression *expr;


		 for (int i = 0; it != dimList->get_expressions().end(); it++, i++)
		 {
		 expr = *it;

		 omega::CG_roseRepr *r = new omega::CG_roseRepr(expr);
		 std::vector<IR_ArrayRef *> a = FindArrayRef(r);
		 delete r;
		 std::copy(a.begin(), a.end(), back_inserter(arrays));
		 }

		 }
		 arrays.push_back(ref);
		 */
	} else if (isSgAssignOp(op)) {
		omega::CG_roseRepr *r1 = new omega::CG_roseRepr(
				isSgAssignOp(op)->get_lhs_operand());
		std::vector<IR_ArrayRef *> a1 = FindArrayRef(r1, ignore_while);
		delete r1;
		std::copy(a1.begin(), a1.end(), back_inserter(arrays));
		omega::CG_roseRepr *r2 = new omega::CG_roseRepr(
				isSgAssignOp(op)->get_rhs_operand());
		std::vector<IR_ArrayRef *> a2 = FindArrayRef(r2, ignore_while);
		delete r2;
		std::copy(a2.begin(), a2.end(), back_inserter(arrays));

	} else if (isSgBinaryOp(op)) {
		omega::CG_roseRepr *r1 = new omega::CG_roseRepr(
				isSgBinaryOp(op)->get_lhs_operand());
		std::vector<IR_ArrayRef *> a1 = FindArrayRef(r1, ignore_while);
		delete r1;
		std::copy(a1.begin(), a1.end(), back_inserter(arrays));
		omega::CG_roseRepr *r2 = new omega::CG_roseRepr(
				isSgBinaryOp(op)->get_rhs_operand());
		std::vector<IR_ArrayRef *> a2 = FindArrayRef(r2, ignore_while);
		delete r2;
		std::copy(a2.begin(), a2.end(), back_inserter(arrays));
	} else if (isSgUnaryOp(op)) {
		omega::CG_roseRepr *r1 = new omega::CG_roseRepr(
				isSgUnaryOp(op)->get_operand());
		std::vector<IR_ArrayRef *> a1 = FindArrayRef(r1, ignore_while);
		delete r1;
		std::copy(a1.begin(), a1.end(), back_inserter(arrays));
	}

	else if (SgCallExpression *call = isSgCallExpression(op)) {

		SgExprListExp* arg_list = call->get_args();

		SgExpressionPtrList & expressionPtrList = arg_list->get_expressions();

		for (SgExpressionPtrList::iterator i = expressionPtrList.begin();
				i != expressionPtrList.end(); i++) {
			omega::CG_roseRepr *r1 = new omega::CG_roseRepr(*i);
			std::vector<IR_ArrayRef *> a1 = FindArrayRef(r1, ignore_while);
			delete r1;
			std::copy(a1.begin(), a1.end(), back_inserter(arrays));
		}
	}
}
return arrays;

/* std::string x;
 SgStatement* stmt = isSgStatement(tnl);
 SGExprStatement* expr_statement = isSgExprStatement(stmt);
 SgExpression* exp= NULL;
 if(expr_statement == NULL){
 if(! (SgExpression* exp = isSgExpression(tnl))
 throw ir_error("FindArrayRef: Not a stmt nor an expression!!");

 if( expr_statement != NULL){
 for(int i=0; i < tnl->get_numberOfTraversalSuccessors(); i++){

 SgNode* tn = isSgStatement(tnl);
 SgStatement* stmt = isSgStatement(tn);
 if(stmt != NULL){
 SgExprStatement* expr_statement = isSgExprStatement(tn);
 if(expr_statement != NULL)
 x = isSgNode(expr_statement)->unparseToString();
 exp = expr_statement->get_expression();

 }
 else{

 exp = isSgExpression(tn);
 }
 if(exp != NULL){
 x = isSgNode(exp)->unparseToString();

 if(SgPntrArrRefExp* arrRef = isSgPntrArrRefExp(exp) ){
 if(arrRef == NULL)
 throw ir_error("something wrong");
 IR_roseArrayRef *ref = new IR_roseArrayRef(this, arrRef);
 arrays.push_back(ref);
 }

 omega::CG_outputRepr *r = new omega::CG_roseRepr(isSgNode(exp->get_rhs_operand()));
 std::vector<IR_ArrayRef *> a = FindArrayRef(r);
 delete r;
 std::copy(a.begin(), a.end(), back_inserter(arrays));

 omega::CG_outputRepr *r1 = new omega::CG_roseRepr(isSgNode(exp->get_lhs_operand()));
 std::vector<IR_ArrayRef *> a1 = FindArrayRef(r1);
 delete r1;
 std::copy(a1.begin(), a1.end(), back_inserter(arrays));

 }
 }*/

}

std::vector<IR_PointerArrayRef *> IR_roseCode::FindPointerArrayRef(
	const omega::CG_outputRepr *repr) const {
std::vector<IR_PointerArrayRef *> arrays;
SgNode *tnl = static_cast<const omega::CG_roseRepr *>(repr)->GetCode();
SgStatementPtrList* list =
		static_cast<const omega::CG_roseRepr *>(repr)->GetList();
SgStatement* stmt;
SgExpression * exp;

if (list != NULL) {
	for (SgStatementPtrList::iterator it = (*list).begin(); it != (*list).end();
			it++) {
		omega::CG_roseRepr *r = new omega::CG_roseRepr(isSgNode(*it));
		std::vector<IR_PointerArrayRef *> a = FindPointerArrayRef(r);
		delete r;
		std::copy(a.begin(), a.end(), back_inserter(arrays));
	}
} else if (tnl != NULL) {
	if (stmt = isSgStatement(tnl)) {
		if (isSgBasicBlock(stmt)) {
			SgStatementPtrList& stmts = isSgBasicBlock(stmt)->get_statements();
			for (int i = 0; i < stmts.size(); i++) {
				omega::CG_roseRepr *r = new omega::CG_roseRepr(
						isSgNode(stmts[i]));
				std::vector<IR_PointerArrayRef *> a = FindPointerArrayRef(r);
				delete r;
				std::copy(a.begin(), a.end(), back_inserter(arrays));
			}

		} else if (isSgForStatement(stmt)) {

			SgForStatement *tnf = isSgForStatement(stmt);
			omega::CG_roseRepr *r = new omega::CG_roseRepr(
					isSgStatement(tnf->get_loop_body()));
			std::vector<IR_PointerArrayRef *> a = FindPointerArrayRef(r);
			delete r;
			std::copy(a.begin(), a.end(), back_inserter(arrays));
		} else if (isSgFortranDo(stmt)) {
			SgFortranDo *tfortran = isSgFortranDo(stmt);
			omega::CG_roseRepr *r = new omega::CG_roseRepr(
					isSgStatement(tfortran->get_body()));
			std::vector<IR_PointerArrayRef *> a = FindPointerArrayRef(r);
			delete r;
			std::copy(a.begin(), a.end(), back_inserter(arrays));
		} else if (isSgIfStmt(stmt)) {
			SgIfStmt* tni = isSgIfStmt(stmt);
			omega::CG_roseRepr *r = new omega::CG_roseRepr(
					isSgNode(tni->get_conditional()));
			std::vector<IR_PointerArrayRef *> a = FindPointerArrayRef(r);
			delete r;
			std::copy(a.begin(), a.end(), back_inserter(arrays));
			r = new omega::CG_roseRepr(isSgNode(tni->get_true_body()));
			a = FindPointerArrayRef(r);
			delete r;
			std::copy(a.begin(), a.end(), back_inserter(arrays));
			r = new omega::CG_roseRepr(isSgNode(tni->get_false_body()));
			a = FindPointerArrayRef(r);
			delete r;
			std::copy(a.begin(), a.end(), back_inserter(arrays));
		} else if (isSgWhileStmt(stmt)) {
			SgWhileStmt* tni = isSgWhileStmt(stmt);
			omega::CG_roseRepr *r = new omega::CG_roseRepr(
					isSgNode(tni->get_condition()));
			std::vector<IR_PointerArrayRef *> a = FindPointerArrayRef(r);
			delete r;
			std::copy(a.begin(), a.end(), back_inserter(arrays));
			r = new omega::CG_roseRepr(isSgNode(tni->get_body()));
			a = FindPointerArrayRef(r);
			delete r;
			std::copy(a.begin(), a.end(), back_inserter(arrays));

		} else if (isSgExprStatement(stmt)) {
			omega::CG_roseRepr *r = new omega::CG_roseRepr(
					isSgExpression(isSgExprStatement(stmt)->get_expression()));
			std::vector<IR_PointerArrayRef *> a = FindPointerArrayRef(r);
			delete r;
			std::copy(a.begin(), a.end(), back_inserter(arrays));

		}
	}
} else {
	SgExpression* op =
			static_cast<const omega::CG_roseRepr *>(repr)->GetExpression();
	if (isSgPntrArrRefExp(op)) {

		SgVarRefExp* base;
		SgExpression* op2;
		if (isSgCompoundAssignOp(isSgPntrArrRefExp(op)->get_parent())
				&& isSgCompoundAssignOp(isSgPntrArrRefExp(op)->get_parent())->get_lhs_operand()
						== isSgPntrArrRefExp(op)) {

			SgExpression *current = op;
			while (isSgPntrArrRefExp(current) || isSgUnaryOp(current)) {
				if (isSgPntrArrRefExp(current))
					current = isSgPntrArrRefExp(current)->get_lhs_operand();
				else if (isSgUnaryOp(current))
					/* To handle support for addressof operator and pointer dereference
					 * both of which are unary ops
					 */
					current = isSgUnaryOp(current)->get_operand();
			}
			if (base = isSgVarRefExp(current)) {
				SgVariableSymbol *arrSymbol =
						(SgVariableSymbol*) (base->get_symbol());

				if (isSgPointerType(arrSymbol->get_type())) {
					int dim = 0;
					SgType *aType = arrSymbol->get_type();
					while (isSgPointerType(aType)) {
						aType = isSgPointerType(aType)->get_base_type();
						dim++;
					}

					IR_rosePointerArrayRef *ref1 = new IR_rosePointerArrayRef(
							this, isSgPntrArrRefExp(op), 1,
							new IR_rosePointerSymbol(this, arrSymbol, dim));
					arrays.push_back(ref1);
					CG_outputRepr *clone_ia = (new CG_roseRepr(
							isSgPntrArrRefExp(op)))->clone();

					IR_rosePointerArrayRef *ref2 =
							new IR_rosePointerArrayRef(this,
									isSgPntrArrRefExp(
											static_cast<CG_roseRepr *>(clone_ia)->GetExpression()),
									0,
									new IR_rosePointerSymbol(this, arrSymbol,
											dim));
					arrays.push_back(ref2);

				}

			}

		} else if (isSgCompoundAssignOp(isSgPntrArrRefExp(op)->get_parent())
				&& isSgCompoundAssignOp(isSgPntrArrRefExp(op)->get_parent())->get_rhs_operand()
						== isSgPntrArrRefExp(op)) {

			SgExpression *current = op;
			while (isSgPntrArrRefExp(current) || isSgUnaryOp(current)) {
				if (isSgPntrArrRefExp(current))
					current = isSgPntrArrRefExp(current)->get_lhs_operand();
				else if (isSgUnaryOp(current))
					/* To handle support for addressof operator and pointer dereference
					 * both of which are unary ops
					 */
					current = isSgUnaryOp(current)->get_operand();
			}
			if (base = isSgVarRefExp(current)) {
				SgVariableSymbol *arrSymbol =
						(SgVariableSymbol*) (base->get_symbol());

				if (isSgPointerType(arrSymbol->get_type())) {
					int dim = 0;
					SgType *aType = arrSymbol->get_type();
					while (isSgPointerType(aType)) {
						aType = isSgPointerType(aType)->get_base_type();
						dim++;
					}

					IR_rosePointerArrayRef *ref1 = new IR_rosePointerArrayRef(
							this, isSgPntrArrRefExp(op), 0,
							new IR_rosePointerSymbol(this, arrSymbol, dim));
					arrays.push_back(ref1);

				}

			}

		}

		else {
			SgExpression *current = op;
			while (isSgPntrArrRefExp(current) || isSgUnaryOp(current)) {
				if (isSgPntrArrRefExp(current))
					current = isSgPntrArrRefExp(current)->get_lhs_operand();
				else if (isSgUnaryOp(current))
					/* To handle support for addressof operator and pointer dereference
					 * both of which are unary ops
					 */
					current = isSgUnaryOp(current)->get_operand();
			}
			if (base = isSgVarRefExp(current)) {
				SgVariableSymbol *arrSymbol =
						(SgVariableSymbol*) (base->get_symbol());

				if (isSgPointerType(arrSymbol->get_type())) {
					int dim = 0;
					SgType *aType = arrSymbol->get_type();
					while (isSgPointerType(aType)) {
						aType = isSgPointerType(aType)->get_base_type();
						dim++;
					}

					IR_rosePointerArrayRef *ref1 = new IR_rosePointerArrayRef(
							this, isSgPntrArrRefExp(op), 0,
							new IR_rosePointerSymbol(this, arrSymbol, dim));
					arrays.push_back(ref1);

				}

			}
		}
		while (isSgPntrArrRefExp(op)) {
			op2 = isSgPntrArrRefExp(op)->get_rhs_operand();
			op = isSgPntrArrRefExp(op)->get_lhs_operand();
			omega::CG_roseRepr *r = new omega::CG_roseRepr(op2);
			std::vector<IR_PointerArrayRef *> a = FindPointerArrayRef(r);
			delete r;
			std::copy(a.begin(), a.end(), back_inserter(arrays));

		}

		/* base = isSgVarRefExp(op);
		 SgVariableSymbol *arrSymbol = (SgVariableSymbol*)(base->get_symbol());
		 SgArrayType *arrType = isSgArrayType(arrSymbol->get_type());

		 SgExprListExp* dimList = arrType->get_dim_info();

		 if(dimList != NULL){
		 SgExpressionPtrList::iterator it = dimList->get_expressions().begin();
		 SgExpression *expr;


		 for (int i = 0; it != dimList->get_expressions().end(); it++, i++)
		 {
		 expr = *it;

		 omega::CG_roseRepr *r = new omega::CG_roseRepr(expr);
		 std::vector<IR_PointerArrayRef *> a = FindPointerArrayRef(r);
		 delete r;
		 std::copy(a.begin(), a.end(), back_inserter(arrays));
		 }

		 }
		 arrays.push_back(ref);
		 */
	} else if (isSgAssignOp(op)) {
		omega::CG_roseRepr *r1 = new omega::CG_roseRepr(
				isSgAssignOp(op)->get_lhs_operand());
		std::vector<IR_PointerArrayRef *> a1 = FindPointerArrayRef(r1);
		delete r1;
		std::copy(a1.begin(), a1.end(), back_inserter(arrays));
		omega::CG_roseRepr *r2 = new omega::CG_roseRepr(
				isSgAssignOp(op)->get_rhs_operand());
		std::vector<IR_PointerArrayRef *> a2 = FindPointerArrayRef(r2);
		delete r2;
		std::copy(a2.begin(), a2.end(), back_inserter(arrays));

	} else if (isSgBinaryOp(op)) {
		omega::CG_roseRepr *r1 = new omega::CG_roseRepr(
				isSgBinaryOp(op)->get_lhs_operand());
		std::vector<IR_PointerArrayRef *> a1 = FindPointerArrayRef(r1);
		delete r1;
		std::copy(a1.begin(), a1.end(), back_inserter(arrays));
		omega::CG_roseRepr *r2 = new omega::CG_roseRepr(
				isSgBinaryOp(op)->get_rhs_operand());
		std::vector<IR_PointerArrayRef *> a2 = FindPointerArrayRef(r2);
		delete r2;
		std::copy(a2.begin(), a2.end(), back_inserter(arrays));
	} else if (isSgUnaryOp(op)) {
		omega::CG_roseRepr *r1 = new omega::CG_roseRepr(
				isSgUnaryOp(op)->get_operand());
		std::vector<IR_PointerArrayRef *> a1 = FindPointerArrayRef(r1);
		delete r1;
		std::copy(a1.begin(), a1.end(), back_inserter(arrays));
	}

	else if (SgCallExpression *call = isSgCallExpression(op)) {

		SgExprListExp* arg_list = call->get_args();

		SgExpressionPtrList & expressionPtrList = arg_list->get_expressions();

		for (SgExpressionPtrList::iterator i = expressionPtrList.begin();
				i != expressionPtrList.end(); i++) {
			omega::CG_roseRepr *r1 = new omega::CG_roseRepr(*i);
			std::vector<IR_PointerArrayRef *> a1 = FindPointerArrayRef(r1);
			delete r1;
			std::copy(a1.begin(), a1.end(), back_inserter(arrays));
		}
	}
}
return arrays;

/* std::string x;
 SgStatement* stmt = isSgStatement(tnl);
 SGExprStatement* expr_statement = isSgExprStatement(stmt);
 SgExpression* exp= NULL;
 if(expr_statement == NULL){
 if(! (SgExpression* exp = isSgExpression(tnl))
 throw ir_error("FindPointerArrayRef: Not a stmt nor an expression!!");

 if( expr_statement != NULL){
 for(int i=0; i < tnl->get_numberOfTraversalSuccessors(); i++){

 SgNode* tn = isSgStatement(tnl);
 SgStatement* stmt = isSgStatement(tn);
 if(stmt != NULL){
 SgExprStatement* expr_statement = isSgExprStatement(tn);
 if(expr_statement != NULL)
 x = isSgNode(expr_statement)->unparseToString();
 exp = expr_statement->get_expression();

 }
 else{

 exp = isSgExpression(tn);
 }
 if(exp != NULL){
 x = isSgNode(exp)->unparseToString();

 if(SgPntrArrRefExp* arrRef = isSgPntrArrRefExp(exp) ){
 if(arrRef == NULL)
 throw ir_error("something wrong");
 IR_roseArrayRef *ref = new IR_roseArrayRef(this, arrRef);
 arrays.push_back(ref);
 }

 omega::CG_outputRepr *r = new omega::CG_roseRepr(isSgNode(exp->get_rhs_operand()));
 std::vector<IR_PointerArrayRef *> a = FindPointerArrayRef(r);
 delete r;
 std::copy(a.begin(), a.end(), back_inserter(arrays));

 omega::CG_outputRepr *r1 = new omega::CG_roseRepr(isSgNode(exp->get_lhs_operand()));
 std::vector<IR_PointerArrayRef *> a1 = FindPointerArrayRef(r1);
 delete r1;
 std::copy(a1.begin(), a1.end(), back_inserter(arrays));

 }
 }*/

}

bool IR_roseCode::parent_is_array(IR_ArrayRef *a) {

SgPntrArrRefExp* ia_orig = static_cast<IR_roseArrayRef *>(a)->ia_;

if (isSgPntrArrRefExp(isSgNode(ia_orig)->get_parent()))
	return true;

return false;
}

// Manu:: Function to check if two array references are from the same statement
//CG_outputRepr * IR_roseCode::FromSameStmt(IR_ArrayRef *A, IR_ArrayRef *B) {
bool IR_roseCode::FromSameStmt(IR_ArrayRef *A, IR_ArrayRef *B) {
omega::CG_outputRepr *reprA = A->convert();
SgNode *nodeA = static_cast<const omega::CG_roseRepr *>(reprA)->GetExpression();
omega::CG_outputRepr *reprB = B->convert();
SgNode *nodeB = static_cast<const omega::CG_roseRepr *>(reprB)->GetExpression();
if ((nodeB == NULL) || (nodeA == NULL))
	std::cout << "FromSameStmt:: SgNode is NULL \n";
if (FromStmt(nodeA) == FromStmt(nodeB)) {
//		omega::CG_roseRepr *r = new omega::CG_roseRepr(isSgNode(FromStmt(nodeA)));
//		return r->clone();
	return true;
} else {
	return false;
//		return NULL;
}
}

// Manu:: Function returns the first SgStatement for an array reference
SgStatement * IR_roseCode::FromStmt(SgNode *node) {
if (isSgStatement(node))
	return isSgStatement(node);
return FromStmt(isSgNode(node->get_parent()));

}

// Manu:: Function returns the first SgForStatement for given statement
IR_Control * IR_roseCode::FromForStmt(const omega::CG_outputRepr *repr) {
SgNode *node = static_cast<const omega::CG_roseRepr *>(repr)->GetCode();
SgNode *resNode = FromForStmtHelper(node);
std::cout << "FromForStmt:: " << resNode->unparseToString() << "\n";
return (new IR_roseLoop(this, resNode));
}

// Manu:: Function returns the first SgForStatement for given statement
SgNode * IR_roseCode::FromForStmtHelper(SgNode *node) {
if (isSgForStatement(node)) {
	return node;
}
return FromForStmtHelper(isSgNode(node->get_parent()));
}

// Manu:: Function to print a statement
void IR_roseCode::printStmt(const omega::CG_outputRepr *repr) {
SgNode *tnl = static_cast<const omega::CG_roseRepr *>(repr)->GetCode();
SgNode *node = static_cast<const omega::CG_roseRepr *>(repr)->GetExpression();
SgStatementPtrList *list =
		static_cast<const omega::CG_roseRepr *>(repr)->GetList();
if (tnl != NULL) {
	if (isSgStatement(tnl))
		std::cout << "Is an SgStatement \n";
	std::cout << tnl->unparseToString() << "\n";
	//printStmtHelper(tnl);
	//std::cout<<"\n";
} else if (node != NULL) {
	std::cout << node->unparseToString() << "\n";
	//printStmtHelper(node);
	//std::cout<<"\n";
} else if (list != NULL) {
	SgStatementPtrList::iterator it = list->begin();
	for (it = list->begin(); it != list->end(); it++) {
		SgNode *node1 = isSgNode(*it);
		if (node1 != NULL)
			std::cout << node1->unparseToString() << "\n";
	}
} else {
	std::cout << "printStmt:: Statement is NULL \n";
}
}

// Manu
void IR_roseCode::printStmtHelper(SgNode *node) {
std::string x;
size_t numberOfSuccessors = node->get_numberOfTraversalSuccessors();
if (numberOfSuccessors == 0) {
	x = node->unparseToString();
	std::cout << x.c_str();
} else {
//		 std::cout << "Manu::11 " <<  x.c_str() << "\n";
	for (size_t idx = 0; idx < numberOfSuccessors; idx++) {
		SgNode *child = NULL;
		child = node->get_traversalSuccessorByIndex(idx);
		printStmtHelper(child);
	}

}
}

// Manu:: Function to return statement type
int IR_roseCode::getStmtType(const omega::CG_outputRepr *repr) {
SgNode *tnl = static_cast<const omega::CG_roseRepr *>(repr)->GetCode();
SgNode *node = static_cast<const omega::CG_roseRepr *>(repr)->GetExpression();
SgStatementPtrList *list =
		static_cast<const omega::CG_roseRepr *>(repr)->GetList();
if (tnl != NULL) {
	return 1;
} else if (node != NULL) {
	return 2;
} else if (list != NULL) {
	return 3;
} else {
	return 0; // something wrong here!
}
}

// Manu: This will not work for more than 1 statement in a block.
IR_OPERATION_TYPE IR_roseCode::getReductionOp(
	const omega::CG_outputRepr *repr) {
SgExpression* opExp =
		static_cast<const omega::CG_roseRepr *>(repr)->GetExpression();
SgNode* op, *op1;

if (opExp == NULL) {
	op = static_cast<const omega::CG_roseRepr *>(repr)->GetCode();
	if (op != NULL) {
		if (isSgExprStatement(op)) {
			opExp = isSgExprStatement(op)->get_expression();
			if (isSgPlusAssignOp(opExp))
				return IR_OP_PLUS;
			if (isSgMinusAssignOp(opExp))
				return IR_OP_MINUS;
			if (isSgDivAssignOp(opExp))
				return IR_OP_DIVIDE;
			if (isSgAssignOp(opExp))
				op1 = isSgAssignOp(opExp)->get_rhs_operand();
			if (isSgAddOp(op1))
				return IR_OP_PLUS;
			else if (isSgSubtractOp(op1))
				return IR_OP_MINUS;
			else if (isSgMultiplyOp(op1))
				return IR_OP_MULTIPLY;
			else if (isSgDivideOp(op1))
				return IR_OP_DIVIDE;

		} else {
			return IR_OP_UNKNOWN; // something wrong
		}
	} else {
		return IR_OP_UNKNOWN; // something wrong
	}
}
return IR_OP_UNKNOWN; // the operation is not handled
}

std::vector<IR_Control *> IR_roseCode::FindOneLevelControlStructure(
	const IR_Block *block) const {

std::vector<IR_Control *> controls;
int i;
int j;
int begin;
int end;
SgNode* tnl_ =
		((static_cast<IR_roseBlock *>(const_cast<IR_Block *>(block)))->tnl_);

if (isSgForStatement(tnl_))
	controls.push_back(new IR_roseLoop(this, tnl_));
else if (isSgIfStmt(tnl_))
	controls.push_back(new IR_roseIf(this, tnl_));

else if (isSgBasicBlock(tnl_)) {

	SgStatementPtrList& stmts = isSgBasicBlock(tnl_)->get_statements();

	for (i = 0; i < stmts.size(); i++) {
		if (isSgNode(stmts[i])
				== ((static_cast<IR_roseBlock *>(const_cast<IR_Block *>(block)))->start_))
			begin = i;
		if (isSgNode(stmts[i])
				== ((static_cast<IR_roseBlock *>(const_cast<IR_Block *>(block)))->end_))
			end = i;
	}

	SgNode* start = NULL;
	SgNode* prev = NULL;
	for (i = begin; i <= end; i++) {
		if (isSgForStatement(stmts[i]) || isSgFortranDo(stmts[i])) {
			if (start != NULL) {
				controls.push_back(
						new IR_roseBlock(this,
								(static_cast<IR_roseBlock *>(const_cast<IR_Block *>(block)))->tnl_,
								start, prev));
				start = NULL;
			}
			controls.push_back(new IR_roseLoop(this, isSgNode(stmts[i])));
		} else if (isSgIfStmt(stmts[i])) {
			if (start != NULL) {
				controls.push_back(
						new IR_roseBlock(this,
								(static_cast<IR_roseBlock *>(const_cast<IR_Block *>(block)))->tnl_,
								start, prev));
				start = NULL;
			}
			controls.push_back(new IR_roseIf(this, isSgNode(stmts[i])));

		} else if (start == NULL)
			start = isSgNode(stmts[i]);

		prev = isSgNode(stmts[i]);
	}

	if ((start != NULL) && (start != isSgNode(stmts[begin])))
		controls.push_back(
				new IR_roseBlock(this,
						(static_cast<IR_roseBlock *>(const_cast<IR_Block *>(block)))->tnl_,
						start, prev));
}

return controls;

}

/*std::vector<IR_Control *> IR_roseCode::FindOneLevelControlStructure(const IR_Block *block) const {

 std::vector<IR_Control *> controls;
 int i;
 int j;
 SgNode* tnl_ = ((static_cast<IR_roseBlock *>(const_cast<IR_Block *>(block)))->tnl_);


 if(isSgForStatement(tnl_))
 controls.push_back(new IR_roseLoop(this,tnl_));

 else if(isSgBasicBlock(tnl_)){

 SgStatementPtrList& stmts = isSgBasicBlock(tnl_)->get_statements();

 for(i =0; i < stmts.size(); i++){
 if(isSgNode(stmts[i]) == ((static_cast<IR_roseBlock *>(const_cast<IR_Block *>(block)))->start_))
 break;
 }


 SgNode* start= NULL;
 SgNode* prev= NULL;
 for(; i < stmts.size(); i++){
 if ( isSgForStatement(stmts[i]) || isSgFortranDo(stmts[i])){
 if(start != NULL){
 controls.push_back(new IR_roseBlock(this, (static_cast<IR_roseBlock *>(const_cast<IR_Block *>(block)))->tnl_ , start, prev));
 start = NULL;
 }
 controls.push_back(new IR_roseLoop(this, isSgNode(stmts[i])));
 }
 else if( start == NULL )
 start = isSgNode(stmts[i]);

 prev = isSgNode(stmts[i]);
 }

 if((start != NULL) && (start != isSgNode(stmts[0])))
 controls.push_back(new IR_roseBlock(this, (static_cast<IR_roseBlock *>(const_cast<IR_Block *>(block)))->tnl_, start, prev));
 }

 return controls;

 }

 */
IR_Block *IR_roseCode::MergeNeighboringControlStructures(
	const std::vector<IR_Control *> &controls) const {
if (controls.size() == 0)
	return NULL;

SgNode *tnl = NULL;
SgNode *start, *end;
for (int i = 0; i < controls.size(); i++) {
	switch (controls[i]->type()) {
	case IR_CONTROL_LOOP: {
		SgNode *tf = static_cast<IR_roseLoop *>(controls[i])->tf_;
		if (tnl == NULL) {
			tnl = tf->get_parent();
			start = end = tf;
		} else {
			if (tnl != tf->get_parent())
				throw ir_error("controls to merge not at the same level");
			end = tf;
		}
		break;
	}
	case IR_CONTROL_BLOCK: {
		if (tnl == NULL) {
			tnl = static_cast<IR_roseBlock *>(controls[0])->tnl_;
			start = static_cast<IR_roseBlock *>(controls[0])->start_;
			end = static_cast<IR_roseBlock *>(controls[0])->end_;
		} else {
			if (tnl != static_cast<IR_roseBlock *>(controls[0])->tnl_)
				throw ir_error("controls to merge not at the same level");
			end = static_cast<IR_roseBlock *>(controls[0])->end_;
		}
		break;
	}
	default:
		throw ir_error("unrecognized control to merge");
	}
}

return new IR_roseBlock(controls[0]->ir_, tnl, start, end);
}

IR_Block *IR_roseCode::GetCode() const {
SgFunctionDefinition* def = NULL;
SgBasicBlock* block = NULL;
if (func != 0) {
	if (def = func->get_definition()) {
		if (block = def->get_body())
			return new IR_roseBlock(this, func->get_definition()->get_body());
	}
}

return NULL;

}

IR_Control *IR_roseCode::GetCode(omega::CG_outputRepr *code) const {

SgNode* node_ = static_cast<omega::CG_roseRepr *>(code)->GetCode();

if (node_ != NULL) {
	if (isSgIfStmt(node_))
		return new IR_roseIf(this, node_);
	else if (isSgForStatement(node_))
		return new IR_roseLoop(this, node_);
	else

		return new IR_roseBlock(this, node_);
} else
	return NULL;

}

void IR_roseCode::ReplaceCode(IR_Control *old, omega::CG_outputRepr *repr) {
/*	SgStatementPtrList *tnl =
 static_cast<omega::CG_roseRepr *>(repr)->GetList();
 SgNode *tf_old;
 */
SgStatementPtrList *tnl = static_cast<omega::CG_roseRepr *>(repr)->GetList();
SgNode* node_ = static_cast<omega::CG_roseRepr *>(repr)->GetCode();
SgNode * tf_old;

/* May need future revision it tnl has more than one statement */

switch (old->type()) {

case IR_CONTROL_LOOP:
	tf_old = static_cast<IR_roseLoop *>(old)->tf_;
	break;
case IR_CONTROL_BLOCK:
	tf_old = static_cast<IR_roseBlock *>(old)->start_;
	break;

default:
	throw ir_error("control structure to be replaced not supported");
	break;
}

std::string y = tf_old->unparseToString();
SgStatement *s = isSgStatement(tf_old);
if (s != 0) {
	SgStatement *p = isSgStatement(tf_old->get_parent());

	if (p != 0) {
		SgStatement* temp = s;
		if (tnl != NULL) {
			SgStatementPtrList::iterator it = (*tnl).begin();
			p->insert_statement(temp, *it, true);
			temp = *it;
			p->remove_statement(s);
			it++;
			for (; it != (*tnl).end(); it++) {
				p->insert_statement(temp, *it, false);
				temp = *it;
			}
		} else if (node_ != NULL) {
			if (!isSgStatement(node_))
				throw ir_error("Replacing Code not a statement!");
			else {
				SgStatement* replace_ = isSgStatement(node_);
				p->insert_statement(s, replace_, true);
				p->remove_statement(s);

			}
		} else {
			throw ir_error("Replacing Code not a statement!");
		}
	} else
		throw ir_error("Replacing Code not a statement!");
} else
	throw ir_error("Replacing Code not a statement!");

delete old;
delete repr;
/* May need future revision it tnl has more than one statement */
/*
 switch (old->type()) {

 case IR_CONTROL_LOOP:
 tf_old = static_cast<IR_roseLoop *>(old)->tf_;
 break;
 case IR_CONTROL_BLOCK:
 tf_old = static_cast<IR_roseBlock *>(old)->start_;
 break;

 default:
 throw ir_error("control structure to be replaced not supported");
 break;
 }

 // std::string y = tf_old->unparseToString();
 SgStatement *s = isSgStatement(tf_old);
 if (s != 0) {
 SgStatement *p = isSgStatement(tf_old->get_parent());

 if (p != 0) {
 //      SgStatement* it2 = isSgStatement(tnl);

 //   if(it2 != NULL){
 p->replace_statement(s, *tnl);
 //   }
 //   else {
 //          throw ir_error("Replacing Code not a statement!");
 //      }
 } else
 throw ir_error("Replacing Code not a statement!");
 } else
 throw ir_error("Replacing Code not a statement!");
 //  y = tnl->unparseToString();
 delete old;
 delete repr;
 */
}

void IR_roseCode::ReplaceExpression(IR_Ref *old, omega::CG_outputRepr *repr) {

SgExpression* op = static_cast<omega::CG_roseRepr *>(repr)->GetExpression();

if (typeid(*old) == typeid(IR_roseArrayRef)) {
	SgPntrArrRefExp* ia_orig = static_cast<IR_roseArrayRef *>(old)->ia_;
	SgExpression* parent = isSgExpression(isSgNode(ia_orig)->get_parent());
	//std::string x = isSgNode(op)->unparseToString();
	//	std::string y = isSgNode(ia_orig)->unparseToString();
	if (parent != NULL) {
		//	std::string z = isSgNode(parent)->unparseToString();
		parent->replace_expression(ia_orig, op);
		isSgNode(op)->set_parent(isSgNode(parent));

		/* if(isSgBinaryOp(parent))
		 {
		 if(isSgBinaryOp(parent)->get_lhs_operand() == ia_orig){
		 isSgBinaryOp(parent)->set_lhs_operand(op);
		 }else if(isSgBinaryOp(parent)->get_rhs_operand() == ia_orig){
		 isSgBinaryOp(parent)->set_rhs_operand(op);


		 }
		 else
		 parent->replace_expression(ia_orig, op);
		 */
	} else {
		SgStatement* parent_stmt = isSgStatement(
				isSgNode(ia_orig)->get_parent());
		if (parent_stmt != NULL)
			parent_stmt->replace_expression(ia_orig, op);
		else
			throw ir_error(
					"ReplaceExpression: parent neither expression nor statement");
	}
} else if (typeid(*old) == typeid(IR_rosePointerArrayRef)) {
	SgPntrArrRefExp* ia_orig = static_cast<IR_roseArrayRef *>(old)->ia_;
	SgExpression* parent = isSgExpression(isSgNode(ia_orig)->get_parent());
	//std::string x = isSgNode(op)->unparseToString();
	//std::string y = isSgNode(ia_orig)->unparseToString();
	if (parent != NULL) {
		//std::string z = isSgNode(parent)->unparseToString();
		parent->replace_expression(ia_orig, op);
		isSgNode(op)->set_parent(isSgNode(parent));

		/* if(isSgBinaryOp(parent))
		 {
		 if(isSgBinaryOp(parent)->get_lhs_operand() == ia_orig){
		 isSgBinaryOp(parent)->set_lhs_operand(op);
		 }else if(isSgBinaryOp(parent)->get_rhs_operand() == ia_orig){
		 isSgBinaryOp(parent)->set_rhs_operand(op);


		 }
		 else
		 parent->replace_expression(ia_orig, op);
		 */
	} else {
		SgStatement* parent_stmt = isSgStatement(
				isSgNode(ia_orig)->get_parent());
		if (parent_stmt != NULL)
			parent_stmt->replace_expression(ia_orig, op);
		else
			throw ir_error(
					"ReplaceExpression: parent neither expression nor statement");
	}
}

else if (typeid(*old) == typeid(IR_roseScalarRef)) {

	SgVarRefExp* ia_orig = static_cast<IR_roseScalarRef *>(old)->vs_;
	SgExpression* parent = isSgExpression(isSgNode(ia_orig)->get_parent());
	if (parent != NULL) {
		std::string z = isSgNode(parent)->unparseToString();
		parent->replace_expression(ia_orig, op);
		isSgNode(op)->set_parent(isSgNode(parent));
		z = isSgNode(parent)->unparseToString();
		/* if(isSgBinaryOp(parent))
		 {
		 if(isSgBinaryOp(parent)->get_lhs_operand() == ia_orig){
		 isSgBinaryOp(parent)->set_lhs_operand(op);
		 }else if(isSgBinaryOp(parent)->get_rhs_operand() == ia_orig){
		 isSgBinaryOp(parent)->set_rhs_operand(op);


		 }
		 else
		 parent->replace_expression(ia_orig, op);
		 */
	} else {
		SgStatement* parent_stmt = isSgStatement(
				isSgNode(ia_orig)->get_parent());
		if (parent_stmt != NULL)
			parent_stmt->replace_expression(ia_orig, op);
		else
			throw ir_error(
					"ReplaceExpression: parent neither expression nor statement");
	}
}

//delete old; Anand: figure deletion later
}

/*std::pair<std::vector<DependenceVector>, std::vector<DependenceVector> > IR_roseCode::FindScalarDeps(
 const omega::CG_outputRepr *repr1, const omega::CG_outputRepr *repr2,
 std::vector<std::string> index, int i, int j) {

 std::vector<DependenceVector> dvs1;
 std::vector<DependenceVector> dvs2;
 SgNode *tnl_1 = static_cast<const omega::CG_roseRepr *>(repr1)->GetCode();
 SgNode *tnl_2 = static_cast<const omega::CG_roseRepr *>(repr2)->GetCode();
 SgStatementPtrList* list_1 =
 static_cast<const omega::CG_roseRepr *>(repr1)->GetList();
 SgStatementPtrList output_list_1;

 std::map<SgVarRefExp*, IR_ScalarRef*> read_scalars_1;
 std::map<SgVarRefExp*, IR_ScalarRef*> write_scalars_1;
 std::set<std::string> indices;
 //std::set<VirtualCFG::CFGNode> reaching_defs_1;
 std::set<std::string> def_vars_1;

 populateLists(tnl_1, list_1, output_list_1);
 populateScalars(repr1, read_scalars_1, write_scalars_1, indices, index);
 //def_vars_1);
 //findDefinitions(output_list_1, reaching_defs_1, write_scalars_1);
 //def_vars_1);
 if (repr1 == repr2)
 checkSelfDependency(output_list_1, dvs1, read_scalars_1,
 write_scalars_1, index, i, j);
 else {
 SgStatementPtrList* list_2 =
 static_cast<const omega::CG_roseRepr *>(repr2)->GetList();
 SgStatementPtrList output_list_2;

 std::map<SgVarRefExp*, IR_ScalarRef*> read_scalars_2;
 std::map<SgVarRefExp*, IR_ScalarRef*> write_scalars_2;
 //std::set<VirtualCFG::CFGNode> reaching_defs_2;
 std::set<std::string> def_vars_2;

 populateLists(tnl_2, list_2, output_list_2);
 populateScalars(repr2, read_scalars_2, write_scalars_2, indices, index);
 //def_vars_2);

 checkDependency(output_list_2, dvs1, read_scalars_2, write_scalars_1,
 index, i, j);
 checkDependency(output_list_1, dvs1, read_scalars_1, write_scalars_2,
 index, i, j);
 checkWriteDependency(output_list_2, dvs1, write_scalars_2,
 write_scalars_1, index, i, j);
 checkWriteDependency(output_list_1, dvs1, write_scalars_1,
 write_scalars_2, index, i, j);
 }

 return std::make_pair(dvs1, dvs2);
 //populateLists(tnl_2, list_2, list2);

 }
 */
IR_OPERATION_TYPE IR_roseCode::QueryExpOperation(
	const omega::CG_outputRepr *repr) const {
SgExpression* op =
		static_cast<const omega::CG_roseRepr *>(repr)->GetExpression();

if (op == NULL) {
	SgNode *nd = static_cast<const omega::CG_roseRepr *>(repr)->GetCode();
	if (isSgExpression(nd))
		op = isSgExpression(nd);
	else if (isSgExprStatement(nd))
		op = isSgExprStatement(nd)->get_expression();
	else
		throw ir_error("unknown expression type in QueryExpOperation");
}
if (isSgValueExp(op))
	return IR_OP_CONSTANT;
else if (isSgVarRefExp(op))
	return IR_OP_VARIABLE;
//Anand: Added this to differentiate an Array from a scalar
else if (isSgPlusAssignOp(op))
	return IR_OP_PLUS_ASSIGNMENT;
else if (isSgPntrArrRefExp(op))
	return IR_OP_ARRAY_VARIABLE;
//Anand adding support for macro
else if (isSgCallExpression(op))
	return IR_OP_MACRO;
else if (isSgAssignOp(op) || isSgCompoundAssignOp(op))
	return IR_OP_ASSIGNMENT;
else if (isSgAddOp(op))
	return IR_OP_PLUS;
else if (isSgSubtractOp(op))
	return IR_OP_MINUS;
else if (isSgMultiplyOp(op))
	return IR_OP_MULTIPLY;
else if (isSgDivideOp(op))
	return IR_OP_DIVIDE;
else if (isSgMinusOp(op))
	return IR_OP_NEGATIVE;
else if (isSgConditionalExp(op)) {
	SgExpression* cond = isSgConditionalExp(op)->get_conditional_exp();
	if (isSgGreaterThanOp(cond))
		return IR_OP_MAX;
	else if (isSgLessThanOp(cond))
		return IR_OP_MIN;
} else if (isSgUnaryAddOp(op))
	return IR_OP_POSITIVE;
else if (isSgNullExpression(op))
	return IR_OP_NULL;
else
	return IR_OP_UNKNOWN;
}
/*void IR_roseCode::populateLists(SgNode* tnl_1, SgStatementPtrList* list_1,
 SgStatementPtrList& output_list_1) {
 if ((tnl_1 == NULL) && (list_1 != NULL)) {
 output_list_1 = *list_1;
 } else if (tnl_1 != NULL) {

 if (isSgForStatement(tnl_1)) {
 SgStatement* check = isSgForStatement(tnl_1)->get_loop_body();
 if (isSgBasicBlock(check)) {
 output_list_1 = isSgBasicBlock(check)->get_statements();

 } else
 output_list_1.push_back(check);

 } else if (isSgBasicBlock(tnl_1))
 output_list_1 = isSgBasicBlock(tnl_1)->get_statements();
 else if (isSgExprStatement(tnl_1))
 output_list_1.push_back(isSgExprStatement(tnl_1));
 else
 //if (isSgIfStmt(tnl_1)) {

 throw ir_error(
 "Statement type not handled, (probably IF statement)!!");

 }

 }

 void IR_roseCode::populateScalars(const omega::CG_outputRepr *repr1,
 std::map<SgVarRefExp*, IR_ScalarRef*> &read_scalars_1,
 std::map<SgVarRefExp*, IR_ScalarRef*> &write_scalars_1,
 std::set<std::string> &indices, std::vector<std::string> &index) {

 //std::set<std::string> &def_vars) {
 std::vector<IR_ScalarRef *> scalars = FindScalarRef(repr1);

 for (int k = 0; k < index.size(); k++)
 indices.insert(index[k]);

 for (int k = 0; k < scalars.size(); k++)
 if (indices.find(scalars[k]->name()) == indices.end()) {
 if (scalars[k]->is_write()) {
 write_scalars_1.insert(
 std::pair<SgVarRefExp*, IR_ScalarRef*>(
 (isSgVarRefExp(
 static_cast<const omega::CG_roseRepr *>(scalars[k]->convert())->GetExpression())),
 scalars[k]));

 } else

 read_scalars_1.insert(
 std::pair<SgVarRefExp*, IR_ScalarRef*>(
 (isSgVarRefExp(
 static_cast<const omega::CG_roseRepr *>(scalars[k]->convert())->GetExpression())),
 scalars[k]));
 }

 }


 void IR_roseCode::checkWriteDependency(SgStatementPtrList &output_list_1,
 std::vector<DependenceVector> &dvs1,
 std::map<SgVarRefExp*, IR_ScalarRef*> &read_scalars_1,
 std::map<SgVarRefExp*, IR_ScalarRef*> &write_scalars_1,
 std::vector<std::string> &index, int i, int j) {

 for (std::map<SgVarRefExp*, IR_ScalarRef*>::iterator it =
 read_scalars_1.begin(); it != read_scalars_1.end(); it++) {
 SgVarRefExp* var__ = it->first;

 ssa_unfiltered_cfg::SSA_UnfilteredCfg::NodeReachingDefTable to_compare =
 main_ssa->getReachingDefsBefore(isSgNode(var__));

 for (ssa_unfiltered_cfg::SSA_UnfilteredCfg::NodeReachingDefTable::iterator it4 =
 to_compare.begin(); it4 != to_compare.end(); it4++) {
 ssa_unfiltered_cfg::SSA_UnfilteredCfg::VarName var_ = it4->first;
 for (int j = 0; j < var_.size(); j++) {
 int found = 0;
 if (var_[j] == var__->get_symbol()->get_declaration()) {

 ssa_unfiltered_cfg::ReachingDef::ReachingDefPtr to_compare_2 =
 it4->second;

 if (to_compare_2->isPhiFunction()) {
 std::set<VirtualCFG::CFGNode> to_compare_set =
 to_compare_2->getActualDefinitions();
 for (std::set<VirtualCFG::CFGNode>::iterator cfg_it =
 to_compare_set.begin();
 cfg_it != to_compare_set.end(); cfg_it++) {

 if (isSgAssignOp(cfg_it->getNode())
 || isSgCompoundAssignOp(cfg_it->getNode()))
 if (SgVarRefExp* variable =
 isSgVarRefExp(
 isSgBinaryOp(cfg_it->getNode())->get_lhs_operand())) {

 if (write_scalars_1.find(variable)
 != write_scalars_1.end()) {


 //end debug
 found = 1;
 DependenceVector dv1;
 dv1.sym = it->second->symbol();
 dv1.is_scalar_dependence = true;

 int max = (j > i) ? j : i;
 int start = index.size() - max;

 //1.lbounds.push_back(0);
 //1.ubounds.push_back(0);
 //dv2.sym =
 //		read_scalars_2.find(*di)->second->symbol();
 for (int k = 0; k < index.size(); k++) {
 if (k >= max) {
 dv1.lbounds.push_back(
 negInfinity);
 dv1.ubounds.push_back(-1);
 } else {
 dv1.lbounds.push_back(0);
 dv1.ubounds.push_back(0);

 }

 }
 dvs1.push_back(dv1);
 break;
 }
 }
 }

 }

 }
 if (found == 1)
 break;
 }
 }
 }
 }
 void IR_roseCode::checkDependency(SgStatementPtrList &output_list_1,
 std::vector<DependenceVector> &dvs1,
 std::map<SgVarRefExp*, IR_ScalarRef*> &read_scalars_1,
 std::map<SgVarRefExp*, IR_ScalarRef*> &write_scalars_1,
 std::vector<std::string> &index, int i, int j) {

 for (SgStatementPtrList::iterator it2 = output_list_1.begin();
 it2 != output_list_1.end(); it2++) {

 std::set<SgVarRefExp*> vars_1 = main_ssa->getUsesAtNode(
 isSgNode(isSgExprStatement(*it2)->get_expression()));

 std::set<SgVarRefExp*>::iterator di;

 for (di = vars_1.begin(); di != vars_1.end(); di++) {
 int found = 0;
 if (read_scalars_1.find(*di) != read_scalars_1.end()) {

 ssa_unfiltered_cfg::ReachingDef::ReachingDefPtr to_compare =
 main_ssa->getDefinitionForUse(*di);
 if (to_compare->isPhiFunction()) {

 std::set<VirtualCFG::CFGNode> to_compare_set =
 to_compare->getActualDefinitions();

 for (std::set<VirtualCFG::CFGNode>::iterator cfg_it =
 to_compare_set.begin();
 cfg_it != to_compare_set.end(); cfg_it++) {


 if (SgAssignOp* definition = isSgAssignOp(
 cfg_it->getNode()))
 if (SgVarRefExp* variable = isSgVarRefExp(
 definition->get_lhs_operand())) {

 if (write_scalars_1.find(variable)
 != write_scalars_1.end()) {

 found = 1;
 DependenceVector dv1;
 //DependenceVector dv2;
 dv1.sym =
 read_scalars_1.find(*di)->second->symbol();
 dv1.is_scalar_dependence = true;

 int max = (j > i) ? j : i;
 int start = index.size() - max;

 //1.lbounds.push_back(0);
 //1.ubounds.push_back(0);
 //dv2.sym =
 //		read_scalars_2.find(*di)->second->symbol();
 for (int k = 0; k < index.size(); k++) {
 if (k >= max) {
 dv1.lbounds.push_back(negInfinity);
 dv1.ubounds.push_back(-1);
 } else {
 dv1.lbounds.push_back(0);
 dv1.ubounds.push_back(0);

 }

 }
 dvs1.push_back(dv1);
 break;
 }
 }
 }
 }
 if (found == 1)
 break;
 }
 }
 }

 }

 void IR_roseCode::checkSelfDependency(SgStatementPtrList &output_list_1,
 std::vector<DependenceVector> &dvs1,
 std::map<SgVarRefExp*, IR_ScalarRef*> &read_scalars_1,
 std::map<SgVarRefExp*, IR_ScalarRef*> &write_scalars_1,
 std::vector<std::string> &index, int i, int j) {

 for (SgStatementPtrList::iterator it2 = output_list_1.begin();
 it2 != output_list_1.end(); it2++) {

 std::set<SgVarRefExp*> vars_1 = main_ssa->getUsesAtNode(
 isSgNode(isSgExprStatement(*it2)->get_expression()));

 std::set<SgVarRefExp*>::iterator di;

 for (di = vars_1.begin(); di != vars_1.end(); di++) {

 if (read_scalars_1.find(*di) != read_scalars_1.end()) {

 ssa_unfiltered_cfg::ReachingDef::ReachingDefPtr to_compare =
 main_ssa->getDefinitionForUse(*di);
 if (to_compare->isPhiFunction()) {

 std::set<VirtualCFG::CFGNode> to_compare_set =
 to_compare->getActualDefinitions();
 int found = 0;
 for (std::set<VirtualCFG::CFGNode>::iterator cfg_it =
 to_compare_set.begin();
 cfg_it != to_compare_set.end(); cfg_it++) {

 if (isSgAssignOp(cfg_it->getNode())
 || isSgCompoundAssignOp(cfg_it->getNode()))
 if (SgVarRefExp* variable =
 isSgVarRefExp(
 isSgBinaryOp(cfg_it->getNode())->get_lhs_operand())) {

 if (write_scalars_1.find(variable)
 == write_scalars_1.end()) {


 found = 1;
 DependenceVector dv1;
 dv1.sym =
 read_scalars_1.find(*di)->second->symbol();
 dv1.is_scalar_dependence = true;

 int max = (j > i) ? j : i;
 int start = index.size() - max;

 //1.lbounds.push_back(0);
 //1.ubounds.push_back(0);
 //dv2.sym =
 //		read_scalars_2.find(*di)->second->symbol();
 for (int k = 0; k < index.size(); k++) {
 if (k >= max) {
 dv1.lbounds.push_back(negInfinity);
 dv1.ubounds.push_back(-1);
 } else {
 dv1.lbounds.push_back(0);
 dv1.ubounds.push_back(0);

 }

 }
 dvs1.push_back(dv1);
 break;
 }
 }
 }
 }

 }
 }
 }

 }
 */
IR_CONDITION_TYPE IR_roseCode::QueryBooleanExpOperation(
	const omega::CG_outputRepr *repr) const {
SgExpression* op2 =
		static_cast<const omega::CG_roseRepr *>(repr)->GetExpression();
SgNode* op;

if (op2 == NULL) {
	op = static_cast<const omega::CG_roseRepr *>(repr)->GetCode();

	if (op != NULL) {
		if (isSgExprStatement(op))
			op2 = isSgExprStatement(op)->get_expression();
		else if (isSgIfStmt(op))
			return IR_COND_UNKNOWN;
	} else
		return IR_COND_UNKNOWN;
}

if (isSgEqualityOp(op2))
	return IR_COND_EQ;
else if (isSgNotEqualOp(op2))
	return IR_COND_NE;
else if (isSgLessThanOp(op2))
	return IR_COND_LT;
else if (isSgLessOrEqualOp(op2))
	return IR_COND_LE;
else if (isSgGreaterThanOp(op2))
	return IR_COND_GT;
else if (isSgGreaterOrEqualOp(op2))
	return IR_COND_GE;
else if (isSgAndOp(op2))
	return IR_COND_AND;

return IR_COND_UNKNOWN;

}

std::vector<omega::CG_outputRepr *> IR_roseCode::QueryExpOperand(
	const omega::CG_outputRepr *repr) const {
std::vector<omega::CG_outputRepr *> v;
SgExpression* op1;
SgExpression* op2;
SgExpression* op =
		static_cast<const omega::CG_roseRepr *>(repr)->GetExpression();
omega::CG_roseRepr *repr1;

if (isSgValueExp(op) || isSgVarRefExp(op)) {
	omega::CG_roseRepr *repr = new omega::CG_roseRepr(op);
	v.push_back(repr);
} else if (isSgAssignOp(op)) {
	op1 = isSgAssignOp(op)->get_rhs_operand();
	repr1 = new omega::CG_roseRepr(op1);
	v.push_back(repr1);
	/*may be a problem as assignOp is a binaryop destop might be needed */
} else if (isSgMinusOp(op)) {
	op1 = isSgMinusOp(op)->get_operand();
	repr1 = new omega::CG_roseRepr(op1);
	v.push_back(repr1);
} else if (isSgUnaryAddOp(op)) {
	op1 = isSgUnaryAddOp(op)->get_operand();
	repr1 = new omega::CG_roseRepr(op1);
	v.push_back(repr1);
} else if ((isSgAddOp(op) || isSgSubtractOp(op))
		|| (isSgMultiplyOp(op) || isSgDivideOp(op))) {
	op1 = isSgBinaryOp(op)->get_lhs_operand();
	repr1 = new omega::CG_roseRepr(op1);
	v.push_back(repr1);

	op2 = isSgBinaryOp(op)->get_rhs_operand();
	repr1 = new omega::CG_roseRepr(op2);
	v.push_back(repr1);
} else if (isSgConditionalExp(op)) {
	SgExpression* cond = isSgConditionalExp(op)->get_conditional_exp();
	op1 = isSgBinaryOp(cond)->get_lhs_operand();
	repr1 = new omega::CG_roseRepr(op1);
	v.push_back(repr1);

	op2 = isSgBinaryOp(cond)->get_rhs_operand();
	repr1 = new omega::CG_roseRepr(op2);
	v.push_back(repr1);
} else if (isSgCompoundAssignOp(op)) {
	SgExpression* cond = isSgCompoundAssignOp(op);
	op1 = isSgBinaryOp(cond)->get_lhs_operand();
	repr1 = new omega::CG_roseRepr(op1);
	v.push_back(repr1);

	op2 = isSgBinaryOp(cond)->get_rhs_operand();
	repr1 = new omega::CG_roseRepr(op2);
	v.push_back(repr1);

} else if (isSgPntrArrRefExp(op)) {
	omega::CG_roseRepr *repr = new omega::CG_roseRepr(op);
	v.push_back(repr);
} else if (isSgBinaryOp(op)) {

	op1 = isSgBinaryOp(op)->get_lhs_operand();
	repr1 = new omega::CG_roseRepr(op1);
	v.push_back(repr1);

	op2 = isSgBinaryOp(op)->get_rhs_operand();
	repr1 = new omega::CG_roseRepr(op2);
	v.push_back(repr1);
} else if (isSgCallExpression(op)) {
	op1 = isSgCallExpression(op)->get_function();
	repr1 = new omega::CG_roseRepr(op1);
	v.push_back(repr1);
} else
	throw ir_error("operation not supported");

return v;
}

IR_Ref *IR_roseCode::Repr2Ref(const omega::CG_outputRepr *repr) const {
SgExpression* op =
		static_cast<const omega::CG_roseRepr *>(repr)->GetExpression();

if (SgValueExp* im = isSgValueExp(op)) {
	if (isSgIntVal(im))
		return new IR_roseConstantRef(this,
				static_cast<omega::coef_t>(isSgIntVal(im)->get_value()));
	else if (isSgUnsignedIntVal(im))
		return new IR_roseConstantRef(this,
				static_cast<omega::coef_t>(isSgUnsignedIntVal(im)->get_value()));
	else if (isSgLongIntVal(im))
		return new IR_roseConstantRef(this,
				static_cast<omega::coef_t>(isSgLongIntVal(im)->get_value()));
	else if (isSgFloatVal(im))
		return new IR_roseConstantRef(this, isSgFloatVal(im)->get_value());
	else
		assert(0);

} else if (isSgVarRefExp(op))
	return new IR_roseScalarRef(this, isSgVarRefExp(op));
else if (isSgFunctionRefExp(op))
	return new IR_roseFunctionRef(this, isSgFunctionRefExp(op));
else if (isSgPntrArrRefExp(op)) {
	SgAssignOp *assignment;
	int write = 0;
	if (assignment = isSgAssignOp(isSgNode(op)->get_parent())) {
		if (assignment->get_lhs_operand() == op)
			write = 1;
	} else if (SgExprStatement* expr_stmt = isSgExprStatement(
			isSgNode(op)->get_parent())) {
		SgExpression* exp = expr_stmt->get_expression();

		if (exp) {
			if (assignment = isSgAssignOp(exp)) {
				if (assignment->get_lhs_operand() == op)
					write = 1;

			}
		}

	}

	SgExpression *tmp = isSgPntrArrRefExp(op);
	int dim = 0;
	while (isSgPntrArrRefExp(tmp)) {
		tmp = isSgPntrArrRefExp(tmp)->get_lhs_operand();
		dim++;
	}

	if (SgVarRefExp *var_ref = isSgVarRefExp(tmp)) {
		if (isSgPointerType(var_ref->get_symbol()->get_type()))
			return new IR_rosePointerArrayRef(this, isSgPntrArrRefExp(op),
					write,
					new IR_rosePointerSymbol(this, var_ref->get_symbol(), dim));
	}

	return new IR_roseArrayRef(this, isSgPntrArrRefExp(op), write);
} else
	assert(0);

}

