/*=============================================================================
    Copyright (c) 2001-2009 Joel de Guzman

    Distributed under the Boost Software License, Version 1.0. (See accompanying
    file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
=============================================================================*/
///////////////////////////////////////////////////////////////////////////////
//
//  Yet another calculator example! This time, we will compile to a simple
//  virtual machine. This is actually one of the very first Spirit example
//  circa 2000. Now, it's ported to Spirit2.
//
//  [ JDG Sometime 2000 ]       pre-boost
//  [ JDG September 18, 2002 ]  spirit1
//  [ JDG April 8, 2007 ]       spirit2
//
///////////////////////////////////////////////////////////////////////////////

#include <boost/config/warning_disable.hpp>
#include <boost/spirit/include/qi.hpp>
#include <boost/spirit/include/phoenix_core.hpp>
#include <boost/spirit/include/phoenix_container.hpp>
#include <boost/spirit/include/phoenix_statement.hpp>
#include <boost/spirit/include/phoenix_object.hpp>
#include <boost/spirit/include/phoenix_operator.hpp>

#include <iostream>
#include <string>
#include <vector>
#include <codegen.h>
#include "loop.hh"
#include "ir_code.hh"
#include "chill_error.hh"


using namespace omega

namespace client
{
    namespace qi = boost::spirit::qi;
    namespace phoenix = boost::phoenix;
    namespace ascii = boost::spirit::ascii;

    ///////////////////////////////////////////////////////////////////////////
    //  The Virtual Machine
    ///////////////////////////////////////////////////////////////////////////
    enum byte_code
    {
        op_neg,     //  negate the top stack entry
        op_add,     //  add top two stack entries
        op_sub,     //  subtract top two stack entries
        op_mul,     //  multiply top two stack entries
        op_div,     //  divide top two stack entries
        op_int,     //  push constant integer into the stack
    };
   

         

struct createIdentifier
{
public:
    
    createIdentifier(std::vector<std::CG_outputRepr *> &code, std::string & string_ref, CG_outputBuilder &builder) :m_code(code), global_string(string_ref){}
    template <typename IteratorT>
    void operator()( IteratorT, IteratorT ) const
    {
       
    }
    template <typename IteratorT>
    void operator()( IteratorT ) const
    {
       
    }
 
    //template <typename IteratorT>
    void operator()(qi::unused_type,qi::unused_type, qi::unused_type ) const
    { 
        m_code.push_back(builder.CreateIdent(global_string));  
        m_code.push_back(global_string);
        global_string.clear();
        //std::cout<<"1"<<std::endl; 
    }

    //template <typename IteratorT>
    void operator()(char &s ,qi::unused_type, qi::unused_type ) const
    { 
        global_string += s;
       // std::cout<<"2"<<std::endl; 
        //std::cout<< global_string<< std::endl;
    }

    //template <typename IteratorT>
    void operator()(unsigned int &s ,qi::unused_type, qi::unused_type ) const
    { 
       
        
        m_code.push_back(builder.CreateInt((int)s));
       // std::cout<<"2"<<std::endl; 
        //std::cout<< global_string<< std::endl;
    }

private:
   std::vector<CG_outputRepr *> &m_code;
   CG_outputBuilder &builder; 
   std::string &global_string ;
};  
  /*  class vmachine
    {
    public:

        vmachine(unsigned stackSize = 4096)
          : stack(stackSize)
          , stack_ptr(stack.begin())
        {
        }

        int top() const { return stack_ptr[-1]; };
        void execute(std::vector<int> const& code);

    private:

        std::vector<int> stack;
        std::vector<int>::iterator stack_ptr;
    };

    void vmachine::execute(std::vector<int> const& code)
    {
        std::vector<int>::const_iterator pc = code.begin();
        stack_ptr = stack.begin();

        while (pc != code.end())
        {
            switch (*pc++)
            {
                case op_neg:
                    stack_ptr[-1] = -stack_ptr[-1];
                    break;

                case op_add:
                    --stack_ptr;
                    stack_ptr[-1] += stack_ptr[0];
                    break;

                case op_sub:
                    --stack_ptr;
                    stack_ptr[-1] -= stack_ptr[0];
                    break;

                case op_mul:
                    --stack_ptr;
                    stack_ptr[-1] *= stack_ptr[0];
                    break;

                case op_div:
                    --stack_ptr;
                    stack_ptr[-1] /= stack_ptr[0];
                    break;

                case op_int:
                    *stack_ptr++ = *pc++;
                    break;
            }
        }
    }
   */

    ///////////////////////////////////////////////////////////////////////////
    //  Our calculator grammar and compiler
    ///////////////////////////////////////////////////////////////////////////
    template <typename Iterator>
    struct calculator : qi::grammar<Iterator, ascii::space_type>
    {
        calculator(std::vector<CG_outputRepr *>& code, CG_outputBuilder &builder)
          : calculator::base_type(expression)
          , code(code)
        {
            using namespace qi::labels;
            using qi::uint_;
            using ascii::char_;
            using qi::on_error;
            using qi::fail;

            using phoenix::val;
            using phoenix::ref;
            using phoenix::push_back;
            using phoenix::construct;

          /*  expression =
                term
                >> *(   ('+' > term             [push_back(ref(code), op_add)])
                    |   ('-' > term             [push_back(ref(code), op_sub)])
                    )
                ;

            term =
                factor
                >> *(   ('*' > factor           [push_back(ref(code), op_mul)])
                    |   ('/' > factor           [push_back(ref(code), op_div)])
                    )
                ;

            factor =
                uint_                           [
                                                    push_back(ref(code), op_int),
                                                    push_back(ref(code), _1)
                                                ]
                |   '(' > expression > ')'
                |   ('-' > factor               [push_back(ref(code), op_neg)])
                |   ('+' > factor)
                ;

           */

            expression =
                term
                >> *(   ('+' >> term            )
                    |   ('-' >> term             )
                    )
                ;

            term =
                factor
                >> *(   ('*' >> factor         )
                    |   ('/' >> factor         )
                    )
                ;

            factor =
                    identifier    [client::createIdentifier(code, global_string,builder)] 
                  >> -(('(' >> expression >> ')') >> -( '[' >> uint_[client::createIdentifier(code, global_string,builder)] >>  ']') )
                |   uint_                         
                |   '(' >> expression >> ')'
                |   ('-' >> factor )
                |   ('+' >> factor)
                ;

            identifier = char_("a-zA-Z") [client::createIdentifier(code, global_string,builder)]  
                         >> *(char_("_a-zA-Z0-9")[client::createIdentifier(code, global_string,builder)])  
                        ;     

            expression.name("expression");
            term.name("term");
            factor.name("factor");
            identifier.name("identifier");
            on_error<fail>
            (
                expression
              , std::cout
                    << val("Error! Expecting ")
                    << _4                               // what failed?
                    << val(" here: \"")
                    << construct<std::string>(_3, _2)   // iterators to error-pos, end
                    << val("\"")
                    << std::endl
            );
        }

        qi::rule<Iterator, ascii::space_type> expression, term, factor,identifier;
        std::vector<CG_outputRepr *>& code;
        std::string global_string;

    };

    template <typename Grammar>
    bool compile(Grammar const& calc, std::string const& expr)
    {
        std::string::const_iterator iter = expr.begin();
        std::string::const_iterator end = expr.end();
        bool r = phrase_parse(iter, end, calc, ascii::space);

        if (r && iter == end)
        {
            std::cout << "-------------------------\n";
            std::cout << "Parsing succeeded\n";
            std::cout << "-------------------------\n";
            return true;
        }
        else
        {
            std::cout << "-------------------------\n";
            std::cout << "Parsing failed\n";
            std::cout << "-------------------------\n";
            return false;
        }
    }
}

///////////////////////////////////////////////////////////////////////////////
//  Main program
///////////////////////////////////////////////////////////////////////////////
CG_outputRepr * Loop::iegen_parser(std::string str)
{
    std::cout << "/////////////////////////////////////////////////////////\n\n";
    std::cout << "Expression parser...\n\n";
    std::cout << "/////////////////////////////////////////////////////////\n\n";
    std::cout << "Type an expression...or [q or Q] to quit\n\n";

    typedef std::string::const_iterator iterator_type;
    typedef client::calculator<iterator_type> calculator;

//    client::vmachine mach;          //  Our virtual machine
    std::vector<CG_outputRepr *> code;          //  Our VM code
    calculator calc(code, *(ir->builder()));          //  Our grammar

   

        //code.clear();
        if (client::compile(calc, str))
        {
       /*     mach.execute(code);
            std::cout << "\n\nresult = " << mach.top() << std::endl;
            std::cout << "-------------------------\n\n";
        */
           return code[0];

        }
       else
          throw loop_error("iegen string parsing failed");
   

    return 0;
}
