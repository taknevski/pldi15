#ifndef LOOP_HH
#define LOOP_HH
#define _DEBUG_ 0
#include <omega.h>
#include <codegen.h>
#include <code_gen/CG.h>
#include <vector>
#include <map>
#include <set>
#include "dep.hh"
#include "ir_code.hh"
#include "irtools.hh"
#include <code_gen/CG_stringBuilder.h>
#include "wavefront_specializer.hh"
//#include "wavefront_intel_specializer.hh"
class IR_Code;

enum TilingMethodType { StridedTile, CountedTile };
enum LoopLevelType { LoopLevelOriginal, LoopLevelTile, LoopLevelUnknown };
enum BarrierType {Barrier, P2P, DOACROSS};

// Describes properties of each loop level of a statement. "payload"
// for LoopLevelOriginal means iteration space dimension, for
// LoopLevelTile means tiled loop level.  Special value -1 for
// LoopLevelTile means purely derived loop. For dependence dimension
// payloads, the values must be in an increasing order.
// "parallel_level" will be used by code generation to support
// multi-level parallelization (default 0 means sequential loop under
// the current parallelization level).
struct LoopLevel {
  LoopLevelType type;
  int payload;  
  int parallel_level;
  bool segreducible;
  std::string segment_descriptor;
  bool made_dense;  		
};

struct Statement {
  omega::CG_outputRepr *code;

  omega::Relation IS;
  omega::Relation xform;
  std::vector<LoopLevel> loop_level;
  ir_tree_node *ir_stmt_node;
  bool has_inspector;
  bool has_wavefront_inspector; //additional flag for I/E approach of wavefronts  
  int reduction; // Manu:: 0 == reduction not possible, 1 == reduction possible, 2 == reduction with some processing
  IR_OPERATION_TYPE reductionOp; // Manu
		//protonu--temporarily putting this back here
  //omega::Tuple<int> nonSplitLevels;
		//end--protonu.
};


class Loop {
protected:
  int tmp_loop_var_name_counter;
  static const std::string tmp_loop_var_name_prefix;
  int overflow_var_name_counter;
  static const std::string overflow_var_name_prefix;
  std::vector<int> stmt_nesting_level_;
  std::vector<std::string> index;
  std::map<int, omega::CG_outputRepr *> replace;
  std::map<int, std::pair<int, std::string> > reduced_statements;
  std::vector<std::vector<std::string> > vars_to_be_reversed;
  bool wavefront_set;

public:
  IR_Code *ir;
  wavefront_dep_graph *wfdg;
  wavefront_schedule *wfsc;
  std::vector<IR_PointerSymbol *> ptr_variables;
  std::vector<IR_PointerSymbol *> ptr_variables_non_const;
  std::vector<omega::Free_Var_Decl*> freevar;
  std::vector<Statement> stmt;
  std::vector<omega::CG_outputRepr*> actual_code;
  std::vector<ir_tree_node *> ir_stmt;
  std::vector<ir_tree_node *> ir_tree;
  std::set<std::string> reduced_write_refs;
  std::map<std::string, int> array_dims;
  DependenceGraph dep;
  std::vector<omega::Relation> dep_relation;
  int num_dep_dim;
  omega::Relation known;
  omega::CG_outputRepr *init_code;
  omega::CG_outputRepr *cleanup_code;
  std::map<std::string, std::set<std::string > > unin_symbol_args;
  std::map<std::string, std::string > unin_symbol_for_iegen;
  std::vector<std::pair<std::string, std::string > > dep_rel_for_iegen;
  //Added by Anand 02/26/2016
  //Data Structure to track permutation routines in the context of
  //sparse computations eg. i' = sigma(i), sigma is a permutation
  //represented by a uninterpreted function symbol internally

  std::vector<std::pair<std::string, std::string > >permutation_symbols;


  std::map<int, std::vector<omega::Free_Var_Decl *> > overflow;
  std::map<std::string, std::vector<omega::EQ_Handle > > index_variables;
  std::vector<std::map<std::string, std::vector<omega::CG_outputRepr * > > > uninterpreted_symbols;
  std::vector<std::map<std::string, std::vector<omega::CG_outputRepr * > > >uninterpreted_symbols_stringrepr;
  std::vector<std::map<std::string, std::vector<omega::Relation > > >unin_rel;

  //Protonu--adding these to generate OMP parallel regions

  BarrierType use_omp_barrier;
  int loop_for_omp_parallel_region;
  std::map<int, std::vector<int> > omp_threads;
  std::map<int, std::vector<std::string> > omp_prv;
  std::map<int, std::pair< std::vector<int>,std::string > > pragmas;
  std::map<int, std::vector<int> > omp_syncs;
  std::map<int, int> loops_for_omp_parallel_region;
  int omp_thrds_to_use;
  int omp_parallel_for;
  int barrier_level;
  //end.

protected:
  mutable omega::CodeGen *last_compute_cg_;
  mutable omega::CG_result *last_compute_cgr_;
  mutable int last_compute_effort_;
  bool is_single;//for compact 	
protected:
  bool init_loop(std::vector<ir_tree_node *> &ir_tree, std::vector<ir_tree_node *> &ir_stmt);
  int get_dep_dim_of(int stmt, int level) const;
  int get_last_dep_dim_before(int stmt, int level) const;
  std::vector<omega::Relation> getNewIS() const;
  omega::Relation getNewIS(int stmt_num) const;
  std::vector<int> getLexicalOrder(int stmt_num) const;
  int getLexicalOrder(int stmt_num, int level) const;
  std::set<int> getStatements(const std::vector<int> &lex, int dim) const;
  void shiftLexicalOrder(const std::vector<int> &lex, int dim, int amount);
  void setLexicalOrder(int dim, const std::set<int> &active, int starting_order = 0, std::vector< std::vector<std::string> >idxNames= std::vector< std::vector<std::string> >());
  void apply_xform(int stmt_num);
  void apply_xform(std::set<int> &active);
  void apply_xform();

  std::set<int> getSubLoopNest(int stmt_num, int level) const;
  int  getMinLexValue(std::set<int> stmts, int level);

public:
  Loop() { ir = NULL; tmp_loop_var_name_counter = 1; init_code = NULL; }
  Loop(const IR_Control *control);
   ~Loop();
  
  omega::CG_outputRepr *getCode(int effort = 1		) const;
  void printCode(int effort = 1) const;
  void addKnown(const omega::Relation &cond);
  void print_internal_loop_structure() const;
  bool isInitialized() const;
  int num_statement() const { return stmt.size(); }
  void printIterationSpace() const;
  void printDependenceGraph() const;
  void removeDependence(int stmt_num_from, int stmt_num_to);

  void dump() const;

  std::vector<std::set <int > > sort_by_same_loops(std::set<int > active, int level);
  //
  // legacy unimodular transformations for perfectly nested loops
  // e.g. M*(i,j)^T = (i',j')^T or M*(i,j,1)^T = (i',j')^T
  //
  bool nonsingular(const std::vector<std::vector<int> > &M);
  
  //
  // high-level loop transformations
  //
  void permute(const std::set<int> &active, const std::vector<int> &pi);
  void permute(int stmt_num, int level, const std::vector<int> &pi);
  void permute(const std::vector<int> &pi);
  void original();
  
  void tile(int stmt_num, int level, int tile_size, int outer_level = 1, TilingMethodType method = StridedTile, int alignment_offset = 0, int alignment_multiple = 1);
  std::set<int> split(int stmt_num, int level, const omega::Relation &cond);
  std::set<int> unroll(int stmt_num, int level, int unroll_amount, std::vector< std::vector<std::string> >idxNames= std::vector< std::vector<std::string> >(), int cleanup_split_level = 0);
  
  bool datacopy(const std::vector<std::pair<int, std::vector<int> > > &array_ref_nums, int level, bool allow_extra_read = false, int fastest_changing_dimension = -1, int padding_stride = 1, int padding_alignment = 4, int memory_type = 0);
  bool datacopy(int stmt_num, int level, const std::string &array_name, bool allow_extra_read = false, int fastest_changing_dimension = -1, int padding_stride = 1, int padding_alignment = 4, int memory_type = 0);
  bool datacopy_privatized(int stmt_num, int level, const std::string &array_name, const std::vector<int> &privatized_levels, bool allow_extra_read = false, int fastest_changing_dimension = -1, int padding_stride = 1, int padding_alignment = 1, int memory_type = 0);
  bool datacopy_privatized(const std::vector<std::pair<int, std::vector<int> > > &array_ref_nums, int level, const std::vector<int> &privatized_levels, bool allow_extra_read = false, int fastest_changing_dimension = -1, int padding_stride = 1, int padding_alignment = 1, int memory_type = 0);
  bool datacopy_privatized(const std::vector<std::pair<int, std::vector<IR_ArrayRef *> > > &stmt_refs, int level, const std::vector<int> &privatized_levels, bool allow_extra_read, int fastest_changing_dimension, int padding_stride, int padding_alignment, int memory_type = 0);
  //std::set<int> scalar_replacement_inner(int stmt_num);
  CG_outputRepr* inspection_code() const;


  Graph<std::set<int>, bool> construct_induced_graph_at_level(std::vector<std::set<int> > s, DependenceGraph dep, int dep_dim);
  std::vector<std::set<int> > typed_fusion(Graph<std::set<int>, bool> g, std::vector<bool> &types);

  void fuse(const std::set<int> &stmt_nums, int level);
  void distribute(const std::set<int> &stmt_nums, int level);
  void skew(const std::set<int> &stmt_nums, int level, const std::vector<int> &skew_amount);
  void shift(const std::set<int> &stmt_nums, int level, int shift_amount);
  void scale(const std::set<int> &stmt_nums, int level, int scale_amount);
  void reverse(const std::set<int> &stmt_nums, int level);
  void peel(int stmt_num, int level, int peel_amount = 1);
  //
  // more fancy loop transformations
  //

  omega::Relation parseExpWithWhileToRel(omega::CG_outputRepr *repr, omega::Relation &R, int loc);

  void modular_shift(int stmt_num, int level, int shift_amount) {}
  void diagonal_map(int stmt_num, const std::pair<int, int> &levels, int offset) {}
  void modular_partition(int stmt_num, int level, int stride) {}
  void sparse_partition(int stmt_num, int level, std::string name,  bool suppress_negative = false);
  void sparse_wavefront(int stmt_num, int level, bool parallel_threaded_execution );
  void reorder_by_inspector(int stmt_num, int level, std::string perm="", std::string dep="", std::string dep_graph_constructor_name="", std::string perm_func_name="");
  void reorder_data(int stmt_num, std::string perm_name, std::string inv_perm, std::vector<std::string> arrays);
  void flatten(int stmt_num, std::string index_name, std::vector<int> &loop_levels, std::string inspector_name);
  void normalize(int stmt_num,  int loop_level);
  //
  // derived loop transformations
  //
  void shift_to(int stmt_num, int level, int absolute_position);
  std::set<int> unroll_extra(int stmt_num, int level, int unroll_amount, int cleanup_split_level = 0);
  bool is_dependence_valid_based_on_lex_order(int i, int j,
			const DependenceVector &dv, bool before);
  void split_with_alignment(int stmt_num, int level, int alignment,
  		int direction=0);

  // Manu:: reduction operation
  void reduce(int stmt_num, std::vector<int> &level, int param, std::string func_name, std::vector<int> &seq_levels, std::vector<int> cudaized_levels = std::vector<int>(), int bound_level = -1);
  void scalar_expand(int stmt_num, const std::vector<int> &levels, std::string arrName, int memory_type =0, int padding_alignment=0, int assign_then_accumulate = 1, int padding_stride = 0);
  void ELLify(int stmt_num, std::vector<std::string> arrays_to_pad, int pad_to, bool dense_pad = false, std::string dense_pad_pos_array = "");
  void compact(int stmt_num, std::vector<int> level, std::vector<std::string > new_array, int zero,
			std::vector< std::string >  data_array ,  omega::Relation R= omega::Relation::True(1), bool max_allocate=false,  std::vector<int>additional_compacted = std::vector<int>());
  void make_dense(int stmt_num, int loop_level, std::string new_loop_index);
  void set_array_size(std::string name, int size );
  omega::CG_outputRepr * iegen_parser(std::string &str, std::vector<std::string> &index_names);

	//Protonu--adding functions to generate OpenMP code
	bool _map_loop_to_openmp_shared_region(int loop_num /* should add a vector of string for shared */);
	std::vector<int> omp_loop_numbers;

	//better version of OMP code, with an OMP parallel region
	void mark_omp_parallel_region(int stmt, int use_barrier, int level_for_barrier);
	void mark_omp_threads(int , std::vector<int> );
	void mark_omp_syncs(int, std::vector<int>);
	void mark_pragma(int, int, std::string);
	void mark_omp_private(int, std::vector< std::string > );
        omega::CG_outputRepr *add_pragma (omega::CG_outputRepr *repr, int, int , std::string) const;
	omega::CG_outputRepr *add_annote (omega::CG_outputRepr *repr, int, int , omega::CG_outputRepr *repr2, bool firstScope = true) const;
	omega::CG_outputRepr *add_annote (omega::CG_outputRepr *repr, int, int , std::string repr2, bool firstScope = true) const;
	bool generate_omp_parallel_region(int use_barrier , int num_threads = 0);
    omega::CG_outputRepr *add_omp_thread_info (omega::CG_outputRepr *)const;


	//Protonu--Adding more OpenMP code generation
	//OMP parallel-for split into #omp parallel and #parallel for
	void omp_par_for(int loop_outer, int loop_inner, int num_thrds);
	omega::CG_outputRepr *add_omp_parallel_for(omega::CG_outputRepr *repr)const;
	omega::CG_outputRepr *add_omp_for_recursive(omega::CG_outputRepr *repr, int a, int b, int num_thrds=0, std::vector<std::string> prv = std::vector<std::string>())const;
	void scrape_loop_indices(omega::CG_outputRepr *repr)const;
	std::set<const char *> omp_thrd_private;


};




#endif
