#ifndef CHILL_ENV_H
#define CHILL_ENV_H

typedef struct lua_State lua_State;

void register_globals(lua_State *L);
void register_functions(lua_State *L);
int get_loop_num(lua_State *L);
#endif
