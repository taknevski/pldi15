/* A Bison parser, made by GNU Bison 2.5.  */

/* Bison implementation for Yacc-like parsers in C
   
      Copyright (C) 1984, 1989-1990, 2000-2011 Free Software Foundation, Inc.
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.
   
   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output.  */
#define YYBISON 1

/* Bison version.  */
#define YYBISON_VERSION "2.5"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 0

/* Push parsers.  */
#define YYPUSH 0

/* Pull parsers.  */
#define YYPULL 1

/* Using locations.  */
#define YYLSP_NEEDED 0

/* Substitute the variable and function names.  */
#define yyparse         zzparse
#define yylex           zzlex
#define yyerror         zzerror
#define yylval          zzlval
#define yychar          zzchar
#define yydebug         zzdebug
#define yynerrs         zznerrs


/* Copy the first part of user declarations.  */

/* Line 268 of yacc.c  */
#line 15 "parser.yy"

#include <stdio.h>
#include <math.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <map>
#include <set>
#include <string>
#include "parser.hh"


#include "parser.tab.hh"

#include <omega.h>
#include "ir_code.hh"
#include "loop.hh"

#include "ir_rose.hh"

using namespace omega;
  
extern int yydebug;

void yyerror(const char *);
int yylex();  
extern int yylex(); //{ return lexer.yylex();}
namespace {
  enum COMPILER_IR_TYPE {CIT_NULL, CIT_ROSE};
  char *source_filename = NULL;
  COMPILER_IR_TYPE cit_name = CIT_NULL;
  char* procedure_name = NULL;
   
  int loop_num_start, loop_num_end;
  Loop *myloop = NULL;
}

#define PRINT_ERR_LINENO {if (is_interactive) fprintf(stderr, "\n"); else fprintf(stderr, " at line %d\n", lexer.lineno()-1);}

std::map<std::string, int> parameter_tab;
bool is_interactive;
const char *PROMPT_STRING = ">>>";

IR_Code *ir_code = NULL;
std::vector<IR_Control *> ir_controls;
std::vector<int> loops;


/* Line 268 of yacc.c  */
#line 128 "parser.tab.cc"

/* Enabling traces.  */
#ifndef YYDEBUG
# define YYDEBUG 1
#endif

/* Enabling verbose error messages.  */
#ifdef YYERROR_VERBOSE
# undef YYERROR_VERBOSE
# define YYERROR_VERBOSE 1
#else
# define YYERROR_VERBOSE 0
#endif

/* Enabling the token table.  */
#ifndef YYTOKEN_TABLE
# define YYTOKEN_TABLE 0
#endif


/* Tokens.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
   /* Put the tokens into the symbol table, so that GDB and other debuggers
      know about them.  */
   enum yytokentype {
     NUMBER = 258,
     LEVEL = 259,
     TRUEORFALSE = 260,
     FILENAME = 261,
     PROCEDURENAME = 262,
     VARIABLE = 263,
     FREEVAR = 264,
     SOURCE = 265,
     PROCEDURE = 266,
     FORMAT = 267,
     LOOP = 268,
     PERMUTE = 269,
     ORIGINAL = 270,
     TILE = 271,
     UNROLL = 272,
     SPLIT = 273,
     UNROLL_EXTRA = 274,
     REDUCE = 275,
     SPLIT_WITH_ALIGNMENT = 276,
     DATACOPY = 277,
     DATACOPY_PRIVATIZED = 278,
     FLATTEN = 279,
     SCALAR_EXPAND = 280,
     NORMALIZE = 281,
     ELLIFY = 282,
     COMPACT = 283,
     MAKE_DENSE = 284,
     SET_ARRAY_SIZE = 285,
     SPARSE_PARTITION = 286,
     SPARSE_WAVEFRONT = 287,
     REORDER_BY_INSPECTOR = 288,
     NONSINGULAR = 289,
     EXIT = 290,
     KNOWN = 291,
     SKEW = 292,
     SHIFT = 293,
     SHIFT_TO = 294,
     FUSE = 295,
     DISTRIBUTE = 296,
     REMOVE_DEP = 297,
     SCALE = 298,
     REVERSE = 299,
     PEEL = 300,
     REORDER_DATA = 301,
     STRIDED = 302,
     COUNTED = 303,
     NUM_STATEMENT = 304,
     CEIL = 305,
     FLOOR = 306,
     PRINT = 307,
     PRINT_CODE = 308,
     PRINT_DEP = 309,
     PRINT_IS = 310,
     PRINT_STRUCTURE = 311,
     NE = 312,
     LE = 313,
     GE = 314,
     EQ = 315,
     MAP_TO_OPENMP_REGION = 316,
     MARK_OMP_PARL_REGION = 317,
     MARK_OMP_THRDS = 318,
     MARK_OMP_SYNC = 319,
     GEN_OMP_PARL_REGION = 320,
     OMP_PAR_FOR = 321,
     MARK_PRAGMA = 322,
     UMINUS = 323
   };
#endif



#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
typedef union YYSTYPE
{

/* Line 293 of yacc.c  */
#line 64 "parser.yy"

  int val;
  float fval;
  bool bool_val;
  char *name;
  std::vector<int> *vec;
  std::vector<std::string> *string_vec;
  std::vector<std::vector<int> > *mat;
  std::map<std::string, int> *tab;
  std::vector<std::map<std::string, int> > *tab_lst;
  std::pair<std::vector<std::map<std::string, int> >, std::map<std::string, int> > *eq_term_pair;



/* Line 293 of yacc.c  */
#line 247 "parser.tab.cc"
} YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define yystype YYSTYPE /* obsolescent; will be withdrawn */
# define YYSTYPE_IS_DECLARED 1
#endif


/* Copy the second part of user declarations.  */


/* Line 343 of yacc.c  */
#line 259 "parser.tab.cc"

#ifdef short
# undef short
#endif

#ifdef YYTYPE_UINT8
typedef YYTYPE_UINT8 yytype_uint8;
#else
typedef unsigned char yytype_uint8;
#endif

#ifdef YYTYPE_INT8
typedef YYTYPE_INT8 yytype_int8;
#elif (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
typedef signed char yytype_int8;
#else
typedef short int yytype_int8;
#endif

#ifdef YYTYPE_UINT16
typedef YYTYPE_UINT16 yytype_uint16;
#else
typedef unsigned short int yytype_uint16;
#endif

#ifdef YYTYPE_INT16
typedef YYTYPE_INT16 yytype_int16;
#else
typedef short int yytype_int16;
#endif

#ifndef YYSIZE_T
# ifdef __SIZE_TYPE__
#  define YYSIZE_T __SIZE_TYPE__
# elif defined size_t
#  define YYSIZE_T size_t
# elif ! defined YYSIZE_T && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# else
#  define YYSIZE_T unsigned int
# endif
#endif

#define YYSIZE_MAXIMUM ((YYSIZE_T) -1)

#ifndef YY_
# if defined YYENABLE_NLS && YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(msgid) dgettext ("bison-runtime", msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(msgid) msgid
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YYUSE(e) ((void) (e))
#else
# define YYUSE(e) /* empty */
#endif

/* Identity function, used to suppress warnings about constant conditions.  */
#ifndef lint
# define YYID(n) (n)
#else
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static int
YYID (int yyi)
#else
static int
YYID (yyi)
    int yyi;
#endif
{
  return yyi;
}
#endif

#if ! defined yyoverflow || YYERROR_VERBOSE

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   elif defined __BUILTIN_VA_ARG_INCR
#    include <alloca.h> /* INFRINGES ON USER NAME SPACE */
#   elif defined _AIX
#    define YYSTACK_ALLOC __alloca
#   elif defined _MSC_VER
#    include <malloc.h> /* INFRINGES ON USER NAME SPACE */
#    define alloca _alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if ! defined _ALLOCA_H && ! defined EXIT_SUCCESS && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#     ifndef EXIT_SUCCESS
#      define EXIT_SUCCESS 0
#     endif
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's `empty if-body' warning.  */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (YYID (0))
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM YYSIZE_MAXIMUM
#  endif
#  if (defined __cplusplus && ! defined EXIT_SUCCESS \
       && ! ((defined YYMALLOC || defined malloc) \
	     && (defined YYFREE || defined free)))
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   ifndef EXIT_SUCCESS
#    define EXIT_SUCCESS 0
#   endif
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if ! defined malloc && ! defined EXIT_SUCCESS && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if ! defined free && ! defined EXIT_SUCCESS && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
# endif
#endif /* ! defined yyoverflow || YYERROR_VERBOSE */


#if (! defined yyoverflow \
     && (! defined __cplusplus \
	 || (defined YYSTYPE_IS_TRIVIAL && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  yytype_int16 yyss_alloc;
  YYSTYPE yyvs_alloc;
};

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (sizeof (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (sizeof (yytype_int16) + sizeof (YYSTYPE)) \
      + YYSTACK_GAP_MAXIMUM)

# define YYCOPY_NEEDED 1

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack_alloc, Stack)				\
    do									\
      {									\
	YYSIZE_T yynewbytes;						\
	YYCOPY (&yyptr->Stack_alloc, Stack, yysize);			\
	Stack = &yyptr->Stack_alloc;					\
	yynewbytes = yystacksize * sizeof (*Stack) + YYSTACK_GAP_MAXIMUM; \
	yyptr += yynewbytes / sizeof (*yyptr);				\
      }									\
    while (YYID (0))

#endif

#if defined YYCOPY_NEEDED && YYCOPY_NEEDED
/* Copy COUNT objects from FROM to TO.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined __GNUC__ && 1 < __GNUC__
#   define YYCOPY(To, From, Count) \
      __builtin_memcpy (To, From, (Count) * sizeof (*(From)))
#  else
#   define YYCOPY(To, From, Count)		\
      do					\
	{					\
	  YYSIZE_T yyi;				\
	  for (yyi = 0; yyi < (Count); yyi++)	\
	    (To)[yyi] = (From)[yyi];		\
	}					\
      while (YYID (0))
#  endif
# endif
#endif /* !YYCOPY_NEEDED */

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL  2
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   825

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS  84
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS  13
/* YYNRULES -- Number of rules.  */
#define YYNRULES  130
/* YYNRULES -- Number of states.  */
#define YYNSTATES  601

/* YYTRANSLATE(YYLEX) -- Bison symbol number corresponding to YYLEX.  */
#define YYUNDEFTOK  2
#define YYMAXUTOK   323

#define YYTRANSLATE(YYX)						\
  ((unsigned int) (YYX) <= YYMAXUTOK ? yytranslate[YYX] : YYUNDEFTOK)

/* YYTRANSLATE[YYLEX] -- Bison symbol number corresponding to YYLEX.  */
static const yytype_uint8 yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
      81,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,    74,     2,     2,
      79,    80,    72,    70,    78,    71,     2,    73,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,    82,     2,
      69,    83,    68,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,    76,     2,    77,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    40,    41,    42,    43,    44,
      45,    46,    47,    48,    49,    50,    51,    52,    53,    54,
      55,    56,    57,    58,    59,    60,    61,    62,    63,    64,
      65,    66,    67,    75
};

#if YYDEBUG
/* YYPRHS[YYN] -- Index of the first RHS symbol of rule number YYN in
   YYRHS.  */
static const yytype_uint16 yyprhs[] =
{
       0,     0,     3,     4,     7,    11,    12,    14,    18,    22,
      23,    25,    29,    33,    35,    39,    41,    43,    47,    52,
      57,    61,    65,    69,    73,    77,    80,    82,    84,    88,
      93,    98,   102,   106,   110,   114,   118,   121,   125,   129,
     133,   137,   141,   143,   145,   147,   151,   155,   159,   163,
     166,   168,   171,   176,   181,   186,   191,   196,   203,   206,
     210,   214,   218,   222,   226,   229,   234,   240,   248,   253,
     259,   269,   277,   287,   299,   313,   329,   347,   357,   365,
     375,   389,   397,   409,   423,   439,   457,   471,   487,   505,
     513,   523,   535,   549,   565,   575,   587,   601,   617,   635,
     645,   655,   671,   683,   701,   721,   731,   743,   753,   765,
     775,   781,   791,   801,   809,   819,   829,   839,   847,   855,
     863,   877,   887,   899,   911,   917,   927,   937,   945,   955,
     963
};

/* YYRHS -- A `-1'-separated list of the rules' RHS.  */
static const yytype_int8 yyrhs[] =
{
      85,     0,    -1,    -1,    85,    96,    -1,    76,    87,    77,
      -1,    -1,    92,    -1,    87,    78,    92,    -1,    76,    89,
      77,    -1,    -1,     8,    -1,    89,    78,     8,    -1,    76,
      91,    77,    -1,    86,    -1,    91,    78,    86,    -1,     3,
      -1,     8,    -1,    49,    79,    80,    -1,    50,    79,    93,
      80,    -1,    51,    79,    93,    80,    -1,    79,    92,    80,
      -1,    92,    71,    92,    -1,    92,    70,    92,    -1,    92,
      72,    92,    -1,    92,    73,    92,    -1,    71,    92,    -1,
       3,    -1,     8,    -1,    49,    79,    80,    -1,    50,    79,
      93,    80,    -1,    51,    79,    93,    80,    -1,    79,    93,
      80,    -1,    93,    71,    93,    -1,    93,    70,    93,    -1,
      93,    72,    93,    -1,    93,    73,    93,    -1,    71,    93,
      -1,    95,    59,    95,    -1,    95,    68,    95,    -1,    95,
      58,    95,    -1,    95,    69,    95,    -1,    95,    60,    95,
      -1,     3,    -1,     4,    -1,     8,    -1,    79,    95,    80,
      -1,    95,    71,    95,    -1,    95,    70,    95,    -1,    95,
      72,    95,    -1,    71,    95,    -1,    81,    -1,     1,    81,
      -1,    10,    82,     6,    81,    -1,    11,    82,     8,    81,
      -1,    11,    82,     3,    81,    -1,    12,    82,     6,    81,
      -1,    13,    82,     3,    81,    -1,    13,    82,     3,    71,
       3,    81,    -1,    52,    81,    -1,    52,    53,    81,    -1,
      52,    54,    81,    -1,    52,    55,    81,    -1,    52,    56,
      81,    -1,    52,    92,    81,    -1,    35,    81,    -1,     8,
      83,    92,    81,    -1,    36,    79,    94,    80,    81,    -1,
      42,    79,     3,    78,     3,    80,    81,    -1,    15,    79,
      80,    81,    -1,    14,    79,    86,    80,    81,    -1,    14,
      79,    92,    78,     3,    78,    86,    80,    81,    -1,    14,
      79,    86,    78,    86,    80,    81,    -1,    16,    79,    92,
      78,     3,    78,    92,    80,    81,    -1,    24,    79,     3,
      78,     8,    78,    86,    78,     8,    80,    81,    -1,    28,
      79,     3,    78,    86,    78,    88,    78,     3,    78,    88,
      80,    81,    -1,    28,    79,     3,    78,    86,    78,    88,
      78,     3,    78,    88,    78,    94,    80,    81,    -1,    28,
      79,     3,    78,    86,    78,    88,    78,     3,    78,    88,
      78,    94,    78,     3,    80,    81,    -1,    29,    79,     3,
      78,     3,    78,     8,    80,    81,    -1,    30,    79,     8,
      78,     3,    80,    81,    -1,    27,    79,     3,    78,    88,
      78,     3,    80,    81,    -1,    27,    79,     3,    78,    88,
      78,     3,    78,     5,    78,     8,    80,    81,    -1,    26,
      79,     3,    78,     3,    80,    81,    -1,    16,    79,    92,
      78,     3,    78,    92,    78,     3,    80,    81,    -1,    16,
      79,    92,    78,     3,    78,    92,    78,     3,    78,    47,
      80,    81,    -1,    16,    79,    92,    78,     3,    78,    92,
      78,     3,    78,    47,    78,    92,    80,    81,    -1,    16,
      79,    92,    78,     3,    78,    92,    78,     3,    78,    47,
      78,    92,    78,    92,    80,    81,    -1,    16,    79,    92,
      78,     3,    78,    92,    78,     3,    78,    48,    80,    81,
      -1,    16,    79,    92,    78,     3,    78,    92,    78,     3,
      78,    48,    78,    92,    80,    81,    -1,    16,    79,    92,
      78,     3,    78,    92,    78,     3,    78,    48,    78,    92,
      78,    92,    80,    81,    -1,    22,    79,    90,    78,     3,
      80,    81,    -1,    22,    79,    90,    78,     3,    78,     5,
      80,    81,    -1,    22,    79,    90,    78,     3,    78,     5,
      78,    92,    80,    81,    -1,    22,    79,    90,    78,     3,
      78,     5,    78,    92,    78,    92,    80,    81,    -1,    22,
      79,    90,    78,     3,    78,     5,    78,    92,    78,    92,
      78,    92,    80,    81,    -1,    22,    79,    92,    78,     3,
      78,     8,    80,    81,    -1,    22,    79,    92,    78,     3,
      78,     8,    78,     5,    80,    81,    -1,    22,    79,    92,
      78,     3,    78,     8,    78,     5,    78,    92,    80,    81,
      -1,    22,    79,    92,    78,     3,    78,     8,    78,     5,
      78,    92,    78,    92,    80,    81,    -1,    22,    79,    92,
      78,     3,    78,     8,    78,     5,    78,    92,    78,    92,
      78,    92,    80,    81,    -1,    32,    79,     3,    78,     3,
      78,     5,    80,    81,    -1,    31,    79,     3,    78,     3,
      78,     8,    80,    81,    -1,    33,    79,     3,    78,     3,
      78,     8,    78,     8,    78,     8,    78,     8,    80,    81,
      -1,    46,    79,     3,    78,     8,    78,     8,    78,    88,
      80,    81,    -1,    23,    79,    90,    78,     3,    78,    86,
      78,     5,    78,    92,    78,    92,    78,    92,    80,    81,
      -1,    23,    79,    92,    78,     3,    78,     8,    78,    86,
      78,     5,    78,    92,    78,    92,    78,    92,    80,    81,
      -1,    17,    79,    92,    78,     3,    78,    92,    80,    81,
      -1,    17,    79,    92,    78,     3,    78,    92,    78,    92,
      80,    81,    -1,    19,    79,    92,    78,     3,    78,    92,
      80,    81,    -1,    19,    79,    92,    78,     3,    78,    92,
      78,    92,    80,    81,    -1,    18,    79,    92,    78,     3,
      78,    94,    80,    81,    -1,    34,    79,    90,    80,    81,
      -1,    37,    79,    86,    78,     3,    78,    86,    80,    81,
      -1,    43,    79,    86,    78,     3,    78,    92,    80,    81,
      -1,    44,    79,    86,    78,     3,    80,    81,    -1,    38,
      79,    86,    78,     3,    78,    92,    80,    81,    -1,    39,
      79,    92,    78,     3,    78,    92,    80,    81,    -1,    45,
      79,     3,    78,     3,    78,    92,    80,    81,    -1,    45,
      79,     3,    78,     3,    80,    81,    -1,    40,    79,    86,
      78,     3,    80,    81,    -1,    41,    79,    86,    78,     3,
      80,    81,    -1,    20,    79,     3,    78,    86,    78,     3,
      78,     8,    78,    86,    80,    81,    -1,    21,    79,     3,
      78,     3,    78,     3,    80,    81,    -1,    21,    79,     3,
      78,     3,    78,     3,    78,     3,    80,    81,    -1,    25,
      79,     3,    78,    86,    78,     8,    78,     3,    80,    81,
      -1,    61,    79,     3,    80,    81,    -1,    66,    79,     3,
      78,     3,    78,     3,    80,    81,    -1,    62,    79,     3,
      78,     3,    78,     3,    80,    81,    -1,    63,    79,     3,
      78,    86,    80,    81,    -1,    67,    79,     3,    78,     3,
      78,     8,    80,    81,    -1,    64,    79,     3,    78,    86,
      80,    81,    -1,    65,    79,     3,    78,     3,    80,    81,
      -1
};

/* YYRLINE[YYN] -- source line where rule number YYN was defined.  */
static const yytype_uint16 yyrline[] =
{
       0,   108,   108,   109,   113,   116,   117,   118,   121,   124,
     125,   126,   129,   131,   132,   134,   135,   151,   157,   160,
     163,   164,   165,   166,   167,   168,   171,   172,   188,   194,
     197,   200,   201,   202,   203,   204,   205,   209,   217,   226,
     234,   243,   256,   257,   258,   269,   270,   276,   282,   309,
     316,   317,   318,   329,   342,   350,   375,   441,   523,   535,
     555,   567,   579,   591,   603,   604,   609,   680,   695,   710,
     728,   746,   770,   785,   807,   832,   890,   967,   982,   997,
    1015,  1033,  1047,  1062,  1077,  1092,  1107,  1122,  1137,  1152,
    1178,  1204,  1230,  1256,  1282,  1300,  1318,  1336,  1354,  1372,
    1389,  1406,  1423,  1443,  1472,  1492,  1507,  1522,  1537,  1552,
    1603,  1618,  1641,  1662,  1683,  1705,  1721,  1737,  1753,  1775,
    1801,  1817,  1827,  1838,  1848,  1865,  1880,  1897,  1917,  1936,
    1959
};
#endif

#if YYDEBUG || YYERROR_VERBOSE || YYTOKEN_TABLE
/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals.  */
static const char *const yytname[] =
{
  "$end", "error", "$undefined", "NUMBER", "LEVEL", "TRUEORFALSE",
  "FILENAME", "PROCEDURENAME", "VARIABLE", "FREEVAR", "SOURCE",
  "PROCEDURE", "FORMAT", "LOOP", "PERMUTE", "ORIGINAL", "TILE", "UNROLL",
  "SPLIT", "UNROLL_EXTRA", "REDUCE", "SPLIT_WITH_ALIGNMENT", "DATACOPY",
  "DATACOPY_PRIVATIZED", "FLATTEN", "SCALAR_EXPAND", "NORMALIZE", "ELLIFY",
  "COMPACT", "MAKE_DENSE", "SET_ARRAY_SIZE", "SPARSE_PARTITION",
  "SPARSE_WAVEFRONT", "REORDER_BY_INSPECTOR", "NONSINGULAR", "EXIT",
  "KNOWN", "SKEW", "SHIFT", "SHIFT_TO", "FUSE", "DISTRIBUTE", "REMOVE_DEP",
  "SCALE", "REVERSE", "PEEL", "REORDER_DATA", "STRIDED", "COUNTED",
  "NUM_STATEMENT", "CEIL", "FLOOR", "PRINT", "PRINT_CODE", "PRINT_DEP",
  "PRINT_IS", "PRINT_STRUCTURE", "NE", "LE", "GE", "EQ",
  "MAP_TO_OPENMP_REGION", "MARK_OMP_PARL_REGION", "MARK_OMP_THRDS",
  "MARK_OMP_SYNC", "GEN_OMP_PARL_REGION", "OMP_PAR_FOR", "MARK_PRAGMA",
  "'>'", "'<'", "'+'", "'-'", "'*'", "'/'", "'%'", "UMINUS", "'['", "']'",
  "','", "'('", "')'", "'\\n'", "':'", "'='", "$accept", "script",
  "vector", "vector_number", "vector_string", "string_vector", "matrix",
  "matrix_part", "expr", "float_expr", "cond", "cond_term", "command", 0
};
#endif

# ifdef YYPRINT
/* YYTOKNUM[YYLEX-NUM] -- Internal token number corresponding to
   token YYLEX-NUM.  */
static const yytype_uint16 yytoknum[] =
{
       0,   256,   257,   258,   259,   260,   261,   262,   263,   264,
     265,   266,   267,   268,   269,   270,   271,   272,   273,   274,
     275,   276,   277,   278,   279,   280,   281,   282,   283,   284,
     285,   286,   287,   288,   289,   290,   291,   292,   293,   294,
     295,   296,   297,   298,   299,   300,   301,   302,   303,   304,
     305,   306,   307,   308,   309,   310,   311,   312,   313,   314,
     315,   316,   317,   318,   319,   320,   321,   322,    62,    60,
      43,    45,    42,    47,    37,   323,    91,    93,    44,    40,
      41,    10,    58,    61
};
# endif

/* YYR1[YYN] -- Symbol number of symbol that rule YYN derives.  */
static const yytype_uint8 yyr1[] =
{
       0,    84,    85,    85,    86,    87,    87,    87,    88,    89,
      89,    89,    90,    91,    91,    92,    92,    92,    92,    92,
      92,    92,    92,    92,    92,    92,    93,    93,    93,    93,
      93,    93,    93,    93,    93,    93,    93,    94,    94,    94,
      94,    94,    95,    95,    95,    95,    95,    95,    95,    95,
      96,    96,    96,    96,    96,    96,    96,    96,    96,    96,
      96,    96,    96,    96,    96,    96,    96,    96,    96,    96,
      96,    96,    96,    96,    96,    96,    96,    96,    96,    96,
      96,    96,    96,    96,    96,    96,    96,    96,    96,    96,
      96,    96,    96,    96,    96,    96,    96,    96,    96,    96,
      96,    96,    96,    96,    96,    96,    96,    96,    96,    96,
      96,    96,    96,    96,    96,    96,    96,    96,    96,    96,
      96,    96,    96,    96,    96,    96,    96,    96,    96,    96,
      96
};

/* YYR2[YYN] -- Number of symbols composing right hand side of rule YYN.  */
static const yytype_uint8 yyr2[] =
{
       0,     2,     0,     2,     3,     0,     1,     3,     3,     0,
       1,     3,     3,     1,     3,     1,     1,     3,     4,     4,
       3,     3,     3,     3,     3,     2,     1,     1,     3,     4,
       4,     3,     3,     3,     3,     3,     2,     3,     3,     3,
       3,     3,     1,     1,     1,     3,     3,     3,     3,     2,
       1,     2,     4,     4,     4,     4,     4,     6,     2,     3,
       3,     3,     3,     3,     2,     4,     5,     7,     4,     5,
       9,     7,     9,    11,    13,    15,    17,     9,     7,     9,
      13,     7,    11,    13,    15,    17,    13,    15,    17,     7,
       9,    11,    13,    15,     9,    11,    13,    15,    17,     9,
       9,    15,    11,    17,    19,     9,    11,     9,    11,     9,
       5,     9,     9,     7,     9,     9,     9,     7,     7,     7,
      13,     9,    11,    11,     5,     9,     9,     7,     9,     7,
       7
};

/* YYDEFACT[STATE-NAME] -- Default reduction number in state STATE-NUM.
   Performed when YYTABLE doesn't specify something else to do.  Zero
   means the default is an error.  */
static const yytype_uint8 yydefact[] =
{
       2,     0,     1,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
      50,     3,    51,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,    64,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,    15,    16,     0,     0,     0,     0,     0,     0,     0,
       0,     0,    58,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     5,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,    42,    43,    44,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,    59,    60,    61,    62,    25,     0,     0,
       0,     0,     0,    63,     0,     0,     0,     0,     0,     0,
       0,    65,    52,    54,    53,    55,     0,    56,     0,     6,
       0,     0,     0,    68,     0,     0,     0,     0,     0,     0,
      13,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,    49,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,    17,    26,
      27,     0,     0,     0,     0,     0,     0,     0,    20,    22,
      21,    23,    24,     0,     0,     0,     0,     0,     0,     0,
       0,     4,     0,     0,    69,     0,     0,     0,     0,     0,
       0,     0,    12,     0,     0,     0,     0,     0,     0,     0,
       0,     9,     0,     0,     0,     0,     0,     0,     0,   110,
      45,    66,    39,    37,    41,    38,    40,    47,    46,    48,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,    36,     0,     0,     0,     0,     0,    18,
      19,   124,     0,     0,     0,     0,     0,     0,    57,     7,
       0,     0,     0,     0,     0,     0,     0,     0,    14,     0,
       0,     0,     0,     0,     0,     0,     0,    10,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,    28,     0,     0,
      31,    33,    32,    34,    35,     0,     0,     0,     0,     0,
       0,    71,     0,     0,     0,     0,     0,     0,     0,     0,
      89,     0,     0,     0,     0,     0,    81,     8,     0,     0,
       0,     0,    78,     0,     0,     0,     0,     0,     0,   118,
     119,    67,     0,   113,     0,   117,     0,    29,    30,     0,
     127,   129,   130,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,    11,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,    70,     0,    72,     0,   105,   109,     0,   107,     0,
       0,   121,     0,    90,     0,    94,     0,     0,     0,     0,
       0,    79,     0,    77,   100,    99,     0,   111,   114,   115,
     112,   116,     0,   126,   125,   128,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,    82,   106,   108,     0,
     122,     0,    91,     0,    95,     0,     0,    73,   123,     0,
       0,     0,   102,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,    83,
       0,    86,   120,     0,    92,     0,    96,     0,     0,    80,
       0,    74,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,    84,     0,    87,    93,
       0,    97,     0,     0,     0,    75,   101,     0,     0,     0,
       0,     0,     0,    85,    88,    98,   103,     0,    76,     0,
     104
};

/* YYDEFGOTO[NTERM-NUM].  */
static const yytype_int16 yydefgoto[] =
{
      -1,     1,   118,   188,   282,   348,   128,   201,   103,   246,
     148,   149,    51
};

/* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
   STATE-NUM.  */
#define YYPACT_NINF -350
static const yytype_int16 yypact[] =
{
    -350,   210,  -350,   -77,   -58,   -33,    -4,     1,    65,   -24,
      -6,    69,    75,    76,    81,    82,   140,   189,   190,   199,
     209,   211,   217,   218,   223,   224,   231,   260,   271,   274,
      29,   275,   286,   289,   290,   293,   294,   295,   297,   298,
     300,   302,     3,   303,   313,   316,   317,   328,   331,   332,
    -350,  -350,  -350,    62,   145,    64,   172,   304,    26,   213,
      62,    62,    62,    62,   322,   337,    30,    30,   352,   363,
     367,   394,   409,   419,   281,   420,   422,   423,   351,  -350,
      28,   353,   353,    62,   353,   353,   438,   353,   353,   439,
     440,  -350,  -350,   359,   371,   373,   270,   312,   327,   383,
      62,    62,  -350,   -31,   451,   464,   465,   466,   476,   481,
     483,   -10,   384,   407,   428,   430,   -59,    62,   -43,   -27,
     433,    72,   405,   421,   425,   424,   426,   353,   450,   435,
     452,   449,   458,   459,   461,   472,   473,   474,   480,   482,
     485,   486,   436,  -350,  -350,  -350,    28,    28,   479,   105,
     487,   488,   453,   489,   490,   491,   492,   493,   494,   495,
     496,   117,   117,  -350,  -350,  -350,  -350,  -350,    51,    62,
      62,    62,    62,  -350,   497,   500,   501,   502,   503,   504,
     505,  -350,  -350,  -350,  -350,  -350,   498,  -350,    -9,    99,
     353,   448,   558,  -350,   571,   572,   581,   582,   353,   583,
    -350,    10,   584,   585,   586,   587,   588,   353,   589,   515,
     353,   590,   591,   592,   594,   595,   518,  -350,    56,   519,
      28,    28,    28,    28,    28,    28,    28,    28,   598,   599,
     600,   601,   602,   603,   604,   605,   606,   607,  -350,  -350,
    -350,   531,   532,   533,   117,   117,   261,   272,  -350,   -54,
     -54,  -350,  -350,   535,   610,   353,   353,   611,   614,   615,
     538,  -350,    62,   540,  -350,   543,   544,   545,   546,   547,
     548,   549,  -350,   353,    20,   550,   551,   552,   553,   554,
     555,   625,   556,   559,   560,   561,   562,   564,   565,  -350,
    -350,  -350,    68,    68,    68,    68,    68,   567,   567,  -350,
     566,   568,   569,   570,   573,   574,   577,   576,    52,   579,
     578,   117,   117,  -350,   276,   117,   117,   117,   117,  -350,
    -350,  -350,   593,   580,   596,   597,   608,   609,  -350,    99,
     612,   353,    62,    62,    28,    62,   633,   642,  -350,   643,
     613,   641,   353,   644,   353,   651,   616,  -350,    37,   648,
     515,   653,   617,   654,   658,   656,   353,    62,    62,   618,
     619,   620,    62,   621,    62,   622,   657,  -350,   287,   291,
    -350,    31,    31,  -350,  -350,   663,   623,   624,   626,   664,
     660,  -350,   628,   109,   113,   629,   127,   632,   112,   123,
    -350,   124,   634,   635,   636,   637,  -350,  -350,   661,   128,
     638,   631,  -350,   639,   640,   645,   646,   314,   318,  -350,
    -350,  -350,   329,  -350,   333,  -350,   647,  -350,  -350,   649,
    -350,  -350,  -350,   650,   652,   655,   667,   659,    62,   662,
     665,    62,   666,   670,   669,   668,    62,   671,   674,   672,
     675,   353,   673,   679,  -350,   678,   676,   681,   677,   680,
     682,   683,   684,   685,   686,   687,   688,   515,   689,   690,
     691,  -350,   136,  -350,   344,  -350,  -350,   348,  -350,   695,
     694,  -350,   187,  -350,   137,  -350,   697,   698,   699,   700,
     703,  -350,   704,  -350,  -350,  -350,   705,  -350,  -350,  -350,
    -350,  -350,   706,  -350,  -350,  -350,    87,   696,   707,   708,
     353,   709,    62,   710,    62,   711,    62,   701,   712,   713,
     714,   515,   716,   715,   183,   186,  -350,  -350,  -350,   717,
    -350,   214,  -350,   228,  -350,   462,   720,  -350,  -350,   719,
     243,   722,  -350,    62,   721,    62,   723,   724,    62,   725,
      62,   726,    62,    62,   727,    28,   728,   729,   242,  -350,
     246,  -350,  -350,   360,  -350,   257,  -350,   471,   475,  -350,
     258,  -350,   730,    62,   731,    62,   732,   733,    62,   734,
      62,    62,   692,   735,   736,   364,  -350,   375,  -350,  -350,
     386,  -350,   390,   484,   738,  -350,  -350,   739,   740,   741,
     742,    62,   743,  -350,  -350,  -350,  -350,   401,  -350,   744,
    -350
};

/* YYPGOTO[NTERM-NUM].  */
static const yytype_int16 yypgoto[] =
{
    -350,  -350,   -61,  -350,  -349,  -350,   -50,  -350,   -53,  -159,
    -332,  -131,  -350
};

/* YYTABLE[YYPACT[STATE-NUM]].  What to do in state STATE-NUM.  If
   positive, shift that token.  If negative, reduce the rule which
   number is the opposite.  If YYTABLE_NINF, syntax error.  */
#define YYTABLE_NINF -1
static const yytype_uint16 yytable[] =
{
     111,   400,   385,   247,    52,   119,    91,   121,   122,   123,
     124,    92,   186,   129,   131,   217,   218,   130,   171,   172,
     150,   151,   187,   153,   154,    53,   156,   157,   142,    91,
     152,   143,   144,    91,    92,   190,   145,   191,    92,   169,
     170,   171,   172,   169,   170,   171,   172,   167,   168,    54,
     173,   192,    93,    94,    95,    58,    96,    97,    98,    99,
     169,   170,   171,   172,   189,    91,   200,   113,   261,   262,
      92,   181,   114,    59,   100,    93,    94,    95,    55,    93,
      94,    95,   101,    56,   102,   313,   314,   272,   273,   292,
     293,   294,   295,   296,   297,   298,   299,   100,   339,   146,
     340,   100,   117,   317,   318,   101,   127,   147,   492,   101,
      79,    93,    94,    95,   397,   398,   249,   250,   251,   252,
     239,   169,   170,   171,   172,   240,   225,   226,   227,   263,
     364,   248,   365,   100,   514,   515,   290,   270,   225,   226,
     227,   101,   169,   170,   171,   172,   279,    57,    60,   283,
     194,   112,   368,   369,    61,    62,   371,   372,   373,   374,
      63,    64,   530,   220,   221,   222,   241,   242,   243,   169,
     170,   171,   172,   223,   224,   225,   226,   227,   115,   169,
     170,   171,   172,   169,   170,   171,   172,   426,   244,   427,
     434,   428,   435,   429,   323,   324,   245,   169,   170,   171,
     172,   436,   438,   437,   439,   431,   445,   432,   446,   329,
       2,     3,   338,   560,   496,   504,   497,   505,     4,    65,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    40,    41,   169,   170,   171,
     172,   533,    42,   534,   535,   502,   536,   503,    66,    67,
     382,    43,    44,    45,    46,    47,    48,    49,    68,   383,
     384,   392,   386,   394,   169,   170,   171,   172,    69,   138,
      70,    50,   538,   120,   539,   406,    71,    72,   169,   170,
     171,   172,    73,    74,   407,   408,   540,   116,   541,   412,
      75,   414,   169,   170,   171,   172,   169,   170,   171,   172,
     563,   545,   564,   546,   565,   125,   566,   169,   170,   171,
     172,   315,   316,   317,   318,   568,   572,   569,   573,    76,
     126,   319,   315,   316,   317,   318,   315,   316,   317,   318,
      77,   163,   320,    78,    80,   132,   370,   315,   316,   317,
     318,   315,   316,   317,   318,    81,   133,   417,    82,    83,
     134,   418,    84,    85,    86,   464,    87,    88,   467,    89,
     477,    90,   104,   472,   169,   170,   171,   172,   169,   170,
     171,   172,   105,   164,   453,   106,   107,   135,   454,   169,
     170,   171,   172,   169,   170,   171,   172,   108,   165,   455,
     109,   110,   136,   456,   169,   170,   171,   172,   169,   170,
     171,   172,   137,   139,   498,   140,   141,   127,   499,   117,
     169,   170,   171,   172,   169,   170,   171,   172,   160,   519,
     567,   155,   158,   159,   587,   169,   170,   171,   172,   521,
     161,   523,   162,   525,   174,   588,   169,   170,   171,   172,
     169,   170,   171,   172,   166,   182,   589,   175,   176,   177,
     590,   169,   170,   171,   172,   169,   170,   171,   172,   178,
     548,   599,   550,   195,   179,   553,   180,   555,   183,   557,
     558,   169,   170,   171,   172,   169,   170,   171,   172,   196,
       0,   260,   198,   197,   199,   169,   170,   171,   172,   184,
     575,   185,   577,   203,   193,   580,   216,   582,   583,   169,
     170,   171,   172,   169,   170,   171,   172,   205,   202,   264,
     204,   230,   169,   170,   171,   172,   206,   207,   597,   208,
     542,   169,   170,   171,   172,   169,   170,   171,   172,   570,
     209,   210,   211,   571,   169,   170,   171,   172,   212,   219,
     213,   265,   591,   214,   215,   228,   229,   231,   232,   233,
     234,   235,   236,   237,   266,   267,   238,   253,   254,   255,
     256,   257,   258,   259,   268,   269,   271,   274,   275,   276,
     277,   281,   280,   284,   285,   286,   278,   287,   288,   289,
     291,   300,   301,   302,   303,   304,   305,   306,   307,   308,
     310,   311,   312,   322,   325,   309,   321,   326,   327,   328,
     330,   331,   332,   333,   334,   335,   336,   337,   341,   342,
     343,   344,   345,   347,   349,   346,   387,   350,   351,   227,
     353,   352,   354,   355,   356,   388,   357,   358,   389,   391,
     359,   399,   393,   360,   361,   362,   363,   366,   367,   395,
     376,   401,   403,   404,   405,   416,   419,   423,   424,   444,
     462,   375,   470,     0,     0,     0,   377,   378,   469,   474,
     476,   478,   479,   480,   482,     0,   379,   380,     0,     0,
       0,   486,     0,   381,   390,   584,     0,   396,   402,   409,
     410,   411,   413,   415,   420,   421,   526,   422,   425,   430,
     433,   448,   440,   441,   442,   443,   447,     0,     0,   449,
     450,     0,   529,   451,   531,   457,   452,     0,     0,   458,
     459,     0,   460,     0,     0,     0,   461,   562,     0,     0,
     463,     0,     0,   465,     0,     0,   466,   468,     0,   471,
       0,     0,   473,   475,     0,     0,     0,   481,   483,     0,
       0,   484,     0,   485,     0,   487,   488,   489,   490,   491,
     493,   494,   495,   500,   501,   506,   507,   516,     0,   508,
     509,   510,   511,   512,     0,     0,   513,     0,   517,   518,
     520,   522,   524,   527,   528,     0,   532,   537,   543,   544,
     547,     0,   549,     0,   551,   552,   554,   556,   559,   561,
     574,     0,   576,   578,   579,   581,   585,   586,   592,     0,
     593,   594,   595,   596,   598,   600
};

#define yypact_value_is_default(yystate) \
  ((yystate) == (-350))

#define yytable_value_is_error(yytable_value) \
  YYID (0)

static const yytype_int16 yycheck[] =
{
      53,   350,   334,   162,    81,    58,     3,    60,    61,    62,
      63,     8,    71,    66,    67,   146,   147,    67,    72,    73,
      81,    82,    81,    84,    85,    83,    87,    88,    78,     3,
      83,     3,     4,     3,     8,    78,     8,    80,     8,    70,
      71,    72,    73,    70,    71,    72,    73,   100,   101,    82,
      81,    78,    49,    50,    51,    79,    53,    54,    55,    56,
      70,    71,    72,    73,   117,     3,   127,     3,    77,    78,
       8,    81,     8,    79,    71,    49,    50,    51,    82,    49,
      50,    51,    79,    82,    81,   244,   245,    77,    78,   220,
     221,   222,   223,   224,   225,   226,   227,    71,    78,    71,
      80,    71,    76,    72,    73,    79,    76,    79,   457,    79,
      81,    49,    50,    51,    77,    78,   169,   170,   171,   172,
       3,    70,    71,    72,    73,     8,    70,    71,    72,   190,
      78,    80,    80,    71,    47,    48,    80,   198,    70,    71,
      72,    79,    70,    71,    72,    73,   207,    82,    79,   210,
      78,     6,   311,   312,    79,    79,   315,   316,   317,   318,
      79,    79,   511,    58,    59,    60,    49,    50,    51,    70,
      71,    72,    73,    68,    69,    70,    71,    72,     6,    70,
      71,    72,    73,    70,    71,    72,    73,    78,    71,    80,
      78,    78,    80,    80,   255,   256,    79,    70,    71,    72,
      73,    78,    78,    80,    80,    78,    78,    80,    80,   262,
       0,     1,   273,   545,    78,    78,    80,    80,     8,    79,
      10,    11,    12,    13,    14,    15,    16,    17,    18,    19,
      20,    21,    22,    23,    24,    25,    26,    27,    28,    29,
      30,    31,    32,    33,    34,    35,    36,    37,    38,    39,
      40,    41,    42,    43,    44,    45,    46,    70,    71,    72,
      73,    78,    52,    80,    78,    78,    80,    80,    79,    79,
     331,    61,    62,    63,    64,    65,    66,    67,    79,   332,
     333,   342,   335,   344,    70,    71,    72,    73,    79,     8,
      79,    81,    78,    80,    80,   356,    79,    79,    70,    71,
      72,    73,    79,    79,   357,   358,    78,     3,    80,   362,
      79,   364,    70,    71,    72,    73,    70,    71,    72,    73,
      78,    78,    80,    80,    78,     3,    80,    70,    71,    72,
      73,    70,    71,    72,    73,    78,    78,    80,    80,    79,
       3,    80,    70,    71,    72,    73,    70,    71,    72,    73,
      79,    81,    80,    79,    79,     3,    80,    70,    71,    72,
      73,    70,    71,    72,    73,    79,     3,    80,    79,    79,
       3,    80,    79,    79,    79,   428,    79,    79,   431,    79,
     441,    79,    79,   436,    70,    71,    72,    73,    70,    71,
      72,    73,    79,    81,    80,    79,    79,     3,    80,    70,
      71,    72,    73,    70,    71,    72,    73,    79,    81,    80,
      79,    79,     3,    80,    70,    71,    72,    73,    70,    71,
      72,    73,     3,     3,    80,     3,     3,    76,    80,    76,
      70,    71,    72,    73,    70,    71,    72,    73,    79,   500,
      80,     3,     3,     3,    80,    70,    71,    72,    73,   502,
      79,   504,    79,   506,     3,    80,    70,    71,    72,    73,
      70,    71,    72,    73,    81,    81,    80,     3,     3,     3,
      80,    70,    71,    72,    73,    70,    71,    72,    73,     3,
     533,    80,   535,    78,     3,   538,     3,   540,    81,   542,
     543,    70,    71,    72,    73,    70,    71,    72,    73,    78,
      -1,     3,    78,    78,    78,    70,    71,    72,    73,    81,
     563,    81,   565,    78,    81,   568,    80,   570,   571,    70,
      71,    72,    73,    70,    71,    72,    73,    78,    78,    81,
      78,    78,    70,    71,    72,    73,    78,    78,   591,    78,
      78,    70,    71,    72,    73,    70,    71,    72,    73,    78,
      78,    78,    78,    78,    70,    71,    72,    73,    78,    80,
      78,     3,    78,    78,    78,    78,    78,    78,    78,    78,
      78,    78,    78,    78,     3,     3,    80,    80,    78,    78,
      78,    78,    78,    78,     3,     3,     3,     3,     3,     3,
       3,    76,     3,     3,     3,     3,     8,     3,     3,    81,
      81,     3,     3,     3,     3,     3,     3,     3,     3,     3,
      79,    79,    79,     3,     3,     8,    81,     3,     3,    81,
      80,    78,    78,    78,    78,    78,    78,    78,    78,    78,
      78,    78,    78,     8,    78,    80,     3,    78,    78,    72,
      78,    80,    78,    78,    78,     3,    78,    78,     5,     8,
      80,     3,     8,    80,    80,    78,    80,    78,    80,     8,
      80,     8,     8,     5,     8,     8,     3,     3,     8,     8,
       3,    78,     3,    -1,    -1,    -1,    80,    80,     8,     5,
       5,     8,     3,     5,     3,    -1,    78,    78,    -1,    -1,
      -1,     8,    -1,    81,    81,     3,    -1,    81,    81,    81,
      81,    81,    81,    81,    81,    81,     5,    81,    80,    80,
      78,    80,    78,    78,    78,    78,    78,    -1,    -1,    80,
      80,    -1,     8,    78,     8,    78,    80,    -1,    -1,    80,
      80,    -1,    80,    -1,    -1,    -1,    81,     8,    -1,    -1,
      81,    -1,    -1,    81,    -1,    -1,    81,    81,    -1,    81,
      -1,    -1,    81,    81,    -1,    -1,    -1,    81,    81,    -1,
      -1,    81,    -1,    81,    -1,    81,    81,    81,    81,    81,
      81,    81,    81,    78,    80,    78,    78,    81,    -1,    80,
      80,    78,    78,    78,    -1,    -1,    80,    -1,    81,    81,
      81,    81,    81,    81,    81,    -1,    81,    80,    78,    80,
      78,    -1,    81,    -1,    81,    81,    81,    81,    81,    81,
      80,    -1,    81,    81,    81,    81,    81,    81,    80,    -1,
      81,    81,    81,    81,    81,    81
};

/* YYSTOS[STATE-NUM] -- The (internal number of the) accessing
   symbol of state STATE-NUM.  */
static const yytype_uint8 yystos[] =
{
       0,    85,     0,     1,     8,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    40,    41,    42,    43,    44,
      45,    46,    52,    61,    62,    63,    64,    65,    66,    67,
      81,    96,    81,    83,    82,    82,    82,    82,    79,    79,
      79,    79,    79,    79,    79,    79,    79,    79,    79,    79,
      79,    79,    79,    79,    79,    79,    79,    79,    79,    81,
      79,    79,    79,    79,    79,    79,    79,    79,    79,    79,
      79,     3,     8,    49,    50,    51,    53,    54,    55,    56,
      71,    79,    81,    92,    79,    79,    79,    79,    79,    79,
      79,    92,     6,     3,     8,     6,     3,    76,    86,    92,
      80,    92,    92,    92,    92,     3,     3,    76,    90,    92,
      90,    92,     3,     3,     3,     3,     3,     3,     8,     3,
       3,     3,    90,     3,     4,     8,    71,    79,    94,    95,
      86,    86,    92,    86,    86,     3,    86,    86,     3,     3,
      79,    79,    79,    81,    81,    81,    81,    92,    92,    70,
      71,    72,    73,    81,     3,     3,     3,     3,     3,     3,
       3,    81,    81,    81,    81,    81,    71,    81,    87,    92,
      78,    80,    78,    81,    78,    78,    78,    78,    78,    78,
      86,    91,    78,    78,    78,    78,    78,    78,    78,    78,
      78,    78,    78,    78,    78,    78,    80,    95,    95,    80,
      58,    59,    60,    68,    69,    70,    71,    72,    78,    78,
      78,    78,    78,    78,    78,    78,    78,    78,    80,     3,
       8,    49,    50,    51,    71,    79,    93,    93,    80,    92,
      92,    92,    92,    80,    78,    78,    78,    78,    78,    78,
       3,    77,    78,    86,    81,     3,     3,     3,     3,     3,
      86,     3,    77,    78,     3,     3,     3,     3,     8,    86,
       3,    76,    88,    86,     3,     3,     3,     3,     3,    81,
      80,    81,    95,    95,    95,    95,    95,    95,    95,    95,
       3,     3,     3,     3,     3,     3,     3,     3,     3,     8,
      79,    79,    79,    93,    93,    70,    71,    72,    73,    80,
      80,    81,     3,    86,    86,     3,     3,     3,    81,    92,
      80,    78,    78,    78,    78,    78,    78,    78,    86,    78,
      80,    78,    78,    78,    78,    78,    80,     8,    89,    78,
      78,    78,    80,    78,    78,    78,    78,    78,    78,    80,
      80,    80,    78,    80,    78,    80,    78,    80,    93,    93,
      80,    93,    93,    93,    93,    78,    80,    80,    80,    78,
      78,    81,    86,    92,    92,    94,    92,     3,     3,     5,
      81,     8,    86,     8,    86,     8,    81,    77,    78,     3,
      88,     8,    81,     8,     5,     8,    86,    92,    92,    81,
      81,    81,    92,    81,    92,    81,     8,    80,    80,     3,
      81,    81,    81,     3,     8,    80,    78,    80,    78,    80,
      80,    78,    80,    78,    78,    80,    78,    80,    78,    80,
      78,    78,    78,    78,     8,    78,    80,    78,    80,    80,
      80,    78,    80,    80,    80,    80,    80,    78,    80,    80,
      80,    81,     3,    81,    92,    81,    81,    92,    81,     8,
       3,    81,    92,    81,     5,    81,     5,    86,     8,     3,
       5,    81,     3,    81,    81,    81,     8,    81,    81,    81,
      81,    81,    88,    81,    81,    81,    78,    80,    80,    80,
      78,    80,    78,    80,    78,    80,    78,    78,    80,    80,
      78,    78,    78,    80,    47,    48,    81,    81,    81,    86,
      81,    92,    81,    92,    81,    92,     5,    81,    81,     8,
      88,     8,    81,    78,    80,    78,    80,    80,    78,    80,
      78,    80,    78,    78,    80,    78,    80,    78,    92,    81,
      92,    81,    81,    92,    81,    92,    81,    92,    92,    81,
      94,    81,     8,    78,    80,    78,    80,    80,    78,    80,
      78,    78,    78,    80,    80,    92,    81,    92,    81,    81,
      92,    81,    92,    92,     3,    81,    81,    80,    80,    80,
      80,    78,    80,    81,    81,    81,    81,    92,    81,    80,
      81
};

#define yyerrok		(yyerrstatus = 0)
#define yyclearin	(yychar = YYEMPTY)
#define YYEMPTY		(-2)
#define YYEOF		0

#define YYACCEPT	goto yyacceptlab
#define YYABORT		goto yyabortlab
#define YYERROR		goto yyerrorlab


/* Like YYERROR except do call yyerror.  This remains here temporarily
   to ease the transition to the new meaning of YYERROR, for GCC.
   Once GCC version 2 has supplanted version 1, this can go.  However,
   YYFAIL appears to be in use.  Nevertheless, it is formally deprecated
   in Bison 2.4.2's NEWS entry, where a plan to phase it out is
   discussed.  */

#define YYFAIL		goto yyerrlab
#if defined YYFAIL
  /* This is here to suppress warnings from the GCC cpp's
     -Wunused-macros.  Normally we don't worry about that warning, but
     some users do, and we want to make it easy for users to remove
     YYFAIL uses, which will produce warnings from Bison 2.5.  */
#endif

#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)					\
do								\
  if (yychar == YYEMPTY && yylen == 1)				\
    {								\
      yychar = (Token);						\
      yylval = (Value);						\
      YYPOPSTACK (1);						\
      goto yybackup;						\
    }								\
  else								\
    {								\
      yyerror (YY_("syntax error: cannot back up")); \
      YYERROR;							\
    }								\
while (YYID (0))


#define YYTERROR	1
#define YYERRCODE	256


/* YYLLOC_DEFAULT -- Set CURRENT to span from RHS[1] to RHS[N].
   If N is 0, then set CURRENT to the empty location which ends
   the previous symbol: RHS[0] (always defined).  */

#define YYRHSLOC(Rhs, K) ((Rhs)[K])
#ifndef YYLLOC_DEFAULT
# define YYLLOC_DEFAULT(Current, Rhs, N)				\
    do									\
      if (YYID (N))                                                    \
	{								\
	  (Current).first_line   = YYRHSLOC (Rhs, 1).first_line;	\
	  (Current).first_column = YYRHSLOC (Rhs, 1).first_column;	\
	  (Current).last_line    = YYRHSLOC (Rhs, N).last_line;		\
	  (Current).last_column  = YYRHSLOC (Rhs, N).last_column;	\
	}								\
      else								\
	{								\
	  (Current).first_line   = (Current).last_line   =		\
	    YYRHSLOC (Rhs, 0).last_line;				\
	  (Current).first_column = (Current).last_column =		\
	    YYRHSLOC (Rhs, 0).last_column;				\
	}								\
    while (YYID (0))
#endif


/* This macro is provided for backward compatibility. */

#ifndef YY_LOCATION_PRINT
# define YY_LOCATION_PRINT(File, Loc) ((void) 0)
#endif


/* YYLEX -- calling `yylex' with the right arguments.  */

#ifdef YYLEX_PARAM
# define YYLEX yylex (YYLEX_PARAM)
#else
# define YYLEX yylex ()
#endif

/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)			\
do {						\
  if (yydebug)					\
    YYFPRINTF Args;				\
} while (YYID (0))

# define YY_SYMBOL_PRINT(Title, Type, Value, Location)			  \
do {									  \
  if (yydebug)								  \
    {									  \
      YYFPRINTF (stderr, "%s ", Title);					  \
      yy_symbol_print (stderr,						  \
		  Type, Value); \
      YYFPRINTF (stderr, "\n");						  \
    }									  \
} while (YYID (0))


/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

/*ARGSUSED*/
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_symbol_value_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep)
#else
static void
yy_symbol_value_print (yyoutput, yytype, yyvaluep)
    FILE *yyoutput;
    int yytype;
    YYSTYPE const * const yyvaluep;
#endif
{
  if (!yyvaluep)
    return;
# ifdef YYPRINT
  if (yytype < YYNTOKENS)
    YYPRINT (yyoutput, yytoknum[yytype], *yyvaluep);
# else
  YYUSE (yyoutput);
# endif
  switch (yytype)
    {
      default:
	break;
    }
}


/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_symbol_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep)
#else
static void
yy_symbol_print (yyoutput, yytype, yyvaluep)
    FILE *yyoutput;
    int yytype;
    YYSTYPE const * const yyvaluep;
#endif
{
  if (yytype < YYNTOKENS)
    YYFPRINTF (yyoutput, "token %s (", yytname[yytype]);
  else
    YYFPRINTF (yyoutput, "nterm %s (", yytname[yytype]);

  yy_symbol_value_print (yyoutput, yytype, yyvaluep);
  YYFPRINTF (yyoutput, ")");
}

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_stack_print (yytype_int16 *yybottom, yytype_int16 *yytop)
#else
static void
yy_stack_print (yybottom, yytop)
    yytype_int16 *yybottom;
    yytype_int16 *yytop;
#endif
{
  YYFPRINTF (stderr, "Stack now");
  for (; yybottom <= yytop; yybottom++)
    {
      int yybot = *yybottom;
      YYFPRINTF (stderr, " %d", yybot);
    }
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)				\
do {								\
  if (yydebug)							\
    yy_stack_print ((Bottom), (Top));				\
} while (YYID (0))


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_reduce_print (YYSTYPE *yyvsp, int yyrule)
#else
static void
yy_reduce_print (yyvsp, yyrule)
    YYSTYPE *yyvsp;
    int yyrule;
#endif
{
  int yynrhs = yyr2[yyrule];
  int yyi;
  unsigned long int yylno = yyrline[yyrule];
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %lu):\n",
	     yyrule - 1, yylno);
  /* The symbols being reduced.  */
  for (yyi = 0; yyi < yynrhs; yyi++)
    {
      YYFPRINTF (stderr, "   $%d = ", yyi + 1);
      yy_symbol_print (stderr, yyrhs[yyprhs[yyrule] + yyi],
		       &(yyvsp[(yyi + 1) - (yynrhs)])
		       		       );
      YYFPRINTF (stderr, "\n");
    }
}

# define YY_REDUCE_PRINT(Rule)		\
do {					\
  if (yydebug)				\
    yy_reduce_print (yyvsp, Rule); \
} while (YYID (0))

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args)
# define YY_SYMBOL_PRINT(Title, Type, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef	YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif


#if YYERROR_VERBOSE

# ifndef yystrlen
#  if defined __GLIBC__ && defined _STRING_H
#   define yystrlen strlen
#  else
/* Return the length of YYSTR.  */
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static YYSIZE_T
yystrlen (const char *yystr)
#else
static YYSIZE_T
yystrlen (yystr)
    const char *yystr;
#endif
{
  YYSIZE_T yylen;
  for (yylen = 0; yystr[yylen]; yylen++)
    continue;
  return yylen;
}
#  endif
# endif

# ifndef yystpcpy
#  if defined __GLIBC__ && defined _STRING_H && defined _GNU_SOURCE
#   define yystpcpy stpcpy
#  else
/* Copy YYSRC to YYDEST, returning the address of the terminating '\0' in
   YYDEST.  */
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static char *
yystpcpy (char *yydest, const char *yysrc)
#else
static char *
yystpcpy (yydest, yysrc)
    char *yydest;
    const char *yysrc;
#endif
{
  char *yyd = yydest;
  const char *yys = yysrc;

  while ((*yyd++ = *yys++) != '\0')
    continue;

  return yyd - 1;
}
#  endif
# endif

# ifndef yytnamerr
/* Copy to YYRES the contents of YYSTR after stripping away unnecessary
   quotes and backslashes, so that it's suitable for yyerror.  The
   heuristic is that double-quoting is unnecessary unless the string
   contains an apostrophe, a comma, or backslash (other than
   backslash-backslash).  YYSTR is taken from yytname.  If YYRES is
   null, do not copy; instead, return the length of what the result
   would have been.  */
static YYSIZE_T
yytnamerr (char *yyres, const char *yystr)
{
  if (*yystr == '"')
    {
      YYSIZE_T yyn = 0;
      char const *yyp = yystr;

      for (;;)
	switch (*++yyp)
	  {
	  case '\'':
	  case ',':
	    goto do_not_strip_quotes;

	  case '\\':
	    if (*++yyp != '\\')
	      goto do_not_strip_quotes;
	    /* Fall through.  */
	  default:
	    if (yyres)
	      yyres[yyn] = *yyp;
	    yyn++;
	    break;

	  case '"':
	    if (yyres)
	      yyres[yyn] = '\0';
	    return yyn;
	  }
    do_not_strip_quotes: ;
    }

  if (! yyres)
    return yystrlen (yystr);

  return yystpcpy (yyres, yystr) - yyres;
}
# endif

/* Copy into *YYMSG, which is of size *YYMSG_ALLOC, an error message
   about the unexpected token YYTOKEN for the state stack whose top is
   YYSSP.

   Return 0 if *YYMSG was successfully written.  Return 1 if *YYMSG is
   not large enough to hold the message.  In that case, also set
   *YYMSG_ALLOC to the required number of bytes.  Return 2 if the
   required number of bytes is too large to store.  */
static int
yysyntax_error (YYSIZE_T *yymsg_alloc, char **yymsg,
                yytype_int16 *yyssp, int yytoken)
{
  YYSIZE_T yysize0 = yytnamerr (0, yytname[yytoken]);
  YYSIZE_T yysize = yysize0;
  YYSIZE_T yysize1;
  enum { YYERROR_VERBOSE_ARGS_MAXIMUM = 5 };
  /* Internationalized format string. */
  const char *yyformat = 0;
  /* Arguments of yyformat. */
  char const *yyarg[YYERROR_VERBOSE_ARGS_MAXIMUM];
  /* Number of reported tokens (one for the "unexpected", one per
     "expected"). */
  int yycount = 0;

  /* There are many possibilities here to consider:
     - Assume YYFAIL is not used.  It's too flawed to consider.  See
       <http://lists.gnu.org/archive/html/bison-patches/2009-12/msg00024.html>
       for details.  YYERROR is fine as it does not invoke this
       function.
     - If this state is a consistent state with a default action, then
       the only way this function was invoked is if the default action
       is an error action.  In that case, don't check for expected
       tokens because there are none.
     - The only way there can be no lookahead present (in yychar) is if
       this state is a consistent state with a default action.  Thus,
       detecting the absence of a lookahead is sufficient to determine
       that there is no unexpected or expected token to report.  In that
       case, just report a simple "syntax error".
     - Don't assume there isn't a lookahead just because this state is a
       consistent state with a default action.  There might have been a
       previous inconsistent state, consistent state with a non-default
       action, or user semantic action that manipulated yychar.
     - Of course, the expected token list depends on states to have
       correct lookahead information, and it depends on the parser not
       to perform extra reductions after fetching a lookahead from the
       scanner and before detecting a syntax error.  Thus, state merging
       (from LALR or IELR) and default reductions corrupt the expected
       token list.  However, the list is correct for canonical LR with
       one exception: it will still contain any token that will not be
       accepted due to an error action in a later state.
  */
  if (yytoken != YYEMPTY)
    {
      int yyn = yypact[*yyssp];
      yyarg[yycount++] = yytname[yytoken];
      if (!yypact_value_is_default (yyn))
        {
          /* Start YYX at -YYN if negative to avoid negative indexes in
             YYCHECK.  In other words, skip the first -YYN actions for
             this state because they are default actions.  */
          int yyxbegin = yyn < 0 ? -yyn : 0;
          /* Stay within bounds of both yycheck and yytname.  */
          int yychecklim = YYLAST - yyn + 1;
          int yyxend = yychecklim < YYNTOKENS ? yychecklim : YYNTOKENS;
          int yyx;

          for (yyx = yyxbegin; yyx < yyxend; ++yyx)
            if (yycheck[yyx + yyn] == yyx && yyx != YYTERROR
                && !yytable_value_is_error (yytable[yyx + yyn]))
              {
                if (yycount == YYERROR_VERBOSE_ARGS_MAXIMUM)
                  {
                    yycount = 1;
                    yysize = yysize0;
                    break;
                  }
                yyarg[yycount++] = yytname[yyx];
                yysize1 = yysize + yytnamerr (0, yytname[yyx]);
                if (! (yysize <= yysize1
                       && yysize1 <= YYSTACK_ALLOC_MAXIMUM))
                  return 2;
                yysize = yysize1;
              }
        }
    }

  switch (yycount)
    {
# define YYCASE_(N, S)                      \
      case N:                               \
        yyformat = S;                       \
      break
      YYCASE_(0, YY_("syntax error"));
      YYCASE_(1, YY_("syntax error, unexpected %s"));
      YYCASE_(2, YY_("syntax error, unexpected %s, expecting %s"));
      YYCASE_(3, YY_("syntax error, unexpected %s, expecting %s or %s"));
      YYCASE_(4, YY_("syntax error, unexpected %s, expecting %s or %s or %s"));
      YYCASE_(5, YY_("syntax error, unexpected %s, expecting %s or %s or %s or %s"));
# undef YYCASE_
    }

  yysize1 = yysize + yystrlen (yyformat);
  if (! (yysize <= yysize1 && yysize1 <= YYSTACK_ALLOC_MAXIMUM))
    return 2;
  yysize = yysize1;

  if (*yymsg_alloc < yysize)
    {
      *yymsg_alloc = 2 * yysize;
      if (! (yysize <= *yymsg_alloc
             && *yymsg_alloc <= YYSTACK_ALLOC_MAXIMUM))
        *yymsg_alloc = YYSTACK_ALLOC_MAXIMUM;
      return 1;
    }

  /* Avoid sprintf, as that infringes on the user's name space.
     Don't have undefined behavior even if the translation
     produced a string with the wrong number of "%s"s.  */
  {
    char *yyp = *yymsg;
    int yyi = 0;
    while ((*yyp = *yyformat) != '\0')
      if (*yyp == '%' && yyformat[1] == 's' && yyi < yycount)
        {
          yyp += yytnamerr (yyp, yyarg[yyi++]);
          yyformat += 2;
        }
      else
        {
          yyp++;
          yyformat++;
        }
  }
  return 0;
}
#endif /* YYERROR_VERBOSE */

/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

/*ARGSUSED*/
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yydestruct (const char *yymsg, int yytype, YYSTYPE *yyvaluep)
#else
static void
yydestruct (yymsg, yytype, yyvaluep)
    const char *yymsg;
    int yytype;
    YYSTYPE *yyvaluep;
#endif
{
  YYUSE (yyvaluep);

  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yytype, yyvaluep, yylocationp);

  switch (yytype)
    {
      case 6: /* "FILENAME" */

/* Line 1391 of yacc.c  */
#line 97 "parser.yy"
	{delete [](yyvaluep->name); };

/* Line 1391 of yacc.c  */
#line 1699 "parser.tab.cc"
	break;
      case 8: /* "VARIABLE" */

/* Line 1391 of yacc.c  */
#line 97 "parser.yy"
	{delete [](yyvaluep->name); };

/* Line 1391 of yacc.c  */
#line 1708 "parser.tab.cc"
	break;
      case 9: /* "FREEVAR" */

/* Line 1391 of yacc.c  */
#line 97 "parser.yy"
	{delete [](yyvaluep->name); };

/* Line 1391 of yacc.c  */
#line 1717 "parser.tab.cc"
	break;
      case 86: /* "vector" */

/* Line 1391 of yacc.c  */
#line 98 "parser.yy"
	{delete (yyvaluep->vec); };

/* Line 1391 of yacc.c  */
#line 1726 "parser.tab.cc"
	break;
      case 87: /* "vector_number" */

/* Line 1391 of yacc.c  */
#line 98 "parser.yy"
	{delete (yyvaluep->vec); };

/* Line 1391 of yacc.c  */
#line 1735 "parser.tab.cc"
	break;
      case 90: /* "matrix" */

/* Line 1391 of yacc.c  */
#line 98 "parser.yy"
	{delete (yyvaluep->mat); };

/* Line 1391 of yacc.c  */
#line 1744 "parser.tab.cc"
	break;
      case 91: /* "matrix_part" */

/* Line 1391 of yacc.c  */
#line 98 "parser.yy"
	{delete (yyvaluep->mat); };

/* Line 1391 of yacc.c  */
#line 1753 "parser.tab.cc"
	break;
      case 94: /* "cond" */

/* Line 1391 of yacc.c  */
#line 98 "parser.yy"
	{delete (yyvaluep->tab_lst); };

/* Line 1391 of yacc.c  */
#line 1762 "parser.tab.cc"
	break;
      case 95: /* "cond_term" */

/* Line 1391 of yacc.c  */
#line 98 "parser.yy"
	{delete (yyvaluep->tab); };

/* Line 1391 of yacc.c  */
#line 1771 "parser.tab.cc"
	break;

      default:
	break;
    }
}


/* Prevent warnings from -Wmissing-prototypes.  */
#ifdef YYPARSE_PARAM
#if defined __STDC__ || defined __cplusplus
int yyparse (void *YYPARSE_PARAM);
#else
int yyparse ();
#endif
#else /* ! YYPARSE_PARAM */
#if defined __STDC__ || defined __cplusplus
int yyparse (void);
#else
int yyparse ();
#endif
#endif /* ! YYPARSE_PARAM */


/* The lookahead symbol.  */
int yychar;

/* The semantic value of the lookahead symbol.  */
YYSTYPE yylval;

/* Number of syntax errors so far.  */
int yynerrs;


/*----------.
| yyparse.  |
`----------*/

#ifdef YYPARSE_PARAM
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
int
yyparse (void *YYPARSE_PARAM)
#else
int
yyparse (YYPARSE_PARAM)
    void *YYPARSE_PARAM;
#endif
#else /* ! YYPARSE_PARAM */
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
int
yyparse (void)
#else
int
yyparse ()

#endif
#endif
{
    int yystate;
    /* Number of tokens to shift before error messages enabled.  */
    int yyerrstatus;

    /* The stacks and their tools:
       `yyss': related to states.
       `yyvs': related to semantic values.

       Refer to the stacks thru separate pointers, to allow yyoverflow
       to reallocate them elsewhere.  */

    /* The state stack.  */
    yytype_int16 yyssa[YYINITDEPTH];
    yytype_int16 *yyss;
    yytype_int16 *yyssp;

    /* The semantic value stack.  */
    YYSTYPE yyvsa[YYINITDEPTH];
    YYSTYPE *yyvs;
    YYSTYPE *yyvsp;

    YYSIZE_T yystacksize;

  int yyn;
  int yyresult;
  /* Lookahead token as an internal (translated) token number.  */
  int yytoken;
  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;

#if YYERROR_VERBOSE
  /* Buffer for error messages, and its allocated size.  */
  char yymsgbuf[128];
  char *yymsg = yymsgbuf;
  YYSIZE_T yymsg_alloc = sizeof yymsgbuf;
#endif

#define YYPOPSTACK(N)   (yyvsp -= (N), yyssp -= (N))

  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int yylen = 0;

  yytoken = 0;
  yyss = yyssa;
  yyvs = yyvsa;
  yystacksize = YYINITDEPTH;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yystate = 0;
  yyerrstatus = 0;
  yynerrs = 0;
  yychar = YYEMPTY; /* Cause a token to be read.  */

  /* Initialize stack pointers.
     Waste one element of value and location stack
     so that they stay on the same level as the state stack.
     The wasted elements are never initialized.  */
  yyssp = yyss;
  yyvsp = yyvs;

  goto yysetstate;

/*------------------------------------------------------------.
| yynewstate -- Push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
 yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  yyssp++;

 yysetstate:
  *yyssp = yystate;

  if (yyss + yystacksize - 1 <= yyssp)
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYSIZE_T yysize = yyssp - yyss + 1;

#ifdef yyoverflow
      {
	/* Give user a chance to reallocate the stack.  Use copies of
	   these so that the &'s don't force the real ones into
	   memory.  */
	YYSTYPE *yyvs1 = yyvs;
	yytype_int16 *yyss1 = yyss;

	/* Each stack pointer address is followed by the size of the
	   data in use in that stack, in bytes.  This used to be a
	   conditional around just the two extra args, but that might
	   be undefined if yyoverflow is a macro.  */
	yyoverflow (YY_("memory exhausted"),
		    &yyss1, yysize * sizeof (*yyssp),
		    &yyvs1, yysize * sizeof (*yyvsp),
		    &yystacksize);

	yyss = yyss1;
	yyvs = yyvs1;
      }
#else /* no yyoverflow */
# ifndef YYSTACK_RELOCATE
      goto yyexhaustedlab;
# else
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
	goto yyexhaustedlab;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
	yystacksize = YYMAXDEPTH;

      {
	yytype_int16 *yyss1 = yyss;
	union yyalloc *yyptr =
	  (union yyalloc *) YYSTACK_ALLOC (YYSTACK_BYTES (yystacksize));
	if (! yyptr)
	  goto yyexhaustedlab;
	YYSTACK_RELOCATE (yyss_alloc, yyss);
	YYSTACK_RELOCATE (yyvs_alloc, yyvs);
#  undef YYSTACK_RELOCATE
	if (yyss1 != yyssa)
	  YYSTACK_FREE (yyss1);
      }
# endif
#endif /* no yyoverflow */

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;

      YYDPRINTF ((stderr, "Stack size increased to %lu\n",
		  (unsigned long int) yystacksize));

      if (yyss + yystacksize - 1 <= yyssp)
	YYABORT;
    }

  YYDPRINTF ((stderr, "Entering state %d\n", yystate));

  if (yystate == YYFINAL)
    YYACCEPT;

  goto yybackup;

/*-----------.
| yybackup.  |
`-----------*/
yybackup:

  /* Do appropriate processing given the current state.  Read a
     lookahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to lookahead token.  */
  yyn = yypact[yystate];
  if (yypact_value_is_default (yyn))
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* YYCHAR is either YYEMPTY or YYEOF or a valid lookahead symbol.  */
  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token: "));
      yychar = YYLEX;
    }

  if (yychar <= YYEOF)
    {
      yychar = yytoken = YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yytable_value_is_error (yyn))
        goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  /* Shift the lookahead token.  */
  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);

  /* Discard the shifted token.  */
  yychar = YYEMPTY;

  yystate = yyn;
  *++yyvsp = yylval;

  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- Do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     `$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];


  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
        case 4:

/* Line 1806 of yacc.c  */
#line 113 "parser.yy"
    {(yyval.vec) = (yyvsp[(2) - (3)].vec);}
    break;

  case 5:

/* Line 1806 of yacc.c  */
#line 116 "parser.yy"
    {(yyval.vec) = new std::vector<int>();}
    break;

  case 6:

/* Line 1806 of yacc.c  */
#line 117 "parser.yy"
    {(yyval.vec) = new std::vector<int>(); (yyval.vec)->push_back((yyvsp[(1) - (1)].val));}
    break;

  case 7:

/* Line 1806 of yacc.c  */
#line 118 "parser.yy"
    {(yyval.vec) = (yyvsp[(1) - (3)].vec); (yyval.vec)->push_back((yyvsp[(3) - (3)].val));}
    break;

  case 8:

/* Line 1806 of yacc.c  */
#line 121 "parser.yy"
    {(yyval.string_vec) = (yyvsp[(2) - (3)].string_vec);}
    break;

  case 9:

/* Line 1806 of yacc.c  */
#line 124 "parser.yy"
    {(yyval.string_vec) = new std::vector<std::string>();}
    break;

  case 10:

/* Line 1806 of yacc.c  */
#line 125 "parser.yy"
    {(yyval.string_vec) = new std::vector<std::string>(); (yyval.string_vec)->push_back((yyvsp[(1) - (1)].name));}
    break;

  case 11:

/* Line 1806 of yacc.c  */
#line 126 "parser.yy"
    {(yyval.string_vec) = (yyvsp[(1) - (3)].string_vec); (yyval.string_vec)->push_back((yyvsp[(3) - (3)].name));}
    break;

  case 12:

/* Line 1806 of yacc.c  */
#line 129 "parser.yy"
    {(yyval.mat) = (yyvsp[(2) - (3)].mat);}
    break;

  case 13:

/* Line 1806 of yacc.c  */
#line 131 "parser.yy"
    {(yyval.mat) = new std::vector<std::vector<int> >(); (yyval.mat)->push_back(*(yyvsp[(1) - (1)].vec)); delete (yyvsp[(1) - (1)].vec);}
    break;

  case 14:

/* Line 1806 of yacc.c  */
#line 132 "parser.yy"
    {(yyval.mat) = (yyvsp[(1) - (3)].mat); (yyval.mat)->push_back(*(yyvsp[(3) - (3)].vec)); delete (yyvsp[(3) - (3)].vec);}
    break;

  case 15:

/* Line 1806 of yacc.c  */
#line 134 "parser.yy"
    {(yyval.val) = (yyvsp[(1) - (1)].val);}
    break;

  case 16:

/* Line 1806 of yacc.c  */
#line 135 "parser.yy"
    {
       std::map<std::string, int>::iterator it = parameter_tab.find(std::string((yyvsp[(1) - (1)].name)));
       if (it != parameter_tab.end()) {
         (yyval.val) = it->second;
         delete [](yyvsp[(1) - (1)].name);
       }
       else {
         if (is_interactive)
           fprintf(stderr, "variable \"%s\" undefined\n", (yyvsp[(1) - (1)].name));
         else
           fprintf(stderr, "variable \"%s\" undefined at line %d\n", (yyvsp[(1) - (1)].name), lexer.lineno());
         delete [](yyvsp[(1) - (1)].name);
         if (!is_interactive)
           exit(2);
       }
     }
    break;

  case 17:

/* Line 1806 of yacc.c  */
#line 151 "parser.yy"
    {
       if (myloop == NULL)
         (yyval.val) = 0;
       else
         (yyval.val) = myloop->num_statement();
     }
    break;

  case 18:

/* Line 1806 of yacc.c  */
#line 157 "parser.yy"
    {
       (yyval.val) = ceil((yyvsp[(3) - (4)].fval));
     }
    break;

  case 19:

/* Line 1806 of yacc.c  */
#line 160 "parser.yy"
    {
       (yyval.val) = floor((yyvsp[(3) - (4)].fval));
     }
    break;

  case 20:

/* Line 1806 of yacc.c  */
#line 163 "parser.yy"
    {(yyval.val) = (yyvsp[(2) - (3)].val);}
    break;

  case 21:

/* Line 1806 of yacc.c  */
#line 164 "parser.yy"
    {(yyval.val) = (yyvsp[(1) - (3)].val)-(yyvsp[(3) - (3)].val);}
    break;

  case 22:

/* Line 1806 of yacc.c  */
#line 165 "parser.yy"
    {(yyval.val) = (yyvsp[(1) - (3)].val)+(yyvsp[(3) - (3)].val);}
    break;

  case 23:

/* Line 1806 of yacc.c  */
#line 166 "parser.yy"
    {(yyval.val) = (yyvsp[(1) - (3)].val)*(yyvsp[(3) - (3)].val);}
    break;

  case 24:

/* Line 1806 of yacc.c  */
#line 167 "parser.yy"
    {(yyval.val) = (yyvsp[(1) - (3)].val)/(yyvsp[(3) - (3)].val);}
    break;

  case 25:

/* Line 1806 of yacc.c  */
#line 168 "parser.yy"
    {(yyval.val) = -(yyvsp[(2) - (2)].val);}
    break;

  case 26:

/* Line 1806 of yacc.c  */
#line 171 "parser.yy"
    {(yyval.fval) = (yyvsp[(1) - (1)].val);}
    break;

  case 27:

/* Line 1806 of yacc.c  */
#line 172 "parser.yy"
    {
             std::map<std::string, int>::iterator it = parameter_tab.find(std::string((yyvsp[(1) - (1)].name)));
             if (it != parameter_tab.end()) {
               (yyval.fval) = it->second;
               delete [](yyvsp[(1) - (1)].name);
             }
             else {
               if (is_interactive)
                 fprintf(stderr, "variable \"%s\" undefined\n", (yyvsp[(1) - (1)].name));
               else
                 fprintf(stderr, "variable \"%s\" undefined at line %d\n", (yyvsp[(1) - (1)].name), lexer.lineno());
               delete [](yyvsp[(1) - (1)].name);
               if (!is_interactive)
                 exit(2);
             }
           }
    break;

  case 28:

/* Line 1806 of yacc.c  */
#line 188 "parser.yy"
    {
             if (myloop == NULL)
               (yyval.fval) = 0;
             else
               (yyval.fval) = myloop->num_statement();
           }
    break;

  case 29:

/* Line 1806 of yacc.c  */
#line 194 "parser.yy"
    {
             (yyval.fval) = ceil((yyvsp[(3) - (4)].fval));
           }
    break;

  case 30:

/* Line 1806 of yacc.c  */
#line 197 "parser.yy"
    {
             (yyval.fval) = floor((yyvsp[(3) - (4)].fval));
           }
    break;

  case 31:

/* Line 1806 of yacc.c  */
#line 200 "parser.yy"
    {(yyval.fval) = (yyvsp[(2) - (3)].fval);}
    break;

  case 32:

/* Line 1806 of yacc.c  */
#line 201 "parser.yy"
    {(yyval.fval) = (yyvsp[(1) - (3)].fval)-(yyvsp[(3) - (3)].fval);}
    break;

  case 33:

/* Line 1806 of yacc.c  */
#line 202 "parser.yy"
    {(yyval.fval) = (yyvsp[(1) - (3)].fval)+(yyvsp[(3) - (3)].fval);}
    break;

  case 34:

/* Line 1806 of yacc.c  */
#line 203 "parser.yy"
    {(yyval.fval) = (yyvsp[(1) - (3)].fval)*(yyvsp[(3) - (3)].fval);}
    break;

  case 35:

/* Line 1806 of yacc.c  */
#line 204 "parser.yy"
    {(yyval.fval) = (yyvsp[(1) - (3)].fval)/(yyvsp[(3) - (3)].fval);}
    break;

  case 36:

/* Line 1806 of yacc.c  */
#line 205 "parser.yy"
    {(yyval.fval) = -(yyvsp[(2) - (2)].fval);}
    break;

  case 37:

/* Line 1806 of yacc.c  */
#line 209 "parser.yy"
    {
       for (std::map<std::string, int>::iterator it = (yyvsp[(3) - (3)].tab)->begin(); it != (yyvsp[(3) - (3)].tab)->end(); it++)
         (*(yyvsp[(1) - (3)].tab))[it->first] -= it->second;
       (yyval.tab_lst) = new std::vector<std::map<std::string, int> >();
       (yyval.tab_lst)->push_back(*(yyvsp[(1) - (3)].tab));
       delete (yyvsp[(1) - (3)].tab);
       delete (yyvsp[(3) - (3)].tab);
     }
    break;

  case 38:

/* Line 1806 of yacc.c  */
#line 217 "parser.yy"
    {
       for (std::map<std::string, int>::iterator it = (yyvsp[(3) - (3)].tab)->begin(); it != (yyvsp[(3) - (3)].tab)->end(); it++)
         (*(yyvsp[(1) - (3)].tab))[it->first] -= it->second;
       (yyval.tab_lst) = new std::vector<std::map<std::string, int> >();
       (*(yyvsp[(1) - (3)].tab))[to_string(0)] -= 1;
       (yyval.tab_lst)->push_back(*(yyvsp[(1) - (3)].tab));
       delete (yyvsp[(1) - (3)].tab);
       delete (yyvsp[(3) - (3)].tab);
     }
    break;

  case 39:

/* Line 1806 of yacc.c  */
#line 226 "parser.yy"
    {
       for (std::map<std::string, int>::iterator it = (yyvsp[(1) - (3)].tab)->begin(); it != (yyvsp[(1) - (3)].tab)->end(); it++)
         (*(yyvsp[(3) - (3)].tab))[it->first] -= it->second;
       (yyval.tab_lst) = new std::vector<std::map<std::string, int> >();
       (yyval.tab_lst)->push_back(*(yyvsp[(3) - (3)].tab));
       delete (yyvsp[(1) - (3)].tab);
       delete (yyvsp[(3) - (3)].tab);
     }
    break;

  case 40:

/* Line 1806 of yacc.c  */
#line 234 "parser.yy"
    {
       for (std::map<std::string, int>::iterator it = (yyvsp[(1) - (3)].tab)->begin(); it != (yyvsp[(1) - (3)].tab)->end(); it++)
         (*(yyvsp[(3) - (3)].tab))[it->first] -= it->second;
       (yyval.tab_lst) = new std::vector<std::map<std::string, int> >();
       (*(yyvsp[(3) - (3)].tab))[to_string(0)] -= 1;
       (yyval.tab_lst)->push_back(*(yyvsp[(3) - (3)].tab));
       delete (yyvsp[(1) - (3)].tab);
       delete (yyvsp[(3) - (3)].tab);
     }
    break;

  case 41:

/* Line 1806 of yacc.c  */
#line 243 "parser.yy"
    {
       for (std::map<std::string, int>::iterator it = (yyvsp[(3) - (3)].tab)->begin(); it != (yyvsp[(3) - (3)].tab)->end(); it++)
         (*(yyvsp[(1) - (3)].tab))[it->first] -= it->second;
       (yyval.tab_lst) = new std::vector<std::map<std::string, int> >();
       (yyval.tab_lst)->push_back(*(yyvsp[(1) - (3)].tab));
       for (std::map<std::string, int>::iterator it = (yyvsp[(1) - (3)].tab)->begin(); it != (yyvsp[(1) - (3)].tab)->end(); it++)
         it->second = -it->second;
       (yyval.tab_lst)->push_back(*(yyvsp[(1) - (3)].tab));
       delete (yyvsp[(1) - (3)].tab);
       delete (yyvsp[(3) - (3)].tab);
     }
    break;

  case 42:

/* Line 1806 of yacc.c  */
#line 256 "parser.yy"
    {(yyval.tab) = new std::map<std::string, int>(); (*(yyval.tab))[to_string(0)] = (yyvsp[(1) - (1)].val);}
    break;

  case 43:

/* Line 1806 of yacc.c  */
#line 257 "parser.yy"
    {(yyval.tab) = new std::map<std::string, int>(); (*(yyval.tab))[to_string((yyvsp[(1) - (1)].val))] = 1;}
    break;

  case 44:

/* Line 1806 of yacc.c  */
#line 258 "parser.yy"
    {
            (yyval.tab) = new std::map<std::string, int>();

            std::map<std::string, int>::iterator it = parameter_tab.find(std::string((yyvsp[(1) - (1)].name)));
            if (it != parameter_tab.end())
              (*(yyval.tab))[to_string(0)] = it->second;
            else
             (*(yyval.tab))[std::string((yyvsp[(1) - (1)].name))] = 1;

            delete [](yyvsp[(1) - (1)].name);
          }
    break;

  case 45:

/* Line 1806 of yacc.c  */
#line 269 "parser.yy"
    {(yyval.tab) = (yyvsp[(2) - (3)].tab);}
    break;

  case 46:

/* Line 1806 of yacc.c  */
#line 270 "parser.yy"
    {
            for (std::map<std::string, int>::iterator it = (yyvsp[(3) - (3)].tab)->begin(); it != (yyvsp[(3) - (3)].tab)->end(); it++)
              (*(yyvsp[(1) - (3)].tab))[it->first] -= it->second;
            (yyval.tab) = (yyvsp[(1) - (3)].tab);
            delete (yyvsp[(3) - (3)].tab);
          }
    break;

  case 47:

/* Line 1806 of yacc.c  */
#line 276 "parser.yy"
    {
            for (std::map<std::string, int>::iterator it = (yyvsp[(3) - (3)].tab)->begin(); it != (yyvsp[(3) - (3)].tab)->end(); it++)
              (*(yyvsp[(1) - (3)].tab))[it->first] += it->second;
            (yyval.tab) = (yyvsp[(1) - (3)].tab);
            delete (yyvsp[(3) - (3)].tab);
          }
    break;

  case 48:

/* Line 1806 of yacc.c  */
#line 282 "parser.yy"
    {
            (*(yyvsp[(1) - (3)].tab))[to_string(0)] += 0;
            (*(yyvsp[(3) - (3)].tab))[to_string(0)] += 0;
            if ((yyvsp[(1) - (3)].tab)->size() == 1) {
              int t = (*(yyvsp[(1) - (3)].tab))[to_string(0)];
              for (std::map<std::string, int>::iterator it = (yyvsp[(3) - (3)].tab)->begin(); it != (yyvsp[(3) - (3)].tab)->end(); it++)
                it->second *= t;
              (yyval.tab) = (yyvsp[(3) - (3)].tab);
              delete (yyvsp[(1) - (3)].tab);
            }
            else if ((yyvsp[(3) - (3)].tab)->size() == 1) {
              int t = (*(yyvsp[(3) - (3)].tab))[to_string(0)];
              for (std::map<std::string, int>::iterator it = (yyvsp[(1) - (3)].tab)->begin(); it != (yyvsp[(1) - (3)].tab)->end(); it++)
                it->second *= t;
              (yyval.tab) = (yyvsp[(1) - (3)].tab);
              delete (yyvsp[(3) - (3)].tab);
            }
            else {
              if (is_interactive)
                fprintf(stderr, "require Presburger formula\n");
              else
                fprintf(stderr, "require Presburger formula at line %d\n", lexer.lineno());
              delete (yyvsp[(1) - (3)].tab);
              delete (yyvsp[(3) - (3)].tab);
              exit(2);
            }
          }
    break;

  case 49:

/* Line 1806 of yacc.c  */
#line 309 "parser.yy"
    {
            for (std::map<std::string, int>::iterator it = (yyvsp[(2) - (2)].tab)->begin(); it != (yyvsp[(2) - (2)].tab)->end(); it++)
              it->second = -(it->second);
            (yyval.tab) = (yyvsp[(2) - (2)].tab);
          }
    break;

  case 50:

/* Line 1806 of yacc.c  */
#line 316 "parser.yy"
    { if (is_interactive) printf("%s ", PROMPT_STRING); }
    break;

  case 51:

/* Line 1806 of yacc.c  */
#line 317 "parser.yy"
    { if (!is_interactive) exit(2); else printf("%s ", PROMPT_STRING); }
    break;

  case 52:

/* Line 1806 of yacc.c  */
#line 318 "parser.yy"
    {
          if (source_filename != NULL) {
            fprintf(stderr, "only one file can be handle in a script");
            PRINT_ERR_LINENO;
            if (!is_interactive)
              exit(2);
          }
          source_filename = (yyvsp[(3) - (4)].name);
          if (is_interactive)
            printf("%s ", PROMPT_STRING);
        }
    break;

  case 53:

/* Line 1806 of yacc.c  */
#line 329 "parser.yy"
    {


          if (procedure_name != NULL) {
            fprintf(stderr, "only one procedure can be handled in a script");
            PRINT_ERR_LINENO;
            if (!is_interactive)
              exit(2);
          }
          procedure_name = (yyvsp[(3) - (4)].name);
          if (is_interactive)
            printf("%s ", PROMPT_STRING);
        }
    break;

  case 54:

/* Line 1806 of yacc.c  */
#line 342 "parser.yy"
    {

            fprintf(stderr, "Please specify procedure's name and not number!!");
            PRINT_ERR_LINENO;
            if (!is_interactive)
              exit(2);
        
        }
    break;

  case 55:

/* Line 1806 of yacc.c  */
#line 350 "parser.yy"
    {
          if (cit_name != CIT_NULL) {
            fprintf(stderr, "compiler intermediate format already specified");
            PRINT_ERR_LINENO;
            delete [](yyvsp[(3) - (4)].name);
            if (!is_interactive)
              exit(2);
          }
          else {
            
            if(std::string((yyvsp[(3) - (4)].name)) == "rose" || std::string((yyvsp[(3) - (4)].name)) == "ROSE") {
              cit_name = CIT_ROSE;
              delete [](yyvsp[(3) - (4)].name);
            }   
            else {
              fprintf(stderr, "unrecognized IR format");
              PRINT_ERR_LINENO;
              delete [](yyvsp[(3) - (4)].name);
              if (!is_interactive)
                exit(2);
            }
          }
          if (is_interactive)
            printf("%s ", PROMPT_STRING);
        }
    break;

  case 56:

/* Line 1806 of yacc.c  */
#line 375 "parser.yy"
    {
          if (source_filename == NULL) {
            fprintf(stderr, "source file not set when initializing the loop");
            PRINT_ERR_LINENO;
            if (!is_interactive)
              exit(2);
          }
          else {
          if (ir_code == NULL) {
            if (procedure_name == NULL)
              procedure_name = "main";
           
              switch (cit_name) {
              case CIT_ROSE:
                 ir_code = new IR_roseCode(source_filename, procedure_name);
                break; 
              case CIT_NULL:
                fprintf(stderr, "compiler IR format not specified");
                PRINT_ERR_LINENO;
                if (!is_interactive)
                  exit(2);
                break;
              }
            
            IR_Block *block = ir_code->GetCode();
            ir_controls = ir_code->FindOneLevelControlStructure(block);
            for (int i = 0; i < ir_controls.size(); i++)
              if (ir_controls[i]->type() == IR_CONTROL_LOOP)
                loops.push_back(i);
            delete block;
            }
            if (myloop != NULL && myloop->isInitialized()) {
              if (loop_num_start == loop_num_end) {
                ir_code->ReplaceCode(ir_controls[loops[loop_num_start]], myloop->getCode());
                ir_controls[loops[loop_num_start]] = NULL;
              }
              else {
                std::vector<IR_Control *> parm;
                for (int i = loops[loop_num_start]; i <= loops[loop_num_end]; i++)
                  parm.push_back(ir_controls[i]);
                IR_Block *block = ir_code->MergeNeighboringControlStructures(parm);
                ir_code->ReplaceCode(block, myloop->getCode());
                for (int i = loops[loop_num_start]; i <= loops[loop_num_end]; i++) {
                  delete ir_controls[i];
                  ir_controls[i] = NULL;
                }
              } 
              delete myloop;
            }
            loop_num_start = loop_num_end = (yyvsp[(3) - (4)].val);
            if (loop_num_start >= loops.size()) {
              fprintf(stderr, "loop %d does not exist", loop_num_start);
              PRINT_ERR_LINENO;
              if (!is_interactive)
                exit(2);
            }
            if (ir_controls[loops[loop_num_start]] == NULL) {
              fprintf(stderr, "loop %d has already be transformed", loop_num_start);
              PRINT_ERR_LINENO;
              if (!is_interactive)
                exit(2);
            }
            myloop = new Loop(ir_controls[loops[loop_num_start]]);
          }
          if (is_interactive) printf("%s ", PROMPT_STRING);
        }
    break;

  case 57:

/* Line 1806 of yacc.c  */
#line 441 "parser.yy"
    {
          if (source_filename == NULL) {
            fprintf(stderr, "source file not set when initializing the loop");
            PRINT_ERR_LINENO;
            if (!is_interactive)
              exit(2);
          }
          else {
           if (ir_code == NULL) {
            if (procedure_name == NULL)
              procedure_name = "main";
            
              switch (cit_name) {
              case CIT_ROSE:
                 ir_code = new IR_roseCode(source_filename, procedure_name);
                break;
              case CIT_NULL:
                fprintf(stderr, "compiler IR format not specified");
                PRINT_ERR_LINENO;
                if (!is_interactive)
                  exit(2);
                break;
              }
            
           
 
              IR_Block *block = ir_code->GetCode();
              ir_controls = ir_code->FindOneLevelControlStructure(block);
              for (int i = 0; i < ir_controls.size(); i++)
                if (ir_controls[i]->type() == IR_CONTROL_LOOP)
                  loops.push_back(i);
              delete block;
           }                          
              if (myloop != NULL && myloop->isInitialized()) {
                if (loop_num_start == loop_num_end) {
                  ir_code->ReplaceCode(ir_controls[loops[loop_num_start]], myloop->getCode());
                  ir_controls[loops[loop_num_start]] = NULL;
                }
                else {
                  std::vector<IR_Control *> parm;
                  for (int i = loops[loop_num_start]; i <= loops[loop_num_end]; i++)
                    parm.push_back(ir_controls[i]);
                  IR_Block *block = ir_code->MergeNeighboringControlStructures(parm);
                  ir_code->ReplaceCode(block, myloop->getCode());
                  for (int i = loops[loop_num_start]; i <= loops[loop_num_end]; i++) {
                    delete ir_controls[i];
                    ir_controls[i] = NULL;
                  }
                }
                delete myloop;
              }
              loop_num_start = (yyvsp[(3) - (6)].val);
              loop_num_end = (yyvsp[(5) - (6)].val);
              if ((yyvsp[(5) - (6)].val) < (yyvsp[(3) - (6)].val)) {
                fprintf(stderr, "the last loop must be after the start loop");
                PRINT_ERR_LINENO;
                if (!is_interactive)
                  exit(2);
              }              
              if (loop_num_end >= loops.size()) {
                fprintf(stderr, "loop %d does not exist", loop_num_end);
                PRINT_ERR_LINENO;
                if (!is_interactive)
                  exit(2);
              }
              std::vector<IR_Control *> parm;
              for (int i = loops[loop_num_start]; i <= loops[loop_num_end]; i++) {
                if (ir_controls[i] == NULL) {
                  fprintf(stderr, "loop has already been processed");
                  PRINT_ERR_LINENO;
                  if (!is_interactive)
                    exit(2);
                }
                parm.push_back(ir_controls[i]);
              }
              IR_Block *block = ir_code->MergeNeighboringControlStructures(parm);
              myloop = new Loop(block);
              delete block;
            
          }
          if (is_interactive) printf("%s ", PROMPT_STRING);
        }
    break;

  case 58:

/* Line 1806 of yacc.c  */
#line 523 "parser.yy"
    {
          if (myloop == NULL) {
            fprintf(stderr, "loop not initialized");
            PRINT_ERR_LINENO;
            if (!is_interactive)
              exit(2);
          }
          else {
            myloop->printCode();
          }
          if (is_interactive) printf("%s ", PROMPT_STRING); else printf("\n");
        }
    break;

  case 59:

/* Line 1806 of yacc.c  */
#line 535 "parser.yy"
    {
          if (myloop == NULL) {
            fprintf(stderr, "loop not initialized");
            PRINT_ERR_LINENO;
            if (!is_interactive)
              exit(2);
          }
          else {
            if (!is_interactive) {
              if (loop_num_start != loop_num_end)
                std::cout << "/* procedure :" << procedure_name << " loop #" << loop_num_start << "-" << loop_num_end << " */" << std::endl;
              else
                std::cout << "/* procedure :" << procedure_name << " loop #" << loop_num_start << " */" << std::endl;

           } 

            myloop->printCode();
          }
          if (is_interactive) printf("%s ", PROMPT_STRING); else printf("\n");
        }
    break;

  case 60:

/* Line 1806 of yacc.c  */
#line 555 "parser.yy"
    {
          if (myloop == NULL) {
            fprintf(stderr, "loop not initialized");
            PRINT_ERR_LINENO;
            if (!is_interactive)
              YYABORT;
          }
          else {
            myloop->printDependenceGraph();
          }
          if (is_interactive) printf("%s ", PROMPT_STRING); else printf("\n");
        }
    break;

  case 61:

/* Line 1806 of yacc.c  */
#line 567 "parser.yy"
    {
          if (myloop == NULL) {
            fprintf(stderr, "loop not initialized");
            PRINT_ERR_LINENO;
            if (!is_interactive)
              YYABORT;
          }
          else {
            myloop->printIterationSpace();
          }
          if (is_interactive) printf("%s ", PROMPT_STRING); else printf("\n");
        }
    break;

  case 62:

/* Line 1806 of yacc.c  */
#line 579 "parser.yy"
    {
          if (myloop == NULL) {
            fprintf(stderr, "loop not initialized");
            PRINT_ERR_LINENO;
            if (!is_interactive)
              exit(2);
          }
          else {
            myloop->print_internal_loop_structure();
          }
          if (is_interactive) printf("%s ", PROMPT_STRING); else printf("\n");
        }
    break;

  case 63:

/* Line 1806 of yacc.c  */
#line 591 "parser.yy"
    {
/*          if (parameter_tab.find(std::string($2)) == parameter_tab.end()) {
            fprintf(stderr, "cannot print undefined variable %s\n", $2);
            PRINT_ERR_LINENO;
            if (!is_interactive)
              exit(2);
          }
          std::cout << parameter_tab[std::string($2)] << std::endl;
*/
          std::cout << (yyvsp[(2) - (3)].val) << std::endl;
          if (is_interactive) printf("%s ", PROMPT_STRING); else printf("\n");
        }
    break;

  case 64:

/* Line 1806 of yacc.c  */
#line 603 "parser.yy"
    { return(0); }
    break;

  case 65:

/* Line 1806 of yacc.c  */
#line 604 "parser.yy"
    {
          parameter_tab[std::string((yyvsp[(1) - (4)].name))] = (yyvsp[(3) - (4)].val);
          delete [](yyvsp[(1) - (4)].name);
          if (is_interactive) printf("%s ", PROMPT_STRING);
        }
    break;

  case 66:

/* Line 1806 of yacc.c  */
#line 609 "parser.yy"
    {
          try {
            if (myloop == NULL)
              throw std::runtime_error("loop not initialized");
            
            int num_dim = myloop->known.n_set();
            Relation rel(num_dim);
            F_And *f_root = rel.add_and();
            for (int j = 0; j < (yyvsp[(3) - (5)].tab_lst)->size(); j++) {
              GEQ_Handle h = f_root->add_GEQ();
              /*for (std::map<std::string, int>::iterator it = (*$3)[j].begin(); it != (*$3)[j].end(); it++) {
               try {
                  int dim = from_string<int>(it->first);
                  if (dim == 0)
                    h.update_const(it->second);
                  else
                  	throw std::invalid_argument("only symbolic variables are allowed in known condition");
                }
                catch (std::ios::failure e) {
                */
               for (std::map<std::string, int>::iterator it = (*(yyvsp[(3) - (5)].tab_lst))[j].begin(); it != (*(yyvsp[(3) - (5)].tab_lst))[j].end(); it++) {
                 std::istringstream ss(it->first);
                 ss.exceptions(std::ios::failbit);
                 try {

                  int dim;
                  ss >> dim;
                  if (dim == 0)
                    h.update_const(it->second);
                  else
                   throw std::invalid_argument("only symbolic variables are allowed in known condition");
                }
                catch (const std::exception &e) {      

                  Free_Var_Decl *g = NULL;
                  for (unsigned i = 0; i < myloop->freevar.size(); i++) {
                    std::string name = myloop->freevar[i]->base_name();
                    if (name == it->first) {
                      g = myloop->freevar[i];
                      break;
                    }
                  }
                  if (g == NULL){
		    g =  new Free_Var_Decl(it->first);

       		    h.update_coef(rel.get_local(g), it->second);
		
		    myloop->freevar.push_back(g);			
                    //throw std::invalid_argument("symbolic variable " + it->first + " not found");
                  }else
		    if(g->arity() == 0) 			
                    h.update_coef(rel.get_local(g), it->second);
		    else
                    h.update_coef(rel.get_local(g, Input_Tuple), it->second);
				
                }
              }
            }
            myloop->addKnown(rel);
          }
          catch (const std::exception &e) {
            fprintf(stderr, e.what());
            PRINT_ERR_LINENO;
            if (!is_interactive) {
              delete (yyvsp[(3) - (5)].tab_lst);
              exit(2);
            }
          }
          delete (yyvsp[(3) - (5)].tab_lst);
          if (is_interactive) printf("%s ", PROMPT_STRING);
        }
    break;

  case 67:

/* Line 1806 of yacc.c  */
#line 680 "parser.yy"
    {
          try {
            if (myloop == NULL)
              throw std::runtime_error("loop not initialized");

            myloop->removeDependence((yyvsp[(3) - (7)].val), (yyvsp[(5) - (7)].val));
          }
          catch (const std::exception &e) {
            fprintf(stderr, e.what());
            PRINT_ERR_LINENO;
            if (!is_interactive)
              YYABORT;
          }
          if (is_interactive) printf("%s ", PROMPT_STRING);
        }
    break;

  case 68:

/* Line 1806 of yacc.c  */
#line 695 "parser.yy"
    {
          try {
            if (myloop == NULL)
              throw std::runtime_error("loop not initialized");

            myloop->original();
          }
          catch (const std::exception &e) {
            fprintf(stderr, e.what());
            PRINT_ERR_LINENO;
            if (!is_interactive)
              exit(2);
          }
          if (is_interactive) printf("%s ", PROMPT_STRING);
        }
    break;

  case 69:

/* Line 1806 of yacc.c  */
#line 710 "parser.yy"
    {
          try {
            if (myloop == NULL)
              throw std::runtime_error("loop not initialized");

            myloop->permute(*(yyvsp[(3) - (5)].vec));
          }
          catch (const std::exception &e) {
            fprintf(stderr, e.what());
            PRINT_ERR_LINENO;
            if (!is_interactive) {
              delete (yyvsp[(3) - (5)].vec);
              exit(2);
            }
          }
          delete (yyvsp[(3) - (5)].vec);
          if (is_interactive) printf("%s ", PROMPT_STRING);
        }
    break;

  case 70:

/* Line 1806 of yacc.c  */
#line 728 "parser.yy"
    {
          try {
            if (myloop == NULL)
              throw std::runtime_error("loop not initialized");

            myloop->permute((yyvsp[(3) - (9)].val), (yyvsp[(5) - (9)].val), *(yyvsp[(7) - (9)].vec));
          }
          catch (const std::exception &e) {
            fprintf(stderr, e.what());
            PRINT_ERR_LINENO;
            if (!is_interactive) {
              delete (yyvsp[(7) - (9)].vec);
              exit(2);
            }
          }
          delete (yyvsp[(7) - (9)].vec);
          if (is_interactive) printf("%s ", PROMPT_STRING);
        }
    break;

  case 71:

/* Line 1806 of yacc.c  */
#line 746 "parser.yy"
    {
          try {
            if (myloop == NULL)
              throw std::runtime_error("loop not initialized");

            std::set<int> active;
            for (int i = 0; i < (*(yyvsp[(3) - (7)].vec)).size(); i++)
              active.insert((*(yyvsp[(3) - (7)].vec))[i]);
            
            myloop->permute(active, *(yyvsp[(5) - (7)].vec));
          }
          catch (const std::exception &e) {
            fprintf(stderr, e.what());
            PRINT_ERR_LINENO;
            if (!is_interactive) {
              delete (yyvsp[(3) - (7)].vec);
              delete (yyvsp[(5) - (7)].vec);
              exit(2);
            }
          }
          delete (yyvsp[(3) - (7)].vec);
          delete (yyvsp[(5) - (7)].vec);
          if (is_interactive) printf("%s ", PROMPT_STRING);
        }
    break;

  case 72:

/* Line 1806 of yacc.c  */
#line 770 "parser.yy"
    {
          try {
            if (myloop == NULL)
              throw std::runtime_error("loop not initialized");

            myloop->tile((yyvsp[(3) - (9)].val),(yyvsp[(5) - (9)].val),(yyvsp[(7) - (9)].val));
          }
          catch (const std::exception &e) {
            fprintf(stderr, e.what());
            PRINT_ERR_LINENO;
            if (!is_interactive)
              exit(2);
          }
          if (is_interactive) printf("%s ", PROMPT_STRING);
        }
    break;

  case 73:

/* Line 1806 of yacc.c  */
#line 785 "parser.yy"
    {
          try {
            if (myloop == NULL)
              throw std::runtime_error("loop not initialized");
             std::vector<int> loop_levels;
            for (int i = 0; i < (*(yyvsp[(7) - (11)].vec)).size(); i++)
              loop_levels.push_back((*(yyvsp[(7) - (11)].vec))[i]);

            const char* index= (yyvsp[(5) - (11)].name); 
            std::string str1(index);
            const char* inspector= (yyvsp[(9) - (11)].name); 
            std::string str2(inspector);
            myloop->flatten((yyvsp[(3) - (11)].val),str1, loop_levels,str2);
          }
          catch (const std::exception &e) {
            fprintf(stderr, e.what());
            PRINT_ERR_LINENO;
            if (!is_interactive)
              exit(2);
          }
          if (is_interactive) printf("%s ", PROMPT_STRING);
        }
    break;

  case 74:

/* Line 1806 of yacc.c  */
#line 807 "parser.yy"
    {
          try {
            if (myloop == NULL)
              throw std::runtime_error("loop not initialized");
            std::vector<std::string> arrays;
	    std::vector<int> levels;
            for (int i = 0; i < (*(yyvsp[(5) - (13)].vec)).size(); i++)
              levels.push_back((*(yyvsp[(5) - (13)].vec))[i]);
	    for (int i = 0; i < (*(yyvsp[(7) - (13)].string_vec)).size(); i++)
              arrays.push_back((*(yyvsp[(7) - (13)].string_vec))[i]);

            std::vector<std::string> arrays2;
            for (int i = 0; i < (*(yyvsp[(11) - (13)].string_vec)).size(); i++)
              arrays2.push_back((*(yyvsp[(11) - (13)].string_vec))[i]);
    
            myloop->compact((yyvsp[(3) - (13)].val),levels, arrays, (yyvsp[(9) - (13)].val), arrays2);
          }
          catch (const std::exception &e) {
            fprintf(stderr, e.what());
            PRINT_ERR_LINENO;
            if (!is_interactive)
              exit(2);
          }
          if (is_interactive) printf("%s ", PROMPT_STRING);
        }
    break;

  case 75:

/* Line 1806 of yacc.c  */
#line 832 "parser.yy"
    {
          try {
            if (myloop == NULL)
              throw std::runtime_error("loop not initialized");
            std::vector<std::string> arrays;
            for (int i = 0; i < (*(yyvsp[(7) - (15)].string_vec)).size(); i++)
              arrays.push_back((*(yyvsp[(7) - (15)].string_vec))[i]);

            std::vector<std::string> arrays2;
            for (int i = 0; i < (*(yyvsp[(11) - (15)].string_vec)).size(); i++)
              arrays2.push_back((*(yyvsp[(11) - (15)].string_vec))[i]);
            std::vector<int> levels;
            for (int i = 0; i < (*(yyvsp[(5) - (15)].vec)).size(); i++)
              levels.push_back((*(yyvsp[(5) - (15)].vec))[i]);
            int num_dim = myloop->known.n_set();
            Relation rel(num_dim);
            F_And *f_root = rel.add_and();
            for (int j = 0; j < (yyvsp[(13) - (15)].tab_lst)->size(); j++) {
              GEQ_Handle h = f_root->add_GEQ();
              for (std::map<std::string, int>::iterator it = (*(yyvsp[(13) - (15)].tab_lst))[j].begin(); it != (*(yyvsp[(13) - (15)].tab_lst))[j].end(); it++) {
                try {
                  int dim = from_string<int>(it->first);
                  if (dim == 0)
                    h.update_const(it->second);
                  else
                  	throw std::invalid_argument("only symbolic variables are allowed in condition");
                }
                catch (std::ios::failure e) {
                  Free_Var_Decl *g = NULL;
                  for (unsigned i = 0; i < myloop->freevar.size(); i++) {
                    std::string name = myloop->freevar[i]->base_name();
                    if (name == it->first) {
                      g = myloop->freevar[i];
                      break;
                    }
                  }
                  if (g == NULL)
                    throw std::invalid_argument("symbolic variable " + it->first + " not found");
                  else
		    if(g->arity() == 0) 			
                    h.update_coef(rel.get_local(g), it->second);
		    else
                    h.update_coef(rel.get_local(g, Input_Tuple), it->second);
				
                }
              }
            }
         
			myloop->compact((yyvsp[(3) - (15)].val),levels, arrays, (yyvsp[(9) - (15)].val), arrays2, rel);
          }
          catch (const std::exception &e) {
            fprintf(stderr, e.what());
            PRINT_ERR_LINENO;
            if (!is_interactive)
              exit(2);
          }
          if (is_interactive) printf("%s ", PROMPT_STRING);
        }
    break;

  case 76:

/* Line 1806 of yacc.c  */
#line 890 "parser.yy"
    {
          try {
            if (myloop == NULL)
              throw std::runtime_error("loop not initialized");
            std::vector<std::string> arrays;
            for (int i = 0; i < (*(yyvsp[(7) - (17)].string_vec)).size(); i++)
              arrays.push_back((*(yyvsp[(7) - (17)].string_vec))[i]);

            std::vector<std::string> arrays2;
            for (int i = 0; i < (*(yyvsp[(11) - (17)].string_vec)).size(); i++)
              arrays2.push_back((*(yyvsp[(11) - (17)].string_vec))[i]);
            std::vector<int> levels;
            for (int i = 0; i < (*(yyvsp[(5) - (17)].vec)).size(); i++)
              levels.push_back((*(yyvsp[(5) - (17)].vec))[i]);
            int num_dim = myloop->known.n_set();
            Relation rel(num_dim);
            F_And *f_root = rel.add_and();
            for (int j = 0; j < (yyvsp[(13) - (17)].tab_lst)->size(); j++) {
              GEQ_Handle h = f_root->add_GEQ();
              for (std::map<std::string, int>::iterator it = (*(yyvsp[(13) - (17)].tab_lst))[j].begin(); it != (*(yyvsp[(13) - (17)].tab_lst))[j].end(); it++) {
                try {
                  int dim = from_string<int>(it->first);
                  if (dim == 0)
                    h.update_const(it->second);
                  else
                  	throw std::invalid_argument("only symbolic variables are allowed in condition");
                }
                catch (std::ios::failure e) {
                  Free_Var_Decl *g = NULL;
                  for (unsigned i = 0; i < myloop->freevar.size(); i++) {
                    std::string name = myloop->freevar[i]->base_name();
                    if (name == it->first) {
                      g = myloop->freevar[i];
                      break;
                    }
                  }
                  if (g == NULL)
                    throw std::invalid_argument("symbolic variable " + it->first + " not found");
                  else
		    if(g->arity() == 0) 			
                    h.update_coef(rel.get_local(g), it->second);
		    else
                    h.update_coef(rel.get_local(g, Input_Tuple), it->second);
				
                }
              }
            }
		if((yyvsp[(15) - (17)].val) == 0)		
			myloop->compact((yyvsp[(3) - (17)].val),levels, arrays, (yyvsp[(9) - (17)].val), arrays2, rel, false);
		else
			myloop->compact((yyvsp[(3) - (17)].val),levels, arrays, (yyvsp[(9) - (17)].val), arrays2, rel, true);
          }
          catch (const std::exception &e) {
            fprintf(stderr, e.what());
            PRINT_ERR_LINENO;
            if (!is_interactive)
              exit(2);
          }
          if (is_interactive) printf("%s ", PROMPT_STRING);
        }
    break;

  case 77:

/* Line 1806 of yacc.c  */
#line 967 "parser.yy"
    {
          try {
            if (myloop == NULL)
              throw std::runtime_error("loop not initialized");

            myloop->make_dense((yyvsp[(3) - (9)].val),(yyvsp[(5) - (9)].val),(yyvsp[(7) - (9)].name));
          }
          catch (const std::exception &e) {
            fprintf(stderr, e.what());
            PRINT_ERR_LINENO;
            if (!is_interactive)
              exit(2);
          }
          if (is_interactive) printf("%s ", PROMPT_STRING);
        }
    break;

  case 78:

/* Line 1806 of yacc.c  */
#line 982 "parser.yy"
    {
          try {
            if (myloop == NULL)
              throw std::runtime_error("loop not initialized");

            myloop->set_array_size((yyvsp[(3) - (7)].name),(yyvsp[(5) - (7)].val));
          }
          catch (const std::exception &e) {
            fprintf(stderr, e.what());
            PRINT_ERR_LINENO;
            if (!is_interactive)
              exit(2);
          }
          if (is_interactive) printf("%s ", PROMPT_STRING);
        }
    break;

  case 79:

/* Line 1806 of yacc.c  */
#line 997 "parser.yy"
    {
          try {
            if (myloop == NULL)
              throw std::runtime_error("loop not initialized");
            std::vector<std::string> arrays;
            for (int i = 0; i < (*(yyvsp[(5) - (9)].string_vec)).size(); i++)
              arrays.push_back((*(yyvsp[(5) - (9)].string_vec))[i]);

            myloop->ELLify((yyvsp[(3) - (9)].val), arrays,(yyvsp[(7) - (9)].val));
          }
          catch (const std::exception &e) {
            fprintf(stderr, e.what());
            PRINT_ERR_LINENO;
            if (!is_interactive)
              exit(2);
          }
          if (is_interactive) printf("%s ", PROMPT_STRING);
        }
    break;

  case 80:

/* Line 1806 of yacc.c  */
#line 1015 "parser.yy"
    {
          try {
            if (myloop == NULL)
              throw std::runtime_error("loop not initialized");
            std::vector<std::string> arrays;
            for (int i = 0; i < (*(yyvsp[(5) - (13)].string_vec)).size(); i++)
              arrays.push_back((*(yyvsp[(5) - (13)].string_vec))[i]);

            myloop->ELLify((yyvsp[(3) - (13)].val), arrays,(yyvsp[(7) - (13)].val),(yyvsp[(9) - (13)].bool_val),(yyvsp[(11) - (13)].name));
          }
          catch (const std::exception &e) {
            fprintf(stderr, e.what());
            PRINT_ERR_LINENO;
            if (!is_interactive)
              exit(2);
          }
          if (is_interactive) printf("%s ", PROMPT_STRING);
        }
    break;

  case 81:

/* Line 1806 of yacc.c  */
#line 1033 "parser.yy"
    {
          try {
            if (myloop == NULL)
              throw std::runtime_error("loop not initialized");
            myloop->normalize((yyvsp[(3) - (7)].val), (yyvsp[(5) - (7)].val));
          }
          catch (const std::exception &e) {
            fprintf(stderr, e.what());
            PRINT_ERR_LINENO;
            if (!is_interactive)
              exit(2);
          }
          if (is_interactive) printf("%s ", PROMPT_STRING);
        }
    break;

  case 82:

/* Line 1806 of yacc.c  */
#line 1047 "parser.yy"
    {
          try {
            if (myloop == NULL)
              throw std::runtime_error("loop not initialized");

            myloop->tile((yyvsp[(3) - (11)].val),(yyvsp[(5) - (11)].val),(yyvsp[(7) - (11)].val),(yyvsp[(9) - (11)].val));
          }
          catch (const std::exception &e) {
            fprintf(stderr, e.what());
            PRINT_ERR_LINENO;
            if (!is_interactive)
              exit(2);
          }
          if (is_interactive) printf("%s ", PROMPT_STRING);
        }
    break;

  case 83:

/* Line 1806 of yacc.c  */
#line 1062 "parser.yy"
    {
          try {
            if (myloop == NULL)
              throw std::runtime_error("loop not initialized");

            myloop->tile((yyvsp[(3) - (13)].val),(yyvsp[(5) - (13)].val),(yyvsp[(7) - (13)].val),(yyvsp[(9) - (13)].val),StridedTile);
          }
          catch (const std::exception &e) {
            fprintf(stderr, e.what());
            PRINT_ERR_LINENO;
            if (!is_interactive)
              exit(2);
          }
          if (is_interactive) printf("%s ", PROMPT_STRING);
        }
    break;

  case 84:

/* Line 1806 of yacc.c  */
#line 1077 "parser.yy"
    {
          try {
            if (myloop == NULL)
              throw std::runtime_error("loop not initialized");

            myloop->tile((yyvsp[(3) - (15)].val),(yyvsp[(5) - (15)].val),(yyvsp[(7) - (15)].val),(yyvsp[(9) - (15)].val),StridedTile,(yyvsp[(13) - (15)].val));
          }
          catch (const std::exception &e) {
            fprintf(stderr, e.what());
            PRINT_ERR_LINENO;
            if (!is_interactive)
              exit(2);
          }
          if (is_interactive) printf("%s ", PROMPT_STRING);
        }
    break;

  case 85:

/* Line 1806 of yacc.c  */
#line 1092 "parser.yy"
    {
          try {
            if (myloop == NULL)
              throw std::runtime_error("loop not initialized");

            myloop->tile((yyvsp[(3) - (17)].val),(yyvsp[(5) - (17)].val),(yyvsp[(7) - (17)].val),(yyvsp[(9) - (17)].val),StridedTile,(yyvsp[(13) - (17)].val),(yyvsp[(15) - (17)].val));
          }
          catch (const std::exception &e) {
            fprintf(stderr, e.what());
            PRINT_ERR_LINENO;
            if (!is_interactive)
              exit(2);
          }
          if (is_interactive) printf("%s ", PROMPT_STRING);
        }
    break;

  case 86:

/* Line 1806 of yacc.c  */
#line 1107 "parser.yy"
    {
          try {
            if (myloop == NULL)
              throw std::runtime_error("loop not initialized");

            myloop->tile((yyvsp[(3) - (13)].val),(yyvsp[(5) - (13)].val),(yyvsp[(7) - (13)].val),(yyvsp[(9) - (13)].val),CountedTile);
          }
          catch (const std::exception &e) {
            fprintf(stderr, e.what());
            PRINT_ERR_LINENO;
            if (!is_interactive)
              exit(2);
          }
          if (is_interactive) printf("%s ", PROMPT_STRING);
        }
    break;

  case 87:

/* Line 1806 of yacc.c  */
#line 1122 "parser.yy"
    {
          try {
            if (myloop == NULL)
              throw std::runtime_error("loop not initialized");

            myloop->tile((yyvsp[(3) - (15)].val),(yyvsp[(5) - (15)].val),(yyvsp[(7) - (15)].val),(yyvsp[(9) - (15)].val),CountedTile,(yyvsp[(13) - (15)].val));
          }
          catch (const std::exception &e) {
            fprintf(stderr, e.what());
            PRINT_ERR_LINENO;
            if (!is_interactive)
              exit(2);
          }
          if (is_interactive) printf("%s ", PROMPT_STRING);
        }
    break;

  case 88:

/* Line 1806 of yacc.c  */
#line 1137 "parser.yy"
    {
          try {
            if (myloop == NULL)
              throw std::runtime_error("loop not initialized");

            myloop->tile((yyvsp[(3) - (17)].val),(yyvsp[(5) - (17)].val),(yyvsp[(7) - (17)].val),(yyvsp[(9) - (17)].val),CountedTile,(yyvsp[(13) - (17)].val),(yyvsp[(15) - (17)].val));
          }
          catch (const std::exception &e) {
            fprintf(stderr, e.what());
            PRINT_ERR_LINENO;
            if (!is_interactive)
              exit(2);
          }
          if (is_interactive) printf("%s ", PROMPT_STRING);
        }
    break;

  case 89:

/* Line 1806 of yacc.c  */
#line 1152 "parser.yy"
    {
          try {
            if (myloop == NULL)
              throw std::runtime_error("loop not initialized");
            
            std::vector<std::pair<int, std::vector<int> > > array_ref_nums((*(yyvsp[(3) - (7)].mat)).size());
            for (int i = 0; i < (*(yyvsp[(3) - (7)].mat)).size(); i++) {
              if ((*(yyvsp[(3) - (7)].mat))[i].size() <= 1)
                throw std::invalid_argument("statement missing in the first parameter");
              array_ref_nums[i].first = (*(yyvsp[(3) - (7)].mat))[i][0];
              for (int j = 1; j < (*(yyvsp[(3) - (7)].mat))[i].size(); j++)
                array_ref_nums[i].second.push_back((*(yyvsp[(3) - (7)].mat))[i][j]);
            }
            myloop->datacopy(array_ref_nums,(yyvsp[(5) - (7)].val));
          }
          catch (const std::exception &e) {
            fprintf(stderr, e.what());
            PRINT_ERR_LINENO;
            if (!is_interactive) {
              delete (yyvsp[(3) - (7)].mat);
              exit(2);
            }
          }
          delete (yyvsp[(3) - (7)].mat);
          if (is_interactive) printf("%s ", PROMPT_STRING);
        }
    break;

  case 90:

/* Line 1806 of yacc.c  */
#line 1178 "parser.yy"
    {
          try {
            if (myloop == NULL)
              throw std::runtime_error("loop not initialized");
            
            std::vector<std::pair<int, std::vector<int> > > array_ref_nums((*(yyvsp[(3) - (9)].mat)).size());
            for (int i = 0; i < (*(yyvsp[(3) - (9)].mat)).size(); i++) {
              if ((*(yyvsp[(3) - (9)].mat))[i].size() <= 1)
                throw std::invalid_argument("statement missing in the first parameter");
              array_ref_nums[i].first = (*(yyvsp[(3) - (9)].mat))[i][0];
              for (int j = 1; j < (*(yyvsp[(3) - (9)].mat))[i].size(); j++)
                array_ref_nums[i].second.push_back((*(yyvsp[(3) - (9)].mat))[i][j]);
            }
            myloop->datacopy(array_ref_nums,(yyvsp[(5) - (9)].val),(yyvsp[(7) - (9)].bool_val));
          }
          catch (const std::exception &e) {
            fprintf(stderr, e.what());
            PRINT_ERR_LINENO;
            if (!is_interactive) {
              delete (yyvsp[(3) - (9)].mat);
              exit(2);
            }
          }
          delete (yyvsp[(3) - (9)].mat);
          if (is_interactive) printf("%s ", PROMPT_STRING);
        }
    break;

  case 91:

/* Line 1806 of yacc.c  */
#line 1204 "parser.yy"
    {
          try {
            if (myloop == NULL)
              throw std::runtime_error("loop not initialized");
            
            std::vector<std::pair<int, std::vector<int> > > array_ref_nums((*(yyvsp[(3) - (11)].mat)).size());
            for (int i = 0; i < (*(yyvsp[(3) - (11)].mat)).size(); i++) {
              if ((*(yyvsp[(3) - (11)].mat))[i].size() <= 1)
                throw std::invalid_argument("statement missing in the first parameter");
              array_ref_nums[i].first = (*(yyvsp[(3) - (11)].mat))[i][0];
              for (int j = 1; j < (*(yyvsp[(3) - (11)].mat))[i].size(); j++)
                array_ref_nums[i].second.push_back((*(yyvsp[(3) - (11)].mat))[i][j]);
            }
            myloop->datacopy(array_ref_nums,(yyvsp[(5) - (11)].val),(yyvsp[(7) - (11)].bool_val),(yyvsp[(9) - (11)].val));
          }
          catch (const std::exception &e) {
            fprintf(stderr, e.what());
            PRINT_ERR_LINENO;
            if (!is_interactive) {
              delete (yyvsp[(3) - (11)].mat);
              exit(2);
            }
          }
          delete (yyvsp[(3) - (11)].mat);
          if (is_interactive) printf("%s ", PROMPT_STRING);
        }
    break;

  case 92:

/* Line 1806 of yacc.c  */
#line 1230 "parser.yy"
    {
          try {
            if (myloop == NULL)
              throw std::runtime_error("loop not initialized");
            
            std::vector<std::pair<int, std::vector<int> > > array_ref_nums((*(yyvsp[(3) - (13)].mat)).size());
            for (int i = 0; i < (*(yyvsp[(3) - (13)].mat)).size(); i++) {
              if ((*(yyvsp[(3) - (13)].mat))[i].size() <= 1)
                throw std::invalid_argument("statement missing in the first parameter");
              array_ref_nums[i].first = (*(yyvsp[(3) - (13)].mat))[i][0];
              for (int j = 1; j < (*(yyvsp[(3) - (13)].mat))[i].size(); j++)
                array_ref_nums[i].second.push_back((*(yyvsp[(3) - (13)].mat))[i][j]);
            }
            myloop->datacopy(array_ref_nums,(yyvsp[(5) - (13)].val),(yyvsp[(7) - (13)].bool_val),(yyvsp[(9) - (13)].val),(yyvsp[(11) - (13)].val));
          }
          catch (const std::exception &e) {
            fprintf(stderr, e.what());
            PRINT_ERR_LINENO;
            if (!is_interactive) {
              delete (yyvsp[(3) - (13)].mat);
              exit(2);
            }
          }
          delete (yyvsp[(3) - (13)].mat);
          if (is_interactive) printf("%s ", PROMPT_STRING);
        }
    break;

  case 93:

/* Line 1806 of yacc.c  */
#line 1256 "parser.yy"
    {
          try {
            if (myloop == NULL)
              throw std::runtime_error("loop not initialized");
            
            std::vector<std::pair<int, std::vector<int> > > array_ref_nums((*(yyvsp[(3) - (15)].mat)).size());
            for (int i = 0; i < (*(yyvsp[(3) - (15)].mat)).size(); i++) {
              if ((*(yyvsp[(3) - (15)].mat))[i].size() <= 1)
                throw std::invalid_argument("statement missing in the first parameter");
              array_ref_nums[i].first = (*(yyvsp[(3) - (15)].mat))[i][0];
              for (int j = 1; j < (*(yyvsp[(3) - (15)].mat))[i].size(); j++)
                array_ref_nums[i].second.push_back((*(yyvsp[(3) - (15)].mat))[i][j]);
            }
            myloop->datacopy(array_ref_nums,(yyvsp[(5) - (15)].val),(yyvsp[(7) - (15)].bool_val),(yyvsp[(9) - (15)].val),(yyvsp[(11) - (15)].val),(yyvsp[(13) - (15)].val));
          }
          catch (const std::exception &e) {
            fprintf(stderr, e.what());
            PRINT_ERR_LINENO;
            if (!is_interactive) {
              delete (yyvsp[(3) - (15)].mat);
              exit(2);
            }
          }
          delete (yyvsp[(3) - (15)].mat);
          if (is_interactive) printf("%s ", PROMPT_STRING);
        }
    break;

  case 94:

/* Line 1806 of yacc.c  */
#line 1282 "parser.yy"
    {
          try {
            if (myloop == NULL)
              throw std::runtime_error("loop not initialized");
            
            myloop->datacopy((yyvsp[(3) - (9)].val),(yyvsp[(5) - (9)].val),(yyvsp[(7) - (9)].name));
          }
          catch (const std::exception &e) {
            fprintf(stderr, e.what());
            PRINT_ERR_LINENO;
            if (!is_interactive) {
              delete [](yyvsp[(7) - (9)].name);
              exit(2);
            }
          }
          delete [](yyvsp[(7) - (9)].name);
          if (is_interactive) printf("%s ", PROMPT_STRING);
        }
    break;

  case 95:

/* Line 1806 of yacc.c  */
#line 1300 "parser.yy"
    {
          try {
            if (myloop == NULL)
              throw std::runtime_error("loop not initialized");
            
            myloop->datacopy((yyvsp[(3) - (11)].val),(yyvsp[(5) - (11)].val),(yyvsp[(7) - (11)].name),(yyvsp[(9) - (11)].bool_val));
          }
          catch (const std::exception &e) {
            fprintf(stderr, e.what());
            PRINT_ERR_LINENO;
            if (!is_interactive) {
              delete [](yyvsp[(7) - (11)].name);
              exit(2);
            }
          }
          delete [](yyvsp[(7) - (11)].name);
          if (is_interactive) printf("%s ", PROMPT_STRING);
        }
    break;

  case 96:

/* Line 1806 of yacc.c  */
#line 1318 "parser.yy"
    {
          try {
            if (myloop == NULL)
              throw std::runtime_error("loop not initialized");

            myloop->datacopy((yyvsp[(3) - (13)].val),(yyvsp[(5) - (13)].val),(yyvsp[(7) - (13)].name),(yyvsp[(9) - (13)].bool_val),(yyvsp[(11) - (13)].val));
          }
          catch (const std::exception &e) {
            fprintf(stderr, e.what());
            PRINT_ERR_LINENO;
            if (!is_interactive) {
              delete [](yyvsp[(7) - (13)].name);
              exit(2);
            }
          }
          delete [](yyvsp[(7) - (13)].name);
          if (is_interactive) printf("%s ", PROMPT_STRING);
        }
    break;

  case 97:

/* Line 1806 of yacc.c  */
#line 1336 "parser.yy"
    {
          try {
            if (myloop == NULL)
              throw std::runtime_error("loop not initialized");

            myloop->datacopy((yyvsp[(3) - (15)].val),(yyvsp[(5) - (15)].val),(yyvsp[(7) - (15)].name),(yyvsp[(9) - (15)].bool_val),(yyvsp[(11) - (15)].val),(yyvsp[(13) - (15)].val));
          }
          catch (const std::exception &e) {
            fprintf(stderr, e.what());
            PRINT_ERR_LINENO;
            if (!is_interactive) {
              delete [](yyvsp[(7) - (15)].name);
              exit(2);
            }
          }
          delete [](yyvsp[(7) - (15)].name);
          if (is_interactive) printf("%s ", PROMPT_STRING);
        }
    break;

  case 98:

/* Line 1806 of yacc.c  */
#line 1354 "parser.yy"
    {
          try {
            if (myloop == NULL)
              throw std::runtime_error("loop not initialized");

            myloop->datacopy((yyvsp[(3) - (17)].val),(yyvsp[(5) - (17)].val),(yyvsp[(7) - (17)].name),(yyvsp[(9) - (17)].bool_val),(yyvsp[(11) - (17)].val),(yyvsp[(13) - (17)].val),(yyvsp[(15) - (17)].val));
          }
          catch (const std::exception &e) {
            fprintf(stderr, e.what());
            PRINT_ERR_LINENO;
            if (!is_interactive) {
              delete [](yyvsp[(7) - (17)].name);
              exit(2);
            }
          }
          delete [](yyvsp[(7) - (17)].name);
          if (is_interactive) printf("%s ", PROMPT_STRING);
        }
    break;

  case 99:

/* Line 1806 of yacc.c  */
#line 1372 "parser.yy"
    {
          try {
            if (myloop == NULL)
              throw std::runtime_error("loop not initialized");
 
            myloop->sparse_wavefront((yyvsp[(3) - (9)].val),(yyvsp[(5) - (9)].val),(yyvsp[(7) - (9)].bool_val));
          }
          catch (const std::exception &e) {
            fprintf(stderr, e.what());
            PRINT_ERR_LINENO;
            if (!is_interactive) {
                exit(2);
            }
          }

          if (is_interactive) printf("%s ", PROMPT_STRING);
        }
    break;

  case 100:

/* Line 1806 of yacc.c  */
#line 1389 "parser.yy"
    { 
          try { 
            if (myloop == NULL) 
              throw std::runtime_error("loop not initialized"); 
 
            myloop->sparse_partition((yyvsp[(3) - (9)].val),(yyvsp[(5) - (9)].val),(yyvsp[(7) - (9)].name)); 
          } 
          catch (const std::exception &e) { 
            fprintf(stderr, e.what()); 
            PRINT_ERR_LINENO; 
            if (!is_interactive) { 
                exit(2); 
            } 
          } 
          
          if (is_interactive) printf("%s ", PROMPT_STRING); 
        }
    break;

  case 101:

/* Line 1806 of yacc.c  */
#line 1406 "parser.yy"
    { 
          try { 
            if (myloop == NULL) 
              throw std::runtime_error("loop not initialized"); 
 
            myloop->reorder_by_inspector((yyvsp[(3) - (15)].val),(yyvsp[(5) - (15)].val),(yyvsp[(7) - (15)].name), (yyvsp[(9) - (15)].name), (yyvsp[(11) - (15)].name), (yyvsp[(13) - (15)].name)); 
          } 
          catch (const std::exception &e) { 
            fprintf(stderr, e.what()); 
            PRINT_ERR_LINENO; 
            if (!is_interactive) { 
                exit(2); 
            } 
          } 
          
          if (is_interactive) printf("%s ", PROMPT_STRING); 
        }
    break;

  case 102:

/* Line 1806 of yacc.c  */
#line 1423 "parser.yy"
    {
          try {
            if (myloop == NULL)
              throw std::runtime_error("loop not initialized");
	   std::vector<std::string> arrays;	 
           for (int i = 0; i < (*(yyvsp[(9) - (11)].string_vec)).size(); i++)
              arrays.push_back((*(yyvsp[(9) - (11)].string_vec))[i]);

            myloop->reorder_data((yyvsp[(3) - (11)].val),(yyvsp[(5) - (11)].name),(yyvsp[(7) - (11)].name), arrays);
          }
          catch (const std::exception &e) {
            fprintf(stderr, e.what());
            PRINT_ERR_LINENO;
            if (!is_interactive) {
                exit(2);
            }
          }

          if (is_interactive) printf("%s ", PROMPT_STRING);
        }
    break;

  case 103:

/* Line 1806 of yacc.c  */
#line 1443 "parser.yy"
    {
          try {
            if (myloop == NULL)
              throw std::runtime_error("loop not initialized");
            
            std::vector<std::pair<int, std::vector<int> > > array_ref_nums((*(yyvsp[(3) - (17)].mat)).size());
            for (int i = 0; i < (*(yyvsp[(3) - (17)].mat)).size(); i++) {

              if ((*(yyvsp[(3) - (17)].mat))[i].size() <= 1)
                throw std::invalid_argument("statement missing in the first parameter");
              array_ref_nums[i].first = (*(yyvsp[(3) - (17)].mat))[i][0];
              for (int j = 1; j < (*(yyvsp[(3) - (17)].mat))[i].size(); j++)
                array_ref_nums[i].second.push_back((*(yyvsp[(3) - (17)].mat))[i][j]);
            }
            myloop->datacopy_privatized(array_ref_nums,(yyvsp[(5) - (17)].val),*(yyvsp[(7) - (17)].vec),(yyvsp[(9) - (17)].bool_val),(yyvsp[(11) - (17)].val),(yyvsp[(13) - (17)].val),(yyvsp[(15) - (17)].val));
          }
          catch (const std::exception &e) {
            fprintf(stderr, e.what());
            PRINT_ERR_LINENO;
            if (!is_interactive) {
              delete (yyvsp[(3) - (17)].mat);
              delete (yyvsp[(7) - (17)].vec);
              exit(2);
            }
          }
          delete (yyvsp[(3) - (17)].mat);
          delete (yyvsp[(7) - (17)].vec);
          if (is_interactive) printf("%s ", PROMPT_STRING);
        }
    break;

  case 104:

/* Line 1806 of yacc.c  */
#line 1472 "parser.yy"
    {
          try {
            if (myloop == NULL)
              throw std::runtime_error("loop not initialized");

            myloop->datacopy_privatized((yyvsp[(3) - (19)].val),(yyvsp[(5) - (19)].val),(yyvsp[(7) - (19)].name),*(yyvsp[(9) - (19)].vec),(yyvsp[(11) - (19)].bool_val),(yyvsp[(13) - (19)].val),(yyvsp[(15) - (19)].val),(yyvsp[(17) - (19)].val));
          }
          catch (const std::exception &e) {
            fprintf(stderr, e.what());
            PRINT_ERR_LINENO;
            if (!is_interactive) {
              delete [](yyvsp[(7) - (19)].name);
              delete (yyvsp[(9) - (19)].vec);
              exit(2);
            }
          }
          delete [](yyvsp[(7) - (19)].name);
          delete (yyvsp[(9) - (19)].vec);
          if (is_interactive) printf("%s ", PROMPT_STRING);
        }
    break;

  case 105:

/* Line 1806 of yacc.c  */
#line 1492 "parser.yy"
    {
          try {
            if (myloop == NULL)
              throw std::runtime_error("loop not initialized");

            myloop->unroll((yyvsp[(3) - (9)].val),(yyvsp[(5) - (9)].val),(yyvsp[(7) - (9)].val));
          }
          catch (const std::exception &e) {
            fprintf(stderr, e.what());
            PRINT_ERR_LINENO;
            if (!is_interactive)
              exit(2);
          }
          if (is_interactive) printf("%s ", PROMPT_STRING);
        }
    break;

  case 106:

/* Line 1806 of yacc.c  */
#line 1507 "parser.yy"
    {
          try {
            if (myloop == NULL)
              throw std::runtime_error("loop not initialized");

            myloop->unroll((yyvsp[(3) - (11)].val),(yyvsp[(5) - (11)].val),(yyvsp[(7) - (11)].val),std::vector< std::vector<std::string> >(),  (yyvsp[(9) - (11)].val));
          }
          catch (const std::exception &e) {
            fprintf(stderr, e.what());
            PRINT_ERR_LINENO;
            if (!is_interactive)
              exit(2);
          }
          if (is_interactive) printf("%s ", PROMPT_STRING);
        }
    break;

  case 107:

/* Line 1806 of yacc.c  */
#line 1522 "parser.yy"
    {
          try {
            if (myloop == NULL)
              throw std::runtime_error("loop not initialized");

            myloop->unroll_extra((yyvsp[(3) - (9)].val),(yyvsp[(5) - (9)].val),(yyvsp[(7) - (9)].val));
          }
          catch (const std::exception &e) {
            fprintf(stderr, e.what());
            PRINT_ERR_LINENO;
            if (!is_interactive)
              exit(2);
          }
          if (is_interactive) printf("%s ", PROMPT_STRING);
        }
    break;

  case 108:

/* Line 1806 of yacc.c  */
#line 1537 "parser.yy"
    {
          try {
            if (myloop == NULL)
              throw std::runtime_error("loop not initialized");

            myloop->unroll_extra((yyvsp[(3) - (11)].val),(yyvsp[(5) - (11)].val),(yyvsp[(7) - (11)].val),(yyvsp[(9) - (11)].val));
          }
          catch (const std::exception &e) {
            fprintf(stderr, e.what());
            PRINT_ERR_LINENO;
            if (!is_interactive)
              exit(2);
          }
          if (is_interactive) printf("%s ", PROMPT_STRING);
        }
    break;

  case 109:

/* Line 1806 of yacc.c  */
#line 1552 "parser.yy"
    {
          try {
            if (myloop == NULL)
              throw std::runtime_error("loop not initialized");
            if ((yyvsp[(3) - (9)].val) < 0 || (yyvsp[(3) - (9)].val) >= myloop->num_statement())
              throw std::invalid_argument("invalid statement " + to_string((yyvsp[(3) - (9)].val)));
            int num_dim = myloop->stmt[(yyvsp[(3) - (9)].val)].xform.n_out();
            
            Relation rel((num_dim-1)/2);
            F_And *f_root = rel.add_and();
            for (int j = 0; j < (yyvsp[(7) - (9)].tab_lst)->size(); j++) {
              GEQ_Handle h = f_root->add_GEQ();
              for (std::map<std::string, int>::iterator it = (*(yyvsp[(7) - (9)].tab_lst))[j].begin(); it != (*(yyvsp[(7) - (9)].tab_lst))[j].end(); it++) {
                try {
                  int dim = from_string<int>(it->first);
                  if (dim == 0)
                    h.update_const(it->second);
                  else {
                    if (dim > (num_dim-1)/2)
                      throw std::invalid_argument("invalid loop level " + to_string(dim) + " in split condition");
                    h.update_coef(rel.set_var(dim), it->second);
                  }
                }
                catch (std::ios::failure e) {
                  Free_Var_Decl *g = NULL;
                  for (unsigned i = 0; i < myloop->freevar.size(); i++) {
                    std::string name = myloop->freevar[i]->base_name();
                    if (name == it->first) {
                      g = myloop->freevar[i];
                      break;
                    }
                  }
                  if (g == NULL)
                    throw std::invalid_argument("unrecognized variable " + to_string(it->first.c_str()));
                  h.update_coef(rel.get_local(g), it->second);
                }
              }
            }
            myloop->split((yyvsp[(3) - (9)].val),(yyvsp[(5) - (9)].val),rel);
          }
          catch (const std::exception &e) {
            fprintf(stderr, e.what());
            PRINT_ERR_LINENO;
            if (!is_interactive) {
              delete (yyvsp[(7) - (9)].tab_lst);
              exit(2);
            }
          }
          delete (yyvsp[(7) - (9)].tab_lst);
          if (is_interactive) printf("%s ", PROMPT_STRING);
        }
    break;

  case 110:

/* Line 1806 of yacc.c  */
#line 1603 "parser.yy"
    {
          try {
            myloop->nonsingular(*(yyvsp[(3) - (5)].mat));
          }
          catch (const std::exception &e) {
            fprintf(stderr, e.what());
            PRINT_ERR_LINENO;
            if (!is_interactive) {
              delete (yyvsp[(3) - (5)].mat);
              exit(2);
            }
          }
          delete (yyvsp[(3) - (5)].mat);
          if (is_interactive) printf("%s ", PROMPT_STRING);
        }
    break;

  case 111:

/* Line 1806 of yacc.c  */
#line 1618 "parser.yy"
    {
          try {
            if (myloop == NULL)
              throw std::runtime_error("loop not initialized");

            std::set<int> stmt_nums;
            for (int i = 0; i < (*(yyvsp[(3) - (9)].vec)).size(); i++)
              stmt_nums.insert((*(yyvsp[(3) - (9)].vec))[i]);
            myloop->skew(stmt_nums, (yyvsp[(5) - (9)].val), *(yyvsp[(7) - (9)].vec));
          }
          catch (const std::exception &e) {
            fprintf(stderr, e.what());
            PRINT_ERR_LINENO;
            if (!is_interactive) {
              delete (yyvsp[(3) - (9)].vec);
              delete (yyvsp[(7) - (9)].vec);
              exit(2);
            }
          }
          delete (yyvsp[(3) - (9)].vec);
          delete (yyvsp[(7) - (9)].vec);
          if (is_interactive) printf("%s ", PROMPT_STRING);
          }
    break;

  case 112:

/* Line 1806 of yacc.c  */
#line 1641 "parser.yy"
    {
          try {
            if (myloop == NULL)
              throw std::runtime_error("loop not initialized");

            std::set<int> stmt_nums;
            for (int i = 0; i < (*(yyvsp[(3) - (9)].vec)).size(); i++)
              stmt_nums.insert((*(yyvsp[(3) - (9)].vec))[i]);
            myloop->scale(stmt_nums, (yyvsp[(5) - (9)].val), (yyvsp[(7) - (9)].val));
          }
          catch (const std::exception &e) {
            fprintf(stderr, e.what());
            PRINT_ERR_LINENO;
            if (!is_interactive) {
              delete (yyvsp[(3) - (9)].vec);
              exit(2);
            }
          }
          delete (yyvsp[(3) - (9)].vec);
          if (is_interactive) printf("%s ", PROMPT_STRING);
        }
    break;

  case 113:

/* Line 1806 of yacc.c  */
#line 1662 "parser.yy"
    {
          try {
            if (myloop == NULL)
              throw std::runtime_error("loop not initialized");

            std::set<int> stmt_nums;
            for (int i = 0; i < (*(yyvsp[(3) - (7)].vec)).size(); i++)
              stmt_nums.insert((*(yyvsp[(3) - (7)].vec))[i]);
            myloop->reverse(stmt_nums, (yyvsp[(5) - (7)].val));
          }
          catch (const std::exception &e) {
            fprintf(stderr, e.what());
            PRINT_ERR_LINENO;
            if (!is_interactive) {
              delete (yyvsp[(3) - (7)].vec);
             exit(2);
            }
          }
          delete (yyvsp[(3) - (7)].vec);
          if (is_interactive) printf("%s ", PROMPT_STRING);
        }
    break;

  case 114:

/* Line 1806 of yacc.c  */
#line 1683 "parser.yy"
    {
          try {
            if (myloop == NULL)
              throw std::runtime_error("loop not initialized");

            std::set<int> stmt_nums;
            for (int i = 0; i < (*(yyvsp[(3) - (9)].vec)).size(); i++)
              stmt_nums.insert((*(yyvsp[(3) - (9)].vec))[i]);

            myloop->shift(stmt_nums, (yyvsp[(5) - (9)].val), (yyvsp[(7) - (9)].val));
          }
          catch (const std::exception &e) {
            fprintf(stderr, e.what());
            PRINT_ERR_LINENO;
            if (!is_interactive) {
              delete (yyvsp[(3) - (9)].vec);
              exit(2);
            }
          }
          delete (yyvsp[(3) - (9)].vec);
          if (is_interactive) printf("%s ", PROMPT_STRING);
        }
    break;

  case 115:

/* Line 1806 of yacc.c  */
#line 1705 "parser.yy"
    {
          try {
            if (myloop == NULL)
              throw std::runtime_error("loop not initialized");

            myloop->shift_to((yyvsp[(3) - (9)].val), (yyvsp[(5) - (9)].val), (yyvsp[(7) - (9)].val));
          }
          catch (const std::exception &e) {
            fprintf(stderr, e.what());
            PRINT_ERR_LINENO;
            if (!is_interactive) {
              exit(2);
            }
          }
          if (is_interactive) printf("%s ", PROMPT_STRING);
        }
    break;

  case 116:

/* Line 1806 of yacc.c  */
#line 1721 "parser.yy"
    {
          try {
            if (myloop == NULL)
              throw std::runtime_error("loop not initialized");

            myloop->peel((yyvsp[(3) - (9)].val), (yyvsp[(5) - (9)].val), (yyvsp[(7) - (9)].val));
          }
          catch (const std::exception &e) {
            fprintf(stderr, e.what());
            PRINT_ERR_LINENO;
            if (!is_interactive) {
              exit(2);
            }
          }
          if (is_interactive) printf("%s ", PROMPT_STRING);
        }
    break;

  case 117:

/* Line 1806 of yacc.c  */
#line 1737 "parser.yy"
    {
          try {
            if (myloop == NULL)
              throw std::runtime_error("loop not initialized");

            myloop->peel((yyvsp[(3) - (7)].val), (yyvsp[(5) - (7)].val));
          }
          catch (const std::exception &e) {
            fprintf(stderr, e.what());
            PRINT_ERR_LINENO;
            if (!is_interactive) {
              exit(2);
            }
          }
          if (is_interactive) printf("%s ", PROMPT_STRING);
        }
    break;

  case 118:

/* Line 1806 of yacc.c  */
#line 1753 "parser.yy"
    {
          try {
            if (myloop == NULL)
              throw std::runtime_error("loop not initialized");

            std::set<int> stmt_nums;
            for (int i = 0; i < (*(yyvsp[(3) - (7)].vec)).size(); i++)
              stmt_nums.insert((*(yyvsp[(3) - (7)].vec))[i]);

            myloop->fuse(stmt_nums, (yyvsp[(5) - (7)].val));
          }
          catch (const std::exception &e) {
            fprintf(stderr, e.what());
            PRINT_ERR_LINENO;
            if (!is_interactive) {
              delete (yyvsp[(3) - (7)].vec);
              exit(2);
            }
          }
          delete (yyvsp[(3) - (7)].vec);
          if (is_interactive) printf("%s ", PROMPT_STRING);
        }
    break;

  case 119:

/* Line 1806 of yacc.c  */
#line 1775 "parser.yy"
    {
          if (myloop == NULL) {
            fprintf(stderr, "loop not initialized");
            PRINT_ERR_LINENO;
            delete (yyvsp[(3) - (7)].vec);
            if (!is_interactive)
              exit(2);
          }
          else {
            std::set<int> stmt_nums;
            for (int i = 0; i < (*(yyvsp[(3) - (7)].vec)).size(); i++)
              stmt_nums.insert((*(yyvsp[(3) - (7)].vec))[i]);
            delete (yyvsp[(3) - (7)].vec);
            try {
              myloop->distribute(stmt_nums, (yyvsp[(5) - (7)].val));
            }
            catch (const std::exception &e) {
              fprintf(stderr, e.what());
              PRINT_ERR_LINENO;
              if (!is_interactive)
                exit(2);
            }
            if (is_interactive) printf("%s ", PROMPT_STRING);
          }
        }
    break;

  case 120:

/* Line 1806 of yacc.c  */
#line 1801 "parser.yy"
    {
 	   if (myloop == NULL) {
            fprintf(stderr, "loop not initialized");
            PRINT_ERR_LINENO;
            if (!is_interactive)
              exit(2);
          } else {
            std::vector<int> seq_levels;
            for (int i = 0; i < (*(yyvsp[(11) - (13)].vec)).size(); i++)
              seq_levels.push_back((*(yyvsp[(11) - (13)].vec))[i]);
            std::vector<int> levels;
            for (int i = 0; i < (*(yyvsp[(5) - (13)].vec)).size(); i++)
              levels.push_back((*(yyvsp[(5) - (13)].vec))[i]);
             myloop->reduce((yyvsp[(3) - (13)].val),levels, (yyvsp[(7) - (13)].val), (yyvsp[(9) - (13)].name), seq_levels);
          }	 
        }
    break;

  case 121:

/* Line 1806 of yacc.c  */
#line 1817 "parser.yy"
    {
 	   if (myloop == NULL) {
            fprintf(stderr, "loop not initialized");
            PRINT_ERR_LINENO;
            if (!is_interactive)
              exit(2);
          } else {
             myloop->split_with_alignment((yyvsp[(3) - (9)].val), (yyvsp[(5) - (9)].val), (yyvsp[(7) - (9)].val));
          }	 
        }
    break;

  case 122:

/* Line 1806 of yacc.c  */
#line 1827 "parser.yy"
    {
 	   if (myloop == NULL) {
            fprintf(stderr, "loop not initialized");
            PRINT_ERR_LINENO;
            if (!is_interactive)
              exit(2);
          } else {
             myloop->split_with_alignment((yyvsp[(3) - (11)].val), (yyvsp[(5) - (11)].val), (yyvsp[(7) - (11)].val), (yyvsp[(9) - (11)].val));
          }	 
        }
    break;

  case 123:

/* Line 1806 of yacc.c  */
#line 1838 "parser.yy"
    {
 	   if (myloop == NULL) {
            fprintf(stderr, "loop not initialized");
            PRINT_ERR_LINENO;
            if (!is_interactive)
              exit(2);
          } else {
             myloop->scalar_expand((yyvsp[(3) - (11)].val), *(yyvsp[(5) - (11)].vec), (yyvsp[(7) - (11)].name), (yyvsp[(9) - (11)].val));
          }	 
        }
    break;

  case 124:

/* Line 1806 of yacc.c  */
#line 1848 "parser.yy"
    {
	try {
            if (myloop == NULL)
              throw std::runtime_error("loop not initialized");

            myloop->_map_loop_to_openmp_shared_region((yyvsp[(3) - (5)].val));
          }
          catch (const std::exception &e) {
            fprintf(stderr, e.what());
            PRINT_ERR_LINENO;
            if (!is_interactive)
              YYABORT;
          }
          if (is_interactive) printf("%s ", PROMPT_STRING);

	}
    break;

  case 125:

/* Line 1806 of yacc.c  */
#line 1865 "parser.yy"
    {
	  try {
            if (myloop == NULL)
              throw std::runtime_error("loop not initialized");
            myloop->omp_par_for((yyvsp[(3) - (9)].val), (yyvsp[(5) - (9)].val), (yyvsp[(7) - (9)].val));
          }
          catch (const std::exception &e) {
            fprintf(stderr, e.what());
            PRINT_ERR_LINENO;
            if (!is_interactive)
              exit(2);
          }
          if (is_interactive) printf("%s ", PROMPT_STRING);
	}
    break;

  case 126:

/* Line 1806 of yacc.c  */
#line 1880 "parser.yy"
    {
	  try {
            if (myloop == NULL)
              throw std::runtime_error("loop not initialized");
            myloop->mark_omp_parallel_region((yyvsp[(3) - (9)].val), (yyvsp[(5) - (9)].val), (yyvsp[(7) - (9)].val));
          }
          catch (const std::exception &e) {
            fprintf(stderr, e.what());
            PRINT_ERR_LINENO;
            if (!is_interactive)
              exit(2);
          }
          if (is_interactive) printf("%s ", PROMPT_STRING);


	}
    break;

  case 127:

/* Line 1806 of yacc.c  */
#line 1897 "parser.yy"
    {

	  try {
            if (myloop == NULL)
              throw std::runtime_error("loop not initialized");
            std::vector<int> thrds;
            for (int i = 0; i < (*(yyvsp[(5) - (7)].vec)).size(); i++)
              thrds.push_back((*(yyvsp[(5) - (7)].vec))[i]);
     
            myloop->mark_omp_threads((yyvsp[(3) - (7)].val), thrds);
          }
          catch (const std::exception &e) {
            fprintf(stderr, e.what());
            PRINT_ERR_LINENO;
            if (!is_interactive)
              exit(2);
          }
          if (is_interactive) printf("%s ", PROMPT_STRING);

	}
    break;

  case 128:

/* Line 1806 of yacc.c  */
#line 1917 "parser.yy"
    {

	  try {
            if (myloop == NULL)
              throw std::runtime_error("loop not initialized");
  
     
            myloop->mark_pragma((yyvsp[(3) - (9)].val),(yyvsp[(5) - (9)].val), (yyvsp[(7) - (9)].name));
          }
          catch (const std::exception &e) {
            fprintf(stderr, e.what());
            PRINT_ERR_LINENO;
            if (!is_interactive)
              exit(2);
          }
          if (is_interactive) printf("%s ", PROMPT_STRING);

	}
    break;

  case 129:

/* Line 1806 of yacc.c  */
#line 1936 "parser.yy"
    {

	  try {
            if (myloop == NULL)
              throw std::runtime_error("loop not initialized");
 
	    std::vector<int> thrds;
            for (int i = 0; i < (*(yyvsp[(5) - (7)].vec)).size(); i++)
              thrds.push_back((*(yyvsp[(5) - (7)].vec))[i]);

            myloop->mark_omp_syncs((yyvsp[(3) - (7)].val), thrds);

          }
          catch (const std::exception &e) {
            fprintf(stderr, e.what());
            PRINT_ERR_LINENO;
            if (!is_interactive)
              exit(2);
          }
          if (is_interactive) printf("%s ", PROMPT_STRING);

	}
    break;

  case 130:

/* Line 1806 of yacc.c  */
#line 1959 "parser.yy"
    {

	  try {
            if (myloop == NULL)
              throw std::runtime_error("loop not initialized");
            myloop->generate_omp_parallel_region((yyvsp[(3) - (7)].val), (yyvsp[(5) - (7)].val));
          }
          catch (const std::exception &e) {
            fprintf(stderr, e.what());
            PRINT_ERR_LINENO;
            if (!is_interactive)
              exit(2);
          }
          if (is_interactive) printf("%s ", PROMPT_STRING);

	}
    break;



/* Line 1806 of yacc.c  */
#line 4655 "parser.tab.cc"
      default: break;
    }
  /* User semantic actions sometimes alter yychar, and that requires
     that yytoken be updated with the new translation.  We take the
     approach of translating immediately before every use of yytoken.
     One alternative is translating here after every semantic action,
     but that translation would be missed if the semantic action invokes
     YYABORT, YYACCEPT, or YYERROR immediately after altering yychar or
     if it invokes YYBACKUP.  In the case of YYABORT or YYACCEPT, an
     incorrect destructor might then be invoked immediately.  In the
     case of YYERROR or YYBACKUP, subsequent parser actions might lead
     to an incorrect destructor call or verbose syntax error message
     before the lookahead is translated.  */
  YY_SYMBOL_PRINT ("-> $$ =", yyr1[yyn], &yyval, &yyloc);

  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);

  *++yyvsp = yyval;

  /* Now `shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */

  yyn = yyr1[yyn];

  yystate = yypgoto[yyn - YYNTOKENS] + *yyssp;
  if (0 <= yystate && yystate <= YYLAST && yycheck[yystate] == *yyssp)
    yystate = yytable[yystate];
  else
    yystate = yydefgoto[yyn - YYNTOKENS];

  goto yynewstate;


/*------------------------------------.
| yyerrlab -- here on detecting error |
`------------------------------------*/
yyerrlab:
  /* Make sure we have latest lookahead translation.  See comments at
     user semantic actions for why this is necessary.  */
  yytoken = yychar == YYEMPTY ? YYEMPTY : YYTRANSLATE (yychar);

  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
#if ! YYERROR_VERBOSE
      yyerror (YY_("syntax error"));
#else
# define YYSYNTAX_ERROR yysyntax_error (&yymsg_alloc, &yymsg, \
                                        yyssp, yytoken)
      {
        char const *yymsgp = YY_("syntax error");
        int yysyntax_error_status;
        yysyntax_error_status = YYSYNTAX_ERROR;
        if (yysyntax_error_status == 0)
          yymsgp = yymsg;
        else if (yysyntax_error_status == 1)
          {
            if (yymsg != yymsgbuf)
              YYSTACK_FREE (yymsg);
            yymsg = (char *) YYSTACK_ALLOC (yymsg_alloc);
            if (!yymsg)
              {
                yymsg = yymsgbuf;
                yymsg_alloc = sizeof yymsgbuf;
                yysyntax_error_status = 2;
              }
            else
              {
                yysyntax_error_status = YYSYNTAX_ERROR;
                yymsgp = yymsg;
              }
          }
        yyerror (yymsgp);
        if (yysyntax_error_status == 2)
          goto yyexhaustedlab;
      }
# undef YYSYNTAX_ERROR
#endif
    }



  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse lookahead token after an
	 error, discard it.  */

      if (yychar <= YYEOF)
	{
	  /* Return failure if at end of input.  */
	  if (yychar == YYEOF)
	    YYABORT;
	}
      else
	{
	  yydestruct ("Error: discarding",
		      yytoken, &yylval);
	  yychar = YYEMPTY;
	}
    }

  /* Else will try to reuse lookahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:

  /* Pacify compilers like GCC when the user code never invokes
     YYERROR and the label yyerrorlab therefore never appears in user
     code.  */
  if (/*CONSTCOND*/ 0)
     goto yyerrorlab;

  /* Do not reclaim the symbols of the rule which action triggered
     this YYERROR.  */
  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;	/* Each real token shifted decrements this.  */

  for (;;)
    {
      yyn = yypact[yystate];
      if (!yypact_value_is_default (yyn))
	{
	  yyn += YYTERROR;
	  if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYTERROR)
	    {
	      yyn = yytable[yyn];
	      if (0 < yyn)
		break;
	    }
	}

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
	YYABORT;


      yydestruct ("Error: popping",
		  yystos[yystate], yyvsp);
      YYPOPSTACK (1);
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  *++yyvsp = yylval;


  /* Shift the error token.  */
  YY_SYMBOL_PRINT ("Shifting", yystos[yyn], yyvsp, yylsp);

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturn;

/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturn;

#if !defined(yyoverflow) || YYERROR_VERBOSE
/*-------------------------------------------------.
| yyexhaustedlab -- memory exhaustion comes here.  |
`-------------------------------------------------*/
yyexhaustedlab:
  yyerror (YY_("memory exhausted"));
  yyresult = 2;
  /* Fall through.  */
#endif

yyreturn:
  if (yychar != YYEMPTY)
    {
      /* Make sure we have latest lookahead translation.  See comments at
         user semantic actions for why this is necessary.  */
      yytoken = YYTRANSLATE (yychar);
      yydestruct ("Cleanup: discarding lookahead",
                  yytoken, &yylval);
    }
  /* Do not reclaim the symbols of the rule which action triggered
     this YYABORT or YYACCEPT.  */
  YYPOPSTACK (yylen);
  YY_STACK_PRINT (yyss, yyssp);
  while (yyssp != yyss)
    {
      yydestruct ("Cleanup: popping",
		  yystos[*yyssp], yyvsp);
      YYPOPSTACK (1);
    }
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif
#if YYERROR_VERBOSE
  if (yymsg != yymsgbuf)
    YYSTACK_FREE (yymsg);
#endif
  /* Make sure YYID is used.  */
  return YYID (yyresult);
}



/* Line 2067 of yacc.c  */
#line 1982 "parser.yy"



void yyerror(const char *str) {
  int err_lineno = lexer.lineno();
  if (lexer.YYText()[0] == '\n')
    err_lineno--;

  if (is_interactive)
    fprintf(stderr, "%s\n", str);
  else 
    fprintf(stderr, "%s at line %d\n", str, err_lineno);
}

int main(int argc, char *argv[]) {
  yydebug = 0;

  if (argc > 2) {
    fprintf(stderr, "Usage: %s [script_file]\n", argv[0]);
    exit(-1);
  }

  std::ifstream script;
  if (argc == 2) {
    script.open(argv[1]);
    if (!script.is_open()) {
      printf("can't open script file \"%s\"\n", argv[1]);
      exit(-1);
    }  
    lexer.switch_streams(&script, &std::cout);
  }

  if (argc == 1 && isatty((int)fileno(stdin))) {
    is_interactive = true;
    printf("CHiLL v0.2.0 (built on %s)\n", CHILL_BUILD_DATE);
    printf("Copyright (C) 2008 University of Southern California\n");
    printf("Copyright (C) 2009-2012 University of Utah\n");
    printf("%s ", PROMPT_STRING);
  }
  else
    is_interactive = false;
    
  ir_code = NULL;
  initializeOmega();

  if (yyparse() == 0) {
    if (!is_interactive)
      fprintf(stderr, "script success!\n");
    else
      printf("\n");
    if (ir_code != NULL && myloop != NULL && myloop->isInitialized()) {
      if (loop_num_start == loop_num_end) {
        ir_code->ReplaceCode(ir_controls[loops[loop_num_start]], myloop->getCode());
        ir_controls[loops[loop_num_start]] = NULL;
      }
      else {
        std::vector<IR_Control *> parm;
        for (int i = loops[loop_num_start]; i <= loops[loop_num_end]; i++)
          parm.push_back(ir_controls[i]);
        IR_Block *block = ir_code->MergeNeighboringControlStructures(parm);
        ir_code->ReplaceCode(block, myloop->getCode());
        for (int i = loops[loop_num_start]; i <= loops[loop_num_end]; i++) {
          delete ir_controls[i];
          ir_controls[i] = NULL;
        }
      } 
    }
  }

  delete myloop;
  for (int i = 0; i < ir_controls.size(); i++)
    delete ir_controls[i];
  delete ir_code;
  delete []source_filename;
}

