#ifndef IR_ROSE_HH
#define IR_ROSE_HH

#include <omega.h>
#include "ir_code.hh"
#include "ir_rose_utils.hh"
#include <AstInterface_ROSE.h>
#include "chill_error.hh"
#include "staticSingleAssignment.h"
#include "VariableRenaming.h"
#include "ssaUnfilteredCfg.h"
#include "virtualCFG.h"
#include <omega.h>
#include "loop_cuda_rose.hh"
#include <code_gen/CG_roseRepr.h>

struct IR_roseScalarSymbol: public IR_ScalarSymbol {
	SgVariableSymbol* vs_;
    SgDotExp *scalar_within_struct;
	IR_roseScalarSymbol(const IR_Code *ir, SgVariableSymbol *vs) {
		ir_ = ir;
		vs_ = vs;
		scalar_within_struct = NULL;
	}

	IR_roseScalarSymbol(const IR_Code *ir, SgVariableSymbol *vs, SgDotExp *exp ) {
		ir_ = ir;
		vs_ = vs;
		scalar_within_struct = exp;
	}

	std::string name() const;
	int size() const;
	bool operator==(const IR_Symbol &that) const;
	IR_Symbol *clone() const;
};


struct IR_roseFunctionSymbol: public IR_FunctionSymbol {
	SgFunctionSymbol* fs_;



	IR_roseFunctionSymbol(const IR_Code *ir, SgFunctionSymbol *fs ) {
		ir_ = ir;
		fs_ = fs;
		//scalar_within_struct = exp;
	}

	std::string name() const;
	bool operator==(const IR_Symbol &that) const;
	IR_Symbol *clone() const;
};


struct IR_roseArraySymbol: public IR_ArraySymbol {

	SgVariableSymbol* vs_;
    std::string name_;
	SgDotExp *arr_within_struct;
	IR_roseArraySymbol(const IR_Code *ir, SgVariableSymbol* vs, std::string name, SgDotExp *exp) {
		ir_ = ir;
		vs_ = vs;
		name_ = name;
		if(exp != NULL){
		omega::CG_roseRepr *repr = new omega::CG_roseRepr(exp);
		arr_within_struct = isSgDotExp(static_cast<omega::CG_roseRepr *>(repr->clone())->GetExpression());
		}
		else
		arr_within_struct = NULL;

	}
	std::string name() const;
	int elem_size() const;
	IR_CONSTANT_TYPE elem_type() const;
	int n_dim() const;
	omega::CG_outputRepr *size(int dim) const;
	bool operator==(const IR_Symbol &that) const;
	IR_ARRAY_LAYOUT_TYPE layout_type() const;
	IR_Symbol *clone() const;

};

struct IR_rosePointerSymbol: public IR_PointerSymbol {

	SgVariableSymbol* vs_;
    std::string name_;
	SgDotExp *arr_within_struct;
	int dim_;
	std::vector<omega::CG_outputRepr *> dims;
	IR_rosePointerSymbol(const IR_Code *ir, SgVariableSymbol* vs, int dim) {
		ir_ = ir;
		vs_ = vs;
		name_ = vs->get_name().getString();
        dim_ = dim;
        dims.resize(dim_);
	}
	std::string name() const;
    int n_dim() const;
    void set_dim(int dim){dim_ = dim;}
	bool operator==(const IR_Symbol &that) const;
	omega::CG_outputRepr *size(int dim) const ;
	void set_size(int dim, omega::CG_outputRepr*) ;
	IR_Symbol *clone() const;
	IR_CONSTANT_TYPE elem_type() const;

};


struct IR_roseConstantRef: public IR_ConstantRef {
	union {
		omega::coef_t i_;
		double f_;
	};

	IR_roseConstantRef(const IR_Code *ir, omega::coef_t i) {
		ir_ = ir;
		type_ = IR_CONSTANT_INT;
		i_ = i;
	}
	IR_roseConstantRef(const IR_Code *ir, double f) {
		ir_ = ir;
		type_ = IR_CONSTANT_FLOAT;
		f_ = f;
	}
	omega::coef_t integer() const {
		assert(is_integer());
		return i_;
	}
	bool operator==(const IR_Ref &that) const;
	omega::CG_outputRepr *convert();
	IR_Ref *clone() const;

};

struct IR_roseScalarRef: public IR_ScalarRef {
	SgAssignOp *ins_pos_;
	int op_pos_; // -1 means destination operand, otherwise source operand
	SgVarRefExp *vs_;
	SgDotExp *struct_exp;
	int is_write_;
	IR_roseScalarRef(const IR_Code *ir, SgVarRefExp *sym) {
		ir_ = ir;
		ins_pos_ = isSgAssignOp(sym->get_parent());
		op_pos_ = 0;
		if (ins_pos_ != NULL)
			if (sym == isSgVarRefExp(ins_pos_->get_lhs_operand()))
				op_pos_ = -1;

		vs_ = sym;
		struct_exp = NULL;
	}
	IR_roseScalarRef(const IR_Code *ir, SgVarRefExp *ins, int pos) {
		ir_ = ir;
		/*	ins_pos_ = ins;
		 op_pos_ = pos;
		 SgExpression* op;
		 if (pos == -1)
		 op = ins->get_lhs_operand();
		 else
		 op = ins->get_rhs_operand();

		 */

		is_write_ = pos;

		/*	if (vs_ == NULL || pos > 0)
		 throw ir_error(
		 "Src operand not a variable or more than one src operand!!");
		 */

		vs_ = ins;
		struct_exp = NULL;

	}

	IR_roseScalarRef(const IR_Code *ir, SgVarRefExp *ins, int pos, SgDotExp * exp) {
		ir_ = ir;
		/*	ins_pos_ = ins;
		 op_pos_ = pos;
		 SgExpression* op;
		 if (pos == -1)
		 op = ins->get_lhs_operand();
		 else
		 op = ins->get_rhs_operand();

		 */

		is_write_ = pos;

		/*	if (vs_ == NULL || pos > 0)
		 throw ir_error(
		 "Src operand not a variable or more than one src operand!!");
		 */

		vs_ = ins;
		struct_exp = exp;


	}
	bool is_write() const;
	IR_ScalarSymbol *symbol() const;
	bool operator==(const IR_Ref &that) const;
	omega::CG_outputRepr *convert();
	IR_Ref *clone() const;
};


struct IR_roseFunctionRef: public IR_FunctionRef {

	SgFunctionRefExp *vs_;

	int is_write_;

	IR_roseFunctionRef(const IR_Code *ir, SgFunctionRefExp *ins) {
		ir_ = ir;
		vs_ = ins;
		is_write_ = 0;
	}
	bool is_write() const;
	IR_FunctionSymbol *symbol() const;
	bool operator==(const IR_Ref &that) const;
	omega::CG_outputRepr *convert();
	IR_Ref *clone() const;
};


struct IR_roseArrayRef: public IR_ArrayRef {

	SgPntrArrRefExp *ia_;


	int is_write_;
	IR_roseArrayRef(const IR_Code *ir, SgPntrArrRefExp *ia, int write) {
		ir_ = ir;
		ia_ = ia;
		is_write_ = write;


	}


	bool is_write() const;
	omega::CG_outputRepr *index(int dim) const;
	IR_ArraySymbol *symbol() const;
	bool operator==(const IR_Ref &that) const;
	omega::CG_outputRepr *convert();
	IR_Ref *clone() const;
};


struct IR_rosePointerArrayRef: public IR_PointerArrayRef {

	SgPntrArrRefExp *ia_;
    IR_PointerSymbol *sym_;

	int is_write_;
	IR_rosePointerArrayRef(const IR_Code *ir, SgPntrArrRefExp *ia, int write, IR_PointerSymbol * sym) {
		ir_ = ir;
		ia_ = ia;
		is_write_ = write;
        sym_ = sym;

	}


	bool is_write() const;
	omega::CG_outputRepr *index(int dim) const;
	IR_PointerSymbol *symbol() const;
	bool operator==(const IR_Ref &that) const;
	omega::CG_outputRepr *convert();
	IR_Ref *clone() const;
};

struct IR_roseLoop: public IR_Loop {
	SgNode *tf_;
    SgExpression *stop_condition;
    ssa_unfiltered_cfg::SSA_UnfilteredCfg *main_ssa;
	IR_roseLoop(const IR_Code *ir, SgNode *tf, ssa_unfiltered_cfg::SSA_UnfilteredCfg *ssa = NULL) {
		ir_ = ir;
		tf_ = tf;
		main_ssa = ssa;
		if(isSgWhileStmt(tf_))
			stop_condition = isSgExprStatement(isSgWhileStmt(tf_)->get_condition())->get_expression();
		else if(isSgForStatement(tf_))
			stop_condition = isSgForStatement(tf_)->get_test_expr();

	}

	std::vector<IR_ScalarSymbol * > index() const;
	std::vector<omega::CG_outputRepr *> lower_bound() const;
	std::vector<omega::CG_outputRepr *> upper_bound() const;
	std::vector<IR_CONDITION_TYPE> stop_cond() const;
	CG_outputRepr *cond() const
	{return new CG_roseRepr(stop_condition);}
	IR_Block *body() const;
	IR_Block *convert();
	int step_size() const;
	IR_Control *clone() const;
};

struct IR_roseBlock: public IR_Block {
	SgNode* tnl_;
	SgNode *start_, *end_;

	IR_roseBlock(const IR_Code *ir, SgNode *tnl, SgNode *start, SgNode *end) {
		ir_ = ir;
		tnl_ = tnl;
		start_ = start;
		end_ = end;
	}

	IR_roseBlock(const IR_Code *ir, SgNode *tnl) {
		ir_ = ir;
		tnl_ = tnl;

		if(tnl_->get_numberOfTraversalSuccessors() > 0){
		start_ = tnl_->get_traversalSuccessorByIndex(0);
		end_ = tnl_->get_traversalSuccessorByIndex(
				(tnl_->get_numberOfTraversalSuccessors()) - 1);
		}
		else{
			start_ = end_ = tnl_;


		}


	}
	omega::CG_outputRepr *extract() const;
	omega::CG_outputRepr *original() const;
	IR_Control *clone() const;
};

struct IR_roseIf: public IR_If {
	SgNode *ti_;

	IR_roseIf(const IR_Code *ir, SgNode *ti) {
		ir_ = ir;
		ti_ = ti;
	}
	~IR_roseIf() {
	}
	omega::CG_outputRepr *condition() const;
	IR_Block *then_body() const;
	IR_Block *else_body() const;
	IR_Block *convert();
	IR_Control *clone() const;
};

class IR_roseCode_Global_Init {
private:
	static IR_roseCode_Global_Init *pinstance;
public:
	SgProject* project;
	static IR_roseCode_Global_Init *Instance(char** argv);
};

class IR_roseCode: public IR_Code {
protected:
	SgSourceFile* file;
	SgGlobal *root;
	SgGlobal *firstScope;
	SgSymbolTable* symtab_;
	SgSymbolTable* symtab2_;
	SgSymbolTable* symtab3_;
	SgDeclarationStatementPtrList::iterator p;
	SgFunctionDeclaration *func;
	bool is_fortran_;
	int i_;
	StaticSingleAssignment *ssa_for_scalar;
	ssa_unfiltered_cfg::SSA_UnfilteredCfg *main_ssa;
	VariableRenaming *varRenaming_for_scalar;
	std::map<std::string, std::pair<std::string,SgNode*> > defined_macros;
	std::string proc_name_;
	int rose_array_counter;
	int rose_pointer_counter;
public:
	IR_roseCode(const char *filename, const char* proc_name);
	~IR_roseCode();

	int getPointerCounter(void){return rose_pointer_counter;}
	int getArrayCounter(void){return rose_array_counter;}

	IR_ScalarSymbol *CreateScalarSymbol(const IR_Symbol *sym, int memory_type =
			0);
	IR_ScalarSymbol *CreateScalarSymbol(IR_CONSTANT_TYPE type, int memory_type = 0, std::string name = "", bool is_static=false, CG_outputRepr * initializer = NULL);
	IR_ArraySymbol *CreateArraySymbol(const IR_Symbol *sym,
			std::vector<omega::CG_outputRepr *> &size, int memory_type = 0);
	IR_PointerSymbol *CreatePointerSymbol(const IR_Symbol *sym,
			std::vector<CG_outputRepr *> &size_repr);
	IR_PointerSymbol *CreatePointerSymbol(const IR_CONSTANT_TYPE type,
			std::vector<CG_outputRepr *> &size_repr, std::string name = "");
	IR_PointerSymbol *CreatePointerSymbol(CG_outputRepr * type,
			std::vector<CG_outputRepr *> &size_repr);
	IR_PointerSymbol *CreatePointerSymbol(CG_outputRepr *type,
			std::string name, bool is_static = false);
	IR_ArraySymbol *CreateArraySymbol(CG_outputRepr * type,
			std::vector<CG_outputRepr *> &size_repr);
	IR_ScalarRef *CreateScalarRef(const IR_ScalarSymbol *sym);
	IR_ArrayRef *CreateArrayRef(const IR_ArraySymbol *sym,
			std::vector<omega::CG_outputRepr *> &index);
	IR_PointerArrayRef *CreatePointerArrayRef( IR_PointerSymbol *sym,
			std::vector<omega::CG_outputRepr *> &index);
    omega::CG_outputRepr *CreateArrayType(IR_CONSTANT_TYPE type, omega::CG_outputRepr *size);
    omega::CG_outputRepr *CreateArrayType(IR_CONSTANT_TYPE type,
    		std::vector<omega::CG_outputRepr *>size);
    omega::CG_outputRepr *CreatePointerType(IR_CONSTANT_TYPE type);
    omega::CG_outputRepr *CreatePointerType(omega::CG_outputRepr* type);
    omega::CG_outputRepr *CreateScalarType(IR_CONSTANT_TYPE type);
    void CreateDefineMacro(std::string s, std::string args,  omega::CG_outputRepr *repr);
	//void CreateDefineMacro(std::string s, std::string args,  std::string repr);
	omega::CG_outputRepr *RetrieveMacro(std::string s);
	int ArrayIndexStartAt() {
		if (is_fortran_)
			return 1;
		else
			return 0;
	}

	void populateLists(SgNode* tnl_1, SgStatementPtrList* list_1,
			SgStatementPtrList& output_list_1);
	void populateScalars(const omega::CG_outputRepr *repr1,
			std::map<SgVarRefExp*, IR_ScalarRef*> &read_scalars_1,
			std::map<SgVarRefExp*, IR_ScalarRef*> &write_scalars_1,
			std::set<std::string> &indices, std::vector<std::string> &index);
	//		std::set<std::string> &def_vars);
	/*void findDefinitions(SgStatementPtrList &list_1,
	 std::set<VirtualCFG::CFGNode> &reaching_defs_1,
	 std::map<SgVarRefExp*, IR_ScalarRef*> &write_scalars_1,
	 std::set<std::string> &def_vars);
	 */
	/*	void checkDependency(SgStatementPtrList &output_list_1,
	 std::vector<DependenceVector> &dvs1,
	 std::map<SgVarRefExp*, IR_ScalarRef*> &read_scalars_1,
	 std::map<SgVarRefExp*, IR_ScalarRef*> &write_scalars_1,
	 std::vector<std::string> &index, int i, int j);
	 void checkSelfDependency(SgStatementPtrList &output_list_1,
	 std::vector<DependenceVector> &dvs1,
	 std::map<SgVarRefExp*, IR_ScalarRef*> &read_scalars_1,
	 std::map<SgVarRefExp*, IR_ScalarRef*> &write_scalars_1,
	 std::vector<std::string> &index, int i, int j);
	 void checkWriteDependency(SgStatementPtrList &output_list_1,
	 std::vector<DependenceVector> &dvs1,
	 std::map<SgVarRefExp*, IR_ScalarRef*> &read_scalars_1,
	 std::map<SgVarRefExp*, IR_ScalarRef*> &write_scalars_1,
	 std::vector<std::string> &index, int i, int j);
	 */
	std::vector<IR_ArrayRef *> FindArrayRef(
			const omega::CG_outputRepr *repr, bool ignore_while=true) const;
	std::vector<IR_Loop *> FindLoops(omega::CG_outputRepr *repr);
	std::vector<IR_PointerArrayRef *> FindPointerArrayRef(
			const omega::CG_outputRepr *repr) const;
	std::vector<IR_ScalarRef *> FindScalarRef(
			const omega::CG_outputRepr *repr) const;
	std::vector<IR_Control *> FindOneLevelControlStructure(
			const IR_Block *block) const;
	IR_Block *MergeNeighboringControlStructures(
			const std::vector<IR_Control *> &controls) const;
	IR_Block *GetCode() const;
	IR_Control *GetCode(omega::CG_outputRepr *code) const;
	void ReplaceCode(IR_Control *old, omega::CG_outputRepr *repr);
	void ReplaceExpression(IR_Ref *old, omega::CG_outputRepr *repr);

	IR_OPERATION_TYPE QueryExpOperation(const omega::CG_outputRepr *repr) const;
	IR_CONDITION_TYPE QueryBooleanExpOperation(
			const omega::CG_outputRepr *repr) const;
	std::vector<omega::CG_outputRepr *> QueryExpOperand(
			const omega::CG_outputRepr *repr) const;
	IR_Ref *Repr2Ref(const omega::CG_outputRepr *) const;

	//Anand: Adding a simple utility to check if parent expression is array

	 bool parent_is_array(IR_ArrayRef *a);



	/*	std::pair<std::vector<DependenceVector>, std::vector<DependenceVector> >
	 FindScalarDeps(const omega::CG_outputRepr *repr1,
	 const omega::CG_outputRepr *repr2, std::vector<std::string> index,
	 int i, int j);
	 */

	// Manu:: Added functions required for reduction operation
//	omega::CG_outputRepr * FromSameStmt(IR_ArrayRef *A, IR_ArrayRef *B);
	bool FromSameStmt(IR_ArrayRef *A, IR_ArrayRef *B);
	SgStatement * FromStmt(SgNode *node);  // internal method -- should be private
	void printStmt(const omega::CG_outputRepr *repr);
	void printStmtHelper(SgNode *node); // internal method -- should be private
	int getStmtType(const omega::CG_outputRepr *repr);
	IR_OPERATION_TYPE getReductionOp(const omega::CG_outputRepr *repr);
	IR_Control * FromForStmt(const omega::CG_outputRepr *repr);
	SgNode * FromForStmtHelper(SgNode *node);


	// Manu:: Added functions for scalar expansion
	IR_ArraySymbol *CreateArraySymbol(omega::CG_outputRepr *size, const IR_Symbol *sym);
	bool ReplaceRHSExpression(omega::CG_outputRepr *code, IR_Ref *ref);
	bool ReplaceLHSExpression(omega::CG_outputRepr *code, IR_ArrayRef *ref);
	omega::CG_outputRepr * GetRHSExpression(omega::CG_outputRepr *code);
	omega::CG_outputRepr * GetLHSExpression(omega::CG_outputRepr *code);
	CG_outputRepr *CreateMalloc(const IR_CONSTANT_TYPE type, std::string lhs,
			CG_outputRepr * size_repr);
	CG_outputRepr *CreateMalloc(CG_outputRepr *type, std::string lhs,
			CG_outputRepr * size_repr);
	CG_outputRepr *CreateMalloc(const IR_CONSTANT_TYPE type,
			CG_outputRepr * lhs, CG_outputRepr * size_repr);
	CG_outputRepr *CreateFree(CG_outputRepr *exp);

        CG_outputRepr *CreateDelete(CG_outputRepr *exp);
        CG_outputRepr *CreateReturn(CG_outputRepr *rep); 
	SgGlobal *getroot() const;
	std::vector<SgSymbolTable *> getsymtabs() const;
	SgFunctionDeclaration * get_func() const {return func; }
	SgGlobal *get_root() const{ return root;}
	friend class IR_roseArraySymbol;
	friend class IR_roseArrayRef;
	friend class LoopCuda;
	friend class wavefront_intel_dep_graph;
};

#endif
