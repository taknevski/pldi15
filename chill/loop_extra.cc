/*****************************************************************************
 Copyright (C) 2010 University of Utah
 All Rights Reserved.

 Purpose:
 Additional loop transformations.

 Notes:
 
 History:
 07/31/10 Created by Chun Chen
 *****************************************************************************/

#include <codegen.h>
#include <code_gen/CG_utils.h>
#include "loop.hh"
#include "omegatools.hh"
#include "ir_code.hh"
#include "chill_error.hh"

using namespace omega;

void Loop::shift_to(int stmt_num, int level, int absolute_position) {
	// combo
	tile(stmt_num, level, 1, level, CountedTile);
	std::vector<int> lex = getLexicalOrder(stmt_num);
	std::set<int> active = getStatements(lex, 2 * level - 2);
	shift(active, level, absolute_position);
	//apply_xform(active);//AV:
	// remove unnecessary tiled loop since tile size is one
	for (std::set<int>::iterator i = active.begin(); i != active.end(); i++) {
		int n = stmt[*i].xform.n_out();
		Relation mapping(n, n - 2);
		F_And *f_root = mapping.add_and();
		for (int j = 1; j <= 2 * level; j++) {
			EQ_Handle h = f_root->add_EQ();
			h.update_coef(mapping.output_var(j), 1);
			h.update_coef(mapping.input_var(j), -1);
		}
		for (int j = 2 * level + 3; j <= n; j++) {
			EQ_Handle h = f_root->add_EQ();
			h.update_coef(mapping.output_var(j - 2), 1);
			h.update_coef(mapping.input_var(j), -1);
		}
		stmt[*i].xform = Composition(mapping, stmt[*i].xform);
		stmt[*i].xform.simplify();

		for (int j = 0; j < stmt[*i].loop_level.size(); j++)
			if (j != level - 1 && stmt[*i].loop_level[j].type == LoopLevelTile
					&& stmt[*i].loop_level[j].payload >= level)
				stmt[*i].loop_level[j].payload--;

		stmt[*i].loop_level.erase(stmt[*i].loop_level.begin() + level - 1);
	}
	apply_xform(active);
}

void Loop::split_with_alignment(int stmt_num, int level, int alignment,
		int direction) {

	// check for sanity of parameters
	if (stmt_num < 0 || stmt_num >= stmt.size())
		throw std::invalid_argument(
				"invalid statement number " + to_string(stmt_num));
	if (level <= 0 || level > stmt[stmt_num].loop_level.size())
		throw std::invalid_argument("invalid loop level " + to_string(level));
	int dim = 2 * level - 1;
	std::set<int> subloop = getSubLoopNest(stmt_num, level);
	std::vector<Relation> Rs;
	std::map<int, int> what_stmt_num;
	for (std::set<int>::iterator i = subloop.begin(); i != subloop.end(); i++) {
		Relation r = getNewIS(*i);
		Relation f(r.n_set(), level);
		F_And *f_root = f.add_and();
		for (int j = 1; j <= level; j++) {
			EQ_Handle h = f_root->add_EQ();
			h.update_coef(f.input_var(2 * j), 1);
			h.update_coef(f.output_var(j), -1);
		}
		//Anand composition will fail due to unintepreted function symbols introduced by flattening
		//r = Composition(f, r);
		r = omega::Range(Restrict_Domain(f, r));
		r.simplify();
		Rs.push_back(r);
	}
	Relation hull = SimpleHull(Rs);

	GEQ_Handle bound_eq;
	bool found_bound = false;

	for (GEQ_Iterator e(hull.single_conjunct()->GEQs()); e; e++)
		if (!(*e).has_wildcards() && (*e).get_coef(hull.set_var(level)) < 0) {
			bound_eq = *e;
			found_bound = true;
			break;
		}
	if (!found_bound)
		for (GEQ_Iterator e(hull.single_conjunct()->GEQs()); e; e++)
			if ((*e).has_wildcards()
					&& (*e).get_coef(hull.set_var(level)) < 0) {
				bool is_bound = true;
				for (Constr_Vars_Iter cvi(*e, true); cvi; cvi++) {
					std::pair<bool, GEQ_Handle> result = find_floor_definition(
							hull, cvi.curr_var());
					if (!result.first) {
						is_bound = false;
						break;
					}
				}
				if (is_bound) {
					bound_eq = *e;
					found_bound = true;
					break;
				}
			}
	if (!found_bound)
		throw loop_error(
				"can't find upper bound for split_with_alignment at level  "
						+ to_string(level));

	Relation r(level);
	F_Exists *f_exists = r.add_and()->add_exists();
	F_And *f_root = f_exists->add_and();
	std::map<Variable_ID, Variable_ID> exists_mapping;
	//  GEQ_Handle h = f_root->add_GEQ();

	Variable_ID ub = f_exists->declare();

	coef_t coef = bound_eq.get_coef(hull.set_var(level));
	if (coef == -1) { // e.g. if i <= m+5, then UB = m+5
		EQ_Handle h = f_root->add_EQ();
		h.update_coef(ub, -1);
		for (Constr_Vars_Iter ci(bound_eq); ci; ci++) {
			switch ((*ci).var->kind()) {
			case Input_Var: {
				int pos = (*ci).var->get_position();
				if (pos != level)
					h.update_coef(r.set_var(ci.curr_var()->get_position()),
							ci.curr_coef());
				break;
			}
			case Wildcard_Var: {
				Variable_ID v = replicate_floor_definition(hull, ci.curr_var(),
						r, f_exists, f_root, exists_mapping);
				h.update_coef(v, ci.curr_coef());
				break;
			}
			case Global_Var: {
				Global_Var_ID g = ci.curr_var()->get_global_var();
				Variable_ID v;
				if (g->arity() == 0)
					v = r.get_local(g);
				else
					v = r.get_local(g, ci.curr_var()->function_of());
				h.update_coef(v, ci.curr_coef());
				break;
			}
			default:
				throw loop_error(
						"cannot handle complex upper bound in split_with_alignment!");
			}
		}
		h.update_const(bound_eq.get_const());
	} else { // e.g. if 2i <= m+5, then m+5-2 < 2*UB <= m+5

		throw loop_error(
				"cannot handle complex upper bound in split_with_alignment!");
	}




	Variable_ID aligned_ub = f_exists->declare();

	Variable_ID e = f_exists->declare();
	EQ_Handle h = f_root->add_EQ();
	h.update_coef(aligned_ub, 1);
	h.update_coef(e, -alignment);

	GEQ_Handle h1 = f_root->add_GEQ();
	GEQ_Handle h2 = f_root->add_GEQ();
	h1.update_coef(e, alignment);
	h2.update_coef(e, -alignment);
	h1.update_coef(ub, -1);
	h2.update_coef(ub, 1);
	h2.update_const(1);
	h1.update_const(alignment - 2);

	GEQ_Handle h3 = f_root->add_GEQ();

	h3.update_coef(r.set_var(level), -1);
	h3.update_coef(aligned_ub, 1);
	h3.update_const(-1);

	r.simplify();
	//split(stmt_num, level, r);

	std::vector<int> lex = getLexicalOrder(stmt_num);
	std::set<int> same_loop = getStatements(lex, dim - 1);
    int cur_lex = lex[dim - 1];
	apply_xform(stmt_num);

	int n = stmt[stmt_num].IS.n_set();
	Relation part1 = Intersection(copy(stmt[stmt_num].IS),
				Extend_Set(copy(r), n - level));
	//Relation part2 = Intersection(copy(stmt[stmt_num].IS),
	//			Extend_Set(Complement(copy(r)),
	//					n - level));

	Relation part2;
	{

		Relation r(level);
		F_Exists *f_exists = r.add_and()->add_exists();
		F_And *f_root = f_exists->add_and();
		std::map<Variable_ID, Variable_ID> exists_mapping;
		//  GEQ_Handle h = f_root->add_GEQ();

		Variable_ID ub = f_exists->declare();

		coef_t coef = bound_eq.get_coef(hull.set_var(level));
		if (coef == -1) { // e.g. if i <= m+5, then UB = m+5
			EQ_Handle h = f_root->add_EQ();
			h.update_coef(ub, -1);
			for (Constr_Vars_Iter ci(bound_eq); ci; ci++) {
				switch ((*ci).var->kind()) {
				case Input_Var: {
					int pos = (*ci).var->get_position();
					if (pos != level)
						h.update_coef(r.set_var(ci.curr_var()->get_position()),
								ci.curr_coef());
					break;
				}
				case Wildcard_Var: {
					Variable_ID v = replicate_floor_definition(hull, ci.curr_var(),
							r, f_exists, f_root, exists_mapping);
					h.update_coef(v, ci.curr_coef());
					break;
				}
				case Global_Var: {
					Global_Var_ID g = ci.curr_var()->get_global_var();
					Variable_ID v;
					if (g->arity() == 0)
						v = r.get_local(g);
					else
						v = r.get_local(g, ci.curr_var()->function_of());
					h.update_coef(v, ci.curr_coef());
					break;
				}
				default:
					throw loop_error(
							"cannot handle complex upper bound in split_with_alignment!");
				}
			}
			h.update_const(bound_eq.get_const());
		} else { // e.g. if 2i <= m+5, then m+5-2 < 2*UB <= m+5

			throw loop_error(
					"cannot handle complex upper bound in split_with_alignment!");
		}




		//Working

/*		Variable_ID aligned_lb = f_exists->declare();

		Variable_ID e = f_exists->declare();
			EQ_Handle h = f_root->add_EQ();
			h.update_coef(aligned_lb, 1);
			h.update_coef(e, -alignment);

			GEQ_Handle h1 = f_root->add_GEQ();
			GEQ_Handle h2 = f_root->add_GEQ();
			h1.update_coef(e, alignment);
			h2.update_coef(e, -alignment);
			h1.update_coef(ub, -1);
			h2.update_coef(ub, 1);
			h2.update_const(1);
			h1.update_const(alignment - 2);

			EQ_Handle h3 = f_root->add_EQ();

			h3.update_coef(r.set_var(level), -1);
			h3.update_coef(aligned_lb, 1);


			r.simplify();

			part2 = Intersection(copy(stmt[stmt_num].IS),
						Extend_Set(copy(r), n - level));
        */

	 // e.g. to align at 4, aligned_lb = 4*alpha && LB-4 < 4*alpha <= LB

		/*Variable_ID	aligned_lb = f_exists->declare();
		Variable_ID e = f_exists->declare();

		EQ_Handle h = f_root->add_EQ();
		h.update_coef(aligned_lb, 1);
		h.update_coef(e, -alignment);

	    GEQ_Handle h1 = f_root->add_GEQ();

		h1.update_coef(e, alignment);

	    h1.update_coef(ub, -1);

		h1.update_const(alignment - 1);

	    GEQ_Handle h2 = f_root->add_GEQ();

		h2.update_coef(e, -alignment);

	    h2.update_coef(ub, 1);

		h2.update_const(-1);

		GEQ_Handle h3 = f_root->add_GEQ();

		h3.update_coef(ub, 1);
		h3.update_coef(r.set_var(level), -1);
		h3.update_coef(aligned_lb, -1);
		h3.update_const(-1);


		GEQ_Handle h4 = f_root->add_GEQ();


		h4.update_coef(r.set_var(level), 1);
		h4.update_coef(aligned_lb, -1);

		r.simplify();
		*/


			Variable_ID aligned_lb = f_exists->declare();

			Variable_ID e = f_exists->declare();
			EQ_Handle h = f_root->add_EQ();
			h.update_coef(aligned_lb, 1);
			h.update_coef(e, -alignment);

			GEQ_Handle h1 = f_root->add_GEQ();
			GEQ_Handle h2 = f_root->add_GEQ();
			h1.update_coef(e, alignment);
			h2.update_coef(e, -alignment);
			h1.update_coef(ub, -1);
			h2.update_coef(ub, 1);
			h2.update_const(1);
			h1.update_const(alignment - 2);

			GEQ_Handle h3 = f_root->add_GEQ();

			h3.update_coef(r.set_var(level), 1);
			h3.update_coef(aligned_lb, -1);


			r.simplify();

		part2 = Intersection(copy(stmt[stmt_num].IS),
					Extend_Set(copy(r), n - level));
	 }



	part1.simplify(2,4);
	part2.simplify(2,4);
	stmt[stmt_num].IS = part1;


	//if (Intersection(copy(part2),
	//		Extend_Set(copy(this->known), n - this->known.n_set())).is_upper_bound_satisfiable()) {
		Statement new_stmt;
		new_stmt.code = stmt[stmt_num].code->clone();
		new_stmt.IS = part2;
		new_stmt.xform = copy(stmt[stmt_num].xform);
		new_stmt.ir_stmt_node = NULL;
		new_stmt.loop_level = stmt[stmt_num].loop_level;
        new_stmt.has_inspector = stmt[stmt_num].has_inspector;
        new_stmt.reduction = stmt[stmt_num].reduction;
        new_stmt.reductionOp = stmt[stmt_num].reductionOp;
		stmt_nesting_level_.push_back(stmt_nesting_level_[stmt_num]);

        assign_const(new_stmt.xform, dim - 1, cur_lex + 1);


		stmt.push_back(new_stmt);
		uninterpreted_symbols.push_back(uninterpreted_symbols[stmt_num]);
		uninterpreted_symbols_stringrepr.push_back(uninterpreted_symbols_stringrepr[stmt_num]);
		unin_rel.push_back(unin_rel[stmt_num]);
		dep.insert();
		what_stmt_num[stmt_num] = stmt.size() - 1;



	//}

	// update dependence graph
		int dep_dim = get_dep_dim_of(stmt_num, level);
		for (int i = 0; i < stmt.size() -1 ; i++) {
			std::vector<std::pair<int, std::vector<DependenceVector> > > D;

			for (DependenceGraph::EdgeList::iterator j =
					dep.vertex[i].second.begin();
					j != dep.vertex[i].second.end(); j++) {
				if (same_loop.find(i) != same_loop.end()) {
					if (same_loop.find(j->first) != same_loop.end()) {
						if (what_stmt_num.find(i) != what_stmt_num.end()
								&& what_stmt_num.find(j->first)
										!= what_stmt_num.end())
							dep.connect(what_stmt_num[i],
									what_stmt_num[j->first], j->second);
						if (what_stmt_num.find(j->first)
										!= what_stmt_num.end()) {
							std::vector<DependenceVector> dvs;
							for (int k = 0; k < j->second.size(); k++) {
								DependenceVector dv = j->second[k];
								if (dv.is_data_dependence() && dep_dim != -1) {
									dv.lbounds[dep_dim] = -posInfinity;
									dv.ubounds[dep_dim] = posInfinity;
								}
								dvs.push_back(dv);
							}
							if (dvs.size() > 0)
								D.push_back(
										std::make_pair(what_stmt_num[j->first],
												dvs));
						} /*else if (!place_after
								&& what_stmt_num.find(i)
										!= what_stmt_num.end()) {
							std::vector<DependenceVector> dvs;
							for (int k = 0; k < j->second.size(); k++) {
								DependenceVector dv = j->second[k];
								if (dv.is_data_dependence() && dep_dim != -1) {
									dv.lbounds[dep_dim] = -posInfinity;
									dv.ubounds[dep_dim] = posInfinity;
								}
								dvs.push_back(dv);
							}
							if (dvs.size() > 0)
								dep.connect(what_stmt_num[i], j->first, dvs);

						}*/
					} else {
						if (what_stmt_num.find(i) != what_stmt_num.end())
							dep.connect(what_stmt_num[i], j->first, j->second);
					}
				} else if (same_loop.find(j->first) != same_loop.end()) {
					if (what_stmt_num.find(j->first) != what_stmt_num.end())
						D.push_back(
								std::make_pair(what_stmt_num[j->first],
										j->second));
				}
			}

			for (int j = 0; j < D.size(); j++)
				dep.connect(i, D[j].first, D[j].second);
		}


}

std::set<int> Loop::unroll_extra(int stmt_num, int level, int unroll_amount,
		int cleanup_split_level) {
	std::set<int> cleanup_stmts = unroll(stmt_num, level, unroll_amount,
			std::vector<std::vector<std::string> >(), cleanup_split_level);
	for (std::set<int>::iterator i = cleanup_stmts.begin();
			i != cleanup_stmts.end(); i++)
		unroll(*i, level, 0);

	return cleanup_stmts;
}

void Loop::peel(int stmt_num, int level, int peel_amount) {
	// check for sanity of parameters
	if (stmt_num < 0 || stmt_num >= stmt.size())
		throw std::invalid_argument(
				"invalid statement number " + to_string(stmt_num));
	if (level <= 0 || level > stmt[stmt_num].loop_level.size())
		throw std::invalid_argument("invalid loop level " + to_string(level));

	if (peel_amount == 0)
		return;

	std::set<int> subloop = getSubLoopNest(stmt_num, level);
	std::vector<Relation> Rs;
	for (std::set<int>::iterator i = subloop.begin(); i != subloop.end(); i++) {
		Relation r = getNewIS(*i);
		Relation f(r.n_set(), level);
		F_And *f_root = f.add_and();
		for (int j = 1; j <= level; j++) {
			EQ_Handle h = f_root->add_EQ();
			h.update_coef(f.input_var(2 * j), 1);
			h.update_coef(f.output_var(j), -1);
		}
		//Anand composition will fail due to unintepreted function symbols introduced by flattening
		//r = Composition(f, r);
		r = omega::Range(Restrict_Domain(f, r));
		r.simplify();
		Rs.push_back(r);
	}
	Relation hull = SimpleHull(Rs);

	if (peel_amount > 0) {
		GEQ_Handle bound_eq;
		bool found_bound = false;
		for (GEQ_Iterator e(hull.single_conjunct()->GEQs()); e; e++)
			if (!(*e).has_wildcards()
					&& (*e).get_coef(hull.set_var(level)) > 0) {
				bound_eq = *e;
				found_bound = true;
				break;
			}
		if (!found_bound)
			for (GEQ_Iterator e(hull.single_conjunct()->GEQs()); e; e++)
				if ((*e).has_wildcards()
						&& (*e).get_coef(hull.set_var(level)) > 0) {
					bool is_bound = true;
					for (Constr_Vars_Iter cvi(*e, true); cvi; cvi++) {
						std::pair<bool, GEQ_Handle> result =
								find_floor_definition(hull, cvi.curr_var());
						if (!result.first) {
							is_bound = false;
							break;
						}
					}
					if (is_bound) {
						bound_eq = *e;
						found_bound = true;
						break;
					}
				}
		if (!found_bound)
			throw loop_error(
					"can't find lower bound for peeling at loop level "
							+ to_string(level));

		for (int i = 1; i <= peel_amount; i++) {
			Relation r(level);
			F_Exists *f_exists = r.add_and()->add_exists();
			F_And *f_root = f_exists->add_and();
			GEQ_Handle h = f_root->add_GEQ();
			std::map<Variable_ID, Variable_ID> exists_mapping;
			for (Constr_Vars_Iter cvi(bound_eq); cvi; cvi++)
				switch (cvi.curr_var()->kind()) {
				case Input_Var:
					h.update_coef(r.set_var(cvi.curr_var()->get_position()),
							cvi.curr_coef());
					break;
				case Wildcard_Var: {
					Variable_ID v = replicate_floor_definition(hull,
							cvi.curr_var(), r, f_exists, f_root,
							exists_mapping);
					h.update_coef(v, cvi.curr_coef());
					break;
				}
				case Global_Var: {
					Global_Var_ID g = cvi.curr_var()->get_global_var();
					Variable_ID v;
					if (g->arity() == 0)
						v = r.get_local(g);
					else
						v = r.get_local(g, cvi.curr_var()->function_of());
					h.update_coef(v, cvi.curr_coef());
					break;
				}
				default:
					assert(false);
				}
			h.update_const(bound_eq.get_const() - i);
			r.simplify();

			split(stmt_num, level, r);
		}
	} else { // peel_amount < 0
		GEQ_Handle bound_eq;
		bool found_bound = false;
		for (GEQ_Iterator e(hull.single_conjunct()->GEQs()); e; e++)
			if (!(*e).has_wildcards()
					&& (*e).get_coef(hull.set_var(level)) < 0) {
				bound_eq = *e;
				found_bound = true;
				break;
			}
		if (!found_bound)
			for (GEQ_Iterator e(hull.single_conjunct()->GEQs()); e; e++)
				if ((*e).has_wildcards()
						&& (*e).get_coef(hull.set_var(level)) < 0) {
					bool is_bound = true;
					for (Constr_Vars_Iter cvi(*e, true); cvi; cvi++) {
						std::pair<bool, GEQ_Handle> result =
								find_floor_definition(hull, cvi.curr_var());
						if (!result.first) {
							is_bound = false;
							break;
						}
					}
					if (is_bound) {
						bound_eq = *e;
						found_bound = true;
						break;
					}
				}
		if (!found_bound)
			throw loop_error(
					"can't find upper bound for peeling at loop level "
							+ to_string(level));

		for (int i = 1; i <= -peel_amount; i++) {
			Relation r(level);
			F_Exists *f_exists = r.add_and()->add_exists();
			F_And *f_root = f_exists->add_and();
			GEQ_Handle h = f_root->add_GEQ();
			std::map<Variable_ID, Variable_ID> exists_mapping;
			for (Constr_Vars_Iter cvi(bound_eq); cvi; cvi++)
				switch (cvi.curr_var()->kind()) {
				case Input_Var:
					h.update_coef(r.set_var(cvi.curr_var()->get_position()),
							cvi.curr_coef());
					break;
				case Wildcard_Var: {
					Variable_ID v = replicate_floor_definition(hull,
							cvi.curr_var(), r, f_exists, f_root,
							exists_mapping);
					h.update_coef(v, cvi.curr_coef());
					break;
				}
				case Global_Var: {
					Global_Var_ID g = cvi.curr_var()->get_global_var();
					Variable_ID v;
					if (g->arity() == 0)
						v = r.get_local(g);
					else
						v = r.get_local(g, cvi.curr_var()->function_of());
					h.update_coef(v, cvi.curr_coef());
					break;
				}
				default:
					assert(false);
				}
			h.update_const(bound_eq.get_const() - i);
			r.simplify();

			split(stmt_num, level, r);
		}
	}
}

