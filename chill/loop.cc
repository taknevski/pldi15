/*****************************************************************************
 Copyright (C) 2008 University of Southern California
 Copyright (C) 2009-2010 University of Utah
 All Rights Reserved.

 Purpose:
 Core loop transformation functionality.

 Notes:
 "level" (starting from 1) means loop level and it corresponds to "dim"
 (starting from 0) in transformed iteration space [c_1,l_1,c_2,l_2,....,
 c_n,l_n,c_(n+1)], e.g., l_2 is loop level 2 in generated code, dim 3
 in transformed iteration space, and variable 4 in Omega relation.
 All c's are constant numbers only and they will not show up as actual loops.
 Formula:
 dim = 2*level - 1
 var = dim + 1
 
 History:
 10/2005 Created by Chun Chen.
 09/2009 Expand tile functionality, -chun
 10/2009 Initialize unfusible loop nest without bailing out, -chun
 *****************************************************************************/

#include <limits.h>
#include <math.h>
#include <codegen.h>
#include <code_gen/CG_utils.h>
#include <code_gen/CG_roseBuilder.h> // Manu
#include <code_gen/CG_stringRepr.h>
#include <iostream>
#include <algorithm>
#include <map>
#include "loop.hh"
#include "omegatools.hh"
#include "irtools.hh"
#include "chill_error.hh"
#include <string.h>
#include <list>
bool mycompare(std::pair<int, int> i, std::pair<int, int> j) {
	return i.first < j.first;
}
omega::Relation Loop::getNewIS(int stmt_num) const {

	omega::Relation result;

	if (stmt[stmt_num].xform.is_null()) {
		omega::Relation known = omega::Extend_Set(omega::copy(this->known),
				stmt[stmt_num].IS.n_set() - this->known.n_set());
		result = omega::Intersection(omega::copy(stmt[stmt_num].IS), known);
	} else {

		omega::Relation known = omega::copy(this->known);

		if (known.n_set() < stmt[stmt_num].xform.n_out()) {
			omega::Relation known = omega::Extend_Set(omega::copy(this->known),
					stmt[stmt_num].xform.n_out() - this->known.n_set());

			result = omega::Intersection(
					omega::Range(
							omega::Restrict_Domain(
									omega::copy(stmt[stmt_num].xform),
									omega::copy(stmt[stmt_num].IS))), known);
		} else {

			result = omega::Range(
					omega::Restrict_Domain(omega::copy(stmt[stmt_num].xform),
							omega::copy(stmt[stmt_num].IS)));

			result = omega::Extend_Set(result,
					this->known.n_set() - stmt[stmt_num].xform.n_out());
			result = omega::Intersection(result, known);

		}
	}

	result.simplify(2, 4);

	return result;
}

void Loop::reduce(int stmt_num, std::vector<int> &level, int param,
		std::string func_name, std::vector<int> &seq_levels,
		std::vector<int> cudaized_levels, int bound_level) {
   // std::cout << "Reducing stmt# " << stmt_num << " at level " << level[0] << "\n";
	// ir->printStmt(stmt[stmt_num].code);

	/*	if (stmt[stmt_num].reduction != 1) {
	 std::cout << "Cannot reduce this statement\n";
	 return;
	 }
	 */
	/*for (int i = 0; i < level.size(); i++)
	 if (stmt[stmt_num].loop_level[level[i] - 1].segreducible != true) {
	 std::cout << "Cannot reduce this statement\n";
	 return;
	 }
	 for (int i = 0; i < seq_levels.size(); i++)
	 if (stmt[stmt_num].loop_level[seq_levels[i] - 1].segreducible != true) {
	 std::cout << "Cannot reduce this statement\n";
	 return;
	 }
	 */
//	std::pair<int, std::string> to_insert(level, func_name);
//	reduced_statements.insert(std::pair<int, std::pair<int, std::string> >(stmt_num, to_insert ));
	// invalidate saved codegen computation
	delete last_compute_cgr_;
	last_compute_cgr_ = NULL;
	delete last_compute_cg_;
	last_compute_cg_ = NULL;

	omega::CG_outputBuilder *ocg = ir->builder();

	omega::CG_outputRepr *funCallRepr;
	std::vector<omega::CG_outputRepr *> arg_repr_list;
	apply_xform(stmt_num);

	IR_Control * stmts = ir->GetCode(stmt[stmt_num].code);
	omega::CG_outputRepr *cond = NULL;
	if (stmts)
		if (stmts->type() == IR_CONTROL_IF)
			cond = static_cast<IR_If *>(stmts)->condition();

	std::vector<IR_ArrayRef *> access;
	if (!cond)
		access = ir->FindArrayRef(stmt[stmt_num].code);
	else
		access = ir->FindArrayRef(
				static_cast<IR_If *>(stmts)->then_body()->extract());
	std::set<std::string> names;
	for (int i = 0; i < access.size(); i++) {
		std::vector<IR_ArrayRef *> access2;
		for (int j = 0; j < access[i]->n_dim(); j++) {
			std::vector<IR_ArrayRef *> access3 = ir->FindArrayRef(
					access[i]->index(j));
			access2.insert(access2.end(), access3.begin(), access3.end());
		}
		if (access2.size() == 0) {
			if (names.find(access[i]->name()) == names.end()) {
				arg_repr_list.push_back(
						ocg->CreateAddressOf(access[i]->convert()));
				names.insert(access[i]->name());
				if (access[i]->is_write())
					reduced_write_refs.insert(access[i]->name());
			}
		} else {
			if (names.find(access[i]->name()) == names.end()) {
				arg_repr_list.push_back(
						ocg->CreateAddressOf(
								ocg->CreateArrayRefExpression(
										ocg->CreateIdent(access[i]->name()),
										ocg->CreateInt(0))));
				names.insert(access[i]->name());
				if (access[i]->is_write())
					reduced_write_refs.insert(access[i]->name());
			}
		}
	}

	for (int i = 0; i < seq_levels.size(); i++)
		arg_repr_list.push_back(
				ocg->CreateIdent(
						stmt[stmt_num].IS.set_var(seq_levels[i])->name()));

	if (bound_level != -1) {

		omega::Relation new_IS = copy(stmt[stmt_num].IS);
		new_IS.copy_names(stmt[stmt_num].IS);
		new_IS.setup_names();
		new_IS.simplify();
		int dim = bound_level;
		//omega::Relation r = getNewIS(stmt_num);
		for (int j = dim + 1; j <= new_IS.n_set(); j++)
			new_IS = omega::Project(new_IS, new_IS.set_var(j));

		new_IS.simplify(2, 4);

		omega::Relation bound_ = get_loop_bound(copy(new_IS), dim - 1);
		omega::Variable_ID v = bound_.set_var(dim);
		std::vector<omega::CG_outputRepr *> ubList;
		for (omega::GEQ_Iterator e(
				const_cast<omega::Relation &>(bound_).single_conjunct()->GEQs());
				e; e++) {
			if ((*e).get_coef(v) < 0) {
				//	&& (*e).is_const_except_for_global(v))
				omega::CG_outputRepr *UPPERBOUND =
						omega::output_upper_bound_repr(ir->builder(), *e, v,
								bound_,
								std::vector<
										std::pair<omega::CG_outputRepr *, int> >(
										bound_.n_set(),
										std::make_pair(
												static_cast<omega::CG_outputRepr *>(NULL),
												0)),
								uninterpreted_symbols[stmt_num]);
				if (UPPERBOUND != NULL)
					ubList.push_back(UPPERBOUND);

			}

		}

		omega::CG_outputRepr * ubRepr;
		if (ubList.size() > 1) {

			ubRepr = ir->builder()->CreateInvoke("min", ubList);
			arg_repr_list.push_back(ubRepr);
		} else if (ubList.size() == 1)
			arg_repr_list.push_back(ubList[0]);
	}

	funCallRepr = ocg->CreateInvoke(func_name, arg_repr_list);

	if (cond)
		stmt[stmt_num].code = ocg->CreateIf(0, cond, funCallRepr, NULL);
	else
		stmt[stmt_num].code = funCallRepr;

	std::vector<std::string> loop_vars;
		std::vector<omega::CG_outputRepr *> subs;
	for (int i = 0; i < level.size(); i++) {
		//stmt[*i].code = outputStatement(ocg, stmt[*i].code, 0, mapping, known, std::vector<CG_outputRepr *>(mapping.n_out(), NULL));

		loop_vars.push_back(stmt[stmt_num].IS.set_var(level[i])->name());


		subs.push_back(ocg->CreateInt(0));
	}
		stmt[stmt_num].code = ocg->CreateSubstitutedStmt(0, stmt[stmt_num].code,
				loop_vars, subs, false);



	omega::Relation new_IS = omega::copy(stmt[stmt_num].IS);
	new_IS.copy_names(stmt[stmt_num].IS);
	new_IS.setup_names();
	new_IS.simplify();
	int old_size = new_IS.n_set();

	omega::Relation R = omega::copy(stmt[stmt_num].IS);
	R.copy_names(stmt[stmt_num].IS);
	R.setup_names();

	for (int i = level.size() - 1; i >= 0; i--) {
		int j;

		for (j = 0; j < cudaized_levels.size(); j++) {
			if (cudaized_levels[j] == level[i])
				break;

		}

		if (j == cudaized_levels.size() ) {

			R = omega::Project(R, level[i], omega::Input_Var);
			R.simplify();

		}
		//

	}

	omega::F_And *f_Root = R.and_with_and();
	for (int i = level.size() - 1; i >= 0; i--) {
		int j;

		for (j = 0; j < cudaized_levels.size(); j++) {
			if (cudaized_levels[j] == level[i])
				break;

		}

		if (j == cudaized_levels.size() ) {

			omega::EQ_Handle h = f_Root->add_EQ();

			h.update_coef(R.set_var(level[i]), 1);
			h.update_const(-1);
		}
		//

	}

	R.simplify();

	stmt[stmt_num].IS = R;

	/*if (j == cudaized_levels.size()) {

	 std::set<int> positions_to_elide;
	 omega::Relation R = omega::copy(stmt[stmt_num].IS);
	 omega::Conjunct *c =
	 const_cast<omega::Relation &>(R).single_conjunct();

	 omega::Variable_ID curr = R.set_var(level[i]);

	 bool new_pos_found = true;

	 positions_to_elide.insert(level[i]);

	 while (new_pos_found) {

	 new_pos_found = false;
	 for (omega::EQ_Iterator e(c->EQs()); e; e++) {
	 if ((*e).get_coef(curr) != 0) {
	 std::set<int> positions_found;
	 for (omega::Constr_Vars_Iter cvi(*e); cvi; cvi++) {
	 if (cvi.curr_var()->kind() == omega::Input_Var) {
	 int pos = cvi.curr_var()->get_position();
	 if (cvi.curr_var() != curr) {
	 positions_found.insert(pos);
	 }
	 }
	 }
	 if (positions_found.size() == 1) {
	 if (positions_to_elide.find(
	 *(positions_found.begin()))
	 == positions_to_elide.end()) {
	 positions_to_elide.insert(
	 *(positions_found.begin()));
	 new_pos_found = true;
	 curr = R.set_var(*(positions_found.begin()));
	 }
	 } else if (positions_found.size() > 1) {
	 int max = -1;
	 for (std::set<int>::iterator it =
	 positions_found.begin();
	 it != positions_found.end(); it++) {
	 if (*it > max)
	 max = *it;
	 }
	 if (positions_to_elide.find(max)
	 == positions_to_elide.end()) {
	 positions_to_elide.insert(max);
	 new_pos_found = true;
	 curr = R.set_var(max);
	 }
	 }
	 }
	 }
	 }

	 int diff = positions_to_elide.size();

	 for (std::set<int>::reverse_iterator it =
	 positions_to_elide.rbegin();
	 it != positions_to_elide.rend(); it++) {

	 int n = new_IS.n_set();

	 omega::Relation r(n, n - 1);
	 omega::F_And *f_root = r.add_and();
	 for (int j = 1; j < *it; j++) {
	 omega::EQ_Handle h = f_root->add_EQ();

	 h.update_coef(r.input_var(j), 1);
	 h.update_coef(r.output_var(j), -1);
	 }

	 for (int j = *it + 1; j <= n; j++) {
	 omega::EQ_Handle h = f_root->add_EQ();

	 h.update_coef(r.input_var(j), 1);
	 h.update_coef(r.output_var(j - 1), -1);
	 }
	 new_IS = omega::Range(omega::Restrict_Domain(r, new_IS));
	 new_IS.simplify();

	 //stmt[stmt_num].IS = new_IS;
	 const int m = 2 * n + 1;
	 std::vector<int> lex(m, 0);

	 for (int l = 0; l < m; l += 2) {
	 lex[l] = get_const(stmt[stmt_num].xform, l,
	 omega::Output_Var);
	 }

	 // replace original transformation relation with straight 1-1 mapping
	 omega::Relation mapping = omega::Relation(n - 1, 2 * n - 1);
	 omega::F_And *f_root_ = mapping.add_and();
	 for (int j = 1; j <= n - 1; j++) {
	 omega::EQ_Handle h = f_root_->add_EQ();
	 h.update_coef(mapping.output_var(2 * j), 1);
	 h.update_coef(mapping.input_var(j), -1);
	 }
	 for (int j = 1; j <= 2 * n - 1; j += 2) {
	 omega::EQ_Handle h = f_root_->add_EQ();
	 h.update_coef(mapping.output_var(j), 1);
	 if (j < 2 * level[i])
	 h.update_const(-lex[j - 1]);
	 else
	 h.update_const(-lex[j + 1]);
	 }
	 stmt[stmt_num].xform = mapping;
	 stmt[stmt_num].xform.simplify();

	 }

	 }
	 */
	/*int n = new_IS.n_set();
	 Relation mapping_(2 * n + 1, n);
	 F_And *froot = mapping_.add_and();
	 for (int j = 1; j <= n; j++) {
	 EQ_Handle h = froot->add_EQ();
	 h.update_coef(mapping_.output_var(j), 1);
	 h.update_coef(mapping_.input_var(2 * j), -1);
	 }
	 mapping_ = Composition(mapping_, copy(stmt[stmt_num].xform));
	 mapping_.simplify();

	 // match omega input/output variables to variable names in the code
	 for (int j = 1; j <= n; j++)
	 mapping_.name_input_var(j, stmt[stmt_num].IS.set_var(j)->name());
	 for (int j = 1; j <= n; j++)
	 mapping_.name_output_var(j,
	 tmp_loop_var_name_prefix
	 + to_string(tmp_loop_var_name_counter + j - 1));
	 mapping_.setup_names();

	 //Relation known = Extend_Set(copy(this->known),
	 //		mapping_.n_out() - this->known.n_set());
	 //stmt[*i].code = outputStatement(ocg, stmt[*i].code, 0, mapping, known, std::vector<CG_outputRepr *>(mapping.n_out(), NULL));
	 std::vector<std::string> loop_vars;
	 for (int j = 1; j <= n; j++)
	 loop_vars.push_back(stmt[stmt_num].IS.set_var(j)->name());
	 std::vector<CG_outputRepr *> subs = output_substitutions(ocg,
	 Inverse(copy(mapping_)),
	 std::vector<std::pair<CG_outputRepr *, int> >(mapping_.n_out(),
	 std::make_pair(static_cast<CG_outputRepr *>(NULL), 0)));
	 stmt[stmt_num].code = ocg->CreateSubstitutedStmt(0, stmt[stmt_num].code,
	 loop_vars, subs);
	 tmp_loop_var_name_counter += n;
	 */

//for (int t = 1; t <= new_IS.n_set(); t++)
//	new_IS.name_set_var(t, stmt[stmt_num].IS.set_var(t)->base_name);
//stmt[stmt_num].IS = new_IS;
//	int dim = 2 * level - 1;
// check for sanity of parameters
	/*	std::vector<int> ref_lex;
	 int ref_stmt_num;
	 if (stmt_num < 0)
	 throw std::invalid_argument(
	 "invalid statement number " + to_string(stmt_num));
	 if (level <= 0
	 || (level > (stmt[stmt_num].xform.n_out() - 1) / 2
	 || level > stmt[stmt_num].loop_level.size()))
	 throw std::invalid_argument("invalid loop level " + to_string(level));
	 if (ref_lex.size() == 0) {
	 ref_lex = getLexicalOrder(stmt_num);
	 ref_stmt_num = stmt_num;
	 } else {
	 std::vector<int> lex = getLexicalOrder(stmt_num);
	 for (int j = 0; j < dim - 1; j += 2)
	 if (lex[j] != ref_lex[j])
	 throw std::invalid_argument(
	 "statements for fusion must be in the same level-"
	 + to_string(level - 1) + " subloop");
	 }

	 std::vector<IR_ArrayRef *> access = ir->FindArrayRef(stmt[stmt_num].code);
	 */
//	IR_Control *irc = ir->FromForStmt(stmt[stmt_num].code);
// Anand : 06/19/2013 commenting out the code below for segreduce
	/*	CG_outputBuilder *ocg = ir->builder();
	 CG_outputRepr *funCallRepr;
	 std::vector<CG_outputRepr *> arg_repr_list;
	 arg_repr_list.push_back(access[0]->convert());
	 funCallRepr = ocg->CreateInvoke("reduce", arg_repr_list);
	 stmt[stmt_num].code = funCallRepr;
	 omega::coef_t lb, ub;
	 stmt[stmt_num].IS.query_variable_bounds(stmt[stmt_num].IS.set_var(level),
	 lb, ub);

	 //	std::cout << "Upper Bound = " << ub << "\n";
	 assign_const(stmt[stmt_num].xform, dim, ub);
	 apply_xform(stmt_num);
	 */
//}
}
omega::CG_outputRepr * Loop::inspection_code() const {

 IR_ScalarSymbol *a =  ir->CreateScalarSymbol(IR_CONSTANT_INT,0, "inspected_already", true, ir->builder()->CreateInt(0));


   return ir->builder()->CreateIdent(a->name());



}
void Loop::apply_xform(std::set<int> &active) {
	int max_n = 0;

	omega::CG_outputBuilder *ocg = ir->builder();
	for (std::set<int>::iterator i = active.begin(); i != active.end(); i++) {
		int n = stmt[*i].loop_level.size();
		if (n > max_n)
			max_n = n;

		std::vector<int> lex = getLexicalOrder(*i);

		omega::Relation mapping(2 * n + 1, n);
		omega::F_And *f_root = mapping.add_and();
		for (int j = 1; j <= n; j++) {
			omega::EQ_Handle h = f_root->add_EQ();
			h.update_coef(mapping.output_var(j), 1);
			h.update_coef(mapping.input_var(2 * j), -1);
		}
		mapping = omega::Composition(mapping, stmt[*i].xform);
		mapping.simplify();

		// match omega input/output variables to variable names in the code
		for (int j = 1; j <= stmt[*i].IS.n_set(); j++)
			mapping.name_input_var(j, stmt[*i].IS.set_var(j)->name());
		for (int j = 1; j <= n; j++)
			mapping.name_output_var(j,
					tmp_loop_var_name_prefix
							+ omega::to_string(
									tmp_loop_var_name_counter + j - 1));
		mapping.setup_names();

		//omega::Relation known = Extend_Set(copy(this->known),
		//mapping.n_out() - this->known.n_set());
		//stmt[*i].code = outputStatement(ocg, stmt[*i].code, 0, mapping, known, std::vector<CG_outputRepr *>(mapping.n_out(), NULL));

		omega::CG_roseBuilder *ocgr =
				dynamic_cast<omega::CG_roseBuilder *>(ir->builder());
		omega::CG_stringBuilder *ocgs = new omega::CG_stringBuilder;
		if (uninterpreted_symbols[*i].size() == 0) {

			std::set<std::string> globals;

			for (omega::DNF_Iterator di(stmt[*i].IS.query_DNF()); di; di++) {

				for (omega::Constraint_Iterator e(*di); e; e++) {
					for (omega::Constr_Vars_Iter cvi(*e); cvi; cvi++) {
						omega::Variable_ID v = cvi.curr_var();
						if (v->kind() == omega::Global_Var
								&& v->get_global_var()->arity() > 0
								&& globals.find(v->name()) == globals.end()) {
							omega::Global_Var_ID g = v->get_global_var();
							globals.insert(v->name());
							std::vector<omega::CG_outputRepr *> reprs;
							std::vector<omega::CG_outputRepr *> reprs2;
							std::vector<omega::Relation> reprs3;
							for (int l = 1; l <= g->arity(); l++) {
								omega::CG_outputRepr *temp = ocgr->CreateIdent(
										stmt[*i].IS.set_var(l)->name());
								omega::CG_outputRepr *temp2 = ocgs->CreateIdent(
										stmt[*i].IS.set_var(l)->name());

								reprs.push_back(temp);
								reprs2.push_back(temp2);

								omega::Relation mapping1(mapping.n_out(), 1);
								omega::F_And *f_root = mapping1.add_and();
								omega::EQ_Handle h = f_root->add_EQ();
								h.update_coef(mapping1.output_var(1), 1);
								h.update_coef(mapping1.input_var(l), -1);
								omega::Relation r = Composition(mapping1,
										copy(mapping));
								r.simplify();

								omega::Variable_ID v = r.output_var(1);

								std::vector<
										std::pair<omega::CG_outputRepr *, int> > atof =
										std::vector<
												std::pair<
														omega::CG_outputRepr *,
														int> >(
												stmt[*i].xform.n_out(),
												std::make_pair(
														static_cast<omega::CG_outputRepr *>(NULL),
														0));
								std::pair<omega::EQ_Handle, int> result1 =
										find_simplest_assignment(r, v, atof);

								omega::Relation c = omega::copy(r);
								reprs3.push_back(c);

							}
							uninterpreted_symbols[*i].insert(
									std::pair<std::string,
											std::vector<omega::CG_outputRepr *> >(
											v->get_global_var()->base_name(),
											reprs));
							uninterpreted_symbols_stringrepr[*i].insert(
									std::pair<std::string,
											std::vector<omega::CG_outputRepr *> >(
											v->get_global_var()->base_name(),
											reprs2));
							unin_rel[*i].insert(
									std::pair<std::string,
											std::vector<omega::Relation> >(
											v->get_global_var()->base_name(),
											reprs3));
						}
					}
				}
			}
		}

		std::vector<std::string> loop_vars;

		for (int j = 1; j <= stmt[*i].IS.n_set(); j++)
			loop_vars.push_back(stmt[*i].IS.set_var(j)->name());

		std::vector<omega::CG_outputRepr *> subs = omega::output_substitutions(
				ocg, omega::Inverse(omega::copy(mapping)),
				std::vector<std::pair<omega::CG_outputRepr *, int> >(
						mapping.n_out(),
						std::make_pair(
								static_cast<omega::CG_outputRepr *>(NULL), 0)),
				uninterpreted_symbols[*i]);
		std::vector<omega::CG_outputRepr *> subs2;
		for (int l = 0; l < subs.size(); l++)
			subs2.push_back(subs[l]->clone());

		for (std::map<std::string, std::vector<omega::CG_outputRepr *> >::iterator it =
				uninterpreted_symbols[*i].begin();
				it != uninterpreted_symbols[*i].end(); it++) {

			std::vector<omega::CG_outputRepr *> reprs_ = it->second;
			std::vector<omega::CG_outputRepr *> reprs_2;
			for (int k = 0; k < reprs_.size(); k++) {
				std::vector<omega::CG_outputRepr *> subs;
				for (int l = 0; l < subs2.size(); l++)
					subs.push_back(subs2[l]->clone());
				reprs_2.push_back(
						ocgr->CreateSubstitutedStmt(0, reprs_[k]->clone(),
								loop_vars, subs, false, "", true));
			}

			it->second = reprs_2;

		}

		std::vector<omega::CG_outputRepr *> subs3 = omega::output_substitutions(
				ocgs, omega::Inverse(omega::copy(mapping)),
				std::vector<std::pair<omega::CG_outputRepr *, int> >(
						mapping.n_out(),
						std::make_pair(
								static_cast<omega::CG_outputRepr *>(NULL), 0)),
				uninterpreted_symbols_stringrepr[*i]);

		std::vector<omega::CG_outputRepr *> subs4;
		for (int l = 0; l < subs3.size(); l++)
			subs4.push_back(subs3[l]->clone());

		for (std::map<std::string, std::vector<omega::CG_outputRepr *> >::iterator it =
				uninterpreted_symbols_stringrepr[*i].begin();
				it != uninterpreted_symbols_stringrepr[*i].end(); it++) {
			char stmt_string[10];
			sprintf(stmt_string, "s%i\0", *i);
			std::string conv_stmt(stmt_string);
			std::vector<omega::CG_outputRepr *> reprs_ = it->second;
			std::vector<omega::CG_outputRepr *> reprs_2;
			for (int k = 0; k < reprs_.size(); k++) {
				std::vector<omega::CG_outputRepr *> subs;
				for (int l = 0; l < subs4.size(); l++)
					subs.push_back(subs4[l]->clone());
				reprs_2.push_back(
						ocgs->CreateSubstitutedStmt(0, reprs_[k]->clone(),
								loop_vars, subs, true, conv_stmt, true));

			}

			it->second = reprs_2;

		}

		char stmt_string[10];
		sprintf(stmt_string, "s%i\0", *i);
		std::string conv_stmt(stmt_string);
		stmt[*i].code = ocg->CreateSubstitutedStmt(0, stmt[*i].code, loop_vars,
				subs, false);
		stmt[*i].IS = omega::Range(
				omega::Restrict_Domain(copy(mapping), stmt[*i].IS));
		stmt[*i].IS.simplify();

		for (std::map<std::string, std::vector<omega::Relation> >::iterator it =
				unin_rel[*i].begin(); it != unin_rel[*i].end(); it++) {

			if (it->second.size() > 0) {
				std::vector<omega::Relation> reprs_ = it->second;
				std::vector<omega::Relation> reprs_2;
				for (int k = 0; k < reprs_.size(); k++) {

					Relation temp = reprs_[k];

					omega::Variable_ID v = temp.output_var(1);

					std::vector<std::pair<omega::CG_outputRepr *, int> > atof =
							std::vector<std::pair<omega::CG_outputRepr *, int> >(
									temp.n_inp(),
									std::make_pair(
											static_cast<omega::CG_outputRepr *>(NULL),
											0));
					std::pair<omega::EQ_Handle, int> result1 =
							find_simplest_assignment(temp, v, atof);

					Relation R(mapping.n_out(), 1);

					int max_pos = -1;

					for (omega::Constr_Vars_Iter cvi(result1.first); cvi;
							cvi++) {
						omega::Variable_ID v = cvi.curr_var();
						switch (v->kind()) {
						case omega::Input_Var: {

							int pos = v->get_position();
							int coef = cvi.curr_coef();
							if (coef < 0)
								coef *= (-1);
							if (pos > max_pos && coef == 1)
								max_pos = pos;

							break;

						}

						default:
							break;
						}
					}

					assert(max_pos > 0);
					int out_pos = -1;
					if(max_pos <= mapping.n_inp()){
					for (EQ_Iterator gi(mapping.single_conjunct()->EQs()); gi;
							gi++) {

						if ((*gi).get_coef(mapping.input_var(max_pos)) != 0)
							for (omega::Constr_Vars_Iter cvi(*gi); cvi; cvi++) {
								omega::Variable_ID v = cvi.curr_var();
								switch (v->kind()) {
								case omega::Output_Var: {

									int pos = v->get_position();
									if (pos > out_pos)
										out_pos = pos;

									break;

								}

								default:
									break;
								}
							}
					}
					assert(out_pos > 0);

					omega: F_And *f_root = R.add_and();

					omega::EQ_Handle e = f_root->add_EQ();
					e.update_coef(R.output_var(1), -1);
					e.update_coef(R.input_var(out_pos), 1);

					R.simplify();

					/*	temp.simplify();

					 //output_.add_and();

					 for (omega::EQ_Iterator e(
					 reprs_[k].single_conjunct()->EQs()); e; e++) {
					 for (omega::Constr_Vars_Iter c(*e); c; c++) {
					 if (c.curr_var()->kind() == omega::Input_Var) {
					 omega::Relation temp = omega::Inverse(
					 omega::copy(mapping2));

					 omega::Variable_ID v = temp.output_var(
					 c.curr_var()->get_position());

					 std::vector<
					 std::pair<omega::CG_outputRepr *, int> > atof =
					 std::vector<
					 std::pair<
					 omega::CG_outputRepr *,
					 int> >(temp.n_out(),
					 std::make_pair(
					 static_cast<omega::CG_outputRepr *>(NULL),
					 0));
					 std::pair<omega::EQ_Handle, int> result1 =
					 find_simplest_assignment(temp, v, atof);

					 omega::Relation r(temp.n_inp(), 1);

					 omega::F_And *root = r.add_and();
					 omega::EQ_Handle h1 = root->add_EQ();

					 h1.update_coef(r.output_var(1), -1);

					 for (omega::Constr_Vars_Iter c1(result1.first);
					 c1; c1++) {

					 if (result1.first.get_const() > 0)
					 h1.update_const(
					 result1.first.get_const());
					 else
					 h1.update_const(
					 -(result1.first.get_const()));

					 omega::Variable_ID v = c1.curr_var();
					 switch (v->kind()) {
					 case omega::Input_Var: {
					 int coef = c1.curr_coef();
					 if (coef < 0)
					 coef *= (-1);
					 h1.update_coef(
					 r.input_var(v->get_position()),
					 coef);

					 break;
					 }
					 case omega::Wildcard_Var: {
					 omega::F_Exists *f_exists =
					 root->add_exists();
					 int coef = c1.curr_coef();
					 if (coef < 0)
					 coef *= (-1);
					 std::map<omega::Variable_ID,
					 omega::Variable_ID> exists_mapping;
					 omega::Variable_ID v2 =
					 replicate_floor_definition(temp,
					 v, r, f_exists, root,
					 exists_mapping);
					 h1.update_coef(v2, coef);
					 break;
					 }
					 case omega::Global_Var: {
					 omega::Global_Var_ID g =
					 v->get_global_var();
					 omega::Variable_ID v2;
					 int coef = c1.curr_coef();
					 if (coef < 0)
					 coef *= (-1);
					 if (g->arity() == 0)
					 v2 = r.get_local(g);
					 else
					 v2 = r.get_local(g,
					 v->function_of());
					 h1.update_coef(v2, coef);
					 break;
					 }
					 case omega::Output_Var: {

					 break;

					 }
					 default:
					 assert(false);
					 }

					 //h1.update_const(result1.first.get_const());

					 }

					 if (first) {
					 output_ = r;
					 first = false;
					 } else {
					 for (omega::EQ_Iterator e(
					 r.single_conjunct()->EQs()); e; e++)
					 output_.and_with_EQ(*e);
					 }

					 }

					 }

					 }
					 */
					reprs_2.push_back(R);
					}
					else
						reprs_2.push_back(reprs_[k]);
				}

				it->second = reprs_2;

			}
		}

		// replace original transformation relation with straight 1-1 mapping
		mapping = omega::Relation(n, 2 * n + 1);
		f_root = mapping.add_and();
		for (int j = 1; j <= n; j++) {
			omega::EQ_Handle h = f_root->add_EQ();
			h.update_coef(mapping.output_var(2 * j), 1);
			h.update_coef(mapping.input_var(j), -1);
		}
		for (int j = 1; j <= 2 * n + 1; j += 2) {
			omega::EQ_Handle h = f_root->add_EQ();
			h.update_coef(mapping.output_var(j), 1);
			h.update_const(-lex[j - 1]);
		}
		stmt[*i].xform = mapping;

		omega::Relation mapping2 = copy(mapping);
		for (int j = 1; j <= stmt[*i].IS.n_set(); j++)
			mapping2.name_input_var(j, stmt[*i].IS.set_var(j)->name());
		for (int j = 1; j <= n; j++)
			mapping2.name_output_var(j,
					"t" + omega::to_string(tmp_loop_var_name_counter + j - 1));
		mapping2.setup_names();

	}

	tmp_loop_var_name_counter += max_n;

}

using namespace omega;

const std::string Loop::tmp_loop_var_name_prefix = std::string("_t");
const std::string Loop::overflow_var_name_prefix = std::string("over");

//-----------------------------------------------------------------------------
// Class Loop
//-----------------------------------------------------------------------------
// --begin Anand: Added from CHiLL 0.2

bool Loop::isInitialized() const {
	return stmt.size() != 0 && !stmt[0].xform.is_null();
}

//--end Anand: added from CHiLL 0.2

bool Loop::init_loop(std::vector<ir_tree_node *> &ir_tree,
		std::vector<ir_tree_node *> &ir_stmt) {
	ir_stmt = extract_ir_stmts(ir_tree);
	stmt_nesting_level_.resize(ir_stmt.size());
	std::vector<int> stmt_nesting_level(ir_stmt.size());
        vars_to_be_reversed.resize(ir_stmt.size()); 	
      for (int i = 0; i < ir_stmt.size(); i++) {
		ir_stmt[i]->payload = i;
		int t = 0;
		ir_tree_node *itn = ir_stmt[i];
		while (itn->parent != NULL) {
			itn = itn->parent;
			if (itn->content->type() == IR_CONTROL_LOOP)
				t++;
		}
		stmt_nesting_level_[i] = t;
		stmt_nesting_level[i] = t;
	}

	if (actual_code.size() == 0)
		actual_code = std::vector<CG_outputRepr*>(ir_stmt.size());
	stmt = std::vector<Statement>(ir_stmt.size());
	uninterpreted_symbols = std::vector<
			std::map<std::string, std::vector<omega::CG_outputRepr *> > >(
			ir_stmt.size());
	uninterpreted_symbols_stringrepr = std::vector<
			std::map<std::string, std::vector<omega::CG_outputRepr *> > >(
			ir_stmt.size());
	unin_rel =
			std::vector<std::map<std::string, std::vector<omega::Relation> > >(
					ir_stmt.size());

	int n_dim = -1;
	int max_loc;
//std::vector<std::string> index;
	for (int i = 0; i < ir_stmt.size(); i++) {
		int max_nesting_level = -1;
		int loc;
		for (int j = 0; j < ir_stmt.size(); j++)
			if (stmt_nesting_level[j] > max_nesting_level) {
				max_nesting_level = stmt_nesting_level[j];
				loc = j;
			}

		// most deeply nested statement acting as a reference point
		if (n_dim == -1) {
			n_dim = max_nesting_level;
			max_loc = loc;

			index = std::vector<std::string>(n_dim);

			ir_tree_node *itn = ir_stmt[loc];
			int cur_dim = n_dim - 1;
			while (itn->parent != NULL) {
				itn = itn->parent;
				if (itn->content->type() == IR_CONTROL_LOOP) {
					assert(
							static_cast<IR_Loop *>(itn->content)->index().size()
									== 1);
					index[cur_dim] =
							static_cast<IR_Loop *>(itn->content)->index()[0]->name();
					itn->payload = cur_dim--;
				}
			}
		}

		// align loops by names, temporary solution
		ir_tree_node *itn = ir_stmt[loc];
		int depth = stmt_nesting_level_[loc] - 1;
		/*   while (itn->parent != NULL) {
		 itn = itn->parent;
		 if (itn->content->type() == IR_CONTROL_LOOP && itn->payload == -1) {
		 std::string name = static_cast<IR_Loop *>(itn->content)->index()->name();
		 for (int j = 0; j < n_dim; j++)
		 if (index[j] == name) {
		 itn->payload = j;
		 break;
		 }
		 if (itn->payload == -1)
		 throw loop_error("no complex alignment yet");
		 }
		 }
		 */
		for (int t = depth; t >= 0; t--) {
			int y = t;
			ir_tree_node *itn = ir_stmt[loc];

			while ((itn->parent != NULL) && (y >= 0)) {
				itn = itn->parent;
				if (itn->content->type() == IR_CONTROL_LOOP)
					y--;
			}

			if (itn->content->type() == IR_CONTROL_LOOP && itn->payload == -1) {
				CG_outputBuilder *ocg = ir->builder();

				itn->payload = depth - t;

				CG_outputRepr *code =
						static_cast<IR_Block *>(ir_stmt[loc]->content)->extract();

				std::vector<CG_outputRepr *> index_expr;
				std::vector<std::string> old_index;
				CG_outputRepr *repl = ocg->CreateIdent(index[itn->payload]);
				index_expr.push_back(repl);
				assert(
						static_cast<IR_Loop *>(itn->content)->index().size()
								== 1);
				old_index.push_back(
						static_cast<IR_Loop *>(itn->content)->index()[0]->name());
				code = ocg->CreateSubstitutedStmt(0, code, old_index,
						index_expr);

				replace.insert(std::pair<int, CG_outputRepr*>(loc, code));
				//stmt[loc].code = code;

			}
		}

		// set relation variable names
		Relation r(n_dim);
		std::vector<std::string> insp_lb;
		std::vector<std::string> insp_ub;
		F_And *f_root = r.add_and();
		itn = ir_stmt[loc];
		int temp_depth = depth;

		for (int i = 1; i <= n_dim; i++)
			r.name_set_var(i, index[i - 1]);
		while (itn->parent != NULL) {

			itn = itn->parent;
			if (itn->content->type() == IR_CONTROL_LOOP) {
				r.name_set_var(itn->payload + 1, index[temp_depth]);

				temp_depth--;
			}
			//static_cast<IR_Loop *>(itn->content)->index()->name());
		}

		/*while (itn->parent != NULL) {
		 itn = itn->parent;
		 if (itn->content->type() == IR_CONTROL_LOOP)
		 r.name_set_var(itn->payload+1, static_cast<IR_Loop *>(itn->content)->index()->name());
		 }*/

		// extract information from loop/if structures
		std::vector<bool> processed(n_dim, false);
		//std::vector<std::string> vars_to_be_reversed;
		itn = ir_stmt[loc];

		while (itn->parent != NULL) {
			itn = itn->parent;
			//Anand: RED needs a better fix
			//std::vector<omega::Free_Var_Decl*> freevar2;
			//freevar2.clear();

			switch (itn->content->type()) {
			case IR_CONTROL_LOOP: {
				IR_Loop *lp = static_cast<IR_Loop *>(itn->content);
				Variable_ID v = r.set_var(itn->payload + 1);
				int c;

				try {
					c = lp->step_size();
					if (c > 0) {
						CG_outputRepr *lb = lp->lower_bound()[0];

						exp2formula(this, ir, r, f_root, freevar, lb, v, 's',
								IR_COND_GE, true, uninterpreted_symbols[loc],
								uninterpreted_symbols_stringrepr[loc],
								unin_rel[loc]);
						std::vector<CG_outputRepr *> ub = lp->upper_bound();
						std::vector<IR_CONDITION_TYPE> cond = lp->stop_cond();

						if (ub.size() > 1) {
							for (int k = 0; k < ub.size(); k++) {

								if (cond[k] == IR_COND_LT
										|| cond[k] == IR_COND_LE) {

									exp2constraint(this, ir, r, f_root, freevar,
											ub[k], true,
											uninterpreted_symbols[loc],
											uninterpreted_symbols_stringrepr[loc],
											unin_rel[loc]);
									/*				exp2formula(this, ir, r, f_root, freevar, op[0], e,
									 's', IR_COND_EQ, true, uninterpreted_symbols[loc],
									 uninterpreted_symbols_stringrepr[loc],
									 unin_rel[loc]);

									 exp2formula(this, ir, r, f_root, freevar, op[1], e,
									 's', cond[k], true, uninterpreted_symbols[loc],
									 uninterpreted_symbols_stringrepr[loc],
									 unin_rel[loc]);
									 */

								} else
									throw ir_error(
											"loop condition not supported");
							}
						} else {
							if (cond[0] == IR_COND_LT || cond[0] == IR_COND_LE)
								exp2formula(this, ir, r, f_root, freevar, ub[0],
										v, 's', cond[0], true,
										uninterpreted_symbols[loc],
										uninterpreted_symbols_stringrepr[loc],
										unin_rel[loc]);
							else
								throw ir_error("loop condition not supported");

						}
						if ((ir->QueryExpOperation(lp->lower_bound()[0])
								== IR_OP_ARRAY_VARIABLE)
								&& (ir->QueryExpOperation(lp->lower_bound()[0])
										== ir->QueryExpOperation(
												lp->upper_bound()[0]))) {

							std::vector<CG_outputRepr *> v =
									ir->QueryExpOperand(lp->lower_bound()[0]);
							IR_ArrayRef *ref =
									static_cast<IR_ArrayRef *>(ir->Repr2Ref(
											v[0]));
							std::string s0 = ref->name();
							std::vector<CG_outputRepr *> v2 =
									ir->QueryExpOperand(lp->upper_bound()[0]);
							IR_ArrayRef *ref2 =
									static_cast<IR_ArrayRef *>(ir->Repr2Ref(
											v2[0]));
							std::string s1 = ref2->name();

							if (s0 == s1) {
								insp_lb.push_back(s0);
								insp_ub.push_back(s1);

							}

						}

					} else if (c < 0) {
						CG_outputBuilder *ocg = ir->builder();
						CG_outputRepr *lb = lp->lower_bound()[0];
						lb = ocg->CreateMinus(NULL, lb);
						exp2formula(this, ir, r, f_root, freevar, lb, v, 's',
								IR_COND_GE, true, uninterpreted_symbols[loc],
								uninterpreted_symbols_stringrepr[loc],
								unin_rel[loc]);
						CG_outputRepr *ub = lp->upper_bound()[0];
						ub = ocg->CreateMinus(NULL, ub);
						IR_CONDITION_TYPE cond = lp->stop_cond()[0];
						if (cond == IR_COND_GE)
							exp2formula(this, ir, r, f_root, freevar, ub, v,
									's', IR_COND_LE, true,
									uninterpreted_symbols[loc],
									uninterpreted_symbols_stringrepr[loc],
									unin_rel[loc]);
						else if (cond == IR_COND_GT)
							exp2formula(this, ir, r, f_root, freevar, ub, v,
									's', IR_COND_LT, true,
									uninterpreted_symbols[loc],
									uninterpreted_symbols_stringrepr[loc],
									unin_rel[loc]);
						else
							throw ir_error("loop condition not supported");
						assert(lp->index().size() == 1);
						vars_to_be_reversed[loc].push_back(lp->index()[0]->name());
					} else
						throw ir_error("loop step size zero");
				} catch (const ir_error &e) {
					actual_code[loc] =
							static_cast<IR_Block *>(ir_stmt[loc]->content)->extract();
					for (int i = 0; i < itn->children.size(); i++)
						delete itn->children[i];
					itn->children = std::vector<ir_tree_node *>();
					itn->content = itn->content->convert();

					return false;
				}

				if (abs(c) != 1) {
					F_Exists *f_exists = f_root->add_exists();
					Variable_ID e = f_exists->declare();
					F_And *f_and = f_exists->add_and();
					Stride_Handle h = f_and->add_stride(abs(c));
					if (c > 0)
						h.update_coef(e, 1);
					else
						h.update_coef(e, -1);
					h.update_coef(v, -1);
					CG_outputRepr *lb = lp->lower_bound()[0];
					exp2formula(this, ir, r, f_and, freevar, lb, e, 's',
							IR_COND_EQ, true, uninterpreted_symbols[loc],
							uninterpreted_symbols_stringrepr[loc],
							unin_rel[loc]);
				}

				processed[itn->payload] = true;
				break;
			}
			case IR_CONTROL_IF: {
				CG_outputRepr *cond =
						static_cast<IR_If *>(itn->content)->condition();
				try {
					if (itn->payload % 2 == 1)
						exp2constraint(this, ir, r, f_root, freevar, cond, true,
								uninterpreted_symbols[loc],
								uninterpreted_symbols_stringrepr[loc],
								unin_rel[loc]);
					else {
						F_Not *f_not = f_root->add_not();
						F_And *f_and = f_not->add_and();
						exp2constraint(this, ir, r, f_and, freevar, cond, true,
								uninterpreted_symbols[loc],
								uninterpreted_symbols_stringrepr[loc],
								unin_rel[loc]);
					}
				} catch (const ir_error &e) {

					std::vector<ir_tree_node *> *t;
					if (itn->parent == NULL)
						t = &ir_tree;
					else
						t = &(itn->parent->children);
					int id = itn->payload;
					int i = t->size() - 1;
					while (i >= 0) {
						if ((*t)[i] == itn) {
							for (int j = 0; j < itn->children.size(); j++)
								delete itn->children[j];
							itn->children = std::vector<ir_tree_node *>();
							itn->content = itn->content->convert();
						} else if ((*t)[i]->payload >> 1 == id >> 1) {
							delete (*t)[i];
							t->erase(t->begin() + i);
						}
						i--;
					}
					return false;
				}

				break;
			}
			default:
				for (int i = 0; i < itn->children.size(); i++)
					delete itn->children[i];
				itn->children = std::vector<ir_tree_node *>();
				itn->content = itn->content->convert();
				return false;
			}
			//for(int i= 0; i < freevar2.size(); i++)
			//	freevar.push_back(freevar2[i]);
		}

		// add information for missing loops
		for (int j = 0; j < n_dim; j++)
			if (!processed[j]) {
				ir_tree_node *itn = ir_stmt[max_loc];
				while (itn->parent != NULL) {
					itn = itn->parent;
					if (itn->content->type() == IR_CONTROL_LOOP
							&& itn->payload == j)
						break;
				}

				Variable_ID v = r.set_var(j + 1);
				if (loc < max_loc) {

					CG_outputBuilder *ocg = ir->builder();

					CG_outputRepr *lb =
							static_cast<IR_Loop *>(itn->content)->lower_bound()[0];

					//lb = ocg->CreateMinus(lb, ocg->CreateInt(1));
					exp2formula(this, ir, r, f_root, freevar, lb, v, 's',
							IR_COND_EQ, false, uninterpreted_symbols[loc],
							uninterpreted_symbols_stringrepr[loc],
							unin_rel[loc]);

					/*	if (ir->QueryExpOperation(
					 static_cast<IR_Loop *>(itn->content)->lower_bound())
					 == IR_OP_VARIABLE) {
					 IR_ScalarRef *ref =
					 static_cast<IR_ScalarRef *>(ir->Repr2Ref(
					 static_cast<IR_Loop *>(itn->content)->lower_bound()));
					 std::string name_ = ref->name();

					 for (int i = 0; i < index.size(); i++)
					 if (index[i] == name_) {
					 exp2formula(ir, r, f_root, freevar, lb, v, 's',
					 IR_COND_GE, false);

					 CG_outputRepr *ub =
					 static_cast<IR_Loop *>(itn->content)->upper_bound();
					 IR_CONDITION_TYPE cond =
					 static_cast<IR_Loop *>(itn->content)->stop_cond();
					 if (cond == IR_COND_LT || cond == IR_COND_LE)
					 exp2formula(ir, r, f_root, freevar, ub, v,
					 's', cond, false);



					 }

					 }
					 */

				} else { // loc > max_loc

					CG_outputBuilder *ocg = ir->builder();
					CG_outputRepr *ub =
							static_cast<IR_Loop *>(itn->content)->upper_bound()[0];

					exp2formula(this, ir, r, f_root, freevar, ub, v, 's',
							IR_COND_EQ, false, uninterpreted_symbols[loc],
							uninterpreted_symbols_stringrepr[loc],
							unin_rel[loc]);
					/*if (ir->QueryExpOperation(
					 static_cast<IR_Loop *>(itn->content)->upper_bound())
					 == IR_OP_VARIABLE) {
					 IR_ScalarRef *ref =
					 static_cast<IR_ScalarRef *>(ir->Repr2Ref(
					 static_cast<IR_Loop *>(itn->content)->upper_bound()));
					 std::string name_ = ref->name();

					 for (int i = 0; i < index.size(); i++)
					 if (index[i] == name_) {

					 CG_outputRepr *lb =
					 static_cast<IR_Loop *>(itn->content)->lower_bound();

					 exp2formula(ir, r, f_root, freevar, lb, v, 's',
					 IR_COND_GE, false);

					 CG_outputRepr *ub =
					 static_cast<IR_Loop *>(itn->content)->upper_bound();
					 IR_CONDITION_TYPE cond =
					 static_cast<IR_Loop *>(itn->content)->stop_cond();
					 if (cond == IR_COND_LT || cond == IR_COND_LE)
					 exp2formula(ir, r, f_root, freevar, ub, v,
					 's', cond, false);


					 }
					 }
					 */
				}
			}

		r.setup_names();
		r.simplify();

		for (int j = 0; j < insp_lb.size(); j++) {

			std::string lb = insp_lb[j] + "_";
			std::string ub = lb + "_";

			Global_Var_ID u, l;
			bool found_ub = false;
			bool found_lb = false;
			for (DNF_Iterator di(copy(r).query_DNF()); di; di++)
				for (Constraint_Iterator ci = (*di)->constraints(); ci; ci++)

					for (Constr_Vars_Iter cvi(*ci); cvi; cvi++) {
						Variable_ID v = cvi.curr_var();
						if (v->kind() == Global_Var)
							if (v->get_global_var()->arity() > 0) {

								std::string name =
										v->get_global_var()->base_name();
								if (name == lb) {
									l = v->get_global_var();
									found_lb = true;
								} else if (name == ub) {
									u = v->get_global_var();
									found_ub = true;
								}
							}

					}

			if (found_lb && found_ub) {
				Relation known_(copy(r).n_set());
				known_.copy_names(copy(r));
				known_.setup_names();
				Variable_ID index_lb = known_.get_local(l, Input_Tuple);
				Variable_ID index_ub = known_.get_local(u, Input_Tuple);
				F_And *fr = known_.add_and();
				GEQ_Handle g = fr->add_GEQ();
				g.update_coef(index_ub, 1);
				g.update_coef(index_lb, -1);
				g.update_const(-1);
				addKnown(known_);

			}

		}

		// insert the statement
		CG_outputBuilder *ocg = ir->builder();
		CG_stringBuilder ocg_s = ir->builder_s();
		std::vector<CG_outputRepr *> reverse_expr;
		for (int j = 0; j < vars_to_be_reversed[loc].size(); j++) {
			CG_outputRepr *repl = ocg->CreateIdent(vars_to_be_reversed[loc][j]);
			repl = ocg->CreateMinus(NULL, repl);
			reverse_expr.push_back(repl->clone());
			std::vector<CG_outputRepr *> reverse;
			reverse.push_back(repl->clone());
			CG_outputRepr *repl_ = ocg_s.CreateIdent(vars_to_be_reversed[loc][j]);
			repl_ = ocg_s.CreateMinus(NULL, repl_);
			std::vector<CG_outputRepr *> reverse_;

			reverse_.push_back(repl_->clone());
			int pos;
			for (int k = 1; k <= r.n_set(); k++)
				if (vars_to_be_reversed[loc][j] == r.set_var(k)->name())
					pos = k;

//			for(int k =0; k < ir_stmt.size(); k++ ){
			for (std::map<std::string, std::vector<omega::CG_outputRepr *> >::iterator it =
					uninterpreted_symbols[loc].begin();
					it != uninterpreted_symbols[loc].end(); it++) {
                                //std::cout<< it->first<<std::endl;
				for (int l = 0; l < freevar.size(); l++) {
					int arity;
					if (freevar[l]->base_name() == it->first) {
						arity = freevar[l]->arity();
						if (arity >= pos) {
							std::vector<CG_outputRepr *> tmp = it->second;
							std::vector<CG_outputRepr *> reverse;
							reverse.push_back(repl->clone());
							std::vector<std::string> tmp2;
							tmp2.push_back(vars_to_be_reversed[loc][j]);
							tmp[pos - 1] = ocg->CreateSubstitutedStmt(0,
									tmp[pos - 1]->clone(), tmp2, reverse);

							it->second = tmp;
							break;
						}
					}

				}

			}

			for (std::map<std::string, std::vector<omega::Relation> >::iterator it =
					unin_rel[loc].begin(); it != unin_rel[loc].end(); it++) {

				if (it->second.size() > 0) {
					for (int l = 0; l < freevar.size(); l++) {
						int arity;
						if (freevar[l]->base_name() == it->first) {
							arity = freevar[l]->arity();
							if (arity >= pos) {

								break;

								std::vector<omega::Relation> reprs_ = it->second;
								std::vector<omega::Relation> reprs_2;
								//					for (int k = 0; k < reprs_.size(); k++) {
								omega::Relation r(reprs_[pos - 1].n_inp(), 1);

								omega::F_And *root = r.add_and();
								omega::EQ_Handle h1 = root->add_EQ();

								h1.update_coef(r.output_var(1), 1);
								for (omega::EQ_Iterator e(
										reprs_[pos - 1].single_conjunct()->EQs());
										e; e++) {
									for (omega::Constr_Vars_Iter c(*e); c;
											c++) {

										if ((*e).get_const() > 0)
											h1.update_const((*e).get_const());
										else
											h1.update_const(
													-((*e).get_const()));

										omega::Variable_ID v = c.curr_var();
										switch (v->kind()) {
										case omega::Input_Var: {
											int coef = c.curr_coef();
											if (coef < 0)
												coef *= (-1);
											h1.update_coef(
													r.input_var(
															v->get_position()),
													coef);

											break;
										}
										case omega::Wildcard_Var: {
											omega::F_Exists *f_exists =
													root->add_exists();
											int coef = c.curr_coef();
											if (coef < 0)
												coef *= (-1);
											std::map<omega::Variable_ID,
													omega::Variable_ID> exists_mapping;
											omega::Variable_ID v2 =
													replicate_floor_definition(
															copy(
																	reprs_[pos
																			- 1]),
															v, r, f_exists,
															root,
															exists_mapping);
											h1.update_coef(v2, coef);
											break;
										}
										default:
											break;

											//h1.update_const(result1.first.get_const());

										}

										//	}

										//}

									}

									reprs_[pos - 1] = r;
									break;
									//}

									it->second = reprs_;

								}
							}

						}

					}

				}
			}
		}

		/*
		 for(std::map<std::string, std::vector<omega::CG_outputRepr *> >::iterator it =  uninterpreted_symbols_stringrepr[k].begin();
		 it != uninterpreted_symbols_stringrepr[k].end(); it++){

		 int arity;
		 for(int l =0; l < freevar.size(); l++){
		 if(freevar[l]->base_name() == it->first)
		 arity =	freevar[l]->arity();
		 if(arity >= pos){
		 std::vector<CG_outputRepr *> tmp = it->second;

		 std::vector<std::string> tmp2;
		 tmp2.push_back(vars_to_be_reversed[j]);
		 tmp[pos-1] = ocg_s.CreateSubstitutedStmt(0, tmp[pos-1], tmp2,
		 reverse_, false, "", true);


		 }


		 }


		 }
		 */

//				}
		CG_outputRepr *code =
				static_cast<IR_Block *>(ir_stmt[loc]->content)->extract();
		code = ocg->CreateSubstitutedStmt(0, code, vars_to_be_reversed[loc],
				reverse_expr);
		stmt[loc].code = code;
		stmt[loc].IS = r;

		//Anand: Add Information on uninterpreted function constraints to
		//Known relation

		stmt[loc].loop_level = std::vector<LoopLevel>(n_dim);
		stmt[loc].ir_stmt_node = ir_stmt[loc];
		stmt[loc].has_inspector = false;
		stmt[loc].has_wavefront_inspector = false;
                for (int i = 0; i < n_dim; i++) {
			stmt[loc].loop_level[i].type = LoopLevelOriginal;
			stmt[loc].loop_level[i].payload = i;
			stmt[loc].loop_level[i].parallel_level = 0;
			stmt[loc].loop_level[i].segreducible = false;
			stmt[loc].loop_level[i].made_dense = false;
		}

		stmt_nesting_level[loc] = -1;
	}

	return true;
}

Loop::Loop(const IR_Control *control) {

	last_compute_cgr_ = NULL;
	last_compute_cg_ = NULL;

	ir = const_cast<IR_Code *>(control->ir_);
	init_code = NULL;
	cleanup_code = NULL;
	tmp_loop_var_name_counter = 1;
	overflow_var_name_counter = 1;
	known = Relation::True(0);

	ir_tree = build_ir_tree(control->clone(), NULL);
//	std::vector<ir_tree_node *> ir_stmt;

	while (!init_loop(ir_tree, ir_stmt)) {
	}

	for (int i = 0; i < stmt.size(); i++) {
		std::map<int, CG_outputRepr*>::iterator it = replace.find(i);

		if (it != replace.end())
			stmt[i].code = it->second;
		else
			stmt[i].code = stmt[i].code;


		Relation r = parseExpWithWhileToRel(stmt[i].code, stmt[i].IS, i);
		r.simplify();
      }
	if (stmt.size() != 0)
		dep = DependenceGraph(stmt[0].IS.n_set());
	else
		dep = DependenceGraph(0);
// init the dependence graph
	for (int i = 0; i < stmt.size(); i++)
		dep.insert();

	for (int i = 0; i < stmt.size(); i++) {
		stmt[i].reduction = 0; // Manu -- initialization

             std::set<std::string> ufs_before;
                std::set<std::string> ufs_after;

         for (std::map<std::string, std::vector<CG_outputRepr *> >::iterator it_ =
                                                     uninterpreted_symbols[i].begin();  it_ != uninterpreted_symbols[i].end(); it_++)
                                                               ufs_before.insert(it_->first);

		for (int j = i; j < stmt.size(); j++) {

			std::pair<std::vector<DependenceVector>,
					std::vector<DependenceVector> > dv = test_data_dependences(
					this, ir, stmt[i].code, stmt[i].IS, stmt[j].code,
					stmt[j].IS, freevar, index, stmt_nesting_level_[i],
					stmt_nesting_level_[j], uninterpreted_symbols[i],
					uninterpreted_symbols_stringrepr[i], unin_rel[i],
					dep_relation);

			for (int k = 0; k < dv.first.size(); k++) {
				if (is_dependence_valid(ir_stmt[i], ir_stmt[j], dv.first[k],
						true))
					dep.connect(i, j, dv.first[k]);
				else {
					dep.connect(j, i, dv.first[k].reverse());
				}

			}
			for (int k = 0; k < dv.second.size(); k++)
				if (is_dependence_valid(ir_stmt[j], ir_stmt[i], dv.second[k],
						false))
					dep.connect(j, i, dv.second[k]);
				else {
					dep.connect(i, j, dv.second[k].reverse());
				}
			// std::pair<std::vector<DependenceVector>,
			//				std::vector<DependenceVector> > dv_ = test_data_dependences(

		}

               for (std::map<std::string, std::vector<CG_outputRepr *> >::iterator it_ =
                                                     uninterpreted_symbols[i].begin();  it_ != uninterpreted_symbols[i].end(); it_++)
                                                          ufs_after.insert(it_->first);

         
                                  if(ufs_before.size() != ufs_after.size()){
                                          for(std::set<std::string>::iterator it = ufs_after.begin(); it != ufs_after.end();it++)
                                                if (ufs_before.find(*it) == ufs_before.end()){

                                                 for (int j = 0; j < vars_to_be_reversed[i].size(); j++) {

                                                       std::map<std::string, std::vector<omega::CG_outputRepr *> >::iterator it2  = uninterpreted_symbols[i].find(*it);


                              

                        CG_outputRepr *repl = ir->builder()->CreateIdent(vars_to_be_reversed[i][j]);
                        repl = ir->builder()->CreateMinus(NULL, repl);
                        std::vector<CG_outputRepr *> reverse;
                        reverse.push_back(repl->clone());
                        int pos;
                        for (int k = 1; k <= stmt[i].IS.n_set(); k++)
                                if (vars_to_be_reversed[i][j] == stmt[i].IS.set_var(k)->name())
                                        pos = k;

                                for (int l = 0; l < freevar.size(); l++) {
                                        int arity;
                                        if (freevar[l]->base_name() == it2->first) {
                                                arity = freevar[l]->arity();
                                                if (arity >= pos) {
                                                        std::vector<CG_outputRepr *> tmp = it2->second;
                                                        std::vector<CG_outputRepr *> reverse;
                                                        reverse.push_back(repl->clone());
                                                        std::vector<std::string> tmp2;
                                                        tmp2.push_back(vars_to_be_reversed[i][j]);
                                                        tmp[pos - 1] = ir->builder()->CreateSubstitutedStmt(0,
                                                                        tmp[pos - 1]->clone(), tmp2, reverse);

                                                        it2->second = tmp;
                                                        break;
                                                }
                                        }

                                }

                        }

        }}
}


// TODO: Reduction check
// Manu:: Initial implementation / algorithm
	std::set<int> reducCand = std::set<int>();
	std::vector<int> canReduce = std::vector<int>();
	for (int i = 0; i < stmt.size(); i++) {
		if (!dep.hasEdge(i, i)) {
			continue;
		}
		// for each statement check if it has all the three dependences (RAW, WAR, WAW)
		// If there is such a statement, it is a reduction candidate. Mark all reduction candidates.
		std::vector<DependenceVector> tdv = dep.getEdge(i, i);
		for (int j = 0; j < tdv.size(); j++) {
			if (tdv[j].is_reduction_cand)
				reducCand.insert(i);
		}
	}
	bool reduc;
	std::set<int>::iterator it;
	for (it = reducCand.begin(); it != reducCand.end(); it++) {
		reduc = true;
		for (int j = 0; j < stmt.size(); j++) {
			if ((*it != j)
					&& (stmt_nesting_level_[*it] < stmt_nesting_level_[j])) {
				if (dep.hasEdge(*it, j) || dep.hasEdge(j, *it)) {
					reduc = false;
					break;
				}
			}
		}
		if (reduc) {
			canReduce.push_back(*it);
			stmt[*it].reduction = 2; // First, assume that reduction is possible with some processing
		}
	}
// If reduction is possible without processing, update the value of the reduction variable to 1
	for (int i = 0; i < canReduce.size(); i++) {
		// Here, assuming that stmtType returns 1 when there is a single statement within stmt[i]
		if (stmtType(ir, stmt[canReduce[i]].code) == 1) {
			stmt[canReduce[i]].reduction = 1;
			IR_OPERATION_TYPE opType;
			opType = getReductionOperator(ir, stmt[canReduce[i]].code);
			stmt[canReduce[i]].reductionOp = opType;
		}
	}

// printing out stuff for debugging

	if (DEP_DEBUG) {
		std::cout << "STATEMENTS THAT CAN BE REDUCED: \n";
		for (int i = 0; i < canReduce.size(); i++) {
			std::cout << "------- " << canReduce[i] << " ------- "
					<< stmt[canReduce[i]].reduction << "\n";
			ir->printStmt(stmt[canReduce[i]].code); // Manu
			if (stmt[canReduce[i]].reductionOp == IR_OP_PLUS)
				std::cout << "Reduction type:: + \n";
			else if (stmt[canReduce[i]].reductionOp == IR_OP_MINUS)
				std::cout << "Reduction type:: - \n";
			else if (stmt[canReduce[i]].reductionOp == IR_OP_MULTIPLY)
				std::cout << "Reduction type:: * \n";
			else if (stmt[canReduce[i]].reductionOp == IR_OP_DIVIDE)
				std::cout << "Reduction type:: / \n";
			else
				std::cout << "Unknown reduction type\n";
		}
	}
// cleanup the IR tree

// init dumb transformation relations e.g. [i, j] -> [ 0, i, 0, j, 0]

	num_dep_dim = 0;
	for (int i = 0; i < stmt.size(); i++) {
		int n = stmt[i].IS.n_set();
		stmt[i].xform = Relation(n, 2 * n + 1);
		F_And *f_root = stmt[i].xform.add_and();

		for (int j = 1; j <= n; j++) {
			EQ_Handle h = f_root->add_EQ();
			h.update_coef(stmt[i].xform.output_var(2 * j), 1);
			h.update_coef(stmt[i].xform.input_var(j), -1);
		}

		for (int j = 1; j <= 2 * n + 1; j += 2) {
			EQ_Handle h = f_root->add_EQ();
			h.update_coef(stmt[i].xform.output_var(j), 1);
		}
		stmt[i].xform.simplify();

		if (stmt[i].IS.n_set() > num_dep_dim)
			num_dep_dim = stmt[i].IS.n_set();
	}

// debug
	/*for (int i = 0; i < stmt.size(); i++) {
	 std::cout << i << ": ";
	 //stmt[i].xform.print();
	 stmt[i].IS.print();
	 std::cout << std::endl;

	 }*/
//end debug
	//Protonu--temp support for omp code
	omp_loop_numbers = std::vector<int>();
	loop_for_omp_parallel_region = -1;
	use_omp_barrier = Barrier;
	omp_parallel_for = 0;
	omp_thrds_to_use = 0;
        wavefront_set = false;
	//end--Protonu
//Anand: initialize known
	Relation known = Relation::True(stmt[0].xform.n_out());
	this->known = known;
}

Loop::~Loop() {

	delete last_compute_cgr_;
	delete last_compute_cg_;

	for (int i = 0; i < stmt.size(); i++)
		if (stmt[i].code != NULL) {
			stmt[i].code->clear();
			delete stmt[i].code;
		}

	for (int i = 0; i < ir_tree.size(); i++)
		delete ir_tree[i];

	if (init_code != NULL) {
		init_code->clear();
		delete init_code;
	}
	if (cleanup_code != NULL) {
		cleanup_code->clear();
		delete cleanup_code;
	}
}

int Loop::get_dep_dim_of(int stmt_num, int level) const {
	if (stmt_num < 0 || stmt_num >= stmt.size())
		throw std::invalid_argument("invaid statement " + to_string(stmt_num));

	if (level < 1 || level > stmt[stmt_num].loop_level.size())
		return -1;

	int trip_count = 0;
	while (true) {
		switch (stmt[stmt_num].loop_level[level - 1].type) {
		case LoopLevelOriginal:
			return stmt[stmt_num].loop_level[level - 1].payload;
		case LoopLevelTile:
			level = stmt[stmt_num].loop_level[level - 1].payload;
			if (level < 1)
				return -1;
			if (level > stmt[stmt_num].loop_level.size())
				throw loop_error(
						"incorrect loop level information for statement " //
						+ to_string(stmt_num));
			break;
		default:
			throw loop_error(
					"unknown loop level information for statement "
							+ to_string(stmt_num));
		}
		trip_count++;
		if (trip_count >= stmt[stmt_num].loop_level.size())
			throw loop_error(
					"incorrect loop level information for statement "
							+ to_string(stmt_num));
	}
}

int Loop::get_last_dep_dim_before(int stmt_num, int level) const {
	if (stmt_num < 0 || stmt_num >= stmt.size())
		throw std::invalid_argument("invaid statement " + to_string(stmt_num));

	if (level < 1)
		return -1;
	if (level > stmt[stmt_num].loop_level.size())
		level = stmt[stmt_num].loop_level.size() + 1;

	for (int i = level - 1; i >= 1; i--)
		if (stmt[stmt_num].loop_level[i - 1].type == LoopLevelOriginal)
			return stmt[stmt_num].loop_level[i - 1].payload;

	return -1;
}

void Loop::print_internal_loop_structure() const {
	for (int i = 0; i < stmt.size(); i++) {
		std::vector<int> lex = getLexicalOrder(i);
		std::cout << "s" << i + 1 << ": ";
		for (int j = 0; j < stmt[i].loop_level.size(); j++) {
			if (2 * j < lex.size())
				std::cout << lex[2 * j];
			switch (stmt[i].loop_level[j].type) {
			case LoopLevelOriginal:
				std::cout << "(dim:" << stmt[i].loop_level[j].payload << ")";
				break;
			case LoopLevelTile:
				std::cout << "(tile:" << stmt[i].loop_level[j].payload << ")";
				break;
			default:
				std::cout << "(unknown)";
			}
			std::cout << ' ';
		}
		for (int j = 2 * stmt[i].loop_level.size(); j < lex.size(); j += 2) {
			std::cout << lex[j];
			if (j != lex.size() - 1)
				std::cout << ' ';
		}
		std::cout << std::endl;
	}
}

CG_outputRepr *Loop::getCode(int effort) const {
	const int m = stmt.size();
	/*	if (m == 0)
	 return NULL;
	 const int n = stmt[0].xform.n_out();

	 if (last_compute_cg_ == NULL) {
	 std::vector<Relation> IS(m);
	 std::vector<Relation> xforms(m);
	 for (int i = 0; i < m; i++) {
	 IS[i] = stmt[i].IS;
	 xforms[i] = stmt[i].xform;
	 }
	 Relation known = Extend_Set(copy(this->known), n - this->known.n_set());

	 last_compute_cg_ = new CodeGen(xforms, IS, known);
	 delete last_compute_cgr_;
	 last_compute_cgr_ = NULL;
	 }

	 if (last_compute_cgr_ == NULL || last_compute_effort_ != effort) {
	 delete last_compute_cgr_;
	 last_compute_cgr_ = last_compute_cg_->buildAST(effort);
	 last_compute_effort_ = effort;
	 }
	 */
	std::vector<std::pair<int, int> > lex;

	for (int i = 0; i < m; i++)
		lex.push_back(
				std::pair<int, int>(get_const(stmt[i].xform, 0, Output_Var),
						i));

	std::sort(lex.begin(), lex.end(), mycompare);

	std::vector<std::vector<int> > outer_loops;

	for (int i = 0; i < m;) {
		std::vector<int> first;
		first.push_back(lex[i].second);
		int j = i + 1;
		while (j < m && lex[j].first == lex[i].first) {
			first.push_back(lex[j].second);
			j++;
		}
		outer_loops.push_back(first);
		i = j;
	}
	CG_outputBuilder *ocg = ir->builder();
	CG_outputRepr * all_loops = NULL;
        int start = -1;
        CG_outputRepr *running = NULL;
	for (int i = 0; i < outer_loops.size(); i++) {

		const int m = outer_loops[i].size();
		if (m == 0)
			continue;
		const int n = stmt[outer_loops[i][0]].xform.n_out();
		std::vector<std::map<std::string, std::vector<omega::CG_outputRepr *> > > uninterpreted_symbols_(
				m);

		//CodeGen::CodeGen(const std::vector<Relation> &xforms, const std::vector<Relation> &IS, const Relation &known, std::vector< std::vector<int> > smtNonSplitLevels_ , std::vector< std::vector<std::string> > loopIdxNames_,  std::vector< std::pair<int, std::string> > syncs_, std::vector<std::map<std::string, std::vector<Relation > > > unin) {
		if (last_compute_cg_ == NULL) {
			std::vector<Relation> IS(m);
			std::vector<Relation> xforms(m);
			std::vector<std::map<std::string, std::vector<Relation> > > unin__;
			for (int j = 0; j < outer_loops[i].size(); j++) {

				unin__.push_back(unin_rel[outer_loops[i][j]]);
				IS[j] = stmt[outer_loops[i][j]].IS;
				xforms[j] = stmt[outer_loops[i][j]].xform;
				uninterpreted_symbols_[j] =
						uninterpreted_symbols[outer_loops[i][j]];

			}
			Relation known = Extend_Set(copy(this->known),
					n - this->known.n_set());

			last_compute_cg_ = new CodeGen(xforms, IS, known,
					std::vector<std::vector<int> >(),
					std::vector<std::vector<std::string> >(),
					std::vector<std::pair<int, std::string> >(), unin__);
			delete last_compute_cgr_;
			last_compute_cgr_ = NULL;
		}

		if (last_compute_cgr_ == NULL || last_compute_effort_ != effort) {
			delete last_compute_cgr_;
			last_compute_cgr_ = last_compute_cg_->buildAST(effort);
			last_compute_effort_ = effort;
		}

		std::vector<CG_outputRepr *> stmts(outer_loops[i].size());
		for (int j = 0; j < outer_loops[i].size(); j++) {
                           stmts[j] = stmt[outer_loops[i][j]].code;
		}

		CG_outputRepr *repr = last_compute_cgr_->printRepr(ocg, stmts,
				uninterpreted_symbols_);

		for (int k = 0; k < outer_loops[i].size(); k++) {
			for (std::map<int, std::pair<std::vector<int>, std::string> >::const_iterator l =
					pragmas.begin(); l != pragmas.end(); l++) {
				if (l->first == outer_loops[i][k]) {
					for (int j = 0; j < l->second.first.size(); j++)
						repr = add_pragma(repr, 1, l->second.first[j],
								l->second.second);

				}

			}
		}

		//Protonu-experiment to check if OMP parallel region works
		if (loop_for_omp_parallel_region >= 0) {
			for (int k = 0; k < outer_loops[i].size(); k++) {
				if (loop_for_omp_parallel_region == outer_loops[i][k]) {
					repr = add_omp_thread_info(repr);
				}
			}
		}

		//Protonu--generating OMP parallel for-loop instead
		if (omp_parallel_for) {
			bool found = false;

			for (int k = 0; k < outer_loops[i].size(); k++) {

				for (std::map<int, std::vector<int> >::const_iterator l =
						omp_threads.begin(); l != omp_threads.end(); l++) {
					if (l->first == outer_loops[i][k]) {
					        std::vector<std::string> prv;
                                                if(omp_prv.find(l->first) != omp_prv.end())
                                                      prv = omp_prv.find(l->first)->second;  
                                                                    
 
                                                for (int j = 0; j < l->second.size(); j++)
						      if(prv.size() > 0)
                                                          repr = add_omp_for_recursive(repr, 0, 0,
                                                                           omp_thrds_to_use, prv);
                                                      else
                                                	repr = add_omp_for_recursive(repr, 0, 0,
									omp_thrds_to_use);

					}

				}

			}

			//repr = add_omp_parallel_for(repr);
			//repr = add_omp_for_recursive(repr,0, 2, omp_thrds_to_use);

		}

        if(wavefront_set){
           bool consistent = stmt[outer_loops[i][0]].has_wavefront_inspector; 

         // for(int i = 0; i < outer_loops.size(); i++)
            for(int j = 1; j < outer_loops[i].size(); j++)  
                  assert(stmt[outer_loops[i][j]].has_wavefront_inspector == consistent);
            //int start = -1;
            if(stmt[outer_loops[i][outer_loops[i].size()-1]].has_wavefront_inspector){ //&& start == -1)
               if(start == 1 || start == -1){
               if (start == 1)
                  all_loops = ocg->StmtListAppend(all_loops, ocg->CreateIf(0, ocg->CreateEQ(this->inspection_code(), ocg->CreateInt(1)),running, NULL));

               
                  start = 0;
                  running = repr;//  ocg->StmtListAppend(running, repr);
               
               
            }
            else{
                  running = ocg->StmtListAppend(running, repr);

            }   
            start = 0;
            }else{
               if( start == -1|| start == 0){
                    //all_loops = ocg->StmtListAppend(all_loops, repr); 
                if (start == 0)
                  all_loops = ocg->StmtListAppend(all_loops, ocg->CreateIf(0, ocg->CreateEQ(this->inspection_code(), ocg->CreateInt(0)),running, NULL));


                  start = 1;
                  running = repr;//  ocg->StmtListAppend(running, repr);
               


               } else{
                  running = ocg->StmtListAppend(running, repr);
                  
               }

               start = 1;
         }
         if(i== outer_loops.size()-1) {
            if(start == 0)
                all_loops = ocg->StmtListAppend(all_loops, ocg->CreateIf(0, ocg->CreateEQ(this->inspection_code(), ocg->CreateInt(0)),running, NULL));
            else
               all_loops = ocg->StmtListAppend(all_loops, ocg->CreateIf(0, ocg->CreateEQ(this->inspection_code(), ocg->CreateInt(1)),running, NULL));
         }

      }
      else
          all_loops = ocg->StmtListAppend(all_loops, repr);
		delete last_compute_cg_;
		last_compute_cg_ = NULL;
	}
	if (init_code != NULL)
		all_loops = ocg->StmtListAppend(init_code->clone(), all_loops);
	if (cleanup_code != NULL)
		all_loops = ocg->StmtListAppend(all_loops, cleanup_code->clone());

	return all_loops;
}

void Loop::printCode(int effort) const {
	const int m = stmt.size();
	if (m == 0)
		return;
	const int n = stmt[0].xform.n_out();

	if (last_compute_cg_ == NULL) {

		std::vector<Relation> IS(m);
		std::vector<Relation> xforms(m);
		std::vector<std::map<std::string, std::vector<Relation> > > unin__;
		for (int j = 0; j < m; j++) {

			unin__.push_back(unin_rel[j]);
			IS[j] = stmt[j].IS;
			xforms[j] = stmt[j].xform;

		}
		Relation known = Extend_Set(copy(this->known), n - this->known.n_set());

		last_compute_cg_ = new CodeGen(xforms, IS, known,
				std::vector<std::vector<int> >(),
				std::vector<std::vector<std::string> >(),
				std::vector<std::pair<int, std::string> >(), unin__);
		delete last_compute_cgr_;
		last_compute_cgr_ = NULL;
	}

	if (last_compute_cgr_ == NULL || last_compute_effort_ != effort) {
		delete last_compute_cgr_;
		last_compute_cgr_ = last_compute_cg_->buildAST(effort);
		last_compute_effort_ = effort;
	}

	std::string repr = last_compute_cgr_->printString(
			uninterpreted_symbols_stringrepr);
	std::cout << repr << std::endl;
}

void Loop::printIterationSpace() const {
	for (int i = 0; i < stmt.size(); i++) {
		std::cout << "s" << i << ": ";
		Relation r = getNewIS(i);
		for (int j = 1; j <= r.n_inp(); j++)
			r.name_input_var(j, CodeGen::loop_var_name_prefix + to_string(j));
		r.setup_names();
		r.print();
	}
}

void Loop::printDependenceGraph() const {
	if (dep.edgeCount() == 0)
		std::cout << "no dependence exists" << std::endl;
	else {
		std::cout << "dependence graph:" << std::endl;
		std::cout << dep;
	}
}

std::vector<Relation> Loop::getNewIS() const {
	const int m = stmt.size();

	std::vector<Relation> new_IS(m);
	for (int i = 0; i < m; i++)
		new_IS[i] = getNewIS(i);

	return new_IS;
}

std::vector<int> Loop::getLexicalOrder(int stmt_num) const {
	assert(stmt_num < stmt.size());

	const int n = stmt[stmt_num].xform.n_out();
	std::vector<int> lex(n, 0);

	for (int i = 0; i < n; i += 2)
		lex[i] = get_const(stmt[stmt_num].xform, i, Output_Var);

	return lex;
}

// find the sub loop nest specified by stmt_num and level,
// only iteration space satisfiable statements returned.
std::set<int> Loop::getSubLoopNest(int stmt_num, int level) const {
	assert(stmt_num >= 0 && stmt_num < stmt.size());
	assert(level > 0 && level <= stmt[stmt_num].loop_level.size());

	std::set<int> working;
	for (int i = 0; i < stmt.size(); i++)
		if (const_cast<Loop *>(this)->stmt[i].IS.is_upper_bound_satisfiable()
				&& stmt[i].loop_level.size() >= level)
			working.insert(i);

	for (int i = 1; i <= level; i++) {
		int a = getLexicalOrder(stmt_num, i);
		for (std::set<int>::iterator j = working.begin(); j != working.end();) {
			int b = getLexicalOrder(*j, i);
			if (b != a)
				working.erase(j++);
			else
				++j;
		}
	}

	return working;
}

int Loop::getLexicalOrder(int stmt_num, int level) const {
	assert(stmt_num >= 0 && stmt_num < stmt.size());
	assert(level > 0 && level <= stmt[stmt_num].loop_level.size() + 1);

	Relation &r = const_cast<Loop *>(this)->stmt[stmt_num].xform;
	for (EQ_Iterator e(r.single_conjunct()->EQs()); e; e++)
		if (abs((*e).get_coef(r.output_var(2 * level - 1))) == 1) {
			bool is_const = true;
			for (Constr_Vars_Iter cvi(*e); cvi; cvi++)
				if (cvi.curr_var() != r.output_var(2 * level - 1)) {
					is_const = false;
					break;
				}
			if (is_const) {
				int t = static_cast<int>((*e).get_const());
				return (*e).get_coef(r.output_var(2 * level - 1)) > 0 ? -t : t;
			}
		}

	throw loop_error(
			"can't find lexical order for statement " + to_string(stmt_num)
					+ "'s loop level " + to_string(level));
}

std::set<int> Loop::getStatements(const std::vector<int> &lex, int dim) const {
	const int m = stmt.size();

	std::set<int> same_loops;
	for (int i = 0; i < m; i++) {
		if (dim < 0)
			same_loops.insert(i);
		else {
			std::vector<int> a_lex = getLexicalOrder(i);
			int j;
			for (j = 0; j <= dim; j += 2)
				if (lex[j] != a_lex[j])
					break;
			if (j > dim)
				same_loops.insert(i);
		}
	}

	return same_loops;
}

void Loop::shiftLexicalOrder(const std::vector<int> &lex, int dim, int amount) {
	const int m = stmt.size();

	if (amount == 0)
		return;

	for (int i = 0; i < m; i++) {
		std::vector<int> lex2 = getLexicalOrder(i);

		bool need_shift = true;

		for (int j = 0; j < dim; j++)
			if (lex2[j] != lex[j]) {
				need_shift = false;
				break;
			}

		if (!need_shift)
			continue;

		if (amount > 0) {
			if (lex2[dim] < lex[dim])
				continue;
		} else if (amount < 0) {
			if (lex2[dim] > lex[dim])
				continue;
		}

		assign_const(stmt[i].xform, dim, lex2[dim] + amount);
	}
}

std::vector<std::set<int> > Loop::sort_by_same_loops(std::set<int> active,
		int level) {

	std::set<int> not_nested_at_this_level;
	std::map<ir_tree_node*, std::set<int> > sorted_by_loop;
	std::map<int, std::set<int> > sorted_by_lex_order;
	std::vector<std::set<int> > to_return;
	bool lex_order_already_set = false;
	for (std::set<int>::iterator it = active.begin(); it != active.end();
			it++) {

		if (stmt[*it].ir_stmt_node == NULL)
			lex_order_already_set = true;
	}

	if (lex_order_already_set) {

		for (std::set<int>::iterator it = active.begin(); it != active.end();
				it++) {
			std::map<int, std::set<int> >::iterator it2 =
					sorted_by_lex_order.find(
							get_const(stmt[*it].xform, 2 * (level - 1),
									Output_Var));

			if (it2 != sorted_by_lex_order.end())
				it2->second.insert(*it);
			else {

				std::set<int> to_insert;

				to_insert.insert(*it);

				sorted_by_lex_order.insert(
						std::pair<int, std::set<int> >(
								get_const(stmt[*it].xform, 2 * (level - 1),
										Output_Var), to_insert));

			}

		}

		for (std::map<int, std::set<int> >::iterator it2 =
				sorted_by_lex_order.begin(); it2 != sorted_by_lex_order.end();
				it2++)
			to_return.push_back(it2->second);

	} else {

		for (std::set<int>::iterator it = active.begin(); it != active.end();
				it++) {

			ir_tree_node* itn = stmt[*it].ir_stmt_node;

			itn = itn->parent;

			//while (itn->content->type() != IR_CONTROL_LOOP && itn != NULL)
			//	itn = itn->parent;

			while ((itn != NULL) && (itn->payload != level - 1)) {
				itn = itn->parent;
				while (itn != NULL && itn->content->type() != IR_CONTROL_LOOP)
					itn = itn->parent;
			}
			if (itn == NULL)
				not_nested_at_this_level.insert(*it);
			else {
				std::map<ir_tree_node*, std::set<int> >::iterator it2 =
						sorted_by_loop.find(itn);

				if (it2 != sorted_by_loop.end())
					it2->second.insert(*it);
				else {
					std::set<int> to_insert;

					to_insert.insert(*it);

					sorted_by_loop.insert(
							std::pair<ir_tree_node*, std::set<int> >(itn,
									to_insert));

				}

			}

		}
		if (not_nested_at_this_level.size() > 0) {
			for (std::set<int>::iterator it = not_nested_at_this_level.begin();
					it != not_nested_at_this_level.end(); it++) {
				std::set<int> temp;
				temp.insert(*it);
				to_return.push_back(temp);

			}
		}
		for (std::map<ir_tree_node*, std::set<int> >::iterator it2 =
				sorted_by_loop.begin(); it2 != sorted_by_loop.end(); it2++)
			to_return.push_back(it2->second);
	}
	return to_return;
}

void update_successors(int n, int node_num[], int cant_fuse_with[],
		Graph<std::set<int>, bool> &g, std::list<int> &work_list,
		std::list<bool> &type_list, std::vector<bool> types) {

	std::set<int> disconnect;
	for (Graph<std::set<int>, bool>::EdgeList::iterator i =
			g.vertex[n].second.begin(); i != g.vertex[n].second.end(); i++) {
		int m = i->first;

		if (node_num[m] != -1)
			throw loop_error("Graph input for fusion has cycles not a DAG!!");

		std::vector<bool> check_ = g.getEdge(n, m);

		bool has_bad_edge_path = false;
		for (int i = 0; i < check_.size(); i++)
			if (!check_[i]) {
				has_bad_edge_path = true;
				break;
			}
		if (!types[m]) {
			cant_fuse_with[m] = std::max(cant_fuse_with[m], cant_fuse_with[n]);
		} else {
			if (has_bad_edge_path)
				cant_fuse_with[m] = std::max(cant_fuse_with[m], node_num[n]);
			else
				cant_fuse_with[m] = std::max(cant_fuse_with[m],
						cant_fuse_with[n]);
		}
		disconnect.insert(m);
	}

	for (std::set<int>::iterator i = disconnect.begin(); i != disconnect.end();
			i++) {
		g.disconnect(n, *i);

		bool no_incoming_edges = true;
		for (int j = 0; j < g.vertex.size(); j++)
			if (j != *i)
				if (g.hasEdge(j, *i)) {
					no_incoming_edges = false;
					break;
				}

		if (no_incoming_edges) {
			work_list.push_back(*i);
			type_list.push_back(types[*i]);
		}

	}

}

int Loop::getMinLexValue(std::set<int> stmts, int level) {

	int min;

	std::set<int>::iterator it = stmts.begin();

	min = getLexicalOrder(*it, level);

	for (; it != stmts.end(); it++) {
		int curr = getLexicalOrder(*it, level);
		if (curr < min)
			min = curr;

	}

	return min;

}
Graph<std::set<int>, bool> Loop::construct_induced_graph_at_level(
		std::vector<std::set<int> > s, DependenceGraph dep, int dep_dim) {
	Graph<std::set<int>, bool> g;

	for (int i = 0; i < s.size(); i++)
		g.insert(s[i]);

	for (int i = 0; i < s.size(); i++) {

		for (int j = i + 1; j < s.size(); j++) {
			bool has_true_edge_i_to_j = false;
			bool has_true_edge_j_to_i = false;
			bool is_connected_i_to_j = false;
			bool is_connected_j_to_i = false;
			for (std::set<int>::iterator ii = s[i].begin(); ii != s[i].end();
					ii++) {

				for (std::set<int>::iterator jj = s[j].begin();
						jj != s[j].end(); jj++) {

					std::vector<DependenceVector> dvs = dep.getEdge(*ii, *jj);
					for (int k = 0; k < dvs.size(); k++)
						if (dvs[k].is_control_dependence()
								|| (dvs[k].is_data_dependence()
										&& dvs[k].has_been_carried_at(dep_dim))) {

							if (dvs[k].is_data_dependence()
									&& dvs[k].has_negative_been_carried_at(
											dep_dim)) {
								//g.connect(i, j, false);
								is_connected_i_to_j = true;
								break;
							} else {
								//g.connect(i, j, true);

								has_true_edge_i_to_j = true;
								//break
							}
						}

					//if (is_connected)

					//	break;
					//		if (has_true_edge_i_to_j && !is_connected_i_to_j)
					//				g.connect(i, j, true);
					dvs = dep.getEdge(*jj, *ii);
					for (int k = 0; k < dvs.size(); k++)
						if (dvs[k].is_control_dependence()
								|| (dvs[k].is_data_dependence()
										&& dvs[k].has_been_carried_at(dep_dim))) {

							if (is_connected_i_to_j || has_true_edge_i_to_j)
								throw loop_error(
										"Graph input for fusion has cycles not a DAG!!");

							if (dvs[k].is_data_dependence()
									&& dvs[k].has_negative_been_carried_at(
											dep_dim)) {
								//g.connect(i, j, false);
								is_connected_j_to_i = true;
								break;
							} else {
								//g.connect(i, j, true);

								has_true_edge_j_to_i = true;
								//break;
							}
						}

					//	if (is_connected)
					//break;
					//	if (is_connected)
					//break;
				}

				//if (is_connected)
				//	break;
			}

			if (is_connected_i_to_j)
				g.connect(i, j, false);
			else if (has_true_edge_i_to_j)
				g.connect(i, j, true);

			if (is_connected_j_to_i)
				g.connect(j, i, false);
			else if (has_true_edge_j_to_i)
				g.connect(j, i, true);

		}
	}
	return g;
}

std::vector<std::set<int> > Loop::typed_fusion(Graph<std::set<int>, bool> g,
		std::vector<bool> &types) {

	bool roots[g.vertex.size()];

	for (int i = 0; i < g.vertex.size(); i++)
		roots[i] = true;

	for (int i = 0; i < g.vertex.size(); i++)
		for (int j = i + 1; j < g.vertex.size(); j++) {

			if (g.hasEdge(i, j))
				roots[j] = false;

			if (g.hasEdge(j, i))
				roots[i] = false;

		}

	std::list<int> work_list;
	std::list<bool> type_list;
	int cant_fuse_with[g.vertex.size()];
	int fused = 0;
	int lastfused = 0;
	int lastnum = 0;
	std::vector<std::set<int> > s;
//Each Fused set's representative node

	int node_to_fused_nodes[g.vertex.size()];
	int node_num[g.vertex.size()];
	int next[g.vertex.size()];
	for (int i = 0; i < g.vertex.size(); i++) {
		if (roots[i] == true) {
			work_list.push_back(i);
			type_list.push_back(types[i]);
		}
		cant_fuse_with[i] = 0;
		node_to_fused_nodes[i] = 0;
		node_num[i] = -1;
		next[i] = 0;
	}
// topological sort according to chun's permute algorithm
//   std::vector<std::set<int> > s = g.topoSort();
	std::vector<std::set<int> > s2 = g.topoSort();
	if (work_list.empty() || (s2.size() != g.vertex.size())) {

		std::cout << s2.size() << "\t" << g.vertex.size() << std::endl;
		throw loop_error("Input for fusion not a DAG!!");

	}
	int fused_nodes_counter = 0;
	while (!work_list.empty()) {
		int n = work_list.front();
		bool type = type_list.front();
		//int n_ = g.vertex[n].first;
		work_list.pop_front();
		type_list.pop_front();
		int node;
		/*if (cant_fuse_with[n] == 0)
		 node = 0;
		 else
		 node = cant_fuse_with[n];
		 */
		int p;
		if (type) {
			//if ((fused_nodes_counter != 0) && (node != fused_nodes_counter)) {
			if (cant_fuse_with[n] == 0)
				p = fused;
			else
				p = next[cant_fuse_with[n]];

			if (p != 0) {
				int rep_node = node_to_fused_nodes[p];
				node_num[n] = node_num[rep_node];

				try {
					update_successors(n, node_num, cant_fuse_with, g, work_list,
							type_list, types);
				} catch (const loop_error &e) {

					throw loop_error(
							"statements cannot be fused together due to negative dependence");

				}
				for (std::set<int>::iterator it = g.vertex[n].first.begin();
						it != g.vertex[n].first.end(); it++)
					s[node_num[n] - 1].insert(*it);
			} else {
				//std::set<int> new_node;
				//new_node.insert(n_);
				s.push_back(g.vertex[n].first);
				lastnum = lastnum + 1;
				node_num[n] = lastnum;
				node_to_fused_nodes[node_num[n]] = n;

				if (lastfused == 0) {
					fused = lastnum;
					lastfused = fused;
				} else {
					next[lastfused] = lastnum;
					lastfused = lastnum;

				}

				try {
					update_successors(n, node_num, cant_fuse_with, g, work_list,
							type_list, types);
				} catch (const loop_error &e) {

					throw loop_error(
							"statements cannot be fused together due to negative dependence");

				}
				fused_nodes_counter++;
			}

		} else {
			s.push_back(g.vertex[n].first);
			lastnum = lastnum + 1;
			node_num[n] = lastnum;
			node_to_fused_nodes[node_num[n]] = n;

			try {
				update_successors(n, node_num, cant_fuse_with, g, work_list,
						type_list, types);
			} catch (const loop_error &e) {

				throw loop_error(
						"statements cannot be fused together due to negative dependence");

			}
			//fused_nodes_counter++;

		}

	}

	return s;
}

void Loop::setLexicalOrder(int dim, const std::set<int> &active,
		int starting_order, std::vector<std::vector<std::string> > idxNames) {
	if (active.size() == 0)
		return;

// check for sanity of parameters
	if (dim < 0 || dim % 2 != 0)
		throw std::invalid_argument(
				"invalid constant loop level to set lexicographical order");
	std::vector<int> lex;
	int ref_stmt_num;
	for (std::set<int>::iterator i = active.begin(); i != active.end(); i++) {
		if ((*i) < 0 || (*i) >= stmt.size())
			throw std::invalid_argument(
					"invalid statement number " + to_string(*i));
		if (dim >= stmt[*i].xform.n_out())
			throw std::invalid_argument(
					"invalid constant loop level to set lexicographical order");
		if (i == active.begin()) {
			lex = getLexicalOrder(*i);
			ref_stmt_num = *i;
		} else {
			std::vector<int> lex2 = getLexicalOrder(*i);
			for (int j = 0; j < dim; j += 2)
				if (lex[j] != lex2[j])
					throw std::invalid_argument(
							"statements are not in the same sub loop nest");
		}
	}

// sepearate statements by current loop level types
	int level = (dim + 2) / 2;
	std::map<std::pair<LoopLevelType, int>, std::set<int> > active_by_level_type;
	std::set<int> active_by_no_level;
	for (std::set<int>::iterator i = active.begin(); i != active.end(); i++) {
		if (level > stmt[*i].loop_level.size())
			active_by_no_level.insert(*i);
		else
			active_by_level_type[std::make_pair(
					stmt[*i].loop_level[level - 1].type,
					stmt[*i].loop_level[level - 1].payload)].insert(*i);
	}

// further separate statements due to control dependences
	std::vector<std::set<int> > active_by_level_type_splitted;
	for (std::map<std::pair<LoopLevelType, int>, std::set<int> >::iterator i =
			active_by_level_type.begin(); i != active_by_level_type.end(); i++)
		active_by_level_type_splitted.push_back(i->second);
	for (std::set<int>::iterator i = active_by_no_level.begin();
			i != active_by_no_level.end(); i++)
		for (int j = active_by_level_type_splitted.size() - 1; j >= 0; j--) {
			std::set<int> controlled, not_controlled;
			for (std::set<int>::iterator k =
					active_by_level_type_splitted[j].begin();
					k != active_by_level_type_splitted[j].end(); k++) {
				std::vector<DependenceVector> dvs = dep.getEdge(*i, *k);
				bool is_controlled = false;
				for (int kk = 0; kk < dvs.size(); kk++)
					if (dvs[kk].type = DEP_CONTROL) {
						is_controlled = true;
						break;
					}
				if (is_controlled)
					controlled.insert(*k);
				else
					not_controlled.insert(*k);
			}
			if (controlled.size() != 0 && not_controlled.size() != 0) {
				active_by_level_type_splitted.erase(
						active_by_level_type_splitted.begin() + j);
				active_by_level_type_splitted.push_back(controlled);
				active_by_level_type_splitted.push_back(not_controlled);
			}
		}

// set lexical order separating loops with different loop types first
	if (active_by_level_type_splitted.size() + active_by_no_level.size() > 1) {
		int dep_dim = get_last_dep_dim_before(ref_stmt_num, level) + 1;

		Graph<std::set<int>, Empty> g;
		for (std::vector<std::set<int> >::iterator i =
				active_by_level_type_splitted.begin();
				i != active_by_level_type_splitted.end(); i++)
			g.insert(*i);
		for (std::set<int>::iterator i = active_by_no_level.begin();
				i != active_by_no_level.end(); i++) {
			std::set<int> t;
			t.insert(*i);
			g.insert(t);
		}
		for (int i = 0; i < g.vertex.size(); i++)
			for (int j = i + 1; j < g.vertex.size(); j++) {
				bool connected = false;
				for (std::set<int>::iterator ii = g.vertex[i].first.begin();
						ii != g.vertex[i].first.end(); ii++) {
					for (std::set<int>::iterator jj = g.vertex[j].first.begin();
							jj != g.vertex[j].first.end(); jj++) {
						std::vector<DependenceVector> dvs = dep.getEdge(*ii,
								*jj);
						for (int k = 0; k < dvs.size(); k++)
							if (dvs[k].is_control_dependence()
									|| (dvs[k].is_data_dependence()
											&& !dvs[k].has_been_carried_before(
													dep_dim))) {
								g.connect(i, j);
								connected = true;
								break;
							}
						if (connected)
							break;
					}
					if (connected)
						break;
				}
				connected = false;
				for (std::set<int>::iterator ii = g.vertex[i].first.begin();
						ii != g.vertex[i].first.end(); ii++) {
					for (std::set<int>::iterator jj = g.vertex[j].first.begin();
							jj != g.vertex[j].first.end(); jj++) {
						std::vector<DependenceVector> dvs = dep.getEdge(*jj,
								*ii);
						// find the sub loop nest specified by stmt_num and level,
						// only iteration space satisfiable statements returned.
						for (int k = 0; k < dvs.size(); k++)
							if (dvs[k].is_control_dependence()
									|| (dvs[k].is_data_dependence()
											&& !dvs[k].has_been_carried_before(
													dep_dim))) {
								g.connect(j, i);
								connected = true;
								break;
							}
						if (connected)
							break;
					}
					if (connected)
						break;
				}
			}

		std::vector<std::set<int> > s = g.topoSort();
		if (s.size() != g.vertex.size())
			throw loop_error(
					"cannot separate statements with different loop types at loop level "
							+ to_string(level));

		// assign lexical order
		int order = starting_order;
		for (int i = 0; i < s.size(); i++) {
			std::set<int> &cur_scc = g.vertex[*(s[i].begin())].first;
			int sz = cur_scc.size();
			if (sz == 1) {
				int cur_stmt = *(cur_scc.begin());
				assign_const(stmt[cur_stmt].xform, dim, order);
				for (int j = dim + 2; j < stmt[cur_stmt].xform.n_out(); j += 2)
					assign_const(stmt[cur_stmt].xform, j, 0);
				order++;
			} else {
				setLexicalOrder(dim, cur_scc, order, idxNames);
				order += sz;
			}
		}
	}
// set lexical order seperating single iteration statements and loops
	else {
		std::set<int> true_singles;
		std::set<int> nonsingles;
		std::map<coef_t, std::set<int> > fake_singles;
		std::set<int> fake_singles_;

		// sort out statements that do not require loops
		for (std::set<int>::iterator i = active.begin(); i != active.end();
				i++) {
			Relation cur_IS = getNewIS(*i);
			if (is_single_iteration(cur_IS, dim + 1)) {
				bool is_all_single = true;
				for (int j = dim + 3; j < stmt[*i].xform.n_out(); j += 2)
					if (!is_single_iteration(cur_IS, j)) {
						is_all_single = false;
						break;
					}
				if (is_all_single)
					true_singles.insert(*i);
				else {
					fake_singles_.insert(*i);
					try {
						fake_singles[get_const(cur_IS, dim + 1, Set_Var)].insert(
								*i);
					} catch (const std::exception &e) {
						fake_singles[posInfinity].insert(*i);
					}
				}
			} else
				nonsingles.insert(*i);
		}

		// split nonsingles forcibly according to negative dependences present (loop unfusible)
		int dep_dim = get_dep_dim_of(ref_stmt_num, level);

		if (dim < stmt[ref_stmt_num].xform.n_out() - 1) {

			bool dummy_level_found = false;

			std::vector<std::set<int> > s;

			s = sort_by_same_loops(active, level);
			bool further_levels_exist = false;

			if (!idxNames.empty())
				if (level <= idxNames[ref_stmt_num].size())
					if (idxNames[ref_stmt_num][level - 1].length() == 0) {
						//	&& s.size() == 1) {
						int order1 = 0;
						dummy_level_found = true;

						for (int i = level; i < idxNames[ref_stmt_num].size();
								i++)
							if (idxNames[ref_stmt_num][i].length() > 0)
								further_levels_exist = true;

					}

			//if (!dummy_level_found) {

			if (s.size() > 1) {

				std::vector<bool> types;
				for (int i = 0; i < s.size(); i++)
					types.push_back(true);

				Graph<std::set<int>, bool> g = construct_induced_graph_at_level(
						s, dep, dep_dim);
				s = typed_fusion(g, types);
			}
			int order = starting_order;
			for (int i = 0; i < s.size(); i++) {

				for (std::set<int>::iterator it = s[i].begin();
						it != s[i].end(); it++) {
					assign_const(stmt[*it].xform, dim, order);
					stmt[*it].xform.simplify();
				}
				if ((dim + 2) <= (stmt[ref_stmt_num].xform.n_out() - 1))
					setLexicalOrder(dim + 2, s[i], order, idxNames);

				order++;
			}
			//}
			/*	else {

			 int order1 = 0;
			 int order = 0;
			 for (std::set<int>::iterator i = active.begin();
			 i != active.end(); i++) {
			 if (!further_levels_exist)
			 assign_const(stmt[*i].xform, dim, order1++);
			 else
			 assign_const(stmt[*i].xform, dim, order1);

			 }

			 if ((dim + 2) <= (stmt[ref_stmt_num].xform.n_out() - 1) && further_levels_exist)
			 setLexicalOrder(dim + 2, active, order, idxNames);
			 }
			 */
		} else {
			int dummy_order = 0;
			for (std::set<int>::iterator i = active.begin(); i != active.end();
					i++) {
				assign_const(stmt[*i].xform, dim, dummy_order++);
				stmt[*i].xform.simplify();
			}
		}
		/*for (int i = 0; i < g2.vertex.size(); i++)
		 for (int j = i+1; j < g2.vertex.size(); j++) {
		 std::vector<DependenceVector> dvs = dep.getEdge(g2.vertex[i].first, g2.vertex[j].first);
		 for (int k = 0; k < dvs.size(); k++)
		 if (dvs[k].is_control_dependence() ||
		 (dvs[k].is_data_dependence() && dvs[k].has_negative_been_carried_at(dep_dim))) {
		 g2.connect(i, j);
		 break;
		 }
		 dvs = dep.getEdge(g2.vertex[j].first, g2.vertex[i].first);
		 for (int k = 0; k < dvs.size(); k++)
		 if (dvs[k].is_control_dependence() ||
		 (dvs[k].is_data_dependence() && dvs[k].has_negative_been_carried_at(dep_dim))) {
		 g2.connect(j, i);
		 break;
		 }
		 }

		 std::vector<std::set<int> > s2 = g2.packed_topoSort();

		 std::vector<std::set<int> > splitted_nonsingles;
		 for (int i = 0; i < s2.size(); i++) {
		 std::set<int> cur_scc;
		 for (std::set<int>::iterator j = s2[i].begin(); j != s2[i].end(); j++)
		 cur_scc.insert(g2.vertex[*j].first);
		 splitted_nonsingles.push_back(cur_scc);
		 }
		 */
		//convert to dependence graph for grouped statements
		//dep_dim = get_last_dep_dim_before(ref_stmt_num, level) + 1;
		/*int order = 0;
		 for (std::set<int>::iterator j = active.begin(); j != active.end();
		 j++) {
		 std::set<int> continuous;
		 std::cout<< active.size()<<std::endl;
		 while (nonsingles.find(*j) != nonsingles.end() && j != active.end()) {
		 continuous.insert(*j);
		 j++;
		 }

		 printf("continuous size is %d\n", continuous.size());


		 if (continuous.size() > 0) {
		 std::vector<std::set<int> > s = typed_fusion(continuous, dep,
		 dep_dim);

		 for (int i = 0; i < s.size(); i++) {
		 for (std::set<int>::iterator l = s[i].begin();
		 l != s[i].end(); l++) {
		 assign_const(stmt[*l].xform, dim + 2, order);
		 setLexicalOrder(dim + 2, s[i]);
		 }
		 order++;
		 }
		 }

		 if (j != active.end()) {
		 assign_const(stmt[*j].xform, dim + 2, order);

		 for (int k = dim + 4; k < stmt[*j].xform.n_out(); k += 2)
		 assign_const(stmt[*j].xform, k, 0);
		 order++;
		 }

		 if( j == active.end())
		 break;
		 }
		 */

		// assign lexical order
		/*int order = starting_order;
		 for (int i = 0; i < s.size(); i++) {
		 // translate each SCC into original statements
		 std::set<int> cur_scc;
		 for (std::set<int>::iterator j = s[i].begin(); j != s[i].end(); j++)
		 copy(s[i].begin(), s[i].end(),
		 inserter(cur_scc, cur_scc.begin()));

		 // now assign the constant
		 for (std::set<int>::iterator j = cur_scc.begin();
		 j != cur_scc.end(); j++)
		 assign_const(stmt[*j].xform, dim, order);

		 if (cur_scc.size() > 1)
		 setLexicalOrder(dim + 2, cur_scc);
		 else if (cur_scc.size() == 1) {
		 int cur_stmt = *(cur_scc.begin());
		 for (int j = dim + 2; j < stmt[cur_stmt].xform.n_out(); j += 2)
		 assign_const(stmt[cur_stmt].xform, j, 0);
		 }

		 if (cur_scc.size() > 0)
		 order++;
		 }
		 */
	}

}

/*void Loop::apply_xform_alt() {
 std::set<int> active;
 for (int i = 0; i < stmt.size(); i++)
 active.insert(i);
 apply_xform_alt(active);
 }

 void Loop::apply_xform_alt(int stmt_num) {
 std::set<int> active;
 active.insert(stmt_num);
 apply_xform_alt(active);
 }
 */
void Loop::apply_xform() {
	std::set<int> active;
	for (int i = 0; i < stmt.size(); i++)
		active.insert(i);
	apply_xform(active);
}

void Loop::apply_xform(int stmt_num) {
	std::set<int> active;
	active.insert(stmt_num);
	apply_xform(active);
}

/*void Loop::apply_xform_alt(std::set<int> &active) {
 int max_n = 0;

 CG_outputBuilder *ocg = ir->builder();
 for (std::set<int>::iterator i = active.begin(); i != active.end(); i++) {
 int n = stmt[*i].loop_level.size();
 if (n > max_n)
 max_n = n;

 std::vector<int> lex = getLexicalOrder(*i);

 Relation mapping = copy(stmt[*i].xform);

 // match omega input/output variables to variable names in the code
 for (int j = 1; j <= stmt[*i].IS.n_set(); j++)
 mapping.name_input_var(j, stmt[*i].IS.set_var(j)->name());
 for (int j = 1; j <= n; j++)
 mapping.name_output_var(j,
 tmp_loop_var_name_prefix
 + to_string(tmp_loop_var_name_counter + j - 1));
 mapping.setup_names();

 Relation known = Extend_Set(copy(this->known),
 mapping.n_out() - this->known.n_set());
 //stmt[*i].code = outputStatement(ocg, stmt[*i].code, 0, mapping, known, std::vector<CG_outputRepr *>(mapping.n_out(), NULL));

 Relation temp = Range(Restrict_Domain(mapping, stmt[*i].IS));
 temp.simplify();
 Relation new_mapping = Relation(n, 2 * n + 1);

 F_And *f_root = new_mapping.add_and();
 std::map<Variable_ID, Variable_ID> exists_mapping;

 F_Exists *f_exists = f_root->add_exists();
 F_And *f_root_ = f_exists->add_and();

 for (DNF_Iterator di(temp.query_DNF()); di; di++) {

 for (GEQ_Iterator e((*di)->GEQs()); e; e++) {
 GEQ_Handle h = f_root_->add_GEQ();
 for (Constr_Vars_Iter cvi(*e); cvi; cvi++) {
 Variable_ID v = cvi.curr_var();
 switch (v->kind()) {
 case Input_Var:
 h.update_coef(new_mapping.output_var(v->get_position()),
 cvi.curr_coef());
 break;
 case Wildcard_Var: {

 std::map<Variable_ID, Variable_ID>::iterator p =
 exists_mapping.find(v);
 Variable_ID v3;
 if (p == exists_mapping.end()) {
 v3 = f_exists->declare();
 exists_mapping[v] = v3;
 } else
 v3 = p->second;

 h.update_coef(v3, cvi.curr_coef());
 break;
 }
 case Global_Var: {
 Global_Var_ID g = v->get_global_var();
 Variable_ID v2;
 if (g->arity() == 0)
 v2 = new_mapping.get_local(g);
 else
 v2 = new_mapping.get_local(g, Input_Tuple);
 h.update_coef(v2, cvi.curr_coef());
 break;
 }
 default:
 assert(false);
 }
 }
 h.update_const((*e).get_const());
 }

 for (EQ_Iterator e((*di)->EQs()); e; e++) {
 EQ_Handle h = f_root_->add_EQ();
 for (Constr_Vars_Iter cvi(*e); cvi; cvi++) {
 Variable_ID v = cvi.curr_var();
 switch (v->kind()) {
 case Input_Var:
 h.update_coef(new_mapping.output_var(v->get_position()),
 cvi.curr_coef());
 break;
 case Wildcard_Var: {
 std::map<Variable_ID, Variable_ID>::iterator p =
 exists_mapping.find(v);
 Variable_ID v3;
 if (p == exists_mapping.end()) {
 v3 = f_exists->declare();
 exists_mapping[v] = v3;
 } else
 v3 = p->second;

 h.update_coef(v3, cvi.curr_coef());
 break;
 }
 case Global_Var: {
 Global_Var_ID g = v->get_global_var();
 Variable_ID v2;
 if (g->arity() == 0)
 v2 = new_mapping.get_local(g);
 else
 v2 = new_mapping.get_local(g, Input_Tuple);
 h.update_coef(v2, cvi.curr_coef());
 break;
 }
 default:
 assert(false);
 }
 }
 h.update_const((*e).get_const());
 }

 }

 Relation mapping2 = Inverse(copy(stmt[*i].xform));
 mapping2.simplify();
 mapping2.setup_names();
 std::vector<std::pair<CG_outputRepr *, int> > atof = std::vector<
 std::pair<CG_outputRepr *, int> >(2 * n + 1,
 std::make_pair(static_cast<CG_outputRepr *>(NULL), 0));

 for (int k = 1; k <= mapping2.n_out(); k++) {
 //Relation mapping1(mapping.n_out(), 1);
 //F_And *f_root = mapping1.add_and();
 //EQ_Handle h = f_root->add_EQ();
 //h.update_coef(mapping1.output_var(1), 1);
 //h.update_coef(mapping1.input_var(i), -1);
 //Relation r = Composition(mapping1, copy(mapping));
 //r.simplify();

 Relation r = copy(mapping2);
 Variable_ID v = r.output_var(k);

 std::pair<EQ_Handle, int> result = find_simplest_assignment(r, v,
 atof);

 if (result.second < INT_MAX) {
 EQ_Handle h = f_root->add_EQ();
 for (Constr_Vars_Iter cvi(result.first); cvi; cvi++) {
 Variable_ID v = cvi.curr_var();
 switch (v->kind()) {

 case Output_Var:
 h.update_coef(new_mapping.input_var(v->get_position()),
 cvi.curr_coef());
 break;

 case Input_Var:
 h.update_coef(new_mapping.output_var(v->get_position()),
 cvi.curr_coef());
 break;

 case Global_Var: {
 Global_Var_ID g = v->get_global_var();
 Variable_ID v2;
 if (g->arity() == 0)
 v2 = new_mapping.get_local(g);
 else
 v2 = new_mapping.get_local(g, Output_Tuple);
 h.update_coef(v2, cvi.curr_coef());
 break;
 }
 default:
 assert(false);
 }
 }
 }

 }

 new_mapping.simplify();
 stmt[*i].xform = new_mapping;
 }

 tmp_loop_var_name_counter += max_n;
 }
 */

void Loop::addKnown(const Relation &cond) {

// invalidate saved codegen computation
	delete last_compute_cgr_;
	last_compute_cgr_ = NULL;
	delete last_compute_cg_;
	last_compute_cg_ = NULL;

	int n1 = this->known.n_set();

	Relation r = copy(cond);
	int n2 = r.n_set();

	if (n1 < n2)
		this->known = Extend_Set(this->known, n2 - n1);
	else if (n1 > n2)
		r = Extend_Set(r, n1 - n2);

	this->known = Intersection(this->known, r);
}

void Loop::removeDependence(int stmt_num_from, int stmt_num_to) {
// check for sanity of parameters
	if (stmt_num_from >= stmt.size())
		throw std::invalid_argument(
				"invalid statement number " + to_string(stmt_num_from));
	if (stmt_num_to >= stmt.size())
		throw std::invalid_argument(
				"invalid statement number " + to_string(stmt_num_to));

	dep.disconnect(stmt_num_from, stmt_num_to);
}

void Loop::dump() const {
	for (int i = 0; i < stmt.size(); i++) {
		std::vector<int> lex = getLexicalOrder(i);
		std::cout << "s" << i + 1 << ": ";
		for (int j = 0; j < stmt[i].loop_level.size(); j++) {
			if (2 * j < lex.size())
				std::cout << lex[2 * j];
			switch (stmt[i].loop_level[j].type) {
			case LoopLevelOriginal:
				std::cout << "(dim:" << stmt[i].loop_level[j].payload << ")";
				break;
			case LoopLevelTile:
				std::cout << "(tile:" << stmt[i].loop_level[j].payload << ")";
				break;
			default:
				std::cout << "(unknown)";
			}
			std::cout << ' ';
		}
		for (int j = 2 * stmt[i].loop_level.size(); j < lex.size(); j += 2) {
			std::cout << lex[j];
			if (j != lex.size() - 1)
				std::cout << ' ';
		}
		std::cout << std::endl;
	}
}

bool Loop::nonsingular(const std::vector<std::vector<int> > &T) {
	if (stmt.size() == 0)
		return true;

// check for sanity of parameters
	for (int i = 0; i < stmt.size(); i++) {
		if (stmt[i].loop_level.size() != num_dep_dim)
			throw std::invalid_argument(
					"nonsingular loop transformations must be applied to original perfect loop nest");
		for (int j = 0; j < stmt[i].loop_level.size(); j++)
			if (stmt[i].loop_level[j].type != LoopLevelOriginal)
				throw std::invalid_argument(
						"nonsingular loop transformations must be applied to original perfect loop nest");
	}
	if (T.size() != num_dep_dim)
		throw std::invalid_argument("invalid transformation matrix");
	for (int i = 0; i < stmt.size(); i++)
		if (T[i].size() != num_dep_dim + 1 && T[i].size() != num_dep_dim)
			throw std::invalid_argument("invalid transformation matrix");
// invalidate saved codegen computation
	delete last_compute_cgr_;
	last_compute_cgr_ = NULL;
	delete last_compute_cg_;
	last_compute_cg_ = NULL;
// build relation from matrix
	Relation mapping(2 * num_dep_dim + 1, 2 * num_dep_dim + 1);
	F_And *f_root = mapping.add_and();
	for (int i = 0; i < num_dep_dim; i++) {
		EQ_Handle h = f_root->add_EQ();
		h.update_coef(mapping.output_var(2 * (i + 1)), -1);
		for (int j = 0; j < num_dep_dim; j++)
			if (T[i][j] != 0)
				h.update_coef(mapping.input_var(2 * (j + 1)), T[i][j]);
		if (T[i].size() == num_dep_dim + 1)
			h.update_const(T[i][num_dep_dim]);
	}
	for (int i = 1; i <= 2 * num_dep_dim + 1; i += 2) {
		EQ_Handle h = f_root->add_EQ();
		h.update_coef(mapping.output_var(i), -1);
		h.update_coef(mapping.input_var(i), 1);
	}

// update transformation relations
	for (int i = 0; i < stmt.size(); i++)
		stmt[i].xform = Composition(copy(mapping), stmt[i].xform);

// update dependence graph
	for (int i = 0; i < dep.vertex.size(); i++)
		for (DependenceGraph::EdgeList::iterator j =
				dep.vertex[i].second.begin(); j != dep.vertex[i].second.end();
				j++) {
			std::vector<DependenceVector> dvs = j->second;
			for (int k = 0; k < dvs.size(); k++) {
				DependenceVector &dv = dvs[k];
				switch (dv.type) {
				case DEP_W2R:
				case DEP_R2W:
				case DEP_W2W:
				case DEP_R2R: {
					std::vector<coef_t> lbounds(num_dep_dim), ubounds(
							num_dep_dim);
					for (int p = 0; p < num_dep_dim; p++) {
						coef_t lb = 0;
						coef_t ub = 0;
						for (int q = 0; q < num_dep_dim; q++) {
							if (T[p][q] > 0) {
								if (lb == -posInfinity
										|| dv.lbounds[q] == -posInfinity)
									lb = -posInfinity;
								else
									lb += T[p][q] * dv.lbounds[q];
								if (ub == posInfinity
										|| dv.ubounds[q] == posInfinity)
									ub = posInfinity;
								else
									ub += T[p][q] * dv.ubounds[q];
							} else if (T[p][q] < 0) {
								if (lb == -posInfinity
										|| dv.ubounds[q] == posInfinity)
									lb = -posInfinity;
								else
									lb += T[p][q] * dv.ubounds[q];
								if (ub == posInfinity
										|| dv.lbounds[q] == -posInfinity)
									ub = posInfinity;
								else
									ub += T[p][q] * dv.lbounds[q];
							}
						}
						if (T[p].size() == num_dep_dim + 1) {
							if (lb != -posInfinity)
								lb += T[p][num_dep_dim];
							if (ub != posInfinity)
								ub += T[p][num_dep_dim];
						}
						lbounds[p] = lb;
						ubounds[p] = ub;
					}
					dv.lbounds = lbounds;
					dv.ubounds = ubounds;

					break;
				}
				default:
					;
				}
			}
			j->second = dvs;
		}

// set constant loop values
	std::set<int> active;
	for (int i = 0; i < stmt.size(); i++)
		active.insert(i);
	setLexicalOrder(0, active);

	return true;
}

bool Loop::is_dependence_valid_based_on_lex_order(int i, int j,
		const DependenceVector &dv, bool before) {
	std::vector<int> lex_i = getLexicalOrder(i);
	std::vector<int> lex_j = getLexicalOrder(j);
	int last_dim;
	if (!dv.is_scalar_dependence) {
		for (last_dim = 0;
				last_dim < lex_i.size() && (lex_i[last_dim] == lex_j[last_dim]);
				last_dim++)
			;
		last_dim = last_dim / 2;
		if (last_dim == 0)
			return true;

		for (int i = 0; i < last_dim; i++) {
			if (dv.lbounds[i] > 0)
				return true;
			else if (dv.lbounds[i] < 0)
				return false;
		}
	}
	if (before)
		return true;

	return false;

}

// Manu:: reduction operation

void Loop::scalar_expand(int stmt_num, const std::vector<int> &levels,
		std::string arrName, int memory_type, int padding_alignment,
		int assign_then_accumulate, int padding_stride) {

//	std::cout << "In scalar_expand function: " << stmt_num << ", " << level
//			<< ", " << arrName << "\n";
// check for sanity of parameters
	bool found_non_constant_size_dimension = false;

	if (stmt_num < 0 || stmt_num >= stmt.size())
		throw std::invalid_argument(
				"invalid statement number " + to_string(stmt_num));
//Anand: adding check for privatized levels
//if (arrName != "RHS")
//	throw std::invalid_argument(
//			"invalid 3rd argument: only 'RHS' supported " + arrName);
	for (int i = 0; i < levels.size(); i++) {
		if (levels[i] <= 0 || levels[i] > stmt[stmt_num].loop_level.size())
			throw std::invalid_argument(
					"invalid loop level " + to_string(levels[i]));

		if (i > 0) {
			if (levels[i] < levels[i - 1])
				throw std::invalid_argument(
						"loop levels must be in ascending order");
		}
	}
//end --adding check for privatized levels

	delete last_compute_cgr_;
	last_compute_cgr_ = NULL;
	delete last_compute_cg_;
	last_compute_cg_ = NULL;
	bool scalar = false;
	std::vector<IR_ArrayRef *> access = ir->FindArrayRef(stmt[stmt_num].code);
	IR_Symbol *sym = NULL;
	if (arrName == "RHS")
		sym = access[0]->symbol();
	else {

		for (int k = 0; k < access.size(); k++) {
			if (access[k]->symbol())
				if (access[k]->symbol()->name() == arrName)
					sym = access[k]->symbol();

		}
		if (sym == NULL) {
			std::vector<IR_PointerArrayRef *> access = ir->FindPointerArrayRef(
					stmt[stmt_num].code);
			for (int k = 0; k < access.size(); k++) {
				if (access[k]->symbol())
					if (access[k]->symbol()->name() == arrName)
						sym = access[k]->symbol();

			}
		}
		if (sym == NULL) {

				scalar = true;
				std::vector<IR_ScalarRef *> access2 = ir->FindScalarRef(
						stmt[stmt_num].code);
				for (int k = 0; k < access2.size(); k++) {
					if (access2[k]->symbol()->name() == arrName)
						sym = access2[k]->symbol();

				}

			}
		}
// collect array references by name
		std::vector<int> lex = getLexicalOrder(stmt_num);
		int dim = 2 * levels[levels.size() - 1] - 1;
		std::set<int> same_loop = getStatements(lex, dim - 1);

//Anand: shifting this down
//	assign_const(stmt[newStmt_num].xform, 2*level+1, 1);

//	std::cout << " before temp array name \n ";
// create a temporary variable
		IR_Symbol *tmp_sym;

// get the loop upperbound, that would be the size of the temp array.
		omega::coef_t lb[levels.size()], ub[levels.size()], size[levels.size()];

//Anand Adding apply xform so that tiled loop bounds are reflected
		apply_xform(same_loop);

//Anand commenting out the folowing 4 lines
		/*	copy(stmt[stmt_num].IS).query_variable_bounds(
		 copy(stmt[stmt_num].IS).set_var(level), lb, ub);
		 std::cout << "Upper Bound = " << ub << "\n";
		 std::cout << "lower Bound = " << lb << "\n";
		 */
// testing testing -- Manu ////////////////////////////////////////////////
		/*
		 //	int n_dim = sym->n_dim();
		 //	std::cout << "------- n_dim ----------- " << n_dim << "\n";
		 std::pair<EQ_Handle, Variable_ID> result = find_simplest_stride(stmt[stmt_num].IS, stmt[stmt_num].IS.set_var(level));
		 omega::coef_t  index_stride;
		 if (result.second != NULL) {
		 index_stride = abs(result.first.get_coef(result.second))/gcd(abs(result.first.get_coef(result.second)), abs(result.first.get_coef(stmt[stmt_num].IS.set_var(level))));
		 std::cout << "simplest_stride :: " << index_stride << ", " << result.first.get_coef(result.second) << ", " << result.first.get_coef(stmt[stmt_num].IS.set_var(level))<< "\n";
		 }
		 Relation bound;
		 //	bound = get_loop_bound(stmt[stmt_num].IS, level);
		 bound = SimpleHull(stmt[stmt_num].IS,true, true);
		 bound.print();

		 bound = copy(stmt[stmt_num].IS);
		 for (int i = 1; i < level; i++) {
		 bound = Project(bound, i, Set_Var);
		 std::cout << "-------------------------------\n";
		 bound.print();
		 }

		 bound.simplify();
		 bound.print();
		 //	bound = get_loop_bound(bound, level);

		 copy(bound).query_variable_bounds(copy(bound).set_var(level), lb, ub);
		 std::cout << "Upper Bound = " << ub << "\n";
		 std::cout << "lower Bound = " << lb << "\n";

		 result = find_simplest_stride(bound, bound.set_var(level));
		 if (result.second != NULL)
		 index_stride = abs(result.first.get_coef(result.second))/gcd(abs(result.first.get_coef(result.second)), abs(result.first.get_coef(bound.set_var(level))));
		 else
		 index_stride = 1;
		 std::cout << "simplest_stride 11:: " << index_stride << "\n";
		 */
////////////////////////////////////////////////////////////////////////////////
///////////////////////////// copied datacopy code here /////////////////////////////////////////////
		int n_dim = levels.size();
		bool need_deferred_malloc = false;
		Relation copy_is = copy(stmt[stmt_num].IS);
// extract temporary array information
		CG_outputBuilder *ocg1 = ir->builder();
		std::vector<CG_outputRepr *> index_lb(n_dim);	// initialized to NULL
		std::vector<coef_t> index_stride(n_dim);
		std::vector<bool> is_index_eq(n_dim, false);
		std::vector<std::pair<int, CG_outputRepr *> > index_sz(0);
		Relation reduced_copy_is = copy(copy_is);
		std::vector<CG_outputRepr *> size_repr;
		std::vector<int> size_int;
		Relation xform = copy(stmt[stmt_num].xform);
		for (int i = 0; i < n_dim; i++) {

			dim = 2 * levels[i] - 1;
			//Anand: Commenting out the lines below: not required
			//		if (i != 0)
			//		reduced_copy_is = Project(reduced_copy_is, level - 1 + i, Set_Var);
			Relation bound(Relation::True(reduced_copy_is.n_set()));

			if (!reduced_copy_is.has_single_conjunct()) {
				std::vector<Relation> conjuncts;
				for (DNF_Iterator conj(reduced_copy_is.query_DNF()); conj;
						conj++) {
					Relation tmp(reduced_copy_is.n_set());
					tmp.copy_names(reduced_copy_is);
					tmp.setup_names();
					for (GEQ_Iterator g(*conj); g; g++) {
						tmp.and_with_GEQ(*g);
					}
					for (EQ_Iterator e(*conj); e; e++) {
						tmp.and_with_EQ(*e);
					}
					conjuncts.push_back(tmp);
				}

				for (int i_ = 0; i_ < conjuncts.size(); i_++) {
					Relation indiv_hull = Relation::True(
							reduced_copy_is.n_set());

					for (int j = levels[i] + 1; j <= reduced_copy_is.n_set();
							j++)
						conjuncts[i_] = Project(conjuncts[i_],
								conjuncts[i_].set_var(j));
					indiv_hull = Intersection(indiv_hull, conjuncts[i_]);
					indiv_hull.simplify(2, 4);

					Relation bound_ = get_loop_bound(indiv_hull, levels[i] - 1);
					bound = Intersection(bound, bound_);
					bound.simplify(2, 4);
				}

			} else

				bound = get_loop_bound(copy(reduced_copy_is), levels[i] - 1);

			// extract stride
			std::pair<EQ_Handle, Variable_ID> result = find_simplest_stride(
					bound, bound.set_var(levels[i]));
			if (result.second != NULL)
				index_stride[i] = abs(result.first.get_coef(result.second))
						/ gcd(abs(result.first.get_coef(result.second)),
								abs(
										result.first.get_coef(
												bound.set_var(levels[i]))));
			else
				index_stride[i] = 1;
			//	std::cout << "simplest_stride 11:: " << index_stride[i] << "\n";

			// check if this arary index requires loop
			Conjunct *c = bound.query_DNF()->single_conjunct();
			for (EQ_Iterator ei(c->EQs()); ei; ei++) {
				if ((*ei).has_wildcards())
					continue;

				int coef = (*ei).get_coef(bound.set_var(levels[i]));
				if (coef != 0) {
					int sign = 1;
					if (coef < 0) {
						coef = -coef;
						sign = -1;
					}

					CG_outputRepr *op = NULL;
					for (Constr_Vars_Iter ci(*ei); ci; ci++) {
						switch ((*ci).var->kind()) {
						case Input_Var: {
							if ((*ci).var != bound.set_var(levels[i]))
								if ((*ci).coef * sign == 1)
									op = ocg1->CreateMinus(op,
											ocg1->CreateIdent(
													(*ci).var->name()));
								else if ((*ci).coef * sign == -1)
									op = ocg1->CreatePlus(op,
											ocg1->CreateIdent(
													(*ci).var->name()));
								else if ((*ci).coef * sign > 1)
									op =
											ocg1->CreateMinus(op,
													ocg1->CreateTimes(
															ocg1->CreateInt(
																	abs(
																			(*ci).coef)),
															ocg1->CreateIdent(
																	(*ci).var->name())));
								else
									// (*ci).coef*sign < -1
									op =
											ocg1->CreatePlus(op,
													ocg1->CreateTimes(
															ocg1->CreateInt(
																	abs(
																			(*ci).coef)),
															ocg1->CreateIdent(
																	(*ci).var->name())));
							break;
						}
						case Global_Var: {
							Global_Var_ID g = (*ci).var->get_global_var();
							if ((*ci).coef * sign == 1)
								op = ocg1->CreateMinus(op,
										ocg1->CreateIdent(g->base_name()));
							else if ((*ci).coef * sign == -1)
								op = ocg1->CreatePlus(op,
										ocg1->CreateIdent(g->base_name()));
							else if ((*ci).coef * sign > 1)
								op = ocg1->CreateMinus(op,
										ocg1->CreateTimes(
												ocg1->CreateInt(
														abs((*ci).coef)),
												ocg1->CreateIdent(
														g->base_name())));
							else
								// (*ci).coef*sign < -1
								op = ocg1->CreatePlus(op,
										ocg1->CreateTimes(
												ocg1->CreateInt(
														abs((*ci).coef)),
												ocg1->CreateIdent(
														g->base_name())));
							break;
						}
						default:
							throw loop_error(
									"unsupported array index expression");
						}
					}
					if ((*ei).get_const() != 0)
						op = ocg1->CreatePlus(op,
								ocg1->CreateInt(-sign * ((*ei).get_const())));
					if (coef != 1)
						op = ocg1->CreateIntegerFloor(op,
								ocg1->CreateInt(coef));

					index_lb[i] = op;
					is_index_eq[i] = true;
					break;
				}
			}
			if (is_index_eq[i])
				continue;

			// seperate lower and upper bounds
			std::vector<GEQ_Handle> lb_list, ub_list;
			std::set<Variable_ID> excluded_floor_vars;
			excluded_floor_vars.insert(bound.set_var(levels[i]));
			for (GEQ_Iterator gi(c->GEQs()); gi; gi++) {
				int coef = (*gi).get_coef(bound.set_var(levels[i]));
				if (coef != 0 && (*gi).has_wildcards()) {
					bool clean_bound = true;
					GEQ_Handle h;
					for (Constr_Vars_Iter cvi(*gi, true); gi; gi++)
						if (!find_floor_definition(bound, (*cvi).var,
								excluded_floor_vars).first) {
							clean_bound = false;
							break;
						} else
							h = find_floor_definition(bound, (*cvi).var,
									excluded_floor_vars).second;

					if (!clean_bound)
						continue;
					else {
						if (coef > 0)
							lb_list.push_back(h);
						else if (coef < 0)
							ub_list.push_back(h);
						continue;
					}

				}

				if (coef > 0)
					lb_list.push_back(*gi);
				else if (coef < 0)
					ub_list.push_back(*gi);
			}
			if (lb_list.size() == 0 || ub_list.size() == 0)
				throw loop_error("failed to calcuate array footprint size");

			// build lower bound representation
			std::vector<CG_outputRepr *> lb_repr_list;
			/*     for (int j = 0; j < lb_list.size(); j++){
			 if(this->known.n_set() == 0)
			 lb_repr_list.push_back(output_lower_bound_repr(ocg1, lb_list[j], bound.set_var(level-1+i+1), result.first, result.second, bound, Relation::True(bound.n_set()), std::vector<std::pair<CG_outputRepr *, int> >(bound.n_set(), std::make_pair(static_cast<CG_outputRepr *>(NULL), 0))));
			 else
			 lb_repr_list.push_back(output_lower_bound_repr(ocg1, lb_list[j], bound.set_var(level-1+i+1), result.first, result.second, bound, this->known, std::vector<std::pair<CG_outputRepr *, int> >(bound.n_set(), std::make_pair(static_cast<CG_outputRepr *>(NULL), 0))));

			 }
			 */
			if (lb_repr_list.size() > 1)
				index_lb[i] = ocg1->CreateInvoke("max", lb_repr_list);
			else if (lb_repr_list.size() == 1)
				index_lb[i] = lb_repr_list[0];

			// build temporary array size representation
			{
				Relation cal(copy_is.n_set(), 1);
				std::vector<Relation> cal_output;
				F_And *f_root = cal.add_and();
				for (int j = 0; j < ub_list.size(); j++)
					for (int k = 0; k < lb_list.size(); k++) {
						GEQ_Handle h = f_root->add_GEQ();

						for (Constr_Vars_Iter ci(ub_list[j]); ci; ci++) {
							switch ((*ci).var->kind()) {
							case Input_Var: {
								int pos = (*ci).var->get_position();
								h.update_coef(cal.input_var(pos), (*ci).coef);
								break;
							}
							case Global_Var: {
								Global_Var_ID g = (*ci).var->get_global_var();
								Variable_ID v;
								if (g->arity() == 0)
									v = cal.get_local(g);
								else
									v = cal.get_local(g,
											(*ci).var->function_of());
								h.update_coef(v, (*ci).coef);
								break;
							}
							default:
								throw loop_error(
										"cannot calculate temporay array size statically");
							}
						}
						h.update_const(ub_list[j].get_const());

						for (Constr_Vars_Iter ci(lb_list[k]); ci; ci++) {
							switch ((*ci).var->kind()) {
							case Input_Var: {
								int pos = (*ci).var->get_position();
								h.update_coef(cal.input_var(pos), (*ci).coef);
								break;
							}
							case Global_Var: {
								Global_Var_ID g = (*ci).var->get_global_var();
								Variable_ID v;
								if (g->arity() == 0)
									v = cal.get_local(g);
								else
									v = cal.get_local(g,
											(*ci).var->function_of());
								h.update_coef(v, (*ci).coef);
								break;
							}
							default:
								throw loop_error(
										"cannot calculate temporay array size statically");
							}
						}
						h.update_const(lb_list[k].get_const());

						h.update_const(1);
						h.update_coef(cal.output_var(1), -1);
					}

				bool is_index_bound_const = false;
				if (copy_is.has_single_conjunct()) {
					cal = Restrict_Domain(cal, copy(copy_is));
					for (int j = 1; j <= cal.n_inp(); j++) {
						cal = Project(cal, j, Input_Var);
					}
					cal.simplify();

					// pad temporary array size
					// TODO: for variable array size, create padding formula
					//int padding_stride = 0;
					Conjunct *c = cal.query_DNF()->single_conjunct();
					if (padding_stride != 0 && i == n_dim - 1) {
						//size = (size + index_stride[i] - 1) / index_stride[i];
						size_repr.push_back(ocg1->CreateInt(padding_stride));
					} else {
						for (GEQ_Iterator gi(c->GEQs());
								gi && !is_index_bound_const; gi++)
							if ((*gi).is_const(cal.output_var(1))) {
								coef_t size = (*gi).get_const()
										/ (-(*gi).get_coef(cal.output_var(1)));

								if (padding_alignment > 1 && i == n_dim - 1) { // align to boundary for data packing
									int residue = size % padding_alignment;
									if (residue)
										size = size + padding_alignment
												- residue;
								}

								index_sz.push_back(
										std::make_pair(i,
												ocg1->CreateInt(size)));
								is_index_bound_const = true;
								size_int.push_back(size);
								size_repr.push_back(ocg1->CreateInt(size));

								//	std::cout << "============================== size :: "
								//			<< size << "\n";

							}

						if (!is_index_bound_const) {

							found_non_constant_size_dimension = true;
							Conjunct *c = bound.query_DNF()->single_conjunct();


							for (GEQ_Iterator gi(c->GEQs());
															gi; gi++)
								 for (Constr_Vars_Iter cvi(*gi); cvi; cvi++)
								 if (cvi.curr_var()->kind() == Global_Var) {

									 Global_Var_ID g =
																	 (*cvi).var->get_global_var();

									 if(g->arity() > 0 )
										 need_deferred_malloc = true;

								 }



							for (GEQ_Iterator gi(c->GEQs());
									gi && !is_index_bound_const; gi++) {
								int coef = (*gi).get_coef(
										bound.set_var(levels[i]));
								if (coef < 0) {

									size_repr.push_back(
											ocg1->CreatePlus(
													output_upper_bound_repr(
															ocg1, *gi,
															bound.set_var(
																	levels[i]),
															bound,
															std::vector<
																	std::pair<
																			CG_outputRepr *,
																			int> >(
																	bound.n_set(),
																	std::make_pair(
																			static_cast<CG_outputRepr *>(NULL),
																			0)),
															uninterpreted_symbols[stmt_num]),
													ocg1->CreateInt(1)));

									/*CG_outputRepr *op = NULL;
									 for (Constr_Vars_Iter ci(*gi); ci; ci++) {
									 if ((*ci).var != cal.output_var(1)) {
									 switch ((*ci).var->kind()) {
									 case Global_Var: {
									 Global_Var_ID g =
									 (*ci).var->get_global_var();
									 if ((*ci).coef == 1)
									 op = ocg1->CreatePlus(op,
									 ocg1->CreateIdent(
									 g->base_name()));
									 else if ((*ci).coef == -1)
									 op = ocg1->CreateMinus(op,
									 ocg1->CreateIdent(
									 g->base_name()));
									 else if ((*ci).coef > 1)
									 op =
									 ocg1->CreatePlus(op,
									 ocg1->CreateTimes(
									 ocg1->CreateInt(
									 (*ci).coef),
									 ocg1->CreateIdent(
									 g->base_name())));
									 else
									 // (*ci).coef < -1
									 op =
									 ocg1->CreateMinus(op,
									 ocg1->CreateTimes(
									 ocg1->CreateInt(
									 -(*ci).coef),
									 ocg1->CreateIdent(
									 g->base_name())));
									 break;
									 }
									 default:
									 throw loop_error(
									 "failed to generate array index bound code");
									 }
									 }
									 }
									 int c = (*gi).get_const();
									 if (c > 0)
									 op = ocg1->CreatePlus(op, ocg1->CreateInt(c));
									 else if (c < 0)
									 op = ocg1->CreateMinus(op, ocg1->CreateInt(-c));
									 */
									/*            if (padding_stride != 0) {
									 if (i == fastest_changing_dimension) {
									 coef_t g = gcd(index_stride[i], static_cast<coef_t>(padding_stride));
									 coef_t t1 = index_stride[i] / g;
									 if (t1 != 1)
									 op = ocg->CreateIntegerFloor(ocg->CreatePlus(op, ocg->CreateInt(t1-1)), ocg->CreateInt(t1));
									 coef_t t2 = padding_stride / g;
									 if (t2 != 1)
									 op = ocg->CreateTimes(op, ocg->CreateInt(t2));
									 }
									 else if (index_stride[i] != 1) {
									 op = ocg->CreateIntegerFloor(ocg->CreatePlus(op, ocg->CreateInt(index_stride[i]-1)), ocg->CreateInt(index_stride[i]));
									 }
									 }
									 */
									//index_sz.push_back(std::make_pair(i, op));
									//break;
								}
							}
						}
					}

				} else {
					for (DNF_Iterator conj(copy_is.query_DNF()); conj; conj++) {
						Relation tmp(copy_is.n_set());
						tmp.copy_names(copy_is);
						tmp.setup_names();
						for (GEQ_Iterator g(*conj); g; g++) {
							tmp.and_with_GEQ(*g);
						}
						for (EQ_Iterator e(*conj); e; e++) {
							tmp.and_with_EQ(*e);
						}

						Relation cal2 = Restrict_Domain(cal, copy(tmp));
						for (int j = 1; j <= cal2.n_inp(); j++) {
							cal2 = Project(cal2, j, Input_Var);
						}
						cal2.simplify();

						cal_output.push_back(cal2);
					}
					if (padding_stride != 0 && i == n_dim - 1) {
						//size = (size + index_stride[i] - 1) / index_stride[i];
						size_repr.push_back(ocg1->CreateInt(padding_stride));
					} else {

						for (int k = 0; k < cal_output.size(); k++) {
							for (DNF_Iterator d(cal_output[k].query_DNF()); d;
									d++)
								for (GEQ_Iterator gi((*d)->GEQs());
										gi && !is_index_bound_const; gi++)
									if ((*gi).is_const(
											cal_output[k].output_var(1))) {

										coef_t size = -1;
										size =
												max(size,
														(*gi).get_const()
																/ (-(*gi).get_coef(
																		cal_output[k].output_var(
																				1))));

										if (padding_alignment > 1
												&& i == n_dim - 1) { // align to boundary for data packing
											int residue = size
													% padding_alignment;
											if (residue)
												size = size + padding_alignment
														- residue;
										}

										index_sz.push_back(
												std::make_pair(i,
														ocg1->CreateInt(size)));
										is_index_bound_const = true;
										size_int.push_back(size);
										size_repr.push_back(
												ocg1->CreateInt(size));

										//	std::cout << "============================== size :: "
										//			<< size << "\n";

									}
						}

						if (!is_index_bound_const) {

							found_non_constant_size_dimension = true;
							Conjunct *c = bound.query_DNF()->single_conjunct();
							for (GEQ_Iterator gi(c->GEQs());
									gi && !is_index_bound_const; gi++) {
								int coef = (*gi).get_coef(
										bound.set_var(levels[i]));
								if (coef < 0) {

									size_repr.push_back(
											ocg1->CreatePlus(
													output_upper_bound_repr(
															ocg1, *gi,
															bound.set_var(
																	levels[i]),
															bound,
															std::vector<
																	std::pair<
																			CG_outputRepr *,
																			int> >(
																	bound.n_set(),
																	std::make_pair(
																			static_cast<CG_outputRepr *>(NULL),
																			0)),
															uninterpreted_symbols[stmt_num]),
													ocg1->CreateInt(1)));

								}
							}
						}
					}

				}

			}
			//size[i] = ub[i];

		}
/////////////////////////////////////////////////////////////////////////////////////////////////////
//

//Anand: Creating IS of new statement

//for(int l = dim; l < stmt[stmt_num].xform.n_out(); l+=2)
		int newStmt_num = stmt.size();
		if (!scalar) {
			shiftLexicalOrder(lex, dim + 1, 1);
			Statement s = stmt[stmt_num];
			s.ir_stmt_node = NULL;

			stmt.push_back(s);
			std::map<std::string, std::vector<CG_outputRepr *> > new_unin;
			for (std::map<std::string, std::vector<CG_outputRepr *> >::iterator i =
					uninterpreted_symbols[stmt_num].begin();
					i != uninterpreted_symbols[stmt_num].end(); i++) {
				std::string name = i->first;
				std::vector<CG_outputRepr *> reprs;
				for (int j = 0; j < i->second.size(); j++)
					reprs.push_back(i->second[j]->clone());

				new_unin.insert(
						std::pair<std::string, std::vector<CG_outputRepr *> >(
								name, reprs));
			}
			uninterpreted_symbols.push_back(new_unin);
			uninterpreted_symbols_stringrepr.push_back(
					uninterpreted_symbols_stringrepr[stmt_num]);
			unin_rel.push_back(unin_rel[stmt_num]);
			stmt[newStmt_num].code = stmt[stmt_num].code->clone();
			stmt[newStmt_num].IS = copy(stmt[stmt_num].IS);
			stmt[newStmt_num].xform = xform;
			stmt[newStmt_num].reduction = stmt[stmt_num].reduction;
			stmt[newStmt_num].reductionOp = stmt[stmt_num].reductionOp;
//assign_const(stmt[newStmt_num].xform, stmt[stmt_num].xform.n_out(), 1);//Anand: change from 2*level + 1 to stmt[stmt_num].xform.size()
//Anand-End creating IS of new statement
		}
		CG_outputRepr * tmpArrSz;
		CG_outputBuilder *ocg = ir->builder();

//for(int k =0; k < levels.size(); k++ )
//    size_repr.push_back(ocg->CreateInt(size[k]));//Anand: copying apply_xform functionality to prevent IS modification
//due to side effects with uninterpreted function symbols and failures in omega

//int n = stmt[stmt_num].loop_level.size();

		/*Relation mapping(2 * n + 1, n);
		 F_And *f_root = mapping.add_and();
		 for (int j = 1; j <= n; j++) {
		 EQ_Handle h = f_root->add_EQ();
		 h.update_coef(mapping.output_var(j), 1);
		 h.update_coef(mapping.input_var(2 * j), -1);
		 }
		 mapping = Composition(mapping, copy(stmt[stmt_num].xform));
		 mapping.simplify();

		 // match omega input/output variables to variable names in the code
		 for (int j = 1; j <= stmt[stmt_num].IS.n_set(); j++)
		 mapping.name_input_var(j, stmt[stmt_num].IS.set_var(j)->name());
		 for (int j = 1; j <= n; j++)
		 mapping.name_output_var(j,
		 tmp_loop_var_name_prefix
		 + to_string(tmp_loop_var_name_counter + j - 1));
		 mapping.setup_names();

		 Relation size_ = Range(Restrict_Domain(mapping, copy(stmt[stmt_num].IS)));
		 size_.simplify();
		 */

//Anand -commenting out tmp sym creation as symbol may have more than one dimension
//tmp_sym = ir->CreateArraySymbol(tmpArrSz, sym);
		std::vector<CG_outputRepr *> lhs_index;
		CG_outputRepr *arr_ref_repr;
		arr_ref_repr = ocg->CreateIdent(
				stmt[stmt_num].IS.set_var(levels[levels.size() - 1])->name());

		CG_outputRepr *total_size = size_repr[0];
		for (int i = 1; i < size_repr.size(); i++) {
			total_size = ocg->CreateTimes(total_size->clone(),
					size_repr[i]->clone());

		}
		for (int k = levels.size() - 2; k >= 0; k--) {
			CG_outputRepr *temp_repr = ocg->CreateIdent(
					stmt[stmt_num].IS.set_var(levels[k])->name());
			for (int l = k + 1; l < levels.size(); l++)
				temp_repr = ocg->CreateTimes(temp_repr->clone(),
						size_repr[l]->clone());

			arr_ref_repr = ocg->CreatePlus(arr_ref_repr->clone(),
					temp_repr->clone());
		}

		std::vector<CG_outputRepr *> to_push;
		to_push.push_back(total_size);
		if (!found_non_constant_size_dimension)
			tmp_sym = ir->CreateArraySymbol(sym, to_push, memory_type);
		else {
			tmp_sym = ir->CreatePointerSymbol(sym, to_push);

			static_cast<IR_PointerSymbol *>(tmp_sym)->set_size(0, total_size->clone());

			if(!need_deferred_malloc)
			init_code =
					ocg->StmtListAppend(init_code,
							ir->CreateMalloc(
									static_cast<IR_PointerSymbol *>(tmp_sym)->elem_type(),
			 						tmp_sym->name(), total_size->clone()));
			else{
			ptr_variables_non_const.push_back(static_cast<IR_PointerSymbol *>(tmp_sym));

			}
			ptr_variables.push_back(static_cast<IR_PointerSymbol *>(tmp_sym));
		}
//	std::cout << " temp array name == " << tmp_sym->name().c_str() << "\n";

// get loop index variable at the given "level"
// Relation R = Range(Restrict_Domain(copy(stmt[stmt_num].xform), copy(stmt[stmt_num].IS)));
//	stmt[stmt_num].IS.print();
//stmt[stmt_num].IS.
//	std::cout << stmt[stmt_num].IS.n_set() << std::endl;
//	std::string v = stmt[stmt_num].IS.set_var(level)->name();
//	std::cout << "loop index variable is '" << v.c_str() << "'\n";

// create a reference for the temporary array

		IR_ArrayRef *tmp_array_ref;
		IR_PointerArrayRef * tmp_ptr_array_ref;
		std::vector<CG_outputRepr *> to_push2;
		to_push2.push_back(arr_ref_repr);
//lhs_index[0] = ocg->CreateIdent(v);
		if (!found_non_constant_size_dimension)
			tmp_array_ref = ir->CreateArrayRef(
					static_cast<IR_ArraySymbol *>(tmp_sym), to_push2);
		else
			tmp_ptr_array_ref = ir->CreatePointerArrayRef(
					static_cast<IR_PointerSymbol *>(tmp_sym), to_push2);
//std::string stemp;
//stemp = tmp_array_ref->name();
//std::cout << "Created array reference --> " << stemp.c_str() << "\n";

// get the RHS expression
		CG_outputRepr *rhs;
		if (arrName == "RHS") {
			rhs = ir->GetRHSExpression(stmt[stmt_num].code);

			std::vector<IR_ArrayRef *> symbols = ir->FindArrayRef(rhs);
		}
		std::set<std::string> sym_names;

//for (int i = 0; i < symbols.size(); i++)
//	sym_names.insert(symbols[i]->symbol()->name());

		if (arrName == "RHS") {

			std::vector<IR_ArrayRef *> symbols = ir->FindArrayRef(rhs);

			for (int i = 0; i < symbols.size(); i++)
				sym_names.insert(symbols[i]->symbol()->name());
		} else {
			std::vector<IR_ArrayRef *> refs = ir->FindArrayRef(
					stmt[stmt_num].code);
			bool found = false;
			for (int j = 0; j < refs.size(); j++) {
				CG_outputRepr* to_replace;
				// build new assignment statement with temporary array
				if (!found_non_constant_size_dimension) {

					to_replace = tmp_array_ref->convert();

				} else {
					to_replace = tmp_ptr_array_ref->convert();

				}
				if (refs[j]->name() == arrName) {

					sym_names.insert(refs[j]->symbol()->name());

					if (!found) {
						IR_Control * stmts = ir->GetCode(stmt[stmt_num].code);
						CG_outputRepr *cond = NULL;
						if (stmts) {
							if (stmts->type() == IR_CONTROL_IF)
								cond = static_cast<IR_If *>(stmts)->condition();

						}

						if (!found_non_constant_size_dimension) {
							if (!cond)
								stmt[newStmt_num].code =
										ir->builder()->CreateAssignment(0,
												tmp_array_ref->convert(),
												refs[j]->convert()->clone());
							else
								stmt[newStmt_num].code = ocg->CreateIf(0,
										cond->clone(),
										ir->builder()->CreateAssignment(0,
												tmp_array_ref->convert(),
												refs[j]->convert()->clone()),
										NULL);
						} else {
							if (!cond)
								stmt[newStmt_num].code =
										ir->builder()->CreateAssignment(0,
												tmp_ptr_array_ref->convert(),
												refs[j]->convert()->clone());
							else
								stmt[newStmt_num].code = ocg->CreateIf(0,
										cond->clone(),
										ir->builder()->CreateAssignment(0,
												tmp_ptr_array_ref->convert(),
												refs[j]->convert()->clone()),
										NULL);

						}
						found = true;

					}

					ir->ReplaceExpression(refs[j], to_replace);
				}

			}

			if (!found) {

				CG_outputRepr* to_replace;
				// build new assignment statement with temporary array
				if (!found_non_constant_size_dimension) {

					to_replace = tmp_array_ref->convert();

				} else {
					to_replace = tmp_ptr_array_ref->convert();

				}
				for (std::set<int>::iterator s = same_loop.begin();
						s != same_loop.end(); s++) {
					std::vector<IR_ScalarRef *> refs2 = ir->FindScalarRef(
							stmt[*s].code);

					for (int j = 0; j < refs2.size(); j++) {

						if (refs2[j]->name() == arrName) {

							sym_names.insert(refs2[j]->symbol()->name());

							ir->ReplaceExpression(refs2[j],
									to_replace->clone());
						}

					}
				}
			}
		}
//ToDo need to update the dependence graph
//Anand adding dependence graph update
		dep.insert();

//Anand:Copying Dependence checks from datacopy code, might need to be a separate function/module
// in the future

		/*for (int i = 0; i < newStmt_num; i++) {
		 std::vector<std::vector<DependenceVector> > D;

		 for (DependenceGraph::EdgeList::iterator j =
		 dep.vertex[i].second.begin(); j != dep.vertex[i].second.end();
		 ) {
		 if (same_loop.find(i) != same_loop.end()
		 && same_loop.find(j->first) == same_loop.end()) {
		 std::vector<DependenceVector> dvs1, dvs2;
		 for (int k = 0; k < j->second.size(); k++) {
		 DependenceVector dv = j->second[k];
		 if (dv.sym != NULL
		 && sym_names.find(dv.sym->name()) != sym_names.end()
		 && (dv.type == DEP_R2R || dv.type == DEP_R2W))
		 dvs1.push_back(dv);
		 else
		 dvs2.push_back(dv);
		 }
		 j->second = dvs2;
		 if (dvs1.size() > 0)
		 dep.connect(newStmt_num, j->first, dvs1);
		 } else if (same_loop.find(i) == same_loop.end()
		 && same_loop.find(j->first) != same_loop.end()) {
		 std::vector<DependenceVector> dvs1, dvs2;
		 for (int k = 0; k < j->second.size(); k++) {
		 DependenceVector dv = j->second[k];
		 if (dv.sym != NULL
		 && sym_names.find(dv.sym->name()) != sym_names.end()
		 && (dv.type == DEP_R2R || dv.type == DEP_W2R))
		 dvs1.push_back(dv);
		 else
		 dvs2.push_back(dv);
		 }
		 j->second = dvs2;
		 if (dvs1.size() > 0)
		 D.push_back(dvs1);
		 }

		 if (j->second.size() == 0)
		 dep.vertex[i].second.erase(j++);
		 else
		 j++;
		 }

		 for (int j = 0; j < D.size(); j++)
		 dep.connect(i, newStmt_num, D[j]);
		 }
		 */
//Anand--end dependence check
		if (arrName == "RHS") {

			// build new assignment statement with temporary array
			if (!found_non_constant_size_dimension) {
				if (assign_then_accumulate) {

					IR_Control * stmts = ir->GetCode(stmt[stmt_num].code);
					CG_outputRepr *cond = NULL;
					if (stmts) {
						if (stmts->type() == IR_CONTROL_IF)
							cond = static_cast<IR_If *>(stmts)->condition();

					}
					if (!cond)
						stmt[newStmt_num].code =
								ir->builder()->CreateAssignment(0,
										tmp_array_ref->convert(), rhs);
					else
						stmt[newStmt_num].code = ocg->CreateIf(0, cond->clone(),
								ir->builder()->CreateAssignment(0,
										tmp_array_ref->convert(), rhs), NULL);

					ir->ReplaceRHSExpression(stmt[stmt_num].code,
							tmp_array_ref);
				} else {
					IR_Control * stmts = ir->GetCode(stmt[stmt_num].code);
					CG_outputRepr *cond = NULL;
					if (stmts) {
						if (stmts->type() == IR_CONTROL_IF)
							cond = static_cast<IR_If *>(stmts)->condition();

					}

					CG_outputRepr *temp = tmp_array_ref->convert()->clone();
					if (ir->QueryExpOperation(stmt[stmt_num].code)
							!= IR_OP_PLUS_ASSIGNMENT)
						throw ir_error(
								"Statement is not a += accumulation statement");

					if (!cond)
						stmt[newStmt_num].code =
								ir->builder()->CreatePlusAssignment(0,
										temp->clone(), rhs);
					else
						stmt[newStmt_num].code = ocg->CreateIf(0, cond->clone(),
								ir->builder()->CreateAssignment(0,
										temp->clone(), rhs), NULL);

					CG_outputRepr * lhs = ir->GetLHSExpression(
							stmt[stmt_num].code);

					CG_outputRepr *assignment = ir->builder()->CreateAssignment(
							0, lhs, temp->clone());
					Statement init_ = stmt[newStmt_num];
					init_.ir_stmt_node = NULL;

					init_.code = stmt[newStmt_num].code->clone();
					init_.IS = copy(stmt[newStmt_num].IS);
					init_.xform = copy(stmt[newStmt_num].xform);

					Relation mapping(init_.IS.n_set(), init_.IS.n_set());

					F_And *f_root = mapping.add_and();

					for (int i = 1; i <= mapping.n_inp(); i++) {
						EQ_Handle h = f_root->add_EQ();
						//if (i < levels[0]) {
						if (i <= levels[levels.size() - 1]) {
							h.update_coef(mapping.input_var(i), 1);
							h.update_coef(mapping.output_var(i), -1);
						} else {
							h.update_const(-1);
							h.update_coef(mapping.output_var(i), 1);
						}

						/*else {
						 int j;
						 for (j = 0; j < levels.size(); j++)
						 if (i == levels[j])
						 break;

						 if (j == levels.size()) {

						 h.update_coef(mapping.output_var(i), 1);
						 h.update_const(-1);

						 } else {


						 h.update_coef(mapping.input_var(i), 1);
						 h.update_coef(mapping.output_var(i), -1);


						 }
						 */
						//}
					}

					mapping.simplify();
					// match omega input/output variables to variable names in the code
					for (int j = 1; j <= init_.IS.n_set(); j++)
						mapping.name_output_var(j, init_.IS.set_var(j)->name());
					for (int j = 1; j <= init_.IS.n_set(); j++)
						mapping.name_input_var(j, init_.IS.set_var(j)->name());

					mapping.setup_names();

					init_.IS = omega::Range(
							omega::Restrict_Domain(mapping, init_.IS));
					std::vector<int> lex = getLexicalOrder(newStmt_num);
					int dim = 2 * levels[0] - 1;
					//init_.IS.print();
					//	init_.xform.print();
					//stmt[newStmt_num].xform.print();
					//	shiftLexicalOrder(lex, dim + 1, 1);
					shiftLexicalOrder(lex, dim + 1, 1);
					init_.reduction = stmt[newStmt_num].reduction;
					init_.reductionOp = stmt[newStmt_num].reductionOp;

					init_.code = ir->builder()->CreateAssignment(0,
							temp->clone(), ir->builder()->CreateInt(0));
					stmt.push_back(init_);
					std::map<std::string, std::vector<CG_outputRepr *> > new_unin;
					for (std::map<std::string, std::vector<CG_outputRepr *> >::iterator i =
							uninterpreted_symbols[stmt_num].begin();
							i != uninterpreted_symbols[stmt_num].end(); i++) {
						std::string name = i->first;
						std::vector<CG_outputRepr *> reprs;
						for (int j = 0; j < i->second.size(); j++)
							reprs.push_back(i->second[j]->clone());

						new_unin.insert(
								std::pair<std::string,
										std::vector<CG_outputRepr *> >(name,
										reprs));
					}

					uninterpreted_symbols.push_back(new_unin);
					uninterpreted_symbols_stringrepr.push_back(
							uninterpreted_symbols_stringrepr[newStmt_num]);
					unin_rel.push_back(unin_rel[newStmt_num]);
					stmt[stmt_num].code = assignment;
				}
			} else {
				if (assign_then_accumulate) {
					stmt[newStmt_num].code = ir->builder()->CreateAssignment(0,
							tmp_ptr_array_ref->convert(), rhs);
					ir->ReplaceRHSExpression(stmt[stmt_num].code,
							tmp_ptr_array_ref);
				} else {
					CG_outputRepr *temp = tmp_ptr_array_ref->convert()->clone();
					if (ir->QueryExpOperation(stmt[stmt_num].code)
							!= IR_OP_PLUS_ASSIGNMENT)
						throw ir_error(
								"Statement is not a += accumulation statement");
					stmt[newStmt_num].code =
							ir->builder()->CreatePlusAssignment(0,
									temp->clone(), rhs);

					CG_outputRepr * lhs = ir->GetLHSExpression(
							stmt[stmt_num].code);

					CG_outputRepr *assignment = ir->builder()->CreateAssignment(
							0, lhs, temp->clone());

					stmt[stmt_num].code = assignment;
				}
				// call function to replace rhs with temporary array
			}
		}

		//std::cout << "End of scalar_expand function!! \n";

//  if(arrName == "RHS"){
		DependenceVector dv;
		std::vector<DependenceVector> E;
		dv.lbounds = std::vector<omega::coef_t>(4);
		dv.ubounds = std::vector<omega::coef_t>(4);
		dv.type = DEP_W2R;

		for (int k = 0; k < 4; k++) {
			dv.lbounds[k] = 0;
			dv.ubounds[k] = 0;

		}

//std::vector<IR_ArrayRef*> array_refs = ir->FindArrayRef(stmt[newStmt_num].code);
		dv.sym = tmp_sym->clone();

		E.push_back(dv);

		dep.connect(newStmt_num, stmt_num, E);
// }
		//apply_xform();

	}

