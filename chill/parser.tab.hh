/* A Bison parser, made by GNU Bison 2.5.  */

/* Bison interface for Yacc-like parsers in C
   
      Copyright (C) 1984, 1989-1990, 2000-2011 Free Software Foundation, Inc.
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.
   
   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */


/* Tokens.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
   /* Put the tokens into the symbol table, so that GDB and other debuggers
      know about them.  */
   enum yytokentype {
     NUMBER = 258,
     LEVEL = 259,
     TRUEORFALSE = 260,
     FILENAME = 261,
     PROCEDURENAME = 262,
     VARIABLE = 263,
     FREEVAR = 264,
     SOURCE = 265,
     PROCEDURE = 266,
     FORMAT = 267,
     LOOP = 268,
     PERMUTE = 269,
     ORIGINAL = 270,
     TILE = 271,
     UNROLL = 272,
     SPLIT = 273,
     UNROLL_EXTRA = 274,
     REDUCE = 275,
     SPLIT_WITH_ALIGNMENT = 276,
     DATACOPY = 277,
     DATACOPY_PRIVATIZED = 278,
     FLATTEN = 279,
     SCALAR_EXPAND = 280,
     NORMALIZE = 281,
     ELLIFY = 282,
     COMPACT = 283,
     MAKE_DENSE = 284,
     SET_ARRAY_SIZE = 285,
     SPARSE_PARTITION = 286,
     SPARSE_WAVEFRONT = 287,
     REORDER_BY_INSPECTOR = 288,
     NONSINGULAR = 289,
     EXIT = 290,
     KNOWN = 291,
     SKEW = 292,
     SHIFT = 293,
     SHIFT_TO = 294,
     FUSE = 295,
     DISTRIBUTE = 296,
     REMOVE_DEP = 297,
     SCALE = 298,
     REVERSE = 299,
     PEEL = 300,
     REORDER_DATA = 301,
     STRIDED = 302,
     COUNTED = 303,
     NUM_STATEMENT = 304,
     CEIL = 305,
     FLOOR = 306,
     PRINT = 307,
     PRINT_CODE = 308,
     PRINT_DEP = 309,
     PRINT_IS = 310,
     PRINT_STRUCTURE = 311,
     NE = 312,
     LE = 313,
     GE = 314,
     EQ = 315,
     MAP_TO_OPENMP_REGION = 316,
     MARK_OMP_PARL_REGION = 317,
     MARK_OMP_THRDS = 318,
     MARK_OMP_SYNC = 319,
     GEN_OMP_PARL_REGION = 320,
     OMP_PAR_FOR = 321,
     MARK_PRAGMA = 322,
     UMINUS = 323
   };
#endif



#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
typedef union YYSTYPE
{

/* Line 2068 of yacc.c  */
#line 64 "parser.yy"

  int val;
  float fval;
  bool bool_val;
  char *name;
  std::vector<int> *vec;
  std::vector<std::string> *string_vec;
  std::vector<std::vector<int> > *mat;
  std::map<std::string, int> *tab;
  std::vector<std::map<std::string, int> > *tab_lst;
  std::pair<std::vector<std::map<std::string, int> >, std::map<std::string, int> > *eq_term_pair;



/* Line 2068 of yacc.c  */
#line 133 "parser.tab.hh"
} YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define yystype YYSTYPE /* obsolescent; will be withdrawn */
# define YYSTYPE_IS_DECLARED 1
#endif

extern YYSTYPE zzlval;


