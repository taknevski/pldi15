/*****************************************************************************
 Copyright (C) 2009 University of Utah
 All Rights Reserved.

 Purpose:
 Cudaize methods

 Notes:
 
 History:
 1/7/10 Created by Gabe Rudy by migrating code from loop.cc
 31/1/11 Modified by Protonu Basu
 *****************************************************************************/
#define TRANSFORMATION_FILE_INFO Sg_File_Info::generateDefaultFileInfoForTransformationNode()
#include <code_gen/CG_stringBuilder.h>
#include <codegen.h>
#include <code_gen/CG_utils.h>
#include <code_gen/CG_outputRepr.h>
#include <code_gen/CG_roseRepr.h>
#include "loop_cuda_rose.hh"
#include "loop.hh"
#include <math.h>
#include "omegatools.hh"
#include "ir_cudarose.hh"
#include "ir_rose.hh"
#include "ir_rose_utils.hh"
#include "chill_error.hh"
#include <vector>
#include "Outliner.hh"
//#define DEBUG
using namespace omega;
using namespace SageBuilder;
using namespace SageInterface;
//using namespace Outliner;
//using namespace ASTtools;
char *k_cuda_texture_memory; //protonu--added to track texture memory type
//extern char *omega::k_cuda_texture_memory; //protonu--added to track texture memory type
extern char *omega::k_ocg_comment;

static int cudaDebug;
class CudaStaticInit {
public:
	CudaStaticInit() {
		cudaDebug = 1; //Change this to 1 for debug
	}
};
static CudaStaticInit junkInitInstance__;

std::string& upcase(std::string& s) {
	for (int i = 0; i < s.size(); i++)
		s[i] = toupper(s[i]);
	return s;
}

void printVs(const std::vector<std::string>& curOrder) {
	if (!cudaDebug)
		return;
	for (int i = 0; i < curOrder.size(); i++) {
		if (i > 0)
			printf(",");
		printf("%s", curOrder[i].c_str());
	}
	printf("\n");
}

CG_outputRepr* get_outer_loops(SgNode *code_temp, bool &kernel_called_in_loop) {

	CG_outputRepr *outer_loops = NULL;
	SgForStatement *built = NULL;
	bool found_bx = false;
	SgNode *tmp = code_temp;
	while (isSgForStatement(tmp)) {

			AstTextAttribute* att =
					(AstTextAttribute*) (isSgNode(tmp)->getAttribute(
							"omega_comment"));
			std::string comment = att->toString();

			if (comment.find("~cuda~") != std::string::npos
					&& comment.find("preferredIdx: ") != std::string::npos) {
				std::string idx = comment.substr(
						comment.find("preferredIdx: ") + 14, std::string::npos);
				if (idx.find(" ") != std::string::npos)
					idx = idx.substr(0, idx.find(" "));
				if (strcmp(idx.c_str(), "bx") == 0) {
					if (built == NULL) {
						kernel_called_in_loop = false;

					} else
						kernel_called_in_loop = true;
					found_bx = true;
					if (built != NULL)
						outer_loops = new CG_roseRepr(isSgNode(built));

					return outer_loops;
				}

			}

			SgForStatement *temp = buildForStatement(
					isSgForStatement(code_temp)->get_for_init_stmt(),
					isSgForStatement(code_temp)->get_test(),
					isSgForStatement(code_temp)->get_increment(), NULL);
			if (built == NULL)
				built = temp;
			else
				built->set_loop_body(temp);
			if (isSgForStatement(tmp))
				tmp = isSgForStatement(tmp)->get_loop_body();
			else
				break;
		}

	return NULL;
}

std::set<std::string> get_loop_indices(CG_outputRepr *outer_loop) {

	SgForStatement *tmp = isSgForStatement(
			dynamic_cast<CG_roseRepr *>(outer_loop)->GetCode());
	std::set<std::string> indices;
	while (tmp) {
		SgForInitStatement *f_init = tmp->get_for_init_stmt();
		bool found = false;
		for (SgStatementPtrList::iterator in = f_init->get_init_stmt().begin();
				in != f_init->get_init_stmt().end(); in++) {
			if (isSgExprStatement(*in)) {
				SgExpression *exp = isSgExprStatement(*in)->get_expression();
				if (isSgBinaryOp(exp)) {
					SgVarRefExp *var = isSgVarRefExp(
							isSgBinaryOp(exp)->get_lhs_operand());
					if (var) {
						found = true;
						indices.insert(
								var->get_symbol()->get_name().getString());
						break;
					}

				}

			}

		}

		tmp = isSgForStatement(tmp->get_loop_body());

	}

	return indices;

}

SgNode* get_inner_loops(SgNode *code_temp) {

	bool found_bx = false;
	SgNode *tmp = code_temp;
	while (!found_bx && tmp)
		if (isSgForStatement(tmp)) {

			AstTextAttribute* att =
					(AstTextAttribute*) (isSgNode(tmp)->getAttribute(
							"omega_comment"));
			std::string comment = att->toString();

			if (comment.find("~cuda~") != std::string::npos
					&& comment.find("preferredIdx: ") != std::string::npos) {
				std::string idx = comment.substr(
						comment.find("preferredIdx: ") + 14, std::string::npos);
				if (idx.find(" ") != std::string::npos)
					idx = idx.substr(0, idx.find(" "));
				if (strcmp(idx.c_str(), "bx") == 0) {

					found_bx = true;

					return tmp;
				}

			}

			if (isSgForStatement(tmp))
				tmp = isSgForStatement(tmp)->get_loop_body();
			else
				break;

		}
	return NULL;
}

void printVS(const std::vector<std::string>& curOrder) {
	//if(!cudaDebug) return;
	for (int i = 0; i < curOrder.size(); i++) {
		if (i > 0)
			printf(",");
		printf("%s", curOrder[i].c_str());
	}
	printf("\n");
}

LoopCuda::~LoopCuda() {
	const int m = stmt.size();
	for (int i = 0; i < m; i++)
		stmt[i].code->clear();
}

bool LoopCuda::symbolExists(std::string s) {

	if (body_symtab->find_variable(SgName(s.c_str()))
			|| parameter_symtab->find_variable(SgName(s.c_str())))
		return true;
	if (globals->lookup_variable_symbol(SgName(s.c_str())))
		return true;
	for (int i = 0; i < idxNames.size(); i++)
		for (int j = 0; j < idxNames[i].size(); j++)
			if (strcmp(idxNames[i][j].c_str(), s.c_str()) == 0)
				return true;
	return false;
}

void LoopCuda::addSync(int stmt_num, std::string idxName) {
	//we store these and code-gen inserts sync to omega comments where stmt
	//in loop that has idxName being generated
	syncs.push_back(make_pair(stmt_num, idxName));
}

void LoopCuda::renameIndex(int stmt_num, std::string idx, std::string newName) {
	int level = findCurLevel(stmt_num, idx);
	if (idxNames.size() <= stmt_num || idxNames[stmt_num].size() < level)
		throw std::runtime_error("Invalid statment number of index");
	idxNames[stmt_num][level - 1] = newName.c_str();
}

enum Type {
	Int
};

struct VarDefs {
	std::string name;
	std::string secondName;
	SgExpression* size_expr; //array size as an expression (can be a product of other variables etc)
	SgType* type;
	SgExpression* in_data; //Variable of array to copy data in from (before kernel call)
	SgVariableSymbol* out_data; //Variable of array to copy data out to (after kernel call)
	std::vector<CG_outputRepr *> size_multi_dim; //-1 if linearized, the constant size N, of a NxN 2D array otherwise
	bool tex_mapped; //protonu-- true if this variable will be texture mapped, so no need to pass it as a argument
	std::string original_name; //this is such a hack, to store the original name, to store a table to textures used
};

SgNode* wrapInIfFromMinBound(SgNode* then_part, SgForStatement* loop,
		SgScopeStatement* symtab, SgVariableSymbol* bound_sym) {
	// CG_roseBuilder *ocg = new CG_roseBuilder(

	SgBinaryOp* test_expr = isSgBinaryOp(loop->get_test_expr());
	SgExpression* upperBound;
	SgExpression* conditional;
	upperBound = test_expr->get_rhs_operand();
	CG_outputRepr *ifstmt;

	SgCallExpression *call;
	if (call = isSgCallExpression(upperBound))
		if ((isSgVarRefExp(call->get_function()) &&    isSgVarRefExp(call->get_function())->get_symbol()->get_name().getString()
				== "__rose_lt") || (isSgFunctionRefExp(call->get_function()) &&    isSgFunctionRefExp(call->get_function())->get_symbol()->get_name().getString()
						== "__rose_lt")) {
			SgExprListExp* arg_list = call->get_args();

			SgExpression *if_bound_1 = NULL;
			SgExpression *if_bound_2 = NULL;
			SgExpressionPtrList & expressionPtrList =
					arg_list->get_expressions();

			for (SgExpressionPtrList::iterator i = expressionPtrList.begin();
					i != expressionPtrList.end(); i++)
				if (i == expressionPtrList.begin())
					if_bound_1 = *i;
				else
					if_bound_2 = *i;
			SgIfStmt *ifstmt = NULL;
			if (isSgValueExp(if_bound_2) && !isSgValueExp(if_bound_1))

				/*This relies on the minimum expression being the rhs operand of
				 * the min instruction.
				 */
				ifstmt = buildIfStmt(
						buildLessOrEqualOp(buildVarRefExp(bound_sym),
								if_bound_1), isSgStatement(then_part), NULL);
			else if (isSgVarRefExp(if_bound_2)
					&& (!isSgValueExp(if_bound_1) && !isSgVarRefExp(if_bound_1)))
				ifstmt = buildIfStmt(
						buildLessOrEqualOp(buildVarRefExp(bound_sym),
								if_bound_1), isSgStatement(then_part), NULL);

			else if (isSgValueExp(if_bound_1) && !isSgValueExp(if_bound_2))

				/*This relies on the minimum expression being the rhs operand of
				 * the min instruction.
				 */
				ifstmt = buildIfStmt(
						buildLessOrEqualOp(buildVarRefExp(bound_sym),
								if_bound_2), isSgStatement(then_part), NULL);
			else if (isSgVarRefExp(if_bound_1)
					&& (!isSgValueExp(if_bound_2) && !isSgVarRefExp(if_bound_2)))
				ifstmt = buildIfStmt(
						buildLessOrEqualOp(buildVarRefExp(bound_sym),
								if_bound_2), isSgStatement(then_part), NULL);

			return isSgNode(ifstmt);

		}

	/*if (isSgConditionalExp(upperBound)) {
	 conditional = isSgConditionalExp(upperBound)->get_conditional_exp();

	 if (isSgBinaryOp(conditional)) {
	 SgBinaryOp* binop = isSgBinaryOp(conditional);

	 if (isSgLessThanOp(binop) || isSgLessOrEqualOp(binop)) {
	 SgIfStmt *ifstmt = buildIfStmt(
	 buildLessOrEqualOp(buildVarRefExp(bound_sym),
	 test_expr), isSgStatement(then_part), NULL);
	 return isSgNode(ifstmt);
	 }

	 }

	 }
	 */

	return then_part;
}

/**
 * This would be better if it was done by a CHiLL xformation instead of at codegen
 *
 * state:
 * for(...)
 *   for(...)
 *     cur_body
 *   stmt1
 *
 * stm1 is in-between two loops that are going to be reduced. The
 * solution is to put stmt1 at the end of cur_body but conditionally run
 * in on the last step of the for loop.
 *
 * A CHiLL command that would work better:
 *
 * for(...)
 *   stmt0
 *   for(for i=0; i<n; i++)
 *     cur_body
 *   stmt1
 * =>
 * for(...)
 *   for(for i=0; i<n; i++)
 *     if(i==0) stmt0
 *     cur_body
 *     if(i==n-1) stmt1
 */

std::vector<SgForStatement*> findCommentedFors(const char* index, SgNode* tnl) {
	std::vector<SgForStatement *> result;
	bool next_loop_ok = false;

	if (isSgBasicBlock(tnl)) {

		SgStatementPtrList& list = isSgBasicBlock(tnl)->get_statements();

		for (SgStatementPtrList::iterator it = list.begin(); it != list.end();
				it++) {
			std::vector<SgForStatement*> t = findCommentedFors(index,
					isSgNode(*it));
			std::copy(t.begin(), t.end(), back_inserter(result));
		}
	} else if (isSgForStatement(tnl)) {

		AstTextAttribute* att =
				(AstTextAttribute*) (isSgNode(tnl)->getAttribute(
						"omega_comment"));
		if (att) {
			std::string comment = att->toString();

			if (comment.find("~cuda~") != std::string::npos
					&& comment.find("preferredIdx: ") != std::string::npos) {
				std::string idx = comment.substr(
						comment.find("preferredIdx: ") + 14, std::string::npos);
				if (idx.find(" ") != std::string::npos)
					idx = idx.substr(0, idx.find(" "));
				if (strcmp(idx.c_str(), index) == 0)
					next_loop_ok = true;
			}
		}
		std::vector<SgForStatement*> t = findCommentedFors(index,
				isSgForStatement(tnl)->get_loop_body());
		std::copy(t.begin(), t.end(), back_inserter(result));

		if (next_loop_ok) //{
			//printf("found loop %s\n", static_cast<tree_for *>(tn)->index()->name());
			result.push_back(isSgForStatement(tnl));
		//} //else {
		//printf("looking down for loop %s\n", static_cast<tree_for *>(tn)->index()->name());

		//}
		next_loop_ok = false;

	} else if (isSgIfStmt(tnl)) {
		//printf("looking down if\n");
		SgIfStmt *tni = isSgIfStmt(tnl);
		std::vector<SgForStatement*> t = findCommentedFors(index,
				tni->get_true_body());
		std::copy(t.begin(), t.end(), back_inserter(result));
		if(tni->get_false_body() != NULL){
		std::vector<SgForStatement*> t2 = findCommentedFors(index,
				tni->get_false_body());
		std::copy(t2.begin(), t2.end(), back_inserter(result));
		}
	}

	return result;
}

SgNode* forReduce(SgForStatement* loop, SgVariableSymbol* reduceIndex,
		SgScopeStatement* body_syms) {
	//We did the replacements all at once with recursiveFindPreferedIdxs
	//replacements r;
	//r.oldsyms.append(loop->index());
	//r.newsyms.append(reduceIndex);
	//tree_for* new_loop = (tree_for*)loop->clone_helper(&r, true);
	SgForStatement* new_loop = loop;

	//return body one loops in
	SgNode* tnl = loop_body_at_level(new_loop, 1);
	//wrap in conditional if necessary
	tnl = wrapInIfFromMinBound(tnl, new_loop, body_syms, reduceIndex);
	return tnl;
}

void recursiveFindRefs(SgNode* code, std::set<const SgVariableSymbol *>& syms,
		SgFunctionDefinition* def) {

	SgStatement* s = isSgStatement(code);
	// L = {symbols defined within 's'}, local variables declared within 's'
	ASTtools::VarSymSet_t L;
	ASTtools::collectDefdVarSyms(s, L);
	//dump (L, "L = ");

	// U = {symbols used within 's'}
	ASTtools::VarSymSet_t U;
	ASTtools::collectRefdVarSyms(s, U);
	//dump (U, "U = ");

	// U - L = {symbols used within 's' but not defined in 's'}
	// variable references to non-local-declared variables
	ASTtools::VarSymSet_t diff_U_L;
	set_difference(U.begin(), U.end(), L.begin(), L.end(),
			inserter(diff_U_L, diff_U_L.begin()));
	//dump (diff_U_L, "U - L = ");

	// Q = {symbols defined within the function surrounding 's' that are
	// visible at 's'}, including function parameters
	ASTtools::VarSymSet_t Q;
	ASTtools::collectLocalVisibleVarSyms(def->get_declaration(), s, Q);
//    dump (Q, "Q = ");

	// (U - L) \cap Q = {variables that need to be passed as parameters
	// to the outlined function}
	// a sub set of variables that are not globally visible (no need to pass at all)
	// It excludes the variables with a scope between global and the enclosing function
	set_intersection(diff_U_L.begin(), diff_U_L.end(), Q.begin(), Q.end(),
			inserter(syms, syms.begin()));

	/* std::vector<SgVariableSymbol *> scalars;
	 //SgNode  *tnl = static_cast<const omega::CG_roseRepr *>(repr)->GetCode();
	 SgStatement* stmt;
	 SgExpression* exp;
	 if (tnl != NULL) {
	 if(stmt = isSgStatement(tnl)){
	 if(isSgBasicBlock(stmt)){
	 SgStatementPtrList& stmts = isSgBasicBlock(stmt)->get_statements();
	 for(int i =0; i < stmts.size(); i++){
	 //omega::CG_roseRepr *r = new omega::CG_roseRepr(isSgNode(stmts[i]));
	 std::vector<SgVariableSymbol *> a = recursiveFindRefs(isSgNode(stmts[i]));
	 //delete r;
	 std::copy(a.begin(), a.end(), back_inserter(scalars));
	 }

	 }
	 else if(isSgForStatement(stmt)){

	 SgForStatement *tnf =  isSgForStatement(stmt);
	 //omega::CG_roseRepr *r = new omega::CG_roseRepr(isSgStatement(tnf->get_loop_body()));
	 std::vector<SgVariableSymbol *> a = recursiveFindRefs(isSgNode(tnf->get_loop_body()));
	 //delete r;
	 std::copy(a.begin(), a.end(), back_inserter(scalars));
	 }
	 else if(isSgFortranDo(stmt)){
	 SgFortranDo *tfortran =  isSgFortranDo(stmt);
	 omega::CG_roseRepr *r = new omega::CG_roseRepr(isSgStatement(tfortran->get_body()));
	 std::vector<SgVariableSymbol *> a = recursiveFindRefs(r);
	 delete r;
	 std::copy(a.begin(), a.end(), back_inserter(scalars));
	 }

	 else if(isSgIfStmt(stmt) ){
	 SgIfStmt* tni = isSgIfStmt(stmt);
	 //omega::CG_roseRepr *r = new omega::CG_roseRepr(isSgNode(tni->get_conditional()));
	 std::vector<SgVariableSymbol *> a = recursiveFindRefs(isSgNode(tni->get_conditional()));
	 //delete r;
	 std::copy(a.begin(), a.end(), back_inserter(scalars));
	 //r = new omega::CG_roseRepr(isSgNode(tni->get_true_body()));
	 a = recursiveFindRefs(isSgNode(tni->get_true_body()));
	 //delete r;
	 std::copy(a.begin(), a.end(), back_inserter(scalars));
	 //r = new omega::CG_roseRepr(isSgNode(tni->get_false_body()));
	 a = recursiveFindRefs(isSgNode(tni->get_false_body()));
	 //delete r;
	 std::copy(a.begin(), a.end(), back_inserter(scalars));
	 }
	 else if(isSgExprStatement(stmt)) {
	 //omega::CG_roseRepr *r = new omega::CG_roseRepr(isSgExpression(isSgExprStatement(stmt)->get_expression()));
	 std::vector<SgVariableSymbol *> a = recursiveFindRefs(isSgNode(isSgExprStatement(stmt)->get_expression()));
	 //delete r;
	 std::copy(a.begin(), a.end(), back_inserter(scalars));

	 }
	 }
	 }
	 else{
	 SgExpression* op = isSgExpression(tnl);
	 if(isSgVarRefExp(op)){

	 scalars.push_back(isSgVarRefExp(op)->get_symbol());

	 }
	 else if( isSgAssignOp(op)){
	 //omega::CG_roseRepr *r1 = new omega::CG_roseRepr(isSgAssignOp(op)->get_lhs_operand());
	 std::vector<SgVariableSymbol *> a1 = recursiveFindRefs(isSgNode(isSgAssignOp(op)->get_lhs_operand()));
	 //delete r1;
	 std::copy(a1.begin(), a1.end(), back_inserter(scalars));
	 //omega::CG_roseRepr *r2 = new omega::CG_roseRepr(isSgAssignOp(op)->get_rhs_operand());
	 std::vector<SgVariableSymbol *> a2 = recursiveFindRefs(isSgNode(isSgAssignOp(op)->get_rhs_operand()));
	 //delete r2;
	 std::copy(a2.begin(), a2.end(), back_inserter(scalars));

	 }
	 else if(isSgBinaryOp(op)){
	 // omega::CG_roseRepr *r1 = new omega::CG_roseRepr(isSgBinaryOp(op)->get_lhs_operand());
	 std::vector<SgVariableSymbol *> a1 = recursiveFindRefs(isSgNode(isSgBinaryOp(op)->get_lhs_operand()));
	 //delete r1;
	 std::copy(a1.begin(), a1.end(), back_inserter(scalars));
	 //omega::CG_roseRepr *r2 = new omega::CG_roseRepr(isSgBinaryOp(op)->get_rhs_operand());
	 std::vector<SgVariableSymbol *> a2 = recursiveFindRefs((isSgBinaryOp(op)->get_rhs_operand()));
	 //delete r2;
	 std::copy(a2.begin(), a2.end(), back_inserter(scalars));
	 }
	 else if(isSgUnaryOp(op)){
	 //omega::CG_roseRepr *r1 = new omega::CG_roseRepr(isSgUnaryOp(op)->get_operand());
	 std::vector<SgVariableSymbol *> a1 = recursiveFindRefs(isSgNode(isSgUnaryOp(op)->get_operand()));
	 //delete r1;
	 std::copy(a1.begin(), a1.end(), back_inserter(scalars));
	 }

	 }
	 return scalars;


	 */

}

SgNode* recursiveFindReplacePreferedIdxs(SgNode* code, SgSymbolTable* body_syms,
		SgSymbolTable* param_syms, SgScopeStatement* body,
		std::map<std::string, SgVariableSymbol*>& loop_idxs,
		SgGlobal* globalscope, bool sync = false) {
	//tree_node_list* tnl = new tree_node_list;
	//tree_node_list_iter tnli(code);
	SgVariableSymbol* idxSym = 0;
	std::vector<SgStatement*> r1;
	std::vector<SgNode*> r2;
	SgNode* tnli;
	SgNode* tnli1;
	SgNode* tnli2;
	SgBasicBlock * clone;

	if (isSgForStatement(code)) {
		AstTextAttribute* att =
				(AstTextAttribute*) (isSgNode(code)->getAttribute(
						"omega_comment"));

		std::string comment;
		if (att != NULL)
			comment = att->toString();
		std::string idxname;
		if (comment.find("~cuda~") != std::string::npos
				&& comment.find("preferredIdx: ") != std::string::npos) {

			std::string idx = comment.substr(
					comment.find("preferredIdx: ") + 14, std::string::npos);
			if (idx.find(" ") != std::string::npos)
				idx = idx.substr(0, idx.find(" "));
			if (loop_idxs.find(idx) != loop_idxs.end())
				idxSym = loop_idxs.find(idx)->second;
			idxname = idx;
			//Get the proc variable sybol for this preferred index
			if (idxSym == 0) {
				idxSym = body_syms->find_variable(idx.c_str());
				if (!idxSym)
					idxSym = param_syms->find_variable(idx.c_str());
				//printf("idx not found: lookup %p\n", idxSym);
				if (!idxSym) {
					SgVariableDeclaration* defn = buildVariableDeclaration(
							SgName((char*) idx.c_str()), buildIntType());
					//idxSym = new var_sym(type_s32, (char*)idx.c_str());
					SgInitializedNamePtrList& variables = defn->get_variables();
					SgInitializedNamePtrList::const_iterator i =
							variables.begin();
					SgInitializedName* initializedName = *i;
					SgVariableSymbol* vs = new SgVariableSymbol(
							initializedName);
					prependStatement(defn, body);
					vs->set_parent(body_syms);
					body_syms->insert(SgName((char*) idx.c_str()), vs);
					idxSym = vs;
					//printf("idx created and inserted\n");
				}
				//Now insert into our map for future
				std::cout << idx << "\n\n";
				loop_idxs.insert(make_pair(idx, idxSym));
			}
			//See if we have a sync as well
			if (comment.find("sync") != std::string::npos) {
				//printf("Inserting sync after current block\n");
				sync = true;
			}

		}
		if (idxSym) {
			SgForInitStatement* list =
					isSgForStatement(code)->get_for_init_stmt();
			SgStatementPtrList& initStatements = list->get_init_stmt();
			SgStatementPtrList::const_iterator j = initStatements.begin();
			SgVariableSymbol * index;

			if (SgExprStatement *expr = isSgExprStatement(*j))
				if (SgAssignOp* op = isSgAssignOp(expr->get_expression()))
					if (SgVarRefExp* var_ref = isSgVarRefExp(
							op->get_lhs_operand())) {
						index = var_ref->get_symbol();
						loop_idxs.insert(
								make_pair(index->get_name().getString(),
										idxSym));
					}
			std::vector<SgVarRefExp *> array = substitute(code, index, NULL,
					isSgNode(body_syms));

			for (int j = 0; j < array.size(); j++)
				array[j]->set_symbol(idxSym);
		}

		SgStatement* body_ = isSgStatement(
				recursiveFindReplacePreferedIdxs(
						isSgNode((isSgForStatement(code)->get_loop_body())),
						body_syms, param_syms, body, loop_idxs, globalscope));

		omega::CG_roseRepr * tnl = new omega::CG_roseRepr(code);
		omega::CG_outputRepr* block = tnl->clone();
		tnli = static_cast<const omega::CG_roseRepr *>(block)->GetCode();

		isSgForStatement(tnli)->set_loop_body(body_);
		body_->set_parent(tnli);

		if (idxSym) {
			SgForInitStatement* list =
					isSgForStatement(tnli)->get_for_init_stmt();
			SgStatementPtrList& initStatements = list->get_init_stmt();
			SgStatementPtrList::const_iterator j = initStatements.begin();
			const SgVariableSymbol* index;

			if (SgExprStatement *expr = isSgExprStatement(*j))
				if (SgAssignOp* op = isSgAssignOp(expr->get_expression()))
					if (SgVarRefExp* var_ref = isSgVarRefExp(
							op->get_lhs_operand())) {
						index = var_ref->get_symbol();
						loop_idxs.insert(
								make_pair(index->get_name().getString(),
										idxSym));
					}
			std::vector<SgVarRefExp *> array = substitute(tnli, index, NULL,
					isSgNode(body_syms));

			for (int j = 0; j < array.size(); j++)
				array[j]->set_symbol(idxSym);
		}
		//	std::cout << isSgNode(body_)->unparseToString() << "\n\n";
		if (att != NULL)
			tnli->setAttribute("omega_comment", att);

		if (sync) {
			SgName name_syncthreads("__syncthreads");
			SgFunctionSymbol * syncthreads_symbol =
					globalscope->lookup_function_symbol(name_syncthreads);

			// Create a call to __syncthreads():
			SgFunctionCallExp * syncthreads_call = buildFunctionCallExp(
					syncthreads_symbol, buildExprListExp());

			SgExprStatement* stmt = buildExprStatement(syncthreads_call);

			/*	if (SgBasicBlock* bb = isSgBasicBlock(
			 isSgForStatement(code)->get_loop_body()))
			 appendStatement(isSgStatement(stmt), bb);

			 else if (SgStatement* ss = isSgStatement(
			 isSgForStatement(code)->get_loop_body())) {
			 SgBasicBlock* bb2 = buildBasicBlock();

			 isSgNode(ss)->set_parent(bb2);
			 appendStatement(ss, bb2);

			 appendStatement(isSgStatement(stmt), bb2);
			 isSgNode(stmt)->set_parent(bb2);
			 isSgForStatement(code)->set_loop_body(bb2);
			 isSgNode(bb2)->set_parent(code);
			 }
			 */

			SgBasicBlock* bb2 = buildBasicBlock();

			bb2->append_statement(isSgStatement(tnli));
			bb2->append_statement(stmt);
			/* SgNode* parent = code->get_parent();
			 if(!isSgStatement(parent))
			 throw loop_error("Parent not a statement");

			 if(isSgForStatement(parent)){
			 if(SgStatement *ss = isSgForStatement(isSgForStatement(parent)->get_loop_body())){
			 omega::CG_roseRepr * tnl = new omega::CG_roseRepr(ss);
			 omega::CG_outputRepr* block= tnl->clone();

			 SgNode *new_ss = static_cast<const omega::CG_roseRepr *>(block)->GetCode();
			 SgBasicBlock* bb2 = buildBasicBlock();

			 isSgNode(new_ss)->set_parent(bb2);
			 appendStatement(isSgStatement(new_ss), bb2);
			 appendStatement(isSgStatement(stmt), bb2);
			 isSgNode(stmt)->set_parent(bb2);

			 isSgStatement(parent)->replace_statement_from_basicBlock(ss, isSgStatement(bb2));

			 }else if(isSgBasicBlock(isSgForStatement(parent)->get_loop_body()))
			 isSgStatement(isSgForStatement(parent)->get_loop_body())->insert_statement(isSgStatement(code), stmt, false);
			 else
			 throw loop_error("parent statement type undefined!!");

			 }
			 else if(isSgBasicBlock(parent))
			 isSgStatement(parent)->insert_statement(isSgStatement(code), stmt, false);
			 else
			 throw loop_error("parent statement type undefined!!");

			 //tnl->print();
			 *
			 *
			 */
			sync = true;
			return isSgNode(bb2);

		} else
			return tnli;
	} else if (isSgIfStmt(code)) {
		SgStatement* body_ = isSgStatement(
				recursiveFindReplacePreferedIdxs(
						isSgNode((isSgIfStmt(code)->get_true_body())),
						body_syms, param_syms, body, loop_idxs, globalscope));

		//omega::CG_roseRepr * tnl = new omega::CG_roseRepr(code);
		//omega::CG_outputRepr* block = tnl->clone();
		//tnli = static_cast<const omega::CG_roseRepr *>(block)->GetCode();

		CG_outputRepr *cond = new CG_roseRepr(
				isSgIfStmt(code)->get_conditional());
		SgStatement *conditional = isSgStatement(
				static_cast<CG_roseRepr*>(cond)->GetCode());

		for (std::map<std::string, SgVariableSymbol*>::iterator it =
				loop_idxs.begin(); it != loop_idxs.end(); it++) {
			SgVariableSymbol *indice = body_syms->find_variable(
					it->first.c_str());

			if (indice != NULL) {
				std::vector<SgVarRefExp *> array = substitute(
						isSgNode(conditional), indice, NULL,
						isSgNode(body_syms));

				for (int j = 0; j < array.size(); j++)
					array[j]->set_symbol(it->second);
			}
		}
		SgIfStmt *new_if = buildIfStmt(conditional, body_, NULL);

		isSgNode(body_)->set_parent(isSgNode(new_if));

		if ((isSgIfStmt(code)->get_false_body()))
			new_if->set_false_body(
					isSgStatement(
							recursiveFindReplacePreferedIdxs(
									isSgNode(
											(isSgIfStmt(code)->get_false_body())),
									body_syms, param_syms, body, loop_idxs,
									globalscope)));

		return isSgNode(new_if);
	} else if (isSgStatement(code) && !isSgBasicBlock(code)) {
		omega::CG_roseRepr * tnl = new omega::CG_roseRepr(code);
		omega::CG_outputRepr* block = tnl->clone();
		tnli = static_cast<const omega::CG_roseRepr *>(block)->GetCode();

		return tnli;

	} else if (isSgBasicBlock(code)) {
		SgStatementPtrList& tnl = isSgBasicBlock(code)->get_statements();

		SgStatementPtrList::iterator temp;
		clone = buildBasicBlock();
		bool sync_found = false;
		for (SgStatementPtrList::const_iterator it = tnl.begin();
				it != tnl.end(); it++) {

			if (isSgForStatement(*it)) {
				AstTextAttribute* att =
						(AstTextAttribute*) (isSgNode(*it)->getAttribute(
								"omega_comment"));

				std::string comment;
				if (att != NULL)
					comment = att->toString();

				if (comment.find("~cuda~") != std::string::npos
						&& comment.find("preferredIdx: ")
								!= std::string::npos) {
					std::string idx = comment.substr(
							comment.find("preferredIdx: ") + 14,
							std::string::npos);
					if (idx.find(" ") != std::string::npos)
						idx = idx.substr(0, idx.find(" "));
					//printf("sym_tab preferred index: %s\n", idx.c_str());
					if (loop_idxs.find(idx) != loop_idxs.end())
						idxSym = loop_idxs.find(idx)->second;
					//Get the proc variable sybol for this preferred index
					if (idxSym == 0) {
						idxSym = body_syms->find_variable(idx.c_str());
						if (!idxSym)
							idxSym = param_syms->find_variable(idx.c_str());
						//printf("idx not found: lookup %p\n", idxSym);
						if (!idxSym) {
							SgVariableDeclaration* defn =
									buildVariableDeclaration(
											SgName((char*) idx.c_str()),
											buildIntType());
							//idxSym = new var_sym(type_s32, (char*)idx.c_str());
							SgInitializedNamePtrList& variables =
									defn->get_variables();
							SgInitializedNamePtrList::const_iterator i =
									variables.begin();
							SgInitializedName* initializedName = *i;
							SgVariableSymbol* vs = new SgVariableSymbol(
									initializedName);
							prependStatement(defn, body);
							vs->set_parent(body_syms);
							body_syms->insert(SgName((char*) idx.c_str()), vs);
							//printf("idx created and inserted\n");
							idxSym = vs;
						}
						//Now insert into our map for future
						std::cout << idx << "\n\n";
						loop_idxs.insert(make_pair(idx, idxSym));

					}
					//See if we have a sync as well
					if (comment.find("sync") != std::string::npos) {
						//printf("Inserting sync after current block\n");
						sync = true;
					}

				}
				if (idxSym) {
					SgForInitStatement* list =
							isSgForStatement(*it)->get_for_init_stmt();
					SgStatementPtrList& initStatements = list->get_init_stmt();
					SgStatementPtrList::const_iterator j =
							initStatements.begin();
					const SgVariableSymbol* index;

					if (SgExprStatement *expr = isSgExprStatement(*j))
						if (SgAssignOp* op = isSgAssignOp(
								expr->get_expression()))
							if (SgVarRefExp* var_ref = isSgVarRefExp(
									op->get_lhs_operand()))
								index = var_ref->get_symbol();

					std::vector<SgVarRefExp *> array = substitute(*it, index,
					NULL, isSgNode(body_syms));

					for (int j = 0; j < array.size(); j++)
						array[j]->set_symbol(idxSym);

				}


				SgStatement* body_ =
						isSgStatement(
								recursiveFindReplacePreferedIdxs(
										isSgNode(
												(isSgForStatement(*it)->get_loop_body())),
										body_syms, param_syms, body, loop_idxs,
										globalscope));

				omega::CG_roseRepr * tnl = new omega::CG_roseRepr(*it);
				omega::CG_outputRepr* block = tnl->clone();
				tnli =
						static_cast<const omega::CG_roseRepr *>(block)->GetCode();

				isSgForStatement(tnli)->set_loop_body(body_);
				body_->set_parent(tnli);
				if (idxSym) {
					SgForInitStatement* list =
							isSgForStatement(tnli)->get_for_init_stmt();
					SgStatementPtrList& initStatements = list->get_init_stmt();
					SgStatementPtrList::const_iterator j =
							initStatements.begin();
					const SgVariableSymbol* index;

					if (SgExprStatement *expr = isSgExprStatement(*j))
						if (SgAssignOp* op = isSgAssignOp(
								expr->get_expression()))
							if (SgVarRefExp* var_ref = isSgVarRefExp(
									op->get_lhs_operand()))
								index = var_ref->get_symbol();

					std::vector<SgVarRefExp *> array = substitute(tnli, index,
					NULL, isSgNode(body_syms));

					for (int j = 0; j < array.size(); j++)
						array[j]->set_symbol(idxSym);
				}
				idxSym = 0;
				//	std::cout << isSgNode(body_)->unparseToString() << "\n\n";
				if (att != NULL)
					tnli->setAttribute("omega_comment", att);
				clone->append_statement(isSgStatement(tnli));
				if (sync) {
					SgName name_syncthreads("__syncthreads");
					SgFunctionSymbol * syncthreads_symbol =
							globalscope->lookup_function_symbol(
									name_syncthreads);

					// Create a call to __syncthreads():
					SgFunctionCallExp * syncthreads_call = buildFunctionCallExp(
							syncthreads_symbol, buildExprListExp());

					SgExprStatement* stmt = buildExprStatement(
							syncthreads_call);

					/*	if (SgBasicBlock* bb = isSgBasicBlock(
					 isSgForStatement(code)->get_loop_body()))
					 appendStatement(isSgStatement(stmt), bb);

					 else if (SgStatement* ss = isSgStatement(
					 isSgForStatement(code)->get_loop_body())) {
					 SgBasicBlock* bb2 = buildBasicBlock();

					 isSgNode(ss)->set_parent(bb2);
					 appendStatement(ss, bb2);

					 appendStatement(isSgStatement(stmt), bb2);
					 isSgNode(stmt)->set_parent(bb2);
					 isSgForStatement(code)->set_loop_body(bb2);
					 isSgNode(bb2)->set_parent(code);
					 }
					 */

					//SgBasicBlock* bb2 = buildBasicBlock();
					clone->append_statement(stmt);
					/* SgNode* parent = code->get_parent();
					 if(!isSgStatement(parent))
					 throw loop_error("Parent not a statement");

					 if(isSgForStatement(parent)){
					 if(SgStatement *ss = isSgForStatement(isSgForStatement(parent)->get_loop_body())){
					 omega::CG_roseRepr * tnl = new omega::CG_roseRepr(ss);
					 omega::CG_outputRepr* block= tnl->clone();

					 SgNode *new_ss = static_cast<const omega::CG_roseRepr *>(block)->GetCode();
					 SgBasicBlock* bb2 = buildBasicBlock();

					 isSgNode(new_ss)->set_parent(bb2);
					 appendStatement(isSgStatement(new_ss), bb2);
					 appendStatement(isSgStatement(stmt), bb2);
					 isSgNode(stmt)->set_parent(bb2);

					 isSgStatement(parent)->replace_statement_from_basicBlock(ss, isSgStatement(bb2));

					 }else if(isSgBasicBlock(isSgForStatement(parent)->get_loop_body()))
					 isSgStatement(isSgForStatement(parent)->get_loop_body())->insert_statement(isSgStatement(code), stmt, false);
					 else
					 throw loop_error("parent statement type undefined!!");

					 }
					 else if(isSgBasicBlock(parent))
					 isSgStatement(parent)->insert_statement(isSgStatement(code), stmt, false);
					 else
					 throw loop_error("parent statement type undefined!!");

					 //tnl->print();
					 *
					 *
					 */
					sync = true;
					//	return isSgNode(bb2);

				}

				//	return tnli;
			} else if (isSgIfStmt(*it)) {
				SgStatement* body_ = isSgStatement(
						recursiveFindReplacePreferedIdxs(
								isSgNode((isSgIfStmt(*it)->get_true_body())),
								body_syms, param_syms, body, loop_idxs,
								globalscope));

				omega::CG_roseRepr * tnl = new omega::CG_roseRepr(*it);
				omega::CG_outputRepr* block = tnl->clone();
				tnli1 =
						static_cast<const omega::CG_roseRepr *>(block)->GetCode();

				isSgIfStmt(tnli1)->set_true_body(body_);
				body_->set_parent(tnli1);

				if ((isSgIfStmt(*it)->get_false_body()))
					isSgIfStmt(tnli1)->set_false_body(
							isSgStatement(
									recursiveFindReplacePreferedIdxs(
											isSgNode(
													(isSgIfStmt(*it)->get_false_body())),
											body_syms, param_syms, body,
											loop_idxs, globalscope)));

				clone->append_statement(isSgStatement(tnli1));
				//return tnli;
			} else if (isSgStatement(*it)) {
				omega::CG_roseRepr * tnl = new omega::CG_roseRepr(*it);
				omega::CG_outputRepr* block = tnl->clone();
				tnli2 =
						static_cast<const omega::CG_roseRepr *>(block)->GetCode();

				clone->append_statement(isSgStatement(tnli2));
				//return tnli;

			}
		}

		return isSgNode(clone);

	}

	//return isSgNode(clone);

	/*	if (!isSgBasicBlock(
	 recursiveFindReplacePreferedIdxs(isSgNode(*it), body_syms,
	 param_syms, body, loop_idxs, globalscope))) {
	 SgStatement *to_push = isSgStatement(
	 recursiveFindReplacePreferedIdxs(isSgNode(*it),
	 body_syms, param_syms, body, loop_idxs,
	 globalscope, sync));
	 clone->append_statement(to_push);

	 if ((sync_found) && isSgForStatement(to_push)) {
	 SgName name_syncthreads("__syncthreads");
	 SgFunctionSymbol * syncthreads_symbol =
	 globalscope->lookup_function_symbol(
	 name_syncthreads);

	 // Create a call to __syncthreads():
	 SgFunctionCallExp * syncthreads_call = buildFunctionCallExp(
	 syncthreads_symbol, buildExprListExp());

	 SgExprStatement* stmt = buildExprStatement(
	 syncthreads_call);

	 clone->append_statement(isSgStatement(stmt));
	 }
	 //	std::cout<<isSgNode(*it)->unparseToString()<<"\n\n";
	 } else {

	 SgStatementPtrList& tnl2 = isSgBasicBlock(
	 recursiveFindReplacePreferedIdxs(isSgNode(*it),
	 body_syms, param_syms, body, loop_idxs,
	 globalscope))->get_statements();
	 for (SgStatementPtrList::const_iterator it2 = tnl2.begin();
	 it2 != tnl2.end(); it2++) {
	 clone->append_statement(*it2);

	 sync_found = true;
	 //	std::cout<<isSgNode(*it2)->unparseToString()<<"\n\n";
	 }
	 }

	 }
	 return isSgNode(clone);
	 }
	 */
//  return tnl;
}

// loop_vars -> array references
// loop_idxs -> <idx_name,idx_sym> map for when we encounter a loop with a different preferredIndex
// dim_vars -> out param, fills with <old,new> var_sym pair for 2D array dimentions (messy stuff)
SgNode* swapVarReferences(SgNode* code,
		std::set<const SgVariableSymbol *>& syms, SgSymbolTable* param,
		SgSymbolTable* body, SgScopeStatement* body_stmt) {
	//Iterate over every expression, looking up each variable and type
	//reference used and possibly replacing it or adding it to our symbol
	//table
	//
	//We use the built-in cloning helper methods to seriously help us with this!

	//Need to do a recursive mark

	std::set<const SgVariableSymbol *>::iterator myIterator;
	for (myIterator = syms.begin(); myIterator != syms.end(); myIterator++) {
		SgName var_name = (*myIterator)->get_name();
		std::string x = var_name.getString();

		if ((param->find_variable(var_name) == NULL)
				&& (body->find_variable(var_name) == NULL)) {
			SgInitializedName* decl = (*myIterator)->get_declaration();

			SgVariableSymbol* dvs = new SgVariableSymbol(decl);
			SgVariableDeclaration* var_decl = buildVariableDeclaration(
					dvs->get_name(), dvs->get_type());

			AstTextAttribute* att = (AstTextAttribute*) (isSgNode(
					decl->get_declaration())->getAttribute("__shared__"));
			if (isSgNode(decl->get_declaration())->attributeExists(
					"__shared__"))
				var_decl->get_declarationModifier().get_storageModifier().setCudaShared();

			appendStatement(var_decl, body_stmt);

			dvs->set_parent(body);
			body->insert(var_name, dvs);
		}

		std::vector<SgVarRefExp *> array = substitute(code, *myIterator, NULL,
				isSgNode(body));

		SgVariableSymbol* var = (SgVariableSymbol*) (*myIterator);
		for (int j = 0; j < array.size(); j++)
			array[j]->set_symbol(var);
	}

	return code;
}

bool LoopCuda::validIndexes(int stmt, const std::vector<std::string>& idxs) {
	for (int i = 0; i < idxs.size(); i++) {
		bool found = false;
		for (int j = 0; j < idxNames[stmt].size(); j++) {
			if (strcmp(idxNames[stmt][j].c_str(), idxs[i].c_str()) == 0) {
				found = true;
			}
		}
		if (!found) {
			return false;
		}
	}
	return true;
}

bool LoopCuda::cudaize_v2(int stmt_num, std::string kernel_name,
		std::map<std::string, int> array_dims,
		std::vector<std::string> blockIdxs, std::vector<std::string> threadIdxs,
		std::vector<std::string> kernel_params) {
	//int stmt_num = 0;
	CG_outputBuilder *ocg = ir->builder();
	if (cudaDebug) {
		printf("cudaize_v2(%s, {", kernel_name.c_str());
		//for(
		printf("}, blocks={");
		printVs(blockIdxs);
		printf("}, thread={");
		printVs(threadIdxs);
		printf("})\n");
	}

	for (int i = 0; i < kernel_params.size(); i++)
		kernel_parameters.insert(kernel_params[i]);

	//this->array_dims.push_back(array_dims);
	if (!validIndexes(stmt_num, blockIdxs)) {
		throw std::runtime_error("One of the indexes in the block list was not "
				"found in the current set of indexes.");
	}
	if (!validIndexes(stmt_num, threadIdxs)) {
		throw std::runtime_error(
				"One of the indexes in the thread list was not "
						"found in the current set of indexes.");
	}
	if (blockIdxs.size() == 0) {
		//	throw std::runtime_error("Cudaize: Need at least one block dimention");
		cu_bx.push_back(1);
		cu_bx_repr.push_back(NULL);
	}

	if (threadIdxs.size() == 0) {
		//	throw std::runtime_error("Cudaize: Need at least one block dimention");
		cu_tx.push_back(1);
		cu_tx_repr.push_back(NULL);
	}
	int block_level = 0;
	std::vector<int> thread_and_block_levels;
	//Now, we will determine the actual size (if possible, otherwise
	//complain) for the block dimentions and thread dimentions based on our
	//indexes and the relations for our stmt;
	for (int i = 0; i < blockIdxs.size(); i++) {
		int level = findCurLevel(stmt_num, blockIdxs[i]);
		int ub, lb;
		CG_outputRepr* ubrepr = extractCudaUB(stmt_num, level, ub, lb);
		if (lb != 0) {
			//attempt to "normalize" the loop with an in-place tile and then re-check our bounds
			if (cudaDebug)
				printf(
						"Cudaize: doing tile at level %d to try and normalize lower bounds\n",
						level);
			tile(stmt_num, level, 1, level, CountedTile);
			idxNames[stmt_num].insert(idxNames[stmt_num].begin() + (level), ""); //TODO: possibly handle this for all sibling stmts
			ubrepr = extractCudaUB(stmt_num, level, ub, lb);
		}
		if (lb != 0) {
			char buf[1024];
			sprintf(buf,
					"Cudaize: Loop at level %d does not have 0 as it's lower bound",
					level);
			throw std::runtime_error(buf);
		}
		if (ub < 0) {
			char buf[1024];
			sprintf(buf,
					"Cudaize: Loop at level %d does not have a hard upper bound",
					level);
			//Anand: Commenting out error indication for lack of constant upper bound
			//throw std::runtime_error(buf);
		}
		if (cudaDebug)
			printf("block idx %s level %d lb: %d ub %d\n", blockIdxs[i].c_str(),
					level, lb, ub);
		if (i == 0) {
			block_level = level;
			if (ubrepr == NULL) {
				cu_bx.push_back(ub + 1);
				cu_bx_repr.push_back(NULL);
			} else {
				cu_bx.push_back(0);
				cu_bx_repr.push_back(
						ocg->CreatePlus(ubrepr, ocg->CreateInt(1)));
			}
			idxNames[stmt_num][level - 1] = "bx";
		} else if (i == 1) {
			if (ubrepr == NULL) {
				cu_by.push_back(ub + 1);
				cu_by_repr.push_back(NULL);
			} else {
				cu_by.push_back(0);
				cu_by_repr.push_back(
						ocg->CreatePlus(ubrepr, ocg->CreateInt(1)));
			}
			idxNames[stmt_num][level - 1] = "by";
		}

		thread_and_block_levels.push_back(level);

	}

	if (cu_by.size() == 0 && cu_by_repr.size() == 0)
		block_level = 0;

	else if (cu_by.size() > 0 && !cu_by[cu_by.size() - 1]
			&& cu_by_repr.size() > 0 && !cu_by_repr[cu_by_repr.size() - 1])
		block_level = 0;

	if (blockIdxs.size() < 2) {
		cu_by_repr.push_back(NULL);
		cu_by.push_back(0);
	}
	int thread_level1 = 0;
	int thread_level2 = 0;
	for (int i = 0; i < threadIdxs.size(); i++) {
		int level = findCurLevel(stmt_num, threadIdxs[i]);
		int ub, lb;
		CG_outputRepr* ubrepr = extractCudaUB(stmt_num, level, ub, lb);
		if (lb != 0) {
			//attempt to "normalize" the loop with an in-place tile and then re-check our bounds
			if (cudaDebug)
				printf(
						"Cudaize: doing tile at level %d to try and normalize lower bounds\n",
						level);
			tile(stmt_num, level, 1, level, CountedTile);
			idxNames[stmt_num].insert(idxNames[stmt_num].begin() + (level), "");
			ubrepr = extractCudaUB(stmt_num, level, ub, lb);
		}
		if (lb != 0) {
			char buf[1024];
			sprintf(buf,
					"Cudaize: Loop at level %d does not have 0 as it's lower bound",
					level);
			throw std::runtime_error(buf);
		}
		if (ub < 0) {
			char buf[1024];
			sprintf(buf,
					"Cudaize: Loop at level %d does not have a hard upper bound",
					level);
			//Anand: Commenting out error indication for lack of constant upper bound
			//throw std::runtime_error(buf);
		}

		if (cudaDebug)
			printf("thread idx %s level %d lb: %d ub %d\n",
					threadIdxs[i].c_str(), level, lb, ub);
		if (i == 0) {
			thread_level1 = level;
			if (ubrepr == NULL) {
				cu_tx.push_back(ub + 1);
				cu_tx_repr.push_back(NULL);
			} else {
				cu_tx.push_back(0);
				cu_tx_repr.push_back(
						ocg->CreatePlus(ubrepr, ocg->CreateInt(1)));
			}
			idxNames[stmt_num][level - 1] = "tx";

		} else if (i == 1) {
			thread_level2 = level;
			if (ubrepr == NULL) {
				cu_ty.push_back(ub + 1);
				cu_ty_repr.push_back(NULL);
			} else {
				cu_ty.push_back(0);
				cu_ty_repr.push_back(
						ocg->CreatePlus(ubrepr, ocg->CreateInt(1)));
			}
			idxNames[stmt_num][level - 1] = "ty";

		} else if (i == 2) {
			if (ubrepr == NULL) {
				cu_tz.push_back(ub + 1);
				cu_tz_repr.push_back(NULL);
			} else {
				cu_tz.push_back(0);
				cu_tz_repr.push_back(
						ocg->CreatePlus(ubrepr, ocg->CreateInt(1)));
			}
			idxNames[stmt_num][level - 1] = "tz";

		}
		thread_and_block_levels.push_back(level);
	}
	if (cu_ty.size() == 0 && cu_ty_repr.size() == 0)
		thread_level1 = 0;

	if (cu_tz.size() == 0 && cu_tz_repr.size() == 0)
		thread_level2 = 0;

	if (cu_ty.size() > 0 && !cu_ty[cu_ty.size() - 1] && cu_ty_repr.size() > 0
			&& !cu_ty_repr[cu_ty_repr.size() - 1])
		thread_level1 = 0;
	if (cu_tz.size() > 0 && !cu_tz[cu_tz.size() - 1] && cu_tz_repr.size() > 0
			&& !cu_tz_repr[cu_tz_repr.size() - 1])
		thread_level2 = 0;

	if (threadIdxs.size() < 2) {
		cu_ty.push_back(0);
		cu_ty_repr.push_back(NULL);

	}
	if (threadIdxs.size() < 3) {
		cu_tz.push_back(0);
		cu_tz_repr.push_back(NULL);
	}

	//Make changes to nonsplitlevels

	std::vector<int> lex = getLexicalOrder(stmt_num);

	//cudaized = getStatements(lex, 0);

	cudaized.push_back(getStatements(lex, 0));

	for (std::set<int>::iterator i = cudaized[cudaized.size() - 1].begin();
			i != cudaized[cudaized.size() - 1].end(); i++) {
		if (block_level) {
			//stmt[i].nonSplitLevels.append((block_level)*2);
			stmt_nonSplitLevels[*i].push_back((block_level) * 2);
		}
		if (thread_level1) {
			//stmt[i].nonSplitLevels.append((thread_level1)*2);
			stmt_nonSplitLevels[*i].push_back((thread_level1) * 2);
		}
		if (thread_level2) {
			//stmt[i].nonSplitLevels.append((thread_level1)*2);
			stmt_nonSplitLevels[*i].push_back((thread_level2) * 2);
		}
		idxNames[*i] = idxNames[stmt_num];
	}

	if (cudaDebug) {
		printf("Codegen: current names: ");
		printVS(idxNames[stmt_num]);
	}
	//Set codegen flag
	code_gen_flags |= GenCudaizeV2;

	//Save array dimention sizes
	this->array_dims.push_back(array_dims);
	cu_kernel_name.push_back(kernel_name.c_str());
	block_and_thread_levels.insert(
			std::pair<int, std::vector<int> >(stmt_num,
					thread_and_block_levels));
}

void LoopCuda::cudaize_codegen_v2() {

	//apply_xform();

	if (!(code_gen_flags & GenCudaizeV2)) {

		std::set<int> stmts;
		for (int i = 0; i < stmt.size(); i++)
			stmts.insert(i);

		setup_code = new CG_roseRepr(getCode(2, stmts));

		return;
	}

	printf("cudaize codegen V2\n");
	CG_roseBuilder *ocg = dynamic_cast<CG_roseBuilder*>(ir->builder());
	if (!ocg)
		return;

	std::vector<std::pair<std::set<int>, int> > ordered_cudaized_stmts;

	//int ref_stmt_num = *(cudaized.begin());
	//std::vector<int> lex = getLexicalOrder(ref_stmt_num);

	//cudaized = getStatements(lex, 0);

	//sort cudaized according to lexical order into ordered cudaized statements

	std::vector<int> sort_aid;
	SgName name_cuda_free("cudaFree");
	SgFunctionDeclaration * decl_cuda_free =
			buildNondefiningFunctionDeclaration(name_cuda_free, buildVoidType(),
					buildFunctionParameterList(), globals);
	std::set<int> all_cudaized_statements;
	for (int i = 0; i < cudaized.size(); i++) {
		sort_aid.push_back(
				get_const(stmt[*(cudaized[i].begin())].xform, 0, Output_Var));
		all_cudaized_statements.insert(cudaized[i].begin(), cudaized[i].end());
	}

	for (int i = 0; i < stmt.size(); i++) {

		if (all_cudaized_statements.find(i) == all_cudaized_statements.end()) {
			int j;
			for (j = 0; j < cudaized.size(); j++)
				if (get_const(stmt[i].xform, 0, Output_Var)
						== get_const(stmt[*(cudaized[j].begin())].xform, 0,
								Output_Var)) {
					cudaized[j].insert(i);
					break;
				}
			if (j == cudaized.size()
					&& all_cudaized_statements.find(i)
							== all_cudaized_statements.end())
				sort_aid.push_back(get_const(stmt[i].xform, 0, Output_Var));

		}
	}

	std::sort(sort_aid.begin(), sort_aid.end());

	for (int i = 0; i < sort_aid.size(); i++) {

		int start = i;

		while (i + 1 < sort_aid.size() && sort_aid[i + 1] == sort_aid[i])
			i++;

		int j;
		for (j = 0; j < cudaized.size(); j++)
			if (get_const(stmt[*(cudaized[j].begin())].xform, 0, Output_Var)
					== sort_aid[start]) {
				ordered_cudaized_stmts.push_back(
						std::pair<std::set<int>, int>(cudaized[j], j));
				break;
			}
		if (j == cudaized.size()) {
			std::set<int> temp;
			for (int j = 0; j < stmt.size(); j++) {

				if (all_cudaized_statements.find(j)
						== all_cudaized_statements.end())
					if (sort_aid[start]
							== get_const(stmt[j].xform, 0, Output_Var)) {

						temp.insert(j);
					}
			}
			ordered_cudaized_stmts.push_back(
					std::pair<std::set<int>, int>(temp, -1));

		}
	}

	SgNode* code_after = NULL;

	std::map<std::string, IR_ArrayRef *> arrays_within_structs;
	std::map<std::string, SgDotExp*> scalars_within_structs;
	SgStatementPtrList * new_list = new SgStatementPtrList;
	for(int i=0; i < ptr_variables_non_const.size(); i++){



	//if(kernel_parameters.find(ptr_variables[i]->name()) == kernel_parameters.end()){
	 SgType *tn;
	 if(ptr_variables_non_const[i]->elem_type() == IR_CONSTANT_FLOAT)
	 tn = buildFloatType();
	 else if(ptr_variables_non_const[i]->elem_type() == IR_CONSTANT_INT)
	 tn = buildIntType();
	 else
	 throw loop_error("Pointer type unidentified in cudaize_codegen_v2!");

	 CG_outputRepr *size = new CG_roseRepr(buildSizeOfOp(tn));
	 for(int j = 0; j < ptr_variables_non_const[i]->n_dim(); j++){
	 tn = buildPointerType(tn);
	 size = ocg->CreateTimes(size, ptr_variables_non_const[i]->size(j)->clone());

	 }
	 SgName name_malloc("malloc");

	 SgFunctionDeclaration * decl_malloc =
	 buildNondefiningFunctionDeclaration(name_malloc,
	 tn	, buildFunctionParameterList(),
	 globals);

	 SgExprListExp* args = buildExprListExp();
	 args->append_expression(static_cast<CG_roseRepr *>(size)->GetExpression());

	 SgVariableSymbol *temp = body_symtab->find_variable(
	 SgName(ptr_variables_non_const[i]->name()));

	 //    decl_cuda_malloc->get_parameterList()->append_arg
	 SgFunctionCallExp *the_call = buildFunctionCallExp(
	 buildFunctionRefExp(decl_malloc), args);

	SgCastExp *assign =  buildCastExp(the_call, tn);

	 SgAssignOp* stmt_ = buildAssignOp(buildVarRefExp(temp), assign);

	 SgExprStatement* stmt = buildExprStatement(stmt_);

	 // = new SgStatementPtrList;
	 (*new_list).push_back(stmt);
	 //setup_code = ocg->StmtListAppend(setup_code,
	 //new CG_roseRepr(new_list));

	// }

	 }

	std::set<int> ptrs;
	for (int iter = 0; iter < ordered_cudaized_stmts.size(); iter++)
		for (std::set<int>::iterator it =
				ordered_cudaized_stmts[iter].first.begin();
				it != ordered_cudaized_stmts[iter].first.end(); it++) {

			std::vector<IR_PointerArrayRef *> refs__ = ir->FindPointerArrayRef(
					stmt[*it].code);

			for (int j = 0; j < refs__.size(); j++)
				for (int k = 0; k < ptr_variables.size(); k++)
					if (refs__[j]->name() == ptr_variables[k]->name())
						ptrs.insert(k);
		}
	for (std::set<int>::iterator it = ptrs.begin(); it != ptrs.end(); it++) {
		//	if (kernel_parameters.find(ptr_variables[*it]->name())
		//			== kernel_parameters.end()) {
		SgType *tn;
		/*if (ptr_variables[*it]->elem_type() == IR_CONSTANT_FLOAT)
		 tn = buildFloatType();
		 else if (ptr_variables[*it]->elem_type() == IR_CONSTANT_INT)
		 tn = buildIntType();
		 else
		 throw loop_error(
		 "Pointer type unidentified in cudaize_codegen_v2!");


		 CG_outputRepr *size = new CG_roseRepr(buildSizeOfOp(tn));
		 for (int j = 0; j < ptr_variables[*it]->n_dim(); j++) {
		 tn = buildPointerType(tn);
		 size = ocg->CreateTimes(size->clone(),
		 ptr_variables[*it]->size(j)->clone());

		 }
		 */
//		assert(ptr_variables[*it]->n_dim() == 1);
		// ocg->StmtListAppend(init_code,
		//					ir->CreateMalloc(ptr_variables[*it]->elem_type() ,ptr_variables[*it]->name(),
		//							ptr_variables[*it]->size(0)->clone()));
		/*	SgName name_malloc("malloc");

		 SgFunctionDeclaration * decl_malloc =
		 buildNondefiningFunctionDeclaration(name_malloc, tn,
		 buildFunctionParameterList(), globals);

		 SgExprListExp* args = buildExprListExp();
		 args->append_expression(
		 static_cast<CG_roseRepr *>(size)->GetExpression());

		 SgVariableSymbol *temp = body_symtab->find_variable(
		 SgName(ptr_variables[*it]->name()));

		 //    decl_cuda_malloc->get_parameterList()->append_arg
		 SgFunctionCallExp *the_call = buildFunctionCallExp(
		 buildFunctionRefExp(decl_malloc), args);

		 CG_outputRepr *assign = new CG_roseRepr(the_call);

		 SgAssignOp* stmt_ = buildAssignOp(buildVarRefExp(temp), the_call);

		 SgExprStatement* stmt = buildExprStatement(stmt_);

		 SgStatementPtrList * new_list = new SgStatementPtrList;
		 (*new_list).push_back(stmt);
		 init_code = ocg->StmtListAppend(init_code, new CG_roseRepr(new_list));
		 */
		//	}
	}
	/*	for (std::set<int>::iterator it = ptrs.begin(); it != ptrs.end();
	 it++) {
	 ptr_variables.erase(ptr_variables.begin() + *it);
	 }
	 */

	bool done = false;

	for (int iter = 0; iter < ordered_cudaized_stmts.size(); iter++) {

		CG_outputRepr* repr;
		std::vector<VarDefs> arrayVars;
		std::vector<VarDefs> localScopedVars;

		std::vector<IR_ArrayRef *> ro_refs;
		std::vector<IR_ArrayRef *> wo_refs;
		std::set<std::string> uniqueRefs;
		std::set<std::string> uniqueWoRefs;
		std::set<const SgVariableSymbol *> syms;
		std::set<const SgVariableSymbol *> psyms;
		std::set<const SgVariableSymbol *> pdSyms;
		SgStatementPtrList* replacement_list = new SgStatementPtrList;
		SgNode *code_temp = getCode(2, ordered_cudaized_stmts[iter].first);

		if (code_temp != NULL) {
			std::set<int>::iterator i =
					ordered_cudaized_stmts[iter].first.begin();
			for (; i != ordered_cudaized_stmts[iter].first.end(); i++)
				if (stmt[*i].has_inspector)
					break;

			if (i == ordered_cudaized_stmts[iter].first.end()) {
				if (ordered_cudaized_stmts[iter].second == -1)
					setup_code = ocg->StmtListAppend(setup_code,
							new CG_roseRepr(code_temp));

			} else {

				std::set<IR_ArrayRef *> outer_refs;
				for (std::set<int>::iterator j =
						ordered_cudaized_stmts[iter].first.begin();
						j != ordered_cudaized_stmts[iter].first.end(); j++)
					if (stmt[*j].ir_stmt_node != NULL) {
						std::vector<CG_outputRepr *> loop_refs =
								collect_loop_inductive_and_conditionals(
										stmt[*j].ir_stmt_node);

						for (int i = 0; i < loop_refs.size(); i++) {
							std::vector<IR_ArrayRef *> refs = ir->FindArrayRef(
									loop_refs[i]);

							for (int l = 0; l < refs.size(); l++) {
								std::set<IR_ArrayRef *>::iterator k =
										outer_refs.begin();
								for (; k != outer_refs.end(); k++)
									if ((*k)->name() == refs[l]->name())
										break;
								if (k == outer_refs.end())
									outer_refs.insert(refs[l]);
							}

						}
					}

				//--end

				//CG_outputRepr *function_declaration = outlineCode(new CG_roseRepr(code_before));
				SgBasicBlock* bb = new SgBasicBlock(TRANSFORMATION_FILE_INFO);

				SgFunctionDefinition* insp_defn = new SgFunctionDefinition(
				TRANSFORMATION_FILE_INFO, bb);
				SgFunctionDeclaration* insp_decl_ = new SgFunctionDeclaration(
				TRANSFORMATION_FILE_INFO, SgName(dynamic_cast<IR_roseCode*>(ir)->proc_name_ + "_inspector"),buildFunctionType(buildVoidType(), buildFunctionParameterList()), insp_defn);
				SgFunctionDeclaration* insp_decl = new SgFunctionDeclaration(
				TRANSFORMATION_FILE_INFO, SgName(dynamic_cast<IR_roseCode*>(ir)->proc_name_ + "_inspector"),buildFunctionType(buildVoidType(), buildFunctionParameterList()), insp_defn);

				insp_decl->set_definingDeclaration(insp_decl);
				insp_defn->set_parent(insp_decl);
				bb->set_parent(insp_defn);
				bb->set_endOfConstruct(TRANSFORMATION_FILE_INFO);
				bb->get_endOfConstruct()->set_parent(bb);

				//SgFunctionSymbol* functionSymbol = new SgFunctionSymbol(kernel_decl_);
				//globals->insert_symbol(SgName((char*) cu_kernel_name.c_str()),
				//			functionSymbol);
				SgFunctionSymbol* functionSymbol2 = new SgFunctionSymbol(
						insp_decl);

				globals->insert_symbol(
						SgName(
								dynamic_cast<IR_roseCode *>(ir)->proc_name_
										+ "_inspector"), functionSymbol2);

				insp_decl_->set_parent(globals);

				insp_decl_->set_scope(globals);

				insp_decl_->setForward();

				globals->prepend_declaration(insp_decl_);

				insp_decl->set_endOfConstruct(TRANSFORMATION_FILE_INFO);
				insp_decl->get_endOfConstruct()->set_parent(insp_decl);

				insp_decl->set_parent(globals);
				insp_decl->set_scope(globals);

				insp_decl->get_definition()->set_endOfConstruct(
				TRANSFORMATION_FILE_INFO);
				insp_decl->get_definition()->get_endOfConstruct()->set_parent(
						insp_decl->get_definition());

				globals->append_statement(insp_decl);

				std::set<const SgVariableSymbol *> syms;

				recursiveFindRefs(code_temp, syms, func_definition);

				SgExprListExp* iml = new SgExprListExp();
				for (std::set<const SgVariableSymbol *>::iterator i =
						syms.begin(); i != syms.end(); i++) {

					SgVariableSymbol* temp = parameter_symtab->find_variable(
							SgName((*i)->get_name().getString().c_str()));
					if (temp) {
						iml->append_expression(buildVarRefExp(temp));
						SgInitializedName* id = buildInitializedName(
								(char*) (*i)->get_name().getString().c_str(),
								(*i)->get_type());
						insp_decl->get_parameterList()->append_arg(id);
						insp_decl_->get_parameterList()->append_arg(id);
						id->set_file_info(TRANSFORMATION_FILE_INFO);

						// DQ (9/8/2007): We now test this, so it has to be set explicitly.
						id->set_scope(insp_decl->get_definition());

						// DQ (9/8/2007): Need to add variable symbol to global scope!
						//printf ("Fixing up the symbol table in scope = %p = %s for SgInitializedName = %p = %s \n"$
						SgVariableSymbol *var_symbol = new SgVariableSymbol(id);
						insp_decl->get_definition()->insert_symbol(
								id->get_name(), var_symbol);

					} else if (isSgClassType((*i)->get_type())) {

						SgVariableSymbol *temp = body_symtab->find_variable(
								SgName((*i)->get_name().getString().c_str()));
						;

						assert(temp);
						iml->append_expression(buildVarRefExp(temp));
						SgInitializedName* id = buildInitializedName(
								(char*) (*i)->get_name().getString().c_str(),
								buildReferenceType((temp)->get_type()));
						insp_decl->get_parameterList()->append_arg(id);
						insp_decl_->get_parameterList()->append_arg(id);
						id->set_file_info(TRANSFORMATION_FILE_INFO);

						id->set_scope(insp_decl->get_definition());

						SgVariableSymbol *var_symbol = new SgVariableSymbol(id);
						insp_decl->get_definition()->insert_symbol(
								id->get_name(), var_symbol);

					}

				}

				for (std::set<IR_ArrayRef *>::iterator l = outer_refs.begin();
						l != outer_refs.end(); l++) {
					SgVariableSymbol* temp =
							parameter_symtab->find_variable(
									SgName(
											dynamic_cast<IR_roseArraySymbol *>((*l)->symbol())->vs_->get_name().getString().c_str()));

					if (temp) {

						SgType *t = temp->get_type();

						assert(isSgArrayType(t) || isSgPointerType(t));
						SgType * base=NULL;
						if(isSgArrayType(t)){
						assert(
								!isSgArrayType(
										isSgArrayType(t)->get_base_type()));
						base = isSgArrayType(t)->get_base_type();
						}
						else{

							assert(
										!isSgArrayType(
												isSgPointerType(t)->get_base_type()));

							base = isSgPointerType(t)->get_base_type();
						}


						iml->append_expression(buildVarRefExp(temp));
						SgInitializedName* id = buildInitializedName(
								(char*) (temp)->get_name().getString().c_str(),
								buildPointerType(base));
						insp_decl->get_parameterList()->append_arg(id);
						insp_decl_->get_parameterList()->append_arg(id);
						id->set_file_info(TRANSFORMATION_FILE_INFO);

						id->set_scope(insp_decl->get_definition());

						SgVariableSymbol *var_symbol = new SgVariableSymbol(id);
						insp_decl->get_definition()->insert_symbol(
								id->get_name(), var_symbol);

					}

				}

				for (std::map<std::string, std::pair<std::string, SgNode*> >::iterator macros_it =
						dynamic_cast<IR_roseCode *>(ir)->defined_macros.begin();
						macros_it
								!= dynamic_cast<IR_roseCode *>(ir)->defined_macros.end();
						macros_it++) {
					if (macros_it->second.second != NULL)
						if (isSgPntrArrRefExp(macros_it->second.second))
							if (isSgVarRefExp(
									isSgPntrArrRefExp(macros_it->second.second)->get_lhs_operand()))
								syms.insert(
										isSgVarRefExp(
												isSgPntrArrRefExp(
														macros_it->second.second)->get_lhs_operand())->get_symbol());

				}

				SgNode* swapped_ =
						swapVarReferences(code_temp, syms,
								insp_decl->get_definition()->get_symbol_table(),
								insp_decl->get_definition()->get_body()->get_symbol_table(),
								insp_decl->get_definition()->get_body());
				if (!isSgBasicBlock(swapped_)) {
					appendStatement(isSgStatement(swapped_),
							insp_decl->get_definition()->get_body());
					swapped_->set_parent(
							isSgNode(insp_decl->get_definition()->get_body()));
				} else {
					for (SgStatementPtrList::iterator it = isSgBasicBlock(
							swapped_)->get_statements().begin();
							it
									!= isSgBasicBlock(swapped_)->get_statements().end();
							it++) {
						appendStatement(*it,
								insp_decl->get_definition()->get_body());
						(*it)->set_parent(
								isSgNode(
										insp_decl->get_definition()->get_body()));
					}
				}

				SgFunctionCallExp * insp_func_call = buildFunctionCallExp(
						buildFunctionRefExp(insp_decl), iml);

				SgExprStatement* stmt = buildExprStatement(insp_func_call);

				SgStatementPtrList* tnl3 = new SgStatementPtrList;

				(*tnl3).push_back(stmt);

				//   tree_node_list* tnl = new tree_node_list;
				//   tnl->append(new tree_instr(the_call));
				setup_code = ocg->StmtListAppend(setup_code,
						new CG_roseRepr(tnl3));

			}
		}



		//	const char* gridName = "dimGrid";
		//const char* blockName = "dimBlock";
		char gridName[20];
		char blockName[20];
		sprintf(gridName, "dimGrid%i\0", iter);
		sprintf(blockName, "dimBlock%i\0", iter);

		//TODO: Could allow for array_dims_vars to be a mapping from array
		//references to to variable names that define their length.
		SgVariableSymbol* dim1 = 0;
		SgVariableSymbol* dim2 = 0;

		//Anand: Moved this out of for loop as they only need to be declared once.
		SgName name_cuda_malloc("cudaMalloc");
		SgFunctionDeclaration * decl_cuda_malloc =
				buildNondefiningFunctionDeclaration(name_cuda_malloc,
						buildVoidType(), buildFunctionParameterList(), globals);

		SgName name_cuda_copy("cudaMemcpy");
		SgFunctionDeclaration * decl_cuda_copy =
				buildNondefiningFunctionDeclaration(name_cuda_copy,
						buildVoidType(), buildFunctionParameterList(), globals);

		SgExprListExp* iml = new SgExprListExp();
		SgCastExp* dim_s;

		//Creating Kernel function
		SgBasicBlock* bb = new SgBasicBlock(TRANSFORMATION_FILE_INFO);
		SgFunctionDefinition* kernel_defn = new SgFunctionDefinition(
		TRANSFORMATION_FILE_INFO, bb);
		SgFunctionDeclaration* kernel_decl_;	// = new SgFunctionDeclaration(
		//TRANSFORMATION_FILE_INFO, SgName((char*)cu_kernel_name[ordered_cudaized_stmts[iter].second].c_str()),buildFunctionType(buildVoidType(), buildFunctionParameterList()), kernel_defn);
		SgFunctionDeclaration* kernel_decl; //= new SgFunctionDeclaration(
		//TRANSFORMATION_FILE_INFO, SgName((char*)cu_kernel_name[ordered_cudaized_stmts[iter].second].c_str()),buildFunctionType(buildVoidType(), buildFunctionParameterList()), kernel_defn);

		SgStatementPtrList* scalar_cuda_mallocs_and_cpys =
				new SgStatementPtrList;

		if (ordered_cudaized_stmts[iter].second != -1) {
			CG_outputRepr *stmt_code = new CG_roseRepr(code_temp);
			//for (std::set<int>::iterator j = cudaized.begin(); j != cudaized.end();
			//		j++) {

			std::vector<IR_ArrayRef *> refs = ir->FindArrayRef(stmt_code);

			//std::vector<IR_ArrayRef *> refs = ir->FindArrayRef(stmt[*j].code);

			//Anand --adding support for finding array references in the loop bounds and
			//conditions in addition to array references in the statement body

			//--begin
			/*std::set<IR_ArrayRef *> outer_refs;
			 for (std::set<int>::iterator j =
			 ordered_cudaized_stmts[iter].first.begin();
			 j != ordered_cudaized_stmts[iter].first.end(); j++)
			 if (stmt[*j].ir_stmt_node != NULL) {
			 std::vector<CG_outputRepr *> loop_refs =
			 collect_loop_inductive_and_conditionals(
			 stmt[*j].ir_stmt_node);

			 for (int i = 0; i < loop_refs.size(); i++) {
			 std::vector<IR_ArrayRef *> refs = ir->FindArrayRef(
			 loop_refs[i]);
			 outer_refs.insert(refs.begin(), refs.end());
			 }

			 refs.insert(refs.end(), outer_refs.begin(),
			 outer_refs.end());
			 }
			 */
			//--end
			for (int i = 0; i < refs.size(); i++) {
				//printf("ref %s wo %d\n", static_cast<const char*>(refs[i]->name()), refs[i]->is_write());
				SgVariableSymbol* var = body_symtab->find_variable(
						SgName((char*) refs[i]->name().c_str()));
				SgVariableSymbol* var2 = parameter_symtab->find_variable(
						SgName((char*) refs[i]->name().c_str()));

				//If the array is not a parameter, then it's a local array and we
				//want to recreate it as a stack variable in the kernel as opposed to
				//passing it in.
				if (var != NULL) {
					//anand-- needs modification, if variable is parameter it wont be part of the
					// block's symbol table but the function definition's symbol table
					if (kernel_parameters.find(refs[i]->name())
							== kernel_parameters.end())
						continue;
				}
				if (uniqueRefs.find(refs[i]->name()) == uniqueRefs.end()) {

					uniqueRefs.insert(refs[i]->name());
					if (refs[i]->is_write()) {
						uniqueWoRefs.insert(refs[i]->name());
						wo_refs.push_back(refs[i]);
					} else {
						ro_refs.push_back(refs[i]);
						if (reduced_write_refs.find(refs[i]->name())
								!= reduced_write_refs.end()) {
							uniqueWoRefs.insert(refs[i]->name());
							wo_refs.push_back(refs[i]);
						}
					}
				}
				else{

					bool need_insert=false;

				/*	if(refs[i]->is_write()){
					for(int k =0; k < ro_refs.size(); k++)
						if(ro_refs[k]->name() == refs[i]->name())
							need_insert = true;


					for(int k =0; k < wo_refs.size(); k++)
							if(wo_refs[k]->name() == refs[i]->name())
								need_insert = false;

					if(need_insert){
						uniqueWoRefs.insert(refs[i]->name());
										wo_refs.push_back(refs[i]);

					}
*/
	//				}
	//				else{


						for(int k =0; k < wo_refs.size(); k++)
									if(wo_refs[k]->name() == refs[i]->name())
										need_insert = true;

						for(int k =0; k < ro_refs.size(); k++)
								if(ro_refs[k]->name() == refs[i]->name())
									need_insert = false;
						if(need_insert){
							//uniqueWoRefs.insert(refs[i]->name());
							ro_refs.push_back(refs[i]);

						}

//					}




				}


				if (refs[i]->is_write()
						&& uniqueWoRefs.find(refs[i]->name())
								== uniqueWoRefs.end()) {
					uniqueWoRefs.insert(refs[i]->name());
					wo_refs.push_back(refs[i]);
					//printf("adding %s to wo\n", static_cast<const char*>(refs[i]->name()));
				}
				if (var == NULL)
					pdSyms.insert((const SgVariableSymbol*) var2);
				else
					pdSyms.insert((const SgVariableSymbol*) var);
			}
			//}

			printf("reading from array ");
			for (int i = 0; i < ro_refs.size(); i++)
				printf("'%s' ", ro_refs[i]->name().c_str());
			printf("and writing to array ");
			for (int i = 0; i < wo_refs.size(); i++)
				printf("'%s' ", wo_refs[i]->name().c_str());
			printf("\n");

			/*const char* gridName = "dimGrid";
			 const char* blockName = "dimBlock";

			 //TODO: Could allow for array_dims_vars to be a mapping from array
			 //references to to variable names that define their length.
			 SgVariableSymbol* dim1 = 0;
			 SgVariableSymbol* dim2 = 0;

			 //Anand: Moved this out of for loop as they only need to be declared once.
			 SgName name_cuda_malloc("cudaMalloc");
			 SgFunctionDeclaration * decl_cuda_malloc =
			 buildNondefiningFunctionDeclaration(name_cuda_malloc,
			 buildVoidType(), buildFunctionParameterList(),
			 globals);

			 SgName name_cuda_copy("cudaMemcpy");
			 SgFunctionDeclaration * decl_cuda_copy =
			 buildNondefiningFunctionDeclaration(name_cuda_copy,
			 buildVoidType(), buildFunctionParameterList(),
			 globals);

			 SgExprListExp* iml = new SgExprListExp();
			 SgCastExp* dim_s;

			 //Creating Kernel function
			 SgBasicBlock* bb = new SgBasicBlock(TRANSFORMATION_FILE_INFO);
			 SgFunctionDefinition* kernel_defn = new SgFunctionDefinition(
			 TRANSFORMATION_FILE_INFO, bb);
			 */
			kernel_decl_ = new SgFunctionDeclaration(TRANSFORMATION_FILE_INFO, SgName((char*)cu_kernel_name[ordered_cudaized_stmts[iter].second].c_str()),buildFunctionType(buildVoidType(), buildFunctionParameterList()), kernel_defn);
			kernel_decl = new SgFunctionDeclaration(TRANSFORMATION_FILE_INFO, SgName((char*)cu_kernel_name[ordered_cudaized_stmts[iter].second].c_str()),buildFunctionType(buildVoidType(), buildFunctionParameterList()), kernel_defn);

			//((kernel_decl->get_declarationModifier()).get_storageModifier()).setStatic();

			kernel_decl->set_definingDeclaration(kernel_decl);
			kernel_defn->set_parent(kernel_decl);
			bb->set_parent(kernel_defn);
			bb->set_endOfConstruct(TRANSFORMATION_FILE_INFO);
			bb->get_endOfConstruct()->set_parent(bb);

			//SgFunctionSymbol* functionSymbol = new SgFunctionSymbol(kernel_decl_);
			//globals->insert_symbol(SgName((char*) cu_kernel_name.c_str()),
			//			functionSymbol);
			SgFunctionSymbol* functionSymbol2 = new SgFunctionSymbol(
					kernel_decl);

			globals->insert_symbol(
					SgName(
							(char*) cu_kernel_name[ordered_cudaized_stmts[iter].second].c_str()),
					functionSymbol2);

			kernel_decl_->set_parent(globals);

			kernel_decl_->set_scope(globals);

			kernel_decl_->setForward();

			globals->prepend_declaration(kernel_decl_);

			kernel_decl->set_endOfConstruct(TRANSFORMATION_FILE_INFO);
			kernel_decl->get_endOfConstruct()->set_parent(kernel_decl);

			kernel_decl->set_parent(globals);
			kernel_decl->set_scope(globals);

			kernel_decl->get_definition()->set_endOfConstruct(
			TRANSFORMATION_FILE_INFO);
			kernel_decl->get_definition()->get_endOfConstruct()->set_parent(
					kernel_decl->get_definition());

			globals->append_statement(kernel_decl);
			for (std::set<std::string>::iterator i = kernel_parameters.begin();
					i != kernel_parameters.end(); i++) {
				int j;
				for (j = 0; j < ro_refs.size(); j++)
					if (ro_refs[j]->name() == *i)
						break;
				if (j < ro_refs.size())
					continue;

				for (j = 0; j < wo_refs.size(); j++)
					if (wo_refs[j]->name() == *i)
							break;
					if (j < wo_refs.size())
						continue;

				std::string name = *i;

				std::vector<IR_ScalarRef *> scalar_refs = ir->FindScalarRef(
						stmt_code);
				SgVariableSymbol *var_sym = NULL;
				int k;
				bool found = false;
				for (k = 0; k < scalar_refs.size(); k++)
					if (scalar_refs[k]->name() == name) {

						var_sym =
								dynamic_cast<IR_roseScalarSymbol *>(scalar_refs[k]->symbol())->vs_;
						found = true;
						break;
					}

				if (!found) {
					bool found2 = false;
					for (std::map<std::string, std::pair<std::string, SgNode*> >::iterator macros_it =
							dynamic_cast<IR_roseCode *>(ir)->defined_macros.begin();
							macros_it
									!= dynamic_cast<IR_roseCode *>(ir)->defined_macros.end();
							macros_it++) {
						if (macros_it->second.second != NULL)
							if (isSgPntrArrRefExp(macros_it->second.second))
								if (isSgVarRefExp(
										isSgPntrArrRefExp(macros_it->second.second)->get_lhs_operand())) {

									if (isSgVarRefExp(
											isSgPntrArrRefExp(macros_it->second.second)->get_lhs_operand())->get_symbol()->get_name()
											== *i){

										found2 = true;
									CG_roseRepr * new_ia = new CG_roseRepr(
											isSgPntrArrRefExp(
													macros_it->second.second));
									CG_outputRepr *ia2 = new_ia->clone();
									ro_refs.push_back(
											new IR_roseArrayRef(
													dynamic_cast<IR_roseCode *>(ir),
													isSgPntrArrRefExp(
															static_cast<CG_roseRepr*>(ia2)->GetExpression()),
													0));

									break;
									}

								}
					}

				}

				SgDotExp *struct_ = NULL;
				if (!var_sym) {

					std::map<std::string, std::pair<std::string,SgNode *> >::iterator it =
							dynamic_cast<IR_roseCode *>(ir)->defined_macros.begin();
					for (;
							it
									!= dynamic_cast<IR_roseCode *>(ir)->defined_macros.end();
							it++)
						if (it->second.second->unparseToString() == *i) {
							if (isSgDotExp(it->second.second)) {
								struct_ =
										isSgDotExp(
												static_cast<CG_roseRepr *>((new CG_roseRepr(
														isSgDotExp(it->second.second)))->clone())->GetExpression());
								SgVarRefExp *varref = isSgVarRefExp(
										struct_->get_rhs_operand());
								assert(varref != NULL);
								var_sym = varref->get_symbol();
							} else if (isSgVarRefExp(it->second.second))
								var_sym =
										isSgVarRefExp(it->second.second)->get_symbol();

							else
								assert(0);
						}
				}

				if (var_sym != NULL) {

					SgType* t = var_sym->get_type();
					if (name.find(".") != std::string::npos) {

						if (found)
							scalars_within_structs.insert(
									std::pair<std::string, SgDotExp*>(name,
											dynamic_cast<IR_roseScalarSymbol *>(scalar_refs[k]->symbol())->scalar_within_struct));
						else {
							assert(struct_);

							scalars_within_structs.insert(
									std::pair<std::string, SgDotExp*>(name,
											struct_));
						}

						int pos = name.find(".");
						name.replace(pos, 1, "_");
					}
					SgVariableSymbol* dvs = body_symtab->find_variable(
							SgName((char*) name.c_str()));

					SgVariableSymbol* dvs2 = parameter_symtab->find_variable(
											SgName((char*) name.c_str()));

					if (dvs == NULL && dvs2 == NULL) {
						SgVariableDeclaration* defn = buildVariableDeclaration(
								SgName(name.c_str()), t);
						SgInitializedNamePtrList& variables =
								defn->get_variables();
						SgInitializedNamePtrList::const_iterator j =
								variables.begin();
						SgInitializedName* initializedName = *j;
						dvs = new SgVariableSymbol(initializedName);
						prependStatement(defn, func_body);

						dvs->set_parent(body_symtab);
						body_symtab->insert(SgName(name.c_str()), dvs);
					}

					if (struct_) {

						setup_code = ocg->StmtListAppend(setup_code,
								ocg->CreateAssignment(0, ocg->CreateIdent(name),
										new CG_roseRepr(
												isSgExpression(

																struct_))));//);

					}

					//    SgVariableSymbol* dvs = body_symtab->find_variable(SgName(arrayVars[i].name.c_str()));

					//  if(dvs == NULL)
					//      dvs =  parameter_symtab->find_variable(SgName(arrayVars[i].name.c_str()));

					//cudaMalloc args
			/*		if(!found){
					SgExprListExp* args = buildExprListExp();
					args->append_expression(
							buildCastExp(buildAddressOfOp(buildVarRefExp(dvs)),
									buildPointerType(
											buildPointerType(
													buildVoidType()))));
					args->append_expression(buildSizeOfOp(t));

					//    decl_cuda_malloc->get_parameterList()->append_arg
					SgFunctionCallExp *the_call = buildFunctionCallExp(
							buildFunctionRefExp(decl_cuda_malloc), args);

					SgExprStatement* stmt = buildExprStatement(the_call);

					//  (*replacement_list).push_back (stmt);
				//	if (dvs == NULL && dvs2 == NULL)
					//(*scalar_cuda_mallocs_and_cpys).push_back(stmt);
					////setup_code = ocg->StmtListAppend(setup_code, new CG_roseRepr(tnl));
					}


					SgExprListExp * cuda_copy_in_args = buildExprListExp();
					cuda_copy_in_args->append_expression(
							isSgExpression(buildVarRefExp(dvs)));

					if (struct_) {
						if (!found) {
							assert(struct_);
							cuda_copy_in_args->append_expression(
									isSgExpression(buildAddressOfOp(struct_)));
						} else
							cuda_copy_in_args->append_expression(
									isSgExpression(
											buildAddressOfOp(
													dynamic_cast<IR_roseScalarSymbol *>(scalar_refs[k]->symbol())->scalar_within_struct)));

						cuda_copy_in_args->append_expression(buildSizeOfOp(t));
						cuda_copy_in_args->append_expression(
								buildOpaqueVarRefExp("cudaMemcpyHostToDevice",
										globals));

						//                                      cuda_copy_in_args->append_expression(
						//                                              new SgVarRefExp(sourceLocation, )
						//                                      );
						SgFunctionCallExp * cuda_copy_in_func_call =
								buildFunctionCallExp(
										buildFunctionRefExp(decl_cuda_copy),
										cuda_copy_in_args);

						SgExprStatement* stmt_ = buildExprStatement(
								cuda_copy_in_func_call);

						//SgStatementPtrList *tnl_ = new SgStatementPtrList;

						//(*scalar_cuda_mallocs_and_cpys).push_back(stmt_);
					}
					*/
					//setup_code = ocg->StmtListAppend(setup_code, new CG_roseRepr(tnl_));

					SgVariableSymbol* temp = body_symtab->find_variable(
							SgName((char*) name.c_str()));
					if (temp == NULL)
						temp = parameter_symtab->find_variable(
								SgName((char*) name.c_str()));
					iml->append_expression(buildVarRefExp(temp));

					SgInitializedName* id;
					/*if (!found || struct_)
						id = buildInitializedName((char*) name.c_str(),
								buildPointerType(t));
					else
					*/
						id = buildInitializedName((char*) name.c_str(), t);
					kernel_decl->get_parameterList()->append_arg(id);
					kernel_decl_->get_parameterList()->append_arg(id);
					id->set_file_info(TRANSFORMATION_FILE_INFO);

					// DQ (9/8/2007): We now test this, so it has to be set explicitly.
					id->set_scope(kernel_decl->get_definition());

					// DQ (9/8/2007): Need to add variable symbol to global scope!
					//printf ("Fixing up the symbol table in scope = %p = %s for SgInitializedName = %p = %s \n"$
					SgVariableSymbol *var_symbol = new SgVariableSymbol(id);
					kernel_decl->get_definition()->insert_symbol(id->get_name(),
							var_symbol);
				} else {
					SgVariableSymbol* var = body_symtab->find_variable(
							SgName((char*) name.c_str()));
					if (var != NULL && isSgArrayType(var->get_type())
							&& (array_dims[ordered_cudaized_stmts[iter].second].find(
									name.c_str())
									!= array_dims[ordered_cudaized_stmts[iter].second].end())) {
						VarDefs v;

						v.name = name;
						v.type =
								isSgArrayType(var->get_type())->get_base_type();
						v.size_multi_dim = std::vector<CG_outputRepr *>();
						v.tex_mapped = false;
						v.original_name = name;
						std::map<std::string, int>::iterator it =
								array_dims[ordered_cudaized_stmts[iter].second].find(
										name.c_str());
						if (it
								!= array_dims[ordered_cudaized_stmts[iter].second].end())
							v.size_expr =
									static_cast<CG_roseRepr*>(ocg->CreateTimes(
											ocg->CreateInt(it->second),
											new omega::CG_roseRepr(
													isSgExpression(
															buildSizeOfOp(
																	v.type)))))->GetExpression();
						v.in_data = buildVarRefExp(var);
						v.out_data = 0;
						arrayVars.push_back(v);
					}
				}

			}
			if(!done){

				setup_code = ocg->StmtListAppend(setup_code,
													new CG_roseRepr(new_list));
				done = true;
			}
			CG_outputRepr* size;
			for (int i = 0; i < wo_refs.size(); i++) {
				//TODO: Currently assume all arrays are floats of one or two dimentions
				//SgVariableSymbol* outArray = 0;
				//Anand: Modifying the way the symbo, corresponding to the array ref is
				//extracted
				size = NULL;
				SgVariableSymbol *outArray =
						dynamic_cast<IR_roseArraySymbol *>(wo_refs[i]->symbol())->vs_;

				std::string name = wo_refs[i]->name();

				//outArray = body_symtab->find_variable(SgName((char*) name.c_str()));
				int size_n_d;
				//if (outArray == NULL)
				//	outArray = parameter_symtab->find_variable(
				//			SgName((char*) name.c_str()));

				VarDefs v;
				v.type = NULL;
				v.size_multi_dim = std::vector<CG_outputRepr *>();
				char buf[32];
				snprintf(buf, 32, "devO%dPtr", i + 1);
				v.name = buf;
				//If our input array is 2D (non-linearized), we want the actual
				//dimentions of the array

				if (isSgPointerType(outArray->get_type())) {
					if (isSgArrayType(
							isSgNode(
									isSgPointerType(outArray->get_type())->get_base_type()))) {
						//	v.type = ((array_type *)(((ptr_type *)(outArray->type()))->ref_type()))->elem_type();
						SgType* t =
								isSgPointerType(outArray->get_type())->get_base_type();
						/*   SgExprListExp* dimList = t->get_dim_info();
						 SgExpressionPtrList::iterator j= dimList->get_expressions().begin();
						 SgExpression* expr=NULL;
						 for (; j != dimList->get_expressions().end(); j++)
						 expr = *j;
						 */
						while (isSgArrayType(t))
							t = isSgArrayType(t)->get_base_type();

						if (!isSgType(t)) {
							char buf[1024];
							sprintf(buf,
									"CudaizeCodeGen: Array type undetected!");
							throw std::runtime_error(buf);

						}

						v.type = t;
					} else
						v.type =
								isSgPointerType(outArray->get_type())->get_base_type();
				} else if (isSgArrayType(outArray->get_type())) {
					if (isSgArrayType(
							isSgNode(
									isSgArrayType(outArray->get_type())->get_base_type()))) {
						//	v.type = ((array_type *)(((ptr_type *)(outArray->type()))->ref_type()))->elem_type();
						SgType* t =
								isSgArrayType(outArray->get_type())->get_base_type();
						/*   SgExprListExp* dimList = t->get_dim_info();
						 SgExpressionPtrList::iterator j= dimList->get_expressions().begin();
						 SgExpression* expr=NULL;
						 for (; j != dimList->get_expressions().end(); j++)
						 expr = *j;
						 */
						while (isSgArrayType(t))
							t = isSgArrayType(t)->get_base_type();

						if (!isSgType(t)) {
							char buf[1024];
							sprintf(buf,
									"CudaizeCodeGen: Array type undetected!");
							throw std::runtime_error(buf);

						}

						v.type = t;
					} else
						v.type =
								isSgArrayType(outArray->get_type())->get_base_type();
				} else
					v.type = buildFloatType();
				v.tex_mapped = false;

				if (name.find(".") != std::string::npos) {
					int pos = name.find(".");
					v.original_name = name;
					name.replace(pos, 1, "_");

					arrays_within_structs.insert(
							std::pair<std::string, IR_ArrayRef*>(name,
									wo_refs[i]));
				} else
					v.original_name = wo_refs[i]->name();
				//Size of the array = dim1 * dim2 * num bytes of our array type

				//Lookup in array_dims
				std::map<std::string, int>::iterator it =
						array_dims[ordered_cudaized_stmts[iter].second].find(
								name.c_str());
				if (isSgPointerType(outArray->get_type())
						&& isSgArrayType(
								isSgNode(
										isSgPointerType(outArray->get_type())->get_base_type()))) {
					SgType* t =
							isSgPointerType(outArray->get_type())->get_base_type();
					/*   SgExprListExp* dimList = t->get_dim_info();
					 SgExpressionPtrList::iterator j= dimList->get_expressions().begin();
					 SgExpression* expr=NULL;
					 for (; j != dimList->get_expressions().end(); j++)
					 expr = *j;
					 */
					/*	if (isSgIntVal(isSgArrayType(t)->get_index()))
					 size_n_d = (int) (isSgIntVal(
					 isSgArrayType(t)->get_index())->get_value());
					 else if (isSgUnsignedIntVal(isSgArrayType(t)->get_index()))
					 size_n_d = (int) (isSgUnsignedIntVal(
					 isSgArrayType(t)->get_index())->get_value());
					 else if (isSgUnsignedLongVal(isSgArrayType(t)->get_index()))
					 size_n_d = (int) (isSgUnsignedLongVal(
					 isSgArrayType(t)->get_index())->get_value());
					 */

					if (it
							!= array_dims[ordered_cudaized_stmts[iter].second].end())
						size_n_d = it->second;
					else
						size_n_d = 1;
					while (isSgArrayType(t)) {
						int dim;
						if (isSgIntVal(isSgArrayType(t)->get_index()))
							dim =
									(int) (isSgIntVal(
											isSgArrayType(t)->get_index())->get_value());
						else if (isSgUnsignedIntVal(
								isSgArrayType(t)->get_index()))
							dim =
									(int) (isSgUnsignedIntVal(
											isSgArrayType(t)->get_index())->get_value());
						else if (isSgUnsignedLongVal(
								isSgArrayType(t)->get_index()))
							dim =
									(int) (isSgUnsignedLongVal(
											isSgArrayType(t)->get_index())->get_value());
						size_n_d *= dim;
						v.size_multi_dim.push_back(ocg->CreateInt(dim));
						t = isSgArrayType(t)->get_base_type();
					}
					//v.size_2d = (int) (isSgIntVal(t->get_index())->get_value());

					printf(
							"Detected Multi-dimensional array sized of %d for %s\n",
							size_n_d, (char*) wo_refs[i]->name().c_str());
					size = ocg->CreateInt(size_n_d);
				} else if (isSgArrayType(outArray->get_type())
						&& isSgArrayType(
								isSgNode(
										isSgArrayType(outArray->get_type())->get_base_type()))) {
					SgType* t = outArray->get_type();
					/*   SgExprListExp* dimList = t->get_dim_info();
					 SgExpressionPtrList::iterator j= dimList->get_expressions().begin();
					 SgExpression* expr=NULL;
					 for (; j != dimList->get_expressions().end(); j++)
					 expr = *j;
					 */

					if (isSgIntVal(isSgArrayType(t)->get_index()))
						size_n_d = (int) (isSgIntVal(
								isSgArrayType(t)->get_index())->get_value());
					else if (isSgUnsignedIntVal(isSgArrayType(t)->get_index()))
						size_n_d = (int) (isSgUnsignedIntVal(
								isSgArrayType(t)->get_index())->get_value());
					else if (isSgUnsignedLongVal(isSgArrayType(t)->get_index()))
						size_n_d = (int) (isSgUnsignedLongVal(
								isSgArrayType(t)->get_index())->get_value());
					t = isSgArrayType(t)->get_base_type();

					while (isSgArrayType(t)) {
						int dim;
						if (isSgIntVal(isSgArrayType(t)->get_index()))
							dim =
									(int) (isSgIntVal(
											isSgArrayType(t)->get_index())->get_value());
						else if (isSgUnsignedIntVal(
								isSgArrayType(t)->get_index()))
							dim =
									(int) (isSgUnsignedIntVal(
											isSgArrayType(t)->get_index())->get_value());
						else if (isSgUnsignedLongVal(
								isSgArrayType(t)->get_index()))
							dim =
									(int) (isSgUnsignedLongVal(
											isSgArrayType(t)->get_index())->get_value());
						size_n_d *= dim;
						v.size_multi_dim.push_back(ocg->CreateInt(dim));
						t = isSgArrayType(t)->get_base_type();
					}

					//v.size_2d = (int) (isSgIntVal(t->get_index())->get_value());

					printf(
							"Detected Multi-Dimensional array sized of %d for %s\n",
							size_n_d, (char*) wo_refs[i]->name().c_str());
					size = ocg->CreateInt(size_n_d);
				} else if (it
						!= array_dims[ordered_cudaized_stmts[iter].second].end()) {
					int ref_size = it->second;
					//size =
					//		ocg->CreateInt(
					//				isSgIntVal(
					//						isSgArrayType(outArray->get_type())->get_index())->get_value());
					//v.size_2d = isSgArrayType(outArray->get_type())->get_rank();
					//v.var_ref_size = ref_size;
					size = ocg->CreateInt(ref_size);

				} else if (kernel_parameters.find(wo_refs[i]->name())
						!= kernel_parameters.end()) {
					int index;
					for (index = 0; index < ptr_variables.size(); index++)
						if (ptr_variables[index]->name() == wo_refs[i]->name())
							break;
					if (index == ptr_variables.size())
						throw loop_error(
								"pointer variable not found in cudaize codegen!");
					size = ptr_variables[index]->size(0)->clone();
					for (int s = 1; s < ptr_variables[index]->n_dim(); s++) {
						size = ocg->CreateTimes(size,
								ptr_variables[index]->size(s)->clone());
						v.size_multi_dim.push_back(
								ptr_variables[index]->size(s)->clone());
					}
					if (ptr_variables[index]->elem_type() == IR_CONSTANT_FLOAT)
						v.type = buildFloatType();
					else if (ptr_variables[index]->elem_type()
							== IR_CONSTANT_INT)
						v.type = buildIntType();
					else if (ptr_variables[index]->elem_type()
							== IR_CONSTANT_SHORT)
						v.type = buildUnsignedShortType();

					else
						throw loop_error(
								"ptr base type unidentified in cudaize codegen");

				}

				else {
					if (dim1) {
						size = ocg->CreateTimes(
								new CG_roseRepr(
										isSgExpression(buildVarRefExp(dim1))),
								new CG_roseRepr(
										isSgExpression(buildVarRefExp(dim2))));
					} else {
						char buf[1024];
						sprintf(buf,
								"CudaizeCodeGen: Array reference %s does not have a "
										"detectable size or specififed dimentions",
								name.c_str());
						throw std::runtime_error(buf);
					}
				}

				v.size_expr =
						static_cast<CG_roseRepr*>(ocg->CreateTimes(size,
								new omega::CG_roseRepr(
										isSgExpression(buildSizeOfOp(v.type)))))->GetExpression();

				v.in_data = 0;
				v.out_data = outArray;
				//Check for in ro_refs and remove it at this point
				std::vector<IR_ArrayRef *>::iterator it_;
				for (it_ = ro_refs.begin(); it_ != ro_refs.end(); it_++) {
					if ((*it_)->name() == wo_refs[i]->name()) {
						break;
					}
				}
				if (it_ != ro_refs.end()) {
					v.in_data = buildVarRefExp(outArray);
					ro_refs.erase(it_);
				}

				arrayVars.push_back(v);

			}

			//protonu-- assuming that all texture mapped memories were originally read only mems
			//there should be safety checks for that, will implement those later

			for (int i = 0; i < ro_refs.size(); i++) {
				size = NULL;
				SgVariableSymbol* inArray =
						dynamic_cast<IR_roseArraySymbol *>(ro_refs[i]->symbol())->vs_;
				std::string name = ro_refs[i]->name();
				bool found_ptr = false;
				//Anand: commenting out below in favor of using rose wrapper routines in ir_rose.cc

				//inArray = body_symtab->find_variable(SgName((char*) name.c_str()));
				//if (inArray == NULL)
				//	inArray = parameter_symtab->find_variable(
				//			SgName((char*) name.c_str()));

				VarDefs v;
				v.type = NULL;
				v.size_multi_dim = std::vector<CG_outputRepr*>();
				char buf[32];
				snprintf(buf, 32, "devI%dPtr", i + 1);
				v.name = buf;
				v.in_data = NULL;
				int size_n_d;
				if ((kernel_parameters.find(name) == kernel_parameters.end())
						&& isSgPointerType(inArray->get_type())) {
					if (isSgArrayType(
							isSgNode(
									isSgPointerType(inArray->get_type())->get_base_type()))) {

						SgType* t =
								isSgPointerType(inArray->get_type())->get_base_type();

						while (isSgArrayType(t))
							t = isSgArrayType(t)->get_base_type();

						if (!isSgType(t)) {
							char buf[1024];
							std::vector<IR_ArrayRef *> refs = ir->FindArrayRef(
									stmt_code);
							sprintf(buf,
									"CudaizeCodeGen: Array type undetected!");
							throw std::runtime_error(buf);

						}
						v.type = t;
					}

					else{

						if(isSgPointerType(inArray->get_type()))

						v.type =
									isSgPointerType(inArray->get_type())->get_base_type();


						for (int in = 0; in < ptr_variables.size(); in++)
						     if (ptr_variables[in]->name() == ro_refs[i]->name())
						     {
						    		size = ptr_variables[in]->size(0)->clone();

									if (ptr_variables[in]->elem_type() == IR_CONSTANT_FLOAT)
											v.type = buildFloatType();
										else if (ptr_variables[in]->elem_type()
												== IR_CONSTANT_INT)
											v.type = buildIntType();
										else if (ptr_variables[in]->elem_type()
												== IR_CONSTANT_SHORT)
											v.type = buildUnsignedShortType();
										else
											throw loop_error(
													"ptr base type unidentified in cudaize codegen");
						     }



					}
				} else if ((kernel_parameters.find(name)
						== kernel_parameters.end())
						&& (isSgArrayType(inArray->get_type()))) {
					if (isSgArrayType(
							isSgNode(
									isSgArrayType(inArray->get_type())->get_base_type()))) {

						SgType* t = inArray->get_type();
						while (isSgArrayType(t))
							t = isSgArrayType(t)->get_base_type();

						if (!isSgType(t)) {
							char buf[1024];
							sprintf(buf,
									"CudaizeCodeGen: Array type undetected!");
							throw std::runtime_error(buf);

						}
						v.type = t;
					} else
						v.type =
								isSgArrayType(inArray->get_type())->get_base_type();
				}

				else if (kernel_parameters.find(name)
						== kernel_parameters.end())
					v.type = buildFloatType();

				v.tex_mapped = false;
				if (name.find(".") != std::string::npos) {
					int pos = name.find(".");
					v.original_name = name;
					name.replace(pos, 1, "_");

					if(ro_refs[i]->symbol()->elem_type() == IR_CONSTANT_INT)
						v.type = buildIntType();
					else if(ro_refs[i]->symbol()->elem_type() == IR_CONSTANT_FLOAT)
						v.type = buildFloatType();
					else
						throw loop_error("Unknown variable type encountered");

					arrays_within_structs.insert(
							std::pair<std::string, IR_ArrayRef*>(name,
									ro_refs[i]));
					v.in_data = buildVarRefExp(v.original_name);
					v.original_name  = name;
				} else
					v.original_name = ro_refs[i]->name();

				//Size of the array = dim1 * dim2 * num bytes of our array type
				//If our input array is 2D (non-linearized), we want the actual
				//dimentions of the array (as it might be less than cu_n
				//CG_outputRepr* size;
				//Lookup in array_dims
				std::map<std::string, int>::iterator it =
						array_dims[ordered_cudaized_stmts[iter].second].find(
								name.c_str());
				int in;
				for (in = 0; in < ptr_variables.size(); in++)
					if (ptr_variables[in]->name() == ro_refs[i]->name())
						break;
				if (in != ptr_variables.size())
					found_ptr = true;
				if (kernel_parameters.find(name) == kernel_parameters.end()
						&& isSgPointerType(inArray->get_type())
						&& isSgArrayType(
								isSgPointerType(inArray->get_type())->get_base_type())) {
					//SgArrayType* t = isSgArrayType(isSgArrayType(inArray->get_type())->get_base_type());
					//v.size_2d = t->get_rank();
					SgType* t =
							isSgPointerType(inArray->get_type())->get_base_type();
					/*   SgExprListExp* dimList = t->get_dim_info();
					 SgExpressionPtrList::iterator j= dimList->get_expressions().begin();
					 SgExpression* expr=NULL;
					 for (; j != dimList->get_expressions().end(); j++)
					 expr = *j;
					 */
					//v.size_2d = 1;
					if (isSgIntVal(isSgArrayType(t)->get_index()))
						size_n_d = (int) (isSgIntVal(
								isSgArrayType(t)->get_index())->get_value());
					else if (isSgUnsignedIntVal(isSgArrayType(t)->get_index()))
						size_n_d = (int) (isSgUnsignedIntVal(
								isSgArrayType(t)->get_index())->get_value());
					else if (isSgUnsignedLongVal(isSgArrayType(t)->get_index()))
						size_n_d = (int) (isSgUnsignedLongVal(
								isSgArrayType(t)->get_index())->get_value());
					while (isSgArrayType(t)) {
						int dim;
						if (isSgIntVal(isSgArrayType(t)->get_index()))
							dim =
									(int) (isSgIntVal(
											isSgArrayType(t)->get_index())->get_value());
						else if (isSgUnsignedIntVal(
								isSgArrayType(t)->get_index()))
							dim =
									(int) (isSgUnsignedIntVal(
											isSgArrayType(t)->get_index())->get_value());
						else if (isSgUnsignedLongVal(
								isSgArrayType(t)->get_index()))
							dim =
									(int) (isSgUnsignedLongVal(
											isSgArrayType(t)->get_index())->get_value());
						size_n_d *= dim;
						v.size_multi_dim.push_back(ocg->CreateInt(dim));
						t = isSgArrayType(t)->get_base_type();
					}
					printf(
							"Detected Multi-dimensional array sized of %d for %s\n",
							size_n_d, (char*) ro_refs[i]->name().c_str());
					size = ocg->CreateInt(size_n_d);
				} else if (kernel_parameters.find(name)
						== kernel_parameters.end()
						&& isSgArrayType(inArray->get_type())
						&& isSgArrayType(
								isSgArrayType(inArray->get_type())->get_base_type())) {
					//SgArrayType* t = isSgArrayType(isSgArrayType(inArray->get_type())->get_base_type());
					//v.size_2d = t->get_rank();
					SgType* t = inArray->get_type();
					/*   SgExprListExp* dimList = t->get_dim_info();
					 SgExpressionPtrList::iterator j= dimList->get_expressions().begin();
					 SgExpression* expr=NULL;
					 for (; j != dimList->get_expressions().end(); j++)
					 expr = *j;
					 */
					if (isSgIntVal(isSgArrayType(t)->get_index()))
						size_n_d = (int) (isSgIntVal(
								isSgArrayType(t)->get_index())->get_value());
					else if (isSgUnsignedIntVal(isSgArrayType(t)->get_index()))
						size_n_d = (int) (isSgUnsignedIntVal(
								isSgArrayType(t)->get_index())->get_value());
					else if (isSgUnsignedLongVal(isSgArrayType(t)->get_index()))
						size_n_d = (int) (isSgUnsignedLongVal(
								isSgArrayType(t)->get_index())->get_value());
					t = isSgArrayType(t)->get_base_type();
					while (isSgArrayType(t)) {
						int dim;
						if (isSgIntVal(isSgArrayType(t)->get_index()))
							dim =
									(int) (isSgIntVal(
											isSgArrayType(t)->get_index())->get_value());
						else if (isSgUnsignedIntVal(
								isSgArrayType(t)->get_index()))
							dim =
									(int) (isSgUnsignedIntVal(
											isSgArrayType(t)->get_index())->get_value());
						else if (isSgUnsignedLongVal(
								isSgArrayType(t)->get_index()))
							dim =
									(int) (isSgUnsignedLongVal(
											isSgArrayType(t)->get_index())->get_value());
						size_n_d *= dim;
						v.size_multi_dim.push_back(ocg->CreateInt(dim));
						t = isSgArrayType(t)->get_base_type();
					}
					printf(
							"Detected Multi-Dimensional array sized of %d for %s\n",
							size_n_d, (char*) ro_refs[i]->name().c_str());
					size = ocg->CreateInt(size_n_d);
				}

				else if (it
						!= array_dims[ordered_cudaized_stmts[iter].second].end()) {
					int ref_size = it->second;
					//				v.var_ref_size = ref_size;
					size = ocg->CreateInt(ref_size);
					if (kernel_parameters.find(ro_refs[i]->name())
											!= kernel_parameters.end()){
						if(ro_refs[i]->symbol()->elem_type()  == IR_CONSTANT_INT)
								v.type = buildIntType();
						else if(ro_refs[i]->symbol()->elem_type()  == IR_CONSTANT_FLOAT)
								v.type = buildFloatType();
						if(!v.in_data)
						v.in_data = buildVarRefExp(inArray);
					}
				} else if (isSgArrayType(inArray->get_type())
						&& kernel_parameters.find(ro_refs[i]->name())
								!= kernel_parameters.end()) {
					SgArrayType *t = isSgArrayType(inArray->get_type());
					if (isSgIntVal(isSgArrayType(t)->get_index()))
						size_n_d = (int) (isSgIntVal(
								isSgArrayType(t)->get_index())->get_value());
					else if (isSgUnsignedIntVal(isSgArrayType(t)->get_index()))
						size_n_d = (int) (isSgUnsignedIntVal(
								isSgArrayType(t)->get_index())->get_value());
					else if (isSgUnsignedLongVal(isSgArrayType(t)->get_index()))
						size_n_d = (int) (isSgUnsignedLongVal(
								isSgArrayType(t)->get_index())->get_value());
					size = ocg->CreateInt(size_n_d);

					ocg->CreatePointer(name);

				} else if (kernel_parameters.find(ro_refs[i]->name())
						!= kernel_parameters.end()) {
					int index;
					for (index = 0; index < ptr_variables.size(); index++)
						if (ptr_variables[index]->name() == ro_refs[i]->name())
							break;
					if (index == ptr_variables.size())
						throw loop_error(
								"pointer variable not found in cudaize codegen!");
					size = ptr_variables[index]->size(0)->clone();
					for (int s = 1; s < ptr_variables[index]->n_dim(); s++) {
						size = ocg->CreateTimes(size,
								ptr_variables[index]->size(s)->clone());
						v.size_multi_dim.push_back(
								ptr_variables[index]->size(s)->clone());
					}
					if (ptr_variables[index]->elem_type() == IR_CONSTANT_FLOAT)
						v.type = buildFloatType();
					else if (ptr_variables[index]->elem_type()
							== IR_CONSTANT_INT)
						v.type = buildIntType();
					else if (ptr_variables[index]->elem_type()
							== IR_CONSTANT_SHORT)
						v.type = buildUnsignedShortType();
					else
						throw loop_error(
								"ptr base type unidentified in cudaize codegen");

					found_ptr = true;
				} else if (isSgPointerType(inArray->get_type()) && !found_ptr) {
					std::string name_ = ro_refs[i]->name();

					int pos = name.find(".");
					if (pos != std::string::npos) {
						name_.replace(pos, 1, "_");
						std::map<std::string, int>::iterator it2 =
								array_dims[ordered_cudaized_stmts[iter].second].find(
										name_.c_str());

						if (it2
								!= array_dims[ordered_cudaized_stmts[iter].second].end()) {
							int ref_size = it2->second;
							//				v.var_ref_size = ref_size;
							size = ocg->CreateInt(ref_size);
						}
					}
				} else {

					if (dim1) {
						size = ocg->CreateTimes(
								new CG_roseRepr(
										isSgExpression(buildVarRefExp(dim1))),
								new CG_roseRepr(
										isSgExpression(buildVarRefExp(dim2))));
					} else if(size == NULL){
						char buf[1024];
						sprintf(buf,
								"CudaizeCodeGen: Array reference %s does not have a "
										"detectable size or specified dimentions",
								name.c_str());
						throw std::runtime_error(buf);
					}
				}
				v.size_expr =
						static_cast<CG_roseRepr*>(ocg->CreateTimes(size,
								new omega::CG_roseRepr(
						isSgExpression(buildSizeOfOp(v.type)))))->GetExpression();

				if(!v.in_data){
				if (found_ptr)
					v.in_data = buildVarRefExp(inArray);
				else if (kernel_parameters.find(ro_refs[i]->name())
						== kernel_parameters.end())
					v.in_data = buildVarRefExp(inArray);
				else
					v.in_data =
							dynamic_cast<IR_roseArraySymbol *>(ro_refs[i]->symbol())->arr_within_struct;
				}

				v.out_data = 0;
				arrayVars.push_back(v);
			}

			if (arrayVars.size() < 2) {
				fprintf(stderr,
						"cudaize error: Did not find two arrays being accessed\n");
				return;
			}
		}

		if (ordered_cudaized_stmts[iter].second != -1) {
			//Add our mallocs (and input array memcpys)
			for (int i = 0; i < arrayVars.size(); i++) {

				SgVariableSymbol* dvs = body_symtab->find_variable(
						SgName((char*) arrayVars[i].name.c_str()));

				if (dvs == NULL) {
					;
					SgVariableDeclaration* defn = buildVariableDeclaration(
							SgName(arrayVars[i].name.c_str()),
							buildPointerType(arrayVars[i].type));
					SgInitializedNamePtrList& variables = defn->get_variables();
					SgInitializedNamePtrList::const_iterator j =
							variables.begin();
					SgInitializedName* initializedName = *j;
					dvs = new SgVariableSymbol(initializedName);
					prependStatement(defn, func_body);

					dvs->set_parent(body_symtab);
					body_symtab->insert(SgName(arrayVars[i].name.c_str()), dvs);
				}
//    SgVariableSymbol* dvs = body_symtab->find_variable(SgName(arrayVars[i].name.c_str()));

				//  if(dvs == NULL)
				//      dvs =  parameter_symtab->find_variable(SgName(arrayVars[i].name.c_str()));

				//cudaMalloc args
				// SgBasicBlock* block = buildBasicBlock();

				SgExprListExp* args = buildExprListExp();
				args->append_expression(
						buildCastExp(buildAddressOfOp(buildVarRefExp(dvs)),
								buildPointerType(
										buildPointerType(buildVoidType()))));
				args->append_expression(arrayVars[i].size_expr);

//    decl_cuda_malloc->get_parameterList()->append_arg
				SgFunctionCallExp *the_call = buildFunctionCallExp(
						buildFunctionRefExp(decl_cuda_malloc), args);

				SgExprStatement* stmt = buildExprStatement(the_call);

				//  (*replacement_list).push_back (stmt);

				SgStatementPtrList* tnl = new SgStatementPtrList;
				(*tnl).push_back(stmt);
				setup_code = ocg->StmtListAppend(setup_code,
						new CG_roseRepr(tnl));
				if (arrayVars[i].in_data) {

					SgExprListExp * cuda_copy_in_args = buildExprListExp();
					cuda_copy_in_args->append_expression(
							isSgExpression(buildVarRefExp(dvs)));
					cuda_copy_in_args->append_expression(
							isSgExpression(arrayVars[i].in_data));
					CG_roseRepr* size_exp = new CG_roseRepr(
							arrayVars[i].size_expr);
					cuda_copy_in_args->append_expression(
							static_cast<CG_roseRepr*>(size_exp->clone())->GetExpression());
					cuda_copy_in_args->append_expression(
							buildOpaqueVarRefExp("cudaMemcpyHostToDevice",
									globals));

//                                      cuda_copy_in_args->append_expression(
//                                              new SgVarRefExp(sourceLocation, )
//                                      );
					SgFunctionCallExp * cuda_copy_in_func_call =
							buildFunctionCallExp(
									buildFunctionRefExp(decl_cuda_copy),
									cuda_copy_in_args);

					SgExprStatement* stmt = buildExprStatement(
							cuda_copy_in_func_call);

					SgStatementPtrList *tnl = new SgStatementPtrList;

					(*tnl).push_back(stmt);

					setup_code = ocg->StmtListAppend(setup_code,
							new CG_roseRepr(tnl));

				}
			}

//Anand: Added the following for passing in scalars to kernel function
			bool kernel_called_in_loop = false;

			CG_outputRepr* outer_loop = get_outer_loops(code_temp,
					kernel_called_in_loop);
			if (kernel_called_in_loop)
				code_temp = get_inner_loops(code_temp);

			//Build dimGrid dim3 variables based on loop dimentions and ti/tj

			CG_outputRepr *body = NULL;

			char blockD1[120];
			char blockD2[120];
			if (dim1) {
				snprintf(blockD1, 120, "%s/%d",
						dim1->get_declaration()->get_name().getString().c_str(),
						cu_tx[ordered_cudaized_stmts[iter].second]);

				snprintf(blockD2, 120, "%s/%d",
						dim2->get_declaration()->get_name().getString().c_str(),
						cu_ty[ordered_cudaized_stmts[iter].second]);
			} else {
				snprintf(blockD1, 120, "%d",
						cu_bx[ordered_cudaized_stmts[iter].second]);

				if (cu_by.size() > 0
						&& cu_by[ordered_cudaized_stmts[iter].second])
					snprintf(blockD2, 120, "%d",
							cu_by[ordered_cudaized_stmts[iter].second]);
				else
					snprintf(blockD2, 120, "%d", 0);
				//snprintf(blockD1, 120, "%d/%d", cu_nx, cu_tx);
				//snprintf(blockD2, 120, "%d/%d", cu_ny, cu_ty);
			}

			SgInitializedName* arg1 = buildInitializedName("i", buildIntType());
			SgInitializedName* arg2 = buildInitializedName("j", buildIntType());
			SgInitializedName* arg3 = buildInitializedName("k", buildIntType());
			SgName type_name("dim3");
			//SgClassSymbol * type_symbol = globalScope->lookup_class_symbol(type_name);

			//ROSE_ASSERT(type_symbol != NULL);

			//SgClassDeclaration * dim3classdecl = isSgClassDeclaration(
			//		type_symbol->get_declaration());

			SgFunctionDeclaration * funcdecl =
					buildNondefiningFunctionDeclaration(SgName("dim3"),
							buildOpaqueType("dim3", globalScope),
							//isSgType(dim3classdecl->get_type()),
							buildFunctionParameterList(arg1, arg2, arg3),
							globalScope);

			repr = NULL;
			if (cu_bx[ordered_cudaized_stmts[iter].second] && cu_by.size() > 0
					&& cu_by[ordered_cudaized_stmts[iter].second])
				repr = ocg->CreateDim3((const char*) gridName,
						ocg->CreateInt(
								cu_bx[ordered_cudaized_stmts[iter].second]),
						ocg->CreateInt(
								cu_by[ordered_cudaized_stmts[iter].second]));
			else if (cu_bx_repr[ordered_cudaized_stmts[iter].second]
					&& cu_by_repr.size() > 0
					&& cu_by_repr[ordered_cudaized_stmts[iter].second])
				repr = ocg->CreateDim3((const char*) gridName,
						cu_bx_repr[ordered_cudaized_stmts[iter].second],
						cu_by_repr[ordered_cudaized_stmts[iter].second]);
			else if (cu_bx_repr[ordered_cudaized_stmts[iter].second])
				repr = ocg->CreateDim3((const char*) gridName,
						cu_bx_repr[ordered_cudaized_stmts[iter].second],
						ocg->CreateInt(1));
			else if (cu_bx[ordered_cudaized_stmts[iter].second])
				repr = ocg->CreateDim3((const char*) gridName,
						ocg->CreateInt(
								cu_bx[ordered_cudaized_stmts[iter].second]),
						ocg->CreateInt(1));
			if (repr != NULL) {
				if (!kernel_called_in_loop)
					setup_code = ocg->StmtListAppend(setup_code, repr);
				else
					body = ocg->StmtListAppend(body, repr);

			}//SgStatementPtrList* dimList else if (cu_bx_repr[ordered_cudaized_stmts[iter].second])

			//for(SgStatementPtrList::iterator it = (*dimList).begin(); it != (*dimList).end(); it++)
			//    (*replacement_list).push_back (*it);

			//  repr = ocg->CreateDim3((const char*)blockName, cu_tx,cu_ty);

			repr = NULL;
			if ((cu_tz.size() > 0
					&& cu_tz[ordered_cudaized_stmts[iter].second] > 1)
					|| (cu_tz_repr.size() > 0
							&& cu_tz_repr[ordered_cudaized_stmts[iter].second])) {

				if (cu_tx[ordered_cudaized_stmts[iter].second]
						&& cu_ty[ordered_cudaized_stmts[iter].second]
						&& cu_tz[ordered_cudaized_stmts[iter].second])
					repr =
							ocg->CreateDim3((char*) blockName,
									ocg->CreateInt(
											cu_tx[ordered_cudaized_stmts[iter].second]),
									ocg->CreateInt(
											cu_ty[ordered_cudaized_stmts[iter].second]),
									ocg->CreateInt(
											cu_tz[ordered_cudaized_stmts[iter].second]));
				else if (cu_tx_repr[ordered_cudaized_stmts[iter].second]
						&& cu_ty_repr[ordered_cudaized_stmts[iter].second]
						&& cu_tz_repr[ordered_cudaized_stmts[iter].second])
					repr = ocg->CreateDim3((char*) blockName,
							cu_tx_repr[ordered_cudaized_stmts[iter].second],
							cu_ty_repr[ordered_cudaized_stmts[iter].second],
							cu_tz_repr[ordered_cudaized_stmts[iter].second]);
				// SgStatementPtrList* dimList = static_cast<CG_roseRepr *>(repr)->GetList();

				// for(SgStatementPtrList::iterator it = (*dimList).begin(); it != (*dimList).end(); it++)
				//    (*replacement_list).push_back (*it);

			} else {
				if (cu_tx[ordered_cudaized_stmts[iter].second]
						&& cu_ty.size() > 0
						&& cu_ty[ordered_cudaized_stmts[iter].second])
					repr =
							ocg->CreateDim3((char*) blockName,
									ocg->CreateInt(
											cu_tx[ordered_cudaized_stmts[iter].second]),
									ocg->CreateInt(
											cu_ty[ordered_cudaized_stmts[iter].second]));
				else if (cu_tx_repr[ordered_cudaized_stmts[iter].second]
						&& cu_ty_repr.size() > 0
						&& cu_ty_repr[ordered_cudaized_stmts[iter].second])
					repr = ocg->CreateDim3((char*) blockName,
							cu_tx_repr[ordered_cudaized_stmts[iter].second],
							cu_ty_repr[ordered_cudaized_stmts[iter].second]);
				else if (cu_tx[ordered_cudaized_stmts[iter].second]) {
					repr = ocg->CreateDim3((char*) blockName,
							ocg->CreateInt(
									cu_tx[ordered_cudaized_stmts[iter].second]),
							ocg->CreateInt(1));

				}
				//SgStatementPtrList* dimList = static_cast<CG_roseRepr *>(repr)->GetList();

				//for(SgStatementPtrList::iterator it = (*dimList).begin(); it != (*dimList).end(); it++)
				//   (*replacement_list).push_back (*it);

			}

			if (repr != NULL) {
				if (!kernel_called_in_loop)
					setup_code = ocg->StmtListAppend(setup_code, repr);
				else
					body = ocg->StmtListAppend(body, repr);

			}

			SgCudaKernelExecConfig* config = new SgCudaKernelExecConfig(
					buildVarRefExp(gridName), buildVarRefExp(blockName),
					NULL,
					NULL);
			//SgCudaKernelExecConfig* config = new SgCudaKernelExecConfig(buildIntVal(cu_bx), , NULL, NULL);

			//setup_code = ocg->StmtListAppend(setup_code,
			//		new CG_roseRepr(scalar_cuda_mallocs_and_cpys));
//

			//if (after.size() > 0)
			//	code_after = getCode(1, after);

			//printf("%s %s\n", static_cast<const char*>(cu_kernel_name), dims);
			for (int i = 0; i < arrayVars.size(); i++)
			//Throw in a type cast if our kernel takes 2D array notation
			//like (float(*) [1024])
					{
				//protonu--throwing in another hack to stop the caller from passing tex mapped
				//vars to the kernel.
				if (arrayVars[i].tex_mapped == true)
					continue;
				if (!(arrayVars[i].size_multi_dim.empty())) {
					//snprintf(dims,120,"(float(*) [%d])%s", arrayVars[i].size_2d,
					//         const_cast<char*>(arrayVars[i].name.c_str()));

					SgType* t = arrayVars[i].type;
					for (int k = arrayVars[i].size_multi_dim.size() - 1; k >= 0;
							k--) {
						t =
								buildArrayType(t,
										static_cast<CG_roseRepr*>(arrayVars[i].size_multi_dim[k])->GetExpression());
					}
					SgVariableSymbol* temp = body_symtab->find_variable(
							SgName((char*) arrayVars[i].name.c_str()));
					if (temp == NULL)
						temp = parameter_symtab->find_variable(
								SgName((char*) arrayVars[i].name.c_str()));

					dim_s = buildCastExp(buildVarRefExp(temp),
							buildPointerType(t), SgCastExp::e_C_style_cast);

					//printf("%d %s\n", i, dims);
					iml->append_expression(dim_s);

					SgInitializedName* id = buildInitializedName(
							(char*) arrayVars[i].original_name.c_str(),
							buildPointerType(t));
					kernel_decl->get_parameterList()->append_arg(id);
					kernel_decl_->get_parameterList()->append_arg(id);
					id->set_file_info(TRANSFORMATION_FILE_INFO);

					// DQ (9/8/2007): We now test this, so it has to be set explicitly.
					id->set_scope(kernel_decl->get_definition());

					// DQ (9/8/2007): Need to add variable symbol to global scope!
					//printf ("Fixing up the symbol table in scope = %p = %s for SgInitializedName = %p = %s \n",globalScope,globalScope->class_name().c_str(),var1_init_name,var1_init_name->get_name().str());
					SgVariableSymbol *var_symbol = new SgVariableSymbol(id);
					kernel_decl->get_definition()->insert_symbol(id->get_name(),
							var_symbol);

					// if(kernel_decl->get_definition()->get_symbol_table()->find((const) id) == NULL)

				} else {
					//printf("%d %s\n", i, static_cast<const char*>(arrayVars[i].name));
					SgVariableSymbol* temp = body_symtab->find_variable(
							SgName((char*) arrayVars[i].name.c_str()));
					if (temp == NULL)
						temp = parameter_symtab->find_variable(
								SgName((char*) arrayVars[i].name.c_str()));
					iml->append_expression(buildVarRefExp(temp));
					SgInitializedName* id = buildInitializedName(
							(char*) arrayVars[i].original_name.c_str(),
							buildPointerType(arrayVars[i].type));
					kernel_decl->get_parameterList()->append_arg(id);
					kernel_decl_->get_parameterList()->append_arg(id);
					id->set_file_info(TRANSFORMATION_FILE_INFO);

					// DQ (9/8/2007): We now test this, so it has to be set explicitly.
					id->set_scope(kernel_decl->get_definition());

					// DQ (9/8/2007): Need to add variable symbol to global scope!
					//printf ("Fixing up the symbol table in scope = %p = %s for SgInitializedName = %p = %s \n"$
					SgVariableSymbol *var_symbol = new SgVariableSymbol(id);
					kernel_decl->get_definition()->insert_symbol(id->get_name(),
							var_symbol);

				}

			}
			if (dim1) {
				iml->append_expression(buildVarRefExp(dim1));
				SgInitializedName* id = buildInitializedName(
						dim1->get_name().getString().c_str(), dim1->get_type());
				kernel_decl->get_parameterList()->append_arg(id);

				iml->append_expression(buildVarRefExp(dim2));
				SgInitializedName* id2 = buildInitializedName(
						dim2->get_name().getString().c_str(), dim2->get_type());

				kernel_decl->get_parameterList()->append_arg(id);
				kernel_decl_->get_parameterList()->append_arg(id);
			}

			if (kernel_called_in_loop) {
				std::set<std::string> indices = get_loop_indices(outer_loop);

				for (std::set<std::string>::iterator str = indices.begin();
						str != indices.end(); str++) {
					SgInitializedName* id = buildInitializedName(
							(char*) (*str).c_str(), buildIntType());
					kernel_decl->get_parameterList()->append_arg(id);
					kernel_decl_->get_parameterList()->append_arg(id);
					id->set_file_info(TRANSFORMATION_FILE_INFO);
					iml->append_expression(buildVarRefExp(id));
					// DQ (9/8/2007): We now test this, so it has to be set explicitly.
					id->set_scope(kernel_decl->get_definition());
					SgVariableSymbol *var_symbol = new SgVariableSymbol(id);

					kernel_decl->get_definition()->insert_symbol(id->get_name(),
							var_symbol);

				}

			}

			kernel_decl->get_functionModifier().setCudaKernel();
			kernel_decl_->get_functionModifier().setCudaKernel();
			SgCudaKernelCallExp * cuda_call_site =
                        new SgCudaKernelCallExp(
			TRANSFORMATION_FILE_INFO, buildFunctionRefExp(kernel_decl),iml, NULL, config);

                        //new SgCudaKernelCallExp(
                        //      buildFunctionRefExp(kernel_decl),iml,config);
			SgStatementPtrList *tnl2 = new SgStatementPtrList;

			if (!kernel_called_in_loop)
				(*replacement_list).push_back(
						buildExprStatement(cuda_call_site));
			else {

				body = ocg->StmtListAppend(body,
						new CG_roseRepr(
								isSgNode(buildExprStatement(cuda_call_site))));

				if (SgNode *tmp = dynamic_cast<CG_roseRepr *>(body)->GetCode())
					isSgForStatement(
							dynamic_cast<CG_roseRepr *>(outer_loop)->GetCode())->set_loop_body(
							isSgStatement(tmp));
				else if (SgStatementPtrList *lst =
						dynamic_cast<CG_roseRepr *>(body)->GetList()) {
					SgBasicBlock *bb = buildBasicBlock();

					for (SgStatementPtrList::iterator it = lst->begin();
							it != lst->end(); it++) {
						bb->append_statement(*it);

					}
					isSgForStatement(
							dynamic_cast<CG_roseRepr *>(outer_loop)->GetCode())->set_loop_body(
							isSgStatement(bb));

				}

				(*replacement_list).push_back(
						isSgStatement(
								dynamic_cast<CG_roseRepr *>(outer_loop)->GetCode()));

			}
			//(*scalar_cuda_mallocs_and_cpys).push_back(buildExprStatement(cuda_call_site));
			//setup_code = ocg->StmtListAppend(setup_code,
			//		new CG_roseRepr(scalar_cuda_mallocs_and_cpys));
			setup_code = ocg->StmtListAppend(setup_code,
					new CG_roseRepr(replacement_list));
			//cuda free variables
			for (int i = 0; i < arrayVars.size(); i++) {
				if (arrayVars[i].out_data) {

					SgName name_cuda_copy("cudaMemcpy");
					SgFunctionDeclaration * decl_cuda_copyout =
							buildNondefiningFunctionDeclaration(name_cuda_copy,
									buildVoidType(),
									buildFunctionParameterList(), globals);

					SgExprListExp* args = buildExprListExp();
					SgExprListExp * cuda_copy_out_args = buildExprListExp();
					cuda_copy_out_args->append_expression(
							isSgExpression(
									buildVarRefExp(arrayVars[i].out_data)));
                                        SgVariableSymbol* temp = body_symtab->find_variable(
                                        SgName((char*) arrayVars[i].name.c_str()));
                                        if (temp == NULL)
                                             temp = parameter_symtab->find_variable(
                                                SgName((char*) arrayVars[i].name.c_str()));
                                        assert(temp);
                                          cuda_copy_out_args->append_expression(
                                                                  isSgExpression(buildVarRefExp(temp)));
					CG_roseRepr* size_exp = new CG_roseRepr(
							arrayVars[i].size_expr);
					cuda_copy_out_args->append_expression(
							static_cast<CG_roseRepr*>(size_exp->clone())->GetExpression());
					cuda_copy_out_args->append_expression(
							buildOpaqueVarRefExp("cudaMemcpyDeviceToHost",
									globals));

//                                      cuda_copy_in_args->append_expression(
//                                              new SgVarRefExp(sourceLocation, )
//                                      );
					SgFunctionCallExp * cuda_copy_out_func_call =
							buildFunctionCallExp(
									buildFunctionRefExp(decl_cuda_copyout),
									cuda_copy_out_args);

					SgFunctionCallExp *the_call = buildFunctionCallExp(
							buildFunctionRefExp(decl_cuda_copyout),
							cuda_copy_out_args);

					SgExprStatement* stmt = buildExprStatement(the_call);

					SgStatementPtrList* tnl3 = new SgStatementPtrList;

					(*tnl3).push_back(stmt);

					//   tree_node_list* tnl = new tree_node_list;
					//   tnl->append(new tree_instr(the_call));
					setup_code = ocg->StmtListAppend(setup_code,
							new CG_roseRepr(tnl3));

				}

				SgExprListExp* args3 = buildExprListExp();

				SgVariableSymbol* tmp = body_symtab->find_variable(
						SgName(arrayVars[i].name.c_str()));
				if (tmp == NULL)
					tmp = parameter_symtab->find_variable(
							SgName(arrayVars[i].name.c_str()));

				args3->append_expression(buildVarRefExp(tmp));

				SgFunctionCallExp *the_call2 = buildFunctionCallExp(
						buildFunctionRefExp(decl_cuda_free), args3);

				SgExprStatement* stmt2 = buildExprStatement(the_call2);

				SgStatementPtrList* tnl4 = new SgStatementPtrList;

				(*tnl4).push_back(stmt2);
				//(*replacement_list).push_back (stmt2);

				setup_code = ocg->StmtListAppend(setup_code,
						new CG_roseRepr(tnl4));
			}
			//if (code_after != NULL)
			//	setup_code = ocg->StmtListAppend(setup_code,
			//			new CG_roseRepr(code_after));
			// ---------------
			// BUILD THE KERNEL
			// ---------------

			//Create kernel function body
			//Add Params
			std::map<std::string, SgVariableSymbol*> loop_vars;
			//In-Out arrays
			/*for (int i = 0; i < arrayVars.size(); i++) {


			 // fptr = new_proc_syms->install_type(fptr);
			 std::string name =
			 arrayVars[i].in_data ?
			 arrayVars[i].in_data->get_declaration()->get_name().getString() :
			 arrayVars[i].out_data->get_declaration()->get_name().getString();
			 //SgVariableSymbol* sym = new var_sym(fptr, arrayVars[i].in_data ? arrayVars[i].in_data->name() : arrayVars[i].out_data->name());

			 SgVariableSymbol* sym =
			 kernel_decl->get_definition()->get_symbol_table()->find_variable(
			 (const char*) name.c_str());

			 if (sym != NULL)
			 loop_vars.insert(
			 std::pair<std::string, SgVariableSymbol*>(std::string(name),
			 sym));
			 }
			 */
			//Figure out which loop variables will be our thread and block dimention variables
			std::vector<SgVariableSymbol *> loop_syms;
			//Get our indexes
			std::vector<const char*> indexes; // = get_loop_indexes(code,cu_num_reduce);
			int threadsPos = 0;

			//CG_outputRepr *body = NULL;
			SgFunctionDefinition* func_d = func_definition;
			//std::vector<SgVariableSymbol *> symbols =  recursiveFindRefs(code);

			SgName name_sync("__syncthreads");
			SgFunctionDeclaration * decl_sync =
					buildNondefiningFunctionDeclaration(name_sync,
							buildVoidType(), buildFunctionParameterList(),
							globalScope);

			recursiveFindRefs(code_temp, syms, func_d);

			//SgFunctionDeclaration* func = Outliner::generateFunction (code, (char*)cu_kernel_name.c_str(), syms, pdSyms, psyms, NULL, globalScope);

			if (cu_bx[ordered_cudaized_stmts[iter].second] > 1
					|| cu_bx_repr[ordered_cudaized_stmts[iter].second]) {
				indexes.push_back("bx");
				SgName type_name("blockIdx.x");
				SgClassSymbol * type_symbol = globalScope->lookup_class_symbol(
						type_name);
				SgVariableDeclaration * var_decl = buildVariableDeclaration(
						"bx", buildIntType(),
						NULL,
						isSgScopeStatement(
								kernel_decl->get_definition()->get_body()));
				SgStatementPtrList *tnl = new SgStatementPtrList;
				// (*tnl).push_back(isSgStatement(var_decl));
				appendStatement(var_decl,
						kernel_decl->get_definition()->get_body());

				SgVariableSymbol* bx =
						kernel_decl->get_definition()->get_body()->lookup_variable_symbol(
								SgName("bx"));
				SgStatement* assign =
						isSgStatement(
								buildAssignStatement(buildVarRefExp(bx),
										buildOpaqueVarRefExp("blockIdx.x",
												kernel_decl->get_definition()->get_body())));
				(*tnl).push_back(assign);
				// body = ocg->StmtListAppend(body,
				//                                  new CG_roseRepr(tnl));
				appendStatement(assign,
						kernel_decl->get_definition()->get_body());

			}
			if ((cu_by.size() > 0
					&& cu_by[ordered_cudaized_stmts[iter].second] > 1)
					|| (cu_by_repr.size() > 0
							&& cu_by_repr[ordered_cudaized_stmts[iter].second])) {
				indexes.push_back("by");
				SgName type_name("blockIdx.y");
				SgClassSymbol * type_symbol = globalScope->lookup_class_symbol(
						type_name);
				SgVariableDeclaration * var_decl = buildVariableDeclaration(
						"by", buildIntType(),
						NULL,
						isSgScopeStatement(
								kernel_decl->get_definition()->get_body()));
				// SgStatementPtrList *tnl = new SgStatementPtrList;
				// (*tnl).push_back(isSgStatement(var_decl));
				appendStatement(var_decl,
						kernel_decl->get_definition()->get_body());

				SgVariableSymbol* by =
						kernel_decl->get_definition()->get_body()->lookup_variable_symbol(
								SgName("by"));
				SgStatement* assign =
						isSgStatement(
								buildAssignStatement(buildVarRefExp(by),
										buildOpaqueVarRefExp("blockIdx.y",
												kernel_decl->get_definition()->get_body())));
				//(*tnl).push_back(assign);
				// body = ocg->StmtListAppend(body,
				//                                  new CG_roseRepr(tnl));
				appendStatement(assign,
						kernel_decl->get_definition()->get_body());

			}
			if (cu_tx_repr[ordered_cudaized_stmts[iter].second]
					|| cu_tx[ordered_cudaized_stmts[iter].second] > 1) {
				threadsPos = indexes.size();
				indexes.push_back("tx");
				SgName type_name("threadIdx.x");
				SgClassSymbol * type_symbol = globalScope->lookup_class_symbol(
						type_name);
				SgVariableDeclaration * var_decl = buildVariableDeclaration(
						"tx", buildIntType(),
						NULL,
						isSgScopeStatement(
								kernel_decl->get_definition()->get_body()));
				//  SgStatementPtrList *tnl = new SgStatementPtrList;
				//  (*tnl).push_back(isSgStatement(var_decl));
				appendStatement(var_decl,
						kernel_decl->get_definition()->get_body());

				SgVariableSymbol* tx =
						kernel_decl->get_definition()->get_body()->lookup_variable_symbol(
								SgName("tx"));
				SgStatement* assign =
						isSgStatement(
								buildAssignStatement(buildVarRefExp(tx),
										buildOpaqueVarRefExp("threadIdx.x",
												kernel_decl->get_definition()->get_body())));
				//(*tnl).push_back(assign);
				// body = ocg->StmtListAppend(body,
				//                                  new CG_roseRepr(tnl));
				appendStatement(assign,
						kernel_decl->get_definition()->get_body());

			}
			if ((cu_ty_repr.size() > 0
					&& cu_ty_repr[ordered_cudaized_stmts[iter].second])
					|| (cu_ty.size() > 0
							&& cu_ty[ordered_cudaized_stmts[iter].second] > 1)) {
				indexes.push_back("ty");
				SgName type_name("threadIdx.y");
				SgClassSymbol * type_symbol = globalScope->lookup_class_symbol(
						type_name);
				SgVariableDeclaration * var_decl = buildVariableDeclaration(
						"ty", buildIntType(),
						NULL,
						isSgScopeStatement(
								kernel_decl->get_definition()->get_body()));
				appendStatement(var_decl,
						kernel_decl->get_definition()->get_body());

				// SgStatementPtrList *tnl = new SgStatementPtrList;
				// (*tnl).push_back(isSgStatement(var_decl));
				SgVariableSymbol* ty =
						kernel_decl->get_definition()->get_body()->lookup_variable_symbol(
								SgName("ty"));
				SgStatement* assign =
						isSgStatement(
								buildAssignStatement(buildVarRefExp(ty),
										buildOpaqueVarRefExp("threadIdx.y",
												kernel_decl->get_definition()->get_body())));
				// (*tnl).push_back(assign);
				//  body = ocg->StmtListAppend(body,
				//                                   new CG_roseRepr(tnl));
				appendStatement(assign,
						kernel_decl->get_definition()->get_body());

			}
			if ((cu_tz_repr.size() > 0
					&& cu_tz_repr[ordered_cudaized_stmts[iter].second])
					|| (cu_tz.size() > 0
							&& cu_tz[ordered_cudaized_stmts[iter].second] > 1)) {
				indexes.push_back("tz");
				SgName type_name("threadIdx.z");
				SgClassSymbol * type_symbol = globalScope->lookup_class_symbol(
						type_name);
				SgVariableDeclaration * var_decl = buildVariableDeclaration(
						"tz", buildIntType(),
						NULL,
						isSgScopeStatement(
								kernel_decl->get_definition()->get_body()));
				//   SgStatementPtrList *tnl = new SgStatementPtrList;
				//   (*tnl).push_back(isSgStatement(var_decl));
				appendStatement(var_decl,
						kernel_decl->get_definition()->get_body());

				SgVariableSymbol* tz =
						kernel_decl->get_definition()->get_body()->lookup_variable_symbol(
								"tz");
				SgStatement* assign =
						isSgStatement(
								buildAssignStatement(buildVarRefExp(tz),
										buildOpaqueVarRefExp("threadIdx.z",
												kernel_decl->get_definition()->get_body())));
				//    (*tnl).push_back(assign);
				//     body = ocg->StmtListAppend(body,
				//                                      new CG_roseRepr(tnl));
				appendStatement(assign,
						kernel_decl->get_definition()->get_body());

			}

			std::map<std::string, SgVariableSymbol*> loop_idxs; //map from idx names to their new syms

			CG_outputRepr *stmt_code_ = new CG_roseRepr(code_temp);

			std::vector<IR_ArrayRef *> refs_ = ir->FindArrayRef(stmt_code_);

			for (int i = 0; i < refs_.size(); i++)
				if (kernel_parameters.find(refs_[i]->name())
						!= kernel_parameters.end()) {
					std::string name = refs_[i]->name();
					int pos = name.find(".");
					if (pos != std::string::npos) {
						name.replace(pos, 1, "_");

						SgVariableSymbol* temp = body_symtab->find_variable(
								name);
						if (temp == NULL)
							temp = parameter_symtab->find_variable(
									SgName(name));
						assert(temp);
						SgVarRefExp * new_exp = buildVarRefExp(temp);

						SgPntrArrRefExp* parent =
								dynamic_cast<IR_roseArrayRef *>(refs_[i])->ia_;

						SgExpression *old = parent->get_lhs_operand();

						assert(isSgDotExp(old));

						parent->replace_expression(old, new_exp);
					}
				}

		/*	for (std::map<std::string, SgDotExp *>::iterator i =
					scalars_within_structs.begin();
					i != scalars_within_structs.end(); i++) {
				std::string name = i->first;
				int pos = name.find(".");
				name.replace(pos, 1, "_");
				std::string j;
				SgVariableSymbol* temp = body_symtab->find_variable(name);
				if (temp == NULL)
					temp = parameter_symtab->find_variable(SgName(name));
				assert(temp);
				SgVarRefExp * new_exp = buildVarRefExp(temp);

				SgExprListExp* args3 = buildExprListExp();

				args3->append_expression(new_exp);

				SgFunctionCallExp *the_call2 = buildFunctionCallExp(
						buildFunctionRefExp(decl_cuda_free), args3);

				SgExprStatement* stmt2 = buildExprStatement(the_call2);

				SgStatementPtrList* tnl4 = new SgStatementPtrList;

				(*tnl4).push_back(stmt2);
				//(*replacement_list).push_back (stmt2);

				setup_code = ocg->StmtListAppend(setup_code,
						new CG_roseRepr(tnl4));

			}
*/
			SgNode* swapped_ =
					swapVarReferences(code_temp, syms,
							kernel_decl->get_definition()->get_symbol_table(),
							kernel_decl->get_definition()->get_body()->get_symbol_table(),
							kernel_decl->get_definition()->get_body());

			//std::cout << swapped_->unparseToString() << std::endl << std::endl;

			SgNode *swapped =
					recursiveFindReplacePreferedIdxs(swapped_,
							kernel_decl->get_definition()->get_body()->get_symbol_table(),
							kernel_decl->get_definition()->get_symbol_table(),
							kernel_decl->get_definition()->get_body(),
							loop_idxs, globalScope); //in-place swapping
			//swapped->print();
			if (!isSgBasicBlock(swapped)) {
				appendStatement(isSgStatement(swapped),
						kernel_decl->get_definition()->get_body());
				swapped->set_parent(
						isSgNode(kernel_decl->get_definition()->get_body()));
			} else {

				for (SgStatementPtrList::iterator it =
						isSgBasicBlock(swapped)->get_statements().begin();
						it != isSgBasicBlock(swapped)->get_statements().end();
						it++) {
					appendStatement(*it,
							kernel_decl->get_definition()->get_body());
					(*it)->set_parent(
							isSgNode(
									kernel_decl->get_definition()->get_body()));

				}

			}

			//std::cout<<swapped->unparseToString();
			for (int i = 0; i < indexes.size(); i++) {
				std::vector<SgForStatement*> tfs = findCommentedFors(indexes[i],
						swapped);
				for (int k = 0; k < tfs.size(); k++) {
					printf("replacing for index %s\n", indexes[i]);
					SgNode* newBlock = forReduce(tfs[k], loop_idxs[indexes[i]],
							kernel_decl->get_definition());
					//newBlock->print();
					swap_node_for_node_list(tfs[k], newBlock);
					//printf("AFTER SWAP\n");        newBlock->print();
				}
			}
		}
	}
	//for (std::set<int>::iterator j = cudaized.begin(); j != cudaized.end();
	//		j++) {

	//return swapped;
	/*	SgNode* newBlock = swapped;
	 for (int i = 0; i < indexes.size(); i++) {
	 std::vector<SgForStatement*> tfs = findCommentedFors(indexes[i],
	 swapped);
	 for (int k = 0; k < tfs.size(); k++) {
	 //printf("replacing %p tfs for index %s\n", tfs[k], indexes[i]);
	 newBlock = forReduce(tfs[k], loop_idxs[indexes[i]],
	 kernel_decl->get_definition());
	 //newBlock->print();
	 swap_node_for_node_list(tfs[k], newBlock);

	 //printf("AFTER SWAP\n");        newBlock->print();
	 }
	 }

	 if (!isSgBasicBlock(newBlock)) {
	 appendStatement(isSgStatement(newBlock),
	 kernel_decl->get_definition()->get_body());
	 newBlock->set_parent(
	 isSgNode(kernel_decl->get_definition()->get_body()));
	 } else {

	 for (SgStatementPtrList::iterator it =
	 isSgBasicBlock(newBlock)->get_statements().begin();
	 it != isSgBasicBlock(newBlock)->get_statements().end(); it++) {
	 appendStatement(*it, kernel_decl->get_definition()->get_body());
	 (*it)->set_parent(
	 isSgNode(kernel_decl->get_definition()->get_body()));

	 }

	 }
	 //std::cout<<newBlock->unparseToString();
	 return newBlock;
	 */

	for (std::map<std::string, SgDotExp *>::iterator i =
			scalars_within_structs.begin(); i != scalars_within_structs.end();
			i++) {
		std::string name = i->first;
		int pos = name.find(".");
		name.replace(pos, 1, "_");
		std::string j;
		SgVariableSymbol* temp = body_symtab->find_variable(name);
		if (temp == NULL)
			temp = parameter_symtab->find_variable(SgName(name));
		assert(temp);
		SgVarRefExp * new_exp = buildVarRefExp(temp);

		/*SgExprListExp* args3 = buildExprListExp();

		 args3->append_expression(new_exp);

		 SgFunctionCallExp *the_call2 = buildFunctionCallExp(
		 buildFunctionRefExp(decl_cuda_free), args3);

		 SgExprStatement* stmt2 = buildExprStatement(the_call2);

		 SgStatementPtrList* tnl4 = new SgStatementPtrList;

		 (*tnl4).push_back(stmt2);
		 //(*replacement_list).push_back (stmt2);

		 setup_code = ocg->StmtListAppend(setup_code, new CG_roseRepr(tnl4));
		 */
		bool found = false;
		std::map<std::string, std::pair<std::string, SgNode *> >::iterator it =
				dynamic_cast<IR_roseCode *>(ir)->defined_macros.begin();
		for (; it != dynamic_cast<IR_roseCode *>(ir)->defined_macros.end();
				it++) {
			if (it->second.second->unparseToString() == i->first) {

				found = true;

				break;
			}

		}

		if (!found) {

			assert(false);

		} else {
			j = it->first;
			std::string k = it->second.first;
		//	dynamic_cast<IR_roseCode *>(ir)->defined_macros.erase(it);

			it->second.second = isSgNode(new_exp);
		//ir->CreateDefineMacro(j, k, new CG_roseRepr(isSgNode(buildPointerDerefExp(new_exp))));

		}
	}
}

//Order taking out dummy variables
std::vector<std::string> cleanOrder(std::vector<std::string> idxNames) {
	std::vector<std::string> results;
	for (int j = 0; j < idxNames.size(); j++) {
		if (idxNames[j].length() != 0)
			results.push_back(idxNames[j]);
	}
	return results;
}

//First non-dummy level in ascending order
int LoopCuda::nonDummyLevel(int stmt, int level) {
	//level comes in 1-basd and should leave 1-based
	for (int j = level - 1; j < idxNames[stmt].size(); j++) {
		if (idxNames[stmt][j].length() != 0) {
//printf("found non dummy level of %d with idx: %s when searching for %d\n", j+1, (const char*) idxNames[stmt][j], level);
			return j + 1;
		}
	}
	char buf[128];
	sprintf(buf, "%d", level);
	throw std::runtime_error(
			std::string("Unable to find a non-dummy level starting from ")
					+ std::string(buf));
}

int LoopCuda::findCurLevel(int stmt, std::string idx) {
	for (int j = 0; j < idxNames[stmt].size(); j++) {
		if (strcmp(idxNames[stmt][j].c_str(), idx.c_str()) == 0)
			return j + 1;
	}
	throw std::runtime_error(
			std::string("Unable to find index ") + idx
					+ std::string(" in current list of indexes"));
}

void LoopCuda::permute_cuda(int stmt,
		const std::vector<std::string>& curOrder) {
	//printf("curOrder: ");
	//printVs(curOrder);
	//printf("idxNames: ");
	//printVS(idxNames[stmt]);
	std::vector<std::string> cIdxNames = cleanOrder(idxNames[stmt]);
	bool same = true;
	std::vector<int> pi;
	for (int i = 0; i < curOrder.size(); i++) {
		bool found = false;
		for (int j = 0; j < cIdxNames.size(); j++) {
			if (strcmp(cIdxNames[j].c_str(), curOrder[i].c_str()) == 0) {
				pi.push_back(j + 1);
				found = true;
				if (j != i)
					same = false;
			}
		}
		if (!found) {
			throw std::runtime_error(
					"One of the indexes in the permute order where not "
							"found in the current set of indexes.");
		}
	}
	for (int i = curOrder.size(); i < cIdxNames.size(); i++) {
		pi.push_back(i);
	}
	if (same)
		return;
	permute(stmt, pi);

	std::vector<int> lex = getLexicalOrder(stmt);
	std::set<int> active = getStatements(lex, 0);
	//Set old indexe names as new
	for (std::set<int>::iterator j = active.begin(); j != active.end(); j++)
		for (int i = 0; i < curOrder.size(); i++) {
			idxNames[*j][i] = curOrder[i].c_str(); //what about sibling stmts?
		}
}

bool LoopCuda::permute(int stmt_num, const std::vector<int> &pi) {
// check for sanity of parameters
	if (stmt_num >= stmt.size() || stmt_num < 0)
		throw std::invalid_argument("invalid statement " + to_string(stmt_num));
	const int n = stmt[stmt_num].xform.n_out();
	if (pi.size() > (n - 1) / 2)
		throw std::invalid_argument(
				"iteration space dimensionality does not match permute dimensionality");
	int first_level = 0;
	int last_level = 0;
	for (int i = 0; i < pi.size(); i++) {
		if (pi[i] > (n - 1) / 2 || pi[i] <= 0)
			throw std::invalid_argument(
					"invalid loop level " + to_string(pi[i])
							+ " in permuation");

		if (pi[i] != i + 1) {
			if (first_level == 0)
				first_level = i + 1;
			last_level = i + 1;
		}
	}
	if (first_level == 0)
		return true;

	//std::vector<int> lex = getLexicalOrder(stmt_num);
//std::set<int> active = getStatements(lex, 2 * first_level - 2);
//Loop::permute(active, pi);
	Loop::permute(stmt_num, first_level, pi);

}

void LoopCuda::tile_cuda(int stmt_num, int level, int outer_level,
		TilingMethodType method) {
	tile_cuda(stmt_num, level, 1, outer_level, "", "", method);

	std::vector<int> lex = getLexicalOrder(stmt_num);
	std::set<int> active = getStatements(lex, 2 * level - 2);

	// remove unnecessary tiled loop since tile size is one
	/*for (std::set<int>::iterator i = active.begin(); i != active.end(); i++) {
	 int n = stmt[*i].xform.n_out();
	 Relation mapping(n, n-2);
	 F_And *f_root = mapping.add_and();
	 for (int j = 1; j <= 2*level; j++) {
	 EQ_Handle h = f_root->add_EQ();
	 h.update_coef(mapping.input_var(j), -1);
	 }
	 for (int j = 2*level+3; j <= n; j++) {
	 EQ_Handle h = f_root->add_EQ();
	 h.update_coef(mapping.output_var(j-2), 1);
	 h.update_coef(mapping.input_var(j), -1);
	 }
	 stmt[*i].xform = Composition(mapping, stmt[*i].xform);
	 stmt[*i].xform.simplify();

	 for (int j = 0; j < stmt[*i].loop_level.size(); j++)
	 if (j != level-1 &&
	 stmt[*i].loop_level[j].type == LoopLevelTile &&
	 stmt[*i].loop_level[j].payload >= level)
	 stmt[*i].loop_level[j].payload--;

	 stmt[*i].loop_level.erase(stmt[*i].loop_level.begin()+level-1);
	 }
	 */

}
void LoopCuda::tile_cuda(int level, int tile_size, int outer_level,
		std::string idxName, std::string ctrlName, TilingMethodType method) {
	tile_cuda(0, level, tile_size, outer_level, idxName, ctrlName, method);
}

void LoopCuda::flatten_cuda(int stmt_num, std::string idxs,
		std::vector<int> &loop_levels, std::string inspector_name) {

	flatten(stmt_num, idxs, loop_levels, inspector_name);

	/*for (std::vector<int>::iterator i = loop_levels.begin();
			i != loop_levels.end(); i++)

		kernel_parameters.insert(
				inspector_name + "." + stmt[stmt_num].IS.set_var(*i)->name());
*/
//	kernel_parameters.insert(inspector_name + "." "count");
	bool initial_val = false;

	idxNames.push_back(idxNames[stmt_num]);
	idxNames[stmt_num + 1].push_back(idxs);
	stmt_nonSplitLevels.push_back(std::vector<int>());
	//syncs.push_back()

}

void LoopCuda::distribute_cuda(std::vector<int> &stmt_nums, int loop_level) {

	std::set<int> stmts;

	for (int i = 0; i < stmt_nums.size(); i++)
		stmts.insert(stmt_nums[i]);

	distribute(stmts, loop_level);

	//syncs.push_back()

}

void LoopCuda::fuse_cuda(std::vector<int> &stmt_nums, int loop_level) {

	std::set<int> stmts;

	for (int i = 0; i < stmt_nums.size(); i++)
		stmts.insert(stmt_nums[i]);

	fuse(stmts, loop_level);

	//syncs.push_back()

}

void LoopCuda::shift_to_cuda(int stmt_num, int level, int absolute_position) {

	shift_to(stmt_num, level, absolute_position);

}
void LoopCuda::peel_cuda(int stmt_num, int level, int amount) {

	int old_stmt_num = stmt.size();
	peel(stmt_num, level, amount);
	int new_stmt_num = stmt.size();
//For all statements that were in this unroll together, drop index name for unrolled level
	for (int i = old_stmt_num; i < new_stmt_num; i++) {
		idxNames.push_back(idxNames[stmt_num]);
		stmt_nonSplitLevels.push_back(std::vector<int>());
	}
	//syncs.push_back()

}
void LoopCuda::compact_cuda(int stmt_num, std::vector<int> level,
		std::vector<std::string> new_array, int zero,
		std::vector<std::string> data_array, Relation rel, bool max_allocate) {
	int old_num_stmts = num_statement();

	for (int i = 0; i < old_num_stmts; i++) {
		std::vector<std::string> last_index;
		for (int j = 0; j < stmt[i].loop_level.size(); j++)
			if (!stmt[i].loop_level[j].made_dense)
				last_index.push_back(idxNames[i][j]);

		idxNames[i] = last_index;
	}


	compact(stmt_num, level, new_array, zero, data_array, rel, max_allocate);

/*	for (int i = 0; i < old_num_stmts; i++) {
		std::vector<std::string> last_index;
		for (int j = 0; j < stmt[i].loop_level.size(); j++)
			if (!stmt[i].loop_level[j].made_dense)
				last_index.push_back(idxNames[i][j]);

		idxNames[i] = last_index;
	}
*/
	int new_num_stmts = num_statement();
	int i;

	if(!is_single){
	for (i = 0; i < new_num_stmts - old_num_stmts; i++) {
		idxNames.push_back(idxNames[stmt_num]);
		stmt_nonSplitLevels.push_back(std::vector<int>());
	}
	std::vector<std::string> last_index;
	for (int j = 0; j < idxNames[stmt_num].size(); j++)
		last_index.push_back(idxNames[stmt_num][j]);

	idxNames[stmt_num] =last_index;
	}
	else{
	std::vector<std::string> last_index;
	for (int j = 0; j < idxNames[stmt_num].size(); j++)
	   for(int k=0; k < level.size();k++)
		if(j != level[k] -1)	
	    	 last_index.push_back(idxNames[stmt_num][j]);

	idxNames[stmt_num] =last_index;

	for (i = 0; i < new_num_stmts - old_num_stmts; i++) {
		
		idxNames.push_back(idxNames[stmt_num]);
		stmt_nonSplitLevels.push_back(std::vector<int>());
	}
	}
	//stmt_nonSplitLevels.push_back(std::vector<int>());

}

Relation LoopCuda::rel_help(std::vector<int> str_coefs,
		std::vector<std::string> str_vals, int constants, bool eq_or_geq) {

	int num_dim = known.n_set();
	Relation rel(num_dim);
	F_And *f_root = rel.add_and();

	if (eq_or_geq) {
		EQ_Handle h = f_root->add_EQ();

		Free_Var_Decl *g = NULL;

		for (int j = 0; j < str_coefs.size(); j++) {

			for (unsigned i = 0; i < freevar.size(); i++) {
				std::string name = freevar[i]->base_name();
				if (name == str_vals[j]) {
					g = freevar[i];
					break;
				}
			}
			if (g == NULL)
				throw std::invalid_argument(
						"symbolic variable " + str_vals[j] + " not found");
			else {
				if (g->arity() == 0)
					h.update_coef(rel.get_local(g), str_coefs[j]);
				else
					h.update_coef(rel.get_local(g, Input_Tuple), str_coefs[j]);
				//h.update_const(-value);

			}
		}

		h.update_const(constants);
	} else {
		GEQ_Handle h = f_root->add_GEQ();

		Free_Var_Decl *g = NULL;

		for (int j = 0; j < str_coefs.size(); j++) {

			for (unsigned i = 0; i < freevar.size(); i++) {
				std::string name = freevar[i]->base_name();
				if (name == str_vals[j]) {
					g = freevar[i];
					break;
				}
			}
			if (g == NULL)
				throw std::invalid_argument(
						"symbolic variable " + str_vals[j] + " not found");
			else {
				if (g->arity() == 0)
					h.update_coef(rel.get_local(g), str_coefs[j]);
				else
					h.update_coef(rel.get_local(g, Input_Tuple), str_coefs[j]);
				//h.update_const(-value);

			}
		}

		h.update_const(constants);
	}

	return rel;

}
void LoopCuda::addKnown_cuda(std::string var, int value) {

	int num_dim = known.n_set();
	Relation rel(num_dim);
	F_And *f_root = rel.add_and();
	EQ_Handle h = f_root->add_EQ();

	Free_Var_Decl *g = NULL;
	for (unsigned i = 0; i < freevar.size(); i++) {
		std::string name = freevar[i]->base_name();
		if (name == var) {
			g = freevar[i];
			break;
		}
	}
	if (g == NULL)
		throw std::invalid_argument("symbolic variable " + var + " not found");
	else {
		h.update_coef(rel.get_local(g), 1);
		h.update_const(-value);

	}

	addKnown(rel);

}

void LoopCuda::skew_cuda(std::vector<int> stmt_num, int level,
		std::vector<int> coefs) {

	std::set<int> stmts;
	for (int i = 0; i < stmt_num.size(); i++)
		stmts.insert(stmt_num[i]);

	skew(stmts, level, coefs);

}

void LoopCuda::normalize_cuda(int stmt_num, int level) {

	normalize(stmt_num, level);

}
void LoopCuda::make_dense_cuda(int stmt_num, int loop_level,
		std::string new_loop_index) {

	make_dense(stmt_num, loop_level, new_loop_index);

	std::vector<int> lex = getLexicalOrder(stmt_num);
	std::set<int> same_loop = getStatements(lex, 2 * loop_level - 2);
	for (std::set<int>::iterator j = same_loop.begin(); j != same_loop.end();
			j++) {

		std::vector<std::string> new_idx;
		for (int i = 0; i < loop_level - 1; i++)
			new_idx.push_back(idxNames[*j][i]);

		new_idx.push_back(new_loop_index);
		for (int i = loop_level - 1; i < idxNames[*j].size(); i++)
			new_idx.push_back(idxNames[*j][i]);

		idxNames[*j] = new_idx;

	}

}
void LoopCuda::split_with_alignment_cuda(int stmt_num, int level, int alignment,
		int direction) {

	split_with_alignment(stmt_num, level, alignment, direction);
	idxNames.push_back(idxNames[stmt_num]);
	stmt_nonSplitLevels.push_back(std::vector<int>());
}

void LoopCuda::scalar_expand_cuda(int stmt_num, std::vector<int> level,
		std::string arrName, int memory_type, int padding,
		int assign_then_accumulate) {
	int old_num_stmts = num_statement();
	scalar_expand(stmt_num, level, arrName, memory_type, padding,
			assign_then_accumulate);
	int new_num_stmts = num_statement();

	for (int i = 0; i < new_num_stmts - old_num_stmts; i++) {
		idxNames.push_back(idxNames[stmt_num]);
		stmt_nonSplitLevels.push_back(std::vector<int>());
	}
}

void LoopCuda::reduce_cuda(int stmt_num, std::vector<int> level, int param,
		std::string func_name, std::vector<int> seq_level, int bound_level) {

	std::vector<int> cudaized_levels =
			block_and_thread_levels.find(stmt_num)->second;

	reduce(stmt_num, level, param, func_name, seq_level, cudaized_levels,
			bound_level);

}
void LoopCuda::tile_cuda(int stmt, int level, int tile_size, int outer_level,
		std::string idxName, std::string ctrlName, TilingMethodType method) {
	//Do regular tile but then update the index and control loop variable
	//names as well as the idxName to reflect the current state of things.
	//printf("tile(%d,%d,%d,%d)\n", stmt, level, tile_size, outer_level);
	//printf("idxNames before: ");
	//printVS(idxNames[stmt]);

	tile(stmt, level, tile_size, outer_level, method);
	std::vector<int> lex = getLexicalOrder(stmt);
	int dim = 2 * level - 1;
	std::set<int> same_loop = getStatements(lex, dim - 1);

	for (std::set<int>::iterator j = same_loop.begin(); j != same_loop.end();
			j++) {
		if (idxName.size())
			idxNames[*j][level - 1] = idxName.c_str();
		if (tile_size == 1) {
//potentially rearrange loops
			if (outer_level < level) {
				std::string tmp = idxNames[*j][level - 1];
				for (int i = level - 1; i > outer_level - 1; i--) {
					if (i - 1 >= 0)
						idxNames[*j][i] = idxNames[*j][i - 1];
				}
				idxNames[*j][outer_level - 1] = tmp;
			}
//TODO: even with a tile size of one, you need a insert (of a dummy loop)
			idxNames[*j].insert(idxNames[*j].begin() + (level), "");

		} else {
			if (!ctrlName.size())
				throw std::runtime_error("No ctrl loop name for tile");
//insert
			idxNames[*j].insert(idxNames[*j].begin() + (outer_level - 1),
					ctrlName.c_str());
		}
	}

	//printf("idxNames after: ");
	//printVS(idxNames[stmt]);
}

bool LoopCuda::datacopy_privatized_cuda(int stmt_num, int level,
		const std::vector<std::pair<int, std::vector<int> > > &array_ref_nums,
		const std::vector<int> &privatized_levels, bool allow_extra_read,
		int fastest_changing_dimension, int padding_stride,
		int padding_alignment, bool cuda_shared) {
	int old_stmts = stmt.size();
	printf("before datacopy_privatized:\n");
	printIS();
//datacopy_privatized(stmt_num, level, array_name, privatized_levels, allow_extra_read, fastest_changing_dimension, padding_stride, padding_alignment, cuda_shared);
	if (cuda_shared)
		datacopy_privatized(array_ref_nums, level, privatized_levels,
				allow_extra_read, fastest_changing_dimension, padding_stride,
				padding_alignment, 1);
	else
		datacopy_privatized(array_ref_nums, level, privatized_levels,
				allow_extra_read, fastest_changing_dimension, padding_stride,
				padding_alignment, 0);
	printf("after datacopy_privatized:\n");
	printIS();

	//Adjust idxNames to reflect updated state
	std::vector<std::string> cIdxNames = cleanOrder(idxNames[stmt_num]);
	int new_stmts = stmt.size();
	for (int i = old_stmts; i < new_stmts; i++) {
		//printf("fixing up statement %d\n", i);
		std::vector<std::string> idxs;

		//protonu-making sure the vector of nonSplitLevels grows along with
		//the statement structure
		stmt_nonSplitLevels.push_back(std::vector<int>());

		//Indexes up to level will be the same
		for (int j = 0; j < level - 1; j++)
			idxs.push_back(cIdxNames[j]);

		//Expect privatized_levels to match
		for (int j = 0; j < privatized_levels.size(); j++)
			idxs.push_back(cIdxNames[privatized_levels[j] - 1]);//level is one-based

//all further levels should match order they are in originally
		if (privatized_levels.size()) {
			int last_privatized = privatized_levels.back();
			int top_level = last_privatized
					+ (stmt[i].IS.n_set() - idxs.size());
//printf("last privatized_levels: %d top_level: %d\n", last_privatized, top_level);
			for (int j = last_privatized; j < top_level; j++) {
				if (j < cIdxNames.size())
					idxs.push_back(cIdxNames[j]);
				//printf("pushing back: %s\n", (const char*)cIdxNames[j]);
			}
		}
		idxNames.push_back(idxs);
	}
}

bool LoopCuda::datacopy_privatized_cuda(int stmt_num, int level,
		const std::string &array_name,
		const std::vector<int> &privatized_levels, bool allow_extra_read,
		int fastest_changing_dimension, int padding_stride,
		int padding_alignment, bool cuda_shared) {
	int old_stmts = stmt.size();
	printf("before datacopy_privatized:\n");
	printIS();
//datacopy_privatized(stmt_num, level, array_name, privatized_levels, allow_extra_read, fastest_changing_dimension, padding_stride, padding_alignment, cuda_shared);
	if (cuda_shared)
		datacopy_privatized(stmt_num, level, array_name, privatized_levels,
				allow_extra_read, fastest_changing_dimension, padding_stride,
				padding_alignment, 1);
	else
		datacopy_privatized(stmt_num, level, array_name, privatized_levels,
				allow_extra_read, fastest_changing_dimension, padding_stride,
				padding_alignment, 0);
	printf("after datacopy_privatized:\n");
	printIS();

	//Adjust idxNames to reflect updated state
	std::vector<std::string> cIdxNames = cleanOrder(idxNames[stmt_num]);
	int new_stmts = stmt.size();
	for (int i = old_stmts; i < new_stmts; i++) {
		//printf("fixing up statement %d\n", i);
		std::vector<std::string> idxs;

		//protonu-making sure the vector of nonSplitLevels grows along with
		//the statement structure
		stmt_nonSplitLevels.push_back(std::vector<int>());

		//Indexes up to level will be the same
		for (int j = 0; j < level - 1; j++)
			idxs.push_back(cIdxNames[j]);

		//Expect privatized_levels to match
		for (int j = 0; j < privatized_levels.size(); j++)
			idxs.push_back(cIdxNames[privatized_levels[j] - 1]);//level is one-based

//all further levels should match order they are in originally
		if (privatized_levels.size()) {
			int last_privatized = privatized_levels.back();
			int top_level = last_privatized
					+ (stmt[i].IS.n_set() - idxs.size());
//printf("last privatized_levels: %d top_level: %d\n", last_privatized, top_level);
			for (int j = last_privatized; j < top_level; j++) {
				if (j < cIdxNames.size())
					idxs.push_back(cIdxNames[j]);
				//printf("pushing back: %s\n", (const char*)cIdxNames[j]);
			}
		}
		idxNames.push_back(idxs);
	}
}

bool LoopCuda::datacopy_cuda(int stmt_num, int level,
		const std::string &array_name, std::vector<std::string> new_idxs,
		bool allow_extra_read, int fastest_changing_dimension,
		int padding_stride, int padding_alignment, bool cuda_shared) {

	int old_stmts = stmt.size();
//datacopy(stmt_num,level,array_name,allow_extra_read,fastest_changing_dimension,padding_stride,padding_alignment,cuda_shared);
	printf("before datacopy:\n");
	printIS();
	if (cuda_shared)
		datacopy(stmt_num, level, array_name, allow_extra_read,
				fastest_changing_dimension, padding_stride, padding_alignment,
				1);
	else
		datacopy(stmt_num, level, array_name, allow_extra_read,
				fastest_changing_dimension, padding_stride, padding_alignment,
				0);
	printf("after datacopy:\n");
	printIS();
	//Adjust idxNames to reflect updated state
	std::vector<std::string> cIdxNames = cleanOrder(idxNames[stmt_num]);
	int new_stmts = stmt.size();
	for (int i = old_stmts; i < new_stmts; i++) {
		//printf("fixing up statement %d\n", i);
		std::vector<std::string> idxs;

		//protonu-making sure the vector of nonSplitLevels grows along with
		//the statement structure
		stmt_nonSplitLevels.push_back(std::vector<int>());

		//Indexes up to level will be the same
		for (int j = 0; j < level - 1; j++)
			idxs.push_back(cIdxNames[j]);

		//all further levels should get names from new_idxs
		int top_level = stmt[i].IS.n_set();
		//printf("top_level: %d level: %d\n", top_level, level);
		if (new_idxs.size() < top_level - level + 1)
			throw std::runtime_error(
					"Need more new index names for new datacopy loop levels");

		for (int j = level - 1; j < top_level; j++) {
			idxs.push_back(new_idxs[j - level + 1].c_str());
//printf("pushing back: %s\n", new_idxs[j-level+1].c_str());
		}
		idxNames.push_back(idxs);
	}
}

void LoopCuda::ELLify_cuda(int stmt_num, std::vector<std::string> arrays_to_pad,
		int pad_to, bool dense_pad, std::string pos_array_name) {

	int old_stmts = stmt.size();

	ELLify(stmt_num, arrays_to_pad, pad_to, dense_pad, pos_array_name);

	int new_stmts = stmt.size();
	for (int i = 0; i < new_stmts - old_stmts; i++) {
		idxNames.push_back(idxNames[stmt_num]);
		stmt_nonSplitLevels.push_back(std::vector<int>());
	}
}

bool LoopCuda::unroll_cuda(int stmt_num, int level, int unroll_amount) {
	int old_stmts = stmt.size();
	//bool b= unroll(stmt_num, , unroll_amount);

	int dim = 2 * level - 1;
	std::vector<int> lex = getLexicalOrder(stmt_num);
	std::set<int> same_loop = getStatements(lex, dim - 1);

	level = nonDummyLevel(stmt_num, level);
	//printf("unrolling %d at level %d\n", stmt_num,level);

	//protonu--using the new version of unroll, which returns
	//a set of ints instead of a bool. To keep Gabe's logic
	//I'll check the size of the set, if it's 0 return true
	//bool b= unroll(stmt_num, level, unroll_amount);
	std::set<int> b_set = unroll(stmt_num, level, unroll_amount, idxNames);
	bool b = false;
	if (b_set.size() == 0)
		b = true;
	//end--protonu

	//Adjust idxNames to reflect updated state
	std::vector<std::string> cIdxNames = cleanOrder(idxNames[stmt_num]);
	std::vector<std::string> origSource = idxNames[stmt_num];
	;
	//Drop index names at level
	if (unroll_amount == 0) {
//For all statements that were in this unroll together, drop index name for unrolled level
		idxNames[stmt_num][level - 1] = "";
		for (std::set<int>::iterator i = same_loop.begin();
				i != same_loop.end(); i++) {
//printf("in same loop as %d is %d\n", stmt_num, (*i));
//idxNames[(*i)][level-1] = "";
			idxNames[(*i)] = idxNames[stmt_num];
		}
	}

	lex = getLexicalOrder(stmt_num);
	same_loop = getStatements(lex, dim - 1);

	bool same_as_source = false;
	int new_stmts = stmt.size();
	for (int i = old_stmts; i < new_stmts; i++) {
		//Check whether we had a sync for the statement we are unrolling, if
		//so, propogate that to newly created statements so that if they are
		//in a different loop structure, they will also get a syncthreads
		int size = syncs.size();
		for (int j = 0; j < size; j++) {
			if (syncs[j].first == stmt_num)
				syncs.push_back(make_pair(i, syncs[j].second));
		}

		bool initial_val = false;

		//cudaized.push_back(initial_val);
		//protonu-making sure the vector of nonSplitLevels grows along with
		//the statement structure
		stmt_nonSplitLevels.push_back(std::vector<int>());

		//We expect that new statements have a constant for the variable in
		//stmt[i].IS at level (as seen with print_with_subs), otherwise there
		//will be a for loop at level and idxNames should match stmt's
		//idxNames pre-unrolled
		Relation IS = stmt[i].IS;
		//Ok, if you know how the hell to get anything out of a Relation, you
		//should probably be able to do this more elegantly. But for now, I'm
		//hacking it.
		std::string s = IS.print_with_subs_to_string();
		//s looks looks like
		//{[_t49,8,_t51,_t52,128]: 0 <= _t52 <= 3 && 0 <= _t51 <= 15 && 0 <= _t49 && 64_t49+16_t52+_t51 <= 128}
		//where level == 5, you see a integer in the input set

		//If that's not an integer and this is the first new statement, then
		//we think codegen will have a loop at that level. It's not perfect,
		//not sure if it can be determined without round-tripping to codegen.
		int sIdx = 0;
		int eIdx = 0;
		for (int j = 0; j < level - 1; j++) {
			sIdx = s.find(",", sIdx + 1);
			if (sIdx < 0)
				break;
		}
		if (sIdx > 0) {
			eIdx = s.find("]");
			int tmp = s.find(",", sIdx + 1);
			if (tmp > 0 && tmp < eIdx)
				eIdx = tmp; //", before ]"
			if (eIdx > 0) {
				sIdx++;
				std::string var = s.substr(sIdx, eIdx - sIdx);
				//printf("%s\n", s.c_str());
				//printf("set var for stmt %d at level %d is %s\n", i, level, var.c_str());
				if (atoi(var.c_str()) == 0 && i == old_stmts) {
					//TODO:Maybe do see if this new statement would be in the same
					//group as the original and if it would, don't say
					//same_as_source
					if (same_loop.find(i) == same_loop.end()) {
						printf(
								"stmt %d level %d, newly created unroll statement should have same level indexes as source\n",
								i, level);
						same_as_source = true;
					}
				}
			}
		}

//printf("fixing up statement %d n_set %d with %d levels\n", i, stmt[i].IS.n_set(), level-1);
		if (same_as_source)
			idxNames.push_back(origSource);
		else
			idxNames.push_back(idxNames[stmt_num]);
	}

	return b;
}

void LoopCuda::copy_to_texture(const char *array_name) {
	//protonu--placeholder for now
	//set the bool for using cuda memory as true
	//in a vector of strings, put the names of arrays to tex mapped
	if (!texture)
		texture = new texture_memory_mapping(true, array_name);
	else
		texture->add(array_name);

}

//protonu--moving this from Loop
void LoopCuda::codegen() {
	//if (code_gen_flags & GenCudaizeV2)
	cudaize_codegen_v2();
	//Do other flagged codegen methods, return plain vanilla generated code
	//std::set<int> stmts;
	//Anand : commenting out the following for now: 06/15/2013
	//for (int i = 0; i < stmt.size(); i++)
	//	stmts.insert(i);

	//return getCode(1, stmts);
}

//These three are in Omega code_gen.cc and are used as a massive hack to
//get out some info from MMGenerateCode. Yea for nasty side-effects.
namespace omega {
extern int checkLoopLevel;
extern int stmtForLoopCheck;
extern int upperBoundForLevel;
extern int lowerBoundForLevel;
}

CG_outputRepr* LoopCuda::extractCudaUB(int stmt_num, int level,
		int &outUpperBound, int &outLowerBound) {
	// check for sanity of parameters
	const int m = stmt.size();

	if (stmt_num >= m || stmt_num < 0)
		throw std::invalid_argument("invalid statement " + to_string(stmt_num));
	const int n = stmt[stmt_num].xform.n_out();
	if (level > (n - 1) / 2 || level <= 0)
		throw std::invalid_argument("invalid loop level " + to_string(level));

	int dim = 2 * level - 1;

	std::vector<int> lex = getLexicalOrder(stmt_num);
	std::set<int> same_loop = getStatements(lex, dim - 1);

	// extract the intersection of the iteration space to be considered
	Relation hull;
     bool has_complex_dnf = false;
	{
		hull = Relation::True(n);
		bool first=true;
		for (std::set<int>::iterator i = same_loop.begin();
				i != same_loop.end(); i++) {
			Relation r = getNewIS(*i);

			//--begin:Anand: Adding DNF traversal 10/10/2015
			std::vector<Relation> conjuncts;
			if (!r.has_single_conjunct()) {
				has_complex_dnf = true;
				for (DNF_Iterator conj(r.query_DNF()); conj; conj++) {
					Relation tmp(r.n_set());
					tmp.copy_names(r);
					tmp.setup_names();
					for (GEQ_Iterator g(*conj); g; g++) {
						tmp.and_with_GEQ(*g);
					}
					for (EQ_Iterator e(*conj); e; e++) {
						tmp.and_with_EQ(*e);
					}
					conjuncts.push_back(tmp);
				}

				for (int i = 0; i < conjuncts.size(); i++) {
					Relation indiv_hull = Relation::True(r.n_set());

					for (int j = dim + 2; j <= r.n_set(); j++)
						conjuncts[i] = Project(conjuncts[i], conjuncts[i].set_var(j));
					indiv_hull = Intersection(indiv_hull, conjuncts[i]);
					indiv_hull.simplify(2, 4);

					Relation bound = get_loop_bound(indiv_hull, dim);
					if(bound.n_set() < hull.n_set())
						bound = Extend_Set(bound, hull.n_set() - bound.n_set());
					else if(hull.n_set() < bound.n_set())
						hull = Extend_Set(hull, bound.n_set() - hull.n_set());

				    if(first){
				    	hull = bound;
				    	first = false;
				    }else{
				    	hull =Union(bound, hull );

				    }

					hull.simplify(2, 4);
				}

			}
			//--end:Anand: Adding DNF traversal 10/10/2015

			else {
				for (int j = dim + 2; j <= r.n_set(); j++)
					r = Project(r, r.set_var(j));
				hull = Intersection(hull, r);
				hull.simplify(2, 4);

			}

		}

		for (int i = 2; i <= dim + 1; i += 2) {
//std::string name = std::string("_t") + to_string(t_counter++);
			std::string name = std::string("_t")
					+ to_string(tmp_loop_var_name_counter++);
			hull.name_set_var(i, name);
		}
		hull.setup_names();
	}

	// extract the exact loop bound of the dimension to be unrolled

	if (is_single_iteration(hull, dim)) {
		throw std::runtime_error(
				"No loop availabe at level to extract upper bound.");
	}
	Relation bound = get_loop_bound(hull, dim);
	if (!bound.has_single_conjunct() || !bound.is_satisfiable()
			|| bound.is_tautology())
		throw loop_error(
				"loop error: unable to extract loop bound for cudaize");

	if(has_complex_dnf){
	 if(lowerBoundIsZero(bound, 2*level)){
			 outLowerBound = 0;
			 for(GEQ_Iterator g(bound.single_conjunct()->GEQs()); g; g++)
				 if((*g).is_const(bound.set_var(2*level)) && (*g).get_coef(bound.set_var(2*level)) < 0){
					 outUpperBound = (*g).get_const();
					 return NULL;
				 }
	  }
	}

	// extract the loop stride
	EQ_Handle stride_eq;
	/*int stride = 1;
	 {
	 bool simple_stride = true;
	 int strides = countStrides(bound.query_DNF()->single_conjunct(),
	 bound.set_var(dim + 1), stride_eq, simple_stride);
	 if (strides > 1)
	 throw loop_error("loop error: too many strides");
	 else if (strides == 1) {
	 int sign = stride_eq.get_coef(bound.set_var(dim + 1));
	 //      assert(sign == 1 || sign == -1);
	 Constr_Vars_Iter it(stride_eq, true);
	 stride = abs((*it).coef / sign);
	 }
	 }
	 */
	int stride = 1;
	{

		coef_t stride;
		std::pair<EQ_Handle, Variable_ID> result = find_simplest_stride(bound,
				bound.set_var(dim + 1));
		if (result.second == NULL)
			stride = 1;
		else
			stride = abs(result.first.get_coef(result.second))
					/ gcd(abs(result.first.get_coef(result.second)),
							abs(result.first.get_coef(bound.set_var(dim + 1))));

		//if (stride > 1)
		//	throw loop_error("loop error: too many strides");
		/*else if (stride == 1) {
		 int sign = result.first.get_coef(bound.set_var(dim+1));
		 assert(sign == 1 || sign == -1);
		 } */
	}

	/*if (stride != 1) {
	 char buf[1024];
	 sprintf(buf, "Cudaize: Loop at level %d has non-one stride of %d",
	 level, stride);
	 throw std::runtime_error(buf);
	 }
	 */
	//Use code generation system to build tell us our bound information. We
	//need a hard upper bound a 0 lower bound.
	checkLoopLevel = level * 2;
	stmtForLoopCheck = 0;
	upperBoundForLevel = -1;
	lowerBoundForLevel = -1;
	printCode(stmt_num, 2, false);
	checkLoopLevel = 0;

	outUpperBound = upperBoundForLevel;
	outLowerBound = lowerBoundForLevel;

	if (outUpperBound == -1) {

		CG_result* temp = last_compute_cgr_;
		CG_outputRepr *UPPERBOUND = NULL;
		CG_outputRepr *LOWERBOUND = NULL;
		while (temp) {
			CG_loop * loop;
			if (loop = dynamic_cast<CG_loop*>(temp)) {
				if (loop->level_ == 2 * level) {
					Relation bound(loop->bounds_.n_set());
					for (int i = 1; i <= bound.n_set(); i++)
						bound.name_set_var(i, "t" + to_string(i));

					bound.setup_names();

					for (GEQ_Iterator e(
							const_cast<Relation &>(loop->bounds_).single_conjunct()->GEQs());
							e; e++)
						bound.and_with_GEQ(*e);

					for (EQ_Iterator e(
							const_cast<Relation &>(loop->bounds_).single_conjunct()->EQs());
							e; e++)
						bound.and_with_EQ(*e);

					std::map<std::string, std::vector<omega::CG_outputRepr *> > u;

					for (omega::DNF_Iterator di(bound.query_DNF()); di; di++) {

						for (omega::Constraint_Iterator e(*di); e; e++) {
							for (omega::Constr_Vars_Iter cvi(*e); cvi; cvi++) {
								omega::Variable_ID v = cvi.curr_var();
								if (v->kind() == omega::Global_Var
										&& v->get_global_var()->arity() > 0) {
									omega::Global_Var_ID g =
											v->get_global_var();

									std::vector<omega::CG_outputRepr *> reprs;
									std::vector<omega::CG_outputRepr *> reprs2;

									for (int l = 1; l <= g->arity(); l++) {
										assert(bound.n_set() >= 2 * l);
										omega::CG_outputRepr *temp =
												ir->builder()->CreateIdent(

												bound.set_var(2 * l)->name());

										reprs.push_back(temp);

									}
									u.insert(
											std::pair<std::string,
													std::vector<
															omega::CG_outputRepr *> >(
													v->get_global_var()->base_name(),
													reprs));

								}
							}
						}
					}

					//char str1[10];
					//sprintf(str1, "newVariable%i\0", i_);
					/*for(int i =1; i <= bound.n_set(); i++){


					 char str1[10];
					 sprintf(str1, "t%i\0", i);
					 std::string str2(str1);
					 bound.name_set_var(i, str2);
					 }
					 */
					// = copy(loop->bounds_);
					Variable_ID v = bound.set_var(2 * level);
					for (GEQ_Iterator e(
							const_cast<Relation &>(bound).single_conjunct()->GEQs());
							e; e++) {
						if ((*e).get_coef(v) < 0)
							//	&& (*e).is_const_except_for_global(v))
							UPPERBOUND =
									output_upper_bound_repr(ir->builder(), *e,
											v, bound,
											std::vector<
													std::pair<CG_outputRepr *,
															int> >(
													bound.n_set(),
													std::make_pair(
															static_cast<CG_outputRepr *>(NULL),
															0)), u);
					}
					if (UPPERBOUND) {
						for (GEQ_Iterator e(

								const_cast<Relation &>(bound).single_conjunct()->GEQs());
								e; e++) {
							if ((*e).get_coef(v) > 0)
								//	&& (*e).is_const_except_for_global(v))
								LOWERBOUND =
										output_inequality_repr(ir->builder(),
												*e, v, bound,
												std::vector<
														std::pair<
																CG_outputRepr *,
																int> >(
														bound.n_set(),
														std::make_pair(
																static_cast<CG_outputRepr *>(NULL),
																0)), u);
						}

						if (LOWERBOUND)
							return ir->builder()->CreateMinus(UPPERBOUND,
									LOWERBOUND);
						else
							return UPPERBOUND;

					}
				}
				if (loop->level_ > 2 * level)
					break;
				else
					temp = loop->body_;
			} else
				break;
		}
	}

	return NULL;
}

void LoopCuda::printCode(int stmt_num, int effort, bool actuallyPrint) const {

	std::set<int> stmts = getStatements(getLexicalOrder(stmt_num), 0);
	//int stmt_translated=0;
	for (std::set<int>::iterator it = stmts.begin(); it != stmts.end(); it++) {
		if (*it == stmt_num)
			break;

		stmtForLoopCheck++;
	}

	const int m = stmts.size();
	if (m == 0)
		return;
	const int n = stmt[stmt_num].xform.n_out();

	//Tuple < Relation > IS(m);
	//Tuple < Relation > xform(m);

	/*or (int i = 0; i < m; i++) {
	 IS[i + 1] = stmt[i].IS;
	 xform[i + 1] = stmt[i].xform;

	 //nonSplitLevels[i+1] = stmt[i].nonSplitLevels;
	 }
	 */

	// invalidate saved codegen computation
	if (last_compute_cgr_ != NULL) {
		delete last_compute_cgr_;
		last_compute_cgr_ = NULL;
	}

	if (last_compute_cg_ != NULL) {
		delete last_compute_cg_;
		last_compute_cg_ = NULL;
	}

	//Relation known = Extend_Set(copy(this->known), n - this->known.n_set());
	/*CG_stringBuilder *ocg = new CG_stringBuilder();
	 Tuple<CG_outputRepr *> nameInfo;
	 for (int i = 1; i <= m; i++)
	 nameInfo.append(new CG_stringRepr("s" + to_string(i)));
	 */

	// -- replacing MMGenerateCode
	// -- formally CG_outputRepr* repr = MMGenerateCode(ocg, xform, IS, nameInfo, known, nonSplitLevels, syncs, idxTupleNames, effort);
	// -- in the future, these if statements need to be cleaned up.
	// -- something like check_lastComputeCG might be a decent protected member function
	// -- and/or something that returns a std::vector<CG_outputRepr*> that also checks last_compute_cg_
	//if (last_compute_cg_ == NULL) {
	//std::vector<Relation> IS(m);
	//std::vector<Relation> xforms(m);
	//std::vector<std::vector<int> > nonSplitLevels(m);
	std::vector<std::vector<std::string> > loopIdxNames_;
	std::vector<std::pair<int, std::string> > syncs_;

	std::vector<Relation> IS;
	std::vector<Relation> xforms;
	std::vector<std::vector<int> > nonSplitLevels;
	int count = 0;

	std::vector<std::map<std::string, std::vector<omega::CG_outputRepr *> > > uninterpreted_symbols;
	std::vector<std::map<std::string, std::vector<omega::Relation> > > unin_rel_;
	for (std::set<int>::iterator i = stmts.begin(); i != stmts.end(); i++) {
		IS.push_back(stmt[*i].IS);
		xforms.push_back(stmt[*i].xform);
		unin_rel_.push_back(unin_rel[*i]);
		uninterpreted_symbols.push_back(uninterpreted_symbols_stringrepr[*i]);
		if (stmt_nonSplitLevels.size() > *i)
			nonSplitLevels.push_back(stmt_nonSplitLevels[*i]);
		loopIdxNames_.push_back(idxNames[*i]);
		for (int j = 0; j < syncs.size(); j++) {
			if (syncs[j].first == *i) {
				std::pair<int, std::string> temp;
				temp.first = count;
				temp.second = syncs[j].second;
				syncs_.push_back(temp);
			}

		}

		count++;
	}

	/*	std::vector < std::vector <std::string> > idxTupleNames;
	 if (useIdxNames) {
	 for (int i = 0; i < idxNames.size(); i++) {
	 Tuple<std::string> idxs;
	 for (int j = 0; j < idxNames[i].size(); j++)
	 idxs.append(idxNames[i][j]);
	 idxTupleNames.append(idxs);
	 }
	 }*/

	/*for (int i = 0; i < m; i++) {
	 IS[i] = stmt[i].IS;
	 xforms[i] = stmt[i].xform;
	 nonSplitLevels[i] = stmt_nonSplitLevels[i];
	 }
	 */
	Relation known = Extend_Set(copy(this->known), n - this->known.n_set());

	last_compute_cg_ = new CodeGen(xforms, IS, known, nonSplitLevels,
			loopIdxNames_, syncs_, unin_rel_);
	delete last_compute_cgr_;
	last_compute_cgr_ = NULL;
	//}

	if (last_compute_cgr_ == NULL || last_compute_effort_ != effort) {
		delete last_compute_cgr_;
		last_compute_cgr_ = last_compute_cg_->buildAST(effort);
		last_compute_effort_ = effort;
	}

	//std::vector<CG_outputRepr *> stmts(m);
	//for (int i = 0; i < m; i++)
	//	stmts[i] = stmt[i].code;
	//CG_outputRepr* repr = last_compute_cgr_->printRepr(ocg, stmts);
	// -- end replacing MMGenerateCode

	std::string repr = last_compute_cgr_->printString(uninterpreted_symbols);

//	if (actuallyPrint)
	std::cout << repr << std::endl;
	//std::cout << static_cast<CG_stringRepr*>(repr)->GetString();
	/*
	 for (int i = 1; i <= m; i++)
	 delete nameInfo[i];
	 */

	//delete ocg;
}

void LoopCuda::printRuntimeInfo() const {
	for (int i = 0; i < stmt.size(); i++) {
		Relation IS = stmt[i].IS;
		Relation xform = stmt[i].xform;
		printf("stmt[%d]\n", i);
		printf("IS\n");
		IS.print_with_subs();

		printf("xform[%d]\n", i);
		xform.print_with_subs();

	}
}

void LoopCuda::printIndexes() const {
	for (int i = 0; i < stmt.size(); i++) {
		printf("stmt %d nset %d ", i, stmt[i].IS.n_set());

		for (int j = 0; j < idxNames[i].size(); j++) {
			if (j > 0)
				printf(",");
			printf("%s", idxNames[i][j].c_str());
		}
		printf("\n");
	}
}

SgNode* LoopCuda::getCode(int effort, std::set<int> stmts) const {
	const int m = stmts.size();
	if (m == 0)
		return new SgNode;

	int ref_stmt = *(stmts.begin());

	const int n = stmt[ref_stmt].xform.n_out();
	/*
	 Tuple<CG_outputRepr *> ni(m);
	 Tuple < Relation > IS(m);
	 Tuple < Relation > xform(m);
	 vector < vector <int> > nonSplitLevels(m);
	 for (int i = 0; i < m; i++) {
	 ni[i + 1] = stmt[i].code;
	 IS[i + 1] = stmt[i].IS;
	 xform[i + 1] = stmt[i].xform;
	 nonSplitLevels[i + 1] = stmt_nonSplitLevels[i];

	 //nonSplitLevels[i+1] = stmt[i].nonSplitLevels;
	 }
	 */
	//Relation known = Extend_Set(copy(this->known), n - this->known.n_set());
//#ifdef DEBUG
//#endif
//std::cout << GetString(MMGenerateCode(new CG_stringBuilder(), xform, IS, ni, known,
//                nonSplitLevels, syncs, idxTupleNames, effort));
	if (last_compute_cgr_ != NULL) {
		delete last_compute_cgr_;
		last_compute_cgr_ = NULL;
	}

	if (last_compute_cg_ != NULL) {
		delete last_compute_cg_;
		last_compute_cg_ = NULL;
	}

	CG_outputBuilder *ocg = ir->builder();
	// -- replacing MMGenerateCode
	// -- formally CG_outputRepr* repr = MMGenerateCode(ocg, xform, IS, nameInfo, known, nonSplitLevels, syncs, idxTupleNames, effort);
	// -- in the future, these if statements need to be cleaned up.
	// -- something like check_lastComputeCG might be a decent protected member function
	// -- and/or something that returns a std::vector<CG_outputRepr*> that also checks last_compute_cg_
	//if (last_compute_cg_ == NULL) {
	std::vector<std::vector<std::string> > loopIdxNames_;
	std::vector<std::pair<int, std::string> > syncs_;

	std::vector<Relation> IS;
	std::vector<Relation> xforms;
	std::vector<std::vector<int> > nonSplitLevels;
	int count_ = 0;
	std::vector<std::map<std::string, std::vector<omega::CG_outputRepr *> > > unin;
	std::vector<std::map<std::string, std::vector<omega::Relation> > > unin_rel_;
	for (std::set<int>::iterator i = stmts.begin(); i != stmts.end(); i++) {
		IS.push_back(stmt[*i].IS);
		xforms.push_back(stmt[*i].xform);
		unin.push_back(uninterpreted_symbols[*i]);
		unin_rel_.push_back(unin_rel[*i]);
		if (stmt_nonSplitLevels.size() > *i)
			nonSplitLevels.push_back(stmt_nonSplitLevels[*i]);
		loopIdxNames_.push_back(idxNames[*i]);
		for (int j = 0; j < syncs.size(); j++) {
			if (syncs[j].first == *i) {
				std::pair<int, std::string> temp;
				temp.first = count_;
				temp.second = syncs[j].second;
				syncs_.push_back(temp);
			}

		}

		count_++;
	}

	/*std::vector < std::vector<std::string> > idxTupleNames;
	 if (useIdxNames) {
	 for (int i = 0; i < idxNames.size(); i++) {
	 std::vector<std::string> idxs;
	 for (int j = 0; j < idxNames[i].size(); j++)
	 idxs.push_back(idxNames[i][j]);
	 idxTupleNames.push_back(idxs);
	 }
	 }
	 */
	Relation known = Extend_Set(copy(this->known), n - this->known.n_set());

	last_compute_cg_ = new CodeGen(xforms, IS, known, nonSplitLevels,
			loopIdxNames_, syncs_, unin_rel_);
	delete last_compute_cgr_;
	last_compute_cgr_ = NULL;
	//}

	if (last_compute_cgr_ == NULL || last_compute_effort_ != effort) {
		delete last_compute_cgr_;
		last_compute_cgr_ = last_compute_cg_->buildAST(effort);
		last_compute_effort_ = effort;
	}

	std::vector<CG_outputRepr *> stmts_(m);
	int count = 0;
	for (std::set<int>::iterator i = stmts.begin(); i != stmts.end(); i++)
		stmts_[count++] = stmt[*i].code;
	CG_outputRepr* repr = last_compute_cgr_->printRepr(ocg, stmts_, unin);
	// -- end replacing MMGenerateCode

	//CG_outputRepr *overflow_initialization = ocg->CreateStmtList();
	CG_outputRepr *overflow_initialization = ocg->StmtListAppend(NULL,
	NULL);
	for (std::map<int, std::vector<Free_Var_Decl *> >::const_iterator i =
			overflow.begin(); i != overflow.end(); i++)
		for (std::vector<Free_Var_Decl *>::const_iterator j = i->second.begin();
				j != i->second.end(); j++)
//overflow_initialization = ocg->StmtListAppend(overflow_initialization, ocg->CreateStmtList(ocg->CreateAssignment(0, ocg->CreateIdent((*j)->base_name()), ocg->CreateInt(0))));
			overflow_initialization = ocg->StmtListAppend(
					overflow_initialization,
					ocg->StmtListAppend(
							ocg->CreateAssignment(0,
									ocg->CreateIdent((*j)->base_name()),
									ocg->CreateInt(0)), NULL));

	repr = ocg->StmtListAppend(overflow_initialization, repr);
	SgNode *tnl = static_cast<CG_roseRepr *>(repr)->GetCode();
	SgStatementPtrList *list = static_cast<CG_roseRepr *>(repr)->GetList();

	if (tnl != NULL)
		return tnl;
	else if (tnl == NULL && list != NULL) {
		SgBasicBlock* bb2 = buildBasicBlock();

		for (SgStatementPtrList::iterator it = (*list).begin();
				it != (*list).end(); it++)
			bb2->append_statement(*it);

		tnl = isSgNode(bb2);
	} else
		throw loop_error("codegen failed");

	delete repr;
	/*
	 for (int i = 1; i <= m; i++)
	 delete ni[i];
	 */
	return tnl;

}

//protonu--adding constructors for the new derived class
LoopCuda::LoopCuda() :
		Loop(), code_gen_flags(GenInit) {
}

LoopCuda::LoopCuda(IR_Control *irc, int loop_num) :
		Loop(irc) {
	setup_code = NULL;
	teardown_code = NULL;
	code_gen_flags = 0;
	//cu_bx = cu_by = cu_tx = cu_ty = cu_tz = 1;
	//cu_bx_repr = NULL;
	//cu_tx_repr = NULL;
	//cu_by_repr = NULL;
	//cu_ty_repr = NULL;
	//cu_tz_repr = NULL;

	cu_num_reduce = 0;
	cu_mode = GlobalMem;
	texture = NULL;

	int m = stmt.size();
	//printf("\n the size of stmt(initially) is: %d\n", stmt.size());
	for (int i = 0; i < m; i++)
		stmt_nonSplitLevels.push_back(std::vector<int>());

	globals = ((IR_cudaroseCode *) ir)->gsym_;
	globalScope = ((IR_cudaroseCode *) ir)->first_scope;
	parameter_symtab = ((IR_cudaroseCode *) ir)->parameter;
	body_symtab = ((IR_cudaroseCode *) ir)->body;
	func_body = ((IR_cudaroseCode *) ir)->defn;
	func_definition = ((IR_cudaroseCode *) ir)->func_defn;
	std::vector<SgForStatement *> tf = ((IR_cudaroseCode *) ir)->get_loops();

	symtab = tf[loop_num]->get_symbol_table();

	std::vector<SgForStatement *> deepest = find_deepest_loops(
			isSgNode(tf[loop_num]));

	for (int i = 0; i < deepest.size(); i++) {
		SgVariableSymbol* vs;
		SgForInitStatement* list = deepest[i]->get_for_init_stmt();
		SgStatementPtrList& initStatements = list->get_init_stmt();
		SgStatementPtrList::const_iterator j = initStatements.begin();
		if (SgExprStatement *expr = isSgExprStatement(*j))
			if (SgAssignOp* op = isSgAssignOp(expr->get_expression()))
				if (SgVarRefExp* var_ref = isSgVarRefExp(op->get_lhs_operand()))
					vs = var_ref->get_symbol();

		index.push_back(vs->get_name().getString().c_str()); //reflects original code index names
	}

	for (int i = 0; i < stmt.size(); i++)
		idxNames.push_back(index); //refects prefered index names (used as handles in cudaize v2)

	useIdxNames = false;

}

void LoopCuda::printIS() {
	int k = stmt.size();
	for (int i = 0; i < k; i++) {
		printf(" printing statement:%d\n", i);
		stmt[i].IS.print();
	}
}

