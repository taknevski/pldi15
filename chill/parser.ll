/*****************************************************************************
 Copyright (C) 2008 University of Southern California. 
 All Rights Reserved.

 Purpose:
   CHiLL script lexical analysis

 Update history:
   created by Chun Chen, Jan 2008
*****************************************************************************/

%{


#include <stdio.h>
#include <string.h>
#include <vector>
#include <map>
#include "parser.tab.hh"
#define ONCE
#include "parser.hh"
int zzlex(){return lexer.yylex();}

extern std::map<std::string, int> parameter_tab;
extern bool is_interactive;
extern const char *PROMPT_STRING;
%}

%s LINE COMMENT FILE_NAME PROCEDURE_NAME
%option yylineno
%option noyywrap
%option prefix="zz"
%%
#                      BEGIN(COMMENT);
<COMMENT>.*            /* comment */
source                 BEGIN(FILE_NAME); return SOURCE;
<FILE_NAME>[^ \t\n:#]+ zzlval.name = new char[yyleng+1]; strcpy(zzlval.name, yytext); return FILENAME;
procedure              BEGIN(LINE); return PROCEDURE;
loop                   BEGIN(LINE); return LOOP;
format                 BEGIN(FILE_NAME); return FORMAT;
original               BEGIN(LINE); return ORIGINAL;
permute                BEGIN(LINE); return PERMUTE;
tile                   BEGIN(LINE); return TILE;
datacopy               BEGIN(LINE); return DATACOPY;
datacopy_privatized    BEGIN(LINE); return DATACOPY_PRIVATIZED;
unroll                 BEGIN(LINE); return UNROLL;
unroll_extra           BEGIN(LINE); return UNROLL_EXTRA;
split                  BEGIN(LINE); return SPLIT;
nonsingular            BEGIN(LINE); return NONSINGULAR;
print                  BEGIN(LINE); return PRINT;
dep                    BEGIN(LINE); return PRINT_DEP;
code                   BEGIN(LINE); return PRINT_CODE;
space                  BEGIN(LINE); return PRINT_IS;                     
exit                   BEGIN(LINE); return EXIT;
known                  BEGIN(LINE); return KNOWN;
strided                BEGIN(LINE); return STRIDED;
counted                BEGIN(LINE); return COUNTED;
num_statement          BEGIN(LINE); return NUM_STATEMENT;
ceil                   BEGIN(LINE); return CEIL;
floor                  BEGIN(LINE); return FLOOR;
true                   BEGIN(LINE); zzlval.bool_val = true; return TRUEORFALSE;
false                  BEGIN(LINE); zzlval.bool_val = false; return TRUEORFALSE;
skew                   BEGIN(LINE); return SKEW;
shift                  BEGIN(LINE); return SHIFT;
scale                  BEGIN(LINE); return SCALE;
reverse                BEGIN(LINE); return REVERSE;
shift_to               BEGIN(LINE); return SHIFT_TO;
fuse                   BEGIN(LINE); return FUSE;
peel                   BEGIN(LINE); return PEEL;
distribute             BEGIN(LINE); return DISTRIBUTE;
remove_dep             BEGIN(LINE); return REMOVE_DEP;
structure              BEGIN(LINE); return PRINT_STRUCTURE;
reduce                 BEGIN(LINE); return REDUCE;  /* Added by Manu */	
scalar_expand          BEGIN(LINE); return SCALAR_EXPAND;  /* Added by Manu */
split_with_alignment   BEGIN(LINE); return SPLIT_WITH_ALIGNMENT;  /* Added by Anand */  
flatten                BEGIN(LINE); return FLATTEN; 
normalize              BEGIN(LINE); return NORMALIZE;
ELLify                 BEGIN(LINE); return ELLIFY; 
compact                BEGIN(LINE); return COMPACT; 
make_dense 	       BEGIN(LINE); return MAKE_DENSE;	
set_array_size	       BEGIN(LINE); return SET_ARRAY_SIZE;
sparse_partition       BEGIN(LINE); return SPARSE_PARTITION;
reorder_by_inspector   BEGIN(LINE); return REORDER_BY_INSPECTOR;
reorder_data   	       BEGIN(LINE); return REORDER_DATA;
sparse_wavefront       BEGIN(LINE); return SPARSE_WAVEFRONT;	
map_to_openmp_region   BEGIN(LINE); return MAP_TO_OPENMP_REGION;
mark_omp_parallel_region BEGIN(LINE); return MARK_OMP_PARL_REGION;
mark_omp_threads       BEGIN(LINE); return MARK_OMP_THRDS;
mark_omp_sync	       BEGIN(LINE); return MARK_OMP_SYNC;
gen_omp_parallel_region BEGIN(LINE); return GEN_OMP_PARL_REGION;
omp_par_for 	        BEGIN(LINE); return OMP_PAR_FOR;
mark_pragma             BEGIN(LINE); return MARK_PRAGMA; 
[ \t]+                 /* ignore whitespaces */
\n                     BEGIN(INITIAL); return (int)yytext[0];
L[0-9]+                zzlval.val = atoi(&yytext[1]); return LEVEL;
[a-zA-Z_][a-zA-Z_0-9]* {
                         BEGIN(LINE);
                         zzlval.name = new char[yyleng+1];
                         strcpy(zzlval.name, yytext);
                         return VARIABLE;
                       }                        
[0-9]+                 zzlval.val = atoi(yytext); return NUMBER;
\>\=                   return GE;
\<\=                   return LE;
\!\=                   return NE;
\=\=                   return EQ;
.                      return (int)yytext[0];
<LINE><<EOF>>          BEGIN(INITIAL); unput('\n');

%%


