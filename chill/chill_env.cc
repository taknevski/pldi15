/*****************************************************************************
 Copyright (C) 2010 University of Utah.
 All Rights Reserved.

 Purpose:
 Register variables and functions into the global Lua addres space to
 provide an environment for CHiLL scripts

 Notes:
 Contains Lua wrappers for the CHiLL Loop class and methods.

 History:
 01/2010 created by Gabe Rudy
 *****************************************************************************/

#include <lua.hpp> //All lua includes wrapped in extern "C"
#include "chill_env.hh"
#include "loop.hh"

#include "loop_cuda_rose.hh"
#include "ir_rose.hh"
#include "ir_cudarose.hh"

//using namespace omega;

extern LoopCuda *myloop;
extern IR_Code *ir_code;
extern bool repl_stop;

extern std::vector<IR_Control *> ir_controls;
extern std::vector<int> loops;

//Macros for wrapping code to myloop-> that translates C++ exceptions to
//Lua stracktraced errors
#define REQUIRE_LOOP try{ if (myloop == NULL){ throw std::runtime_error("loop not initialized"); }
#define END_REQUIRE_LOOP  }catch (const std::exception &e) { return luaL_error(L, e.what()); }

void register_v1(lua_State *L);
void register_v2(lua_State *L);

//Extra param checking
static bool luaL_checkboolean(lua_State *L, int narg) {
	if (!lua_isboolean(L,narg))
		luaL_typerror(L, narg, "boolean");
	return lua_toboolean(L, narg);
}

static bool luaL_optboolean(lua_State *L, int narg, bool def) {
	return luaL_opt(L, luaL_checkboolean, narg, def);
}

static bool tointvector(lua_State *L, int narg, std::vector<int>& v) {
	if (!lua_istable(L, narg))
		return false;

	//Iterate through array (table)
	lua_pushnil(L);  // first key
	while (lua_next(L, narg) != 0) {
		// uses 'key' (at index -2) and 'value' (at index -1)
		v.push_back((int) luaL_checknumber(L, -1));
		//printf("added: %d", v[v.size()-1]);
		// removes 'value'; keeps 'key' for next iteration
		lua_pop(L, 1);
	}
	return true;
}

static bool tostringvector(lua_State *L, int narg,
		std::vector<std::string>& v) {
	if (!lua_istable(L, narg))
		return false;

	//Iterate through array (table)
	lua_pushnil(L);  // first key
	while (lua_next(L, narg) != 0) {
		// uses 'key' (at index -2) and 'value' (at index -1)
		v.push_back(luaL_checkstring(L,-1));
		//printf("added: %d", v[v.size()-1]);
		// removes 'value'; keeps 'key' for next iteration
		lua_pop(L, 1);
	}
	return true;
}

static bool tostringmap(lua_State *L, int narg,
		std::map<std::string, std::string>& v) {
	if (!lua_istable(L, narg))
		return false;

	//Iterate through array (table)
	lua_pushnil(L);  // first key
	while (lua_next(L, narg) != 0) {
		// uses 'key' (at index -2) and 'value' (at index -1)
		v.insert(
				std::make_pair(luaL_checkstring(L,-2), luaL_checkstring(L,-1)));
		//printf("added: %d", v[v.size()-1]);
		// removes 'value'; keeps 'key' for next iteration
		lua_pop(L, 1);
	}
	return true;
}

static bool tostringintmap(lua_State *L, int narg,
		std::map<std::string, int>& v) {
	if (!lua_istable(L, narg))
		return false;

	//Iterate through array (table)
	lua_pushnil(L);  // first key
	while (lua_next(L, narg) != 0) {
		// uses 'key' (at index -2) and 'value' (at index -1)
		v.insert(
				std::make_pair(luaL_checkstring(L,-2),
						(int) luaL_checknumber(L, -1)));
		//printf("added: %d", v[v.size()-1]);
		// removes 'value'; keeps 'key' for next iteration
		lua_pop(L, 1);
	}
	return true;
}

static bool tointmatrix(lua_State *L, int narg,
		std::vector<std::vector<int> >& m) {
	if (!lua_istable(L, narg))
		return false;

	//Iterate through array (table)
	lua_pushnil(L);  // first key
	while (lua_next(L, narg) != 0) {
		// uses 'key' (at index -2) and 'value' (at index -1)
		if (!lua_istable(L,-1)) {
			lua_pop(L, 2);
			return false;
		}
		m.push_back(std::vector<int>());
		int i = m.size() - 1;
		//Now iterate over the keys of the second level table
		int l2 = lua_gettop(L); //Index of second level table
		lua_pushnil(L);  // first key
		while (lua_next(L, l2) != 0) {
			int k = (int) luaL_checknumber(L, -1);
			m[i].push_back(k);
			//printf("m[%d][%d] = %d\n", i,m[i].size()-1,k);
			lua_pop(L, 1);
		}
		lua_pop(L, 1);
		// removes 'value'; keeps 'key' for next iteration
	}
	return true;
}


static bool int_to_int_vector_map(lua_State *L, int narg,
		std::vector< std::pair< int, std::vector<int> > >& m) {
	if (!lua_istable(L, narg))
		return false;

	//Iterate through array (table)
	lua_pushnil(L);  // first key
	while (lua_next(L, narg) != 0) {


		// uses 'key' (at index -2) and 'value' (at index -1)
		if (!lua_istable(L,-1)) {
			lua_pop(L, 2);
			return false;
		}


		//m.push_back(std::vector<int>());
		//int i = m.size() - 1;
		//Now iterate over the keys of the second level table
		int l2 = lua_gettop(L); //Index of second level table
		lua_pushnil(L);  // first key
		while (lua_next(L, l2) != 0) {
			int k = (int) luaL_checknumber(L, -2);

			std::vector<int> refs;
			if(!tointvector(L, -1, refs))
					return false;

			m.push_back(std::make_pair(k, refs));
			//printf("m[%d][%d] = %d\n", i,m[i].size()-1,k);
			lua_pop(L, 1);
		}
		lua_pop(L, 1);
		// removes 'value'; keeps 'key' for next iteration
	}
	return true;
}



static void strict_arg_num(lua_State *L, int num) {
	int n = lua_gettop(L); //Number of arguments
	if (n != num)
		throw std::runtime_error("incorrect number of arguments");
}

/* The function we'll call from the lua script */
static int init(lua_State *L) {
	int n = lua_gettop(L); //Number of arguments
	if (n > 0) {
		//Expet one of the following forms
		//l1 = init("mm4.sp2",0,0) --input file, procedure 0, loop 0
		//or
		//l1 = init("mm4.sp2","NameFromPragma")

		const char* source_filename = luaL_optstring(L,1,0);
		if (lua_isstring(L, 2)) {
			const char* procedure_name = luaL_optstring(L, 2, 0);
			int loop_num = luaL_optint(L, 3, 0);

			lua_getglobal(L, "dest");
			const char* dest_lang = lua_tostring(L,-1);
			lua_pop(L, 1);
			ir_code = new IR_cudaroseCode(source_filename, procedure_name);
			IR_Block *block = ir_code->GetCode();
			ir_controls = ir_code->FindOneLevelControlStructure(block);

			int loop_count = 0;
			for (int i = 0; i < ir_controls.size(); i++) {
				if (ir_controls[i]->type() == IR_CONTROL_LOOP) {
					loops.push_back(i);
					loop_count++;
				}
			}
			delete block;

			std::vector<IR_Control *> parm;
			for (int j = 0; j < loop_count; j++)
				parm.push_back(ir_controls[loops[j]]);

			block = ir_code->MergeNeighboringControlStructures(parm);
			myloop = new LoopCuda(block, loop_num);
			delete block;

		} else {
			//TODO: handle pragma lookup
		}
		//Also register a different set of global functions
		myloop->original();
		myloop->useIdxNames = true;			//Use idxName in code_gen
		register_v2(L);
		//TODO: return a reference to the intial array if that makes sense
		//still
		return 0;
	}
	lua_getglobal(L, "source");
	const char* source_filename = lua_tostring(L,-1);
	lua_pop(L, 1);

	lua_getglobal(L, "dest");
	const char* dest_lang = lua_tostring(L,-1);
	lua_pop(L, 1);

	lua_getglobal(L, "procedure");
	const char* procedure_name = lua_tostring(L , -1);
	lua_pop(L, 1);

	lua_getglobal(L, "loop");
	int loop_num = lua_tointeger(L, -1);
	lua_pop(L, 1);

	ir_code = new IR_cudaroseCode(source_filename, procedure_name);

	IR_Block *block = ir_code->GetCode();
	ir_controls = ir_code->FindOneLevelControlStructure(block);

	int loop_count = 0;
	for (int i = 0; i < ir_controls.size(); i++) {
		if (ir_controls[i]->type() == IR_CONTROL_LOOP) {
			loops.push_back(i);
			loop_count++;
		}
	}
	delete block;

	std::vector<IR_Control *> parm;
	for (int j = 0; j < loop_count; j++)
		parm.push_back(ir_controls[loops[j]]);

	block = ir_code->MergeNeighboringControlStructures(parm);
	myloop = new LoopCuda(block, loop_num);
	delete block;

//register_v1(L);
	register_v2(L);
	return 0;
}

static int exit(lua_State *L) {
	strict_arg_num(L, 0);
	repl_stop = true;
	return 0;
}

static int print_code(lua_State *L) {
	REQUIRE_LOOP;
	strict_arg_num(L, 1);
	int stmt = luaL_optint(L,1,0);
	myloop->printCode(stmt);
	printf("\n");
	END_REQUIRE_LOOP;
	return 0;
}

static int print_ri(lua_State *L) {
	REQUIRE_LOOP;
	strict_arg_num(L, 0);
	myloop->printRuntimeInfo();
	printf("\n");
	END_REQUIRE_LOOP;
	return 0;
}

static int print_idx(lua_State *L) {
	REQUIRE_LOOP;
	strict_arg_num(L, 0);
	myloop->printIndexes();
	printf("\n");
	END_REQUIRE_LOOP;
	return 0;
}

static int print_dep(lua_State *L) {
	REQUIRE_LOOP;
	strict_arg_num(L, 0);
	std::cout << myloop->dep;
	END_REQUIRE_LOOP;
	return 0;
}

static int print_space(lua_State *L) {
	REQUIRE_LOOP;
	strict_arg_num(L, 0);
	for (int i = 0; i < myloop->stmt.size(); i++) {
		printf("s%d: ", i + 1);
		omega::Relation r;
		if (!myloop->stmt[i].xform.is_null())
			r = omega::Range(
					omega::Restrict_Domain(copy(myloop->stmt[i].xform),
							copy(myloop->stmt[i].IS)));
		else
			r = copy(myloop->stmt[i].IS);
		r.simplify(2, 4);
		r.print();
	}END_REQUIRE_LOOP;
	return 0;
}

static int num_statement(lua_State *L) {
	REQUIRE_LOOP;
	lua_pushinteger(L, myloop->stmt.size());
	END_REQUIRE_LOOP;
	return 1;
}

static int does_var_exists(lua_State *L) {
	REQUIRE_LOOP;
	strict_arg_num(L, 1);
	std::string symName = luaL_optstring(L,1,"");
	lua_pushboolean(L, myloop->symbolExists(symName));
	END_REQUIRE_LOOP;
	return 1;
}

static int add_sync(lua_State *L) {
	REQUIRE_LOOP;
	strict_arg_num(L, 2);
	int stmt = luaL_optint(L,1,0);
	std::string idxName = luaL_optstring(L,2,"");
	myloop->addSync(stmt, idxName);
	END_REQUIRE_LOOP;
	return 0;
}

static int rename_index(lua_State *L) {
	REQUIRE_LOOP;
	strict_arg_num(L, 3);
	int stmt = luaL_optint(L,1,0);
	std::string idxName = luaL_optstring(L,2,"");
	std::string newName = luaL_optstring(L,3,"");
	myloop->renameIndex(stmt, idxName, newName);
	END_REQUIRE_LOOP;
	return 0;
}

//basic on index names
static int permute_v2(lua_State *L) {
	REQUIRE_LOOP;
	strict_arg_num(L, 2);
	int stmt = luaL_optint(L,1,0);
	std::vector<std::string> order;
	if (!tostringvector(L, 2, order)) {
		throw std::runtime_error("second arg must be a string vector");
	}
	myloop->permute_cuda(stmt, order);
	END_REQUIRE_LOOP;
	return 0;
}

static int tile_v2(lua_State *L) {
	REQUIRE_LOOP;
	int n = lua_gettop(L); //Number of arguments
	if (n != 3 && n != 7 && n != 4)
		throw std::runtime_error("incorrect number of arguments");
	int stmt_num = luaL_optint(L, 1, 0);
	int level = luaL_optint(L, 2, 0);
	if (n == 3) {
		int outer_level = luaL_optint(L, 3, 1);
		myloop->tile_cuda(stmt_num, level, outer_level);
	} else if (n == 4) {

		int outer_level = luaL_optint(L, 3, 1);
		int imethod = luaL_optint(L, 4, 2);
		TilingMethodType method;

		if (imethod == 0)
			method = StridedTile;
		else
			method = CountedTile;
		myloop->tile_cuda(stmt_num, level, outer_level, method);

	} else {
		int tile_size = luaL_optint(L, 3, 0);
		int outer_level = luaL_optint(L, 4, 1);
		std::string idxName = luaL_optstring(L,5,"");
		std::string ctrlName = luaL_optstring(L,6,"");
		TilingMethodType method = StridedTile;
		if (n > 6) {
			int imethod = luaL_optint(L, 7, 2);
			if (imethod == 0)
				method = StridedTile;
			else if (imethod == 1)
				method = CountedTile;
			else {
				throw std::runtime_error(
						"7th argument must be either strided or counted");
			}
		}
		myloop->tile_cuda(stmt_num, level, tile_size, outer_level, idxName,
				ctrlName, method);
	}END_REQUIRE_LOOP;
	return 0;
}

static int cur_indices(lua_State *L) {
	REQUIRE_LOOP;
	strict_arg_num(L, 1);
	int stmt_num = luaL_optint(L, 1, 0);
//TODO: needs to be per stmt
	lua_createtable(L, myloop->idxNames[stmt_num].size(), 0);
	for (int i = 0; i < myloop->idxNames[stmt_num].size(); i++) {
		lua_pushinteger(L, i + 1);
		lua_pushstring(L, myloop->idxNames[stmt_num][i].c_str());
		lua_settable(L, -3);
	}END_REQUIRE_LOOP;
	return 1;
}
//block indices, thread indices and block_dim and thread_dim
// are not fully implemented for multiple cudaize statements,
//needs to be fixed in future
//Anand : 06/15/2013
static int block_indices(lua_State *L) {
	REQUIRE_LOOP;
	strict_arg_num(L, 0);
	lua_newtable(L);
	if (myloop->cu_bx[0] > 1) {
		lua_pushinteger(L, 1);
		lua_pushstring(L, "bx");
		lua_settable(L, -3);
	}
	if (myloop->cu_by[0] > 1) {
		lua_pushinteger(L, 2);
		lua_pushstring(L, "by");
		lua_settable(L, -3);
	}END_REQUIRE_LOOP;
	return 1;
}

static int thread_indices(lua_State *L) {
	REQUIRE_LOOP;
	strict_arg_num(L, 0);
	lua_newtable(L);
	if (myloop->cu_tx[0] > 1) {
		lua_pushinteger(L, 1);
		lua_pushstring(L, "tx");
		lua_settable(L, -3);
	}
	if (myloop->cu_ty[0] > 1) {
		lua_pushinteger(L, 2);
		lua_pushstring(L, "ty");
		lua_settable(L, -3);
	}
	if (myloop->cu_tz[0] > 1) {
		lua_pushinteger(L, 3);
		lua_pushstring(L, "tz");
		lua_settable(L, -3);
	}END_REQUIRE_LOOP;
	return 1;
}

static int block_dims(lua_State *L) {
	REQUIRE_LOOP;
	strict_arg_num(L, 0);
	lua_pushinteger(L, myloop->cu_bx[0]);
	lua_pushinteger(L, myloop->cu_by[0]);
	END_REQUIRE_LOOP;
	return 2;
}

static int thread_dims(lua_State *L) {
	REQUIRE_LOOP;
	strict_arg_num(L, 0);
	lua_pushinteger(L, myloop->cu_tx[0]);
	lua_pushinteger(L, myloop->cu_ty[0]);
	lua_pushinteger(L, myloop->cu_tz[0]);
	END_REQUIRE_LOOP;
	return 3;
}

static int hard_loop_bounds(lua_State *L) {
	REQUIRE_LOOP;
	strict_arg_num(L, 2);
	int stmt = luaL_optint(L, 1, 0);
	int level = luaL_optint(L, 2, 0);
	int upper, lower;
	myloop->extractCudaUB(stmt, level, upper, lower);
	lua_pushinteger(L, lower);
	lua_pushinteger(L, upper);
	END_REQUIRE_LOOP;
	return 2;
}

static int datacopy_v2(lua_State *L) {
	REQUIRE_LOOP;
	int n = lua_gettop(L); //Number of arguments

//overload 1
//examples:
// datacopy(0,4,a,false,0,1,-16)
// datacopy(0,3,2,1)
	if (n < 4 || n > 9)
		throw std::runtime_error("incorrect number of arguments");
	int stmt_num = luaL_optint(L, 1, 0);
	int level = luaL_optint(L, 2, 0);
	const char* array_name = luaL_optstring(L, 3, 0);
	std::vector<std::string> new_idxs;
	if (!tostringvector(L, 4, new_idxs))
		throw std::runtime_error("fourth argument must be an array of strings");
	bool allow_extra_read = luaL_optboolean(L, 5, false);
	int fastest_changing_dimension = luaL_optint(L, 6, -1);
	int padding_stride = luaL_optint(L, 7, 1);
	int padding_alignment = luaL_optint(L, 8, 1);
	bool cuda_shared = luaL_optboolean(L, 9, false);
	myloop->datacopy_cuda(stmt_num, level, array_name, new_idxs,
			allow_extra_read, fastest_changing_dimension, padding_stride,
			padding_alignment, cuda_shared);
	END_REQUIRE_LOOP;
	return 0;
}


static int ELLify_v2(lua_State *L) {
	REQUIRE_LOOP;
	int n = lua_gettop(L); //Number of arguments

//overload 1
//examples:
// datacopy(0,4,a,false,0,1,-16)
// datacopy(0,3,2,1)
	if (n != 3 && n != 5)
		throw std::runtime_error("incorrect number of arguments");
	int stmt_num = luaL_optint(L, 1, 0);
	std::vector<std::string> new_idxs;
	if (!tostringvector(L, 2, new_idxs))
		throw std::runtime_error("2nd argument must be an array of strings");
    int padding_stride = luaL_optint(L, 3, 0);
	bool dense_pad = luaL_optboolean(L, 4, false);
	std::string pos_array_name = luaL_optstring(L, 5, "");
	myloop->ELLify_cuda(stmt_num, new_idxs, padding_stride, dense_pad, pos_array_name);
	END_REQUIRE_LOOP;
	return 0;
}


static int normalize_v2(lua_State *L) {
	REQUIRE_LOOP;
	int n = lua_gettop(L); //Number of arguments

//overload 1
//examples:
// datacopy(0,4,a,false,0,1,-16)
// datacopy(0,3,2,1)
	if (n != 2)
		throw std::runtime_error("incorrect number of arguments");
	int stmt_num = luaL_optint(L, 1, 0);
	int  level = luaL_optint(L, 2, 0);
	myloop->normalize_cuda(stmt_num, level);
	END_REQUIRE_LOOP;
	return 0;
}



static int datacopy_privatized_v3(lua_State *L) {
	REQUIRE_LOOP;
	int n = lua_gettop(L); //Number of arguments

//example:
//datacopy_privatized(0,3,"a",{4,5},false,-1,1,1)
	if (n < 4 || n > 9)
		throw std::runtime_error("incorrect number of arguments");
	int stmt_num = luaL_optint(L, 1, 0);
	std::string level_idx = luaL_optstring(L, 2, 0);
	int level = myloop->findCurLevel(stmt_num, level_idx);
	std::vector<std::pair<int, std::vector<int> > > array_ref_nums;
	std::vector<std::vector<int> > array_ref_nums_tmp;
	if (!tointmatrix(L, 3, array_ref_nums_tmp))
		throw std::runtime_error(
				"3rd argument must vector of pairs of (int,std::vector<int> ");
    std::map<int, std::set<int > >refs;

    for(int i=0; i < array_ref_nums_tmp.size(); i++){
		assert(array_ref_nums_tmp[i].size() == 2);
    	refs.insert(std::pair<int, std::set<int> >(array_ref_nums_tmp[i][0], std::set<int>()));
    }


    for(std::map<int, std::set<int > >::iterator it = refs.begin(); it != refs.end(); it++){
    for(int i=0; i < array_ref_nums_tmp.size(); i++){
	  if(array_ref_nums_tmp[i][0] == it->first)
	  {
		  it->second.insert(array_ref_nums_tmp[i][1]);

	  }
    }
    }

    for(std::map<int, std::set<int > >::iterator it = refs.begin(); it != refs.end(); it++){
    	    std::vector<int> to_push;
            for(std::set<int>::iterator it2 = it->second.begin(); it2 != it->second.end(); it2++)
            	to_push.push_back(*it2);

            array_ref_nums.push_back(std::pair<int, std::vector<int> >(it->first, to_push));
       }

	//const char* array_name = luaL_optstring(L, 3, 0);

	std::vector<std::string> privatized_idxs;
	if (!tostringvector(L, 4, privatized_idxs))
		throw std::runtime_error(
				"4th argument must be an array of index strings");

    std::vector<int> privatized_levels(privatized_idxs.size());
	for (int i = 0; i < privatized_idxs.size(); i++)
		privatized_levels[i] = myloop->findCurLevel(stmt_num,
				privatized_idxs[i]);

	bool allow_extra_read = luaL_optboolean(L, 5, false);
	int fastest_changing_dimension = luaL_optint(L, 6, -1);
	int padding_stride = luaL_optint(L, 7, 1);
	int padding_alignment = luaL_optint(L, 8, 1);
	bool cuda_shared = luaL_optboolean(L, 9, false);
	myloop->datacopy_privatized_cuda(stmt_num, level, array_ref_nums,
			privatized_levels, allow_extra_read, fastest_changing_dimension,
			padding_stride, padding_alignment, cuda_shared);
	END_REQUIRE_LOOP;
	return 0;
}



static int datacopy_privatized_v2(lua_State *L) {
	REQUIRE_LOOP;
	int n = lua_gettop(L); //Number of arguments

//example:
//datacopy_privatized(0,3,"a",{4,5},false,-1,1,1)
	if (n < 4 || n > 9)
		throw std::runtime_error("incorrect number of arguments");
	int stmt_num = luaL_optint(L, 1, 0);
	std::string level_idx = luaL_optstring(L, 2, 0);
	int level = myloop->findCurLevel(stmt_num, level_idx);
	const char* array_name = luaL_optstring(L, 3, 0);

	std::vector<std::string> privatized_idxs;
	if (!tostringvector(L, 4, privatized_idxs))
		throw std::runtime_error(
				"4th argument must be an array of index strings");
	
    std::vector<int> privatized_levels(privatized_idxs.size());
	for (int i = 0; i < privatized_idxs.size(); i++)
		privatized_levels[i] = myloop->findCurLevel(stmt_num,
				privatized_idxs[i]);

	bool allow_extra_read = luaL_optboolean(L, 5, false);
	int fastest_changing_dimension = luaL_optint(L, 6, -1);
	int padding_stride = luaL_optint(L, 7, 1);
	int padding_alignment = luaL_optint(L, 8, 1);
	bool cuda_shared = luaL_optboolean(L, 9, false);
	myloop->datacopy_privatized_cuda(stmt_num, level, array_name,
			privatized_levels, allow_extra_read, fastest_changing_dimension,
			padding_stride, padding_alignment, cuda_shared);
	END_REQUIRE_LOOP;
	return 0;
}

static int unroll_v2(lua_State *L) {
	REQUIRE_LOOP;
//int n = lua_gettop(L); //Number of arguments
	strict_arg_num(L, 3);
	int stmt_num = luaL_optint(L, 1, 0);
	int level;
	if (lua_isnumber(L, 2)) {
		level = luaL_optint(L, 2, 0);
	} else {
		std::string level_idx = luaL_optstring(L,2,"");
		level = myloop->findCurLevel(stmt_num, level_idx);
	}
	int unroll_amount = luaL_optint(L, 3, 0);
	bool does_expand = myloop->unroll_cuda(stmt_num, level, unroll_amount);
	lua_pushboolean(L, does_expand);
	END_REQUIRE_LOOP;
	return 1;
}

static int flatten_v2(lua_State *L) {
	REQUIRE_LOOP;
//int n = lua_gettop(L); //Number of arguments
	strict_arg_num(L, 4);
	int stmt_num = luaL_optint(L, 1, 0);
	//std::vector<std::string> idxs;
	//if (!tostringvector(L, 2, idxs)) {
	//	throw std::runtime_error("second arg must be a string vector");
	//}

	std::string  idxs = luaL_optstring(L, 2, "");

	std::vector<int> loop_levels;

	if (!tointvector(L, 3, loop_levels)) {
		throw std::runtime_error("third arg must be a int vector");
	}

	std::string inspector_name = luaL_optstring(L,4,"");

	myloop->flatten_cuda(stmt_num, idxs, loop_levels, inspector_name);

	END_REQUIRE_LOOP;
	return 1;
}

static int distribute_v2(lua_State *L) {
	REQUIRE_LOOP;
//int n = lua_gettop(L); //Number of arguments
	strict_arg_num(L, 2);

	std::vector<int> stmt_nums;
	if (!tointvector(L, 1, stmt_nums)) {
		throw std::runtime_error("1st arg must be an integer vector");
	}

	int loop_level = luaL_optint(L, 2, 0);

	myloop->distribute_cuda(stmt_nums, loop_level);

	END_REQUIRE_LOOP;
	return 1;
}

static int fuse_v2(lua_State *L) {
	REQUIRE_LOOP;
//int n = lua_gettop(L); //Number of arguments
	strict_arg_num(L, 2);

	std::vector<int> stmt_nums;
	if (!tointvector(L, 1, stmt_nums)) {
		throw std::runtime_error("1st arg must be an integer vector");
	}

	int loop_level = luaL_optint(L, 2, 0);

	myloop->fuse_cuda(stmt_nums, loop_level);

	END_REQUIRE_LOOP;
	return 1;
}

static int peel_v2(lua_State *L) {
	REQUIRE_LOOP;
//int n = lua_gettop(L); //Number of arguments
	strict_arg_num(L, 3);

	int stmt_num = luaL_optint(L, 1, 0);

	int loop_level = luaL_optint(L, 2, 0);

	int amount = luaL_optint(L, 3, 0);

	myloop->peel_cuda(stmt_num, loop_level, amount);

	END_REQUIRE_LOOP;
	return 1;
}

static int scalar_expand_v2(lua_State *L) {
	REQUIRE_LOOP;
	int n = lua_gettop(L); //Number of arguments
//strict_arg_num(L, 3);
	if (n != 3 && n != 4 && n != 5 && n != 6)
		throw std::runtime_error("wrong number of arguments");
	int stmt_num = luaL_optint(L, 1, 0);

	std::vector<int> level;
	if (!tointvector(L, 2, level)) {
		throw std::runtime_error("1st arg must be an integer vector");
	}
	std::string arrName = luaL_optstring(L,3,"");

	int cuda_shared = luaL_optint(L, 4, 0);
	int padding = luaL_optint(L, 5, 0);
	if (n != 6)
		myloop->scalar_expand_cuda(stmt_num, level, arrName, cuda_shared,
				padding);
	else {
		int assign_then_accumulate = luaL_optint(L, 6, 0);
		myloop->scalar_expand_cuda(stmt_num, level, arrName, cuda_shared,
				padding, assign_then_accumulate);
	}
	END_REQUIRE_LOOP;
	return 1;
}

static int shift_to_v2(lua_State *L) {
	REQUIRE_LOOP;
//int n = lua_gettop(L); //Number of arguments
	strict_arg_num(L, 3);

	int stmt_num = luaL_optint(L, 1, 0);

	int level = luaL_optint(L, 2, 0);

	int absolute_position = luaL_optint(L, 3, 0);

	myloop->shift_to_cuda(stmt_num, level, absolute_position);

	END_REQUIRE_LOOP;
	return 1;
}
static int split_with_alignment_v2(lua_State *L) {
	REQUIRE_LOOP;
//int n = lua_gettop(L); //Number of arguments
	strict_arg_num(L, 3);

	int stmt_num = luaL_optint(L, 1, 0);

	int level = luaL_optint(L, 2, 0);

	int alignment = luaL_optint(L, 3, 0);

	myloop->split_with_alignment_cuda(stmt_num, level, alignment);

	END_REQUIRE_LOOP;
	return 1;
}
static int compact_v2(lua_State *L){
	REQUIRE_LOOP;
	int n = lua_gettop(L);

	if (n != 5 && n!=8 && n != 9)
		throw std::runtime_error("wrong number of arguments");

	int stmt_num = luaL_optint(L, 1, 0);

	std::vector<int> level;

	if (!tointvector(L, 2, level)) {
			throw std::runtime_error("2nd arg must be a int vector");
		}



	std::vector<std::string> new_array;
	if (!tostringvector(L, 3, new_array)) {
			throw std::runtime_error("3rd arg must be a string vector");
		}

	int zero = luaL_optint(L, 4, 0);



	std::vector<std::string> data_array;
	if (!tostringvector(L, 5, data_array)) {
			throw std::runtime_error("fifth arg must be a string vector");
		}

	if(n == 5)
		myloop->compact_cuda(stmt_num,  level,  new_array,  zero,data_array);
	else{
		std::vector<int> str_coefs;
				if (!tointvector(L, 6, str_coefs)) {
						throw std::runtime_error("first arg must be a int vector");
					}//int val = luaL_optint(L, 2, 0);

				std::vector<std::string> str_vals;
				if(!tostringvector(L, 7, str_vals))
						throw std::runtime_error("second arg must be a string vector");


		int  constants = luaL_optint(L, 8, 0);

		Relation rel = 	myloop->rel_help(str_coefs, str_vals, constants);
		bool max_allocate =false;
		if(n > 8){

		int alloc;
		alloc	 = luaL_optint(L, 9, 0);
		if(alloc == 1)
			max_allocate = true;
		}
		myloop->compact_cuda(stmt_num,  level,  new_array,  zero,data_array, rel, max_allocate);
	}


	END_REQUIRE_LOOP;
	return 1;
}
static int  make_dense_v2(lua_State *L){

	REQUIRE_LOOP;
	strict_arg_num(L, 3);

	int stmt_num = luaL_optint(L, 1, 0);

	int loop_level = luaL_optint(L, 2, 0);

	std::string new_loop_index = luaL_optstring(L,3,"");


	myloop->make_dense_cuda(stmt_num,  loop_level,  new_loop_index);
	END_REQUIRE_LOOP;
 return 1;

}

static int  addKnown_v2(lua_State *L){

	REQUIRE_LOOP;

	int n = lua_gettop(L);
	if(n == 2){

	std::string var = luaL_optstring(L,1,"");
	int val = luaL_optint(L, 2, 0);

	myloop->addKnown_cuda(var,val);

	}
	else if(n == 4){

		std::vector<std::string> str_vals;
				if(!tostringvector(L, 1, str_vals))
						throw std::runtime_error("first arg must be a string vector");

		std::vector<int> str_coefs;
					if (!tointvector(L, 2, str_coefs)) {
							throw std::runtime_error("second arg must be a int vector");
						}//int val = luaL_optint(L, 2, 0);




			int  constants = luaL_optint(L, 3, 0);

			int eq_or_geq = luaL_optint(L, 4, 0);


			Relation rel;
			if(eq_or_geq == 0)
			rel= 	myloop->rel_help(str_coefs, str_vals, constants, false);
			else
				rel= 	myloop->rel_help(str_coefs, str_vals, constants, true);
		    myloop->addKnown(rel);

	}



	END_REQUIRE_LOOP;
return 1;

}


static int  skew_v2(lua_State *L){

	REQUIRE_LOOP;
	strict_arg_num(L, 3);

	std::vector<int> stmt_num;
		if (!tointvector(L, 1, stmt_num)) {
			throw std::runtime_error("first arg must be a int vector");
		}
	int level = luaL_optint(L, 2, 0);


	std::vector<int> coefs;
		if (!tointvector(L, 3, coefs)) {
			throw std::runtime_error("third arg must be a int vector");
		}


	myloop->skew_cuda(stmt_num,level, coefs);
	END_REQUIRE_LOOP;
return 1;

}

static int reduce_v2(lua_State *L) {
	REQUIRE_LOOP;
    int n = lua_gettop(L); //Number of arguments
	//strict_arg_num(L, 5);

    if(n != 5 && n != 6)
    	throw std::runtime_error("wrong number of arguments");

	int stmt_num = luaL_optint(L, 1, 0);

	std::vector<int> level;
	if (!tointvector(L, 2, level)) {
		throw std::runtime_error("second arg must be a int vector");
	}

	int param = luaL_optint(L, 3, 0);

	std::string func_name = luaL_optstring(L,4,"");

	std::vector<int> seq_level;
	if (!tointvector(L, 5, seq_level)) {
		throw std::runtime_error("second arg must be a int vector");
	}

	if(n <= 5)
	myloop->reduce_cuda(stmt_num, level, param, func_name, seq_level);
	else{
		int bound = luaL_optint(L, 6, 0);
		myloop->reduce_cuda(stmt_num, level, param, func_name, seq_level,bound);
	}

	END_REQUIRE_LOOP;
	return 1;
}





static int cudaize_v2(lua_State *L) {
	REQUIRE_LOOP;
//int n = lua_gettop(L); //Number of arguments
	strict_arg_num(L, 5);
	int stmt_num = luaL_optint(L, 1, 0);
	std::string kernel_name = luaL_optstring(L, 2, 0);

	std::vector<std::string> blockIdxs;
	std::vector<std::string> threadIdxs;
	std::vector<std::string> kernel_params;
	std::map<std::string, int> array_sizes;
	if (!tostringintmap(L, 3, array_sizes))
		throw std::runtime_error("second argument must be an map[string->int]");

	if (lua_istable(L, 4)) {
		//Iterate through array (table)
		lua_pushnil(L);	// first key
		while (lua_next(L, 4) != 0) {
			// uses 'key' (at index -2) and 'value' (at index -1)
			if (strcmp(luaL_checkstring(L,-2), "block") == 0) {
				if (!tostringvector(L, lua_gettop(L), blockIdxs))
					throw std::runtime_error(
							"third argument must have a string list for it's 'block' key");
			} else if (strcmp(luaL_checkstring(L,-2), "thread") == 0) {
				if (!tostringvector(L, lua_gettop(L), threadIdxs))
					throw std::runtime_error(
							"third argument must have a string list for it's 'thread' key");
			} else {
				goto v2NotTable;
			}
			lua_pop(L, 1);
		}
	} else {
		v2NotTable: throw std::runtime_error(
				"third argument must be a table with 'block' and 'thread' as potential keys and list of indexes as values");
	}

	if (!tostringvector(L, 5, kernel_params))
		throw std::runtime_error(
				"fifth argument must have a string list for it's kernel parameters");

	myloop->cudaize_v2(stmt_num, kernel_name, array_sizes, blockIdxs,
			threadIdxs, kernel_params);
	END_REQUIRE_LOOP;
	return 0;
}

int get_loop_num(lua_State *L) {
	lua_getglobal(L, "loop");
	int loop_num = lua_tointeger(L, -1);
	lua_pop(L, 1);
	return loop_num;
}

/**
 * Register global methods available to chill scripts
 */
void register_globals(lua_State *L) {
//---
//Preset globals
//---
	lua_pushstring(L, CHILL_BUILD_VERSION);
	lua_setglobal(L, "VERSION");

	lua_pushstring(L, "C");
	lua_setglobal(L, "dest");
	lua_pushstring(L, "C");
	lua_setglobal(L, "C");

//---
//Enums for functions
//---

//TileMethod
	lua_pushinteger(L, 0);
	lua_setglobal(L, "strided");
	lua_pushinteger(L, 1);
	lua_setglobal(L, "counted");

//MemoryMode
	lua_pushinteger(L, 0);
	lua_setglobal(L, "global");
	lua_pushinteger(L, 1);
	lua_setglobal(L, "shared");
	lua_pushinteger(L, 2);
	lua_setglobal(L, "texture");

//Bool flags
	lua_pushboolean(L, 1);
	lua_setglobal(L, "sync");
//...
}

void register_functions(lua_State *L) {
	lua_register(L, "init", init);
	lua_register(L, "exit", exit);
	lua_register(L, "print_code", print_code);
	lua_register(L, "print_ri", print_ri);
	lua_register(L, "print_idx", print_idx);
	lua_register(L, "print_dep", print_dep);
	lua_register(L, "print_space", print_space);
	lua_register(L, "num_statement", num_statement);
}

void register_v2(lua_State *L) {
	lua_register(L, "cudaize", cudaize_v2);
	lua_register(L, "tile", tile_v2);
	lua_register(L, "permute", permute_v2);
	lua_register(L, "datacopy_privatized_by_ref", datacopy_privatized_v3);
	lua_register(L, "datacopy_privatized", datacopy_privatized_v2);
	lua_register(L, "datacopy", datacopy_v2);
	lua_register(L, "ELLify", ELLify_v2);
	lua_register(L, "unroll", unroll_v2);
	lua_register(L, "coalesce", flatten_v2);
	lua_register(L, "distribute", distribute_v2);
	lua_register(L, "fuse", fuse_v2);
	lua_register(L, "peel", peel_v2);
	lua_register(L, "shift_to", shift_to_v2);
	lua_register(L, "scalar_expand", scalar_expand_v2);
	lua_register(L, "reduce", reduce_v2);
	lua_register(L, "skew", skew_v2);
	lua_register(L, "split_with_alignment", split_with_alignment_v2);
	lua_register(L, "compact", compact_v2);
	lua_register(L, "make_dense", make_dense_v2);
	lua_register(L, "known", addKnown_v2);
	lua_register(L, "normalize", normalize_v2);

	lua_register(L, "cur_indices", cur_indices);
	lua_register(L, "block_indices", block_indices);
	lua_register(L, "thread_indices", thread_indices);
	lua_register(L, "block_dims", block_dims);

	lua_register(L, "thread_dims", thread_dims);
	lua_register(L, "hard_loop_bounds", hard_loop_bounds);
	lua_register(L, "num_statements", num_statement);

	lua_register(L, "does_exists", does_var_exists);
	lua_register(L, "add_sync", add_sync);
	lua_register(L, "rename_index", rename_index);

}

