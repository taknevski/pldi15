// Abstract class for wavefront implementation .
#ifndef WAVEFRONT_SPEC_HH
#define WAVEFRONTT_SPEC_HH

#include <code_gen/CG_roseRepr.h>
//#include <code_gen/CG_outputBuilder.h>

using namespace omega;

struct wavefront_dep_graph {
	bool symmetric(void){return true;}

	virtual ~wavefront_dep_graph(){};
	virtual std::string name()=0;
	virtual CG_outputRepr *convert()=0;
	virtual CG_outputRepr *dep_graph_initialize()=0;
	virtual CG_outputRepr *increment_row_count(CG_outputRepr * row)=0;
	virtual CG_outputRepr *increment_and_add_to_row(CG_outputRepr *row, CG_outputRepr *val)=0;
	virtual CG_outputRepr *dep_graph_connect_local(CG_outputRepr *src , CG_outputRepr  *dest )=0;
	virtual CG_outputRepr *dep_graph_connect_remote(CG_outputRepr *src , CG_outputRepr *dest )=0;
	virtual CG_outputRepr *eliminateDuplicateEdges()=0;
	virtual CG_outputRepr *sortEdges()=0;
	virtual CG_outputRepr *aggregate_rows(CG_outputRepr *row)=0;
	virtual CG_outputRepr *realloc_col() =0;
	virtual CG_outputRepr *realloc_col2()=0;
	virtual CG_outputRepr *free_and_reassign_col()=0;
	virtual CG_outputRepr *free_counters()=0;
	virtual CG_outputRepr *free_and_reassign_col2()=0;
	virtual CG_outputRepr *sort_by_col_final(CG_outputRepr *row)=0;
	virtual CG_outputRepr *sort_by_col(CG_outputRepr *row)=0;

	virtual CG_outputRepr *populate_col2(CG_outputRepr *row)=0;
	virtual CG_outputRepr *populate_col3(CG_outputRepr *row)=0;
	virtual CG_outputRepr *initialize_sum()=0;
	virtual CG_outputRepr *initialize_offset()=0;
	virtual CG_outputRepr *alloc_counter2() =0;
	virtual CG_outputRepr *aggregate_count(CG_outputRepr *row)=0;
	virtual CG_outputRepr *aggregate_count2(CG_outputRepr *row)=0 ;
	virtual CG_outputRepr *clear_counter(CG_outputRepr *row) =0;
	virtual CG_outputRepr *clear_counter2(CG_outputRepr *row)=0;
	virtual CG_outputRepr *symmetrify2(CG_outputRepr *row)=0;
	virtual CG_outputRepr *symmetrify(CG_outputRepr *row)=0;
	virtual CG_outputRepr *resize_and_initialize_columns(CG_outputRepr *row)=0;
   virtual CG_outputRepr *initialization_code(CG_outputRepr *code)=0;
	virtual  CG_outputRepr *initialization_code2()=0;

   /*Order:
    * 1. Initialization code (loop, OMP)
    * 2. Loop nest 1
    * 3. aggregate_rows(Loop NO OMP)
    * 4. clear_counter (Loop, OMP)
    * 5. resize_and_initialize_columns (0, NO OMP)
    * 6. loop nest 2
    * 7. initialize sum;(0, NO OMP)
    *	8.aggregate_count(loop, NO OMP)
    * 9.realloc_col (0, NO OMP)
    *10. initialize_offset (0, NO_OMP)
    *11. populate_col2 (loop, NO OMP)
    *12. free_and_reassign_col(no loop, NO OMP)
    *13. sort_by_col (loop,OMP)
    *14. clear_counter2 	(loop, OMP)
    *15. symmetrify (loop, OMP)
    *16. initialize_sum (no loop, no OMP)
    *17. aggregate count (loop, NO OMP)
    *18. realloc_col2 (no loop, NO OMP)
    *19.initialize_offset (no loop, NO OMP)
    *20.populate_col3 (loop, NO OMP)
    *21.free_and_reassign_col2(no loop, NO OMP)
    *22.symmetrify2  (loop, OMP)
    *23.sort_by_col_final(loop, OMP)
    */
};


struct wavefront_schedule {
	bool multithreaded(void){return true;}
	virtual bool is_negative_IS() = 0;
	virtual std::string name()=0;
	virtual CG_outputRepr *schedule_initialize()=0;
	virtual ~wavefront_schedule(){};
	virtual CG_outputRepr *barrier_index_repr(void)=0;
	virtual CG_outputRepr  *barrier_sync(void)=0;
	virtual CG_outputRepr  *p2p_post_sync()=0;
	virtual CG_outputRepr *p2p_wait_sync(CG_outputRepr * level, CG_outputRepr *scope)=0;
	virtual CG_outputRepr  *p2p_initialization(CG_outputRepr * scope)=0;
	virtual std::string  threadBoundaries()=0;
	virtual std::string  wavefront_boundaries()=0;
	virtual CG_outputRepr  *build_task_graph(wavefront_dep_graph *)=0;
	virtual CG_outputRepr  *perm(void)=0;
	virtual CG_outputRepr  *inv_perm(void)=0;
};



/*

class wavefront_specializer {
protected:
  omega::CG_outputBuilder *ocg_;
  omega::CG_stringBuilder ocgs;
  omega::CG_outputRepr *init_code_;
  omega::CG_outputRepr *cleanup_code_;
public:
  wavefront_specializer() {ocg_ = NULL; init_code_ = cleanup_code_ = NULL;}
  virtual ~wavefront_specializer() { delete ocg_; delete init_code_; delete cleanup_code_; }

  virtual CG_outputRepr  *task_graph_constructor(void);

  virtual CG_outputRepr  *barrier_sync(void);
  virtual CG_outputRepr  *p2p_post_sync(void);
  virtual CG_outputRepr  *p2p_wait_sync(void);

  virtual CG_outputRepr  *tasks(void);
  virtual CG_outputRepr  *parents(void);
  virtual CG_outputRepr  *task_finished(void);
  virtual CG_outputRepr  *perm(void);





  omega::CG_outputBuilder *builder() const {return ocg_;}
  omega::CG_stringBuilder builder_s() const {return ocgs;}
};
*/
#endif
