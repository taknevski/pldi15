#ifndef WAVEFRONT_INTEL_SPEC_HH
#define WAVEFRONTT_INTEL_SPEC_HH
#include "ir_code.hh"
#include "ir_rose.hh"
#include "ir_rose_utils.hh"
//#include "wavefront_specializer.hh"
using namespace SageBuilder;
using namespace SageInterface;
/*class wavefront_intel_specializer: public wavefront_specializer {
protected:
	std::string task_graph_constructor_name;

public:
	 CG_outputRepr  *task_graph_constructor(void);
	 CG_outputRepr  *dependence_graph(void);
	 CG_outputRepr  *dep_graph_connect(void);
	 CG_outputRepr  *barrier_sync(void);
	 CG_outputRepr  *p2p_post_sync(void);
	 CG_outputRepr  *p2p_wait_sync(void);

	 CG_outputRepr  *tasks(void);
	 CG_outputRepr  *parents(void);
	 CG_outputRepr  *task_finished(void);
	 CG_outputRepr  *perm(void);
};
*/


struct wavefront_intel_dep_graph:public wavefront_dep_graph {
	IR_Code * ir_;
	CG_outputBuilder *ocg_;
	SgClassSymbol *repr;
	CG_outputRepr  *repr2;
	std::string name_;
	IR_PointerSymbol *ptr;
	~wavefront_intel_dep_graph(){};
	std::string name(){return name_;}
	wavefront_intel_dep_graph(IR_Code * ir, CG_outputBuilder *ocg, std::string identifier){
		ir_ = ir;
		ocg_ = ocg;
		name_ = identifier;
	    std::vector<SgSymbolTable *> tabs = dynamic_cast<IR_roseCode *>(ir)->getsymtabs();
	    repr = tabs[0]->find_class(SgName("CSR"));
	    assert(repr);
	    SgClassDefinition *repr22 = isSgClassDeclaration(repr->get_declaration()->get_definingDeclaration())->get_definition();
	    assert(repr22);
	    repr2 = new CG_roseRepr(isSgNode(repr22->get_declaration()->get_type()));
	    ptr = ir->CreatePointerSymbol(repr2->clone(),  name_);

		//initialize repr;
	}
	wavefront_dep_graph *clone(){return  NULL;}
	CG_outputRepr *convert(){return  NULL;}
	CG_outputRepr *dep_graph_initialize(){
		return ocg_->CreateAssignment(0, ocg_->CreateIdent(name_), new CG_roseRepr(isSgExpression(buildNewExp(isSgClassType(dynamic_cast<CG_roseRepr *>(repr2)->GetCode()), 0,0,0,0,0))));

	}
	CG_outputRepr *dep_graph_connect_local(CG_outputRepr *src , CG_outputRepr  *dest ){
		SgExprListExp* args = buildExprListExp();
		//appendExpression(arg_list, op1);
		//appendExpression(arg_list, op2);;
	    appendExpression(args,dynamic_cast<CG_roseRepr *>(src)->GetExpression());
		appendExpression(args,dynamic_cast<CG_roseRepr *>(dest)->GetExpression());
		return new CG_roseRepr(isSgNode(buildExprStatement(buildFunctionCallExp(dynamic_cast<CG_roseRepr *>(dynamic_cast<CG_roseBuilder *>(ocg_)->CreateArrowRefExpression(ocg_->CreateIdent(ptr->name()),dynamic_cast<CG_roseBuilder *> (ocg_)->lookup_member_function(repr2->clone(), "connect_local")))->GetExpression(), args)))); }
	CG_outputRepr *dep_graph_connect_remote(CG_outputRepr *src , CG_outputRepr  *dest){return NULL;}
	CG_outputRepr *eliminateDuplicateEdges(){return NULL;}
	CG_outputRepr *sortEdges(){return NULL;}
	friend class Loop;
};


struct wavefront_intel_schedule:public wavefront_schedule {

	    IR_Code * ir_;
		CG_outputBuilder *ocg_;
		SgClassSymbol *repr;
		CG_outputRepr  *repr2;
		std::string name_;
		IR_PointerSymbol *ptr;
		//~wavefront_intel_dep_graph(){};
		std::string name(){return name_;}
		wavefront_intel_schedule(IR_Code * ir, CG_outputBuilder *ocg, std::string identifier){
			ir_ = ir;
			ocg_ = ocg;
			name_ = identifier;
		    std::vector<SgSymbolTable *> tabs = dynamic_cast<IR_roseCode *>(ir)->getsymtabs();
		    repr = tabs[0]->find_class(SgName("LevelSchedule"));
		    assert(repr);
		    SgClassDefinition *repr22 = isSgClassDeclaration(repr->get_declaration()->get_definingDeclaration())->get_definition();
		    assert(repr22);
		    repr2 = new CG_roseRepr(isSgNode(repr22->get_declaration()->get_type()));
		    ptr = ir->CreatePointerSymbol(repr2->clone(),  name_);

			//initialize repr;
		}
    ~wavefront_intel_schedule(){};

	CG_outputRepr *schedule_initialize(){
		return ocg_->CreateAssignment(0, ocg_->CreateIdent(name_), new CG_roseRepr(isSgExpression(buildNewExp(isSgClassType(dynamic_cast<CG_roseRepr *>(repr2)->GetCode()), 0,0,0,0,0))));

	}

	CG_outputRepr  *barrier_sync(void){return NULL;}
	CG_outputRepr  *p2p_post_sync(void){return NULL;}
	CG_outputRepr  *p2p_wait_sync(void){return NULL;}
	CG_outputRepr  *p2p_initialization(){return NULL;}
	std::string  *threadBoundaries(){return std::string("threadBoundaries");}
	std::string    *wavefront_boundaries(){return std::string("taskBoundaries");}
	CG_outputRepr  *build_task_graph(wavefront_dep_graph *A){
		SgExprListExp* args = buildExprListExp();
	//appendExpression(arg_list, op1);
	//appendExpression(arg_list, op2);;
    appendExpression(args,buildPointerDerefExp(buildVarRefExp(A->name())));
    return new CG_roseRepr(isSgNode(buildExprStatement(buildFunctionCallExp(dynamic_cast<CG_roseRepr *>(dynamic_cast<CG_roseBuilder *>(ocg_)->CreateArrowRefExpression(ocg_->CreateIdent(ptr->name()),dynamic_cast<CG_roseBuilder *> (ocg_)->lookup_member_function(repr2->clone(), "constructTaskGraph")))->GetExpression(), args)))); }
	CG_outputRepr  *perm(void){return NULL;}
	friend class Loop;
};
#endif
