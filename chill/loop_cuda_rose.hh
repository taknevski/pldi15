#ifndef LOOP_CUDA_HH
#define LOOP_CUDA_HH

#include "loop.hh"
#include <string.h>
#include "rose.h"

using namespace omega;
enum MemoryMode {
	GlobalMem, SharedMem, TexMem
};

//protonu --class introduced to hold texture memory information in one single place
//this might help me get over the weird memory issues I am having with the Loop class
//where someone/something corrupts my memory
class texture_memory_mapping {
private:
	bool tex_mem_used;
	std::vector<std::string> tex_mapped_array_name;
public:
	texture_memory_mapping(bool used, const char * array_name) {
		tex_mem_used = used;
		tex_mapped_array_name.push_back(std::string(array_name));
	}

	void add(const char * array_name) {
		tex_mapped_array_name.push_back(std::string(array_name));
	}

	bool is_tex_mem_used() {
		return tex_mem_used;
	}
	bool is_array_tex_mapped(const char * array_name) {

		for (int i = 0; i < tex_mapped_array_name.size(); i++) {
			if (!(strcmp(array_name, tex_mapped_array_name[i].c_str())))
				return true;
		}
		return false;
	}
	texture_memory_mapping() {
		tex_mem_used = false;
	}
};

class LoopCuda: public Loop {

public:
	//std::vector<proc_sym*> new_procs; //Need adding to a fse
	std::vector<std::vector<std::string> > idxNames;
	std::vector<std::pair<int, std::string> > syncs;
	bool useIdxNames;
	std::vector<std::string> index;
	std::vector<std::set< int> > cudaized;
	SgSymbolTable* symtab;
	SgSymbolTable* parameter_symtab;
	SgSymbolTable* body_symtab;
	SgGlobal* globals;
	SgGlobal* globalScope;
	SgScopeStatement* func_body;
	SgFunctionDefinition* func_definition;

	//Anand: Adding a placeholder for variables that
	//will be passed as parameters to cuda kernel function
	//could be possibly modified by cudaize
	std::set<std::string> kernel_parameters;

	//protonu--inserting this here, Gabe's implementation had it
	//the struct statment as nonSplitLevels
	std::vector<std::vector<int> > stmt_nonSplitLevels;

	texture_memory_mapping *texture; //protonu
	std::vector<std::map<std::string, int> >array_dims;
	omega::CG_outputRepr *setup_code;
	omega::CG_outputRepr *teardown_code;

	unsigned int code_gen_flags;
	enum CodeGenFlags {
		GenInit = 0x00, GenCudaizeV2 = 0x02,
	};

	//varibles used by cudaize_codegen
	//block x, y sizes, N and num_red
	std::vector<int> cu_bx, cu_by;
	int cu_n, cu_num_reduce;
	//block statement and level
	int cu_block_stmt, cu_block_level;
	//thread x, y, z
	std::vector<int> cu_tx, cu_ty, cu_tz;

	//Anand: adding map of blockids and threadids per statements that are cudaized

	std::map<int, std::vector<int> > block_and_thread_levels;
	//Anand: Adding CG_outputRepr* representations of cu_bx, cu_by, cu_tx, cu_ty
	//and cu_tz for non constant loop bounds

	std::vector<CG_outputRepr *> cu_bx_repr, cu_by_repr, cu_tx_repr, cu_ty_repr, cu_tz_repr;

	//tile statements, and loop-levels (cudaize v1)
	std::vector<std::vector<int> > cu_thread_loop;
	std::vector<int> cu_thread_sync;
	MemoryMode cu_mode;

	std::string cu_nx_name, cu_ny_name;

	std::vector<std::string> cu_kernel_name;
	int nonDummyLevel(int stmt, int level);
	bool symbolExists(std::string s);
	void addSync(int stmt, std::string idx);
	void renameIndex(int stmt, std::string idx, std::string newName);
	bool validIndexes(int stmt, const std::vector<std::string>& idxs);
	CG_outputRepr* extractCudaUB(int stmt_num, int level, int &outUpperBound,
			int &outLowerBound);

	void printCode(int stmt_num, int effort = 2,
			bool actuallyPrint = true) const;
	void printRuntimeInfo() const;
	void printIndexes() const;
	SgNode* getCode(int effort, std::set<int> stmts) const;
	void printIS();

	void permute_cuda(int stmt, const std::vector<std::string>& curOrder);
	//protonu-writing a wrapper for the Chun's new permute function
	bool permute(int stmt_num, const std::vector<int> &pi);
	//end--protonu.
	void tile_cuda(int stmt, int level, int outer_level, TilingMethodType method = CountedTile);
	void tile_cuda(int level, int tile_size, int outer_level,
			std::string idxName, std::string ctrlName, TilingMethodType method =
					StridedTile);
	void tile_cuda(int stmt, int level, int tile_size, int outer_level,
			std::string idxName, std::string ctrlName, TilingMethodType method =
					StridedTile);
	bool datacopy_privatized_cuda(int stmt_num, int level,
			const std::string &array_name,
			const std::vector<int> &privatized_levels, bool allow_extra_read =
					false, int fastest_changing_dimension = -1,
			int padding_stride = 1, int padding_alignment = 1,
			bool cuda_shared = false);
	bool datacopy_privatized_cuda(int stmt_num, int level,
			const std::vector<std::pair<int, std::vector<int> > > &array_ref_nums,
			const std::vector<int> &privatized_levels, bool allow_extra_read =
					false, int fastest_changing_dimension = -1,
			int padding_stride = 1, int padding_alignment = 1,
			bool cuda_shared = false);
	bool datacopy_cuda(int stmt_num, int level, const std::string &array_name,
			std::vector<std::string> new_idxs, bool allow_extra_read = false,
			int fastest_changing_dimension = -1, int padding_stride = 1,
			int padding_alignment = 4, bool cuda_shared = false);
	bool unroll_cuda(int stmt_num, int level, int unroll_amount);
	//protonu--using texture memory
	void copy_to_texture(const char *array_name);
	void flatten_cuda(int stmt_num, std::string idxs, std::vector<int> &loop_levels, std::string inspector_name);
	void ELLify_cuda(int stmt_num, std::vector<std::string> arrays_to_pad, int pad_to,bool dense_pad, std::string pos_array_name);
	void distribute_cuda(std::vector<int> &stmt_nums, int loop_level);
	void fuse_cuda(std::vector<int> &stmt_nums, int loop_level);
	void peel_cuda(int stmt_num, int level, int amount);
	void shift_to_cuda(int stmt_num, int level, int absolute_position);
	void scalar_expand_cuda(int stmt_num, std::vector<int> level, std::string arrName, int memory_type =0, int padding =0,int assign_then_accumulate = 1);
	void split_with_alignment_cuda(int stmt_num, int level, int alignment, int direction=0);
	void reduce_cuda(int stmt_num, std::vector<int> level, int param, std::string func_name,  std::vector<int> seq_level, int bound_level=-1);
	void compact_cuda(int stmt_num, std::vector<int> level, std::vector<std::string> new_array, int zero,
				std::vector<std::string> data_array, Relation rel = Relation::True(1), bool max_allocate=false);
	void make_dense_cuda(int stmt_num, int loop_level, std::string new_loop_index);
	void addKnown_cuda(std::string var, int value);
	void normalize_cuda(int stmt_num, int level);
	void skew_cuda(std::vector<int> stmt_num,int level, std::vector<int> coefs);

	Relation rel_help(std::vector<int> str_coefs, std::vector<std::string> str_vals, int constants, bool eq_or_geq = true);

	int findCurLevel(int stmt, std::string idx);
	/**
	 *
	 * @param kernel_name Name of the GPU generated kernel
	 * @param nx Iteration space over the x dimention
	 * @param ny Iteration space over the y dimention
	 * @param tx Tile dimention over x dimention
	 * @param ty Tile dimention over the y dimention
	 * @param num_reduce The number of dimentions to reduce by mapping to the GPU implicit blocks/threads
	 */
	//stmnt_num is referenced from the perspective of being inside the cudaize block loops
	//Anand : 06/18/2013 adding kernel_params to enforce cuda kernel arguments
	//as opposed to creating them on the stack
	bool cudaize_v2(int stmt_num, std::string kernel_name,
			std::map<std::string, int> array_dims,
			std::vector<std::string> blockIdxs,
			std::vector<std::string> threadIdxs,std::vector<std::string> kernel_params);
	//Anand: 06/15/2013 changing the following function return types to void from SgNode*
	void cudaize_codegen_v2();
	void codegen();

	//protonu--have to add the constructors for the new class
	//and maybe destructors (?)
	LoopCuda();
	//LoopCuda(IR_Code *ir, tree_for *tf, global_symtab* gsym);
	LoopCuda(IR_Control *ir_c, int loop_num);//protonu-added so as to not change ir_suif
	~LoopCuda();

};

#endif
