/*

 * Loop_sparse.cc
 *  Loop Class for Sparse Matrix Format Transformations
 *  Created on: October 7, 2013
 *      Author: Anand Venkat
 */

#include "loop.hh"
#include "chill_error.hh"
#include <omega.h>
#include "omegatools.hh"
#include <string.h>
#include <code_gen/CG_roseBuilder.h>
#include <code_gen/CG_utils.h>
#include "ir_rose.hh"
#include "ir_rose_utils.hh"
#include <iostream>
#include <sstream>
#include "iegenlib.h"
using namespace std;
//#using iegenlib::Set;
//#using iegenlib::Relation;
#define TRANSFORMATION_FILE_INFO Sg_File_Info::generateDefaultFileInfoForTransformationNode()
using namespace omega;
using namespace SageBuilder;
using namespace SageInterface;

struct wavefront_intel_dep_graph: public wavefront_dep_graph {
	IR_Code * ir_;
	CG_outputBuilder *ocg_;
	CG_outputRepr *vertex_count;
	SgNamespaceSymbol *ns;
	SgClassSymbol *repr;
	CG_outputRepr *repr2;
	std::string name_;
	IR_PointerSymbol *ptr;
	IR_PointerSymbol *row_counter, *row_counter2;
	IR_PointerSymbol *temp_col, *temp_col2;
	CG_outputRepr *rowptr, *diagptr, *colidx, *extptr, *_m;
        CG_outputRepr *inspection_done;
	~wavefront_intel_dep_graph() {
	}
	;
	std::string name() {
		return name_;
	}
	std::vector<SgSymbolTable *> tabs;
	wavefront_intel_dep_graph(IR_Code * ir, CG_outputBuilder *ocg,
			std::string identifier, CG_outputRepr *m, Loop *loop, CG_outputRepr *inspection_var) {
		ir_ = ir;
		ocg_ = ocg;
		name_ = identifier;
                inspection_done = inspection_var;
		tabs = dynamic_cast<IR_roseCode *>(ir)->getsymtabs();
		ns = tabs[0]->find_namespace(SgName("SpMP"));
		SgNamespaceDefinitionStatement *nsp =
				ns->get_declaration()->get_definition();
		repr = ns->get_declaration()->get_definition()->lookup_class_symbol(
				SgName("CSR"));
		std::vector<CG_outputRepr *> size_repr;
             
		vertex_count = m->clone();
		size_repr.push_back(vertex_count->clone());
		row_counter = ir->CreatePointerSymbol(IR_CONSTANT_INT, size_repr);
		row_counter2 = ir->CreatePointerSymbol(IR_CONSTANT_INT, size_repr);
		temp_col = ir->CreatePointerSymbol(IR_CONSTANT_INT, size_repr);
		temp_col2 = ir->CreatePointerSymbol(IR_CONSTANT_INT, size_repr);
		CG_outputRepr *init_code = 
				ir->CreateMalloc(IR_CONSTANT_INT, row_counter->name(), m->clone());
		init_code = ocg->StmtListAppend(init_code,
				ir->CreateMalloc(IR_CONSTANT_INT, row_counter2->name(),
						m->clone()));
                loop->init_code = ocg->StmtListAppend(loop->init_code, ocg->CreateIf(0, ocg->CreateEQ(inspection_done->clone(),ocg->CreateInt(0)), init_code,NULL));
                CG_outputRepr *free_code =
                                ir->CreateFree(ocg->CreateIdent(row_counter->name()));
                free_code = ocg->StmtListAppend(free_code, ir->CreateFree(ocg->CreateIdent(row_counter2->name())));
              //loop->cleanup_code = ocg->StmtListAppend(loop->cleanup_code, ocg->CreateIf(0, ocg->CreateEQ(inspection_done,ocg->CreateInt(0)), free_code,NULL));

		nsp = nsp->get_nextNamepaceDefinition();
		while (!repr && nsp) {

			SgDeclarationStatementPtrList lst = nsp->get_declarations();
			//SgDeclarationStatementPtrList::iterator  t = lst.begin();
			//bool found = false;
			for (SgDeclarationStatementPtrList::iterator t = lst.begin();
					t != lst.end(); t++) {
				if (isSgClassDeclaration(*t)) {
					repr =
							isSgClassSymbol(
									isSgClassDeclaration(*t)->search_for_symbol_from_symbol_table());

					if (repr->get_name().getString() != "CSR") {
						repr = NULL;

					} else {
						break;
					}

				}
			}
			nsp = nsp->get_nextNamepaceDefinition();
		}

		nsp = ns->get_declaration()->get_definition();
		nsp = nsp->get_previousNamepaceDefinition();
		while (!repr && nsp) {

			SgDeclarationStatementPtrList lst = nsp->get_declarations();
			//SgDeclarationStatementPtrList::iterator  t = lst.begin();

			for (SgDeclarationStatementPtrList::iterator t = lst.begin();
					t != lst.end(); t++) {
				if (isSgClassDeclaration(*t)) {
					repr =
							isSgClassSymbol(
									isSgClassDeclaration(*t)->search_for_symbol_from_symbol_table());

					if (repr->get_name().getString() != "CSR") {
						repr = NULL;

					} else {
						break;
					}

				}
			}
			nsp = nsp->get_previousNamepaceDefinition();
		}

		assert(repr);

		SgClassDefinition *repr22 =
				isSgClassDeclaration(
						repr->get_declaration()->get_definingDeclaration())->get_definition();
		assert(repr22);
		repr2 = new CG_roseRepr(isSgNode(repr22->get_declaration()->get_type()));
		ptr = ir->CreatePointerSymbol(repr2->clone(), name_);

		rowptr = dynamic_cast<CG_roseBuilder *>(ocg_)->CreateArrowRefExpression(
				ocg_->CreateIdent(ptr->name()),
				dynamic_cast<CG_roseBuilder *>(ocg_)->lookup_member_data(
						repr2->clone(), "rowptr", ocg_->CreateIdent(ptr->name())));
                free_code = ocg->StmtListAppend(free_code, ir->CreateFree(rowptr->clone()));

		diagptr = dynamic_cast<CG_roseBuilder *>(ocg_)->CreateArrowRefExpression(
				ocg_->CreateIdent(ptr->name()),
				dynamic_cast<CG_roseBuilder *>(ocg_)->lookup_member_data(
						repr2->clone(), "diagptr", ocg_->CreateIdent(ptr->name())));
                free_code = ocg->StmtListAppend(free_code, ir->CreateFree(diagptr->clone()));

		extptr = dynamic_cast<CG_roseBuilder *>(ocg_)->CreateArrowRefExpression(
				ocg_->CreateIdent(ptr->name()),
				dynamic_cast<CG_roseBuilder *>(ocg_)->lookup_member_data(
						repr2->clone(), "extptr", ocg_->CreateIdent(ptr->name())));

		colidx = dynamic_cast<CG_roseBuilder *>(ocg_)->CreateArrowRefExpression(
				ocg_->CreateIdent(ptr->name()),
				dynamic_cast<CG_roseBuilder *>(ocg_)->lookup_member_data(
						repr2->clone(), "colidx", ocg_->CreateIdent(ptr->name())));

                free_code = ocg->StmtListAppend(free_code, ir->CreateFree(colidx->clone()));

                _m = dynamic_cast<CG_roseBuilder *>(ocg_)->CreateArrowRefExpression(
                                ocg_->CreateIdent(ptr->name()),
                                dynamic_cast<CG_roseBuilder *>(ocg_)->lookup_member_data(
                                                repr2->clone(), "m", ocg_->CreateIdent(ptr->name())));
                
		//initialize repr;

                loop->cleanup_code = ocg->StmtListAppend(loop->cleanup_code, ocg->CreateIf(0, ocg->CreateEQ(inspection_done,ocg->CreateInt(0)), free_code,NULL));

	}
	wavefront_dep_graph *clone() {
		return NULL;
	}
	CG_outputRepr *convert() {
		return NULL;
	}

	CG_outputRepr *increment_row_count(CG_outputRepr * row) {
		CG_outputRepr *conditional = ocg_->CreateEQ(ocg_->CreateIdent("found"),
				ocg_->CreateInt(1));
		CG_outputRepr *body = ocg_->CreateAssignment(0,
				ocg_->CreateArrayRefExpression(row_counter->name(), row->clone()),
				ocg_->CreatePlus(
						ocg_->CreateArrayRefExpression(row_counter->name(),
								row->clone()), ocg_->CreateInt(1)));

		CG_outputRepr *to_return = ocg_->CreateIf(0, conditional, body, NULL);

		return to_return;

	}

	CG_outputRepr *aggregate_rows(CG_outputRepr *row) {

		CG_outputRepr *body = ocg_->CreateAssignment(0,
				ocg_->CreateArrayRefExpression(rowptr->clone(),
						ocg_->CreatePlus(row->clone(), ocg_->CreateInt(1))),

				ocg_->CreatePlus(
						ocg_->CreateArrayRefExpression(rowptr->clone(), row->clone()),
						ocg_->CreateArrayRefExpression(row_counter->name(),
								row->clone())));

		return body;

	}

	CG_outputRepr *realloc_col() {

		CG_outputRepr *body = ir_->CreateMalloc(IR_CONSTANT_INT, temp_col->name(),
				ocg_->CreateIdent("acc"));

		return body;

	}

	CG_outputRepr *realloc_col2() {

		CG_outputRepr *body = ir_->CreateMalloc(IR_CONSTANT_INT,
				temp_col2->name(),
				ocg_->CreatePlus(
						ocg_->CreateArrayRefExpression(rowptr->clone(),
								vertex_count->clone()), ocg_->CreateIdent("acc")));

		return body;

	}

	CG_outputRepr *free_and_reassign_col() {

		CG_outputRepr *body = ir_->CreateFree(colidx->clone());
		CG_outputRepr *body2 = ocg_->CreateAssignment(0, colidx->clone(),
				ocg_->CreateIdent(temp_col->name()));

		return ocg_->StmtListAppend(body, body2);

	}

	CG_outputRepr *free_counters() {

		CG_outputRepr *body = ir_->CreateFree(
				ocg_->CreateIdent(row_counter->name()));
		CG_outputRepr *body2 = ir_->CreateFree(
				ocg_->CreateIdent(row_counter2->name()));
		return ocg_->StmtListAppend(body, body2);

	}
	CG_outputRepr *free_and_reassign_col2() {

		CG_outputRepr *body = ir_->CreateFree(colidx->clone());
		CG_outputRepr *body2 = ocg_->CreateAssignment(0, colidx->clone(),
				ocg_->CreateIdent(temp_col2->name()));

		return ocg_->StmtListAppend(body, body2);

	}

	CG_outputRepr *sort_by_col_final(CG_outputRepr *row) {

		SgName name_find("sort");
		SgFunctionDeclaration *find = buildNondefiningFunctionDeclaration(
				name_find, buildVoidType(), buildFunctionParameterList(),
				static_cast<IR_roseCode *>(ir_)->get_root());
		SgExprListExp *args = buildExprListExp();
		args->append_expression(
				static_cast<CG_roseRepr *>(ocg_->CreatePlus(colidx->clone(),
						ocg_->CreateArrayRefExpression(rowptr->clone(), row->clone())))->GetExpression());

		args->append_expression(
				static_cast<CG_roseRepr *>(ocg_->CreatePlus(colidx->clone(),
						ocg_->CreateArrayRefExpression(rowptr->clone(),
								ocg_->CreatePlus(row->clone(), ocg_->CreateInt(1)))))->GetExpression());

		SgFunctionCallExp *the_call = buildFunctionCallExp(
				buildFunctionRefExp(find), args);
		SgExprStatement* stmt = buildExprStatement(the_call);
		CG_outputRepr *body = new CG_roseRepr(isSgNode(stmt));

		CG_outputRepr *inductive = ocg_->CreateInductive(ocg_->CreateIdent("j"),
				ocg_->CreateArrayRefExpression(rowptr->clone(), row->clone()),
				ocg_->CreateMinus(
						ocg_->CreateArrayRefExpression(rowptr->clone(),
								ocg_->CreatePlus(row->clone(), ocg_->CreateInt(1))),
						ocg_->CreateInt(1)),
				NULL);

		CG_outputRepr *condition = ocg_->CreateEQ(
				ocg_->CreateArrayRefExpression(colidx->clone(),
						ocg_->CreateIdent("j")), row->clone());
		CG_outputRepr *body2 = ocg_->CreateAssignment(0,
				ocg_->CreateArrayRefExpression(diagptr->clone(), row->clone()),
				ocg_->CreateIdent("j"));

		body2 = ocg_->CreateIf(0, condition, body2, NULL);

		body2 = ocg_->CreateLoop(0, inductive, body2);

		return ocg_->StmtListAppend(body, body2);

		//return body;

	}

	CG_outputRepr *sort_by_col(CG_outputRepr *row) {

		SgName name_find("sort");
		SgFunctionDeclaration *find = buildNondefiningFunctionDeclaration(
				name_find, buildVoidType(), buildFunctionParameterList(),
				static_cast<IR_roseCode *>(ir_)->get_root());
		SgExprListExp *args = buildExprListExp();
		args->append_expression(
				static_cast<CG_roseRepr *>(ocg_->CreatePlus(colidx->clone(),
						ocg_->CreateArrayRefExpression(rowptr->clone(), row->clone())))->GetExpression());

		args->append_expression(
				static_cast<CG_roseRepr *>(ocg_->CreatePlus(colidx->clone(),
						ocg_->CreateArrayRefExpression(rowptr->clone(),
								ocg_->CreatePlus(row->clone(), ocg_->CreateInt(1)))))->GetExpression());

		SgFunctionCallExp *the_call = buildFunctionCallExp(
				buildFunctionRefExp(find), args);
		SgExprStatement* stmt = buildExprStatement(the_call);
		CG_outputRepr *body = new CG_roseRepr(isSgNode(stmt));

		return body;

	}

	CG_outputRepr *populate_col2(CG_outputRepr *row) {

		SgName name_find("memcpy");
		SgFunctionDeclaration *find = buildNondefiningFunctionDeclaration(
				name_find, buildVoidType(), buildFunctionParameterList(),
				static_cast<IR_roseCode *>(ir_)->get_root());
		SgExprListExp *args = buildExprListExp();

		args->append_expression(
				static_cast<CG_roseRepr *>(ocg_->CreatePlus(
						ocg_->CreateIdent(temp_col->name()),
						ocg_->CreateArrayRefExpression(rowptr->clone(), row->clone())))->GetExpression());

		args->append_expression(
				static_cast<CG_roseRepr *>(ocg_->CreatePlus(colidx->clone(),
						ocg_->CreateIdent("source_offset")))->GetExpression());
		SgExpression *tmp =

		static_cast<CG_roseRepr *>(ocg_->CreateArrayRefExpression(
				row_counter->name(), row->clone()))->GetExpression();

		tmp = buildMultiplyOp(tmp, buildSizeOfOp(buildIntType()));
		args->append_expression(tmp);

		SgFunctionCallExp *the_call = buildFunctionCallExp(
				buildFunctionRefExp(find), args);
		SgExprStatement* stmt = buildExprStatement(the_call);
		CG_outputRepr *body = new CG_roseRepr(isSgNode(stmt));

		CG_outputRepr *three = ocg_->CreateAssignment(0,
				ocg_->CreateIdent("source_offset"),
				ocg_->CreateArrayRefExpression(rowptr->clone(),
						ocg_->CreatePlus(row->clone(), ocg_->CreateInt(1))));
		CG_outputRepr *four = ocg_->CreateAssignment(0,
				ocg_->CreateArrayRefExpression(rowptr->clone(),
						ocg_->CreatePlus(row->clone(), ocg_->CreateInt(1))),
				ocg_->CreatePlus(
						ocg_->CreateArrayRefExpression(rowptr->clone(), row->clone()),
						ocg_->CreateArrayRefExpression(row_counter->name(),
								row->clone())));

		body = ocg_->StmtListAppend(body, three);
		body = ocg_->StmtListAppend(body, four);

		return body;
		/*		   int source_offset=0;
		 for (int i = 0; i < m; ++i) {
		 memcpy(
		 col + gr->rowptr[i], gr->colidx + source_offset,
		 count[i]*sizeof(int));
		 source_offset = gr->rowptr[i+1];
		 gr->rowptr[i+1] = gr->rowptr[i] + count[i];

		 }
		 */

		return body;

	}

	CG_outputRepr *populate_col3(CG_outputRepr *row) {

		SgName name_find("memcpy");
		SgFunctionDeclaration *find = buildNondefiningFunctionDeclaration(
				name_find, buildVoidType(), buildFunctionParameterList(),
				static_cast<IR_roseCode *>(ir_)->get_root());
		SgExprListExp *args = buildExprListExp();

		args->append_expression(
				static_cast<CG_roseRepr *>(ocg_->CreatePlus(
						ocg_->CreateIdent(temp_col2->name()),
						ocg_->CreateArrayRefExpression(rowptr->clone(), row->clone())))->GetExpression());

		args->append_expression(
				static_cast<CG_roseRepr *>(ocg_->CreatePlus(colidx->clone(),
						ocg_->CreateIdent("source_offset")))->GetExpression());
		SgExpression *tmp =

				static_cast<CG_roseRepr *>(ocg_->CreatePlus(
						ocg_->CreateArrayRefExpression(row_counter->name(),
								row->clone()),
						ocg_->CreateArrayRefExpression(row_counter2->name(),
								row->clone())))->GetExpression();

		tmp = buildMultiplyOp(tmp, buildSizeOfOp(buildIntType()));
		args->append_expression(tmp);

		SgFunctionCallExp *the_call = buildFunctionCallExp(
				buildFunctionRefExp(find), args);
		SgExprStatement* stmt = buildExprStatement(the_call);
		CG_outputRepr *body = new CG_roseRepr(isSgNode(stmt));

		CG_outputRepr *one = ocg_->CreateAssignment(0, ocg_->CreateIdent("tmp"),
				ocg_->CreateArrayRefExpression(row_counter2->name(), row->clone()));

		CG_outputRepr *two = ocg_->CreateAssignment(0,
				ocg_->CreateArrayRefExpression(row_counter2->name(), row->clone()),
				ocg_->CreateMinus(
						ocg_->CreateArrayRefExpression(rowptr->clone(),
								ocg_->CreatePlus(row->clone(), ocg_->CreateInt(1))),
						ocg_->CreateIdent("source_offset")));
		CG_outputRepr *three = ocg_->CreateAssignment(0,
				ocg_->CreateIdent("source_offset"),
				ocg_->CreateArrayRefExpression(rowptr->clone(),
						ocg_->CreatePlus(row->clone(), ocg_->CreateInt(1))));
		CG_outputRepr *four = ocg_->CreateAssignment(0,
				ocg_->CreateArrayRefExpression(rowptr->clone(),
						ocg_->CreatePlus(row->clone(), ocg_->CreateInt(1))),
				ocg_->CreatePlus(
						ocg_->CreatePlus(
								ocg_->CreateArrayRefExpression(rowptr->clone(),
										row->clone()),
								ocg_->CreateArrayRefExpression(row_counter->name(),
										row->clone())), ocg_->CreateIdent("tmp")));

		body = ocg_->StmtListAppend(body, one);
		body = ocg_->StmtListAppend(body, two);
		body = ocg_->StmtListAppend(body, three);
		body = ocg_->StmtListAppend(body, four);

		return body;
		/*		   int source_offset=0;
		 for (int i = 0; i < m; ++i) {
		 memcpy(
		 col + gr->rowptr[i], gr->colidx + source_offset,
		 count[i]*sizeof(int));
		 source_offset = gr->rowptr[i+1];
		 gr->rowptr[i+1] = gr->rowptr[i] + count[i];

		 }
		 */

		return body;

	}

	CG_outputRepr *initialize_sum() {

		CG_outputRepr *body = ocg_->CreateAssignment(0, ocg_->CreateIdent("acc"),
				ocg_->CreateInt(0));

		return body;

	}
	CG_outputRepr *initialize_offset() {

		CG_outputRepr *body = ocg_->CreateAssignment(0,
				ocg_->CreateIdent("source_offset"), ocg_->CreateInt(0));

		return body;

	}
	CG_outputRepr *alloc_counter2() {

		CG_outputRepr *body = ir_->CreateMalloc(IR_CONSTANT_INT,
				row_counter2->name(), vertex_count->clone());
		return body;

	}
	CG_outputRepr *aggregate_count(CG_outputRepr *row) {

		CG_outputRepr *body = ocg_->CreatePlusAssignment(0,
				ocg_->CreateIdent("acc"),
				ocg_->CreateArrayRefExpression(row_counter->name(), row->clone()));

		return body;

	}

	CG_outputRepr *aggregate_count2(CG_outputRepr *row) {

		CG_outputRepr *body = ocg_->CreatePlusAssignment(0,
				ocg_->CreateIdent("acc"),
				ocg_->CreateArrayRefExpression(row_counter2->name(), row->clone()));

		return body;

	}

	CG_outputRepr *clear_counter(CG_outputRepr *row) {

		CG_outputRepr *body = ocg_->CreateAssignment(0,

		ocg_->CreateArrayRefExpression(row_counter->name(), row->clone()),
				ocg_->CreateInt(0));

		return body;

	}

	CG_outputRepr *clear_counter2(CG_outputRepr *row) {

		CG_outputRepr *body = ocg_->CreateAssignment(0,

		ocg_->CreateArrayRefExpression(row_counter2->name(), row->clone()),
				ocg_->CreateInt(0));

		return body;

	}

	CG_outputRepr *symmetrify2(CG_outputRepr *row) {

		CG_outputRepr *body;

		/*
		 #pragma omp parallel for
		 for (int i = 0; i < m; i++) {
		 if (!binary_search(gr->colidx + gr->rowptr[i], gr->colidx + gr->rowptr[i+1], i)) {
		 // for each (i, c), add (c, i)
		 __sync_fetch_and_add(count2 + i, 1);
		 }


		 for (int j = gr->rowptr[i]; j < gr->rowptr[i+1]; ++j) {
		 int c = gr->colidx[j];
		 // assume colidx is sorted
		 if (!binary_search(gr->colidx + gr->rowptr[c], gr->colidx + gr->rowptr[c+1], i)) {
		 // for each (i, c), add (c, i)
		 __sync_fetch_and_add(count2 + c, 1);
		 }
		 }
		 }
		 */

		SgName name_find("binary_search");
		SgFunctionDeclaration *find = buildNondefiningFunctionDeclaration(
				name_find, buildVoidType(), buildFunctionParameterList(),
				static_cast<IR_roseCode *>(ir_)->get_root());
		SgExprListExp *args = buildExprListExp();

		args->append_expression(
				static_cast<CG_roseRepr *>(ocg_->CreatePlus(colidx->clone(),
						ocg_->CreateArrayRefExpression(rowptr->clone(), row->clone())))->GetExpression());

		args->append_expression(
				static_cast<CG_roseRepr *>(ocg_->CreatePlus(
						ocg_->CreatePlus(colidx->clone(),
								ocg_->CreateArrayRefExpression(rowptr->clone(),
										row->clone())),
						ocg_->CreateArrayRefExpression(row_counter2->name(),
								row->clone())))->GetExpression());

		args->append_expression(
				static_cast<CG_roseRepr *>(row->clone())->GetExpression());

		SgFunctionCallExp *the_call = buildFunctionCallExp(
				buildFunctionRefExp(find), args);
		CG_outputRepr *exp = new CG_roseRepr(
				isSgExpression(buildNotOp(the_call)));

		SgName name_find2("__sync_fetch_and_add");
		SgFunctionDeclaration *find2 = buildNondefiningFunctionDeclaration(
				name_find2, buildVoidType(), buildFunctionParameterList(),
				static_cast<IR_roseCode *>(ir_)->get_root());
		SgExprListExp *args2 = buildExprListExp();

		args2->append_expression(
				static_cast<CG_roseRepr *>(ocg_->CreatePlus(
						ocg_->CreateIdent(row_counter->name()), row->clone()))->GetExpression());

		args2->append_expression(
				static_cast<CG_roseRepr *>(ocg_->CreateInt(1))->GetExpression());

		SgFunctionCallExp *the_call2 = buildFunctionCallExp(
				buildFunctionRefExp(find2), args2);

		CG_outputRepr *body2 = ocg_->CreateAssignment(0, ocg_->CreateIdent("cnt"),
				new CG_roseRepr(the_call2)); //new CG_roseRepr(isSgNode(stmt));
		CG_outputRepr *second = ocg_->CreateAssignment(0,
				ocg_->CreateArrayRefExpression(colidx->clone(),
						ocg_->CreatePlus(
								ocg_->CreateArrayRefExpression(rowptr, row->clone()),
								ocg_->CreateIdent("cnt"))), row->clone());
		body2 = ocg_->StmtListAppend(body2, second);
		body2 = ocg_->CreateIf(0, exp, body2, NULL);

//		SgExprStatement* stmt = buildExprStatement(the_call2);
//		CG_outputRepr *body2 = new CG_roseRepr(isSgNode(stmt));
		args = buildExprListExp();

		args->append_expression(
				static_cast<CG_roseRepr *>(ocg_->CreatePlus(colidx->clone(),
						ocg_->CreateArrayRefExpression(rowptr->clone(),
								ocg_->CreateIdent("c"))))->GetExpression());

		args->append_expression(
				static_cast<CG_roseRepr *>(ocg_->CreatePlus(
						ocg_->CreatePlus(colidx->clone(),
								ocg_->CreateArrayRefExpression(rowptr->clone(),
										ocg_->CreateIdent("c"))),
						ocg_->CreateArrayRefExpression(row_counter2->name(),
								ocg_->CreateIdent("c"))))->GetExpression());

		args->append_expression(
				static_cast<CG_roseRepr *>(row->clone())->GetExpression());

		the_call = buildFunctionCallExp(buildFunctionRefExp(find), args);
		exp = new CG_roseRepr(isSgExpression(buildNotOp(the_call)));
		args2 = buildExprListExp();

		args2->append_expression(
				static_cast<CG_roseRepr *>(ocg_->CreatePlus(
						ocg_->CreateIdent(row_counter->name()),
						ocg_->CreateIdent("c")))->GetExpression());

		args2->append_expression(
				static_cast<CG_roseRepr *>(ocg_->CreateInt(1))->GetExpression());

		the_call2 = buildFunctionCallExp(buildFunctionRefExp(find2), args2);
		CG_outputRepr *body3 = ocg_->CreateAssignment(0, ocg_->CreateIdent("cnt"),
				new CG_roseRepr(the_call2));
		//SgExprStatement* stmt = buildExprStatement(the_call2);

		//  CG_outputRepr* three =  ocg_->CreateIf(0,exp, body2, NULL);
		//three= ocg_->StmtListAppend(two, three);

		CG_outputRepr *two = ocg_->CreateAssignment(0, ocg_->CreateIdent("c"),
				ocg_->CreateArrayRefExpression(colidx->clone(),
						ocg_->CreateIdent("j")));
		CG_outputRepr *four = ocg_->CreateAssignment(0,
				ocg_->CreateArrayRefExpression(colidx->clone(),
						ocg_->CreatePlus(
								ocg_->CreateArrayRefExpression(rowptr,
										ocg_->CreateIdent("c")),
								ocg_->CreateIdent("cnt"))), row->clone());

		body3 = ocg_->StmtListAppend(body3, four);

		CG_outputRepr* three = ocg_->CreateIf(0, exp, body3, NULL);

		three = ocg_->StmtListAppend(two, three);
		CG_outputRepr *inductive = ocg_->CreateInductive(ocg_->CreateIdent("j"),
				ocg_->CreateArrayRefExpression(rowptr->clone(), row->clone()),
				ocg_->CreateMinus(
						ocg_->CreatePlus(
								ocg_->CreateArrayRefExpression(rowptr->clone(),
										row->clone()),
								ocg_->CreateArrayRefExpression(row_counter2->name(),
										row->clone())), ocg_->CreateInt(1)), NULL);

		three = ocg_->CreateLoop(0, inductive, three);

		return ocg_->StmtListAppend(body2, three);

	}

	CG_outputRepr *initialization_code(CG_outputRepr *row) {

		CG_outputRepr *body = ocg_->CreateAssignment(0,
				ocg_->CreateArrayRefExpression(rowptr->clone(), row->clone()),
				ocg_->CreateInt(0));
		CG_outputRepr *body2 = ocg_->CreateAssignment(0,
				ocg_->CreateArrayRefExpression(row_counter->name(), row->clone()),
				ocg_->CreateInt(0));

		return ocg_->StmtListAppend(body, body2);

		//return ocg_->StmtListAppend(body, epilogue);

	}

	CG_outputRepr *initialization_code2() {

		CG_outputRepr *epilogue = ocg_->CreateAssignment(0,
				ocg_->CreateArrayRefExpression(rowptr->clone(),
						vertex_count->clone()), ocg_->CreateInt(0));

		return epilogue;

	}

	CG_outputRepr *symmetrify(CG_outputRepr *row) {

		CG_outputRepr *body;

		/*
		 #pragma omp parallel for
		 for (int i = 0; i < m; i++) {
		 if (!binary_search(gr->colidx + gr->rowptr[i], gr->colidx + gr->rowptr[i+1], i)) {
		 // for each (i, c), add (c, i)
		 __sync_fetch_and_add(count2 + i, 1);
		 }


		 for (int j = gr->rowptr[i]; j < gr->rowptr[i+1]; ++j) {
		 int c = gr->colidx[j];
		 // assume colidx is sorted
		 if (!binary_search(gr->colidx + gr->rowptr[c], gr->colidx + gr->rowptr[c+1], i)) {
		 // for each (i, c), add (c, i)
		 __sync_fetch_and_add(count2 + c, 1);
		 }
		 }
		 }
		 */

		SgName name_find("binary_search");
		SgFunctionDeclaration *find = buildNondefiningFunctionDeclaration(
				name_find, buildVoidType(), buildFunctionParameterList(),
				static_cast<IR_roseCode *>(ir_)->get_root());
		SgExprListExp *args = buildExprListExp();

		args->append_expression(
				static_cast<CG_roseRepr *>(ocg_->CreatePlus(colidx->clone(),
						ocg_->CreateArrayRefExpression(rowptr->clone(), row->clone())))->GetExpression());

		args->append_expression(
				static_cast<CG_roseRepr *>(ocg_->CreatePlus(colidx->clone(),
						ocg_->CreateArrayRefExpression(rowptr->clone(),
								ocg_->CreatePlus(row->clone(), ocg_->CreateInt(1)))))->GetExpression());

		args->append_expression(
				static_cast<CG_roseRepr *>(row->clone())->GetExpression());

		SgFunctionCallExp *the_call = buildFunctionCallExp(
				buildFunctionRefExp(find), args);
		CG_outputRepr *exp = new CG_roseRepr(
				isSgExpression(buildNotOp(the_call)));

		SgName name_find2("__sync_fetch_and_add");
		SgFunctionDeclaration *find2 = buildNondefiningFunctionDeclaration(
				name_find2, buildVoidType(), buildFunctionParameterList(),
				static_cast<IR_roseCode *>(ir_)->get_root());
		SgExprListExp *args2 = buildExprListExp();

		args2->append_expression(
				static_cast<CG_roseRepr *>(ocg_->CreatePlus(
						ocg_->CreateIdent(row_counter2->name()), row->clone()))->GetExpression());

		args2->append_expression(
				static_cast<CG_roseRepr *>(ocg_->CreateInt(1))->GetExpression());

		SgFunctionCallExp *the_call2 = buildFunctionCallExp(
				buildFunctionRefExp(find2), args2);

		SgExprStatement* stmt = buildExprStatement(the_call2);
		CG_outputRepr *body2 = new CG_roseRepr(isSgNode(stmt));
		args = buildExprListExp();

		args->append_expression(
				static_cast<CG_roseRepr *>(ocg_->CreatePlus(colidx->clone(),
						ocg_->CreateArrayRefExpression(rowptr->clone(),
								ocg_->CreateIdent("c"))))->GetExpression());

		args->append_expression(
				static_cast<CG_roseRepr *>(ocg_->CreatePlus(colidx->clone(),
						ocg_->CreateArrayRefExpression(rowptr->clone(),
								ocg_->CreatePlus(ocg_->CreateIdent("c"),
										ocg_->CreateInt(1)))))->GetExpression());

		args->append_expression(
				static_cast<CG_roseRepr *>(row->clone())->GetExpression());

		CG_outputRepr* one = ocg_->CreateIf(0, exp, body2, NULL);

		CG_outputRepr *two = ocg_->CreateAssignment(0, ocg_->CreateIdent("c"),
				ocg_->CreateArrayRefExpression(colidx, ocg_->CreateIdent("j")));
		the_call = buildFunctionCallExp(buildFunctionRefExp(find), args);
		exp = new CG_roseRepr(isSgExpression(buildNotOp(the_call)));
		args2 = buildExprListExp();

		args2->append_expression(
				static_cast<CG_roseRepr *>(ocg_->CreatePlus(
						ocg_->CreateIdent(row_counter2->name()),
						ocg_->CreateIdent("c")))->GetExpression());

		args2->append_expression(
				static_cast<CG_roseRepr *>(ocg_->CreateInt(1))->GetExpression());

		the_call2 = buildFunctionCallExp(buildFunctionRefExp(find2), args2);

		stmt = buildExprStatement(the_call2);
		body2 = new CG_roseRepr(isSgNode(stmt));
		CG_outputRepr* three = ocg_->CreateIf(0, exp, body2, NULL);
		three = ocg_->StmtListAppend(two, three);

		CG_outputRepr *inductive = ocg_->CreateInductive(ocg_->CreateIdent("j"),
				ocg_->CreateArrayRefExpression(rowptr->clone(), row->clone()),
				ocg_->CreateMinus(
						ocg_->CreateArrayRefExpression(rowptr->clone(),
								ocg_->CreatePlus(row->clone(), ocg_->CreateInt(1))),
						ocg_->CreateInt(1)),
				NULL);

		three = ocg_->CreateLoop(0, inductive, three);

		return ocg_->StmtListAppend(one, three);

	}

	CG_outputRepr *resize_and_initialize_columns(CG_outputRepr *row) {

		CG_outputRepr *body = ir_->CreateMalloc(IR_CONSTANT_INT, colidx->clone(),
				ocg_->CreateArrayRefExpression(rowptr->clone(),
						vertex_count->clone()));

		SgName name_find("memset");
		SgFunctionDeclaration *find = buildNondefiningFunctionDeclaration(
				name_find, buildVoidType(), buildFunctionParameterList(),
				static_cast<IR_roseCode *>(ir_)->get_root());
		SgExprListExp *args = buildExprListExp();

		args->append_expression(
				static_cast<CG_roseRepr *>(colidx->clone())->GetExpression());
		args->append_expression(
				static_cast<CG_roseRepr *>(ocg_->CreateMinus(NULL,
						ocg_->CreateInt(1)))->GetExpression());

		args->append_expression(
				static_cast<CG_roseRepr *>(ocg_->CreateArrayRefExpression(
						rowptr->clone(), vertex_count->clone()))->GetExpression());

		SgFunctionCallExp *the_call = buildFunctionCallExp(
				buildFunctionRefExp(find), args);
		SgExprStatement* stmt = buildExprStatement(the_call);
		CG_outputRepr *body2 = new CG_roseRepr(isSgNode(stmt));

		return ocg_->StmtListAppend(body, body2);

	}

	CG_outputRepr *increment_and_add_to_row(CG_outputRepr *row,
			CG_outputRepr *val) {
		CG_outputRepr *conditional = ocg_->CreateEQ(ocg_->CreateIdent("found"),
				ocg_->CreateInt(1));
		SgName name_find("find");
		SgFunctionDeclaration *find = buildNondefiningFunctionDeclaration(
				name_find, buildPointerType(buildIntType()),
				buildFunctionParameterList(),
				static_cast<IR_roseCode *>(ir_)->get_root());

		SgExprListExp *args = buildExprListExp();
		args->append_expression(
				static_cast<CG_roseRepr *>(ocg_->CreatePlus(colidx->clone(),
						ocg_->CreateArrayRefExpression(rowptr->clone(), row->clone())))->GetExpression());
		args->append_expression(
				static_cast<CG_roseRepr *>(ocg_->CreatePlus(colidx->clone(),
						ocg_->CreatePlus(
								ocg_->CreateArrayRefExpression(rowptr->clone(),
										row->clone()),
								ocg_->CreateArrayRefExpression(row_counter->name(),
										row->clone()))))->GetExpression());
		args->append_expression(static_cast<CG_roseRepr *>(val)->GetExpression());

		SgFunctionCallExp *the_call = buildFunctionCallExp(
				buildFunctionRefExp(find), args);

		CG_outputRepr * lhs = new CG_roseRepr(isSgExpression(the_call));
		CG_outputRepr * rhs = ocg_->CreatePlus(
				ocg_->CreatePlus(colidx->clone(),
						ocg_->CreateArrayRefExpression(rowptr->clone(),
								row->clone())),
				ocg_->CreateArrayRefExpression(row_counter->name(), row->clone()));
		CG_outputRepr *conditional2 = ocg_->CreateEQ(lhs, rhs);
		CG_outputRepr *body = ocg_->CreateArrayRefExpression(colidx->clone(),
				ocg_->CreatePlus(
						ocg_->CreateArrayRefExpression(rowptr->clone(), row->clone()),
						ocg_->CreateArrayRefExpression(row_counter->name(),
								row->clone())));
		body = ocg_->CreateAssignment(0, body, val->clone());
		body = ocg_->StmtListAppend(body,
				ocg_->CreateAssignment(0,
						ocg_->CreateArrayRefExpression(row_counter->name(),
								row->clone()),
						ocg_->CreatePlus(
								ocg_->CreateArrayRefExpression(row_counter->name(),
										row->clone()), ocg_->CreateInt(1))));

		CG_outputRepr *second = ocg_->CreateIf(0, conditional2, body, NULL);
		//CG_outputRepr* third = ocg_->StmtListAppend(second,
		//		ocg_->CreateBreakStatement());
		CG_outputRepr *first = ocg_->CreateIf(0, conditional, second, NULL);

		return first;
		//return first;
	}

	CG_outputRepr *dep_graph_initialize() {
		CG_outputRepr* to_return = ocg_->CreateAssignment(0, ocg_->CreateIdent(name_),
                                new CG_roseRepr(
                                                isSgExpression(
                                                                buildNewExp(
                                                                                isSgClassType(
                                                                                                dynamic_cast<CG_roseRepr *>(repr2)->GetCode()),
                                                                                0, 0, 0, 0, 0))));


		to_return = ocg_->StmtListAppend(to_return, ir_->CreateMalloc(IR_CONSTANT_INT,rowptr->clone(), ocg_->CreatePlus(vertex_count->clone(), ocg_->CreateInt(1))));
                to_return = ocg_->StmtListAppend(to_return, ir_->CreateMalloc(IR_CONSTANT_INT, diagptr->clone(), vertex_count->clone()));
                to_return = ocg_->StmtListAppend(to_return, ocg_->CreateAssignment(0, extptr->clone(), ocg_->CreateInt(0)));
                to_return = ocg_->StmtListAppend(to_return, ocg_->CreateAssignment(0,_m->clone(),vertex_count->clone() ));
                return to_return;

	}
	CG_outputRepr *dep_graph_connect_local(CG_outputRepr *src,
			CG_outputRepr *dest) {
		SgExprListExp* args = buildExprListExp();
		//appendExpression(arg_list, op1);
		//appendExpression(arg_list, op2);;
		appendExpression(args, dynamic_cast<CG_roseRepr *>(src)->GetExpression());
		appendExpression(args,
				dynamic_cast<CG_roseRepr *>(dest)->GetExpression());
		return new CG_roseRepr(
				isSgNode(
						buildExprStatement(
								buildFunctionCallExp(
										dynamic_cast<CG_roseRepr *>(dynamic_cast<CG_roseBuilder *>(ocg_)->CreateArrowRefExpression(
												ocg_->CreateIdent(ptr->name()),
												dynamic_cast<CG_roseBuilder *>(ocg_)->lookup_member_function(
														repr2->clone(), "connect_local")))->GetExpression(),
										args))));
	}
	CG_outputRepr *dep_graph_connect_remote(CG_outputRepr *src,
			CG_outputRepr *dest) {
		SgExprListExp* args = buildExprListExp();
		//appendExpression(arg_list, op1);
		//appendExpression(arg_list, op2);;
		appendExpression(args, dynamic_cast<CG_roseRepr *>(src)->GetExpression());
		appendExpression(args,
				dynamic_cast<CG_roseRepr *>(dest)->GetExpression());
		return new CG_roseRepr(
				isSgNode(
						buildExprStatement(
								buildFunctionCallExp(
										dynamic_cast<CG_roseRepr *>(dynamic_cast<CG_roseBuilder *>(ocg_)->CreateArrowRefExpression(
												ocg_->CreateIdent(ptr->name()),
												dynamic_cast<CG_roseBuilder *>(ocg_)->lookup_member_function(
														repr2->clone(), "connect_remote")))->GetExpression(),
										args))));
	}
	CG_outputRepr *eliminateDuplicateEdges() {
		return NULL;
	}
	CG_outputRepr *sortEdges() {
		return NULL;
	}
	friend class Loop;
};

struct wavefront_intel_schedule: public wavefront_schedule {

	IR_Code * ir_;
	CG_outputBuilder *ocg_;
	SgClassSymbol *repr;
	CG_outputRepr *repr2;
	std::string name_;
	std::string ptr;
	SgNamespaceSymbol *ns;
	SgVariableSymbol *barrier_index;
	SgScopeStatement *scope_;
	bool is_negative;
         
	//~wavefront_intel_dep_graph(){};
	std::string name() {
		return name_;
	}
	std::vector<SgSymbolTable *> tabs;
	wavefront_intel_schedule(IR_Code * ir, CG_outputBuilder *ocg,
			std::string identifier, bool _negative){ 
		ir_ = ir;
		ocg_ = ocg;
		name_ = identifier;
		is_negative = _negative;
		tabs = dynamic_cast<IR_roseCode *>(ir)->getsymtabs();
		ns = tabs[0]->find_namespace(SgName("SpMP"));
		repr = ns->get_declaration()->get_definition()->lookup_class_symbol(
				SgName("LevelSchedule"));

		SgNamespaceDefinitionStatement *nsp =
				ns->get_declaration()->get_definition();
		nsp = nsp->get_nextNamepaceDefinition();
		while (!repr && nsp) {

			SgDeclarationStatementPtrList lst = nsp->get_declarations();
			//SgDeclarationStatementPtrList::iterator  t = lst.begin();

			for (SgDeclarationStatementPtrList::iterator t = lst.begin();
					t != lst.end(); t++) {
				if (isSgClassDeclaration(*t)) {
					repr =
							isSgClassSymbol(
									isSgClassDeclaration(*t)->search_for_symbol_from_symbol_table());

					if (repr->get_name().getString() != "LevelSchedule") {
						repr = NULL;

					} else {
						break;
					}

				}
			}
			nsp = nsp->get_nextNamepaceDefinition();
		}
		nsp = ns->get_declaration()->get_definition();
		nsp = nsp->get_previousNamepaceDefinition();
		while (!repr && nsp) {

			SgDeclarationStatementPtrList lst = nsp->get_declarations();
			//SgDeclarationStatementPtrList::iterator  t = lst.begin();

			for (SgDeclarationStatementPtrList::iterator t = lst.begin();
					t != lst.end(); t++) {
				if (isSgClassDeclaration(*t)) {
					repr =
							isSgClassSymbol(
									isSgClassDeclaration(*t)->search_for_symbol_from_symbol_table());

					if (repr->get_name().getString() != "LevelSchedule") {
						repr = NULL;

					} else {
						break;
					}

				}
			}
			nsp = nsp->get_previousNamepaceDefinition();
		}

		assert(repr);

		SgClassDefinition *repr22 =
				isSgClassDeclaration(
						repr->get_declaration()->get_definingDeclaration())->get_definition();
		assert(repr22);
		repr2 = new CG_roseRepr(isSgNode(repr22->get_declaration()->get_type()));
	 
                SgClassType *class_def_ = isSgClassType(
                        static_cast<CG_roseRepr *>(repr2)->GetCode());

   

               SgVariableDeclaration* defn = buildVariableDeclaration(SgName(name_.c_str()),
                        class_def_);
               defn->get_declarationModifier().get_storageModifier().setStatic();


               SgInitializedNamePtrList& variables = defn->get_variables();
               SgInitializedNamePtrList::const_iterator i = variables.begin();
               SgInitializedName* initializedName = *i;
               SgVariableSymbol *vs = new SgVariableSymbol(initializedName);
               prependStatement(defn, isSgScopeStatement(static_cast<IR_roseCode *>(ir)->get_func()->get_definition()->get_body()));
        //defn->set_variableDeclarationContainsBaseTypeDefiningDeclaration(true);
        //defn->set_baseTypeDefiningDeclaration(classDeclaration->get_definingDeclaration());
               vs->set_parent(static_cast<IR_roseCode*>(ir)->getsymtabs()[1]);
               static_cast<IR_roseCode *>(ir)->getsymtabs()[1]->insert(SgName(name_.c_str()), vs);
        
        //SgVariableDeclaration * varDecl2 = buildVariableDeclaration("temp", class_def_->get_declaration()->get_type(), NULL, globalScope);
               ptr = name_;
     	
               //ptr = ir->CreatePointerSymbol(repr2->clone(), name_, true);

		//initialize repr;
	}
	~wavefront_intel_schedule() {
	}
	;

	CG_outputRepr *schedule_initialize() {
		return ocg_->CreateAssignment(0, ocg_->CreateIdent(name_),
				new CG_roseRepr(
						isSgExpression(
								buildNewExp(
										isSgClassType(
												dynamic_cast<CG_roseRepr *>(repr2)->GetCode()),
										0, 0, 0, 0, 0))));

	}

	CG_outputRepr *barrier_sync(void) {
		return NULL;
	}
	CG_outputRepr *barrier_index_repr(void) {
		return new CG_roseRepr(isSgNode(barrier_index));
	}
	CG_outputRepr * p2p_post_sync() {
		//SgScopeStatement *scope_ = isSgScopeStatement(static_cast<CG_roseRepr *>(scope)->GetCode());
		//CG_outputRepr *to_return = ocg_->CreateAssignment(0, ocg_->CreateIdent("task"), level->clone());
		CG_outputRepr *pragma =
				new CG_roseRepr(
						isSgNode(
								buildExprStatement(
										buildOpaqueVarRefExp("SPMP_LEVEL_SCHEDULE_NOTIFY",
												static_cast<IR_roseCode *>(ir_)->get_func()->get_definition()->get_body()))));
		return pragma;
	}

	bool is_negative_IS() {

		return is_negative;
	}
	CG_outputRepr *p2p_wait_sync(CG_outputRepr *level, CG_outputRepr *scope) {
		SgScopeStatement *scope__ = isSgScopeStatement(
				static_cast<CG_roseRepr *>(scope)->GetCode());
		scope_ = scope__;
		CG_outputRepr *to_return = NULL;
		if (level != NULL) {
			if (is_negative)
				to_return = ocg_->CreateAssignment(0, ocg_->CreateIdent("task"),
						ocg_->CreateMinus(NULL, level->clone()));
			else
				to_return = ocg_->CreateAssignment(0, ocg_->CreateIdent("task"),
						level->clone());
		}
		CG_outputRepr *pragma =
				new CG_roseRepr(
						isSgNode(
								buildExprStatement(
										buildOpaqueVarRefExp("SPMP_LEVEL_SCHEDULE_WAIT",
												static_cast<IR_roseCode *>(ir_)->get_func()->get_definition()->get_body()))));
		return ocg_->StmtListAppend(to_return, pragma);
	}
	CG_outputRepr *p2p_initialization(CG_outputRepr *scope) {

		CG_outputRepr *to_return = NULL;
		scope_ = isSgScopeStatement(static_cast<CG_roseRepr *>(scope)->GetCode());

		/*
		 int nthreads = omp_get_num_threads();
		 int tid = omp_get_thread_num();

		 const int ntasks = schedule.ntasks;
		 const short *nparents = schedule.nparentsForward;


		 int nPerThread = (ntasks + nthreads - 1) / nthreads;
		 int nBegin = min(nPerThread * tid, ntasks);
		 int nEnd = min(nBegin + nPerThread, ntasks);

		 volatile int *taskFinished = schedule.taskFinished;
		 int **parents = schedule.parentsForward;

		 memset((char *) (taskFinished + nBegin), 0,
		 (nEnd - nBegin) * sizeof(int));

		 synk::Barrier::getInstance()->wait(tid);
		 */
		SgSymbolTable *body_symtab = tabs[2];

		SgFunctionDeclaration * fn = ((IR_roseCode *) ir_)->get_func();
		SgScopeStatement* func_body = fn->get_definition()->get_body();

		//try "tid" adding to the body
		/*SgVariableDeclaration *defn = buildVariableDeclaration("tid",
		 buildIntType());
		 SgInitializedNamePtrList& variables = defn->get_variables();
		 SgInitializedNamePtrList::const_iterator j = variables.begin();
		 SgInitializedName* initializedName = *j;
		 SgVariableSymbol* dvs = new SgVariableSymbol(initializedName);
		 prependStatement(defn, func_body);
		 dvs->set_parent(body_symtab);
		 body_symtab->insert("tid", dvs);
		 */
		//adding "num_threads" to the body
		SgGlobal *globals = ((IR_roseCode *) ir_)->get_root();
		SgName name_omp_get_thread_num("omp_get_thread_num");
		SgFunctionDeclaration * decl_omp_thread_id =
				buildNondefiningFunctionDeclaration(name_omp_get_thread_num,
						buildIntType(), buildFunctionParameterList(), globals);

		SgExprListExp* args = buildExprListExp();
		SgFunctionCallExp *the_call = buildFunctionCallExp(
				buildFunctionRefExp(decl_omp_thread_id), args);

		SgAssignInitializer *assign_init = buildAssignInitializer(the_call,
				buildIntType());
		SgVariableDeclaration *num_defn2 = buildVariableDeclaration("nthreads",
				buildIntType(), NULL, scope_);
		SgInitializedNamePtrList& vbls2 = num_defn2->get_variables();
		SgInitializedNamePtrList::const_iterator k2 = vbls2.begin();
		SgInitializedName* _Name2 = *k2;
		SgVariableSymbol* new_dvs2 = new SgVariableSymbol(_Name2);
		//prependStatement(num_defn2, func_body);
		new_dvs2->set_parent(body_symtab);
		body_symtab->insert("nthreads", new_dvs2);
		to_return = ocg_->StmtListAppend(to_return,
				new CG_roseRepr(isSgNode(num_defn2)));

		SgExprStatement* __stmt = buildExprStatement(the_call);
		//SgVarRefExp *var = new SgVarRefExp(dvs);
		CG_outputRepr * ins = ocg_->CreateAssignment(0, ocg_->CreateIdent("tid"),
				new CG_roseRepr(the_call));
		to_return = ocg_->StmtListAppend(to_return, ins);
		//building the statement "nthreads = omp_get_num_threads();"
		SgName name_omp_get_num_threads("omp_get_num_threads");
		SgFunctionDeclaration * decl_omp_num_threads =
				buildNondefiningFunctionDeclaration(name_omp_get_num_threads,
						buildIntType(), buildFunctionParameterList(), globals);

		SgExprListExp* new_args = buildExprListExp();
		SgFunctionCallExp *new_call = buildFunctionCallExp(
				buildFunctionRefExp(decl_omp_num_threads), new_args);

		SgVarRefExp *new_var2 = new SgVarRefExp(new_dvs2);
		SgExprStatement* ins_call2 = buildAssignStatement(new_var2, new_call);
		to_return = ocg_->StmtListAppend(to_return,
				new CG_roseRepr(isSgNode(ins_call2)));

		SgExpression *exp =
				dynamic_cast<CG_roseRepr *>(dynamic_cast<CG_roseBuilder *>(ocg_)->CreateDotExpression(
						ocg_->CreateIdent(ptr),
						dynamic_cast<CG_roseBuilder *>(ocg_)->lookup_member_data(
								repr2->clone(), "ntasks",
								ocg_->CreateIdent(ptr))))->GetExpression();

		assign_init = buildAssignInitializer(exp, buildConstType(buildIntType()));

		SgVariableDeclaration *num_defn3 = buildVariableDeclaration("ntasks",
				buildConstType(buildIntType()), assign_init, scope_);
		SgInitializedNamePtrList& vbls3 = num_defn3->get_variables();
		SgInitializedNamePtrList::const_iterator k3 = vbls3.begin();
		SgInitializedName* _Name3 = *k3;
		SgVariableSymbol* new_dvs3 = new SgVariableSymbol(_Name3);
		//prependStatement(num_defn3, func_body);
		new_dvs3->set_parent(body_symtab);
		body_symtab->insert("ntasks", new_dvs3);

		SgVarRefExp *new_var3 = new SgVarRefExp(new_dvs3);

		to_return = ocg_->StmtListAppend(to_return,
				new CG_roseRepr(isSgNode(num_defn3)));

		SgVariableDeclaration *num_defn3a = buildVariableDeclaration("task",
				buildIntType(), NULL, scope_);
		SgInitializedNamePtrList& vbls3a = num_defn3a->get_variables();
		SgInitializedNamePtrList::const_iterator k3a = vbls3a.begin();
		SgInitializedName* _Name3a = *k3a;
		SgVariableSymbol* new_dvs3a = new SgVariableSymbol(_Name3a);
		barrier_index = new_dvs3a;
		//prependStatement(num_defn3a, func_body);
		new_dvs3a->set_parent(body_symtab);
		body_symtab->insert("task", new_dvs3a);

		to_return = ocg_->StmtListAppend(to_return,
				new CG_roseRepr(isSgNode(num_defn3a)));

		/*	SgVariableDeclaration *num_defn4 = buildVariableDeclaration("nparents",
		 buildPointerType(buildConstType(buildShortType())), scope_);
		 SgInitializedNamePtrList& vbls4 = num_defn4->get_variables();
		 SgInitializedNamePtrList::const_iterator k4 = vbls4.begin();
		 SgInitializedName* _Name4 = *k4;
		 SgVariableSymbol* new_dvs4 = new SgVariableSymbol(_Name4);
		 prependStatement(num_defn4, func_body);
		 new_dvs4->set_parent(body_symtab);
		 body_symtab->insert("nparents", new_dvs4);

		 SgVarRefExp *new_var4 = new SgVarRefExp(new_dvs4);
		 */
		SgExpression * exp2;
		if (!is_negative)
			exp2 =
					dynamic_cast<CG_roseRepr *>(dynamic_cast<CG_roseBuilder *>(ocg_)->CreateDotExpression(
							ocg_->CreateIdent(ptr),
							dynamic_cast<CG_roseBuilder *>(ocg_)->lookup_member_data(
									repr2->clone(), "nparentsForward",
									ocg_->CreateIdent(ptr))))->GetExpression();
		else
			exp2 =
					dynamic_cast<CG_roseRepr *>(dynamic_cast<CG_roseBuilder *>(ocg_)->CreateDotExpression(
							ocg_->CreateIdent(ptr),
							dynamic_cast<CG_roseBuilder *>(ocg_)->lookup_member_data(
									repr2->clone(), "nparentsBackward",
									ocg_->CreateIdent(ptr))))->GetExpression();

		assign_init = buildAssignInitializer(exp2,
				buildPointerType(buildConstType(buildShortType())));
		SgVariableDeclaration *num_defn4 = buildVariableDeclaration("nparents",
				buildPointerType(buildConstType(buildShortType())), assign_init,
				scope_);
		SgInitializedNamePtrList& vbls4 = num_defn4->get_variables();
		SgInitializedNamePtrList::const_iterator k4 = vbls4.begin();
		SgInitializedName* _Name4 = *k4;
		SgVariableSymbol* new_dvs4 = new SgVariableSymbol(_Name4);
		//prependStatement(num_defn4, func_body);
		new_dvs4->set_parent(body_symtab);
		body_symtab->insert("nparents", new_dvs4);

		SgVarRefExp *new_var4 = new SgVarRefExp(new_dvs4);

		//SgExprStatement* ins_call4 = buildAssignStatement(new_var4, exp2);
		to_return = ocg_->StmtListAppend(to_return,
				new CG_roseRepr(isSgNode(num_defn4)));

		CG_outputRepr *ins_call6 = //ocg_->//CreateAssignment(0,
				//ocg_->CreateIdent("nPerThread"),
				ocg_->CreateDivide(
						ocg_->CreateMinus(
								ocg_->CreatePlus(ocg_->CreateIdent("ntasks"),
										ocg_->CreateIdent("nthreads")),
								ocg_->CreateInt(1)), ocg_->CreateIdent("nthreads"));

		assign_init = buildAssignInitializer(
				static_cast<CG_roseRepr*>(ins_call6)->GetExpression(),
				buildIntType());
		SgVariableDeclaration *num_defn6 = buildVariableDeclaration("nPerThread",
				buildIntType(), assign_init, scope_);

		SgInitializedNamePtrList& vbls6 = num_defn6->get_variables();
		SgInitializedNamePtrList::const_iterator k6 = vbls6.begin();
		SgInitializedName* _Name6 = *k6;
		SgVariableSymbol* new_dvs6 = new SgVariableSymbol(_Name6);
		//prependStatement(num_defn6, func_body);
		new_dvs6->set_parent(body_symtab);
		body_symtab->insert("nPerThread", new_dvs6);
		/*CG_outputRepr *ins_call6 = ocg_->//CreateAssignment(0,
		 //ocg_->CreateIdent("nPerThread"),
		 ocg_->CreateDivide(
		 ocg_->CreateMinus(
		 ocg_->CreatePlus(ocg_->CreateIdent("ntasks"),
		 ocg_->CreateIdent("nthreads")),
		 ocg_->CreateInt(1)),
		 ocg_->CreateIdent("nthreads"));
		 */
		to_return = ocg_->StmtListAppend(to_return,
				new CG_roseRepr(isSgNode(num_defn6)));
		std::vector<CG_outputRepr*> args_;
		args_.push_back(
				ocg_->CreateTimes(ocg_->CreateIdent("nPerThread"),
						ocg_->CreateIdent("tid")));
		args_.push_back(ocg_->CreateIdent("ntasks"));
		CG_outputRepr * ins_call5 = ocg_->CreateInvoke("min", args_);//ocg_->CreateAssignment(0,
		//ocg_->CreateIdent("nBegin"),
		//);
		assign_init = buildAssignInitializer(
				static_cast<CG_roseRepr *>(ins_call5)->GetExpression(),
				buildIntType());

		SgVariableDeclaration *num_defn5 = buildVariableDeclaration("nBegin",
				buildIntType(), assign_init, scope_);
		SgInitializedNamePtrList& vbls5 = num_defn5->get_variables();
		SgInitializedNamePtrList::const_iterator k5 = vbls5.begin();
		SgInitializedName* _Name5 = *k5;
		SgVariableSymbol* new_dvs5 = new SgVariableSymbol(_Name5);
		//prependStatement(num_defn5, func_body);
		new_dvs5->set_parent(body_symtab);
		body_symtab->insert("nBegin", new_dvs5);

		to_return = ocg_->StmtListAppend(to_return,
				new CG_roseRepr(isSgNode(num_defn5)));

		std::vector<CG_outputRepr *> args__;
		args__.push_back(
				ocg_->CreatePlus(ocg_->CreateIdent("nBegin"),
						ocg_->CreateIdent("nPerThread")));
		args__.push_back(ocg_->CreateIdent("ntasks"));
		CG_outputRepr * ins_call7 = ocg_->CreateInvoke("min", args__);	//ocg_->CreateAssignment(0,
		//ocg_->CreateIdent("nEnd"),
		//ocg_->CreateInvoke("min", args__)//);
		assign_init = buildAssignInitializer(
				static_cast<CG_roseRepr *>(ins_call7)->GetExpression(),
				buildIntType());
		SgVariableDeclaration *num_defn7 = buildVariableDeclaration("nEnd",
				buildIntType(), assign_init, scope_);
		SgInitializedNamePtrList& vbls7 = num_defn7->get_variables();
		SgInitializedNamePtrList::const_iterator k7 = vbls7.begin();
		SgInitializedName* _Name7 = *k7;
		SgVariableSymbol* new_dvs7 = new SgVariableSymbol(_Name7);
		//prependStatement(num_defn7, func_body);
		new_dvs7->set_parent(body_symtab);
		body_symtab->insert("nEnd", new_dvs7);

		to_return = ocg_->StmtListAppend(to_return,
				new CG_roseRepr(isSgNode(num_defn7)));

		SgExpression *exp8 =
				dynamic_cast<CG_roseRepr *>(dynamic_cast<CG_roseBuilder *>(ocg_)->CreateDotExpression(
						ocg_->CreateIdent(ptr),
						dynamic_cast<CG_roseBuilder *>(ocg_)->lookup_member_data(
								repr2->clone(), "taskFinished",
								ocg_->CreateIdent(ptr))))->GetExpression();

		//SgExprStatement* ins_call8 = buildAssignStatement(new_var8, exp8);
		assign_init = buildAssignInitializer(exp8,
				buildPointerType(buildVolatileType(buildIntType())));
		SgVariableDeclaration *num_defn8 = buildVariableDeclaration(
				"taskFinished", buildPointerType(buildVolatileType(buildIntType())),
				assign_init, scope_);
		SgInitializedNamePtrList& vbls8 = num_defn8->get_variables();
		SgInitializedNamePtrList::const_iterator k8 = vbls8.begin();
		SgInitializedName* _Name8 = *k8;
		SgVariableSymbol* new_dvs8 = new SgVariableSymbol(_Name8);
		//prependStatement(num_defn8, func_body);
		new_dvs8->set_parent(body_symtab);
		body_symtab->insert("taskFinished", new_dvs8);
		SgVarRefExp *new_var8 = new SgVarRefExp(new_dvs8);

		to_return = ocg_->StmtListAppend(to_return,
				new CG_roseRepr(isSgNode(num_defn8)));
		SgExpression *exp9;
		if (!is_negative)
			exp9 =
					dynamic_cast<CG_roseRepr *>(dynamic_cast<CG_roseBuilder *>(ocg_)->CreateDotExpression(
							ocg_->CreateIdent(ptr),
							dynamic_cast<CG_roseBuilder *>(ocg_)->lookup_member_data(
									repr2->clone(), "parentsForward",
									ocg_->CreateIdent(ptr))))->GetExpression();
		else
			exp9 =
					dynamic_cast<CG_roseRepr *>(dynamic_cast<CG_roseBuilder *>(ocg_)->CreateDotExpression(
							ocg_->CreateIdent(ptr),
							dynamic_cast<CG_roseBuilder *>(ocg_)->lookup_member_data(
									repr2->clone(), "parentsBackward",
									ocg_->CreateIdent(ptr))))->GetExpression();

		//	SgExprStatement* ins_call9 = buildAssignStatement(new_var9, exp9);

		assign_init = buildAssignInitializer(exp9,
				buildPointerType(buildPointerType(buildIntType())));
		SgVariableDeclaration *num_defn9 = buildVariableDeclaration("parents",
				buildPointerType(buildPointerType(buildIntType())), assign_init,
				scope_);
		SgInitializedNamePtrList& vbls9 = num_defn9->get_variables();
		SgInitializedNamePtrList::const_iterator k9 = vbls9.begin();
		SgInitializedName* _Name9 = *k9;
		SgVariableSymbol* new_dvs9 = new SgVariableSymbol(_Name9);
		//prependStatement(num_defn9, func_body);
		new_dvs9->set_parent(body_symtab);
		body_symtab->insert("parents", new_dvs9);

		SgVarRefExp *new_var9 = new SgVarRefExp(new_dvs9);

		to_return = ocg_->StmtListAppend(to_return,
				new CG_roseRepr(isSgNode(num_defn9)));

		//memset((char *) (taskFinished + nBegin), 0,
		//		 (nEnd - nBegin) * sizeof(int));
		SgName name_memset("memset");
		SgFunctionDeclaration * decl_memset = buildNondefiningFunctionDeclaration(
				name_memset, buildVoidType(), buildFunctionParameterList(),
				globals);

		SgExprListExp * memset_args = buildExprListExp();

		CG_outputRepr *arg1 = ocg_->CreatePlus(ocg_->CreateIdent("taskFinished"),
				ocg_->CreateIdent("nBegin"));

		memset_args->append_expression(
				buildCastExp(dynamic_cast<CG_roseRepr*>(arg1)->GetExpression(),
						buildPointerType(buildCharType())));
		memset_args->append_expression(buildIntVal(0));

		CG_outputRepr *arg3 = ocg_->CreateMinus(ocg_->CreateIdent("nEnd"),
				ocg_->CreateIdent("nBegin"));

		memset_args->append_expression(
				buildMultiplyOp(static_cast<CG_roseRepr*>(arg3)->GetExpression(),
						buildSizeOfOp(buildIntType())));

		SgFunctionCallExp * memset_func_call = buildFunctionCallExp(
				buildFunctionRefExp(decl_memset), memset_args);

		SgExprStatement* stmt = buildExprStatement(memset_func_call);

		to_return = ocg_->StmtListAppend(to_return,
				new CG_roseRepr(isSgNode(stmt)));

		SgOmpBarrierStatement* omp_barrier = new SgOmpBarrierStatement(
		TRANSFORMATION_FILE_INFO);
		to_return = ocg_->StmtListAppend(to_return,
				new CG_roseRepr(isSgNode(omp_barrier)));

		return to_return;

	}
	std::string threadBoundaries() {
		return std::string("threadBoundaries");
	}
	std::string wavefront_boundaries() {
		return std::string("taskBoundaries");
	}
	CG_outputRepr *build_task_graph(wavefront_dep_graph *A) {
		SgExprListExp* args = buildExprListExp();
		//appendExpression(arg_list, op1);
		//appendExpression(arg_list, op2);;
		appendExpression(args, buildPointerDerefExp(buildVarRefExp(A->name())));
		return new CG_roseRepr(
				isSgNode(
						buildExprStatement(
								buildFunctionCallExp(
										dynamic_cast<CG_roseRepr *>(dynamic_cast<CG_roseBuilder *>(ocg_)->CreateDotExpression(
												ocg_->CreateIdent(ptr),
												dynamic_cast<CG_roseBuilder *>(ocg_)->lookup_member_function(
														repr2->clone(), "constructTaskGraph")))->GetExpression(),
										args))));
	}
	CG_outputRepr *perm(void) {

		SgExpression *exp =
				dynamic_cast<CG_roseRepr *>(dynamic_cast<CG_roseBuilder *>(ocg_)->CreateDotExpression(
						ocg_->CreateIdent(ptr),
						dynamic_cast<CG_roseBuilder *>(ocg_)->lookup_member_data(
								repr2->clone(), "threadContToOrigPerm",
								ocg_->CreateIdent(ptr))))->GetExpression();

		return new CG_roseRepr(exp);
	}

	CG_outputRepr *inv_perm(void) {

		SgExpression *exp =
				dynamic_cast<CG_roseRepr *>(dynamic_cast<CG_roseBuilder *>(ocg_)->CreateDotExpression(
						ocg_->CreateIdent(ptr),
						dynamic_cast<CG_roseBuilder *>(ocg_)->lookup_member_data(
								repr2->clone(), "origToThreadContPerm",
								ocg_->CreateIdent(ptr))))->GetExpression();
		return new CG_roseRepr(exp);
	}

	friend class Loop;
};
/*Relation replicate_complex_equalities_in_xform(Relation &xform, int arity) {

 int max_count = -1;
 bool check[arity];

 for (int i = 1; i <= arity; i++) {

 Variable_ID v = xform.input_var(i);
 int count = 0;
 check[i - 1] = false;
 for (DNF_Iterator di(const_cast<Relation &>(xform).queryDNF()); di;
 di++)
 for (EQ_Iterator gi((*di)->EQs()); gi; gi++)
 if ((*gi).get_coef(v) != 0)
 for (Constr_Vars_Iter cvi(*gi); cvi; cvi++) {
 Variable_ID v = cvi.curr_var();
 switch (v->kind()) {
 case Output_Var: {
 if (v->get_position() != 2 * i)
 count++;
 break;
 }
 default:
 break;
 }
 }
 if (count > 0 && count > max_count) {
 max_count = count;
 check[i - 1] = true;
 }
 }

 Relation new_xform


 }
 */

bool CheckForReduction(IR_Code *ir, CG_outputRepr *assignment, int zero) {

	IR_OPERATION_TYPE op = ir->getReductionOp(assignment);

	if (op == IR_OP_PLUS && zero == 0)
		return true;
	else if (op == IR_OP_MINUS && zero == 0)
		return true;
	else if (op == IR_OP_MULTIPLY && zero == 1)
		return true;

	return false;

}

bool checkForUniqueProductInReduction(std::vector<IR_ArrayRef *> &refs,
		std::vector<std::string> &data_array) {

	std::set<std::string> helper;

	for (int j = 0; j < data_array.size(); j++) {

		bool found = false;
		for (int i = 0; i < refs.size(); i++)
			if (data_array[j] == refs[i]->name())
				found = true;
		if (!found)
			return found;
	}
	return true;
	/* if(!found)
	 return found;

	 CG_outputRepr *rhs = ir->GetRHSExpression(assignment);

	 switch(ir->QueryExpOperation(rhs)){

	 case  IR_OP_ARRAY_VARIABLE:
	 {
	 std::vector<IR_ArrayRef*> refs2 = ir->FindArrayRef(rhs);
	 if(data_array.size() == 1)
	 if(refs2.size() == 1 && refs2->name() == data_array[0])
	 return true;




	 }
	 break;


	 case  IR_OP_MULTIPLY:
	 {
	 std::vector<IR_ArrayRef*> refs2 = ir->FindArrayRef(rhs);
	 if(data_array.size() > 1)
	 if(refs2.size() == data_array.size() && refs2->name() == data_array[0])
	 return true;




	 }
	 break;


	 default:
	 break;


	 }

	 */

}

std::pair<Relation, Relation> createCSRstyleISandXFORM(CG_outputBuilder *ocg,
		std::vector<Relation> &outer_loop_bounds, std::string index_name,
		std::map<int, Relation> &zero_loop_bounds,
		std::map<int, Relation> &neglected,
		std::map<std::string, std::vector<omega::CG_outputRepr *> > &uninterpreted_symbols,
		std::map<std::string, std::vector<omega::CG_outputRepr *> > &uninterpreted_symbols_string,
		std::map<std::string, std::vector<omega::Relation> > &unin_rel_,
		Loop *this_loop, std::vector<Free_Var_Decl *> &freevar,
		bool is_single_iteration = false) {

	Relation IS;
	Relation XFORM;
	if (!is_single_iteration) {
		IS = Relation(
				outer_loop_bounds.size() + 1 + zero_loop_bounds.size()
						+ neglected.size());
		XFORM = Relation(
				outer_loop_bounds.size() + 1 + zero_loop_bounds.size()
						+ neglected.size(),
				2
						* (outer_loop_bounds.size() + 1 + zero_loop_bounds.size()
								+ neglected.size()) + 1);
	} else {
		IS = Relation(
				outer_loop_bounds.size() + zero_loop_bounds.size()
						+ neglected.size());
		XFORM = Relation(
				outer_loop_bounds.size() + zero_loop_bounds.size()
						+ neglected.size(),
				2
						* (outer_loop_bounds.size() + zero_loop_bounds.size()
								+ neglected.size()) + 1);
	}
	F_And * f_r_ = IS.add_and();
	F_And * f_root = XFORM.add_and();

	if (outer_loop_bounds.size() > 0) {
		for (int it = 0; it < outer_loop_bounds.size(); it++) {
			IS.name_set_var(it + 1,
					const_cast<Relation &>(outer_loop_bounds[it]).set_var(it + 1)->name());
			XFORM.name_input_var(it + 1,
					const_cast<Relation &>(outer_loop_bounds[it]).set_var(it + 1)->name());

		}
	}

	if (!is_single_iteration) {
		if (outer_loop_bounds.size() > 0) {
			IS.name_set_var(outer_loop_bounds.size() + 1,
					const_cast<Relation &>(outer_loop_bounds[0]).set_var(
							outer_loop_bounds.size() + 1)->name());
			XFORM.name_input_var(outer_loop_bounds.size() + 1,
					const_cast<Relation &>(outer_loop_bounds[0]).set_var(
							outer_loop_bounds.size() + 1)->name());
		} else if (zero_loop_bounds.size() > 0) {
			IS.name_set_var(outer_loop_bounds.size() + 1,
					const_cast<Relation &>(zero_loop_bounds.begin()->second).set_var(
							outer_loop_bounds.size() + 1)->name());
			XFORM.name_input_var(outer_loop_bounds.size() + 1,
					const_cast<Relation &>(zero_loop_bounds.begin()->second).set_var(
							outer_loop_bounds.size() + 1)->name());
		}

	}

	if (zero_loop_bounds.size() > 0) {
		int count = 1;

		if (!is_single_iteration)
			count = 2;
		int track = 0;
		for (std::map<int, Relation>::iterator it = zero_loop_bounds.begin();
				it != zero_loop_bounds.end(); it++, track++) {
			IS.name_set_var(outer_loop_bounds.size() + count + track,
					const_cast<Relation &>(it->second).set_var(it->first)->name());
			XFORM.name_input_var(outer_loop_bounds.size() + count + track,
					const_cast<Relation &>(it->second).set_var(it->first)->name());

		}

	}

	if (neglected.size() > 0) {

		int count = 1;

		if (!is_single_iteration)
			count = 2;
		int track = 0;
		for (std::map<int, Relation>::iterator it = neglected.begin();
				it != neglected.end(); it++, track++) {
			IS.name_set_var(
					outer_loop_bounds.size() + zero_loop_bounds.size() + count
							+ track,
					const_cast<Relation &>(it->second).set_var(it->first)->name());
			XFORM.name_input_var(
					outer_loop_bounds.size() + zero_loop_bounds.size() + count
							+ track,
					const_cast<Relation &>(it->second).set_var(it->first)->name());

		}

	}

	std::map<int, int> pos_map;

	for (int i = 1; i <= outer_loop_bounds.size(); i++) {
		pos_map.insert(std::pair<int, int>(i, i));
	}

	for (int i = 0; i < outer_loop_bounds.size(); i++)
		IS = replace_set_var_as_another_set_var(IS, outer_loop_bounds[i], i + 1,
				i + 1, pos_map);

	int count = 1;
	if (!is_single_iteration) {
		std::map<int, int> pos_map;

		for (std::map<int, Relation>::iterator i = zero_loop_bounds.begin();
				i != zero_loop_bounds.end(); i++, count++)
			pos_map.insert(
					std::pair<int, int>(outer_loop_bounds.size() + 1 + count,
							i->first));

		count = 1;
		for (std::map<int, Relation>::iterator i = zero_loop_bounds.begin();
				i != zero_loop_bounds.end(); i++, count++)
			IS = replace_set_var_as_another_set_var(IS, i->second,
					outer_loop_bounds.size() + 1 + count, i->first, pos_map);
	} else {
		for (std::map<int, Relation>::iterator i = zero_loop_bounds.begin();
				i != zero_loop_bounds.end(); i++, count++)
			pos_map.insert(
					std::pair<int, int>(outer_loop_bounds.size() + 1, i->first));
		count = 1;
		for (std::map<int, Relation>::iterator i = zero_loop_bounds.begin();
				i != zero_loop_bounds.end(); i++, count++)
			IS = replace_set_var_as_another_set_var(IS, i->second,
					outer_loop_bounds.size() + 1, i->first, pos_map);
	}
	int count__ = 0; //outer_loop_bounds.size() + zero_loop_bound.size();
	count = 1;
	if (!is_single_iteration)
		count__++;
	{

		std::map<int, int> pos_map;

		for (std::map<int, Relation>::iterator i = neglected.begin();
				i != neglected.end(); i++) {
			pos_map.insert(
					std::pair<int, int>(
							count + outer_loop_bounds.size() + zero_loop_bounds.size()
									+ count__, i->first));
		}

		for (std::map<int, Relation>::iterator i = neglected.begin();
				i != neglected.end(); i++) {
			IS = replace_set_var_as_another_set_var(IS, i->second,
					count + outer_loop_bounds.size() + zero_loop_bounds.size()
							+ count__, i->first, pos_map);
		}

	}
	for (int j = 1; j <= XFORM.n_inp(); j++) {
		omega::EQ_Handle h = f_root->add_EQ();
		h.update_coef(XFORM.output_var(2 * j), 1);
		h.update_coef(XFORM.input_var(j), -1);
	}

	for (int j = 1; j <= XFORM.n_out(); j += 2) {
		omega::EQ_Handle h = f_root->add_EQ();
		h.update_coef(XFORM.output_var(j), 1);
	}

	int n = IS.n_set();
	omega::Relation mapping(2 * n + 1, n);
	omega::F_And *f_root_m = mapping.add_and();
	for (int j = 1; j <= n; j++) {
		omega::EQ_Handle h = f_root_m->add_EQ();
		h.update_coef(mapping.output_var(j), 1);
		h.update_coef(mapping.input_var(2 * j), -1);
	}
	mapping = omega::Composition(mapping, copy(XFORM));
	mapping.simplify();

	if (outer_loop_bounds.size() > 0) {
		if (!is_single_iteration) {
			bool found_lb = false;
			bool found_ub = false;
			Free_Var_Decl *lb;
			Free_Var_Decl *ub;
			for (int k = 0; k < freevar.size(); k++) {
				if (freevar[k]->base_name() == index_name + "_") {
					found_lb = true;
					lb = freevar[k];

				}
				if (freevar[k]->base_name() == index_name + "__") {
					found_ub = true;
					ub = freevar[k];
				}
			}

			if (!found_ub && !found_lb) {
				lb = new Free_Var_Decl(index_name + "_", outer_loop_bounds.size());
				ub = new Free_Var_Decl(index_name + "__", outer_loop_bounds.size());
				freevar.push_back(ub);

				freevar.push_back(lb);
			}

			Variable_ID csr_lb = IS.get_local(lb, Input_Tuple);

			Variable_ID csr_ub = IS.get_local(ub, Input_Tuple);

			F_And * f_r = IS.and_with_and();
			GEQ_Handle lower_bound = f_r->add_GEQ();
			lower_bound.update_coef(csr_lb, -1);
			lower_bound.update_coef(IS.set_var(outer_loop_bounds.size() + 1), 1);

			//upper bound

			GEQ_Handle upper_bound = f_r->add_GEQ();
			upper_bound.update_coef(csr_ub, 1);
			upper_bound.update_coef(IS.set_var(outer_loop_bounds.size() + 1), -1);
			upper_bound.update_const(-1);

			omega::CG_stringBuilder *ocgs = new CG_stringBuilder;

			std::vector<omega::CG_outputRepr *> reprs;
			std::vector<omega::CG_outputRepr *> reprs2;

			std::vector<omega::Relation> reprs5;
			std::vector<omega::Relation> reprs6;

			std::vector<omega::CG_outputRepr *> reprs3;
			std::vector<omega::CG_outputRepr *> reprs4;

			std::string args = "";
			args += IS.set_var(1)->name();
			for (int i = 0; i < outer_loop_bounds.size(); i++) {

				reprs.push_back(ocg->CreateIdent(IS.set_var(i + 1)->name()));
				reprs2.push_back(ocgs->CreateIdent(IS.set_var(i + 1)->name()));
				if (i >= 1)
					args += "," + IS.set_var(i + 1)->name();

				omega::Relation mapping1(mapping.n_out(), 1);
				omega::F_And *f_root = mapping1.add_and();
				omega::EQ_Handle h = f_root->add_EQ();
				h.update_coef(mapping1.output_var(1), 1);
				h.update_coef(mapping1.input_var(i + 1), -1);
				omega::Relation r = Composition(mapping1, copy(mapping));
				r.simplify();

				Variable_ID v = r.output_var(1);

				std::vector<std::pair<omega::CG_outputRepr *, int> > atof =
						std::vector<std::pair<omega::CG_outputRepr *, int> >(
								IS.n_set(),
								std::make_pair(
										static_cast<omega::CG_outputRepr *>(NULL), 0));
				std::pair<omega::EQ_Handle, int> result1 = find_simplest_assignment(
						r, v, atof);
				for (omega::Constr_Vars_Iter cvi(result1.first); cvi; cvi++)
					;

				std::pair<omega::EQ_Handle, int> result2 = find_simplest_assignment(
						r, v, atof);

				Relation c = copy(r);

				reprs5.push_back(c);
				Relation d = copy(r);

				reprs6.push_back(d);
			}
			uninterpreted_symbols.insert(
					std::pair<std::string, std::vector<CG_outputRepr *> >(
							index_name + "_", reprs));
			uninterpreted_symbols_string.insert(
					std::pair<std::string, std::vector<CG_outputRepr *> >(
							index_name + "_", reprs2));

			unin_rel_.insert(
					std::pair<std::string, std::vector<Relation> >(index_name + "_",
							reprs5));

			std::string args2 = "(" + args + ")";

			CG_outputRepr *subscript;
			for (int i = 0; i < outer_loop_bounds.size(); i++) {
				CG_outputRepr *name = ocg->CreateIdent(IS.set_var(i + 1)->name());
				int mult = 1;

				for (int j = i + 1; j < outer_loop_bounds.size(); j++)
					mult *= extract_loop_trip_count(IS, IS.set_var(j + 1));

				if (mult != 1)
					name = ocg->CreateTimes(ocg->CreateInt(mult), name->clone());

				if (i > 0)
					subscript = ocg->CreatePlus(subscript, name->clone());
				else
					subscript = name->clone();

			}
			CG_outputRepr *repr = ocg->CreateArrayRefExpression(index_name,
					subscript->clone());

			this_loop->ir->CreateDefineMacro(index_name + "_", args2, repr);

			Relation known_(copy(IS).n_set());
			known_.copy_names(IS);
			known_.setup_names();
			Variable_ID index_lb = known_.get_local(lb, Input_Tuple);
			Variable_ID index_ub = known_.get_local(ub, Input_Tuple);
			F_And *fr = known_.add_and();
			GEQ_Handle g = fr->add_GEQ();
			g.update_coef(index_ub, 1);
			g.update_coef(index_lb, -1);
			g.update_const(-1);
			this_loop->addKnown(known_);

			reprs3 = reprs;
			reprs4 = reprs2;

			CG_outputRepr *repr2 = ocg->CreateArrayRefExpression(index_name,
					ocg->CreatePlus(subscript->clone(), ocg->CreateInt(1)));

			this_loop->ir->CreateDefineMacro(index_name + "__", args2, repr2);

			uninterpreted_symbols.insert(
					std::pair<std::string, std::vector<CG_outputRepr *> >(
							index_name + "__", reprs3));
			uninterpreted_symbols_string.insert(
					std::pair<std::string, std::vector<CG_outputRepr *> >(
							index_name + "__", reprs4));
			unin_rel_.insert(
					std::pair<std::string, std::vector<Relation> >(index_name + "__",
							reprs6));
		}
	} else {
		if (!is_single_iteration) {

			bool found_ub = false;

			Free_Var_Decl *ub;
			for (int k = 0; k < freevar.size(); k++) {
				if (freevar[k]->base_name() == index_name) {
					found_ub = true;
					ub = freevar[k];

				}

			}

			if (!found_ub) {

				ub = new Free_Var_Decl(index_name);
				freevar.push_back(ub);

			}

			Variable_ID csr_ub = IS.get_local(ub);
			F_And * f_r = IS.and_with_and();
			GEQ_Handle upper_bound = f_r->add_GEQ();
			upper_bound.update_coef(csr_ub, 1);
			upper_bound.update_coef(IS.set_var(outer_loop_bounds.size() + 1), -1);
			upper_bound.update_const(-1);

			GEQ_Handle lower_bound = f_r->add_GEQ();
			lower_bound.update_coef(IS.set_var(outer_loop_bounds.size() + 1), 1);
		}
	}

	IS.simplify();
	XFORM.simplify();

	if (_DEBUG_) {
		IS.print();
		XFORM.print();

	}

	return std::pair<Relation, Relation>(IS, XFORM);

}
void clean_unin_relation(Relation &IS,
		std::map<std::string, std::vector<omega::Relation> > & unin_relation) {

	int n = IS.n_set();
	bool found = false;
	for (DNF_Iterator di(const_cast<Relation &>(IS).query_DNF()); di; di++) {
		for (Constraint_Iterator gi((*di)->constraints()); gi; gi++) {
			for (Constr_Vars_Iter cvi(*gi); cvi; cvi++) {
				Variable_ID v = cvi.curr_var();
				switch (v->kind()) {
				case Global_Var: {
					Global_Var_ID g = v->get_global_var();
					Variable_ID v2;
					if (g->arity() > 0)
						found = true;

				}
				default:
					break;
				}
			}
		}
	}

	if (found) {

		for (std::map<std::string, std::vector<omega::Relation> >::iterator i =
				unin_relation.begin(); i != unin_relation.end(); i++) {

			std::vector<omega::Relation> reprs2;

			for (int j = 0; j < i->second.size(); j++) {
				int m = i->second[j].n_inp();

				if (n != m) {
					Relation new_r(n, 1);

					F_And *f_root = new_r.add_and();

					omega::Variable_ID v = i->second[j].output_var(1);

					std::vector<std::pair<omega::CG_outputRepr *, int> > atof =
							std::vector<std::pair<omega::CG_outputRepr *, int> >(
									i->second[j].n_inp(),
									std::make_pair(
											static_cast<omega::CG_outputRepr *>(NULL), 0));
					std::pair<omega::EQ_Handle, int> result1 =
							find_simplest_assignment(i->second[j], v, atof);
					EQ_Handle h = f_root->add_EQ();
					h.update_coef(new_r.output_var(1), -1);
					bool illegal = false;

					for (Constr_Vars_Iter cvi((result1.first)); cvi; cvi++) {
						Variable_ID v = cvi.curr_var();
						switch (v->kind()) {
						case Input_Var: {

							if (v->get_position() > n) {
								illegal = true;

							} else

								h.update_coef(new_r.input_var(v->get_position()),
										cvi.curr_coef());
							/*else if (v->get_position() < old_pos)
							 h.update_coef(r.input_var(v->get_position()),
							 cvi.curr_coef());   //Anand: hack//
							 else
							 not_const = true;
							 */
							//else
							//	throw omega_error(
							//			"relation contains set vars other than that to be replicated!");
							break;

						}
						case Output_Var:
							break;

						default:
							assert(false);
						}
					}

					h.update_const(((result1.first)).get_const());
					//if(illegal)
					//reprs2.push_back(i->second[j]);
					if (!illegal) {
						new_r.simplify();
						reprs2.push_back(new_r);
					}
				} else
					reprs2.push_back(i->second[j]);
			}
			i->second = reprs2;
		}
	}
}
std::pair<Relation, Relation> construct_reduced_IS_And_XFORM(IR_Code *ir,
		const Relation &is, const Relation &xform, const std::vector<int> loops,
		std::vector<int> &lex_order, Relation &known,
		std::map<std::string, std::vector<CG_outputRepr *> > &uninterpreted_symbols) {

	Relation IS(loops.size());
	Relation XFORM(loops.size(), 2 * loops.size() + 1);
	int count_ = 1;
	std::map<int, int> pos_mapping;

	int n = is.n_set();

	Relation is_and_known;
	if (n > known.n_set())
		is_and_known = Intersection(copy(is),
				Extend_Set(copy(known), n - known.n_set()));
	else
		is_and_known = Intersection(copy(known),
				Extend_Set(copy(is), known.n_set() - n));

	for (int it = 0; it < loops.size(); it++, count_++) {
		IS.name_set_var(count_,
				const_cast<Relation &>(is).set_var(loops[it])->name());
		XFORM.name_input_var(count_,
				const_cast<Relation &>(xform).input_var(loops[it])->name());
		XFORM.name_output_var(2 * count_,
				const_cast<Relation &>(xform).output_var((loops[it]) * 2)->name());
		XFORM.name_output_var(2 * count_ - 1,
				const_cast<Relation &>(xform).output_var((loops[it]) * 2 - 1)->name());
		pos_mapping.insert(std::pair<int, int>(count_, loops[it]));
	}

	XFORM.name_output_var(2 * loops.size() + 1,
			const_cast<Relation &>(xform).output_var(is.n_set() * 2 + 1)->name());

	F_And * f_r = IS.add_and();
	for (std::map<int, int>::iterator it = pos_mapping.begin();
			it != pos_mapping.end(); it++)
		IS = replace_set_var_as_another_set_var(IS, is_and_known, it->first,
				it->second, pos_mapping);
	/*
	 for (std::map<std::string, std::vector<CG_outputRepr *> >::iterator it2 =
	 uninterpreted_symbols.begin();
	 it2 != uninterpreted_symbols.end(); it2++) {
	 std::vector<CG_outputRepr *> reprs_ = it2->second;
	 //std::vector<CG_outputRepr *> reprs_2;

	 for (int k = 0; k < reprs_.size(); k++) {
	 std::vector<IR_ScalarRef *> refs = ir->FindScalarRef(reprs_[k]);
	 bool exception_found = false;
	 for (int m = 0; m < refs.size(); m++){

	 if (refs[m]->name()
	 == const_cast<Relation &>(is).set_var(it->second)->name())
	 try {
	 ir->ReplaceExpression(refs[m],
	 ir->builder()->CreateIdent(
	 IS.set_var(it->first)->name()));
	 } catch (ir_error &e) {

	 reprs_[k] = ir->builder()->CreateIdent(
	 IS.set_var(it->first)->name());
	 exception_found = true;
	 }
	 if(exception_found)
	 break;
	 }

	 }
	 it2->second = reprs_;
	 }

	 }
	 */
	if (_DEBUG_) {
		std::cout << "relation debug" << std::endl;
		IS.print();
	}

	F_And *f_root = XFORM.add_and();

	count_ = 1;

	for (int j = 1; j <= loops.size(); j++) {
		omega::EQ_Handle h = f_root->add_EQ();
		h.update_coef(XFORM.output_var(2 * j), 1);
		h.update_coef(XFORM.input_var(j), -1);
	}
	for (int j = 0; j < loops.size(); j++, count_++) {
		omega::EQ_Handle h = f_root->add_EQ();
		h.update_coef(XFORM.output_var(count_ * 2 - 1), 1);
		h.update_const(-lex_order[count_ * 2 - 2]);
	}

	omega::EQ_Handle h = f_root->add_EQ();
	h.update_coef(XFORM.output_var((loops.size()) * 2 + 1), 1);
	h.update_const(-lex_order[xform.n_out() - 1]);

	if (_DEBUG_) {
		std::cout << "relation debug" << std::endl;
		IS.print();
		XFORM.print();
	}

	return std::pair<Relation, Relation>(IS, XFORM);

}

std::set<std::string> inspect_repr_for_scalars(IR_Code *ir,
		CG_outputRepr * repr, std::set<std::string> ignore) {

	std::vector<IR_ScalarRef *> refs = ir->FindScalarRef(repr);
	std::set<std::string> loop_vars;

	for (int i = 0; i < refs.size(); i++)
		if (ignore.find(refs[i]->name()) == ignore.end())
			loop_vars.insert(refs[i]->name());

	return loop_vars;

}

std::set<std::string> inspect_loop_bounds(IR_Code *ir, const Relation &R,
		int pos,
		std::map<std::string, std::vector<omega::CG_outputRepr *> > &uninterpreted_symbols) {

	if (!R.is_set())
		throw loop_error("Input R has to be a set not a relation!");

	std::set<std::string> vars;

	std::vector<CG_outputRepr *> refs;
	Variable_ID v = const_cast<Relation &>(R).set_var(pos);
	for (DNF_Iterator di(const_cast<Relation &>(R).query_DNF()); di; di++) {
		for (GEQ_Iterator gi = (*di)->GEQs(); gi; gi++) {
			if ((*gi).get_coef(v) != 0 && (*gi).is_const_except_for_global(v)) {
				for (Constr_Vars_Iter cvi(*gi); cvi; cvi++) {
					Variable_ID v = cvi.curr_var();
					switch (v->kind()) {

					case Global_Var: {
						Global_Var_ID g = v->get_global_var();
						Variable_ID v2;
						if (g->arity() > 0) {

							std::string s = g->base_name();
							std::copy(uninterpreted_symbols.find(s)->second.begin(),
									uninterpreted_symbols.find(s)->second.end(),
									back_inserter(refs));

						}

						break;
					}
					default:
						break;
					}
				}

			}
		}
	}

	for (int i = 0; i < refs.size(); i++) {
		std::vector<IR_ScalarRef *> refs_ = ir->FindScalarRef(refs[i]);

		for (int j = 0; j < refs_.size(); j++)
			vars.insert(refs_[j]->name());

	}
	return vars;
}

CG_outputRepr * create_counting_loop_body(IR_Code *ir, const Relation &R,
		int pos, CG_outputRepr * count,
		std::map<std::string, std::vector<omega::CG_outputRepr *> > &uninterpreted_symbols,
		Relation known) {

	if (!R.is_set())
		throw loop_error("Input R has to be a set not a relation!");

	std::vector<CG_outputRepr *> ub, lb;
	std::vector<CG_outputRepr *> refs;
	Variable_ID v = const_cast<Relation &>(R).set_var(pos);

	if (!is_single_loop_iteration(const_cast<Relation &>(R), pos, known)) {
		Conjunct *c = const_cast<Relation &>(R).single_conjunct();

		//for (DNF_Iterator di(const_cast<Relation &>(R).query_DNF()); di; di++) {
		for (GEQ_Iterator gi = (c)->GEQs(); gi; gi++) {

			if ((*gi).get_coef(v) != 0 && (*gi).is_const_except_for_global(v)) {
				bool same_ge_1 = false;
				bool same_ge_2 = false;
				bool lt_or_gt;
				if ((*gi).get_coef(v) > 0)
					lt_or_gt = false;
				else
					lt_or_gt = true;
				CG_outputRepr *repr = NULL;
				for (Constr_Vars_Iter cvi(*gi); cvi; cvi++) {
					Variable_ID v = cvi.curr_var();
					switch (v->kind()) {

					case Global_Var: {
						Global_Var_ID g = v->get_global_var();
						Variable_ID v2;
						std::string s = g->base_name();

						int coef = cvi.curr_coef();
						if (!lt_or_gt)
							coef = -coef;
						if (g->arity() > 0) {

							if (coef != 1)
								repr =
										ir->builder()->CreatePlus(repr,
												ir->builder()->CreateTimes(
														ir->builder()->CreateInt(coef),
														ir->builder()->CreateInvoke(s,
																uninterpreted_symbols.find(s)->second)));
							else
								repr = ir->builder()->CreatePlus(repr,
										ir->builder()->CreateInvoke(s,
												uninterpreted_symbols.find(s)->second));

						}

						else {
							if (coef != 1)
								repr = ir->builder()->CreatePlus(repr,
										ir->builder()->CreateTimes(
												ir->builder()->CreateInt(coef),
												ir->builder()->CreateIdent(s)));
							else
								repr = ir->builder()->CreatePlus(repr,
										ir->builder()->CreateIdent(s));

						}
						break;
					}
					default:
						break;
					}
				}

				if (lt_or_gt) {
					CG_outputRepr *constant = ir->builder()->CreatePlus(repr,
							ir->builder()->CreateInt(-(*gi).get_const()));
					lb.push_back(constant);
				} else {
					CG_outputRepr *constant;
					if ((*gi).get_const() < 0)
						constant = ir->builder()->CreateMinus(repr,
								ir->builder()->CreateInt((*gi).get_const()));
					else
						constant = ir->builder()->CreatePlus(repr,
								ir->builder()->CreateInt((*gi).get_const()));

					ub.push_back(constant);

				}

				/*if (same_ge_1 && same_ge_2)
				 lb = ir->builder()->CreatePlus(lb->clone(),
				 ir->builder()->CreateInt(-(*gi).get_const()));
				 else if (same_ge_1)
				 ub = ir->builder()->CreatePlus(ub->clone(),
				 ir->builder()->CreateInt(-(*gi).get_const()));
				 else if (same_ge_2)
				 lb = ir->builder()->CreatePlus(lb->clone(),
				 ir->builder()->CreateInt(-(*gi).get_const()));
				 */
			}
		}

		//}
		CG_outputRepr *ub1, *lb1;

		if (ub.size() > 1)
			ub1 = ir->builder()->CreateInvoke("min", ub);
		else
			ub1 = ub[0];

		if (lb.size() > 1)
			lb1 = ir->builder()->CreateInvoke("min", lb);
		else
			lb1 = lb[0];

		return ir->builder()->CreatePlusAssignment(0, count,
				ir->builder()->CreatePlus(
						ir->builder()->CreateMinus(ub1->clone(), lb1->clone()),
						ir->builder()->CreateInt(1)));
	} else
		return ir->builder()->CreatePlusAssignment(0, count,
				ir->builder()->CreateInt(1));
}

std::map<std::string, std::vector<std::string> > recurse_on_exp_for_arrays(
		IR_Code * ir, CG_outputRepr * exp) {

	std::map<std::string, std::vector<std::string> > arr_index_to_ref;
	switch (ir->QueryExpOperation(exp)) {

	case IR_OP_ARRAY_VARIABLE: {
		IR_ArrayRef *ref = dynamic_cast<IR_ArrayRef *>(ir->Repr2Ref(exp));
		IR_PointerArrayRef *ref_ =
				dynamic_cast<IR_PointerArrayRef *>(ir->Repr2Ref(exp));
		if (ref == NULL && ref_ == NULL)
			throw loop_error("Array symbol unidentifiable!");

		if (ref != NULL) {
			std::vector<std::string> s0;

			for (int i = 0; i < ref->n_dim(); i++) {
				CG_outputRepr * index = ref->index(i);
				std::map<std::string, std::vector<std::string> > a0 =
						recurse_on_exp_for_arrays(ir, index);
				std::vector<std::string> s;
				for (std::map<std::string, std::vector<std::string> >::iterator j =
						a0.begin(); j != a0.end(); j++) {
					if (j->second.size() != 1 && (j->second)[0] != "")
						throw loop_error(
								"indirect array references not allowed in guard!");
					s.push_back(j->first);
				}
				std::copy(s.begin(), s.end(), back_inserter(s0));
			}
			arr_index_to_ref.insert(
					std::pair<std::string, std::vector<std::string> >(ref->name(),
							s0));
		} else {
			std::vector<std::string> s0;
			for (int i = 0; i < ref_->n_dim(); i++) {
				CG_outputRepr * index = ref_->index(i);
				std::map<std::string, std::vector<std::string> > a0 =
						recurse_on_exp_for_arrays(ir, index);
				std::vector<std::string> s;
				for (std::map<std::string, std::vector<std::string> >::iterator j =
						a0.begin(); j != a0.end(); j++) {
					if (j->second.size() != 1 && (j->second)[0] != "")
						throw loop_error(
								"indirect array references not allowed in guard!");
					s.push_back(j->first);
				}
				std::copy(s.begin(), s.end(), back_inserter(s0));
			}
			arr_index_to_ref.insert(
					std::pair<std::string, std::vector<std::string> >(ref_->name(),
							s0));
		}
		break;
	}
	case IR_OP_PLUS:
	case IR_OP_MINUS:
	case IR_OP_MULTIPLY:
	case IR_OP_DIVIDE: {
		std::vector<CG_outputRepr *> v = ir->QueryExpOperand(exp);
		std::map<std::string, std::vector<std::string> > a0 =
				recurse_on_exp_for_arrays(ir, v[0]);
		std::map<std::string, std::vector<std::string> > a1 =
				recurse_on_exp_for_arrays(ir, v[1]);
		arr_index_to_ref.insert(a0.begin(), a0.end());
		arr_index_to_ref.insert(a1.begin(), a1.end());
		break;

	}
	case IR_OP_POSITIVE:
	case IR_OP_NEGATIVE: {
		std::vector<CG_outputRepr *> v = ir->QueryExpOperand(exp);
		std::map<std::string, std::vector<std::string> > a0 =
				recurse_on_exp_for_arrays(ir, v[0]);

		arr_index_to_ref.insert(a0.begin(), a0.end());
		break;

	}
	case IR_OP_VARIABLE: {
		std::vector<CG_outputRepr *> v = ir->QueryExpOperand(exp);
		IR_ScalarRef *ref = static_cast<IR_ScalarRef *>(ir->Repr2Ref(v[0]));

		std::string s = ref->name();
		std::vector<std::string> to_insert;
		to_insert.push_back("");
		arr_index_to_ref.insert(
				std::pair<std::string, std::vector<std::string> >(s, to_insert));
		break;
	}
	case IR_OP_CONSTANT:
		break;

	default: {
		std::vector<CG_outputRepr *> v = ir->QueryExpOperand(exp);

		for (int i = 0; i < v.size(); i++) {
			std::map<std::string, std::vector<std::string> > a0 =
					recurse_on_exp_for_arrays(ir, v[i]);

			arr_index_to_ref.insert(a0.begin(), a0.end());
		}

		break;
	}
	}
	return arr_index_to_ref;
}

std::vector<CG_outputRepr *> find_guards(IR_Code *ir, IR_Control *code) {

	std::vector<CG_outputRepr *> guards;
	switch (code->type()) {
	case IR_CONTROL_IF: {
		CG_outputRepr *cond = dynamic_cast<IR_If*>(code)->condition();

		std::vector<CG_outputRepr *> then_body;
		std::vector<CG_outputRepr *> else_body;
		if (dynamic_cast<IR_If*>(code)->then_body() != NULL)
			then_body = find_guards(ir, dynamic_cast<IR_If*>(code)->then_body());
		if (dynamic_cast<IR_If*>(code)->else_body() != NULL)
			else_body = find_guards(ir, dynamic_cast<IR_If*>(code)->else_body());
		guards.push_back(cond);
		if (then_body.size() > 0)
			std::copy(then_body.begin(), then_body.end(), back_inserter(guards));
		if (else_body.size() > 0)
			std::copy(else_body.begin(), else_body.end(), back_inserter(guards));
		break;
	}
	case IR_CONTROL_BLOCK: {
		std::vector<IR_Control *> stmts = ir->FindOneLevelControlStructure(
				dynamic_cast<IR_Block*>(code));
		for (int i = 0; i < stmts.size(); i++) {
			std::vector<CG_outputRepr *> stmt_repr = find_guards(ir, stmts[i]);
			std::copy(stmt_repr.begin(), stmt_repr.end(), back_inserter(guards));
		}
		break;
	}
	case IR_CONTROL_LOOP: {
		std::vector<CG_outputRepr *> body = find_guards(ir,
				dynamic_cast<IR_Loop*>(code)->body());
		if (body.size() > 0)
			std::copy(body.begin(), body.end(), back_inserter(guards));
		break;
	}
	}
	return guards;
}

bool sort_helper(std::pair<std::string, std::vector<std::string> > i,
		std::pair<std::string, std::vector<std::string> > j) {
	int c1 = 0;
	int c2 = 0;
	for (int k = 0; k < i.second.size(); k++)
		if (i.second[k] != "")
			c1++;

	for (int k = 0; k < j.second.size(); k++)
		if (j.second[k] != "")
			c2++;
	return (c1 < c2);

}

bool sort_helper_2(std::pair<int, int> i, std::pair<int, int> j) {

	return (i.second < j.second);

}

std::vector<std::string> construct_iteration_order(
		std::map<std::string, std::vector<std::string> > & input) {
	std::vector<std::string> arrays;
	std::vector<std::string> scalars;
	std::vector<std::pair<std::string, std::vector<std::string> > > input_aid;

	for (std::map<std::string, std::vector<std::string> >::iterator j =
			input.begin(); j != input.end(); j++)
		input_aid.push_back(
				std::pair<std::string, std::vector<std::string> >(j->first,
						j->second));

	std::sort(input_aid.begin(), input_aid.end(), sort_helper);

	for (int j = 0; j < input_aid[input_aid.size() - 1].second.size(); j++)
		if (input_aid[input_aid.size() - 1].second[j] != "") {
			arrays.push_back(input_aid[input_aid.size() - 1].second[j]);

		}

	if (arrays.size() > 0) {
		for (int i = input_aid.size() - 2; i >= 0; i--) {

			int max_count = 0;
			for (int j = 0; j < input_aid[i].second.size(); j++)
				if (input_aid[i].second[j] != "") {
					max_count++;
				}
			if (max_count > 0) {
				for (int j = 0; j < max_count; j++) {
					std::string s = input_aid[i].second[j];
					bool found = false;
					for (int k = 0; k < max_count; k++)
						if (s == arrays[k])
							found = true;
					if (!found)
						throw loop_error("guard condition not solvable");
				}
			} else {
				bool found = false;
				for (int k = 0; k < arrays.size(); k++)
					if (arrays[k] == input_aid[i].first)
						found = true;
				if (!found)
					arrays.push_back(input_aid[i].first);
			}
		}
	} else {

		for (int i = input_aid.size() - 1; i >= 0; i--) {
			arrays.push_back(input_aid[i].first);
		}
	}
	return arrays;
}

void Loop::compact(int stmt_num, std::vector<int> level_,
		std::vector<std::string> new_array, int zero,
		std::vector<std::string> data_array, Relation R, bool max_allocate,
		std::vector<int> additional_compacted) {

	int level = level_[0];
	int dim = level - 1;
	bool enforce_marked = false;

	for (int i = 1; i < level_.size(); i++)
		if (level_[i] - level_[i - 1] != 1)
			throw loop_error("loop levels for compaction must be continuous");
	std::vector<int> lex = getLexicalOrder(stmt_num);
	std::set<int> same_loop; // = getStatements(lex, dim - 1);
	std::set<int> same_loop_;
	std::pair<Relation, Relation> init_is;
	CG_outputBuilder *ocg = ir->builder();
	Relation update;
	std::vector<Relation> actual_bounds;
	std::set<int> remaining_loops;
	std::set<int> level2;
	std::vector<int> loops_for_non_zero_block_count;
	std::map<int, Relation> inner_loops;
	Relation copy_IS;
	CG_outputRepr *old_code;
	IR_PointerSymbol *offset_index2;
	std::vector<CG_outputRepr *> guards;
	is_single = false;
	same_loop_.insert(stmt_num);
	Relation old_IS;
	Relation old_XFORM;
	std::vector<IR_PointerSymbol *> marked3;
	std::vector<IR_PointerSymbol *> explicit_index;
	std::vector<std::vector<CG_outputRepr *> > data_prime_ref2;
	std::vector<LoopLevel> old_loop_level;
	IR_Control *code;
	std::set<int> cancelled_loops;
	std::vector<std::pair<int, int> > inner_loop_bounds;
	same_loop = getStatements(lex, dim - 1);
	std::vector<CG_outputRepr *> size_repr_;
	for (std::set<int>::iterator it = same_loop.begin(); it != same_loop.end();
			it++) {

		apply_xform(*it);
	}

	for (std::set<int>::iterator it = same_loop_.begin(); it != same_loop_.end();
			it++) {
		apply_xform(*it);
		old_IS = stmt[*it].IS;
		old_XFORM = stmt[*it].xform;

		Relation simplification = omega::copy(R);
		Relation result;
		//begin: intersect with known: Must be a separate function
		if (simplification.n_set() < old_XFORM.n_out()) {
			omega::Relation known = omega::Extend_Set(omega::copy(simplification),
					old_XFORM.n_out() - simplification.n_set());
			result = omega::Intersection(
					omega::Range(
							omega::Restrict_Domain(omega::copy(old_XFORM),
									omega::copy(old_IS))), known);
		} else {

			result = omega::Range(
					omega::Restrict_Domain(omega::copy(old_XFORM),
							omega::copy(old_IS)));

			result = omega::Extend_Set(result,
					simplification.n_set() - old_XFORM.n_out());
			result = omega::Intersection(result, simplification);

		}
		//end

		Relation C = get_loop_bound(result, 2 * level, known);
		CG_outputRepr *max_size = NULL;
		CG_outputRepr *max_size2 = NULL;
		if (is_single_loop_iteration(C, 2 * level, known))
			is_single = true;
		//if (max_allocate) {

		//outer loops size calculation
		//encapsulate the following
		for (int i = 1; i < level; i++) {

			std::vector<CG_outputRepr *> ub, lb;
			Relation R = get_loop_bound(result, 2 * i, this->known);
			//R.print();
			//for (DNF_Iterator di(const_cast<Relation &>(R).query_DNF());
			//     di; di++) {
			Conjunct *c = const_cast<Relation &>(R).single_conjunct();
			for (GEQ_Iterator gi(c->GEQs()); gi; gi++) {

				if ((*gi).is_const_except_for_global(R.set_var(2 * i))) {
					CG_outputRepr *repr = NULL;
					for (Constr_Vars_Iter cvi(*gi); cvi; cvi++) {
						Variable_ID v = cvi.curr_var();
						switch (v->kind()) {

						case Global_Var: {

							Global_Var_ID g = v->get_global_var();
							std::string s = g->base_name();
							Variable_ID v2;
							int coef = cvi.curr_coef();
							if (coef < 0)
								coef = -coef;
							if (g->arity() > 0) {
								if (coef != 1) {
									if (cvi.curr_coef() == coef)
										repr = ir->builder()->CreatePlus(repr,
												ir->builder()->CreateTimes(
														ir->builder()->CreateInt(coef),
														ir->builder()->CreateInvoke(s,
																uninterpreted_symbols[*it].find(
																		s)->second)));
									else
										repr = ir->builder()->CreateMinus(repr,
												ir->builder()->CreateTimes(
														ir->builder()->CreateInt(coef),
														ir->builder()->CreateInvoke(s,
																uninterpreted_symbols[*it].find(
																		s)->second)));
								} else {
									if (cvi.curr_coef() == coef)
										repr =
												ir->builder()->CreatePlus(repr,
														ir->builder()->CreateInvoke(s,
																uninterpreted_symbols[*it].find(
																		s)->second));
									else
										repr =
												ir->builder()->CreateMinus(repr,
														ir->builder()->CreateInvoke(s,
																uninterpreted_symbols[*it].find(
																		s)->second));
								}
							} else {
								if (coef != 1) {
									if (cvi.curr_coef() == coef)
										repr = ir->builder()->CreatePlus(repr,
												ir->builder()->CreateTimes(
														ir->builder()->CreateInt(coef),
														ir->builder()->CreateIdent(s)));
									else
										repr = ir->builder()->CreateMinus(repr,
												ir->builder()->CreateTimes(
														ir->builder()->CreateInt(coef),
														ir->builder()->CreateIdent(s)));

								} else {
									if (cvi.curr_coef() == coef)
										repr = ir->builder()->CreatePlus(repr,
												ir->builder()->CreateIdent(s));
									else
										repr = ir->builder()->CreateMinus(repr,
												ir->builder()->CreateIdent(s));
								}
							}
							break;
						}
						}
					}
					if ((*gi).get_coef(R.set_var(2 * i)) < 0) {
						if ((*gi).get_const() > 0)
							ub.push_back(
									ocg->CreatePlus(repr,
											ocg->CreateInt((*gi).get_const())));
						else
							ub.push_back(
									ocg->CreateMinus(repr,
											ocg->CreateInt(-(*gi).get_const())));
					} else {
						if ((*gi).get_const() > 0)
							lb.push_back(
									ocg->CreateMinus(repr,
											ocg->CreateInt((*gi).get_const())));
						else
							lb.push_back(
									ocg->CreatePlus(repr,
											ocg->CreateInt(-(*gi).get_const())));

					}
				}
			}
			CG_outputRepr *ub1, *lb1;
			if (ub.size() > 1)
				ub1 = ir->builder()->CreateInvoke("min", ub);
			else
				ub1 = ub[0];

			if (lb.size() > 1)
				lb1 = ir->builder()->CreateInvoke("min", lb);
			else
				lb1 = lb[0];

			size_repr_.push_back(
					ir->builder()->CreatePlus(
							ir->builder()->CreateMinus(ub1->clone(), lb1->clone()),
							ir->builder()->CreateInt(1)));
		}
		if (max_allocate && level > 1)
			max_size = size_repr_[0]->clone();

		//}
		//}
		Relation equalities_in_xform;
		CG_outputRepr *x_check = NULL;
		//equalities_in_xform = replicate_complex_equalities_in_xform(
		//		stmt[stmt_num].xform);

		// 1a. Identify set of loops enclosed by level (store inner loops' bounds)

		for (int i = level_[level_.size() - 1] + 1;
				i <= stmt[*it].loop_level.size(); i++) {

			if (stmt[*it].loop_level[i - 1].made_dense)
				break;
			enforce_marked = true;
			Relation bound = get_loop_bound(stmt[*it].IS, i, this->known);
			inner_loops.insert(std::pair<int, Relation>(i, bound));
			if (_DEBUG_)
				bound.print();
		}

		// 1b. Extra Check for Padding, in case N does not divide C
		//	    i.   Get Loop Index of interior loop
		// 	ii.  Check for arrays being subscripted by interior loop
		// 	iii. Check for other indices in subscript
		copy_IS = copy(stmt[*it].IS);

		for (std::map<int, Relation>::iterator it2 = inner_loops.begin();
				it2 != inner_loops.end(); it2++) {
			std::string index = stmt[*it].IS.set_var(it2->first)->name();

			Relation bound = it2->second;
			int max = -1;
			for (DNF_Iterator di(bound.query_DNF()); di; di++)
				for (GEQ_Iterator gi = (*di)->GEQs(); gi; gi++)
					if (((*gi).get_coef(bound.set_var(it2->first)) == -1)
							&& (*gi).is_const(bound.set_var(it2->first)))
						max = (*gi).get_const();

			Relation r(copy_IS.n_set());
			r.copy_names(copy_IS);
			r.setup_names();

			F_And *root = r.add_and();
			GEQ_Handle h = root->add_GEQ();
			h.update_coef(r.set_var(it2->first), -1);
			h.update_const(max);
			GEQ_Handle g = root->add_GEQ();
			g.update_coef(r.set_var(it2->first), 1);

			copy_IS = and_with_relation_and_replace_var(copy_IS,
					copy_IS.set_var(it2->first), r);
		}

		for (std::map<int, Relation>::iterator it2 = inner_loops.begin();
				it2 != inner_loops.end(); it2++) {
			std::vector<IR_PointerArrayRef *> refs2 = ir->FindPointerArrayRef(
					stmt[*it].code);
			std::string index = stmt[*it].IS.set_var(it2->first)->name();

			std::set<IR_PointerArrayRef *> subscripts;
			for (int i = 0; i < refs2.size(); i++) {
				//assert(refs2[i]->n_dim() == 1);
				std::vector<IR_ScalarRef *> scalar_refs = ir->FindScalarRef(
						refs2[i]->index(0));
				for (int j = 0; j < scalar_refs.size(); j++)
					if (scalar_refs[j]->name() == index)
						subscripts.insert(refs2[i]);
			}

			for (std::set<IR_PointerArrayRef *>::iterator i = subscripts.begin();
					i != subscripts.end(); i++) {
				CG_outputRepr *repr = (*i)->index(0);

				Relation mapping(stmt[*it].IS.n_set(), stmt[*it].IS.n_set() + 1);
				for (int k = 1; k <= mapping.n_inp(); k++)
					mapping.name_input_var(k, stmt[*it].IS.set_var(k)->name());
				mapping.setup_names();
				F_And *f_root = mapping.add_and();
				for (int k = 1; k <= mapping.n_inp(); k++) {
					EQ_Handle h = f_root->add_EQ();
					h.update_coef(mapping.input_var(k), 1);
					h.update_coef(mapping.output_var(k), -1);
				}

				exp2formula(this, ir, mapping, f_root, freevar, repr->clone(),
						mapping.output_var(stmt[*it].IS.n_set() + 1), 'w', IR_COND_EQ,
						false, uninterpreted_symbols[*it],
						uninterpreted_symbols_stringrepr[*it], unin_rel[*it]);

				Relation r;

				if (known.n_set() < copy_IS.n_set())
					r =

					omega::Range(
							Restrict_Domain(mapping,
									Intersection(copy(copy_IS),
											Extend_Set(copy(this->known), copy_IS.n_set()

											- this->known.n_set()))));
				else

					//hack
					r =

					omega::Range(Restrict_Domain(mapping, copy(copy_IS)));

				if (_DEBUG_)
					r.print();

				for (int j = 1; j <= stmt[*it].IS.n_set(); j++) {
					r = Project(r, j, Input_Var);
					r.simplify();
				}
				r.simplify();

				Conjunct *c = r.query_DNF()->single_conjunct();
				int size = 0;

				if (_DEBUG_)
					r.print();
				for (GEQ_Iterator gi(c->GEQs()); gi; gi++)
					if ((*gi).is_const(r.set_var(stmt[*it].IS.n_set() + 1))
							&& (*gi).get_coef(r.set_var(stmt[*it].IS.n_set() + 1)) < 0)
						size = (*gi).get_const()

						/ (-(*gi).get_coef(r.set_var(stmt[*it].IS.n_set() + 1)));
				if (size == 0)
					break;
				assert(size != 0);

				if (array_dims.find((*i)->name()) != array_dims.end()) {
					int decl_size = array_dims.find((*i)->name())->second;
					if (size + 1 > decl_size) {

						std::vector<CG_outputRepr *> size_repr2;
						size_repr2.push_back(ir->builder()->CreateInt(size + 1));
						IR_PointerSymbol *sub = ir->CreatePointerSymbol(
								(*i)->symbol()->elem_type(), size_repr2);
						x_check = ir->builder()->StmtListAppend(x_check,
								ir->CreateMalloc((*i)->symbol()->elem_type(),
										sub->name(), size_repr2[0]->clone()));
						ptr_variables.push_back(static_cast<IR_PointerSymbol *>(sub));
						cleanup_code = ir->builder()->StmtListAppend(cleanup_code,
								ir->CreateFree(
										ir->builder()->CreateIdent(sub->name())));
						IR_ScalarSymbol * iter = ir->CreateScalarSymbol(
								IR_CONSTANT_INT, 0, "");
						Relation new_rel(1);
						new_rel.name_set_var(1, iter->name());
						F_And *g_root = new_rel.add_and();

						GEQ_Handle g = g_root->add_GEQ();
						g.update_coef(new_rel.set_var(1), 1);

						GEQ_Handle g1 = g_root->add_GEQ();
						g1.update_coef(new_rel.set_var(1), -1);
						g1.update_const(decl_size - 1);

						std::map<std::string, std::vector<omega::CG_outputRepr *> > uninterpreted_symbols;
						CG_outputRepr *loop = output_loop(ir->builder(), new_rel, 1,
								Relation::True(1),
								std::vector<std::pair<CG_outputRepr *, int> >(1,
										std::make_pair(static_cast<CG_outputRepr *>(NULL),
												0)), uninterpreted_symbols);

						CG_outputRepr *body = ocg->CreateAssignment(0,
								ocg->CreateArrayRefExpression(sub->name(),
										ocg->CreateIdent(iter->name())),
								ocg->CreateArrayRefExpression((*i)->name(),
										ocg->CreateIdent(iter->name())));

						body = ir->builder()->CreateLoop(0, loop, body);

						x_check = ir->builder()->StmtListAppend(x_check, body);

						ir->ReplaceExpression(*i,
								ir->builder()->CreateArrayRefExpression(sub->name(),
										repr->clone()));

					}

				}

				// get_bounds
				// 	iv.  Compute max data footprint
				// 	v.   Check if max data exceeds array_dims
				// 	vi   If so extend and copy	(ReplaceExpression)

			}
		}

		// 2. Identify inner guard, if more than one throw error for now

		code = ir->GetCode(stmt[*it].code->clone());
		std::set<std::string> derivative_loops;
		guards = find_guards(ir, code);

		if (guards.size() > 1)
			throw loop_error(
					"Support for compaction with multiple guards currently not supported!");
		std::map<int, int> dont_project;
		std::map<int, CG_outputRepr *> most_reprs;
		std::vector<std::pair<int, int> > name_to_pos;
		if (guards.size() != 0) {
			//throw loop_error("No guards found within compact command");

			// 3. Exp2formula/constraint for  guard
			std::map<std::string, std::vector<std::string> > res =
					recurse_on_exp_for_arrays(ir, guards[0]->clone());

			if (_DEBUG_)
				for (std::map<std::string, std::vector<std::string> >::iterator j =
						res.begin(); j != res.end(); j++) {
					std::cout << j->first << std::endl;
					for (int i = 0; i < j->second.size(); i++)
						if ((j->second)[i] != "")
							std::cout << (j->second)[i] << std::endl;

					std::cout << std::endl;
				}

			std::vector<std::string> variable_order = construct_iteration_order(
					res);

			if (_DEBUG_) {
				for (int i = 0; i < variable_order.size(); i++)
					std::cout << variable_order[i] << std::endl;
			}

			Relation r(variable_order.size());

			for (int i = 0; i < variable_order.size(); i++)
				r.name_set_var(i + 1, variable_order[i]);

			//std::vector<std::pair<int, int> > name_to_pos;
			for (int j = 0; j < variable_order.size(); j++) {
				bool found = false;
				for (int i = 1; i <= stmt[*it].IS.n_set(); i++)
					if (stmt[*it].IS.set_var(i)->name() == variable_order[j]) {
						name_to_pos.push_back(std::pair<int, int>(j + 1, i));
						found = true;
					}
				if (!found)
					throw loop_error("guard condition too complex to compact");
			}

			F_Exists *f_exists = r.add_and()->add_exists();
			F_And *f_root = f_exists->add_and();
			std::map<Variable_ID, Variable_ID> exists_mapping;
			for (std::vector<std::pair<int, int> >::iterator it3 =
					name_to_pos.begin(); it3 != name_to_pos.end(); it3++) {
				Relation R = get_loop_bound(stmt[*it].IS, it3->second, this->known);
				bool ignore = false;

				for (DNF_Iterator di(const_cast<Relation &>(R).query_DNF()); di;
						di++)
					for (GEQ_Iterator gi((*di)->GEQs()); gi; gi++) {
						GEQ_Handle h = f_root->add_GEQ();
						if ((*gi).get_coef(R.set_var(it3->second)) != 0) {
							for (Constr_Vars_Iter cvi(*gi); cvi; cvi++) {
								Variable_ID v = cvi.curr_var();
								switch (v->kind()) {

								case Global_Var: {

									Global_Var_ID g = v->get_global_var();

									if (g->arity() > 0)
										ignore = true;

									break;

								}
								default:
									break;
								}
							}

						}
					}
				if (ignore)
					dont_project.insert(
							std::pair<int, int>(it3->first, it3->second));
			}

			//assert(dont_project.size() == 1);

			std::map<int, int> pos_map;
			for (int i = 0; i < name_to_pos.size(); i++)
				pos_map.insert(name_to_pos[i]);
			for (std::vector<std::pair<int, int> >::iterator it2 =
					name_to_pos.begin(); it2 != name_to_pos.end(); it2++) {
				bool skip = false;
				//for ( i = 0; i < dont_project.size(); i++)
				if (dont_project.find(it2->first) != dont_project.end())
					skip = true;

				if (!skip) {
					actual_bounds.push_back(
							replace_set_var_as_another_set_var(r,
									get_loop_bound(stmt[*it].IS, it2->second,
											this->known), it2->first, it2->second,
									pos_map));

				}
				// pos_map.insert(std::pair<int,int>(it->second, it->first));
			}

			if (_DEBUG_) {
				//	r.print();
				for (std::map<int, int>::iterator i = dont_project.begin();
						i != dont_project.end(); i++)
					std::cout << i->first << std::endl;
			}
			F_And *f_root_ = r.and_with_and();
			//std::vector<omega::Free_Var_Decl*> freevars;
			exp2constraint(this, ir, r, f_root_, freevar, guards[0]->clone(), true,
					uninterpreted_symbols[*it],
					uninterpreted_symbols_stringrepr[*it], unin_rel[*it]);
			r.simplify();
			if (_DEBUG_)
				r.print();

			// 4. Project from  innermost to outermost and
			//	  back in to see which loops may be cancelled(store cancelled loops (build and store their expressions) )

			std::sort(name_to_pos.begin(), name_to_pos.end(), sort_helper_2);
			if (_DEBUG_)
				for (int i = 0; i < name_to_pos.size(); i++)
					std::cout << name_to_pos[i].first << std::endl;
			update = copy(r);
			update.copy_names(r);
			update.setup_names();
			std::stack<Relation> to_eliminate;
			std::vector<std::pair<int, CG_outputRepr *> > pos_to_repr;

			int already_simplified = -1;

			derivative_loops = inspect_loop_bounds(ir,
					get_loop_bound(stmt[*it].IS, dont_project.begin()->second,
							this->known), dont_project.begin()->second,
					uninterpreted_symbols[*it]);
			//Correction to Projection Algorithm : 28th August 2015
			//begin
			{

				std::set<Variable_ID> derived;
				std::pair<Relation, bool> temp;
				temp.first = copy(update);
				temp.first.copy_names(update);
				temp.first.setup_names();
				//derived loops cannot be eliminated
				for (std::set<std::string>::iterator it2 = derivative_loops.begin();
						it2 != derivative_loops.end(); it2++)
					for (int i1 = 1; i1 <= temp.first.n_set(); i1++)
						if (*it2 == temp.first.set_var(i1)->name())
							derived.insert(temp.first.set_var(i1));

				bool found_existential = false;

				//deals with only one eliminable constraint for now:

				//for all variables in name_to_pos
				//	can variable be projected as existential?
				//     if yes project, construct project repr, replace constraint and push to stack,
				//     with this set to already simplified

				//second round just replace one equality and you're done

				//	for (int i = name_to_pos.size() - 1; i >= 0; i--)
				//		already_simplified.push_back(false);

				Relation to_eliminate;
				//	to_eliminate.push(update);
				std::vector<Relation> bounds = actual_bounds;
				//CG_outputRepr * repr = NULL;

				std::map<int, CG_outputRepr *> reprs;
				std::pair<bool, GEQ_Handle> geq_handle;
				for (int k = 0; k < name_to_pos.size(); k++) {
					temp.first = copy(update);
					temp.first.copy_names(update);
					temp.first.setup_names();
					if (dont_project.find(name_to_pos[k].first)
							== dont_project.end()) {
						//replace mod constraint
						temp = replace_set_var_as_existential(temp.first,
								name_to_pos[k].first, bounds);
						//temp.first.setup_names();
						if (temp.second) {
							std::set<Variable_ID> excluded_floor_vars;
							// find the possible expressions of the form summation(AiVi) <= m
							//where Ai=c for all Vi
							Relation temp2 = update;
							//find all possible floor definitions with this variable replaced by mod
							geq_handle = find_floor_definitions(temp.first);

							if (geq_handle.first) {

								//already_simplified[k] = true
								//constuct mod and mark variable as eliminated
								reprs.insert(
										std::pair<int, CG_outputRepr *>(
												name_to_pos[k].second,
												construct_int_mod(ir->builder(), temp.first,
														geq_handle.second,
														temp.first.set_var(
																name_to_pos[k].first),
														std::vector<
																std::pair<CG_outputRepr *, int> >(
																temp.first.n_set(),
																std::make_pair(
																		static_cast<CG_outputRepr *>(NULL),
																		0)),
														uninterpreted_symbols[*it])));

								found_existential = true;
								already_simplified = k;
								derived.insert(
										temp.first.set_var(name_to_pos[k].first));
								break;
							}

						}

					}
				}
				//check for equality

				if (!found_existential) {
					for (int k = 0; k < name_to_pos.size(); k++) {
						bool legal_replace = false;
						//if k not in derivable vars
						if (derivative_loops.find(
								update.set_var(name_to_pos[k].first)->name())
								== derivative_loops.end()) {
							std::pair<EQ_Handle, int> eq_handle =
									find_simplest_assignment(temp.first,
											temp.first.set_var(name_to_pos[k].first),
											std::vector<std::pair<CG_outputRepr *, int> >(
													temp.first.n_set(),
													std::make_pair(
															static_cast<CG_outputRepr *>(NULL),
															0)));
							//if constraint is equality
							//if all other variables are derived vars or constants

							legal_replace = checkIfAllExceptVarAreDerived(temp.first,
									eq_handle.first,
									temp.first.set_var(name_to_pos[k].first), derived);

							// output_assignment
							if (legal_replace) {
								std::pair<CG_outputRepr *,
										std::pair<CG_outputRepr *, int> > result =
										output_assignment(ir->builder(), temp.first,
												name_to_pos[k].first, this->known,
												std::vector<std::pair<CG_outputRepr *, int> >(
														temp.first.n_set(),
														std::make_pair(
																static_cast<CG_outputRepr *>(NULL),
																0)),
												uninterpreted_symbols[*it]);

								if (result.first == NULL && result.second.first != NULL
										&& result.second.second != INT_MAX) {

									CG_outputRepr *assign_repr =
											ir->builder()->CreateAssignment(0,
													ir->builder()->CreateIdent(
															temp.first.set_var(
																	name_to_pos[k].first)->name()),
													result.second.first);

									reprs.insert(
											std::pair<int, CG_outputRepr *>(
													name_to_pos[k].second, assign_repr));

								}
							}
						}
						if (legal_replace)
							break;
					}
				} else {
					for (int k = 0; k < name_to_pos.size(); k++) {
						bool legal_replace = false;

						if (k != already_simplified) {
							//check GEQ_Handle
							//for variables involved in GEQ handle, if not derivable
							legal_replace = checkIfAllExceptVarAreDerivedinModHandle(
									temp.first, geq_handle.second, temp.first.set_var(

									name_to_pos[k].first), derived);

							//legal_replace = true;
							if (legal_replace) {
								//output assignment
								reprs.insert(
										std::pair<int, CG_outputRepr *>(
												name_to_pos[k].second,
												output_assignment_for_mod_handle(temp.first,
														ocg, geq_handle.second,
														temp.first.set_var(
																name_to_pos[k].first),
														std::vector<
																std::pair<CG_outputRepr *, int> >(
																temp.first.n_set(),
																std::make_pair(
																		static_cast<CG_outputRepr *>(NULL),
																		0)),
														uninterpreted_symbols[*it])));
								break;
							}

						}

					}

				}

				most_reprs = reprs;
			}
			//end

			/*	for (int i = name_to_pos.size() - 1; i >= 0; i--)
			 already_simplified.push_back(false);

			 int max_count = -1;

			 for (int k = 0; k < name_to_pos.size(); k++) {
			 int project_count = 0;
			 std::map<int, CG_outputRepr *> reprs;
			 for (int i = name_to_pos.size() - 1; i >= 0; i--)
			 already_simplified[i] = false;
			 std::stack<Relation> to_eliminate;
			 to_eliminate.push(update);
			 std::vector<Relation> bounds = actual_bounds;
			 for (int i = k; i < name_to_pos.size() && !to_eliminate.empty();
			 i++) {
			 Relation update = to_eliminate.top();
			 to_eliminate.pop();

			 // int project_count = 0;

			 std::pair<Relation, bool> temp;
			 temp.first = copy(update);
			 temp.first.copy_names(update);
			 temp.first.copy_names(update);
			 temp.first.setup_names();
			 temp.second = false;
			 std::pair<EQ_Handle, int> eq_handle;
			 bool more_variables_left = false;
			 if (dont_project.find(name_to_pos[i].first)
			 == dont_project.end()) {
			 //Projection Active
			 for (int j = name_to_pos.size() - 1; j > i; j--) {
			 if (dont_project.find(name_to_pos[j].first)
			 == dont_project.end()
			 && !already_simplified[j]) {
			 //temp = Project(temp, name_to_pos[j].first,
			 //		Set_Var);
			 temp = replace_set_var_as_existential(
			 temp.first, name_to_pos[j].first,
			 bounds);
			 more_variables_left = true;
			 }
			 }
			 for (int j = i - 1; j >= 0; j--) {
			 if (dont_project.find(name_to_pos[j].first)
			 == dont_project.end()
			 && !already_simplified[j]) {
			 //temp = Project(temp, name_to_pos[j].first,
			 //		Set_Var);
			 temp = replace_set_var_as_existential(
			 temp.first, name_to_pos[j].first,
			 bounds);
			 more_variables_left = true;
			 }

			 }
			 if (temp.second || !more_variables_left) {

			 eq_handle =
			 find_simplest_assignment(temp.first,
			 temp.first.set_var(
			 name_to_pos[i].first),
			 std::vector<
			 std::pair<CG_outputRepr *,
			 int> >(
			 temp.first.n_set(),
			 std::make_pair(
			 static_cast<CG_outputRepr *>(NULL),
			 0)));
			 if (eq_handle.second == INT_MAX) {

			 std::set<Variable_ID> excluded_floor_vars;
			 std::pair<bool, GEQ_Handle> geq_handle =
			 find_floor_definition(temp.first,
			 temp.first.set_var(
			 name_to_pos[i].first),
			 excluded_floor_vars);

			 if (geq_handle.first) {

			 already_simplified[i] = true;
			 project_count++;

			 reprs.insert(
			 std::pair<int, CG_outputRepr *>(
			 name_to_pos[i].second,
			 construct_int_floor(
			 ir->builder(),
			 temp.first,
			 geq_handle.second,
			 temp.first.set_var(
			 name_to_pos[i].first),
			 std::vector<
			 std::pair<
			 CG_outputRepr *,
			 int> >(
			 temp.first.n_set(),
			 std::make_pair(
			 static_cast<CG_outputRepr *>(NULL),
			 0)),
			 uninterpreted_symbols[*it])));

			 //	reprs.push_back(result.second.first);
			 Relation update_prime =
			 replace_set_var_as_Global(update,
			 name_to_pos[i].first,
			 bounds);

			 std::vector<Relation>::iterator it;
			 bool erase = false;
			 for (it = bounds.begin();
			 it != bounds.end(); it++) {
			 for (DNF_Iterator di(it->query_DNF());
			 di; di++) {
			 for (GEQ_Iterator gi((*di)->GEQs());
			 gi; gi++) {

			 if ((*gi).get_coef(
			 it->set_var(
			 name_to_pos[i].first))
			 != 0
			 && (*gi).is_const_except_for_global(
			 it->set_var(
			 name_to_pos[i].first))) {
			 erase = true;
			 break;
			 }
			 if (erase)
			 break;
			 }
			 if (erase)
			 break;
			 }
			 if (erase)
			 break;
			 }
			 if (erase)
			 bounds.erase(it);

			 //	update_prime.copy_names(update);
			 update_prime.setup_names();
			 //update_prime.and_with_GEQ(geq_handle.second);
			 //update_prime = and_with_relation_and_replace_var(
			 //		update,
			 //		update.set_var(name_to_pos[i].first),
			 //			update_prime);
			 to_eliminate.push(update_prime);

			 } else {
			 bool found = false;
			 for (int j = i + 1; j < name_to_pos.size();
			 j++) {
			 if (dont_project.find(
			 name_to_pos[j].first)
			 == dont_project.end()
			 && !already_simplified[j]) {
			 //temp = Project(temp, name_to_pos[j].first,
			 //		Set_Var);
			 Relation temp =
			 replace_set_var_as_Global(
			 update,
			 name_to_pos[j].first,
			 bounds);
			 eq_handle =
			 find_simplest_assignment(
			 temp,
			 temp.set_var(
			 name_to_pos[i].first),
			 std::vector<
			 std::pair<
			 CG_outputRepr *,
			 int> >(
			 temp.n_set(),
			 std::make_pair(
			 static_cast<CG_outputRepr *>(NULL),
			 0)));

			 bool erase = false;
			 std::vector<Relation>::iterator it2;
			 for (it2 = bounds.begin();
			 it2 != bounds.end();
			 it2++) {
			 for (DNF_Iterator di(
			 it2->query_DNF()); di;
			 di++) {
			 for (GEQ_Iterator gi(
			 (*di)->GEQs()); gi;
			 gi++) {

			 if ((*gi).get_coef(
			 it2->set_var(
			 name_to_pos[j].first))
			 != 0
			 && (*gi).is_const_except_for_global(
			 it2->set_var(
			 name_to_pos[j].first))) {
			 erase = true;
			 break;
			 }
			 if (erase)
			 break;
			 }
			 if (erase)
			 break;
			 }
			 if (erase)
			 break;
			 }
			 if (erase)
			 bounds.erase(it2);
			 already_simplified[j] = true;

			 if (eq_handle.second != INT_MAX) {

			 found = true;

			 already_simplified[i] = true;
			 project_count++;

			 std::pair<CG_outputRepr *,
			 std::pair<
			 CG_outputRepr *,
			 int> > result =
			 output_assignment(
			 ir->builder(),
			 temp,
			 name_to_pos[i].first,
			 this->known,
			 std::vector<
			 std::pair<
			 CG_outputRepr *,
			 int> >(
			 temp.n_set(),
			 std::make_pair(
			 static_cast<CG_outputRepr *>(NULL),
			 0)),
			 uninterpreted_symbols[*it]);

			 if (result.first == NULL
			 && result.second.first
			 != NULL
			 && result.second.second
			 != INT_MAX) {

			 ir->builder()->CreateAssignment(
			 0,
			 ir->builder()->CreateIdent(
			 temp.set_var(
			 name_to_pos[i].first)->name()),
			 result.second.first);

			 reprs.insert(
			 std::pair<int,
			 CG_outputRepr *>(
			 name_to_pos[i].second,
			 result.second.first));
			 Relation update_prime =
			 replace_set_var_as_Global(
			 temp,
			 name_to_pos[i].first,
			 bounds);
			 //update_prime.copy_names(update);
			 update_prime.setup_names();
			 to_eliminate.push(
			 update_prime);

			 std::vector<Relation>::iterator it2;
			 bool erase = false;
			 for (it2 = bounds.begin();
			 it2 != bounds.end();
			 it2++) {
			 for (DNF_Iterator di(
			 it2->query_DNF());
			 di; di++) {
			 for (GEQ_Iterator gi(
			 (*di)->GEQs());
			 gi; gi++) {

			 if ((*gi).get_coef(
			 it2->set_var(
			 name_to_pos[i].first))
			 != 0
			 && (*gi).is_const_except_for_global(
			 it2->set_var(
			 name_to_pos[i].first))) {
			 erase =
			 true;
			 break;
			 }
			 if (erase)
			 break;
			 }
			 if (erase)
			 break;
			 }
			 if (erase)
			 break;
			 }
			 if (erase)
			 bounds.erase(it2);

			 }
			 break;
			 //	update_prime.copy_names(update);
			 //update_prime.setup_names();

			 }

			 }
			 }*/
			/*	if (!found)
			 for (int j = i - 1; j >= 0; j--) {
			 if (dont_project.find(name_to_pos[j].first)
			 == dont_project.end()
			 && !already_simplified[j]) {
			 //temp = Project(temp, name_to_pos[j].first,
			 //		Set_Var);
			 Relation temp =
			 replace_set_var_as_Global(
			 update,
			 name_to_pos[j].first,
			 bounds);
			 eq_handle =
			 find_simplest_assignment(temp,
			 temp.set_var(
			 name_to_pos[i].first),
			 std::vector<
			 std::pair<
			 CG_outputRepr *,
			 int> >(
			 temp.n_set(),
			 std::make_pair(
			 static_cast<CG_outputRepr *>(NULL),
			 0)));

			 bool erase = false;
			 std::vector<Relation>::iterator it;
			 for (it = bounds.begin();
			 it != bounds.end(); it++) {
			 for (DNF_Iterator di(
			 it->query_DNF()); di;
			 di++) {
			 for (GEQ_Iterator gi(
			 (*di)->GEQs()); gi;
			 gi++) {

			 if ((*gi).get_coef(
			 it->set_var(
			 name_to_pos[j].first))
			 != 0
			 && (*gi).is_const_except_for_global(
			 it->set_var(
			 v									 name_to_pos[j].first))) {
			 erase = true;
			 break;
			 }
			 if (erase)
			 break;
			 }
			 if (erase)
			 break;
			 }
			 if (erase)
			 break;
			 }
			 if (erase)
			 bounds.erase(it);
			 already_simplified[j] = true;

			 if (eq_handle.second != INT_MAX) {

			 found = true;

			 already_simplified[i] = true;
			 project_count++;

			 std::pair<CG_outputRepr *,
			 std::pair<CG_outputRepr *,
			 int> > result =
			 output_assignment(
			 ir->builder(), temp,
			 name_to_pos[i].first,
			 this->known,
			 std::vector<
			 std::pair<
			 CG_outputRepr *,
			 int> >(
			 temp.n_set(),
			 std::make_pair(
			 static_cast<CG_outputRepr *>(NULL),
			 0)),
			 uninterpreted_symbols[stmt_num]);

			 if (result.first == NULL
			 && result.second.first
			 != NULL
			 && result.second.second
			 != INT_MAX) {

			 ir->builder()->CreateAssignment(
			 0,
			 ir->builder()->CreateIdent(
			 temp.set_var(
			 name_to_pos[i].first)->name()),
			 result.second.first);

			 reprs.insert(
			 std::pair<int,
			 CG_outputRepr *>(
			 name_to_pos[i].second,
			 result.second.first));
			 Relation update_prime =
			 replace_set_var_as_Global(
			 temp,
			 name_to_pos[i].first,
			 bounds);
			 //update_prime.copy_names(update);
			 update_prime.setup_names();
			 to_eliminate.push(update_prime);

			 std::vector<Relation>::iterator it;
			 bool erase = false;
			 for (it = bounds.begin();
			 it != bounds.end();
			 it++) {
			 for (DNF_Iterator di(
			 it->query_DNF());
			 di; di++) {
			 for (GEQ_Iterator gi(
			 (*di)->GEQs());
			 gi; gi++) {

			 if ((*gi).get_coef(
			 it->set_var(
			 name_to_pos[i].first))
			 != 0
			 && (*gi).is_const_except_for_global(
			 it->set_var(
			 name_to_pos[i].first))) {
			 erase = true;
			 break;
			 }
			 if (erase)
			 break;
			 }
			 if (erase)
			 break;
			 }
			 if (erase)
			 break;
			 }
			 if (erase)
			 bounds.erase(it);

			 }
			 break;
			 //	update_prime.copy_names(update);
			 //update_prime.setup_names();

			 }
			 }

			 }*/

			/*								}
			 }

			 else {
			 already_simplified[i] = true;
			 project_count++;
			 //reprs.push_back(result.second.first);

			 std::pair<CG_outputRepr *,
			 std::pair<CG_outputRepr *, int> > result =
			 output_assignment(ir->builder(),
			 temp.first,
			 name_to_pos[i].first,
			 this->known,
			 std::vector<
			 std::pair<
			 CG_outputRepr *,
			 int> >(
			 temp.first.n_set(),
			 std::make_pair(
			 static_cast<CG_outputRepr *>(NULL),
			 0)),
			 uninterpreted_symbols[*it]);

			 if (result.first == NULL
			 && result.second.first != NULL
			 && result.second.second != INT_MAX) {

			 ir->builder()->CreateAssignment(0,
			 ir->builder()->CreateIdent(
			 temp.first.set_var(
			 name_to_pos[i].first)->name()),
			 result.second.first);

			 reprs.insert(
			 std::pair<int, CG_outputRepr *>(
			 name_to_pos[i].second,
			 result.second.first));
			 Relation update_prime =
			 replace_set_var_as_Global(update,
			 name_to_pos[i].first,
			 bounds);
			 //update_prime.copy_names(update);
			 update_prime.setup_names();
			 to_eliminate.push(update_prime);

			 std::vector<Relation>::iterator it;
			 bool erase = false;
			 for (it = bounds.begin();
			 it != bounds.end(); it++) {
			 for (DNF_Iterator di(it->query_DNF());
			 di; di++) {
			 for (GEQ_Iterator gi((*di)->GEQs());
			 gi; gi++) {

			 if ((*gi).get_coef(
			 it->set_var(
			 name_to_pos[i].first))
			 != 0
			 && (*gi).is_const_except_for_global(
			 it->set_var(
			 name_to_pos[i].first))) {
			 erase = true;
			 break;
			 }
			 if (erase)
			 break;
			 }
			 if (erase)
			 break;
			 }
			 if (erase)
			 break;
			 }
			 if (erase)
			 bounds.erase(it);

			 }


			 }
			 }
			 }
			 }
			 if (project_count > max_count) {
			 most_reprs = reprs;
			 max_count = project_count;

			 }

			 }
			 */

			if (_DEBUG_) {
				//std::cout << max_count << std::endl;

				for (std::map<int, CG_outputRepr *>::iterator it =
						most_reprs.begin(); it != most_reprs.end(); it++) {
					//   for(int i=0; i <most_reprs.size(); i++)
					//	stmt[stmt_num].code = ir->builder()->StmtListAppend(stmt[stmt_num].code->clone(), it->second->clone());
					std::cout << it->first << std::endl;
				}
			}

			// 5. Identify the loop from which the loop index being compacted is being derived(as a function of this loop), identify all loops this loop index is dependent on and compute IS. compacted  being computed non affine loops(pattern match index[i] and index[i+1] to
			//    sum closed form distance. insert loop above computation loop setLexicalOrder,

			if (most_reprs.find(level) == most_reprs.end())
				;	//throw loop_error("Cannot derive compacted iterator !");
			else {
				std::pair<int, CG_outputRepr *> entry = *(most_reprs.find(level));

				std::set<std::string> ignore;
				ignore.insert(stmt[*it].IS.set_var(level)->name());
				std::set<std::string> indices = inspect_repr_for_scalars(ir,
						entry.second, ignore);

				//assert(
				//		indices.size() == 1
				//				&& *(indices.begin())
				//						== stmt[stmt_num].IS.set_var(
				//								dont_project.begin()->second)->name());
			}
		} else {
			int count = 1;
			for (int it2 = level; it2 >= 1; it2--) {

				dont_project.insert(std::pair<int, int>(count, it2));
				name_to_pos.push_back(std::pair<int, int>(count, it2));
				count++;
			}

		}

		if (_DEBUG_) {

			for (std::set<std::string>::iterator it = derivative_loops.begin();
					it != derivative_loops.end(); it++) {
				//   for(int i=0; i <most_reprs.size(); i++)
				//	stmt[stmt_num].code = ir->builder()->StmtListAppend(stmt[stmt_num].code->clone(), it->second->clone());
				std::cout << *it << std::endl;
			}
		}

		Relation counting_IS(derivative_loops.size());
		Relation counting_XFORM(derivative_loops.size(),
				2 * derivative_loops.size() + 1);

		for (std::map<int, Relation>::iterator it2 = inner_loops.begin();
				it2 != inner_loops.end(); it2++) {
			bool found = false;
			for (std::map<int, int>::iterator j = dont_project.begin();
					j != dont_project.end(); j++)
				if (j->second == it2->first)
					found = true;
			if (!found)
				loops_for_non_zero_block_count.push_back(it2->first);

		}

		// 6. Above will give worst case count of non-zero blocks, multiply that by inner loop bounds. compute inner loop bounds

		CG_outputRepr *count = ir->builder()->CreateIdent("chill_count_0");

		CG_outputRepr *loop_body = create_counting_loop_body(ir,
				get_loop_bound(stmt[*it].IS, dont_project.begin()->second,
						this->known), dont_project.begin()->second, count->clone(),
				uninterpreted_symbols[*it], known);

		if (_DEBUG_) {

			for (int it = 0;

			it < loops_for_non_zero_block_count.size(); it++) {
				//   for(int i=0; i <most_reprs.size(); i++)
				//	stmt[stmt_num].code = ir->builder()->StmtListAppend(stmt[stmt_num].code->clone(), it->second->clone());
				std::cout << loops_for_non_zero_block_count[it] << std::endl;

			}
			//stmt[stmt_num].code = ir->builder()->StmtListAppend(stmt[stmt_num].code->clone(), loop_body->clone());
		}

		lex = getLexicalOrder(*it);
		std::vector<int> derived_loops;
		for (std::set<std::string>::iterator it2 = derivative_loops.begin();
				it2 != derivative_loops.end(); it2++) {
			for (int i = 1; i <= stmt[*it].IS.n_set(); i++)
				if (stmt[*it].IS.set_var(i)->name() == *it2) {

					//   for(int i=0; i <most_reprs.size(); i++)
					//	stmt[stmt_num].code = ir->builder()->StmtListAppend(stmt[stmt_num].code->clone(), it->second->clone());
					derived_loops.push_back(i);
					break;
				}
		}
		// AV: 2016 below seems redundant : removing
		/*std::pair<Relation, Relation> IS_and_XFORM =
		 construct_reduced_IS_And_XFORM(ir, stmt[*it].IS,
		 stmt[*it].xform, derived_loops, lex, this->known,
		 uninterpreted_symbols[*it]);

		 CG_outputRepr *body = loop_body;

		 for (int i = derived_loops.size(); i >= 1; i--) {
		 CG_outputRepr *loop = output_loop(ir->builder(), IS_and_XFORM.first,
		 i, this->known,
		 std::vector<std::pair<CG_outputRepr *, int> >(
		 IS_and_XFORM.first.n_set(),
		 std::make_pair(static_cast<CG_outputRepr *>(NULL),
		 0)), uninterpreted_symbols[*it]);

		 body = ir->builder()->CreateLoop(0, loop, body);

		 }
		 *///end redundant part :AV:2016
		CG_outputBuilder *ocg = ir->builder();
		//init_code = ocg->StmtListAppend(init_code,
		//		ocg->CreateAssignment(0, count->clone(), ocg->CreateInt(0)));
		//init_code = ir->builder()->StmtListAppend(init_code, body);

		init_code = ocg->StmtListAppend(init_code, x_check);
		// 7. Malloc offset_index, new_array and explicit_index and marked based on size of kk loop.
		CG_outputRepr *inner_array_cpy_stmts = NULL;
		CG_outputRepr *mark_code = NULL;

		CG_outputRepr * new_code = NULL, *new_code2 = NULL;
		IR_PointerSymbol *indiv_count = NULL;
		IR_PointerSymbol *offset_index, *marked;

		int total = 1;
		if (level > 1) {

			int size;
			std::vector<CG_outputRepr *> size_repr;
			for (int outer = 1; outer < level; outer++) {
				int ub, lb;
				Relation R = get_loop_bound(stmt[*it].IS, outer, this->known);
				for (DNF_Iterator di(const_cast<Relation &>(R).query_DNF()); di;
						di++)
					for (GEQ_Iterator gi((*di)->GEQs()); gi; gi++) {
						if ((*gi).get_coef(R.set_var(outer)) < 0
								&& (*gi).is_const(R.set_var(outer))) {

							ub = (*gi).get_const();
						} else if ((*gi).get_coef(R.set_var(outer)) > 0
								&& (*gi).is_const(R.set_var(outer))) {

							lb = (*gi).get_const();
						}
					}

				if (lb < 0 || ub < 0)
					throw loop_error("bounds of outer loops cannot be determined");

				if (outer == 1)
					size = total;
				int size = (ub - lb + 1);
				total *= size;

				if (!max_allocate) {
					if (outer == 1)
						size_repr.push_back(
								ocg->CreatePlus(ocg->CreateInt(size),
										ocg->CreateInt(1)));
				} else
					size_repr.push_back(max_size);

			}

			offset_index2 = ir->CreatePointerSymbol(IR_CONSTANT_INT, size_repr);

			//for(int i=0; i < level-1;i++)
			offset_index2->set_size(0, size_repr[0]->clone());
			ptr_variables.push_back(
					static_cast<IR_PointerSymbol *>(offset_index2));
			if (!max_allocate) {
				init_code = ocg->StmtListAppend(init_code,
						ir->CreateMalloc(IR_CONSTANT_INT, offset_index2->name(),
								ocg->CreateInt(total + 1)));

				init_code = ocg->StmtListAppend(init_code,
						ocg->CreateAssignment(0,
								ocg->CreateArrayRefExpression(offset_index2->name(),
										ocg->CreateInt(0)), ocg->CreateInt(0)));
			}
			/* Anand: commenting out below, indiv count should count should mirror offset_index: ALERT RED
			 if (most_reprs.find(level - 1) != most_reprs.end()) {

			 std::vector<CG_outputRepr *> size_repr;
			 size_repr.push_back(ocg->CreateInt(size));
			 indiv_count = ir->CreatePointerSymbol(IR_CONSTANT_INT,
			 size_repr);
			 ptr_variables.push_back(
			 static_cast<IR_PointerSymbol *>(indiv_count));
			 init_code = ocg->StmtListAppend(init_code,
			 ir->CreateMalloc(IR_CONSTANT_INT, indiv_count->name(),
			 size_repr[0]->clone()));
			 indiv_count->set_size(0, size_repr[0]->clone());

			 }

			 */

		}
		CG_outputRepr *count_2 = ocg->CreateIdent("chill_count_1");
		std::vector<CG_outputRepr *> size_repr;
		int max_col_size = -1;

		if (!is_single) {
			size_repr.push_back(count_2->clone());

			/*	offset_index = ir->CreatePointerSymbol(IR_CONSTANT_INT, size_repr);
			 ptr_variables.push_back(static_cast<IR_PointerSymbol *>(offset_index));
			 init_code = ocg->StmtListAppend(init_code,
			 ir->CreateMalloc(IR_CONSTANT_INT, offset_index->name(),
			 size_repr[0]->clone()));
			 offset_index->set_size(0, size_repr[0]->clone());
			 */
			//	std::vector<CG_outputRepr *> size_repr2;
			//	size_repr2.push_back(count->clone());
			for (int i = 0; i < level_.size(); i++) {
				Relation R = get_loop_bound(stmt[*it].IS, level_[i], this->known);
				int lb = -1, ub = -1;
				for (DNF_Iterator di(const_cast<Relation &>(R).query_DNF()); di;
						di++)
					for (GEQ_Iterator gi((*di)->GEQs()); gi; gi++) {
						if ((*gi).get_coef(R.set_var(level_[i])) < 0
								&& (*gi).is_const(R.set_var(level_[i]))) {

							ub = (*gi).get_const();
						} else if ((*gi).get_coef(R.set_var(level_[i])) > 0
								&& (*gi).is_const(R.set_var(level_[i]))) {

							lb = (*gi).get_const();
						}
					}

				if (lb < 0 || ub < 0)
					throw loop_error("bounds of inner loops cannot be determined");

				if ((ub - lb + 1) > max_col_size)
					max_col_size = ub - lb + 1;

			}

		} else
			size_repr.push_back(ocg->CreateInt(1));

		for (int i = 0; i < level_.size(); i++) {
			if (max_col_size < 65536)
				explicit_index.push_back(
						ir->CreatePointerSymbol(IR_CONSTANT_SHORT, size_repr));
			else
				explicit_index.push_back(
						ir->CreatePointerSymbol(IR_CONSTANT_INT, size_repr));
			explicit_index[i]->set_size(0, size_repr[0]->clone());
			ptr_variables.push_back(
					static_cast<IR_PointerSymbol *>(explicit_index[i]));
		}

		/*init_code = ocg->StmtListAppend(init_code,
		 ir->CreateMalloc(IR_CONSTANT_INT, explicit_index->name(),
		 size_repr[0]->clone()));
		 */
		std::vector<CG_outputRepr *> size_repr3;
		int size = 1;
		std::vector<CG_outputRepr *> sizes;
		if (max_allocate && level > 1)
			max_size2 = max_size->clone();
		for (int i = 0.; i < loops_for_non_zero_block_count.size(); i++) {
			// Relation R = extract_upper_bound(i->second, i->second.set_var(i->first));
			Relation R = get_loop_bound(stmt[*it].IS,
					loops_for_non_zero_block_count[i], known);

			if (is_single_loop_iteration(R, loops_for_non_zero_block_count[i],
					known)) {
				inner_loop_bounds.push_back(std::pair<int, int>(0, 0));
				size *= 1;
				sizes.push_back(ocg->CreateInt(size));
				continue;

			}

			int lb = -1, ub = -1;
			for (DNF_Iterator di(const_cast<Relation &>(R).query_DNF()); di; di++)
				for (GEQ_Iterator gi((*di)->GEQs()); gi; gi++) {
					if ((*gi).get_coef(R.set_var(loops_for_non_zero_block_count[i]))
							< 0
							&& (*gi).is_const(
									R.set_var(loops_for_non_zero_block_count[i]))) {

						ub = (*gi).get_const();
					} else if ((*gi).get_coef(
							R.set_var(loops_for_non_zero_block_count[i])) > 0
							&& (*gi).is_const(
									R.set_var(loops_for_non_zero_block_count[i]))) {

						lb = (*gi).get_const();
					}
				}

			if (lb < 0 || ub < 0)
				throw loop_error("bounds of inner loops cannot be determined");

			size *= (ub - lb + 1);

			//if (max_allocate) {
			//	max_size2 = ocg->CreateTimes(max_size2, ocg->CreateInt(size));

			//}
			inner_loop_bounds.push_back(std::pair<int, int>(lb, ub));

		}
		sizes.push_back(ocg->CreateInt(size));
		// 9. Cancel loops and initialize mark before computation loop distribute at level kk
		//10. Marked = 0;

		std::set<int> guard_vars;
		std::set<int> unprojected_vars;
		std::set<int> derived_vars;
		for (int it2 = 0; it2 < name_to_pos.size(); it2++)
			guard_vars.insert(name_to_pos[it2].second);

		for (std::map<int, int>::iterator it2 = dont_project.begin();
				it2 != dont_project.end(); it2++)
			unprojected_vars.insert(it2->second);

		for (std::map<int, CG_outputRepr*>::iterator it2 = most_reprs.begin();
				it2 != most_reprs.end(); it2++)
			derived_vars.insert(it2->first);

		bool check_if = true;

		if (guards.size() > 0)
			if (ir->QueryBooleanExpOperation(guards[0]->clone()) == IR_COND_EQ
					&& (int) guard_vars.size() - (int) unprojected_vars.size() - 1
							<= (int) derived_vars.size())
				check_if = false;
		//if(is_single)
		//  stmt[*it].IS = result;
		//CG_outputRepr *assign ;
		/*if(ir->QueryBooleanExpOperation(guards[0]->clone()) == IR_COND_EQ ){

		 check_if = false;

		 if(derived_vars.size() == 0){
		 int min = name_to_pos[0].second;
		 int pos=name_to_pos[0].first;
		 for(int i=1; i < name_to_pos.size(); i++)
		 if (name_to_pos[i].second < min)
		 {
		 min = name_to_pos[i].second;
		 pos = name_to_pos[i].first;
		 }
		 Relation temp_update= copy(update);
		 Relation temp = copy(temp_update);

		 std::vector<Relation> empty;
		 for(int i=0; i < name_to_pos.size(); i++)
		 if(name_to_pos[i].first != pos && dont_project.find(name_to_pos[i].first) == dont_project.end() )
		 temp = replace_set_var_as_Global(
		 temp,
		 name_to_pos[i].first,
		 empty);
		 v		     std::pair<CG_outputRepr *,
		 std::pair<CG_outputRepr *, int> > result =
		 output_assignment(ir->builder(),
		 temp,
		 pos,
		 Relation::True(temp.n_set()),
		 std::vector<
		 std::pair<
		 CG_outputRepr *,
		 int> >(
		 temp.n_set(),
		 std::make_pair(
		 static_cast<CG_outputRepr *>(NULL),
		 0)),
		 uninterpreted_symbols[*it]);

		 if (result.second.first != NULL
		 && result.second.second != INT_MAX) {

		 assign = ir->builder()->CreateAssignment(0,
		 ir->builder()->CreateIdent(
		 temp.set_var(
		 pos)->name()),
		 result.second.first);


		 most_reprs.insert(std::pair<int, CG_outputRepr *>( min ,assign));
		 }

		 }
		 }
		 */

		bool found_outer_level = false;
		int max_level = -1;
		for (std::map<int, CG_outputRepr *>::iterator i = most_reprs.begin();
				i != most_reprs.end(); i++) {
			if (i->first <= level) {
				max_level = i->first;
				found_outer_level = true;
			}
		}

		for (std::map<int, CG_outputRepr *>::iterator it2 = most_reprs.begin();
				it2 != most_reprs.end(); it2++)
			cancelled_loops.insert(it2->first);

		for (int i = 0; i < level_.size(); i++)
			level2.insert(level_[i]);
		for (int i = 1; i <= stmt[*it].loop_level.size(); i++) {

			if (cancelled_loops.find(i) == cancelled_loops.end()) //&& level2.find(i) == level2.end())
				remaining_loops.insert(i);
			if (stmt[*it].loop_level[i - 1].made_dense)
				break;

		}
		std::vector<int> remain;
		std::vector<int> remain_for_offset_index_assign;
		CG_outputRepr *index_for_offset_index2 = NULL;
		for (std::set<int>::iterator i = remaining_loops.begin();
				i != remaining_loops.end(); i++)
			if (*i < level)
				remain_for_offset_index_assign.push_back(*i);

		for (std::set<int>::iterator i = remaining_loops.begin();
				i != remaining_loops.end(); i++) {

			remain.push_back(*i);
			if (stmt[*it].loop_level[*i - 1].made_dense)
				break;

		}
		int count_rem = 1;

		if ((max_level < level && remaining_loops.size() >= 2) && enforce_marked)
			count_rem = 2;
		else
			for (std::set<int>::iterator it2 = remaining_loops.begin();
					it2 != remaining_loops.end(); it2++) {

				if (*it2 >= level)
					break;
				count_rem++;
			}

		Relation new_known = this->known;

		if (is_single) {

			//begin: intersect with known: Must be a separate function
			// Relation new_known;
			if (simplification.n_set() < this->known.n_set()) {
				new_known = omega::Extend_Set(omega::copy(simplification),
						this->known.n_set() - simplification.n_set());
				new_known = omega::Intersection(new_known, this->known);
			} else {

				new_known = omega::Extend_Set(omega::copy(this->known),
						simplification.n_set() - this->known.n_set());
				new_known = omega::Intersection(new_known, simplification);
			}
			//end

		}
		std::pair<Relation, Relation> xform_is = construct_reduced_IS_And_XFORM(
				ir, stmt[*it].IS, stmt[*it].xform, remain, lex, this->known,
				uninterpreted_symbols[*it]);
		std::pair<Relation, Relation> xform_is_2;
		//stmt[*it].IS.print();
		if (most_reprs.find(level - 1) == most_reprs.end())
			xform_is_2 = construct_reduced_IS_And_XFORM(ir, stmt[*it].IS,
					stmt[*it].xform, remain_for_offset_index_assign, lex,
					this->known, uninterpreted_symbols[*it]);

		std::vector<CG_outputRepr *> list_type2;
		std::vector<IR_PointerSymbol *> temp2;

		std::vector<IR_Symbol *> new_array_prime3;
		//for (int k = 0; k < data_array.size(); k++) {

		CG_outputRepr *temp_col_type;

		if (level_.size() > 1) {
			if (max_col_size > 65536)
				temp_col_type = ir->CreateArrayType(IR_CONSTANT_INT,
						ocg->CreateInt(level_.size()));
			else
				temp_col_type = ir->CreateArrayType(IR_CONSTANT_SHORT,
						ocg->CreateInt(level_.size()));
		} else {
			if (max_col_size > 65536)
				temp_col_type = ir->CreateScalarType(IR_CONSTANT_INT);
			else
				temp_col_type = ir->CreateScalarType(IR_CONSTANT_SHORT);

		}
		std::vector<std::string> class_data;
		std::vector<CG_outputRepr *> class_data_types;

		//for(int i=0; i < level.size(); i++){
		class_data.push_back("col_");

		class_data_types.push_back(temp_col_type);
		IR_CONSTANT_TYPE type;
		for (int k = 0; k < data_array.size(); k++) {

			//IR_CONSTANT_TYPE type;
			type = IR_CONSTANT_UNKNOWN;
			std::vector<IR_PointerArrayRef *> refs = ir->FindPointerArrayRef(
					stmt[*it].code);

			IR_PointerArrayRef* data_arr_ref;
			bool is_array_ref = false;
			for (int i = 0; i < refs.size(); i++)
				if (refs[i]->name() == data_array[k]) {
					type = refs[i]->symbol()->elem_type();
					data_arr_ref = refs[i];

				}

			if (type == IR_CONSTANT_UNKNOWN) {
				std::vector<IR_ArrayRef *> refs = ir->FindArrayRef(stmt[*it].code);

				IR_ArrayRef* data_arr_ref;

				for (int i = 0; i < refs.size(); i++)
					if (refs[i]->name() == data_array[k]) {
						type = refs[i]->symbol()->elem_type();
						data_arr_ref = refs[i];
						is_array_ref = true;
					}

			}
			assert(type != IR_CONSTANT_UNKNOWN);
			CG_outputRepr *temp_arr_data_type;
			if (size != 1)
				temp_arr_data_type = ir->CreateArrayType(type, sizes);
			else

				temp_arr_data_type = ir->CreateScalarType(type);

			class_data.push_back(data_array[k]);
			class_data_types.push_back(temp_arr_data_type);
			size_repr3.push_back(ir->CreateArrayType(type, sizes));

		}
		IR_PointerSymbol *new_array_prime;

		IR_Symbol *new_array_prime2;

		CG_outputRepr *list_type = NULL;
		if (!max_allocate)
			list_type = static_cast<CG_roseBuilder *>(ocg)->CreateLinkedListStruct(
					"a_list", class_data, class_data_types);
		list_type2.push_back(list_type);

		//ocg->CreateTimes(count->clone(), ocg->CreateInt(size)));
		std::vector<CG_outputRepr *> to_push;
		to_push.push_back(ocg->CreateInt(0));
		std::vector<IR_CONSTANT_TYPE> types;
		for (int k = 0; k < data_array.size(); k++) {

			std::vector<IR_PointerArrayRef *> refs = ir->FindPointerArrayRef(
					stmt[*it].code);
			IR_CONSTANT_TYPE type;
			IR_PointerArrayRef* data_arr_ref;
			for (int i = 0; i < refs.size(); i++)
				if (refs[i]->name() == data_array[k]) {
					type = refs[i]->symbol()->elem_type();
					data_arr_ref = refs[i];

				}

			types.push_back(type);
			assert(type != IR_CONSTANT_UNKNOWN);
			if (most_reprs.find(level - 1) == most_reprs.end()) {
				if (!max_allocate) {
					new_array_prime2 = ir->CreatePointerSymbol(list_type, to_push);
					new_array_prime3.push_back(new_array_prime2);
				}
			} else {

				int size_ = 1;

				for (int i = 1; i < level_[0]; i++) {
					// Relation R = extract_upper_bound(i->second, i->second.set_var(i->first));
					int lb = -1, ub = -1;
					Relation R = get_loop_bound(stmt[*it].IS, i, this->known);
					for (DNF_Iterator di(const_cast<Relation &>(R).query_DNF()); di;
							di++)
						for (GEQ_Iterator gi((*di)->GEQs()); gi; gi++) {
							if ((*gi).get_coef(R.set_var(i)) < 0
									&& (*gi).is_const(R.set_var(i))) {

								ub = (*gi).get_const();
							} else if ((*gi).get_coef(R.set_var(i)) > 0
									&& (*gi).is_const(R.set_var(i))) {

								lb = (*gi).get_const();
							}
						}

					if (lb < 0 || ub < 0)
						throw loop_error(
								"bounds of inner loops cannot be determined");

					size_ *= (ub - lb + 1);
				}
				std::vector<CG_outputRepr *> size_repr5;
				size_repr5.push_back(ocg->CreateInt(size_));
				if (!max_allocate) {
					new_array_prime2 = ir->CreateArraySymbol(
							ir->CreatePointerType(list_type->clone()), size_repr5);
					new_array_prime3.push_back(new_array_prime2);
				}
			}

			/*

			 std::vector<IR_ArrayRef *> refs = ir->FindArrayRef(stmt[stmt_num].code);
			 IR_CONSTANT_TYPE type;
			 IR_ArrayRef* data_arr_ref;
			 for (int i = 0; i < refs.size(); i++)
			 if (refs[i]->name() == data_array) {
			 type = refs[i]->symbol()->elem_type();
			 data_arr_ref = refs[i];
			 }
			 size_repr3.push_back(
			 ocg->CreateTimes(count->clone(), ocg->CreateInt(size)));
			 new_array_prime = ir->CreatePointerSymbol(type, size_repr3, new_array);
			 ptr_variables.push_back(static_cast<IR_PointerSymbol *>(new_array_prime));
			 new_array_prime->set_size(0, size_repr3[0]->clone());
			 init_code = ocg->StmtListAppend(init_code,
			 ir->CreateMalloc(type, new_array_prime->name(),
			 size_repr3[0]->clone()));



			 */
			//AV: Linearize array change this
			/*new_array_prime = ir->CreatePointerSymbol(size_repr3[k]->clone(),
			 new_array[k]);
			 */
			std::vector<CG_outputRepr *> dumb;
			dumb.push_back(ocg->CreateInt(0));
			new_array_prime = ir->CreatePointerSymbol(type, dumb, new_array[k]);

			//AV: removing below. array dimension is always 1
			//static_cast<IR_rosePointerSymbol *>(new_array_prime)->set_dim(
			//		1 + sizes.size());
			CG_outputRepr *new_size;
			if (max_allocate) {
				//AV: 2016 linearizing and removing below
				//for (int i = 0; i < sizes.size(); i++)
				//	new_array_prime->set_size(i + 1, sizes[i]->clone());
				/*static_cast<IR_rosePointerSymbol *>(new_array_prime)->set_dim(
				 1 + sizes.size());
				 new_array_prime->set_size(0, max_size2->clone());
				 for (int i = 0; i < sizes.size(); i++)
				 new_array_prime->set_size(i + 1, sizes[i]->clone());
				 */
				static_cast<IR_rosePointerSymbol *>(new_array_prime)->set_dim(1);
				new_size = max_size->clone();
				for (int i = 0; i < sizes.size(); i++)
					new_size = ocg->CreateTimes(new_size, sizes[i]->clone());

				new_array_prime->set_size(0, new_size);
				/*static_cast<IR_rosePointerSymbol *>(new_array_prime)->set_dim(
				 1 + sizes.size());
				 new_array_prime->set_size(0, max_size2->clone());
				 for (int i = 0; i < sizes.size(); i++)
				 new_array_prime->set_size(i + 1, sizes[i]->clone());

				 */

			} else {
				static_cast<IR_rosePointerSymbol *>(new_array_prime)->set_dim(1);
				CG_outputRepr *new_size = count_2->clone();
				for (int i = 0; i < sizes.size(); i++)
					new_size = ocg->CreateTimes(new_size, sizes[i]->clone());

				new_array_prime->set_size(0, new_size);
				//AV: linearizing and removing below
				//for (int i = 0; i < sizes.size(); i++)
				//	new_array_prime->set_size(i + 1, sizes[i]->clone());
			}

			ptr_variables.push_back(
					static_cast<IR_PointerSymbol *>(new_array_prime));

			if (max_allocate) {
				new_array_prime3.push_back(new_array_prime);
				init_code = ocg->StmtListAppend(init_code,
						ir->CreateMalloc(type, new_array_prime->name(),
								new_size->clone()));
			} else
				;			//init_code = ocg->StmtListAppend(init_code,
							//ir->CreateMalloc(type, new_array_prime->name(),
							//					size_repr3[0]->clone()));
			if (most_reprs.find(level - 1) == most_reprs.end())
				if (!max_allocate)
					init_code = ocg->StmtListAppend(init_code,
							ocg->CreateAssignment(0,
									ocg->CreateIdent(new_array_prime2->name()),
									ocg->CreateInt(0)));
			std::vector<CG_outputRepr *> size_repr4;
			size = 1;

			// Relation R = extract_upper_bound(i->second, i->second.set_var(i->first));
			int lb = -1, ub = -1;

			Relation R;

			if (max_level > 0) {
				if (max_level == level)
					R = get_loop_bound(stmt[*it].IS, level, this->known);
				else
					R = get_loop_bound(stmt[*it].IS, max_level, this->known);
				for (DNF_Iterator di(const_cast<Relation &>(R).query_DNF()); di;
						di++)
					for (GEQ_Iterator gi((*di)->GEQs()); gi; gi++) {

						if (max_level == level) {
							if ((*gi).get_coef(R.set_var(level)) < 0
									&& (*gi).is_const(R.set_var(level))) {

								ub = (*gi).get_const();
							} else if ((*gi).get_coef(R.set_var(level)) > 0
									&& (*gi).is_const(R.set_var(level))) {

								lb = (*gi).get_const();
							}
						} else {
							if ((*gi).get_coef(R.set_var(max_level)) < 0
									&& (*gi).is_const(R.set_var(max_level))) {

								ub = (*gi).get_const();
							} else if ((*gi).get_coef(R.set_var(max_level)) > 0
									&& (*gi).is_const(R.set_var(max_level))) {

								lb = (*gi).get_const();
							}

						}
					}

				if (lb < 0 || ub < 0)
					;//Anand: needs modification hack:://size = 1;//throw loop_error("bounds of inner loops cannot be determined");
				else
					size *= (ub - lb + 1);

			}
		}
		IR_PointerSymbol *marked2;
		CG_outputRepr *mk_type;

		std::vector<CG_outputRepr *> size_repr4;
		if (!max_allocate) {

			std::vector<std::string> mk_data;
			std::vector<CG_outputRepr *> mk_data_types;

			mk_data.push_back("ptr");
			mk_data_types.push_back(ir->CreatePointerType(list_type));

			mk_type = static_cast<CG_roseBuilder *>(ocg)->CreateClass("mk",
					mk_data, mk_data_types);
			//mk_type = ir->CreatePointerType(mk_type);
			std::vector<CG_outputRepr *> size_repr5;
			size_repr5.push_back(ocg->CreateInt(size));
			marked2 = ir->CreatePointerSymbol(mk_type, size_repr5);
			//AV: 2016 Changing below
			//if ((enforce_marked && max_level != level) ||(enforce_marked && max_level == 1)) {
			if (enforce_marked) {
				init_code = ocg->StmtListAppend(init_code,
						ir->CreateMalloc(mk_type->clone(), marked2->name(),
								size_repr5[0]->clone()));
				marked2->set_size(0, size_repr5[0]->clone());
			}
			marked3.push_back(marked2);

		} else {

			size_repr4.push_back(ocg->CreateInt(size));
			marked2 = ir->CreatePointerSymbol(IR_CONSTANT_INT, size_repr4);
			//ptr_variables.push_back(static_cast<IR_PointerSymbol *>(marked2));
			init_code = ocg->StmtListAppend(init_code,
					ir->CreateMalloc(IR_CONSTANT_INT, marked2->name(),
							size_repr4[0]->clone()));
			marked2->set_size(0, size_repr4[0]->clone());
			marked3.push_back(marked2);

		}

		/*if(!max_allocate)
		 init_code = ocg->StmtListAppend(init_code,
		 ir->CreateMalloc(mk_type, marked2->name(),
		 ocg->CreateInt(size)));
		 else
		 init_code = ocg->StmtListAppend(init_code,
		 ir->CreateMalloc(IR_CONSTANT_INT, marked2->name(),
		 size_repr4[0]->clone()));
		 */
		// 8. Insert count = 0 and offset_index[0] = 0 above loop. throw error if level != 2
//	CG_outputRepr *count_2 = ocg->CreateIdent("chill_count_1");
		CG_outputRepr *count_init = ocg->CreateAssignment(0, count_2->clone(),
				ocg->CreateInt(0));

		CG_outputRepr *offset_init = NULL;

		if (level > 1)
			if (!is_single && enforce_marked)
				offset_init = ocg->CreateAssignment(0,
						ocg->CreateArrayRefExpression(offset_index2->name(),
								ocg->CreateInt(0)), ocg->CreateInt(0));

		//if (level == 2) {

		init_code = ocg->StmtListAppend(init_code, count_init);

		if (level > 1) {
			if (!is_single && enforce_marked)
				init_code = ocg->StmtListAppend(init_code, offset_init);

			//CreateLoop that initializes offset and new_array to NULL;
			if (enforce_marked) {

				std::vector<int> outer;

				for (int i = 1; i < level; i++)
					outer.push_back(i);
				init_is = construct_reduced_IS_And_XFORM(ir, stmt[*it].IS,
						stmt[*it].xform, outer, lex,

						this->known, uninterpreted_symbols[*it]);

				if (max_level == level - 1 && max_level == 1) {

					Relation IS = init_is.first;
					std::vector<int> loop_bounds;

					for (int i = 1; i <= outer.size(); i++)
						loop_bounds.push_back(
								extract_loop_trip_count(IS, IS.set_var(i)));

					CG_outputRepr *index2 = NULL;
					for (int i = 1; i <= outer.size(); i++) {
						int size = 1;
						CG_outputRepr *index = ocg->CreateIdent(
								IS.set_var(i)->name());
						for (int j = i + 1; j <= outer.size(); j++)
							size *= loop_bounds[j - 1];

						index2 = ocg->CreatePlus(index2,
								ocg->CreateTimes(ocg->CreateInt(size), index));

					}
					index_for_offset_index2 = index2->clone();
					CG_outputRepr * body = NULL;
					for (int k = 0; k < data_array.size(); k++)
						body = ocg->StmtListAppend(body,
								ocg->CreateAssignment(0,
										ocg->CreateArrayRefExpression(
												new_array_prime3[k]->name(),
												index2->clone()), ocg->CreateInt(0)));

					body = ocg->StmtListAppend(body,
							ocg->CreateAssignment(0,
									ocg->CreateArrayRefExpression(offset_index2->name(),
											ocg->CreatePlus(index2->clone(),
													ocg->CreateInt(1))), ocg->CreateInt(0)));

					for (int i = outer.size(); i >= 1; i--) {
						CG_outputRepr *loop = output_loop(ir->builder(), IS, i,
								this->known,
								std::vector<std::pair<CG_outputRepr *, int> >(
										IS.n_set(),
										std::make_pair(static_cast<CG_outputRepr *>(NULL),
												0)), uninterpreted_symbols[*it]);

						body = ir->builder()->CreateLoop(0, loop, body);

					}
					init_code = ocg->StmtListAppend(init_code, body);

				}

			}
			if (!enforce_marked) {

				std::vector<int> outer;

				for (int i = 1; i < level; i++)
					outer.push_back(i);

				init_is = construct_reduced_IS_And_XFORM(ir, stmt[*it].IS,
						stmt[*it].xform, outer, lex,

						this->known, uninterpreted_symbols[*it]);

				//std::cout<<"Printing offset loop"<<std::endl;

				//init_is.first.print();

				//construct_reduced_IS_And_XFORM(ir, stmt[*it].IS,
				//stmt[*it].xform, derived_loops, lex, this->known,
				//uninterpreted_symbols[*it]);

				Relation IS = init_is.first;
				std::vector<int> loop_bounds;

				for (int i = 1; i <= outer.size(); i++)
					loop_bounds.push_back(
							extract_loop_trip_count(IS, IS.set_var(i)));

				CG_outputRepr *index2 = NULL;
				for (int i = 1; i <= outer.size(); i++) {
					int size = 1;
					CG_outputRepr *index = ocg->CreateIdent(IS.set_var(i)->name());
					for (int j = i + 1; j <= outer.size(); j++)
						size *= loop_bounds[j - 1];

					index2 = ocg->CreatePlus(index2,
							ocg->CreateTimes(ocg->CreateInt(size), index));

				}
				index_for_offset_index2 = index2->clone();
				CG_outputRepr * body = NULL;
				for (int k = 0; k < data_array.size(); k++)
					body = ocg->StmtListAppend(body,
							ocg->CreateAssignment(0,
									ocg->CreateArrayRefExpression(
											new_array_prime3[k]->name(), index2->clone()),
									ocg->CreateInt(0)));

				body = ocg->StmtListAppend(body,
						ocg->CreateAssignment(0,
								ocg->CreateArrayRefExpression(offset_index2->name(),
										ocg->CreatePlus(index2->clone(),
												ocg->CreateInt(1))), ocg->CreateInt(0)));

				for (int i = outer.size(); i >= 1; i--) {
					CG_outputRepr *loop = output_loop(ir->builder(), IS, i,
							this->known,
							std::vector<std::pair<CG_outputRepr *, int> >(IS.n_set(),
									std::make_pair(static_cast<CG_outputRepr *>(NULL),
											0)), uninterpreted_symbols[*it]);

					body = ir->builder()->CreateLoop(0, loop, body);

				}
				init_code = ocg->StmtListAppend(init_code, body);

			}
		}
		if (level > 2 && enforce_marked) {

			std::vector<int> outer_loops;

			for (int i = 1; i < level; i++)
				outer_loops.push_back(i);

			std::pair<Relation, Relation> IS_and_XFORM =
					construct_reduced_IS_And_XFORM(ir, stmt[*it].IS, stmt[*it].xform,
							outer_loops, lex, this->known, uninterpreted_symbols[*it]);

			Statement s = stmt[*it];
			s.IS = IS_and_XFORM.first;
			s.xform = IS_and_XFORM.second;
			s.has_inspector = false;
			s.ir_stmt_node = NULL;
			s.reduction = 0;

			CG_outputRepr *new_code = ocg->StmtListAppend(count_init, offset_init);
			//delete s.code;
			s.code = new_code;
			std::vector<LoopLevel> ll;

			for (int i = 0; i < outer_loops.size(); i++)
				ll.push_back(s.loop_level[i]);

			s.loop_level = ll;

			stmt.push_back(s);
			uninterpreted_symbols.push_back(uninterpreted_symbols[*it]);
			uninterpreted_symbols_stringrepr.push_back(
					uninterpreted_symbols_stringrepr[*it]);

			unin_rel.push_back(unin_rel[*it]);
			clean_unin_relation(s.IS, unin_rel[unin_rel.size() - 1]);
			dep.insert();
			//shiftLexicalOrder(lex, 0, 1);
		}

		if (found_outer_level) {

			CG_outputRepr * level_repr = most_reprs.find(max_level)->second;
			mark_code = ocg->StmtListAppend(mark_code, level_repr->clone());

			CG_outputRepr *mark_assign2;
			if (!max_allocate)
				mark_assign2 = ocg->CreateAssignment(0,
						static_cast<CG_roseBuilder *>(ocg)->CreateDotExpression(
								ocg->CreateArrayRefExpression(marked2->name(),
										ocg->CreateIdent(
												stmt[*it].IS.set_var(max_level)->name())),
								static_cast<CG_roseBuilder *>(ocg)->lookup_member_data(
										mk_type, "ptr",
										ocg->CreateIdent(marked2->name()))),
						ocg->CreateInt(0));
			else
				mark_assign2 = ocg->CreateAssignment(0,
						ocg->CreateArrayRefExpression(marked2->name(),
								ocg->CreateIdent(
										stmt[*it].IS.set_var(max_level)->name())),
						ocg->CreateInt(-1));

			/*	CG_outputRepr *mark_assign = ocg->CreateAssignment(0,
			 ocg->CreateArrayRefExpression(marked2->name(),
			 ocg->CreateIdent(
			 stmt[stmt_num].IS.set_var(level)->name())),
			 ocg->CreateInt(-1));
			 new_code = ocg->StmtListAppend(mark_code->clone(), mark_assign);
			 */
			new_code2 = ocg->StmtListAppend(mark_code->clone(), mark_assign2);
			//new_code = mark_code->clone();
		} else {

			for (std::map<int, CG_outputRepr *>::iterator i = most_reprs.begin();
					i != most_reprs.end(); i++)
				mark_code = ocg->StmtListAppend(mark_code, i->second->clone());
			CG_outputRepr *cond = NULL;
			if (guards.size() > 0) {
				cond = dynamic_cast<IR_If*>(code)->condition();
				assert(cond != NULL);
			}
			/*	CG_outputRepr *mark_assign = ocg->CreateAssignment(0,
			 ocg->CreateArrayRefExpression(marked2->name(),
			 ocg->CreateIdent(
			 stmt[stmt_num].IS.set_var(level)->name())),
			 ocg->CreateInt(-1));
			 */

			int level_ = level;
			for (int i = 0; i < remain.size(); i++)
				if (remain[i] == level)
					level_ = i + 1;

			CG_outputRepr *mark_assign2;
			if (!max_allocate) {
				if (level == max_level)
					mark_assign2 =
							ocg->CreateAssignment(0,
									static_cast<CG_roseBuilder *>(ocg)->CreateDotExpression(
											ocg->CreateArrayRefExpression(marked2->name(),
													ocg->CreateIdent(
															xform_is.first.set_var(level_)->name())),
											static_cast<CG_roseBuilder *>(ocg)->lookup_member_data(
													mk_type, "ptr",
													ocg->CreateIdent(marked2->name()))),
									ocg->CreateInt(0));
				else
					mark_assign2 =
							ocg->CreateAssignment(0,
									static_cast<CG_roseBuilder *>(ocg)->CreateDotExpression(
											ocg->CreateArrayRefExpression(marked2->name(),
													ocg->CreateIdent(
															xform_is.first.set_var(max_level)->name())),
											static_cast<CG_roseBuilder *>(ocg)->lookup_member_data(
													mk_type, "ptr",
													ocg->CreateIdent(marked2->name()))),
									ocg->CreateInt(0));
			} else

				mark_assign2 = static_cast<CG_roseBuilder *>(ocg)->CreateAssignment(
						0,
						ocg->CreateArrayRefExpression(marked2->name(),
								ocg->CreateIdent(
										xform_is.first.set_var(level_)->name())),
						ocg->CreateInt(-1));
			//CG_outputRepr * new_if = ocg->CreateIf(0, cond->clone(), mark_assign,
			//NULL);
			CG_outputRepr * new_if2;
			if (guards.size() > 0)
				new_if2 = ocg->CreateIf(0, cond->clone(), mark_assign2,
				NULL);
			else
				new_if2 = mark_assign2->clone();
			if (mark_code != NULL && new_if2 != NULL) {
				//new_code = ocg->StmtListAppend(mark_code->clone(), new_if->clone());
				new_code2 = ocg->StmtListAppend(new_code2,
						ocg->StmtListAppend(mark_code->clone(), new_if2->clone()));
			} else if (new_if2 != NULL) {
				//new_code = new_if->clone();
				new_code2 = ocg->StmtListAppend(new_code2, new_if2->clone());
			}
		}

		//11. Insert computation loop after cancellation, cancel guard if possible otherwise throw error
		//12. If marked == 0 increment count, initialize marked[loop_index] to count, set offset_kk[kk] = kk,(use cancelled loops)
		//    initialize inner data footprint(using set of inner loops) to zero.
		//13. Copy value from data_array into new_array.
		CG_outputRepr *marked_check, *explicit_offset_assign,
				*explicit_offset_assign2, *data_init = NULL, *data_init2 =
				NULL, *data_assign, *marked_assign, *count_inc, *count_inc2,
				*marked_check2 = NULL;

		/*	marked_check = ocg->CreateEQ(
		 ocg->CreateArrayRefExpression(marked->name(),
		 ocg->CreateIdent(stmt[stmt_num].IS.set_var(level)->name())),
		 ocg->CreateInt(-1));
		 */
		if (!max_allocate)
			marked_check2 = ocg->CreateEQ(
					static_cast<CG_roseBuilder *>(ocg)->CreateDotExpression(
							ocg->CreateArrayRefExpression(marked2->name(),
									ocg->CreateIdent(
											stmt[*it].IS.set_var(max_level)->name())),
							static_cast<CG_roseBuilder *>(ocg)->lookup_member_data(
									mk_type, "ptr", ocg->CreateIdent(marked2->name()))),
					ocg->CreateInt(0));
		else if (max_level > 0)
			marked_check2 = ocg->CreateEQ(

					ocg->CreateArrayRefExpression(marked2->name(),
							ocg->CreateIdent(stmt[*it].IS.set_var(max_level)->name())),
					ocg->CreateInt(-1));
		else if (is_single) {

			marked_check2 = ocg->CreateEQ(

			ocg->CreateArrayRefExpression(marked2->name(), ocg->CreateInt(0)),
					ocg->CreateInt(-1));

		}
		std::vector<IR_ScalarSymbol *> loop_iters;
		int level_coef = 1;
		for (int i = 0; i < inner_loop_bounds.size(); i++) {
			loop_iters.push_back(ir->CreateScalarSymbol(IR_CONSTANT_INT, 0));
			level_coef *= inner_loop_bounds[i].second - inner_loop_bounds[i].first
					+ 1;
		}

		/*temp = (float_list*)malloc(sizeof(float_list));
		 temp->next = a_list;
		 a_list = temp;
		 mk[kk].ptr = a_list;
		 for (i_ = 0; i_ < R; i_++)
		 for (k_ = 0; k_ < C; k_++)
		 mk[kk].ptr->data[i_ * C + k_] = 0;
		 mk[kk].ptr->col = kk;
		 count++;
		 */

		std::vector<CG_outputRepr *> size__;
		size__.push_back(ocg->CreateInt(1));
		std::vector<CG_outputRepr*> data_array_ref_2;
		CG_outputRepr *allocate = NULL;
		if (!max_allocate) {
			IR_PointerSymbol *temp = ir->CreatePointerSymbol(list_type, size__);
			temp2.push_back(temp);
			allocate = ir->CreateMalloc(list_type, temp->name(),
					ocg->CreateInt(1));

			if (most_reprs.find(level - 1) == most_reprs.end()) {

				allocate =
						ocg->StmtListAppend(allocate,
								ocg->CreateAssignment(0,
										static_cast<CG_roseBuilder *>(ocg)->CreateArrowRefExpression(
												ocg->CreateIdent(temp->name()),
												static_cast<CG_roseBuilder *>(ocg)->lookup_member_data(
														list_type, "next",
														ocg->CreateIdent(temp->name()))),
										ocg->CreateIdent(new_array_prime2->name())));

				allocate = ocg->StmtListAppend(allocate,
						ocg->CreateAssignment(0,
								ocg->CreateIdent(new_array_prime2->name()),
								ocg->CreateIdent(temp->name())));

				if (enforce_marked) {
					allocate =
							ocg->StmtListAppend(allocate,
									ocg->CreateAssignment(0,
											static_cast<CG_roseBuilder *>(ocg)->CreateDotExpression(
													ocg->CreateArrayRefExpression(
															marked2->name(),
															ocg->CreateIdent(
																	stmt[*it].IS.set_var(
																			max_level)->name())),
													static_cast<CG_roseBuilder *>(ocg)->lookup_member_data(
															mk_type, "ptr",
															ocg->CreateIdent(
																	marked2->name()))),
											ocg->CreateIdent(new_array_prime2->name())));
					for (int k = 0; k < data_array.size(); k++)
						data_array_ref_2.push_back(
								static_cast<CG_roseBuilder *>(ocg)->CreateArrowRefExpression(
										static_cast<CG_roseBuilder *>(ocg)->CreateDotExpression(
												ocg->CreateArrayRefExpression(
														marked2->name(),
														ocg->CreateIdent(
																stmt[*it].IS.set_var(max_level)->name())),
												static_cast<CG_roseBuilder *>(ocg)->lookup_member_data(
														mk_type, "ptr",
														ocg->CreateIdent(marked2->name()))),
										static_cast<CG_roseBuilder *>(ocg)->lookup_member_data(
												list_type, data_array[k],
												ocg->CreateIdent(
														new_array_prime2->name()))));
				} else {
					for (int k = 0; k < data_array.size(); k++)
						data_array_ref_2.push_back(
								static_cast<CG_roseBuilder *>(ocg)->CreateArrowRefExpression(
										//static_cast<CG_roseBuilder *>(ocg)->CreateDotExpression(
										ocg->CreateArrayRefExpression(
												new_array_prime2->name(),
												index_for_offset_index2->clone()),
										/*static_cast<CG_roseBuilder *>(ocg)->lookup_member_data(
										 mk_type, "ptr",
										 ocg->CreateIdent(
										 marked2->name()))),*/
										static_cast<CG_roseBuilder *>(ocg)->lookup_member_data(
												list_type, data_array[k],
												ocg->CreateIdent(
														new_array_prime2->name()))));

				}
			} else {

				if (enforce_marked) {

					allocate =
							ocg->StmtListAppend(allocate,
									ocg->CreateAssignment(0,
											static_cast<CG_roseBuilder *>(ocg)->CreateArrowRefExpression(
													ocg->CreateIdent(temp->name()),
													static_cast<CG_roseBuilder *>(ocg)->lookup_member_data(
															list_type, "next",
															ocg->CreateIdent(temp->name()))),
											ocg->CreateArrayRefExpression(
													new_array_prime2->name(),
													ocg->CreateIdent(
															stmt[*it].IS.set_var(level - 1)->name()))));

					allocate =
							ocg->StmtListAppend(allocate,
									ocg->CreateAssignment(0,
											ocg->CreateArrayRefExpression(
													new_array_prime2->name(),
													ocg->CreateIdent(
															stmt[*it].IS.set_var(level - 1)->name())),
											ocg->CreateIdent(temp->name())));
					if (max_level == level)
						allocate =
								ocg->StmtListAppend(allocate,
										ocg->CreateAssignment(0,
												static_cast<CG_roseBuilder *>(ocg)->CreateDotExpression(
														ocg->CreateArrayRefExpression(
																marked2->name(),
																ocg->CreateIdent(
																		stmt[*it].IS.set_var(
																				level)->name())),
														static_cast<CG_roseBuilder *>(ocg)->lookup_member_data(
																mk_type, "ptr",
																ocg->CreateIdent(
																		marked2->name()))),
												ocg->CreateArrayRefExpression(
														new_array_prime2->name(),
														ocg->CreateIdent(
																stmt[*it].IS.set_var(level - 1)->name()))));
					else
						allocate =
								ocg->StmtListAppend(allocate,
										ocg->CreateAssignment(0,
												static_cast<CG_roseBuilder *>(ocg)->CreateDotExpression(
														ocg->CreateArrayRefExpression(
																marked2->name(),
																ocg->CreateIdent(
																		stmt[*it].IS.set_var(
																				max_level)->name())),
														static_cast<CG_roseBuilder *>(ocg)->lookup_member_data(
																mk_type, "ptr",
																ocg->CreateIdent(
																		marked2->name()))),
												ocg->CreateArrayRefExpression(
														new_array_prime2->name(),
														ocg->CreateIdent(
																stmt[*it].IS.set_var(level - 1)->name()))));

					if (max_level == level) {
						for (int k = 0; k < data_array.size(); k++)
							data_array_ref_2.push_back(
									static_cast<CG_roseBuilder *>(ocg)->CreateArrowRefExpression(
											static_cast<CG_roseBuilder *>(ocg)->CreateDotExpression(
													ocg->CreateArrayRefExpression(
															marked2->name(),
															ocg->CreateIdent(
																	stmt[*it].IS.set_var(level)->name())),
													static_cast<CG_roseBuilder *>(ocg)->lookup_member_data(
															mk_type, "ptr",
															ocg->CreateIdent(
																	marked2->name()))),
											static_cast<CG_roseBuilder *>(ocg)->lookup_member_data(
													list_type, data_array[k],
													ocg->CreateArrayRefExpression(
															new_array_prime2->name(),
															ocg->CreateIdent(
																	stmt[*it].IS.set_var(
																			level - 1)->name())))));
					} else {
						for (int k = 0; k < data_array.size(); k++)
							data_array_ref_2.push_back(
									static_cast<CG_roseBuilder *>(ocg)->CreateArrowRefExpression(
											static_cast<CG_roseBuilder *>(ocg)->CreateDotExpression(
													ocg->CreateArrayRefExpression(
															marked2->name(),
															ocg->CreateIdent(
																	stmt[*it].IS.set_var(
																			max_level)->name())),
													static_cast<CG_roseBuilder *>(ocg)->lookup_member_data(
															mk_type, "ptr",
															ocg->CreateIdent(
																	marked2->name()))),
											static_cast<CG_roseBuilder *>(ocg)->lookup_member_data(
													list_type, data_array[k],
													ocg->CreateArrayRefExpression(
															new_array_prime2->name(),
															ocg->CreateIdent(
																	stmt[*it].IS.set_var(
																			level - 1)->name())))));

					}

				} else {

					allocate =
							ocg->StmtListAppend(allocate,
									ocg->CreateAssignment(0,
											static_cast<CG_roseBuilder *>(ocg)->CreateArrowRefExpression(
													ocg->CreateIdent(temp->name()),
													static_cast<CG_roseBuilder *>(ocg)->lookup_member_data(
															list_type, "next",
															ocg->CreateIdent(temp->name()))),
											ocg->CreateArrayRefExpression(
													new_array_prime2->name(),
													index_for_offset_index2->clone())));

					allocate = ocg->StmtListAppend(allocate,
							ocg->CreateAssignment(0,
									ocg->CreateArrayRefExpression(
											new_array_prime2->name(),
											index_for_offset_index2->clone()),
									ocg->CreateIdent(temp->name())));

					for (int k = 0; k < data_array.size(); k++)
						data_array_ref_2.push_back(
								static_cast<CG_roseBuilder *>(ocg)->CreateArrowRefExpression(
								//		static_cast<CG_roseBuilder *>(ocg)->CreateDotExpression(
										ocg->CreateArrayRefExpression(
												new_array_prime2->name(),
												index_for_offset_index2->clone()),
										//static_cast<CG_roseBuilder *>(ocg)->lookup_member_data(
										//		mk_type, "ptr",
										//		ocg->CreateIdent(
										//				marked2->name()))),
										static_cast<CG_roseBuilder *>(ocg)->lookup_member_data(
												list_type, data_array[k],
												ocg->CreateArrayRefExpression(
														new_array_prime2->name(),
														index_for_offset_index2->clone()))));
				}
			}
		} else {
			if (most_reprs.find(level - 1) == most_reprs.end()) {
				if (!is_single) {
					for (int k = 0; k < data_array.size(); k++)
						data_array_ref_2.push_back(
						//ocg->CreateArrayRefExpression(new_array_prime3[k]->name(),
								ocg->CreateArrayRefExpression(marked2->name(),
										ocg->CreateIdent(
												stmt[*it].IS.set_var(level)->name())));//);
				} else {

					for (int k = 0; k < data_array.size(); k++) {
						CG_outputRepr *repr = ocg->CreateArrayRefExpression(
								marked2->name(),
								ocg->CreateIdent(stmt[*it].IS.set_var(level)->name()));
						data_array_ref_2.push_back(repr);
					}
				}
			} else {

				for (int k = 0; k < data_array.size(); k++)
					data_array_ref_2.push_back(
					//ocg->CreateArrayRefExpression(new_array_prime3[k]->name(),
							ocg->CreateArrayRefExpression(marked2->name(),
									ocg->CreateIdent(
											stmt[*it].IS.set_var(level - 1)->name())));//);

			}

		}

		/*CG_outputRepr *data_array_ref = ocg->CreateTimes(
		 ocg->CreateArrayRefExpression(marked->name(),
		 ocg->CreateIdent(stmt[stmt_num].IS.set_var(level)->name())),
		 ocg->CreateInt(level_coef));
		 */

		std::vector<CG_outputRepr *> temp4;
		int coef_ = 1;
		CG_outputRepr *ref_data = NULL;
		for (int i = 0; i < inner_loop_bounds.size(); i++) {
			CG_outputRepr *current = ocg->CreateIdent(loop_iters[i]->name());
			int level_coef = 1;
			coef_ *= inner_loop_bounds[i].second - inner_loop_bounds[i].first + 1;
			for (int j = i + 1; j < inner_loop_bounds.size(); j++)
				level_coef *= inner_loop_bounds[j].second
						- inner_loop_bounds[j].first + 1;

			current = ocg->CreateTimes(ocg->CreateInt(level_coef), current);

			//data_array_ref = ocg->CreatePlus(data_array_ref, current->clone());
			ref_data = ocg->CreatePlus(ref_data, current->clone());

		}
		if (ref_data != NULL)
			temp4.push_back(ref_data->clone());

		std::vector<CG_outputRepr *> data_array_ref_2_cpy;

		for (int k = 0; k < data_array.size(); k++) {

			data_array_ref_2_cpy.push_back(data_array_ref_2[k]->clone());

			if (!max_allocate) {
				if (temp4.size() > 0) {
					for (int x = 0; x < temp4.size(); x++)
						data_array_ref_2[k] = ocg->CreateArrayRefExpression(
								data_array_ref_2[k], temp4[x]);
				} else
					data_array_ref_2[k] = //ocg->CreateArrayRefExpression(
							data_array_ref_2[k]->clone(); //, ocg->CreateInt(0));

				data_init2 = ocg->StmtListAppend(data_init2,
						ocg->CreateAssignment(0, data_array_ref_2[k]->clone(),
								ocg->CreateInt(zero)));
			} else {
				if (temp4.size() > 0) {

					/*
					 for (int x = 0; x < temp4.size(); x++)
					 data_array_ref_2[k] = ocg->CreateArrayRefExpression(
					 data_array_ref_2[k], temp4[x]);
					 */

					for (int x = 0; x < temp4.size(); x++)
						data_array_ref_2[k] = ocg->CreateArrayRefExpression(
								new_array[k],
								ocg->CreatePlus(
										ocg->CreateTimes(data_array_ref_2[k],
												ocg->CreateInt(coef_)), temp4[x]->clone()));
					//data_array_ref_2[k] = ocg->CreateArrayRefExpression(
					//		new_array_prime3[k]->name(),
					//		data_array_ref_2[k]->clone());
				} else
					data_array_ref_2[k] = ocg->CreateArrayRefExpression(
							new_array_prime3[k]->name(),
							ocg->CreateTimes(data_array_ref_2[k],
									ocg->CreateInt(coef_)));

				data_init = ocg->StmtListAppend(data_init,
						ocg->CreateAssignment(0, data_array_ref_2[k]->clone(),
								ocg->CreateInt(zero)));

			}

			//if(max_allocate)
			//data_array_ref_2_cpy.push_back(data_array_ref_2[k]->clone());
			//data_init = ocg->CreateAssignment(0,
			//		ocg->CreateArrayRefExpression(new_array, data_array_ref->clone()),
			//		ocg->CreateInt(0));

		}
		for (int i = inner_loop_bounds.size() - 1; i >= 0; i--) {

			//CG_outputRepr *loop_inductive = ocg->CreateInductive(
			//		ocg->CreateIdent(loop_iters[i]->name()),
			//		ocg->CreateInt(inner_loop_bounds[i].first),
			//		ocg->CreateInt(inner_loop_bounds[i].second), NULL);
			CG_outputRepr *loop_inductive2 = ocg->CreateInductive(
					ocg->CreateIdent(loop_iters[i]->name()),
					ocg->CreateInt(inner_loop_bounds[i].first),
					ocg->CreateInt(inner_loop_bounds[i].second), NULL);
			//data_init = ocg->CreateLoop(0, loop_inductive, data_init);
			if (!max_allocate)
				data_init2 = ocg->CreateLoop(0, loop_inductive2, data_init2);
			else
				data_init = ocg->CreateLoop(0, loop_inductive2, data_init);
		}
		count_inc = ocg->CreatePlusAssignment(0, count_2->clone(),
				ocg->CreateInt(1));

		count_inc2 = NULL;
		// Anand RED ALERT: commenting out indiv_count and making it
		// offset_index instead needs to take into account multiple outer levels for csb
		CG_outputRepr *explicit_offset_assign3 = NULL;
		explicit_offset_assign = NULL;

		if (most_reprs.find(level - 1) != most_reprs.end()) {

			if (enforce_marked)
				count_inc2 = ocg->CreatePlusAssignment(0,
						ocg->CreateArrayRefExpression(offset_index2->name(),
								ocg->CreatePlus(
										ocg->CreateIdent(
												stmt[*it].IS.set_var(level - 1)->name()),
										ocg->CreateInt(1))), ocg->CreateInt(1));
			else
				count_inc2 = ocg->CreatePlusAssignment(0,
						ocg->CreateArrayRefExpression(offset_index2->name(),
								ocg->CreatePlus(index_for_offset_index2->clone(),
										ocg->CreateInt(1))), ocg->CreateInt(1));

		}
		if (max_allocate) {
			marked_assign = ocg->CreateAssignment(0,
					ocg->CreateArrayRefExpression(marked2->name(),
							ocg->CreateIdent(
									stmt[stmt_num].IS.set_var(level)->name())),
					count_2->clone());
			for (int i = 0; i < explicit_index.size(); i++)
				explicit_offset_assign =
						ocg->StmtListAppend(explicit_offset_assign,
								ocg->CreateAssignment(0,
										ocg->CreateArrayRefExpression(
												explicit_index[i]->name(),
												ocg->CreateArrayRefExpression(
														marked2->name(),
														ocg->CreateIdent(
																stmt[stmt_num].IS.set_var(level)->name()))),
										ocg->CreateIdent(
												stmt[stmt_num].IS.set_var(level)->name())));
		}
		if (!max_allocate) {

			for (int i = 0; i < explicit_index.size(); i++) {
				if (most_reprs.find(level - 1) != most_reprs.end()) {
					if (enforce_marked) {
						if (level == max_level)
							explicit_offset_assign2 =
									static_cast<CG_roseBuilder *>(ocg)->CreateArrowRefExpression(
											static_cast<CG_roseBuilder *>(ocg)->CreateDotExpression(
													ocg->CreateArrayRefExpression(
															marked2->name(),
															ocg->CreateIdent(
																	stmt[*it].IS.set_var(level)->name())),
													static_cast<CG_roseBuilder *>(ocg)->lookup_member_data(
															mk_type, "ptr",
															ocg->CreateIdent(
																	marked2->name()))),
											static_cast<CG_roseBuilder *>(ocg)->lookup_member_data(
													list_type, "col_",
													ocg->CreateArrayRefExpression(
															new_array_prime2->name(),
															ocg->CreateIdent(
																	stmt[*it].IS.set_var(
																			level - 1)->name()))));
						else

							explicit_offset_assign2 =
									static_cast<CG_roseBuilder *>(ocg)->CreateArrowRefExpression(
											static_cast<CG_roseBuilder *>(ocg)->CreateDotExpression(
													ocg->CreateArrayRefExpression(
															marked2->name(),
															ocg->CreateIdent(
																	stmt[*it].IS.set_var(
																			max_level)->name())),
													static_cast<CG_roseBuilder *>(ocg)->lookup_member_data(
															mk_type, "ptr",
															ocg->CreateIdent(
																	marked2->name()))),
											static_cast<CG_roseBuilder *>(ocg)->lookup_member_data(
													list_type, "col_",
													ocg->CreateArrayRefExpression(
															new_array_prime2->name(),
															ocg->CreateIdent(
																	stmt[*it].IS.set_var(
																			level - 1)->name()))));

					} else

						explicit_offset_assign2 =
								static_cast<CG_roseBuilder *>(ocg)->CreateArrowRefExpression(
								//static_cast<CG_roseBuilder *>(ocg)->CreateDotExpression(
										ocg->CreateArrayRefExpression(
												new_array_prime2->name(),
												index_for_offset_index2->clone()),
										//static_cast<CG_roseBuilder *>(ocg)->lookup_member_data(
										//		mk_type, "ptr",
										//		ocg->CreateIdent(
										//				marked2->name()))),
										static_cast<CG_roseBuilder *>(ocg)->lookup_member_data(
												list_type, "col_",
												ocg->CreateArrayRefExpression(
														new_array_prime2->name(),
														ocg->CreateIdent(
																stmt[*it].IS.set_var(level - 1)->name()))));

				} else {
					if (enforce_marked)
						explicit_offset_assign2 =
								static_cast<CG_roseBuilder *>(ocg)->CreateArrowRefExpression(
										static_cast<CG_roseBuilder *>(ocg)->CreateDotExpression(
												ocg->CreateArrayRefExpression(
														marked2->name(),
														ocg->CreateIdent(
																stmt[*it].IS.set_var(level)->name())),
												static_cast<CG_roseBuilder *>(ocg)->lookup_member_data(
														mk_type, "ptr",
														ocg->CreateIdent(marked2->name()))),
										static_cast<CG_roseBuilder *>(ocg)->lookup_member_data(
												list_type, "col_",
												ocg->CreateIdent(
														new_array_prime2->name())));
					else
						explicit_offset_assign2 =
								static_cast<CG_roseBuilder *>(ocg)->CreateArrowRefExpression(
								//static_cast<CG_roseBuilder *>(ocg)->CreateDotExpression(
										ocg->CreateArrayRefExpression(
												new_array_prime2->name(),
												index_for_offset_index2->clone()),
										//static_cast<CG_roseBuilder *>(ocg)->lookup_member_data(
										//		mk_type, "ptr",
										//		ocg->CreateIdent(
										//				marked2->name()))),
										static_cast<CG_roseBuilder *>(ocg)->lookup_member_data(
												list_type, "col_",
												ocg->CreateIdent(
														new_array_prime2->name())));

				}
				if (explicit_index.size() > 1) {
					explicit_offset_assign2 = ocg->CreateArrayRefExpression(
							explicit_offset_assign2->clone(), ocg->CreateInt(i));
				}
				explicit_offset_assign3 = ocg->StmtListAppend(
						explicit_offset_assign3,
						ocg->CreateAssignment(0, explicit_offset_assign2,
								ocg->CreateIdent(
										stmt[*it].IS.set_var(level_[i])->name())));

			}
		}
		int level_coef_ = 1;

		for (int i = 0; i < inner_loop_bounds.size(); i++) {

			level_coef_ *= inner_loop_bounds[i].second - inner_loop_bounds[i].first
					+ 1;
		}
		/*CG_outputRepr *data_array_ref_ = ocg->CreateTimes(
		 ocg->CreateArrayRefExpression(marked->name(),
		 ocg->CreateIdent(stmt[stmt_num].IS.set_var(level)->name())),
		 ocg->CreateInt(level_coef_));
		 //To replace original data array ref in executor (a to a_prime)
		 */
		std::vector<CG_outputRepr *> data_prime_ref;

		if (!is_single)
			;										//data_prime_ref.push_back(
													//		ocg->CreateIdent(stmt[*it].IS.set_var(level)->name())->clone());
		else {
			if (level < 2)
				throw loop_error("simplification failure");
			CG_outputRepr *index = ocg->CreateIdent(
					stmt[*it].IS.set_var(level - 1)->name());
			/*for (int i = 1; i < level - 1; i++) {
			 Relation C = get_loop_bound(stmt[*it].IS, i, known);
			 int ub = -1, lb = -1;
			 int size;
			 for (DNF_Iterator di(const_cast<Relation &>(C).query_DNF()); di;
			 di++)
			 for (GEQ_Iterator gi((*di)->GEQs()); gi; gi++) {
			 if ((*gi).get_coef(C.set_var(level)) < 0
			 && (*gi).is_const(C.set_var(level))) {

			 ub = (*gi).get_const();
			 } else if ((*gi).get_coef(C.set_var(level)) > 0
			 && (*gi).is_const(C.set_var(level))) {

			 lb = (*gi).get_const();
			 }
			 }

			 if (lb < 0 || ub < 0)
			 throw loop_error(
			 "bounds of inner loops cannot be determined");
			 else
			 size *= (ub - lb + 1);

			 CG_outputRepr *current = ocg->CreateTimes(ocg->CreateInt(size),
			 ocg->CreateIdent(stmt[*it].IS.set_var(i)->name()));
			 index = ocg->CreatePlus(current, index);

			 }*/
			//data_prime_ref.push_back(index->clone());
		}

		std::vector<CG_outputRepr *> temp5;
		CG_outputRepr *ref2 = NULL;
		for (int i = 0; i < inner_loop_bounds.size(); i++) {

			CG_outputRepr *current, *current2;
			/*if(!is_single)
			 */
			//AV:2016 reverting to single dimensional array)
			//current =
			//		ocg->CreateIdent(
			//				stmt[*it].IS.set_var(
			//						loops_for_non_zero_block_count[i])->name());
			//else{
			current = ocg->CreateIdent(
					stmt[*it].IS.set_var(loops_for_non_zero_block_count[i])->name());

			/*	current2=
			 ocg->CreateIdent(
			 stmt[*it].IS.set_var(
			 loops_for_non_zero_block_count[i])->name());

			 }*/
			int level_coef = 1;
			for (int j = i + 1; j < inner_loop_bounds.size(); j++)
				level_coef *= inner_loop_bounds[j].second
						- inner_loop_bounds[j].first + 1;

			current = ocg->CreateTimes(ocg->CreateInt(level_coef), current);

			ref2 = ocg->CreatePlus(ref2, current->clone());

		}
		if (ref2 != NULL)
			data_prime_ref.push_back(ref2);
		for (int k = 0; k < data_array.size(); k++)
			data_prime_ref2.push_back(data_prime_ref);
		std::vector<CG_outputRepr *> data_prime_ref3;

		for (int k = 0; k < data_array.size(); k++) {
			if (!max_allocate) {
				if (data_prime_ref2[k].size() > 0) {
					CG_outputRepr *temp = ocg->CreateArrayRefExpression(
							data_array_ref_2_cpy[k], data_prime_ref2[k][0]);
					for (int x = 1; x < data_prime_ref2[k].size(); x++)
						temp = ocg->CreateArrayRefExpression(temp,
								data_prime_ref2[k][x]);
					data_prime_ref3.push_back(temp->clone());
				} else
					data_prime_ref3.push_back(

					data_array_ref_2_cpy[k]->clone());
			} else {
				if (is_single) {

					if (data_prime_ref2[k].size() > 0) {

						CG_outputRepr *temp = ocg->CreateArrayRefExpression(
								new_array[k],
								ocg->CreatePlus(
										ocg->CreateTimes(data_array_ref_2_cpy[k],
												ocg->CreateInt(coef_)),
										data_prime_ref2[k][0]->clone()));

						/*CG_outputRepr *temp = ocg->CreateArrayRefExpression(new_array[k]
						 data_array_ref_2_cpy[k], data_prime_ref2[k][0]);
						 for (int x = 1; x < data_prime_ref2[k].size(); x++)
						 temp = ocg->CreateArrayRefExpression(temp,
						 data_prime_ref2[k][x]);
						 */
						data_prime_ref3.push_back(temp->clone());
					} else
						data_prime_ref3.push_back(

						data_array_ref_2_cpy[k]->clone());

				} else {
					if (data_prime_ref2[k].size() > 0) {
						CG_outputRepr *temp = ocg->CreateArrayRefExpression(
								new_array_prime3[k]->name(), data_prime_ref2[k][0]);

						for (int x = 1; x < data_prime_ref2[k].size(); x++)
							temp = ocg->CreateArrayRefExpression(temp,
									data_prime_ref2[k][x]);
						data_prime_ref3.push_back(temp->clone());
					} else
						data_prime_ref3.push_back(
								ocg->CreateArrayRefExpression(
										new_array_prime3[k]->name(),
										ocg->CreateTimes(data_array_ref_2_cpy[k],
												ocg->CreateInt(coef_))));

				}
			}
		}

		CG_outputRepr *data_assign2 = NULL;
		for (int k = 0; k < data_array.size(); k++) {
			std::vector<CG_outputRepr *> data_rhs;

			//IR_CONSTANT_TYPE type;

			std::vector<IR_PointerArrayRef *> refs = ir->FindPointerArrayRef(
					stmt[*it].code);
			IR_CONSTANT_TYPE type = IR_CONSTANT_UNKNOWN;
			IR_PointerArrayRef* data_arr_ref;

			for (int i = 0; i < refs.size(); i++)
				if (refs[i]->name() == data_array[k]) {
					type = refs[i]->symbol()->elem_type();
					data_arr_ref = refs[i];

				}
			CG_outputRepr * data_arr_repr = NULL;
			if (type == IR_CONSTANT_UNKNOWN) {

				IR_ArrayRef* data_arr_ref;
				std::vector<IR_ArrayRef *> refs = ir->FindArrayRef(stmt[*it].code);

				for (int i = 0; i < refs.size(); i++)
					if (refs[i]->name() == data_array[k]) {
						type = refs[i]->symbol()->elem_type();
						data_arr_ref = refs[i];

					}

				for (int i = 0; i < data_arr_ref->n_dim(); i++)
					data_rhs.push_back(data_arr_ref->index(i)->clone());

				data_arr_repr = ocg->CreateArrayRefExpression(data_arr_ref->name(),
						data_rhs[0]);

			} else {

				for (int i = 0; i < data_arr_ref->n_dim(); i++)
					data_rhs.push_back(data_arr_ref->index(i)->clone());

				data_arr_repr = ocg->CreateArrayRefExpression(data_arr_ref->name(),
						data_rhs[0]);

			}

			for (int i = 1; i < data_rhs.size(); i++)
				data_arr_repr = ocg->CreateArrayRefExpression(data_arr_repr,
						data_rhs[i]);

			//data_assign = ocg->CreateAssignment(0,
			//		ocg->CreateArrayRefExpression(new_array, data_array_ref_->clone()),
			//		data_arr_repr->clone());

			data_assign2 = ocg->StmtListAppend(data_assign2,
					ocg->CreateAssignment(0, data_prime_ref3[k],
							data_arr_repr->clone()));

		}
		//CG_outputRepr *replaced_code = NULL;

		CG_outputRepr *replaced_code2 = NULL;
		for (std::map<int, CG_outputRepr *>::iterator i = most_reprs.begin();
				i != most_reprs.end(); i++) {
			//replaced_code = ocg->StmtListAppend(replaced_code, i->second->clone());
			replaced_code2 = ocg->StmtListAppend(replaced_code2,
					i->second->clone());
		}
		//CG_outputRepr *statement_body = NULL;
		CG_outputRepr *statement_body2 = NULL;

		if (max_allocate) {
			statement_body2 = ocg->StmtListAppend(marked_assign, data_init);
			statement_body2 = ocg->StmtListAppend(statement_body2,
					explicit_offset_assign);
		} else {
			statement_body2 = ocg->StmtListAppend(allocate, data_init2);

			statement_body2 = ocg->StmtListAppend(statement_body2,
					explicit_offset_assign3);
		}
		//statement_body = ocg->StmtListAppend(statement_body, count_inc->clone());
		statement_body2 = ocg->StmtListAppend(statement_body2,
				count_inc->clone());
		if (count_inc2 != NULL)
			statement_body2 = ocg->StmtListAppend(statement_body2,
					count_inc2->clone());

		//statement_body = ocg->CreateIf(0, marked_check, statement_body, NULL);
		if (enforce_marked && (max_level > 0 || is_single))
			statement_body2 = ocg->CreateIf(0, marked_check2, statement_body2,
			NULL);
		//statement_body = ocg->StmtListAppend(statement_body, data_assign);
		statement_body2 = ocg->StmtListAppend(statement_body2, data_assign2);

		if (guards.size() > 0 && check_if) {
			CG_outputRepr *cond = dynamic_cast<IR_If*>(code)->condition();
			//statement_body = ocg->CreateIf(0, cond->clone(), statement_body, NULL);
			statement_body2 = ocg->CreateIf(0, cond, statement_body2, NULL);
		}

		//statement_body = ocg->StmtListAppend(replaced_code, statement_body);
		statement_body2 = ocg->StmtListAppend(replaced_code2, statement_body2);

		inner_array_cpy_stmts = ocg->StmtListAppend(inner_array_cpy_stmts,
				statement_body2);

		//}
		//Anand: adding support for multiple array references

		level_coef_ = 1;

		for (int i = 0; i < inner_loop_bounds.size(); i++) {

			level_coef_ *= inner_loop_bounds[i].second - inner_loop_bounds[i].first
					+ 1;
		}

		//14. Set offset_index[ii] = count just outside kk loop.
		CG_outputRepr *offset_index_assign = NULL;
		if (level > 1 && !max_allocate)
			offset_index_assign = ocg->CreateAssignment(0,
					ocg->CreateArrayRefExpression(offset_index2->name(),
							ocg->CreatePlus(
									ocg->CreateIdent(
											stmt[*it].IS.set_var(level - 1)->name()),
									ocg->CreateInt(1))), count_2->clone());

		/*std::vector<int> remain;
		 std::vector<int> remain_for_offset_index_assign;

		 for (std::set<int>::iterator i = remaining_loops.begin();
		 i != remaining_loops.end(); i++)
		 if (*i < level)
		 remain_for_offset_index_assign.push_back(*i);

		 for (std::set<int>::iterator i = remaining_loops.begin();
		 i != remaining_loops.end(); i++)
		 remain.push_back(*i);

		 std::pair<Relation, Relation> xform_is = construct_reduced_IS_And_XFORM(ir,
		 stmt[*it].IS, stmt[*it].xform, remain, lex, this->known,
		 uninterpreted_symbols[*it]);

		 std::pair<Relation, Relation> xform_is_2 = construct_reduced_IS_And_XFORM(
		 ir, stmt[*it].IS, stmt[*it].xform,
		 remain_for_offset_index_assign, lex, this->known,
		 uninterpreted_symbols[*it]);
		 */
		Statement t;
		Statement old_stmt = stmt[*it];
		old_code = stmt[*it].code;
		t.IS = xform_is.first;
		t.IS.simplify();
		t.xform = xform_is.second;
		//stmt[stmt_num].code = statement_body;
		t.code = inner_array_cpy_stmts;	//statement_body2;
		std::vector<LoopLevel> ll0;
		old_loop_level = stmt[*it].loop_level;
		for (std::set<int>::iterator i = remaining_loops.begin();
				i != remaining_loops.end(); i++)
			ll0.push_back(stmt[*it].loop_level[*i - 1]);
		t.loop_level = ll0;

		t.ir_stmt_node = NULL;
		t.has_inspector = false;

		//i. Copy Statement Xform and IS
		int new_stmt_num = stmt.size();
		std::set<int> stmts;
		if ((is_single || (most_reprs.find(max_level) != most_reprs.end()))
				&& enforce_marked) {
			Statement s;
			s.IS = t.IS;
			s.xform = t.xform;
			s.has_inspector = false;
			s.ir_stmt_node = NULL;
			s.reduction = 0;
			std::vector<LoopLevel> ll;
			for (std::set<int>::iterator i = remaining_loops.begin();
					i != remaining_loops.end(); i++)
				ll.push_back(old_loop_level[*i - 1]);
			s.loop_level = ll;
			//s.code = new_code;
			s.code = new_code2;
			//lex = getLexicalOrder(*it);

			stmt.push_back(s);

			uninterpreted_symbols.push_back(uninterpreted_symbols[*it]);
			uninterpreted_symbols_stringrepr.push_back(
					uninterpreted_symbols_stringrepr[*it]);
			unin_rel.push_back(unin_rel[*it]);
			clean_unin_relation(s.IS, unin_rel[unin_rel.size() - 1]);
			dep.insert();
			//apply_xform(stmt.size() - 1);

			stmts.insert(new_stmt_num);

		}
		int new_stmt_num_0 = stmt.size();

		stmt.push_back(t);
		dep.insert();

		stmts.insert(new_stmt_num_0);
		uninterpreted_symbols.push_back(uninterpreted_symbols[*it]);
		uninterpreted_symbols_stringrepr.push_back(
				uninterpreted_symbols_stringrepr[*it]);
		unin_rel.push_back(unin_rel[*it]);
		clean_unin_relation(t.IS, unin_rel[unin_rel.size() - 1]);
		apply_xform(stmts);
		lex[2 * count_rem - 2] += 1;
		shiftLexicalOrder(lex, 2 * count_rem - 2, 1);
		assign_const(stmt[new_stmt_num_0].xform, 2 * count_rem - 2,
				lex[2 * count_rem - 2]);
		lex = getLexicalOrder(new_stmt_num_0);

		//i. Copy Statement Xform and IS

		Statement s2;
		if (most_reprs.find(level - 1) == most_reprs.end()
				&& most_reprs.find(level) != most_reprs.end())
			if (offset_index_assign) {
				s2.IS = xform_is_2.first;
				s2.xform = xform_is_2.second;
				s2.has_inspector = false;
				s2.ir_stmt_node = NULL;
				s2.reduction = 0;
				std::vector<LoopLevel> ll2;
				for (std::set<int>::iterator i = remaining_loops.begin();
						i != remaining_loops.end(); i++)
					if (*i < level)
						ll2.push_back(stmt[new_stmt_num_0].loop_level[*i - 1]);
				s2.loop_level = ll2;
				s2.code = offset_index_assign;

				assign_const(s2.xform, 2 * count_rem - 2,
						lex[2 * count_rem - 2] + 1);

				stmt.push_back(s2);
				dep.insert();
				uninterpreted_symbols.push_back(
						uninterpreted_symbols[new_stmt_num_0]);
				uninterpreted_symbols_stringrepr.push_back(
						uninterpreted_symbols_stringrepr[new_stmt_num_0]);
				unin_rel.push_back(std::map<std::string, std::vector<Relation> >());
				//clean_unin_relation(s2.IS,unin_rel[unin_rel.size() -1]);

			}

		//14b. Create Malloc code after exact size has been determined

		//assign_const(s3.xform, 0, lex[0] + 1);
		//Anand: Hack to shift lexical Order
		lex[0] += 1;

		shiftLexicalOrder(lex, 0, 1);
		if (max_allocate) {
			CG_outputRepr *size = ocg->CreateInt(1);
			for (int i = 0; i < size_repr_.size(); i++)
				size = ocg->CreateTimes(size_repr_[i]->clone(), size);
			for (int i = 0; i < level_.size(); i++)
				if (max_col_size < 65536)
					ocg->StmtListAppend(init_code,
							ir->CreateMalloc(IR_CONSTANT_SHORT,
									explicit_index[i]->name(), size->clone()));
				else
					ocg->StmtListAppend(init_code,
							ir->CreateMalloc(IR_CONSTANT_INT,
									explicit_index[i]->name(), size->clone()));
		}
		if (!max_allocate) {
			Statement m_s[3];

			for (int i = 0; i < 3; i++) {
				// 	m_s[i].IS = xform_is_2.first;
				//	s2.xform = xform_is_2.second;
				m_s[i].has_inspector = false;
				m_s[i].ir_stmt_node = NULL;
				m_s[i].reduction = 0;
				std::vector<LoopLevel> ll2;

			}

			CG_outputRepr *malloced_stmts, *dc_stmts = NULL, *index_cp_stmt,
					*ll_inc_and_free = NULL;
			malloced_stmts = NULL;

			for (int i = 0; i < level_.size(); i++)
				if (max_col_size < 65536)
					malloced_stmts = ocg->StmtListAppend(malloced_stmts,
							ir->CreateMalloc(IR_CONSTANT_SHORT,
									explicit_index[i]->name(), count_2->clone()));
				else
					malloced_stmts = ocg->StmtListAppend(malloced_stmts,
							ir->CreateMalloc(IR_CONSTANT_INT,
									explicit_index[i]->name(), count_2->clone()));
			CG_outputRepr *x1 = count_2->clone();
			for (int i = 0; i < sizes.size(); i++)
				x1 = ocg->CreateTimes(x1->clone(), sizes[i]->clone());
			for (int k = 0; k < new_array.size(); k++) {
				if (size_repr3.size() == 0)
					malloced_stmts = ocg->StmtListAppend(malloced_stmts,
							ir->CreateMalloc(types[k], new_array[k],
									ocg->CreateTimes(count_2->clone(),
											ocg->CreateInt(level_coef_))));
				else
					//change following
					/*malloced_stmts = ocg->StmtListAppend(malloced_stmts,
					 ir->CreateMalloc(size_repr3[k]->clone(),
					 new_array[k], count_2->clone()));
					 */

					malloced_stmts = ocg->StmtListAppend(malloced_stmts,
							ir->CreateMalloc(types[k], new_array[k], x1));

			}
			Relation IS_ms(1);
			Relation IS_ll(1 + inner_loop_bounds.size());
			Relation xform_ms(1, 3);

			Relation xform_ll(1 + inner_loop_bounds.size(),
					2 * (1 + inner_loop_bounds.size()) + 1);
			Relation IS_dc;
			Relation xform_dc;

			//if (most_reprs.find(level - 1) == most_reprs.end()) {
			//if(enforce_marked){
			IS_dc = Relation(1 + inner_loop_bounds.size());
			xform_dc = Relation(1 + inner_loop_bounds.size(),
					2 * (1 + inner_loop_bounds.size()) + 1);
			//}
			//} else {
			//IS_dc = Relation(2 + inner_loop_bounds.size());
			//xform_dc = Relation(2 + inner_loop_bounds.size(),
			//		2 * (2 + inner_loop_bounds.size()) + 1);

			//}
			//prepare malloced stmts relation

			F_And *root_ms = IS_ms.add_and();
			Variable_ID tmp_fv;

			Free_Var_Decl *cc1;

			cc1 = new Free_Var_Decl("chill_count_1");
			tmp_fv = IS_ms.get_local(cc1);
			if (most_reprs.find(level - 1) == most_reprs.end()) {

				EQ_Handle e1 = root_ms->add_EQ();
				e1.update_coef(IS_ms.set_var(1), 1);

				freevar.push_back(cc1);
				e1.update_coef(tmp_fv, 1);
				e1.update_const(-1);
			} else {

				EQ_Handle e1 = root_ms->add_EQ();
				e1.update_coef(IS_ms.set_var(1), 1);

			}
			//} else {
			/*	cc1 = new Free_Var_Decl(indiv_count->name(), 1);
			 ;
			 tmp_fv = IS_ms.get_local(cc1, Input_Tuple);
			 int size = 1;

			 // Relation R = extract_upper_bound(i->second, i->second.set_var(i->first));
			 int lb = -1, ub = -1;
			 Relation R = get_loop_bound(stmt[*it].IS, level - 1,
			 this->known);
			 for (DNF_Iterator di(const_cast<Relation &>(R).query_DNF()); di;
			 di++)
			 for (GEQ_Iterator gi((*di)->GEQs()); gi; gi++) {
			 if ((*gi).get_coef(R.set_var(level - 1)) < 0
			 && (*gi).is_const(R.set_var(level - 1))) {

			 ub = (*gi).get_const();
			 } else if ((*gi).get_coef(R.set_var(level - 1)) > 0
			 && (*gi).is_const(R.set_var(level - 1))) {

			 lb = (*gi).get_const();
			 }
			 }

			 if (lb < 0 || ub < 0)
			 throw loop_error(
			 "bounds of inner loops cannot be determined");

			 size *= (ub - lb);
			 GEQ_Handle g11 = root_ms->add_GEQ();
			 GEQ_Handle g22 = root_ms->add_GEQ();
			 g11.update_coef(IS_ms.set_var(1), 1);
			 g11.update_const(size);
			 g22.update_coef(IS_ms.set_var(1), -1);
			 g22.update_const(-(size - 1));

			 //	e1.update_const(size);

			 //}
			 */
			int ll_size;
			/*	Relation known_(stmt[*it].IS.n_set());
			 known_.copy_names(copy(stmt[*it].IS.n_set()));
			 known_.setup_names();

			 F_And *fr = known_.add_and();
			 GEQ_Handle g = fr->add_GEQ();

			 Variable_ID tmp_fv_;
			 if(most_reprs.find(level-1) == most_reprs.end())

			 tmp_fv_ =  known_.get_local(cc1);
			 else
			 tmp_fv_ = known_.get_local(cc1,Input_Tuple);
			 g.update_coef(tmp_fv_, 1);
			 g.update_const(-1);
			 this->addKnown(known_);
			 */
			F_And *root_ms_x = xform_ms.add_and();
			F_And *root_dc_x = xform_dc.add_and();
			for (int i = 1; i <= 3; i += 2) {
				EQ_Handle e = root_ms_x->add_EQ();
				e.update_coef(xform_ms.output_var(i), 1);
				if (i == 1)
					e.update_const(-lex[0]);
			}
			EQ_Handle e2 = root_ms_x->add_EQ();
			e2.update_coef(xform_ms.output_var(2), 1);
			e2.update_coef(xform_ms.input_var(1), -1);
			m_s[0].IS = IS_ms;
			m_s[0].xform = xform_ms;
			LoopLevel single;
			single.made_dense = false;
			single.parallel_level = false;
			single.type = LoopLevelOriginal;
			single.payload = 0;
			std::vector<LoopLevel> empty_ll;
			empty_ll.push_back(single);
			m_s[0].loop_level = empty_ll;
			m_s[0].code = malloced_stmts;

			F_And *root_dc = IS_dc.add_and();
			GEQ_Handle g1 = root_dc->add_GEQ();
			GEQ_Handle g2 = root_dc->add_GEQ();

			//	if (most_reprs.find(level - 1) == most_reprs.end()) {
			g1.update_coef(IS_dc.set_var(1), 1);

			Variable_ID tmp_fv1;
			tmp_fv1 = IS_dc.get_local(cc1);
			g1.update_coef(tmp_fv1, 1);
			g1.update_const(-1);
			g2.update_coef(IS_dc.set_var(1), -1);
			/*	} else {

			 int size_ = 1;

			 // Relation R = extract_upper_bound(i->second, i->second.set_var(i->first));
			 int lb_ = -1, ub_ = -1;
			 Relation R1 = get_loop_bound(stmt[*it].IS, level - 1,
			 this->known);
			 for (DNF_Iterator di(const_cast<Relation &>(R1).query_DNF()); di;
			 di++)
			 for (GEQ_Iterator gi((*di)->GEQs()); gi; gi++) {
			 if ((*gi).get_coef(R1.set_var(level - 1)) < 0
			 && (*gi).is_const(R1.set_var(level - 1))) {

			 ub_ = (*gi).get_const();
			 } else if ((*gi).get_coef(R1.set_var(level - 1)) > 0
			 && (*gi).is_const(R1.set_var(level - 1))) {

			 lb_ = (*gi).get_const();
			 }
			 }

			 if (lb_ < 0 || ub_ < 0)
			 throw loop_error(
			 "bounds of inner loops cannot be determined");

			 size_ *= (ub_ - lb_);
			 ll_size = size_;

			 g1.update_coef(IS_dc.set_var(1), 1);
			 Variable_ID tmp_fv1;
			 g1.update_const(size_);

			 //tmp_fv1= IS_dc.get_local(cc1);
			 g2.update_coef(IS_dc.set_var(1), -1);

			 GEQ_Handle g3 = root_dc->add_GEQ();
			 GEQ_Handle g4 = root_dc->add_GEQ();

			 tmp_fv1 = IS_dc.get_local(cc1, Input_Tuple);
			 g3.update_coef(tmp_fv1, 1);
			 g3.update_const(-1);
			 g3.update_coef(IS_dc.set_var(2), 1);

			 g4.update_coef(IS_dc.set_var(2), -1);

			 }
			 */
			//g2.update_const(1);
			for (int i = 0; i < inner_loop_bounds.size(); i++) {
				int ub = inner_loop_bounds[i].second;
				int lb = inner_loop_bounds[i].first;

				GEQ_Handle g1 = root_dc->add_GEQ();
				g1.update_coef(IS_dc.set_var(i + 2), 1);
				g1.update_const(-lb);

				GEQ_Handle g2 = root_dc->add_GEQ();
				g2.update_coef(IS_dc.set_var(i + 2), -1);
				g2.update_const(ub);

				EQ_Handle e = root_dc_x->add_EQ();
				e.update_coef(xform_dc.output_var(2 * i + 3), 1);

				EQ_Handle e2 = root_dc_x->add_EQ();
				e2.update_coef(xform_dc.output_var(2 * (i + 2)), 1);
				e2.update_coef(xform_dc.input_var(i + 2), -1);
			}

			EQ_Handle e3 = root_dc_x->add_EQ();
			e3.update_coef(xform_dc.output_var(xform_dc.n_out()), 1);

			EQ_Handle e4 = root_dc_x->add_EQ();
			e4.update_coef(xform_dc.output_var(1), 1);
			e4.update_const(-lex[0]);

			EQ_Handle e5 = root_dc_x->add_EQ();
			e5.update_coef(xform_dc.output_var(2), 1);
			e5.update_coef(xform_dc.input_var(1), -1);

			EQ_Handle e6 = root_dc_x->add_EQ();
			e6.update_coef(xform_dc.output_var(3), 1);
			e6.update_const(0);

			if (level > 2 && enforce_marked) {
				EQ_Handle e7 = root_dc_x->add_EQ();
				e7.update_coef(xform_dc.output_var(4), 1);
				e7.update_coef(xform_dc.input_var(2), -1);
			}
			F_And *root_ll = IS_ll.add_and();
			Variable_ID tmp_fv3;
			F_And *root_ll_x;

			//if (most_reprs.find(level - 1) == most_reprs.end()) {
			tmp_fv3 = IS_ll.get_local(cc1);
			GEQ_Handle g3 = root_ll->add_GEQ();
			g3.update_coef(IS_ll.set_var(1), 1);

			g3.update_coef(tmp_fv3, 1);
			g3.update_const(-1);

			root_ll_x = xform_ll.add_and();
			GEQ_Handle g4 = root_ll->add_GEQ();
			g4.update_coef(IS_ll.set_var(1), -1);

			/*} else {
			 root_ll_x = xform_ll.add_and();
			 tmp_fv3 = IS_ll.get_local(cc1, Input_Tuple);
			 //	g3.update_const(ll_size);
			 GEQ_Handle g1 = root_ll->add_GEQ();
			 GEQ_Handle g2 = root_ll->add_GEQ();
			 g1.update_coef(IS_dc.set_var(1), 1);
			 g1.update_const(size);

			 //tmp_fv1= IS_dc.get_local(cc1);
			 g2.update_coef(IS_dc.set_var(1), -1);

			 }*/

			for (int i = 0; i < inner_loop_bounds.size(); i++) {
				int ub = inner_loop_bounds[i].second;

				EQ_Handle g1 = root_ll->add_EQ();
				g1.update_coef(IS_ll.set_var(i + 2), 1);
				g1.update_const(-ub);

				EQ_Handle e = root_ll_x->add_EQ();
				e.update_coef(xform_ll.output_var(2 * i + 3), 1);
				e.update_const(-1);
				EQ_Handle e2 = root_ll_x->add_EQ();
				e2.update_coef(xform_ll.output_var(2 * (i + 2)), 1);
				e2.update_coef(xform_ll.input_var(i + 2), -1);
			}

			for (int i = 1; i <= 1; i += 1) {
				EQ_Handle e = root_ll_x->add_EQ();
				e.update_coef(xform_ll.output_var(i), 1);
				e.update_const(-lex[0]);
			}

			EQ_Handle e_4 = root_ll_x->add_EQ();
			e_4.update_coef(xform_ll.output_var(2), 1);
			e_4.update_coef(xform_ll.input_var(1), -1);

			EQ_Handle e_5 = root_ll_x->add_EQ();
			e_5.update_coef(xform_ll.output_var(xform_ll.n_out()), 1);

			//data copy from linked list to new_array_prime
			CG_outputRepr *new_data_array_ref_ = ocg->CreateTimes(

			ocg->CreateMinus(NULL, ocg->CreateIdent(IS_dc.set_var(1)->name())),
					ocg->CreateInt(level_coef_));
			CG_outputRepr * temp2_ = ocg->CreateInt(0);
			//0. index expression creation
			for (int i = 0; i < inner_loop_bounds.size(); i++) {
				CG_outputRepr *current = ocg->CreateIdent(
						IS_dc.set_var(i + 2)->name());
				int level_coef = 1;
				for (int j = i + 1; j < inner_loop_bounds.size(); j++)
					level_coef *= inner_loop_bounds[j].second
							- inner_loop_bounds[j].first + 1;

				current = ocg->CreateTimes(ocg->CreateInt(level_coef), current);

				new_data_array_ref_ = ocg->CreatePlus(new_data_array_ref_,
						current->clone());
				if (i == 0)
					temp2_ = current->clone();
				else
					temp2_ = ocg->CreatePlus(temp2_, current->clone());
			}
			CG_outputRepr *ll_inc_and_free2 = NULL;
			//1. lhs array ref
			std::vector<CG_outputRepr *> lhs;
			for (int k = 0; k < new_array.size(); k++) {
				lhs.push_back(
						ocg->CreateArrayRefExpression(new_array[k],
								new_data_array_ref_->clone()));
			}
			if (most_reprs.find(level - 1) == most_reprs.end()) {

				for (int k = 0; k < new_array.size(); k++) {
					CG_outputRepr *rhs =
							dynamic_cast<CG_roseBuilder *>(ocg)->CreateArrowRefExpression(
									new_array_prime3[new_array.size() - 1]->name(),
									dynamic_cast<CG_roseBuilder *>(ocg)->lookup_member_data(
											list_type2[0], data_array[k],
											ocg->CreateIdent(new_array[k])));
					rhs = ocg->CreateArrayRefExpression(rhs, temp2_->clone());
					dc_stmts = ocg->StmtListAppend(dc_stmts,
							ocg->CreateAssignment(0, lhs[k]->clone(), rhs->clone()));
				}
				// copy col
				CG_outputRepr *lhs_col = ocg->CreateArrayRefExpression(
						explicit_index[0]->name(),
						ocg->CreateMinus(NULL,
								ocg->CreateIdent(IS_ll.set_var(1)->name())));
				CG_outputRepr *rhs_col =
						dynamic_cast<CG_roseBuilder *>(ocg)->CreateArrowRefExpression(
								new_array_prime3[new_array.size() - 1]->name(),
								dynamic_cast<CG_roseBuilder *>(ocg)->lookup_member_data(
										list_type2[0], "col_",
										ocg->CreateIdent(
												new_array_prime3[new_array.size() - 1]->name())));
				index_cp_stmt = ocg->CreateAssignment(0, lhs_col->clone(),
						rhs_col->clone());

				ll_inc_and_free =
						ocg->CreateAssignment(0, ocg->CreateIdent(temp2[0]->name()),
								dynamic_cast<CG_roseBuilder *>(ocg)->CreateArrowRefExpression(
										new_array_prime3[new_array.size() - 1]->name(),
										dynamic_cast<CG_roseBuilder *>(ocg)->lookup_member_data(
												list_type2[0], "next",
												ocg->CreateIdent(
														new_array_prime3[new_array.size() - 1]->name()))));
				ll_inc_and_free = ocg->StmtListAppend(ll_inc_and_free,
						ir->CreateFree(
								ocg->CreateIdent(
										new_array_prime3[new_array.size() - 1]->name())));
				ll_inc_and_free = ocg->StmtListAppend(ll_inc_and_free,
						ocg->CreateAssignment(0,
								ocg->CreateIdent(
										new_array_prime3[new_array.size() - 1]->name()),
								ocg->CreateIdent(temp2[0]->name())));
				ll_inc_and_free = ocg->StmtListAppend(index_cp_stmt,
						ll_inc_and_free);
				ll_inc_and_free2 = ocg->StmtListAppend(ll_inc_and_free2,
						ll_inc_and_free);
			} else {

				for (int k = 0; k < new_array.size(); k++) {
					CG_outputRepr *rhs =
							dynamic_cast<CG_roseBuilder *>(ocg)->CreateArrowRefExpression(
									ocg->CreateArrayRefExpression(
											new_array_prime3[new_array.size() - 1]->name(),
											ocg->CreateIdent(
													stmt[*it].IS.set_var(level - 1)->name())),
									dynamic_cast<CG_roseBuilder *>(ocg)->lookup_member_data(
											list_type2[0], data_array[k],
											ocg->CreateArrayRefExpression(
													new_array_prime3[new_array.size() - 1]->name(),
													ocg->CreateIdent(
															stmt[*it].IS.set_var(level - 1)->name()))));
					rhs = ocg->CreateArrayRefExpression(rhs, temp2_->clone());

					dc_stmts = ocg->StmtListAppend(dc_stmts,
							ocg->CreateAssignment(0, lhs[k]->clone(), rhs->clone()));
				}
				// copy col
				CG_outputRepr *lhs_col = ocg->CreateArrayRefExpression(
						explicit_index[0]->name(),
						ocg->CreateMinus(NULL,
								ocg->CreateIdent(IS_ll.set_var(1)->name())));
				CG_outputRepr *rhs_col =
						dynamic_cast<CG_roseBuilder *>(ocg)->CreateArrowRefExpression(
								ocg->CreateArrayRefExpression(
										new_array_prime3[new_array.size() - 1]->name(),
										ocg->CreateIdent(
												stmt[new_stmt_num_0].IS.set_var(level - 1)->name())),
								dynamic_cast<CG_roseBuilder *>(ocg)->lookup_member_data(
										list_type2[0], "col_",
										ocg->CreateArrayRefExpression(
												new_array_prime3[new_array.size() - 1]->name(),
												ocg->CreateIdent(
														stmt[new_stmt_num_0].IS.set_var(
																level - 1)->name()))));
				index_cp_stmt = ocg->CreateAssignment(0, lhs_col->clone(),
						rhs_col->clone());

				ll_inc_and_free =
						ocg->CreateAssignment(0, ocg->CreateIdent(temp2[0]->name()),
								dynamic_cast<CG_roseBuilder *>(ocg)->CreateArrowRefExpression(
										ocg->CreateArrayRefExpression(
												new_array_prime3[new_array.size() - 1]->name(),
												ocg->CreateIdent(
														stmt[new_stmt_num_0].IS.set_var(
																level - 1)->name())),
										dynamic_cast<CG_roseBuilder *>(ocg)->lookup_member_data(
												list_type2[0], "next",
												ocg->CreateArrayRefExpression(
														new_array_prime3[new_array.size() - 1]->name(),
														ocg->CreateIdent(
																stmt[new_stmt_num_0].IS.set_var(
																		level - 1)->name())))));
				ll_inc_and_free =
						ocg->StmtListAppend(ll_inc_and_free,
								ir->CreateFree(
										ocg->CreateArrayRefExpression(
												new_array_prime3[new_array.size() - 1]->name(),
												ocg->CreateIdent(
														stmt[new_stmt_num_0].IS.set_var(
																level - 1)->name()))));
				ll_inc_and_free =
						ocg->StmtListAppend(ll_inc_and_free,
								ocg->CreateAssignment(0,
										ocg->CreateArrayRefExpression(
												new_array_prime3[new_array.size() - 1]->name(),
												ocg->CreateIdent(
														stmt[new_stmt_num_0].IS.set_var(
																level - 1)->name())),
										ocg->CreateIdent(temp2[0]->name())));
				ll_inc_and_free = ocg->StmtListAppend(index_cp_stmt,
						ll_inc_and_free);
				ll_inc_and_free2 = ocg->StmtListAppend(ll_inc_and_free2,
						ll_inc_and_free);

			}

			m_s[1].IS = IS_dc;
			m_s[1].xform = xform_dc;
			std::vector<LoopLevel> empty_ll2;
			for (int i = 0; i <= inner_loop_bounds.size(); i++) {
				LoopLevel tmp;
				tmp.payload = i;
				tmp.type = LoopLevelOriginal;
				tmp.parallel_level = 0;
				tmp.segreducible = false;
				empty_ll2.push_back(tmp);
			}
			m_s[1].loop_level = empty_ll2;
			m_s[1].code = dc_stmts;

			m_s[2].IS = IS_ll;
			m_s[2].xform = xform_ll;
			std::vector<LoopLevel> empty_ll3;
			//for (int i = 0; i <= inner_loop_bounds.size(); i++) {
			LoopLevel tmp;
			tmp.payload = 0;
			tmp.type = LoopLevelOriginal;
			tmp.parallel_level = 0;
			tmp.segreducible = false;
			LoopLevel single2;
			single.made_dense = false;
			single.parallel_level = false;
			single.type = LoopLevelOriginal;
			single.payload = 1;

			empty_ll3.push_back(tmp);
			empty_ll3.push_back(single);
			//}
			m_s[2].loop_level = empty_ll3;
			m_s[2].code = ll_inc_and_free2;

			stmt.push_back(m_s[0]);
			uninterpreted_symbols.push_back(uninterpreted_symbols[*it]);
			uninterpreted_symbols_stringrepr.push_back(
					uninterpreted_symbols_stringrepr[*it]);
			unin_rel.push_back(std::map<std::string, std::vector<Relation> >());
			clean_unin_relation(m_s[0].IS, unin_rel[unin_rel.size() - 1]);
			dep.insert();

			//AV: 2016 causing problem changing
			//if (max_level == level && max_level == 1) {
			if (max_level == level) { //change
				for (int i = 1; i < 3; i++) {
					stmt.push_back(m_s[i]);
					uninterpreted_symbols.push_back(uninterpreted_symbols[*it]);
					uninterpreted_symbols_stringrepr.push_back(
							uninterpreted_symbols_stringrepr[*it]);
					unin_rel.push_back(unin_rel[*it]);
					clean_unin_relation(m_s[i].IS, unin_rel[unin_rel.size() - 1]);
					dep.insert();

				}

				assign_const(stmt[stmt.size() - 1].xform, 0, lex[0]);
				assign_const(stmt[stmt.size() - 2].xform, 0, lex[0]);
				assign_const(stmt[stmt.size() - 3].xform, 0, lex[0]);

			} else {
				Statement new_cpy_stmts;

				new_cpy_stmts.has_inspector = false;
				new_cpy_stmts.ir_stmt_node = NULL;
				new_cpy_stmts.reduction = 0;

				std::vector<LoopLevel> empty_ll2;
				for (int i = 1; i <= level - 1; i++) {
					LoopLevel tmp;
					tmp.payload = i;
					tmp.type = LoopLevelOriginal;
					tmp.parallel_level = 0;
					tmp.segreducible = false;
					empty_ll2.push_back(tmp);
				}
				new_cpy_stmts.loop_level = empty_ll2;
				new_cpy_stmts.IS = init_is.first;
				new_cpy_stmts.xform = init_is.second;

				//code template
				/*for(k=0; k < n; k++){

				 for(i=0; i < n; i++)
				 for(j=1- P[k][i+1]; j <= 0; j++){
				 A[P[k][i] -j] = list->entry
				 temp = list->next;
				 free(list)
				 list = temp;
				 P[k][i+1] += P[k][i];
				 }
				 P[k+1][0] = p[k][n];

				 }
				 */

				Relation IS = init_is.first;
				std::vector<int> loop_bounds;

				for (int i = 1; i <= level - 1; i++)
					loop_bounds.push_back(
							extract_loop_trip_count(IS, IS.set_var(i)));

				CG_outputRepr *index2 = NULL;
				for (int i = 1; i <= level - 1; i++) {
					int size = 1;
					CG_outputRepr *index = ocg->CreateIdent(IS.set_var(i)->name());
					for (int j = i + 1; j <= level - 1; j++)
						size *= loop_bounds[j - 1];

					index2 = ocg->CreatePlus(index2,
							ocg->CreateTimes(ocg->CreateInt(size), index));

				}

				CG_outputRepr *lb_start, *ub_end, *loop_header, *loop_body;
				loop_body = NULL;
				CG_outputRepr *index3 = ocg->CreatePlus(index2->clone(),
						ocg->CreateInt(1));
				lb_start = ocg->CreateMinus(ocg->CreateInt(1),
						ocg->CreateArrayRefExpression(offset_index2->name(),
								index3->clone()));

				CG_outputRepr *free_lb_start, *free_ub_end, *free_loop_header;
				free_lb_start = ocg->CreateArrayRefExpression(offset_index2->name(),
						index2->clone());

				free_ub_end = ocg->CreateMinus(
						ocg->CreateArrayRefExpression(offset_index2->name(),
								index3->clone()), ocg->CreateInt(1));
				ub_end = ocg->CreateInt(0);
				IR_ScalarSymbol *tmp = ir->CreateScalarSymbol(IR_CONSTANT_INT, 0);
				loop_header = ocg->CreateInductive(ocg->CreateIdent(tmp->name()),
						lb_start, ub_end,
						NULL);
				free_loop_header = ocg->CreateInductive(
						ocg->CreateIdent(tmp->name()), free_lb_start, free_ub_end,
						NULL);

				CG_outputRepr *array_cpy_lhs, *array_cpy_rhs;
				CG_outputRepr *free_loop_body = NULL;

				if (explicit_index.size() < 2) {
					loop_body =
							ocg->StmtListAppend(loop_body,
									ocg->CreateAssignment(0,
											ocg->CreateArrayRefExpression(
													explicit_index[0]->name(),
													ocg->CreateMinus(
															ocg->CreateArrayRefExpression(
																	ocg->CreateIdent(
																			offset_index2->name()),
																	index2->clone()),
															ocg->CreateIdent(tmp->name()))),
											static_cast<CG_roseBuilder *>(ocg)->CreateArrowRefExpression(
											//ocg->CreateArrayRefExpression(
													ocg->CreateIdent(temp2[0]->name()),
													//		index2->clone()),
													//	ocg->CreateArrayRefExpression(
													static_cast<CG_roseBuilder *>(ocg)->lookup_member_data(
															list_type, "col_",
															ocg->CreateIdent(
																	temp2[0]->name())))));

				} else
					for (int i = 0; i < explicit_index.size(); i++)
						loop_body =
								ocg->StmtListAppend(loop_body,
										ocg->CreateAssignment(0,
												ocg->CreateArrayRefExpression(
														explicit_index[i]->name(),
														ocg->CreateMinus(
																ocg->CreateArrayRefExpression(
																		ocg->CreateIdent(
																				offset_index2->name()),
																		index2->clone()),
																ocg->CreateIdent(tmp->name()))),
												static_cast<CG_roseBuilder *>(ocg)->CreateArrowRefExpression(
												//ocg->CreateArrayRefExpression(
														ocg->CreateIdent(temp2[0]->name()),
														//		index2->clone()),
														ocg->CreateArrayRefExpression(
																static_cast<CG_roseBuilder *>(ocg)->lookup_member_data(
																		list_type, "col_",
																		ocg->CreateIdent(
																				temp2[0]->name())),
																ocg->CreateInt(i)))));

//data copy from linked list to new_array_prime

				int inner_loop_size = 1;
				for (int i = 0; i < inner_loop_bounds.size(); i++)
					inner_loop_size *= inner_loop_bounds[i].second
							- inner_loop_bounds[i].first + 1;

				CG_outputRepr * new_data_array_ref_;

				new_data_array_ref_ = ocg->CreateMinus(
						ocg->CreateArrayRefExpression(offset_index2->name(),
								index2->clone()), ocg->CreateIdent(tmp->name()));

				new_data_array_ref_ = ocg->CreateTimes(new_data_array_ref_,
						ocg->CreateInt(inner_loop_size));

				//										  ocg->CreateMinus(NULL, ocg->CreateIdent(IS_dc.set_var(1)->name())),
				//						  ocg->CreateInt(level_coef_));
				CG_outputRepr * temp2_ = NULL;
				//CG_outputRepr *temp2_;
				//0. index expression creation
				std::vector<IR_ScalarSymbol *> tmp2;
				for (int i = 0; i < inner_loop_bounds.size(); i++) {

					IR_ScalarSymbol *tmp = ir->CreateScalarSymbol(IR_CONSTANT_INT,
							0);
					CG_outputRepr *current = ocg->CreateIdent(tmp->name());
					int level_coef = 1;
					for (int j = i + 1; j < inner_loop_bounds.size(); j++)
						level_coef *= inner_loop_bounds[j].second
								- inner_loop_bounds[j].first + 1;

					current = ocg->CreateTimes(ocg->CreateInt(level_coef), current);

					new_data_array_ref_ = ocg->CreatePlus(new_data_array_ref_,
							current->clone());
					if (i == 0)
						temp2_ = current->clone();
					else
						temp2_ = ocg->CreatePlus(temp2_, current->clone());

					//temp2_.push_back(current->clone());
					tmp2.push_back(tmp);

					//	new_data_array_ref_.push_back(current->clone());

				}
				CG_outputRepr *ll_inc_and_free2 = NULL;
				//1. lhs array ref
				std::vector<CG_outputRepr *> lhs;
				std::vector<CG_outputRepr *> rhs;

				for (int k = 0; k < new_array.size(); k++) {
					CG_outputRepr *interm0 = ocg->CreateArrayRefExpression(
							new_array_prime->name(), new_data_array_ref_->clone());
					//;
					//for (int x = 1; x < new_data_array_ref_.size(); x++)
					//	interm0 = ocg->CreateArrayRefExpression(interm0,
					//			new_data_array_ref_[x]->clone());
					lhs.push_back(interm0);

					CG_outputRepr *interm =
							dynamic_cast<CG_roseBuilder *>(ocg)->CreateArrowRefExpression(

							ocg->CreateIdent(temp2[k]->name()),

									dynamic_cast<CG_roseBuilder *>(ocg)->lookup_member_data(
											list_type2[k], data_array[k],
											ocg->CreateIdent(new_array[k])));

					//for (int x = 0; x < temp2_.size(); x++)
					if (temp2_ != NULL)
						interm = ocg->CreateArrayRefExpression(interm,
								temp2_->clone());
					rhs.push_back(interm->clone());
				}
				CG_outputRepr *innerLoop = NULL;
				for (int k = 0; k < data_array.size(); k++)

					innerLoop = ocg->StmtListAppend(innerLoop,
							ocg->CreateAssignment(0, lhs[k], rhs[k]));
				for (int i = tmp2.size() - 1; i >= 0; i--) {
					CG_outputRepr *lb = ocg->CreateInt(inner_loop_bounds[i].first);
					CG_outputRepr *ub = ocg->CreateInt(inner_loop_bounds[i].second);
					CG_outputRepr *index = ocg->CreateIdent(tmp2[i]->name());

					innerLoop = ocg->CreateLoop(0,
							ocg->CreateInductive(index, lb, ub, NULL), innerLoop);

				}
				loop_body = ocg->StmtListAppend(loop_body, innerLoop);
				/*
				 `ocg->CreateArrayRefExpression(new_array[k],
				 ocg->CreateMinus(
				 ocg->CreateArrayRefExpression(offset_index2->name(), index2->clone()),
				 ocg->CreateIdent(tmp->name()))),
				 static_cast<CG_roseBuilder *>(ocg)->CreateArrowRefExpression(ocg->CreateArrayRefExpression(new_array_prime3[k]->name(),
				 ocg->CreateIdent(IS.set_var(level-1)->name())),
				 static_cast<CG_roseBuilder *>(ocg)->lookup_member_data(
				 list_type,data_array[k] ,
				 ocg->CreateIdent(
				 new_array_prime3[k]->name())))));

				 */

				for (int k = 0; k < data_array.size(); k++) {
					free_loop_body =
							ocg->StmtListAppend(free_loop_body,
									ocg->CreateAssignment(0,
											ocg->CreateArrayRefExpression(
													new_array_prime3[k]->name(),
													index2->clone()),
											static_cast<CG_roseBuilder *>(ocg)->CreateArrowRefExpression(
													ocg->CreateArrayRefExpression(
															new_array_prime3[k]->name(),
															index2->clone()),
													static_cast<CG_roseBuilder *>(ocg)->lookup_member_data(
															list_type, "next",
															ocg->CreateIdent(
																	new_array_prime3[k]->name())))));
					loop_body =
							ocg->StmtListAppend(loop_body,
									ocg->CreateAssignment(0,
											ocg->CreateIdent(temp2[k]->name()),

											static_cast<CG_roseBuilder *>(ocg)->CreateArrowRefExpression(
											//ocg->CreateArrayRefExpression(
													ocg->CreateIdent(temp2[k]->name()),
													//		index2->clone()),
													static_cast<CG_roseBuilder *>(ocg)->lookup_member_data(
															list_type, "next",
															ocg->CreateIdent(
																	temp2[k]->name())))));

					free_loop_body = ocg->StmtListAppend(free_loop_body,
							ir->CreateFree(
							//				ocg->CreateArrayRefExpression(
									ocg->CreateIdent(temp2[k]->name())));
					//index2->clone())));

					free_loop_body = ocg->StmtListAppend(free_loop_body,
							ocg->CreateAssignment(0,
									ocg->CreateIdent(temp2[k]->name()),
									ocg->CreateArrayRefExpression(
											new_array_prime3[k]->name(),
											index2->clone())));

				}

				CG_outputRepr *outer_stmt = ocg->CreateAssignment(0,
				//
						ocg->CreateIdent(temp2[0]->name()),
						//index2->clone()),
						ocg->CreateArrayRefExpression(new_array_prime3[0]->name(),
								index2->clone()));

				loop_body = ocg->StmtListAppend(outer_stmt->clone(),
						ocg->CreateLoop(0, loop_header, loop_body));

				free_loop_body = ocg->StmtListAppend(outer_stmt->clone(),
						ocg->CreateLoop(0, free_loop_header, free_loop_body));

				//Hack need to check for loop bounds :Anand : 11/23/2015
				CG_outputRepr *free_loop_nest = free_loop_body;
				for (int i = level - 1; i >= 1; i--) {
					CG_outputRepr *loop_h = ocg->CreateInductive(
							ocg->CreateIdent(IS.set_var(i)->name()), ocg->CreateInt(0),
							ocg->CreateInt(
									extract_loop_trip_count(IS, IS.set_var(i)) - 1),
							NULL);
					free_loop_nest = ocg->CreateLoop(0, loop_h, free_loop_nest);

				}

				cleanup_code = ocg->StmtListAppend(cleanup_code, free_loop_nest);
				loop_body = ocg->StmtListAppend(loop_body,
						ocg->CreatePlusAssignment(0,
								ocg->CreateArrayRefExpression(offset_index2->name(),
										index3->clone()),
								ocg->CreateArrayRefExpression(offset_index2->name(),
										index2->clone())));

				new_cpy_stmts.code = loop_body;
				//lex[0] += 1;
				//shiftLexicalOrder(lex, 0, 1);
				stmt.push_back(new_cpy_stmts);
				uninterpreted_symbols.push_back(uninterpreted_symbols[*it]);
				uninterpreted_symbols_stringrepr.push_back(
						uninterpreted_symbols_stringrepr[*it]);
				unin_rel.push_back(std::map<std::string, std::vector<Relation> >());
				clean_unin_relation(stmt[stmt.size() - 1].IS,
						unin_rel[unin_rel.size() - 1]);
				dep.insert();
				std::vector<int> last_stmt;
				last_stmt = getLexicalOrder(stmt.size() - 1);

				assign_const(stmt[stmt.size() - 1].xform, 0, lex[0]);
				dep.insert();
				/*for (int i = level-1; i >= 1; i--) {
				 CG_outputRepr *loop = output_loop(ir->builder(), IS,
				 i, this->known,
				 std::vector<std::pair<CG_outputRepr *, int> >(
				 IS.n_set(),
				 std::make_pair(static_cast<CG_outputRepr *>(NULL),
				 0)), uninterpreted_symbols[*it]);

				 loop_body = ir->builder()->CreateLoop(0, loop, loop_body);

				 }
				 init_code = ocg->StmtListAppend(init_code, body);

				 */

			}
			Relation known_(stmt[*it].IS.n_set());
			known_.copy_names(copy(stmt[*it].IS.n_set()));
			known_.setup_names();

			F_And *fr = known_.add_and();
			GEQ_Handle g = fr->add_GEQ();

			Variable_ID tmp_fv_;
			if (most_reprs.find(level - 1) == most_reprs.end())

				tmp_fv_ = known_.get_local(cc1);
			else
				tmp_fv_ = known_.get_local(cc1, Input_Tuple);
			g.update_coef(tmp_fv_, 1);
			g.update_const(-1);
			this->addKnown(known_);

		}

	}

	std::vector<LoopLevel> ll4;

	lex[0] += 1;
	shiftLexicalOrder(lex, 0, 1);
	for (std::set<int>::iterator it = same_loop.begin(); it != same_loop.end();
			it++) {
		//apply_xform(*it);
		bool if_in_executor = true;
		std::map<int, Relation> inner_loops;

		std::set<int> remaining_loops;

		Relation copy_is;				// = copy(stmt[*it].IS);
		Relation IS;
		Relation xform;
		std::vector<LoopLevel> loops;
		CG_outputRepr *old_code_;
		if (stmt_num == *it) {
			IS = old_IS;
			xform = old_XFORM;
			copy_is = copy_IS;
			loops = old_loop_level;
			old_code_ = old_code;
		} else {
			IS = stmt[*it].IS;
			xform = stmt[*it].xform;
			copy_is = copy(stmt[*it].IS);
			loops = stmt[*it].loop_level;
			old_code_ = stmt[*it].code;
		}

		int level_coef_ = 1;

		for (int i = 0; i < inner_loop_bounds.size(); i++) {

			level_coef_ *= inner_loop_bounds[i].second - inner_loop_bounds[i].first
					+ 1;
		}
		std::vector<CG_outputRepr *> data_prime_ref;
		std::vector<std::vector<CG_outputRepr*> > data_prime_ref2;
		CG_outputRepr *ref = NULL;
		//data_prime_ref = ocg->CreateTimes(
		//		ocg->CreateIdent(IS.set_var(level)->name()),
		//		ocg->CreateInt(level_coef_));
		//if (!is_single)
		//	data_prime_ref.push_back(
		//			ocg->CreateIdent(IS.set_var(level)->name()));
		//else{
		//AV: 2016 commenting out below
		/*if(level != 1)
		 for (int i = 1; i < level; i++)
		 data_prime_ref.push_back(
		 ocg->CreateIdent(IS.set_var(i)->name()));
		 */
		if (!is_single)
			ref = ocg->CreateTimes(ocg->CreateIdent(IS.set_var(level)->name()),
					ocg->CreateInt(level_coef_));
		else if (level == 2)
			ref = ocg->CreateTimes(ocg->CreateIdent(IS.set_var(level - 1)->name()),
					ocg->CreateInt(level_coef_));
		else
			throw loop_error("Transformation not supported");
		/*
		 for (int i = 1; i < level; i++){
		 CG_outputRepr *index =
		 ocg->CreateIdent(IS.set_var(i)->name());

		 for(int j = i+1; j < level;j++)
		 index = ocg->CreateTimes(index, size_repr_[j-1]->clone());
		 if(inner_loop_bounds.size() > 1){

		 index = ocg->CreateTimes(index, ocg->CreateInt(level_coef_));

		 }

		 ref = ocg->CreatePlus(ref, index);
		 }
		 */
		//}
		CG_outputRepr *temp4 = NULL;
		for (int i = 0; i < inner_loop_bounds.size(); i++) {

			CG_outputRepr *current, *current2;
			/*if(!is_single)
			 */
			current = ocg->CreateIdent(
					IS.set_var(loops_for_non_zero_block_count[i])->name());
			/*else{
			 current=
			 ocg->CreateIdent(
			 stmt[*it].IS.set_var(
			 loops_for_non_zero_block_count[i]-1)->name());
			 */
			current2 = ocg->CreateIdent(
					stmt[*it].IS.set_var(loops_for_non_zero_block_count[i])->name());

			int level_coef = 1;
			for (int j = i + 1; j < inner_loop_bounds.size(); j++)
				level_coef *= inner_loop_bounds[j].second
						- inner_loop_bounds[j].first + 1;

			current2 = ocg->CreateTimes(ocg->CreateInt(level_coef), current2);
			//	data_array_ref_ = ocg->CreatePlus(data_array_ref_, current->clone());
			ref = ocg->CreatePlus(ref, current2->clone());
			//data_prime_ref.push_back(current->clone());
			//if (i == 0)
			//	temp4 = current->clone();
			//else
			//	temp4 = ocg->CreatePlus(temp4, current->clone());
		}
		data_prime_ref.push_back(ref->clone());
		for (int k = 0; k < data_array.size(); k++)
			data_prime_ref2.push_back(data_prime_ref);

		for (int i = 1; i <= loops.size(); i++) {

			if (cancelled_loops.find(i) == cancelled_loops.end()) //&& level2.find(i) == level2.end())
				remaining_loops.insert(i);
			//if (loops[i - 1].made_dense)
			//	break;

		}
		for (int i = level_[level_.size() - 1] + 1; i <= loops.size(); i++) {

			if (loops[i - 1].made_dense)
				break;
			enforce_marked = true;
			Relation bound = get_loop_bound(IS, i, this->known);
			inner_loops.insert(std::pair<int, Relation>(i, bound));
			if (_DEBUG_)
				bound.print();
		}
		for (std::map<int, Relation>::iterator it2 = inner_loops.begin();
				it2 != inner_loops.end(); it2++) {
			std::string index = IS.set_var(it2->first)->name();

			Relation bound = it2->second;
			int max = -1;
			for (DNF_Iterator di(bound.query_DNF()); di; di++)
				for (GEQ_Iterator gi = (*di)->GEQs(); gi; gi++)
					if (((*gi).get_coef(bound.set_var(it2->first)) == -1)
							&& (*gi).is_const(bound.set_var(it2->first)))
						max = (*gi).get_const();

			Relation r(copy_is.n_set());
			r.copy_names(copy_is);
			r.setup_names();

			F_And *root = r.add_and();
			GEQ_Handle h = root->add_GEQ();
			h.update_coef(r.set_var(it2->first), -1);
			h.update_const(max);
			GEQ_Handle g = root->add_GEQ();
			g.update_coef(r.set_var(it2->first), 1);

			copy_is = and_with_relation_and_replace_var(copy_is,
					copy_is.set_var(it2->first), r);

		}

		//15. Create Executor Code
		std::vector<Relation> outer_loop_bounds;
		std::map<int, Relation> neglected;
		std::map<int, Relation> zero_loop_bounds;
		for (int i = 1; i < level; i++) {
			outer_loop_bounds.push_back(get_loop_bound(copy_is, i, this->known));
			if (_DEBUG_)
				get_loop_bound(copy_is, i, this->known).print();
		}

		std::set<int>::iterator it5 = remaining_loops.begin();
		for (; it5 != remaining_loops.end(); it5++)
			if (loops[*it5 - 1].made_dense)
				break;
		for (; it5 != remaining_loops.end(); it5++) {
			if (loops[*it5 - 1].made_dense)
				continue;
			if (level2.find(*it5) == level2.end())
				neglected.insert(
						std::pair<int, Relation>((*it5),
								get_loop_bound(copy_is, (*it5), this->known)));

		}

		for (int i = 0; i < loops_for_non_zero_block_count.size(); i++) {
			if (level2.find(loops_for_non_zero_block_count[i]) == level2.end())
				zero_loop_bounds.insert(
						std::pair<int, Relation>(loops_for_non_zero_block_count[i],
								get_loop_bound(copy_is,
										loops_for_non_zero_block_count[i], this->known)));
			if (_DEBUG_)
				get_loop_bound(copy_is, loops_for_non_zero_block_count[i],
						this->known).print();
		}

		std::string index_name;
		if (level > 1 && !max_allocate)
			index_name = offset_index2->name();
		else
			index_name = "chill_count_1";

		std::pair<Relation, Relation> xform_is_3 = createCSRstyleISandXFORM(ocg,
				outer_loop_bounds, index_name, zero_loop_bounds, neglected,
				uninterpreted_symbols[*it], uninterpreted_symbols_stringrepr[*it],
				unin_rel[*it], this, freevar, is_single);
		/*  for(int l=0; l < unin_rel.size(); l++)
		 for(std::map<std::string, std::vector<omega::Relation > >::const_iterator j = unin_rel[l].begin(); j != unin_rel[l].end(); j++)
		 {
		 std::cout<<j->first<<std::endl;
		 for(int k =0; k < j->second.size(); k++){
		 for (omega::Constr_Vars_Iter cvi(j->second[k]); cvi; cvi++);


		 }
		 }
		 */
		CG_outputRepr *assignment;
		IR_Control *code_;
		if (*it != stmt_num) {
			code_ = ir->GetCode(stmt[*it].code->clone());

		} else
			code_ = code;

		if (guards.size() > 0) {

			assignment = dynamic_cast<IR_If*>(code_)->then_body()->extract();

		} else
			assignment = old_code_;

		bool reduction = CheckForReduction(ir, assignment, zero);

		bool array_involved = false;

		if (reduction) {

			std::vector<IR_ArrayRef *> refs = ir->FindArrayRef(assignment);

			array_involved = checkForUniqueProductInReduction(refs, data_array);

		}
		if_in_executor = !reduction || !array_involved;

		if (inner_loops.size() == 0)
			if_in_executor = false;

		std::vector<IR_ArrayRef *> array_refs = ir->FindArrayRef(assignment);
		std::set<int> pos;
		for (int i = 0; i < array_refs.size(); i++) {
			if (!array_refs[i]->is_write()) {

				for (int j = 0; j < array_refs.size(); j++)
					if (array_refs[j]->is_write()
							&& *array_refs[j] == *array_refs[i]) {
						pos.insert(i);
						break;

					}

			}

		}

		std::map<int, int> replace;

		for (int i = 0; i < array_refs.size(); i++) {
			if (pos.find(i) == pos.end()) {
				for (int i_ = 0; i_ < level_.size(); i_++) {
					std::map<int, IR_ScalarRef *> scalar_replace;
					for (int k = 0; k < new_array.size(); k++) {
						if (array_refs[i]->name() != data_array[k]) {
							for (int j = 0; j < array_refs[i]->n_dim(); j++) {

								std::vector<IR_ScalarRef *> scalar_refs =

								ir->FindScalarRef(array_refs[i]->index(j));
								for (int k_ = 0; k_ < scalar_refs.size(); k_++)

									if (scalar_refs[k_]->name()
											== IS.set_var(level_[i_])->name())
										/*ir->ReplaceExpression(scalar_refs[k],
										 ocg->CreateArrayRefExpression(
										 explicit_index->name(),
										 ocg->CreateIdent(
										 old_IS.set_var(level)->name())));
										 */
										scalar_replace.insert(
												std::pair<int, IR_ScalarRef *>(i_,
														scalar_refs[k_]));

							}

						} /*else {

						 replace.insert(std::pair<int, int>(i, k));
						 //ir->ReplaceExpression(array_refs[i],
						 //		ocg->CreateArrayRefExpression(new_array[k],
						 //				data_prime_ref2[k]));
						 }*/

					}

					for (std::map<int, IR_ScalarRef *>::iterator k_ =
							scalar_replace.begin(); k_ != scalar_replace.end(); k_++)

						ir->ReplaceExpression(k_->second,
								ocg->CreateArrayRefExpression(
										explicit_index[k_->first]->name(),
										ocg->CreateIdent(IS.set_var(level_[0])->name())));
				}
			}
		}
		for (int i = 0; i < array_refs.size(); i++)
			for (int k = 0; k < new_array.size(); k++)
				if (array_refs[i]->name() == data_array[k])
					replace.insert(std::pair<int, int>(i, k));

		for (std::map<int, int>::iterator i = replace.begin(); i != replace.end();
				i++) {

			CG_outputRepr *repl = ocg->CreateArrayRefExpression(
					new_array[i->second], data_prime_ref2[i->second][0]->clone());
			for (int j = 1; j < data_prime_ref2[i->second].size(); j++)
				repl = ocg->CreateArrayRefExpression(repl,
						data_prime_ref2[i->second][j]->clone());

			ir->ReplaceExpression(array_refs[i->first], repl->clone());

		}

		Relation r(xform_is_3.first.n_set());
		F_And *f_root_ = r.add_and();
		std::vector<omega::Free_Var_Decl*> freevars;
		CG_outputRepr *cond;
		CG_outputRepr *repl;
		if (if_in_executor) {

			repl = ocg->CreateArrayRefExpression(new_array[0],
					data_prime_ref2[0][0]->clone());
			for (int j = 1; j < data_prime_ref2[0].size(); j++)
				repl = ocg->CreateArrayRefExpression(repl,
						data_prime_ref2[0][j]->clone());

			cond = ocg->CreateNEQ(repl->clone(), ocg->CreateInt(zero));

			for (int i1 = 1; i1 < new_array.size(); i1++) {

				CG_outputRepr *repl = ocg->CreateArrayRefExpression(new_array[i1],
						data_prime_ref2[0][0]->clone());
				for (int j = 1; j < data_prime_ref2[0].size(); j++)
					repl = ocg->CreateArrayRefExpression(repl,
							data_prime_ref2[0][j]->clone());
				cond = ocg->CreateAnd(cond,
						ocg->CreateNEQ(repl, ocg->CreateInt(zero)));

			}

			//assignment = ocg->CreateIf(0, cond, assignment->clone(), NULL);

		}
		Statement s3;
		F_And *f_root = xform_is_3.first.and_with_and();
		if (stmt_num != *it) {

			if (if_in_executor) {

				CG_stringBuilder *ocgs = new CG_stringBuilder;
				for (int i = 0; i < new_array.size(); i++) {
					bool found = false;
					for (int j = 0; j < freevar.size(); j++)
						if (freevar[j]->base_name() == new_array[i])
							found = true;

					Free_Var_Decl *new_arr;
					if (!found) {
						new_arr = new Free_Var_Decl(new_array[i],
								outer_loop_bounds.size() + inner_loops.size() + 1);

						freevar.push_back(new_arr);

					}
					std::string args = "";
					std::vector<CG_outputRepr *> reprs;
					std::vector<CG_outputRepr *> reprs2;

					int n = xform_is_3.first.n_set();
					omega::Relation mapping(2 * n + 1, n);
					omega::F_And *f_root = mapping.add_and();
					for (int j = 1; j <= n; j++) {
						omega::EQ_Handle h = f_root->add_EQ();
						h.update_coef(mapping.output_var(j), 1);
						h.update_coef(mapping.input_var(2 * j), -1);
					}
					mapping = omega::Composition(mapping, copy(xform_is_3.second));
					mapping.simplify();
					std::vector<omega::Relation> reprs3;
					for (int j = 1;
							j <= outer_loop_bounds.size() + inner_loops.size() + 1;
							j++) {

						reprs.push_back(
								ocg->CreateIdent(xform_is_3.first.set_var(j)->name()));
						reprs2.push_back(
								ocgs->CreateIdent(xform_is_3.first.set_var(j)->name()));

						omega::Relation mapping1(mapping.n_out(), 1);
						omega::F_And *f_root = mapping1.add_and();
						omega::EQ_Handle h = f_root->add_EQ();
						h.update_coef(mapping1.output_var(1), 1);
						h.update_coef(mapping1.input_var(j), -1);
						omega::Relation r = Composition(mapping1, copy(mapping));
						r.simplify();

						omega::Variable_ID v = r.output_var(1);

						std::vector<std::pair<omega::CG_outputRepr *, int> > atof =
								std::vector<std::pair<omega::CG_outputRepr *, int> >(
										xform_is_3.second.n_out(),
										std::make_pair(
												static_cast<omega::CG_outputRepr *>(NULL),
												0));
						std::pair<omega::EQ_Handle, int> result1 =
								find_simplest_assignment(r, v, atof);
						Relation c = copy(r);

						reprs3.push_back(c);

						if (j >= 2)
							args += "," + xform_is_3.first.set_var(j)->name();
						else
							args = xform_is_3.first.set_var(j)->name();
					}

					uninterpreted_symbols[*it].insert(
							std::pair<std::string, std::vector<CG_outputRepr *> >(
									new_array[i], reprs));

					uninterpreted_symbols_stringrepr[*it].insert(
							std::pair<std::string, std::vector<CG_outputRepr *> >(
									new_array[i], reprs2));

					unin_rel[*it].insert(
							std::pair<std::string, std::vector<Relation> >(
									new_array[i], reprs3));

					/*for(int l=0; l < unin_rel.size(); l++)
					 for(std::map<std::string, std::vector<omega::Relation > >::const_iterator j = unin_rel[l].begin(); j != unin_rel[l].end(); j++)
					 {
					 std::cout<<j->first<<std::endl;
					 for(int k =0; k < j->second.size(); k++){
					 for (omega::Constr_Vars_Iter cvi(j->second[k]); cvi; cvi++);


					 }
					 }*/

					if (!found) {
						std::string args2 = "(" + args + ")";
						/*
						 CG_outputRepr *subscript = ocg->CreateArrayRefExpression(index_name,
						 ocg->CreateIdent(
						 IS.set_var(1)->name()));
						 ;
						 for (int j = 2; j <= outer_loop_bounds.size() + inner_loops.size() + 1; j++) {
						 CG_outputRepr *name = ocg->CreateIdent(
						 IS.set_var(j)->name());

						 subscript = ocg->CreateArrayRefExpression(subscript, name->clone());


						 }
						 CG_outputRepr *repr = subscript->clone();
						 */
						ir->CreateDefineMacro(new_array[i], args2, repl->clone());
					}
					std::vector<IR_ArrayRef *> refs = ir->FindArrayRef(cond);

					for (int i = 0; i < refs.size(); i++)
						if (refs[i]->name() == new_array[i])
							ir->ReplaceExpression(refs[i],
									ocg->CreateInvoke(new_array[i], reprs));

				}

				exp2constraint(this, ir, xform_is_3.first, f_root, freevar,
						cond->clone(), true, uninterpreted_symbols[*it],
						uninterpreted_symbols_stringrepr[*it], unin_rel[*it]);
				//else

			}
			stmt[*it].IS = xform_is_3.first;
			stmt[*it].xform = xform_is_3.second;
			stmt[*it].has_inspector = false;
			stmt[*it].ir_stmt_node = NULL;
			stmt[*it].reduction = 0;
			clean_unin_relation(stmt[*it].IS, unin_rel[*it]);

			std::vector<LoopLevel> ll3;
			for (int i = 0; i < level - 1; i++)
				ll3.push_back(loops[i]);

			if (!is_single) {
				LoopLevel tmp_;
				tmp_.payload = level - 1;
				tmp_.type = LoopLevelOriginal;
				tmp_.parallel_level = 0;
				tmp_.segreducible = false;
				ll3.push_back(tmp_);
			}
			for (int i = 0; i < loops_for_non_zero_block_count.size(); i++)
				ll3.push_back(loops[loops_for_non_zero_block_count[i] - 1]);
			if (neglected.size() > 0)
				for (std::map<int, Relation>::iterator j = neglected.begin();
						j != neglected.end(); j++)
					ll3.push_back(loops[j->first - 1]);

			stmt[*it].loop_level = ll3;
			stmt[*it].code = assignment;

			assign_const(stmt[*it].xform, 0, lex[0]);
			assign_const(stmt[*it].xform, stmt[*it].xform.n_out() - 1,
					get_const(xform, xform.n_out() - 1, Output_Var));
			//Anand: Hack to shift lexical Order
			//lex[0] += 1;

		} else {
			//    int new_num = stmt.size();

//	std::vector<int> lex3 = getLexicalOrder(new_num);
			if (if_in_executor) {

				CG_stringBuilder *ocgs = new CG_stringBuilder;
				for (int i = 0; i < new_array.size(); i++) {
					bool found = false;
					for (int j = 0; j < freevar.size(); j++)
						if (freevar[j]->base_name() == new_array[i])
							found = true;

					Free_Var_Decl *new_arr;
					if (!found) {
						new_arr = new Free_Var_Decl(new_array[i],
								outer_loop_bounds.size() + inner_loops.size() + 1);

						freevar.push_back(new_arr);

					}
					std::string args = "";
					std::vector<CG_outputRepr *> reprs;
					std::vector<CG_outputRepr *> reprs2;
					int n = xform_is_3.first.n_set();
					omega::Relation mapping(2 * n + 1, n);
					omega::F_And *f_root = mapping.add_and();
					for (int j = 1; j <= n; j++) {
						omega::EQ_Handle h = f_root->add_EQ();
						h.update_coef(mapping.output_var(j), 1);
						h.update_coef(mapping.input_var(2 * j), -1);
					}
					mapping = omega::Composition(mapping, copy(xform_is_3.second));
					mapping.simplify();
					std::vector<omega::Relation> reprs3;
					for (int j = 1;
							j <= outer_loop_bounds.size() + inner_loops.size() + 1;
							j++) {

						reprs.push_back(
								ocg->CreateIdent(xform_is_3.first.set_var(j)->name()));
						reprs2.push_back(
								ocgs->CreateIdent(xform_is_3.first.set_var(j)->name()));

						omega::Relation mapping1(mapping.n_out(), 1);
						omega::F_And *f_root = mapping1.add_and();
						omega::EQ_Handle h = f_root->add_EQ();
						h.update_coef(mapping1.output_var(1), 1);
						h.update_coef(mapping1.input_var(j), -1);
						omega::Relation r = Composition(mapping1, copy(mapping));
						r.simplify();

						omega::Variable_ID v = r.output_var(1);

						std::vector<std::pair<omega::CG_outputRepr *, int> > atof =
								std::vector<std::pair<omega::CG_outputRepr *, int> >(
										xform_is_3.second.n_out(),
										std::make_pair(
												static_cast<omega::CG_outputRepr *>(NULL),
												0));
						std::pair<omega::EQ_Handle, int> result1 =
								find_simplest_assignment(r, v, atof);
						Relation c = copy(r);

						reprs3.push_back(c);
						if (j >= 2)
							args += "," + xform_is_3.first.set_var(j)->name();
						else
							args = xform_is_3.first.set_var(j)->name();
					}

					uninterpreted_symbols[*it].insert(
							std::pair<std::string, std::vector<CG_outputRepr *> >(
									new_array[i], reprs));

					uninterpreted_symbols_stringrepr[*it].insert(
							std::pair<std::string, std::vector<CG_outputRepr *> >(
									new_array[i], reprs2));
					unin_rel[*it].insert(
							std::pair<std::string, std::vector<Relation> >(
									new_array[i], reprs3));

					if (!found) {
						std::string args2 = "(" + args + ")";
						/*
						 CG_outputRepr *subscript = ocg->CreateArrayRefExpression(index_name,
						 ocg->CreateIdent(
						 IS.set_var(1)->name()));
						 ;
						 for (int j = 2; j <= outer_loop_bounds.size() + inner_loops.size() + 1; j++) {
						 CG_outputRepr *name = ocg->CreateIdent(
						 IS.set_var(j)->name());

						 subscript = ocg->CreateArrayRefExpression(subscript, name->clone());


						 }
						 CG_outputRepr *repr = subscript->clone();
						 */
						ir->CreateDefineMacro(new_array[i], args2, repl->clone());
					}
					std::vector<IR_ArrayRef *> refs = ir->FindArrayRef(cond);

					for (int i = 0; i < refs.size(); i++)
						if (refs[i]->name() == new_array[i])
							ir->ReplaceExpression(refs[i],
									ocg->CreateInvoke(new_array[i], reprs));

				}

				exp2constraint(this, ir, xform_is_3.first, f_root, freevar,
						cond->clone(), true, uninterpreted_symbols[*it],
						uninterpreted_symbols_stringrepr[*it], unin_rel[*it]);
			}
			stmt[*it].IS = xform_is_3.first;
			stmt[*it].xform = xform_is_3.second;
			stmt[*it].has_inspector = false;
			stmt[*it].ir_stmt_node = NULL;
			stmt[*it].reduction = 0;
			clean_unin_relation(stmt[*it].IS, unin_rel[*it]);
			std::vector<LoopLevel> ll3;
			for (int i = 0; i < level - 1; i++)
				ll3.push_back(old_loop_level[i]);

			if (!is_single) {
				LoopLevel tmp_;
				tmp_.payload = level - 1;
				tmp_.type = LoopLevelOriginal;
				tmp_.parallel_level = 0;
				tmp_.segreducible = false;
				ll3.push_back(tmp_);
			}
			for (int i = 0; i < loops_for_non_zero_block_count.size(); i++)
				ll3.push_back(
						old_loop_level[loops_for_non_zero_block_count[i] - 1]);

			if (neglected.size() > 0)
				for (std::map<int, Relation>::iterator j = neglected.begin();
						j != neglected.end(); j++)
					ll3.push_back(loops[j->first - 1]);
			stmt[*it].loop_level = ll3;
			ll4 = ll3;
			stmt[*it].code = assignment;
			//lex[0] += 1;
			//shiftLexicalOrder(lex, 0, 1);
			assign_const(stmt[*it].xform, 0, lex[0]);
			assign_const(stmt[*it].xform, stmt[*it].xform.n_out() - 1,
					get_const(xform, xform.n_out() - 1, Output_Var));
			//
			//stmt.push_back(s3);

			//dep.insert();
		}
	}

	if (level > 1 && !max_allocate)
		cleanup_code = ocg->StmtListAppend(cleanup_code,
				ir->CreateFree(ocg->CreateIdent(offset_index2->name())));

	for (int i = 0; i < explicit_index.size(); i++)
		cleanup_code = ocg->StmtListAppend(cleanup_code,
				ir->CreateFree(ocg->CreateIdent(explicit_index[i]->name())));

	for (int k = 0; k < new_array.size(); k++) {
		cleanup_code = ocg->StmtListAppend(cleanup_code,
				ir->CreateFree(ocg->CreateIdent(new_array[k])));

	}
	if (enforce_marked || max_allocate)
		cleanup_code = ocg->StmtListAppend(cleanup_code,
				ir->CreateFree(ocg->CreateIdent(marked3[0]->name())));
	num_dep_dim = ll4.size();
   //apply_xform();
//lex = getLexicalOrder(stmt_num);

//shiftLexicalOrder(lex, 2 * count_rem - 2, 1);
//stmt.push_back(s);

	/*	apply_xform(stmt_num);

	 IR_CONSTANT_TYPE type;

	 IR_ArrayRef *arr_sym = NULL;
	 IR_PointerArrayRef *ptr_sym = NULL;

	 bool found = false;
	 std::vector<IR_PointerArrayRef *> arrRefs = ir->FindPointerArrayRef(
	 stmt[stmt_num].code);std::map<std::string, int>

	 for (int i = 0; i < arrRefs.size(); i++)
	 if (data_array == arrRefs[i]->name()) {
	 type = arrRefs[i]->symbol()->elem_type();
	 found = true;
	 ptr_sym = arrRefs[i];
	 break;
	 }

	 if (!found) {

	 std::vector<IR_ArrayRef *> arrRefs = ir->FindArrayRef(
	 stmt[stmt_num].code);
	 for (int i = 0; i < arrRefs.size(); i++)
	 if (data_array == arrRefs[i]->name()) {
	 type = arrRefs[i]->symbol()->elem_type();
	 found = true;
	 arr_sym = arrRefs[i];
	 break;
	 }

	 }
	 if (!found)
	 throw loop_error("data array " + data_array + " not found!");
	 if (outer_loop_levels.size() < 2)
	 throw loop_error("Compaction requires at least 2 input loop levels!");

	 if (outer_loop_levels[0] != 1)
	 throw loop_error("loop levels must start from outer most loop level");
	 for (int i = 1; i < outer_loop_levels.size(); i++) {
	 if (outer_loop_levels[i] != outer_loop_levels[i - 1] + 1)
	 throw loop_error(
	 "Input loop levels for compaction must be continuous");
	 if (outer_loop_levels[i] < 0
	 || outer_loop_levels[i] > stmt[stmt_num].loop_level.size())
	 throw loop_error("loop levels out of bounds");
	 }
	 std::vector<int> zero_entity_levels;

	 if (outer_loop_levels[outer_loop_levels.size() - 1]
	 == stmt[stmt_num].loop_level.size())
	 zero_entity_levels.push_back(
	 outer_loop_levels[outer_loop_levels.size() - 1]);
	 else
	 for (int i = outer_loop_levels[outer_loop_levels.size() - 1] + 1;
	 i <= stmt[stmt_num].loop_level.size(); i++)
	 zero_entity_levels.push_back(i);

	 int compressed_level = outer_loop_levels[outer_loop_levels.size() - 1];
	 int row_level = outer_loop_levels[outer_loop_levels.size() - 2];

	 //1. discover footprint of outer loops for declaring reorganized array, declare new data array, col array and index array of appropriate size

	 Relation rows = get_loop_bound(stmt[stmt_num].IS, row_level - 1);
	 Relation column = get_loop_bound(stmt[stmt_num].IS, compressed_level - 1);

	 int nnz;
	 int num_rows;
	 int num_cols;
	 Variable_ID v = rows.set_var(row_level);
	 for (GEQ_Iterator e(const_cast<Relation &>(rows).single_conjunct()->GEQs());
	 e; e++)
	 if ((*e).get_coef(v) < 0)
	 num_rows = (*e).get_const() + 1;

	 Variable_ID v2 = column.set_var(compressed_level);
	 for (GEQ_Iterator e(
	 const_cast<Relation &>(column).single_conjunct()->GEQs()); e; e++)
	 if ((*e).get_coef(v2) < 0)
	 num_cols = (*e).get_const() + 1;

	 nnz = num_rows * num_cols;
	 std::vector<int> outer_loop_bounds;
	 std::vector<int> zero_entity_loop_bounds;
	 int data_size = nnz;
	 int index_size = num_rows + 1;
	 ;
	 for (int i = 0; i < outer_loop_levels.size() - 2; i++) {
	 Relation bound = get_loop_bound(stmt[stmt_num].IS,
	 2 * outer_loop_levels[i] - 1);
	 Variable_ID v = bound.set_var(outer_loop_levels[i]);
	 for (GEQ_Iterator e(
	 const_cast<Relation &>(bound).single_conjunct()->GEQs()); e;
	 e++)
	 if ((*e).get_coef(v) < 0)
	 outer_loop_bounds.push_back((*e).get_const() + 1);

	 data_size *= outer_loop_bounds[i];
	 index_size *= outer_loop_bounds[i];
	 }IR_PointerSymbol *offset_index, *explicit_index, *new_array_prime, *marked;

	 if (zero_entity_levels.size() > 1) {
	 for (int i = 0; i < zero_entity_levels.size(); i++) {
	 Relation bound = get_loop_bound(stmt[stmt_num].IS,
	 zero_entity_levels[i] - 1);
	 Variable_ID v = bound.set_var(zero_entity_levels[i]);
	 for (GEQ_Iterator e(
	 const_cast<Relation &>(bound).single_conjunct()->GEQs()); e;
	 e++)
	 if ((*e).get_coef(v) < 0 && (*e).get_const() > 0) {
	 zero_entity_loop_bounds.push_back((*e).get_const() + 1);
	 break;
	 }
	 data_size *= zero_entity_loop_bounds[i];
	 }
	 } else
	 zero_entity_loop_bounds.push_back(1);

	 int col_size = data_size;

	 CG_roseBuilder *ocg = dynamic_cast<CG_roseBuilder*>(ir->builder());

	 IR_PointerSymbol *new_col;
	 IR_PointerSymbol *new_data;
	 IR_PointerSymbol *new_index;
	 std::vector<CG_outputRepr*> dims1;
	 std::vector<CG_outputRepr*> dims2;
	 std::vector<CG_outputRepr*> dims3;
	 dims1.push_back(ocg->CreateInt(col_size));
	 dims2.push_back(ocg->CreateInt(data_size));
	 dims3.push_back(ocg->CreateInt(index_size));
	 new_col = ir->CreatePointerSymbol(IR_CONSTANT_INT, dims1);
	 new_data = ir->CreatePointerSymbol(type, dims2);
	 new_index = ir->CreatePointerSymbol(IR_CONSTANT_INT, dims3);
	 new_col->set_size(0, ocg->CreateInt(col_size));
	 new_data->set_size(0, ocg->CreateInt(data_size));
	 new_index->set_size(0, ocg->CreateInt(index_size));

	 //2. Loop over outer loops and copy data and generate the inspector loop
	 if (zero_entity_levels.size() > 1) {
	 CG_outputRepr *stmt_1, *stmt_2, *stmt_3, *stmt_4, *stmt_5, *stmt_6;
	 CG_outputRepr *index_access = NULL;

	 //i. bcsr_ind[0] = 0;

	 CG_outputRepr* index_expr = NULL;

	 for (int i = 0; i < outer_loop_levels.size() - 2; i++) {
	 CG_outputRepr *indice = ocg->CreateIdent(
	 stmt[stmt_num].IS.set_var(outer_loop_levels[i])->name());
	 for (int j = i + 1; j < outer_loop_levels.size() - 2; j++)
	 indice = ocg->CreateTimes(ocg->CreateInt(outer_loop_bounds[j]),
	 indice->clone());

	 if (index_expr == NULL)
	 index_expr = indice;
	 else
	 index_expr = ocg->CreatePlus(index_expr->clone(),
	 indice->clone());

	 }

	 if (index_expr == NULL) {
	 index_expr = ocg->CreateInt(0);
	 index_access = ocg->CreateInt(0);
	 } else
	 index_access = index_expr->clone();

	 stmt_1 = ocg->CreateArrayRefExpression(new_index->name(),
	 index_expr->clone());

	 Statement s1 = stmt[stmt_num];

	 s1.xform = copy(stmt[stmt_num].xform);
	 s1.code = ocg->CreateAssignment(0, stmt_1->clone(), ocg->CreateInt(0));
	 for (int i = outer_loop_levels[outer_loop_levels.size() - 2];
	 i <= stmt[stmt_num].loop_level.size(); i++)
	 assign_const(s1.xform, 2 * i - 1, 0);
	 s1.xform.simplify();
	 //stmt.push_back(s1);

	 std::vector<int> lex_ = getLexicalOrder(stmt_num);
	 //ii. bcsr_ind[ii+1] = bcsr_ind[ii];

	 CG_outputRepr *index_access2, *index_access3, *index_expr2,
	 *index_expr3;

	 index_access2 =
	 ocg->CreatePlus(index_access->clone(),
	 ocg->CreateIdent(
	 stmt[stmt_num].IS.set_var(
	 outer_loop_levels[outer_loop_levels.size()
	 - 2])->name()));
	 index_access3 = ocg->CreatePlus(index_access2->clone(),
	 ocg->CreateInt(1));

	 index_expr2 = ocg->CreateArrayRefExpression(new_index->name(),
	 index_access2->clone());

	 index_expr3 = ocg->CreateArrayRefExpression(new_index->name(),
	 index_access3->clone());

	 stmt_2 = ocg->CreateAssignment(0, index_expr3->clone(),
	 index_expr2->clone());

	 Statement s2 = stmt[stmt_num];

	 s2.xform = copy(stmt[stmt_num].xform);
	 s2.code = stmt_2;
	 for (int i = outer_loop_levels[outer_loop_levels.size() - 1];
	 i <= stmt[stmt_num].loop_level.size(); i++)
	 assign_const(s2.xform, 2 * i - 1, 0);

	 assign_const(s2.xform,
	 2 * outer_loop_levels[outer_loop_levels.size() - 2] - 2,
	 lex_[0] + 1);

	 //stmt.push_back(s2);

	 ///iii. notAllzero = 0;

	 CG_outputRepr *zero_counter = ocg->CreateIdent("notAllZero");

	 stmt_3 = ocg->CreateAssignment(0, zero_counter->clone(),
	 ocg->CreateInt(0));

	 Statement s3 = stmt[stmt_num];

	 s3.xform = copy(stmt[stmt_num].xform);
	 //s3.xform = copy(stmt[stmt_num].xform);
	 s3.code = stmt_3;

	 for (int i = outer_loop_levels[outer_loop_levels.size() - 1] + 1;
	 i <= stmt[stmt_num].loop_level.size(); i++)
	 assign_const(s3.xform, 2 * i - 1, 0);

	 assign_const(s3.xform,
	 2 * outer_loop_levels[outer_loop_levels.size() - 2] - 2,
	 lex_[0] + 1);
	 assign_const(s3.xform,
	 2 * outer_loop_levels[outer_loop_levels.size() - 1] - 2,
	 lex_[2 * outer_loop_levels[outer_loop_levels.size() - 1] - 2]
	 + 1);

	 //stmt.push_back(s3);

	 ///iv. 	  if (new_P1[ii*r+i][jj*c+j] != 0) {
	 ///           notallzero = 1;
	 ///           break;IR_PointerSymbol *offset_index, *explicit_index, *new_array_prime, *marked;
	 ///       }

	 CG_outputRepr *cond, *brk, *assn, *list;
	 CG_outputRepr *orig_expr;
	 std::vector<CG_outputRepr *> subscripts;
	 if (arr_sym != NULL) {
	 for (int i = 0; i < arr_sym->n_dim(); i++)
	 subscripts.push_back(arr_sym->index(i));

	 orig_expr = ocg->CreateArrayRefExpression(arr_sym->symbol()->name(),
	 subscripts[0]->clone());

	 for (int i = 1; i < arr_sym->n_dim(); i++)
	 orig_expr = ocg->CreateArrayRefExpression(orig_expr->clone(),
	 subscripts[i]->clone());

	 } else if (ptr_sym != NULL) {
	 for (int i = 0; i < ptr_sym->symbol()->n_dim(); i++)
	 subscripts.push_back(ptr_sym->index(i));

	 orig_expr = ocg->CreateArrayRefExpression(ptr_sym->symbol()->name(),
	 subscripts[0]->clone());

	 for (int i = 1; i < ptr_sym->symbol()->n_dim(); i++)
	 orig_expr = ocg->CreateArrayRefExpression(orig_expr->clone(),
	 subscripts[i]->clone());

	 }

	 cond = ocg->CreateNEQ(orig_expr->clone(), ocg->CreateInt(0));
	 assn = ocg->CreateAssignment(0, zero_counter->clone(),
	 ocg->CreateInt(1));
	 brk = ocg->CreateBreakStatement();

	 list = ocg->StmtListAppend(assn->clone(), brk->clone());

	 stmt_4 = ocg->CreateIf(0, cond->clone(), list->clone(), NULL);

	 Statement s4 = stmt[stmt_num];

	 s4.xform = copy(stmt[stmt_num].xform);
	 s4.code = stmt_4;

	 assign_const(s4.xform,
	 2 * outer_loop_levels[outer_loop_levels.size() - 2] - 2,
	 lex_[0] + 1);
	 assign_const(s4.xform,
	 2 * outer_loop_levels[outer_loop_levels.size() - 1] - 2,
	 lex_[2 * outer_loop_levels[outer_loop_levels.size() - 1] - 2]
	 + 1);
	 assign_const(s4.xform, 2 * zero_entity_levels[0] - 2,
	 lex_[2 * zero_entity_levels[0] - 2] + 1);

	 ///v. 	  if (notAllzero == 1)
	 ///           break;

	 CG_outputRepr *cond2 = ocg->CreateEQ(zero_counter->clone(),
	 ocg->CreateInt(1));
	 stmt_5 = ocg->CreateIf(0, cond2->clone(), brk->clone(), NULL);

	 Statement s5 = stmt[stmt_num];

	 s5.xform = copy(stmt[stmt_num].xform);

	 for (int i = outer_loop_levels[outer_loop_levels.size() - 1] + 3;
	 i <= stmt[stmt_num].loop_level.size(); i++)
	 assign_const(s5.xform, 2 * i - 1, 0);

	 assign_const(s5.xform,
	 2 * outer_loop_levels[outer_loop_levels.size() - 2] - 2,
	 lex_[0] + 1);
	 assign_const(s5.xform,
	 2 * outer_loop_levels[outer_loop_levels.size() - 1] - 2,
	 lex_[2 * outer_loop_levels[outer_loop_levels.size() - 1] - 2]
	 + 1);
	 assign_const(s5.xform, 2 * zero_entity_levels[0] - 2,
	 lex_[2 * zero_entity_levels[0] - 2] + 1);
	 assign_const(s5.xform, 2 * zero_entity_levels[1] - 2,
	 lex_[2 * zero_entity_levels[1] - 2] + 1);

	 if (outer_loop_levels[outer_loop_levels.size() - 1] + 2
	 <= stmt[stmt_num].loop_level.size())
	 assign_const(s5.xform,
	 2 * (outer_loop_levels[outer_loop_levels.size() - 1] + 2)
	 - 1,
	 zero_entity_loop_bounds[zero_entity_levels.size() - 1]);
	 s5.code = stmt_5;

	 //stmt.push_back(s5);

	 ///vi.


	 CG_outputRepr *loop_body, *data_copy, *col_copy, *if_check, *loop1,
	 *loop2, *index_inc, *data_subscript = NULL, *data_subscript2,
	 *data_copy2, *col_copy2;

	 for (int i = 0; i < outer_loop_levels.size() - 2; i++) {
	 CG_outputRepr *indice = ocg->CreateIdent(
	 stmt[stmt_num].IS.set_var(outer_loop_levels[i])->name());
	 for (int j = i + 1; j <= stmt[stmt_num].loop_level.size(); j++)
	 indice = ocg->CreateTimes(ocg->CreateInt(j), indice->clone());

	 if (data_subscript == NULL)
	 data_subscript = indice;
	 else
	 data_subscript = ocg->CreatePlus(data_subscript->clone(),
	 indice->clone());
	 }

	 int zero_entity_size = 1;
	 for (int i = 0; i < zero_entity_loop_bounds.size(); i++)
	 zero_entity_size *= zero_entity_loop_bounds[i];

	 //if (zero_entity_size > 1)
	 //	data_subscript = ocg->CreateTimes(ocg->CreateInt(zero_entity_size),
	 //			index_expr3->clone());

	 if (data_subscript != NULL && zero_entity_size > 1) {

	 data_subscript = ocg->CreatePlus(data_subscript->clone(),
	 ocg->CreateTimes(ocg->CreateInt(zero_entity_size),
	 index_expr3->clone()));
	 data_subscript2 =
	 ocg->CreatePlus(data_subscript->clone(),
	 ocg->CreateTimes(ocg->CreateInt(zero_entity_size),
	 ocg->CreateIdent(
	 stmt[stmt_num].IS.set_var(
	 outer_loop_levels[outer_loop_levels.size()
	 - 1])->name())));

	 } else if (data_subscript != NULL && zero_entity_size == 1) {
	 data_subscript = ocg->CreatePlus(data_subscript->clone(),
	 index_expr3->clone());
	 data_subscript2 = ocg->CreatePlus(data_subscript->clone(),
	 ocg->CreateIdent(
	 stmt[stmt_num].IS.set_var(
	 outer_loop_levels[outer_loop_levels.size()
	 - 1])->name()));
	 } else if (data_subscript == NULL && zero_entity_size > 1) {
	 data_subscript = ocg->CreateTimes(ocg->CreateInt(zero_entity_size),
	 index_expr3->clone());
	 data_subscript2 = ocg->CreateTimes(ocg->CreateInt(zero_entity_size),
	 ocg->CreateIdent(
	 stmt[stmt_num].IS.set_var(
	 outer_loop_levels[outer_loop_levels.size()
	 - 1])->name()));
	 } else if (data_subscript == NULL && zero_entity_size == 1) {
	 data_subscript = index_expr3->clone();
	 data_subscript2 =
	 ocg->CreateIdent(
	 stmt[stmt_num].IS.set_var(
	 outer_loop_levels[outer_loop_levels.size()
	 - 1])->name());
	 }
	 if (zero_entity_levels.size() > 1)
	 for (int i = 0; i < zero_entity_levels.size(); i++) {
	 CG_outputRepr *index =
	 ocg->CreateIdent(
	 stmt[stmt_num].IS.set_var(zero_entity_levels[i])->name());
	 for (int j = i + 1; j < zero_entity_levels.size(); j++)
	 index = ocg->CreateTimes(
	 ocg->CreateInt(zero_entity_loop_bounds[j]),
	 index->clone());

	 data_subscript = ocg->CreatePlus(data_subscript->clone(),
	 index->clone());
	 data_subscript2 = ocg->CreatePlus(data_subscript2->clone(),
	 index->clone());
	 }

	 data_copy = ocg->CreateArrayRefExpression(new_data->name(),
	 data_subscript->clone());
	 data_copy2 = ocg->CreateArrayRefExpression(new_data->name(),
	 data_subscript2->clone());
	 col_copy = ocg->CreateArrayRefExpression(new_col->name(),ool sort_helper_2(std::pair<int, int> i, std::pair<int, int> j) {

	 return (i.second < j.second);
	 */
}

void Loop::ELLify(int stmt_num, std::vector<std::string> arrays_to_pad,
		int pad_to, bool dense_pad, std::string dense_pad_pos_array) {

	apply_xform(stmt_num);

	//Sanity Check if Loop is a double loop

	if (stmt[stmt_num].loop_level.size() != 2)
		throw loop_error("ELLify only works on doubly nested loops for now !");

	//Check that outer loop is normalized

	Relation bound = get_loop_bound(copy(stmt[stmt_num].IS), 1, this->known);
	int dim = 2;
	std::vector<int> lex = getLexicalOrder(stmt_num);
	std::set<int> old_loop = getStatements(lex, dim - 1);
	if (!lowerBoundIsZero(bound, 1))
		for (std::set<int>::iterator it = old_loop.begin();

		it != old_loop.end(); it++)
			normalize(*it, 1);
	//0.If inner loop does not start from zero
	//normalize

	bound = get_loop_bound(copy(stmt[stmt_num].IS), 2, this->known);
	dim = 4;
	if (!lowerBoundIsZero(bound, 2))
		for (std::set<int>::iterator it = old_loop.begin(); it != old_loop.end();
				it++)
			normalize(*it, 2);

//1.do a scalar expand get the original statement and the scalar expanded statements

	std::vector<int> loop_levels;
	loop_levels.push_back(1);
	loop_levels.push_back(2);

	if (dense_pad)
		assert(arrays_to_pad.size() == 2);

	std::string data_array_name;
	std::string pos_array_name;
	int count;
	for (int i = 0; i < arrays_to_pad.size(); i++) {

		if (dense_pad) {
			count = dynamic_cast<IR_roseCode *>(ir)->getPointerCounter();
			if (arrays_to_pad[i] == dense_pad_pos_array)
				pos_array_name = "_P_DATA" + omega::to_string(count);
			else
				data_array_name = "_P_DATA" + omega::to_string(count);
		}
		scalar_expand(stmt_num, loop_levels, arrays_to_pad[i], 0, 0, 1, pad_to);

	}
	std::set<int> new_loop = getStatements(lex, dim - 1);

	//2.distribute the original statement and the scalar expanded statements
	distribute(new_loop, 2);
	distribute(new_loop, 1);
	std::set<int> new_stmts;

	for (std::set<int>::iterator it = new_loop.begin(); it != new_loop.end();
			it++)
		if (old_loop.find(*it) == old_loop.end())
			new_stmts.insert(*it);

	fuse(new_stmts, 1);
	fuse(new_stmts, 2);

	fuse(old_loop, 1);
	fuse(old_loop, 2);

	//3.replace the original statement with executor loop extended to pad_to
	apply_xform();
	std::vector<IR_ArrayRef *> refs_;
	for (std::set<int>::iterator it = old_loop.begin(); it != old_loop.end();
			it++) {

		std::vector<IR_ArrayRef *> refs2;

		refs2 = ir->FindArrayRef(stmt[*it].code);

		for (int i = 0; i < refs2.size(); i++)
			refs_.push_back(refs2[i]);

		Relation R(stmt[*it].IS.n_set());
		F_And *f = R.add_and();

		GEQ_Handle g1 = f->add_GEQ();
		g1.update_coef(R.set_var(2), -1);
		g1.update_const(pad_to - 1);

		GEQ_Handle g2 = f->add_GEQ();
		g2.update_coef(R.set_var(2), 1);

		stmt[*it].IS = and_with_relation_and_replace_var(stmt[*it].IS,
				stmt[*it].IS.set_var(2), R);

		if (ir->QueryExpOperation(stmt[*it].code) != IR_OP_PLUS_ASSIGNMENT)
			throw ir_error("Statement is not a += accumulation statement");

	}

	//4.add to inspector loop to extend to pad_to

	std::set<int> final_stmts = new_stmts;
	std::set<int> finally_created_stmts;
	std::vector<IR_PointerArrayRef *> refs;
	for (std::set<int>::iterator it = new_stmts.begin(); it != new_stmts.end();
			it++) {

		std::vector<IR_PointerArrayRef *> refs2;

		refs2 = ir->FindPointerArrayRef(stmt[*it].code);

		for (int i = 0; i < refs2.size(); i++)
			refs.push_back(refs2[i]);
		Relation R = extract_upper_bound(stmt[*it].IS, stmt[*it].IS.set_var(2));

		R = Complement(R);

		F_And *f_root = R.and_with_and();

		GEQ_Handle g1 = f_root->add_GEQ();
		g1.update_coef(R.set_var(2), -1);
		g1.update_const(pad_to - 1);

		R.simplify();
		Relation new_IS = and_with_relation_and_replace_var(copy(stmt[*it].IS),
				stmt[*it].IS.set_var(2), R);

		Statement s = stmt[*it];
		s.IS = new_IS;
		s.code = stmt[*it].code->clone();

		CG_outputRepr *lhs = ir->GetLHSExpression(s.code);
		s.code = ir->builder()->CreateAssignment(0, lhs->clone(),
				ir->builder()->CreateInt(0));

		stmt.push_back(s);
		uninterpreted_symbols.push_back(uninterpreted_symbols[*it]);
		uninterpreted_symbols_stringrepr.push_back(
				uninterpreted_symbols_stringrepr[*it]);
		dep.insert();
		final_stmts.insert(stmt.size() - 1);
		finally_created_stmts.insert(stmt.size() - 1);

	}
	if (dense_pad) {

		IR_PointerSymbol *data = NULL;
		IR_PointerSymbol *col = NULL;
		for (int i = 0; i < refs.size(); i++) {

			if (refs[i]->name() == data_array_name)
				data = refs[i]->symbol();
			else if (refs[i]->name() == pos_array_name)
				col = refs[i]->symbol();

			if (data != NULL && col != NULL)
				break;
		}

//1. define a permutation based on values of col.. sigma[j] = col[j];
//2. Generate a permutation for j[0,n] in col

/// Design decisions for paper
		/*
		 *  1. Use the col array to route the data array "A"
		 *  2. Do the simplification on _P2 since it is a permutation remove the inner indirection x[col[j]]->x[j]
		 *
		 *
		 *
		 *
		 *
		 *
		 */

//1. Set up statement iteration spaces
		Statement s0 = stmt[*(new_stmts.begin())];
		Statement s1 = stmt[*(new_stmts.begin())];
		std::vector<int> lex0 = getLexicalOrder(*(new_stmts.begin()));
		IR_PointerSymbol *sigma;
		IR_PointerSymbol *new_P1;

// 2. Create Additional array variables
		CG_roseBuilder *ocg = dynamic_cast<CG_roseBuilder*>(ir->builder());

		std::vector<CG_outputRepr *> dims1;
		std::vector<CG_outputRepr *> dims2;

		for (int i = 0; i < data->n_dim(); i++) {
			dims1.push_back(ocg->CreateNullStatement());
			dims2.push_back(ocg->CreateNullStatement());

		}

		sigma = ir->CreatePointerSymbol(col, dims1);
		new_P1 = ir->CreatePointerSymbol(data, dims2);

// 3. Create stmt.code for each statement

		CG_outputRepr *linearized_expr = ocg->CreatePlus(
				ocg->CreateTimes(ocg->CreateIdent(s1.IS.set_var(1)->name()),
						ocg->CreateInt(pad_to)),
				ocg->CreateIdent(s1.IS.set_var(2)->name()));
		CG_outputRepr *stmt_1, *stmt_2, *stmt_3, *stmt_4, *stmt_5;

		CG_outputRepr *array_exp1 = ocg->CreateArrayRefExpression(sigma->name(),
				linearized_expr->clone());

		s0.code = ocg->CreateAssignment(0, array_exp1->clone(),
				ocg->CreateInt(0));

		assign_const(s1.xform, 2, lex0[2] + 1);

		CG_outputRepr *array_exp2 = ocg->CreateArrayRefExpression(col->name(),
				linearized_expr->clone());
		CG_outputRepr *linearized_expr2 = ocg->CreatePlus(
				ocg->CreateTimes(ocg->CreateIdent(s1.IS.set_var(1)->name()),
						ocg->CreateInt(pad_to)), array_exp2->clone());

		CG_outputRepr *array_exp3 = ocg->CreateArrayRefExpression(sigma->name(),
				linearized_expr2->clone());

		CG_outputRepr *array_exp4 = ocg->CreateArrayRefExpression(new_P1->name(),
				linearized_expr2->clone());

		CG_outputRepr *array_exp5 = ocg->CreateArrayRefExpression(data->name(),
				linearized_expr->clone());

		stmt_1 = ocg->CreateAssignment(0, array_exp3->clone(), ocg->CreateInt(1));

		stmt_2 = ocg->CreateAssignment(0, array_exp4->clone(),
				array_exp5->clone());
		CG_outputRepr * if_cond = ocg->CreateNEQ(array_exp2->clone(),
				ocg->CreateInt(0));

		CG_outputRepr *linearized_expr3 = ocg->CreatePlus(
				ocg->CreateTimes(ocg->CreateIdent(s1.IS.set_var(1)->name()),
						ocg->CreateInt(pad_to)), ocg->CreateIdent("t6"));

		CG_outputRepr *array_exp6 = ocg->CreateArrayRefExpression(sigma->name(),
				linearized_expr3->clone());

		stmt_4 = ocg->CreateAssignment(0, array_exp6->clone(), ocg->CreateInt(1));

		CG_outputRepr *array_exp7 = ocg->CreateArrayRefExpression(new_P1->name(),
				linearized_expr3->clone());

		CG_outputRepr *array_exp7a = ocg->CreateArrayRefExpression(new_P1->name(),
				linearized_expr->clone());
		stmt_5 = ocg->CreateAssignment(0, array_exp7->clone(),
				array_exp5->clone());
		CG_outputRepr *stmt_6 = ocg->CreateLoop(0,
				ocg->CreateInductive(ocg->CreateIdent("t6"), ocg->CreateInt(0),
						ocg->CreateInt(pad_to - 1),
						NULL),
				ocg->CreateIf(0,
						ocg->CreateEQ(array_exp6->clone(), ocg->CreateInt(1)),
						ocg->StmtListAppend(ocg->StmtListAppend(stmt_4, stmt_5),
								ocg->CreateBreakStatement()), NULL));

		stmt_3 = ocg->CreateIf(0, if_cond, ocg->StmtListAppend(stmt_1, stmt_2),
				stmt_6);
		s1.code = stmt_3;

		dep.insert();

		std::vector<int> lex2 = getLexicalOrder(*(new_stmts.begin()));

		assign_const(s1.xform, 0, lex2[0] + 1);
		lex2[0] = lex2[0] + 1;
		shiftLexicalOrder(lex2, 0, 1);

		stmt.push_back(s1);
		uninterpreted_symbols.push_back(
				uninterpreted_symbols[*(new_stmts.begin())]);
		uninterpreted_symbols_stringrepr.push_back(
				uninterpreted_symbols_stringrepr[*(new_stmts.begin())]);
		lex2 = getLexicalOrder(stmt.size() - 1);

		std::vector<IR_PointerArrayRef *> refs2;

		refs2 = ir->FindPointerArrayRef(stmt[*(old_loop.begin())].code);
		for (int i = 0; i < refs2.size(); i++)
			if (refs2[i]->name() == data_array_name)
				ir->ReplaceExpression(refs2[i], array_exp7a);

		std::vector<IR_ArrayRef *> refs_;

		refs_ = ir->FindArrayRef(stmt[*(old_loop.begin())].code);

		for (int i = 0; i < refs_.size(); i++)
			for (int j = 0; j < refs_[i]->n_dim(); j++) {
				if (ir->QueryExpOperation(refs_[i]->index(j))
						== IR_OP_ARRAY_VARIABLE) {
					std::vector<IR_ArrayRef *> inner_refs = ir->FindArrayRef(
							refs_[i]->index(j));

					for (int k = 0; k < inner_refs.size(); k++)
						if (inner_refs[k]->name() == pos_array_name) {
							CG_outputRepr *array_exp = ocg->CreateArrayRefExpression(
									refs_[i]->name(),
									ocg->CreateIdent(s1.IS.set_var(2)->name()));
							ir->ReplaceExpression(refs_[i], array_exp);

						}

				}
			}
		/*
		 *
		 * Inspector Code
		 for(i=0; i < n; i++)
		 for(j=0; j < n; j++){
		 if(_P2[i][j] != 0 )
		 sigma[i][j] = _P2[i][j];

		 else
		 sigma[i][j] =  j
		 new_P1[i][sigma[i][j]] = _P1[i][j];

		 }

		 }
		 */

// 4.set up lexical order and dependences
//2.by right you have to compose the array access relation with the iteration and data relations, but for now just plug in the simplified relation
// generate the code here instead of at code gen: intorduce the statement here.: new_P1[i][j]*x[j]
	}
	//	distribute(final_stmts, 2);
	//	fuse(finally_created_stmts, 2);
}

void Loop::make_dense(int stmt_num, int loop_level,
		std::string new_loop_index) {

	std::vector<int> lex = getLexicalOrder(stmt_num);
	int dim = 2 * loop_level - 1;
	std::set<int> same_loop = getStatements(lex, dim - 1);
	apply_xform(same_loop);

	//1. Identify Loop Index
	std::string loop_index = stmt[stmt_num].IS.set_var(loop_level)->name();
	if (_DEBUG_)
		std::cout << loop_index << std::endl;

//2. Identify indirect access via loop index, throw error if multiple such accesses, throw error if index expression not basic

//	std::vector<int> lex = getLexicalOrder(stmt_num);

	std::string inner_arr_name;
	int count = 0;

	for (std::set<int>::iterator it = same_loop.begin(); it != same_loop.end();
			it++) {

		std::vector<IR_ArrayRef *> arr_refs = ir->FindArrayRef(stmt[*it].code);

		for (int i = 0; i < arr_refs.size(); i++) {
			for (int j = 0; j < arr_refs[i]->n_dim(); j++) {

				//if (arr_refs[i]->n_dim() == 1)

				//if (ir->QueryExpOperation(arr_refs[i]->index(j))
				//		== IR_OP_ARRAY_VARIABLE) {
				std::vector<CG_outputRepr *> inner_ref = ir->QueryExpOperand(
						arr_refs[i]->index(j));
				for (int k = 0; k < inner_ref.size(); k++) {
					std::vector<IR_ArrayRef *> inner_arr_refs = ir->FindArrayRef(
							inner_ref[k]);
					for (int l = 0; l < inner_arr_refs.size(); l++) {
						if (inner_arr_refs[l]->n_dim() == 1)
							if (ir->QueryExpOperation(inner_arr_refs[l]->index(0))
									== IR_OP_VARIABLE) {
								std::vector<CG_outputRepr *> var_ref =
										ir->QueryExpOperand(inner_arr_refs[l]->index(0));
								for (int m = 0; m < var_ref.size(); m++) {
									if (dynamic_cast<IR_ScalarRef *>(ir->Repr2Ref(
											var_ref[m]))->name() == loop_index) {
										if (count > 0
												&& inner_arr_refs[l]->name()
														!= inner_arr_name)
											throw loop_error(
													"Multiple arrays with indirect accesses not supported currently");
										count++;
										inner_arr_name = inner_arr_refs[0]->name();

										if (_DEBUG_)
											std::cout << inner_arr_name << std::endl;
									}
								}
							}
					}
				}
			}
		}

	}

	if (count == 0)
		throw loop_error("No indirect accesses found");

	//3. Construct new loop index  with lb and ub.. Setup Iteration Space

	Relation new_bound(1);

	//std::cout << (*it)->name() << std::endl;
	F_And *f_root = new_bound.add_and();
	new_bound.name_set_var(1, new_loop_index);

	GEQ_Handle lower_bound = f_root->add_GEQ();
	lower_bound.update_coef(new_bound.set_var(1), 1);
	Free_Var_Decl *lb = new Free_Var_Decl("lb");
	Variable_ID e = lower_bound.get_local(lb);
	freevar.push_back(lb);
	lower_bound.update_coef(e, -1);

	GEQ_Handle upper_bound = f_root->add_GEQ();
	upper_bound.update_coef(new_bound.set_var(1), -1);
	Free_Var_Decl *ub = new Free_Var_Decl("ub");
	Variable_ID e1 = upper_bound.get_local(ub);
	freevar.push_back(ub);
	upper_bound.update_coef(e1, 1);

	new_bound.setup_names();
	new_bound.simplify();

	for (std::set<int>::iterator it = same_loop.begin(); it != same_loop.end();
			it++) {

		Relation bound_cpy = copy(new_bound);

		Relation new_IS = replicate_IS_and_add_at_pos(stmt[*it].IS, loop_level,
				bound_cpy);
		Relation new_xform(stmt[*it].IS.n_set() + 1, stmt[*it].xform.n_out() + 2);

		F_And *f_root_ = new_xform.add_and();
		for (int j = 1; j <= new_IS.n_set(); j++) {
			omega::EQ_Handle h = f_root_->add_EQ();
			h.update_coef(new_xform.output_var(2 * j), 1);
			h.update_coef(new_xform.input_var(j), -1);
		}
		for (int j = 1; j <= 2 * new_IS.n_set() + 1; j += 2) {
			omega::EQ_Handle h = f_root_->add_EQ();
			h.update_coef(new_xform.output_var(j), 1);
			h.update_const(-lex[j - 1]);
		}

		for (std::map<std::string, std::vector<omega::Relation> >::iterator it2 =
				unin_rel[*it].begin(); it2 != unin_rel[*it].end(); it2++) {

			if (it2->second.size() > 0) {
				std::vector<omega::Relation> reprs_ = it2->second;
				std::vector<omega::Relation> reprs_2;
				for (int k = 0; k < reprs_.size(); k++) {
					omega::Relation output_;

					bool first = true;
					output_ = Extend_Domain(reprs_[k], 1);

					reprs_2.push_back(output_);

				}

				it2->second = reprs_2;

			}
		}

		/*	for (int j = 1; j <= new_IS.n_set(); j++) {
		 new_xform.name_input_var(j, stmt[stmt_num].xform.input_var(j)->name());
		 new_xform.name_output_var(j, stmt[stmt_num].xform.output_var(2*j)->name());
		 new_xform.name_output_var(2*j+1, stmt[stmt_num].xform.output_var(2*j+1)->name());
		 }

		 new_xform.name_input_var(stmt[stmt_num].IS.n_set() + 1, new_IS.set_var(stmt[stmt_num].IS.n_set() + 1)->name());
		 new_xform.name_output_var(2*stmt[stmt_num].IS.n_set()+2, "_t"+omega::to_string(tmp_loop_var_name_counter+1));
		 new_xform.name_output_var(2*stmt[stmt_num].IS.n_set()+3, "_t"+omega::to_string(tmp_loop_var_name_counter+2));
		 stmt[stmt_num].xform = new_xform;ir->Repr2Ref(indir_refs[i]->index(0))->name()
		 */
		stmt[*it].xform = new_xform;
		stmt[*it].IS = new_IS;
		this->known = Extend_Set(copy(this->known),
				stmt[*it].IS.n_set() - this->known.n_set());

		if (_DEBUG_) {
			stmt[*it].xform.print();
			stmt[*it].IS.print();
		}

		std::string loop_index = stmt[*it].IS.set_var(loop_level + 1)->name();
//4. Insert Guard Condition and replace indirect access with new loop index. Setup Code AST.
		CG_outputBuilder *ocg = ir->builder();
		CG_outputRepr *guard = ocg->CreateEQ(ocg->CreateIdent(new_loop_index),
				ocg->CreateArrayRefExpression(inner_arr_name,
						ocg->CreateIdent(loop_index)));
		CG_outputRepr *body = stmt[*it].code->clone();
//	delete stmt[stmt_num].code;

		std::vector<IR_ArrayRef *> indir_refs = ir->FindArrayRef(body);
		for (int i = 0; i < indir_refs.size(); i++)
//	for(int j = 0; j < indir_refs[i]->n_dim(); j++)
			if (indir_refs[i]->name() == inner_arr_name
					&& ir->Repr2Ref(indir_refs[i]->index(0))->name() == loop_index)
				ir->ReplaceExpression(indir_refs[i],
						ocg->CreateIdent(new_loop_index));

		CG_outputRepr * new_code = ocg->CreateIf(0, guard, body, NULL);

		stmt[*it].code = new_code;

		LoopLevel dense_loop;

		dense_loop.type = LoopLevelOriginal;
		dense_loop.payload = loop_level - 1;
		dense_loop.parallel_level = 0;
		dense_loop.segreducible =
				stmt[stmt_num].loop_level[loop_level - 1].segreducible;
		dense_loop.made_dense = false;
		if (dense_loop.segreducible)
			dense_loop.segment_descriptor =
					stmt[*it].loop_level[loop_level - 1].segment_descriptor;

		stmt[*it].loop_level.insert(
				stmt[*it].loop_level.begin() + (loop_level - 1), dense_loop);

		stmt[*it].loop_level[loop_level].payload = loop_level;
		stmt[*it].loop_level[loop_level].made_dense = true;

		for (int inner_loops = loop_level + 1;
				inner_loops < stmt[*it].loop_level.size(); inner_loops++)
			if (stmt[*it].loop_level[inner_loops].type == LoopLevelOriginal)
				stmt[*it].loop_level[inner_loops].payload++;

	}
	num_dep_dim += 1;
	//hack for permute:Anand should be removedc
	int size_ = dep.vertex.size();
	dep = DependenceGraph(num_dep_dim);
	for (int i = 0; i < size_; i++)
		dep.insert();
}

void Loop::reorder_data(int stmt_num, std::string perm_name,
		std::string inv_perm, std::vector<std::string> arrays) {

	std::vector<IR_PointerArrayRef *> refs = ir->FindPointerArrayRef(
			stmt[stmt_num].code);
	bool found = false;
	for (int i = 0; i < permutation_symbols.size(); i++) {
		if (permutation_symbols[i].first == perm_name
				&& permutation_symbols[i].second == inv_perm)
			found = true;
		else if (permutation_symbols[i].second == perm_name
				&& permutation_symbols[i].first == inv_perm)
			found = true;
	}

	if (!found)
		throw loop_error("permutation symbol not found!\n");

	for (int i = 0; i < refs.size(); i++)
		for (int j = 0; j < arrays.size(); j++)
			if (arrays[j] == refs[i]->name()) {
				bool found = false;

				if (refs[i]->n_dim() == 1
						&& ir->QueryExpOperation(refs[i]->index(0))
								== IR_OP_ARRAY_VARIABLE) {
					std::vector<CG_outputRepr *> v = ir->QueryExpOperand(
							refs[i]->index(0));
					if (IR_Ref *a = ir->Repr2Ref(v[0]))
						if (dynamic_cast<IR_PointerArrayRef *>(a)->n_dim() == 1
								&& a->name() == inv_perm) {

							found = true;
							CG_outputRepr *inner =
									dynamic_cast<IR_PointerArrayRef *>(a)->index(0);
							ir->ReplaceExpression(refs[i],
									ir->builder()->CreateArrayRefExpression(
											ir->builder()->CreateIdent(refs[i]->name()),
											inner));
						}
				}

				if (!found) {
					CG_outputRepr * inner = refs[i]->index(0);
					ir->ReplaceExpression(refs[i],
							ir->builder()->CreateArrayRefExpression(
									ir->builder()->CreateIdent(refs[i]->name()),
									ir->builder()->CreateArrayRefExpression(
											ir->builder()->CreateIdent(perm_name),
											inner)));

				}
			}

	for (std::map<std::string, std::vector<CG_outputRepr *> >::iterator i =
			uninterpreted_symbols[stmt_num].begin();
			i != uninterpreted_symbols[stmt_num].end(); i++) {
		for (int k = 0; k < arrays.size(); k++)
			if (i->first.find(arrays[k]) != std::string::npos) {

				std::vector<CG_outputRepr *> args = i->second;

				for (int j = 0; j < args.size(); j++) {

					std::vector<IR_PointerArrayRef *> refs = ir->FindPointerArrayRef(
							args[j]);
					for (int l = 0; l < refs.size(); l++) {
						if (refs[l]->n_dim() == 1 && refs[l]->name() == inv_perm)

							found = true;
						CG_outputRepr *inner = refs[l]->index(0);

						try {
							ir->ReplaceExpression(refs[l], inner);
						} catch (const ir_error &err) {

							args[j] = inner;
							break;
						}
					}
					if (!found) {
						CG_outputRepr * inner = args[j];
						args[j] = ir->builder()->CreateArrayRefExpression(
								ir->builder()->CreateIdent(perm_name), inner);
					}
				}

				/*		std::vector<std::string> loop_vars;
				 std::vector<CG_outputRepr *> subs;
				 //for (int j = 1; j <= stmt[*i].IS.n_set(); j++)
				 loop_vars.push_back(stmt[0].IS.set_var(1)->name());
				 if (ir->QueryExpOperation\args[j])
				 == IR_OP_ARRAY_VARIABLE) {
				 std::vector<CG_outputRepr *> v = ir->QueryExpOperand(
				 args[j]);
				 if (IR_Ref *a = ir->Repr2Ref(v[0]))
				 if (IR_Ref *a = ir->Repr2Ref(v[0]))
				 if (dynamic_cast<IR_PointerArrayRef *>(a)->n_dim()
				 == 1 && a->name() == inv_perm) {

				 found = true;
				 CG_outputRepr *inner =
				 dynamic_cast<IR_PointerArrayRef *>(a)->index(
				 0);
				 args[j] = inner; //ir->ReplaceExpression(refs[i], ir->builder()->CreateArrayRefExpression(ir->builder()->CreateIdent(refs[i]->name()), inner));
				 }
				 }
				 if (!found) {
				 CG_outputRepr * inner = args[j];
				 args[j] = ir->builder()->CreateArrayRefExpression(
				 ir->builder()->CreateIdent(perm_name), inner);
				 }
				 }
				 */
				i->second = args;
			}
	}
}

omega::Relation Loop::parseExpWithWhileToRel(omega::CG_outputRepr *repr,
		omega::Relation &R, int loc) {

	std::vector<IR_Loop *> loops = ir->FindLoops(repr);
	int count = 0;
	for (int i = 0; i < loops.size(); i++)
		count += loops[i]->index().size();

	omega::Relation r(count + R.n_set());
	F_And *f_root = r.add_and();
	std::map<int, int> pos_map;

	for (int i = 1; i <= R.n_set(); i++) {
		pos_map.insert(std::pair<int, int>(i, i));
	}

	for (int i = 1; i <= R.n_set(); i++) {
		r.name_set_var(i, R.set_var(i)->name());
		r = replace_set_var_as_another_set_var(r, R, i, i, pos_map);
	}

	count = 1 + R.n_set();
	f_root = r.and_with_and();
	for (int i = 0; i < loops.size(); i++) {
		for (int j = 0; j < loops[i]->index().size(); j++) {
			r.name_set_var(count, loops[i]->index()[j]->name());
			Variable_ID v = r.set_var(count++);
			CG_outputRepr *lb = loops[i]->lower_bound()[j];

			exp2formula(this, ir, r, f_root, freevar, lb, v, 's', IR_COND_GE, true,
					uninterpreted_symbols[loc],
					uninterpreted_symbols_stringrepr[loc], unin_rel[loc]);
			CG_outputRepr *ub = loops[i]->upper_bound()[j];

			IR_CONDITION_TYPE cond = loops[i]->stop_cond()[j];
			if (cond == IR_COND_LT || cond == IR_COND_LE)
				exp2formula(this, ir, r, f_root, freevar, ub, v, 's', cond, true,
						uninterpreted_symbols[loc],
						uninterpreted_symbols_stringrepr[loc], unin_rel[loc]);

		}

	}

	return r;

}
void EXPECT_EQ(string a, string b) {

	if (a != b) {

		std::cout << "\n\nExpected: " << a;
		std::cout << "\n\nActual:" << b << "\n\n";
	}
}

omega::Relation parseISLStringToOmegaRelation(std::string s,
		const omega::Relation &r1, const omega::Relation &r2,
		std::map<std::string, std::string> &unin,
		std::vector<omega::Free_Var_Decl*> &freevar, IR_Code *ir) {

	// find first occurrence of '[' and ]'

	std::size_t pos, pos2;

	pos = s.find("{");
	pos2 = s.find("}");

	std::string var_string = s.substr(pos + 1, pos2 - pos - 1);
	pos = var_string.find("[");
	pos2 = var_string.find("]");

	var_string = var_string.substr(pos + 1, pos2 - pos - 1);
	char * str = new char[var_string.length() + 1];
	std::strcpy(str, var_string.c_str());

	char *pch;
	pch = strtok(str, " ,");
	std::set<std::string> vars;
	while (pch != NULL) {
		std::string var(pch);
		vars.insert(var);
		pch = strtok(NULL, " ,");

	}
	std::vector<std::string> ordered_vars;
	for (std::set<std::string>::iterator i = vars.begin(); i != vars.end();
			i++) {

		for (int j = 1; j <= const_cast<omega::Relation &>(r1).n_set(); j++) {
			if (const_cast<omega::Relation &>(r1).set_var(j)->name() == *i) {
				ordered_vars.push_back(*i);
				break;
			}
		}

	}

	for (std::set<std::string>::iterator i = vars.begin(); i != vars.end();
			i++) {

		for (int j = 1; j <= const_cast<omega::Relation &>(r2).n_set(); j++) {
			if (const_cast<omega::Relation &>(r2).set_var(j)->name() == *i) {
				ordered_vars.push_back(*i);
				break;
			}
		}

	}

	// tokenize and add variables in order first without p and then with p tokenize ", "
	// look for first occurence of ':' and '}'
	pos = s.find(":");

	std::string constraint_string = s.substr(pos + 1);

	char * constr_str = new char[constraint_string.length() + 1];
	std::strcpy(constr_str, constraint_string.c_str());
	std::vector<std::string> conjuncts;
	pch = strtok(constr_str, "&");
	while (pch != NULL) {
		std::string conjunct(pch);
		conjuncts.push_back(conjunct);
		pch = strtok(NULL, "&");
		//if(pch != NULL)
		//pch = strtok(NULL, "&");
	}

	std::set<std::string> terms;
	Relation R(ordered_vars.size());

	for (int j = 1; j <= ordered_vars.size(); j++)
		R.name_set_var(j, ordered_vars[j - 1]);

	F_And *f_root = R.add_and();

	std::map<std::string, Variable_ID> gathered;
	for (int j = 0; j < conjuncts.size(); j++) {
		std::string conjunct = conjuncts[j];
		bool eq = false;
		bool geq = false;
		if (conjunct.find(">=") == std::string::npos)
			geq = false;
		else
			geq = true;

		if (geq == false)
			if (conjunct.find("=") != std::string::npos)
				eq = true;

		GEQ_Handle h;
		EQ_Handle e;

		if (eq)
			e = f_root->add_EQ();
		if (geq)
			h = f_root->add_GEQ();

		assert(eq || geq);
		char * conj_str = new char[conjunct.length() + 1];
		std::strcpy(conj_str, conjunct.c_str());
		//char *conj_str = conjunct.c_str();
		char *pch2 = strtok(conj_str, ">=<");
		std::string part1(pch2);

		char * first_part = new char[part1.length() + 1];
		std::strcpy(first_part, part1.c_str());
		//	char *first_part = part1.c_str();
		char *pch3 = strtok(first_part, " ");
		char bef = '\0';
		while (pch3 != NULL) {

			if (isdigit(*pch3)) {
				int constant = atoi(pch3);
				if (bef == '-') {
					constant *= (-1);

				}
				if (eq)
					e.update_const(constant);
				if (geq)
					h.update_const(constant);

				bef = '\0';
			} else if (*pch3 == '+') {
				bef = '+';

			} else if (*pch3 == '-') {

				bef = '-';
				if (strlen(pch3) > 1 && isalpha(*(pch3 + 1)))
					pch3 += 1;

			}
			if (isalpha(*pch3)) {

				std::string term(pch3);

				int coef = 1;
				if (bef == '-')
					coef = -1;
				//if (term.find("(") == std::string::npos) {
				bef = '\0';
				bool found_set_var = false;
				for (int k = 0; k < ordered_vars.size(); k++)
					if (ordered_vars[k] == term) {
						found_set_var = true;
						if (eq)
							e.update_coef(R.set_var(k + 1), coef);
						else if (geq)
							h.update_coef(R.set_var(k + 1), coef);
					}

				if (!found_set_var) {
					if (term.find("(") == std::string::npos) {
						bool found2 = false;
						for (int k = 0; k < freevar.size(); k++)
							if (freevar[k]->base_name() == term) {
								Variable_ID v;
								found2 = true;
								if (gathered.find(term) == gathered.end()) {
									v = R.get_local(freevar[k]);
									gathered.insert(
											std::pair<std::string, Variable_ID>(term, v));
								} else
									v = gathered.find(term)->second;
								if (eq)
									e.update_coef(v, coef);
								else if (geq)
									h.update_coef(v, coef);
							}
						if (!found2) {
							Free_Var_Decl *repl = new Free_Var_Decl(term);
							Variable_ID v = R.get_local(repl);
							freevar.push_back(repl);
							gathered.insert(
									std::pair<std::string, Variable_ID>(term, v));
							if (eq)
								e.update_coef(v, coef);
							else if (geq)
								h.update_coef(v, coef);
						}

					} else {

						int brace_count = 0;
						int offset = 0;
						while (term.find("(", offset) != std::string::npos) {
							brace_count++;
							offset = term.find("(", offset);
							offset += 1;
						}
						offset = 0;
						int count = 0;
						while (brace_count > count) {
							int prev = offset;
							offset = term.find(")", offset);
							if (offset != std::string::npos) {
								count++;
								offset += 1;
							} else {
								offset = prev;

								pch3 = strtok(NULL, " ");

								term += std::string(pch3);
							}

						}

						for (std::map<std::string, std::string>::iterator it =
								unin.begin(); it != unin.end(); it++) {
							if (it->second == term) {

								std::string to_comp = it->first.substr(0,
										it->first.find("("));
								bool found = false;
								bool found2 = false;
								for (int k = 0; k < freevar.size(); k++)

									if (freevar[k]->base_name() == to_comp) {
										Variable_ID v;
										if (gathered.find(term) == gathered.end()) {

											std::vector<std::string> var;
											for (int j = 1;
													j
															<= const_cast<omega::Relation &>(r2).n_set();
													j++) {
												if (term.find(
														const_cast<omega::Relation &>(r2).set_var(
																j)->name())
														!= std::string::npos) {
													var.push_back(
															const_cast<omega::Relation &>(r2).set_var(
																	j)->name());
													//ordered_vars.push_back(*i);
													found2 = true;
													break;
												}
											}

											if (found2) {
												int pos = -1;
												for (int j = 1; j <= R.n_set(); j++) {

													if (var[var.size() - 1]
															== R.set_var(j)->name())
														pos = j;
												}
												bool found3 = false;
												Variable_ID v2;
												for (int k1 = 0; k1 < freevar.size();
														k1++) {
													if (freevar[k1]->base_name()
															== to_comp + "p") {
														found3 = true;
														v2 = R.get_local(freevar[k1],
																Input_Tuple);
														break;
													}

												}
												if (found3) {
													if (eq)
														e.update_coef(v2, coef);
													else if (geq)
														h.update_coef(v2, coef);

												} else {
													Free_Var_Decl *repl = new Free_Var_Decl(
															to_comp + "p", pos);
													v2 = R.get_local(repl, Input_Tuple);

													std::string args = "(";

													for (int j = 1; j <= pos; j++) {
														if (j > 1)
															args += ",";
														args += R.set_var(j)->name();

													}
													args += ")";
													CG_outputRepr *rhs = ir->RetrieveMacro(
															to_comp);
													rhs = rhs->clone();

													for (int k = 0; k < var.size(); k++) {

														std::string mirror_var =
																var[k].substr(0,
																		var[k].find("p"));

														CG_outputRepr *exp =
																ir->builder()->CreateIdent(
																		var[k]);
														std::vector<IR_ScalarRef *> refs =
																ir->FindScalarRef(rhs);
														for (int l = 0; l < refs.size();
																l++) {
															if (refs[l]->name() == mirror_var)
																ir->ReplaceExpression(refs[l],
																		exp->clone());

														}
													}

													ir->CreateDefineMacro(to_comp + "p",
															args, rhs);

													freevar.push_back(repl);
													//gathered.insert(
													//		std::pair<std::string, Variable_ID>(
													//				to_comp + "p", v2));]
													if (eq)
														e.update_coef(v2, coef);
													else if (geq)
														h.update_coef(v2, coef);

												}
												break;
											} else {
												v = R.get_local(freevar[k], Input_Tuple);
												gathered.insert(
														std::pair<std::string, Variable_ID>(
																term, v));
											}
										} else
											v = gathered.find(term)->second;
										if (eq)
											e.update_coef(v, coef);
										else if (geq)
											h.update_coef(v, coef);
										found = true;
										break;
									}
								if (found || found2)
									break;
							}
						}
					}
				}

			}
			if (pch3 != NULL)
				pch3 = strtok(NULL, " ");
		}

		//pch = strtok(NULL, "&");
	}
	R.simplify();
// current conjunct  strtok with "&&"
// switch  on  identifier, "=", ">=", number
// add UF logic later..
	return R;

}

bool checkIfEqual(Relation &r, Relation s) {

	std::vector<std::set<std::pair<int, int> > > output_vars;
	std::vector<std::set<std::pair<int, int> > > input_vars;
	std::vector<std::set<std::pair<int, std::string> > > global_vars;
	std::vector<int> constants;
	for (DNF_Iterator di(const_cast<Relation &>(r).query_DNF()); di; di++) {
		for (EQ_Iterator gi((*di)->EQs()); gi; gi++) {
			int constant = (*gi).get_const();
			constants.push_back(constant);
			std::set<std::pair<int, int> > ov;
			std::set<std::pair<int, int> > iv;
			std::set<std::pair<int, std::string> > gv;
			for (Constr_Vars_Iter cvi(*gi); cvi; cvi++) {
				Variable_ID v = cvi.curr_var();

				switch (v->kind()) {
				case Output_Var:

					ov.insert(
							std::pair<int, int>(v->get_position(), cvi.curr_coef()));

					break;
				case Input_Var:

					iv.insert(
							std::pair<int, int>(v->get_position(), cvi.curr_coef()));

					break;
				case Wildcard_Var: {
					throw loop_error(
							"wildcard variable not handled in equality constraint for level set parallelization");
					break;
				}
				case Global_Var: {
					Global_Var_ID g = v->get_global_var();
					gv.insert(
							std::pair<int, std::string>(cvi.curr_coef(),
									g->base_name()));

					break;
				}
				default:
					break;
				}
			}
			output_vars.push_back(ov);
			input_vars.push_back(iv);
			global_vars.push_back(gv);
		}
	}

	std::vector<std::set<std::pair<int, int> > > output_vars2;
	std::vector<std::set<std::pair<int, int> > > input_vars2;
	std::vector<std::set<std::pair<int, std::string> > > global_vars2;
	std::vector<int> constants2;
	for (DNF_Iterator di(const_cast<Relation &>(s).query_DNF()); di; di++) {
		for (EQ_Iterator gi((*di)->EQs()); gi; gi++) {
			int constant = (*gi).get_const();
			constants2.push_back(constant);
			std::set<std::pair<int, int> > ov;
			std::set<std::pair<int, int> > iv;
			std::set<std::pair<int, std::string> > gv;
			for (Constr_Vars_Iter cvi(*gi); cvi; cvi++) {
				Variable_ID v = cvi.curr_var();

				switch (v->kind()) {
				case Output_Var:

					ov.insert(
							std::pair<int, int>(v->get_position(), cvi.curr_coef()));

					break;
				case Input_Var:

					iv.insert(
							std::pair<int, int>(v->get_position(), cvi.curr_coef()));

					break;
				case Wildcard_Var: {
					throw loop_error(
							"wildcard variable not handled ine quality constraint for level set parallelization");
					break;
				}
				case Global_Var: {
					Global_Var_ID g = v->get_global_var();
					gv.insert(
							std::pair<int, std::string>(cvi.curr_coef(),
									g->base_name()));

					break;
				}
				default:
					break;
				}
			}
			output_vars2.push_back(ov);
			input_vars2.push_back(iv);
			global_vars2.push_back(gv);
		}
	}
	if (output_vars.size() != output_vars2.size())
		return false;

	for (int i = 0; i < output_vars.size(); i++) {
		bool found = false;

		for (int k = 0; k < output_vars2.size(); k++)
			if (output_vars2[k] == output_vars[i])
				found = true;

		if (!found)
			return false;
	}

	if (input_vars.size() != input_vars2.size())
		return false;

	for (int i = 0; i < input_vars.size(); i++) {
		bool found = false;

		for (int k = 0; k < input_vars2.size(); k++)
			if (input_vars2[k] == input_vars[i])
				found = true;

		if (!found)
			return false;
	}
	if (constants.size() != constants2.size())
		return false;

	for (int i = 0; i < constants.size(); i++) {
		bool found = false;

		for (int k = 0; k < constants2.size(); k++)
			if (constants[k] == constants[i])
				found = true;

		if (!found)
			return false;
	}
	if (global_vars.size() != global_vars2.size())
		return false;

	for (int i = 0; i < global_vars.size(); i++) {
		bool found = false;

		for (int k = 0; k < global_vars2.size(); k++)
			if (global_vars2[k] == global_vars[i])
				found = true;

		if (!found)
			return false;
	}

	return true;
}

std::map<int, Relation> removeRedundantConstraints(
		std::map<int, Relation> &rels) {

	std::map<int, Relation> to_return;
	for (std::map<int, Relation>::iterator i = rels.begin(); i != rels.end();
			i++) {
		bool found = false;
		;
		if (i->second.is_finalized()) {

			for (std::map<int, Relation>::iterator j = rels.begin();
					j != rels.end(); j++) {
				if (j->first > i->first) {

					found |= checkIfEqual(i->second, j->second);
					found |= checkIfEqual(i->second, j->second);
				}
			}
			if (!found) {
				to_return.insert(std::pair<int, Relation>(i->first, i->second));

			}

		}

		/*	if (rels[i].second.is_finalized()) {


		 for (int j = 0; j < rels.size(); j++) {

		 if (i != j) {
		 found |= checkIfEqual(rels[i].second, rels[j].first);
		 found |= checkIfEqual(rels[i].second, rels[j].second);
		 }
		 }
		 if (!found){
		 to_return.push_back(rels[i]);
		 }
		 }
		 */
	}

	return to_return;

}

std::string get_lb_string(Relation &result, Variable_ID &v2,
		bool range = true) {
	std::string a = "";
	Global_Var_ID uf;

	for (DNF_Iterator di(const_cast<Relation &>(result).query_DNF()); di; di++) {
		for (GEQ_Iterator gi((*di)->GEQs()); gi; gi++) {
			bool found_c = false;
			bool dont_consider = false;
			std::pair<int, int> coefs = std::pair<int, int>(0, 0);
			int constant = (*gi).get_const();
			std::string other_global = "";
			bool found_uf = false;
			for (Constr_Vars_Iter cvi(*gi); cvi; cvi++) {
				Variable_ID v = cvi.curr_var();
				switch (v->kind()) {
				case Input_Var: {
					if (v2->kind() == Input_Var) {
						if (v->name() == v2->name() && cvi.curr_coef() == 1)
							found_c = true;
						else
							dont_consider = true;
					} else {

						dont_consider = true;

					}

					break;
				}

				case Global_Var: {

					Global_Var_ID g = v->get_global_var();
					if (g->arity() > 0) {
						std::string name = g->base_name();
						if (v2->kind() == Global_Var) {
							if (name == v2->get_global_var()->base_name()
									&& cvi.curr_coef() == 1)
								found_c = true;
							else
								dont_consider = true;
						} else {
							if (other_global == "" && cvi.curr_coef() < 0) {
								other_global = name;
								uf = g;
								found_uf = true;
							} else
								dont_consider = true;
						}
					} else {
						if (other_global == "" & cvi.curr_coef() < 0) {
							std::string name = g->base_name();
							other_global = name;
						} else
							dont_consider = true;
					}
					break;
				}
				default:
					dont_consider = true;
					break;
				}
			}
			if (!dont_consider && found_c) {
				if (found_uf) {

					Variable_ID v2 = result.get_local(uf, Input_Tuple);
					return get_lb_string(result, v2, range);

				}

				if (constant == 0 && other_global == "") {
					if (found_c) {
						// a = monotonic3[i].first.substr(0, monotonic3[i].first.find("_"));
						if (range)
							a = std::string("0 <= ") + std::string("j");
						else
							a = std::string("0 <= ") + std::string("i");
					}
				} else if (constant > 0 && other_global == "") {
					if (found_c) {
						ostringstream convert;
						convert << constant;
						//  a = monotonic3[i].first.substr(0, monotonic3[i].first.find("_"));
						if (range)
							a = convert.str() + std::string(" <= ") + std::string("j");
						else
							a = convert.str() + std::string(" <= ") + std::string("i");
					}

				} else if (constant < 0 && other_global == "") {
					if (found_c) {
						ostringstream convert;
						convert << constant;
						//		  a = monotonic3[i].first.substr(0, monotonic3[i].first.find("_"));
						if (range)
							a = convert.str() + std::string(" <= ") + std::string("j");
						else
							a = convert.str() + std::string(" <= ") + std::string("i");
					}

				} else if (other_global != "") {
					if (constant > 0)
						constant *= -1;
					ostringstream convert;
					convert << constant;
					a = other_global + convert.str();
					if (range)
						a = a + std::string(" <= ") + std::string("j");
					else
						a = a + std::string(" <= ") + std::string("i");
				}

				return a;
			}
		}
	}
	return a;
}

std::string get_ub_string(Relation &result, Variable_ID &v2,
		bool range = true) {
	std::string b = "";
	Global_Var_ID uf;

	for (DNF_Iterator di(const_cast<Relation &>(result).query_DNF()); di; di++) {
		for (GEQ_Iterator gi((*di)->GEQs()); gi; gi++) {
			bool found_c = false;
			bool dont_consider = false;
			std::pair<int, int> coefs = std::pair<int, int>(0, 0);
			int constant = (*gi).get_const();
			bool found_uf = false;
			std::string other_global = "";
			for (Constr_Vars_Iter cvi(*gi); cvi; cvi++) {
				Variable_ID v = cvi.curr_var();

				switch (v->kind()) {
				case Input_Var: {
					if (v2->kind() == Input_Var) {
						if (v->name() == v2->name() && cvi.curr_coef() == -1)
							found_c = true;
						else
							dont_consider = true;
					} else
						dont_consider = true;
					break;
				}

				case Global_Var: {

					Global_Var_ID g = v->get_global_var();
					if (g->arity() > 0) {
						std::string name = g->base_name();
						if (v2->kind() == Global_Var) {
							if (name == v2->get_global_var()->base_name()
									&& cvi.curr_coef() == -1)
								found_c = true;
							else
								dont_consider = true;
						} else {
							if (other_global == "" && cvi.curr_coef() > 0) {
								std::string s = g->base_name();
								other_global = s;
								uf = g;
								found_uf = true;
							} else
								dont_consider = true;
						}
					} else {
						if (other_global == "" && cvi.curr_coef() > 0) {
							std::string s = g->base_name();
							other_global = s;
						} else
							dont_consider = true;
					}
					break;
				}
				default:
					dont_consider = true;
					break;
				}
			}

			if (!dont_consider && found_c) {
				if (found_uf) {

					Variable_ID v2 = result.get_local(uf, Input_Tuple);
					return get_ub_string(result, v2, range);

				}
				b = "";

				if (constant < 0) {
					ostringstream convert;
					convert << constant;
					b += convert.str();
				} else {
					ostringstream convert;
					convert << constant;
					b += "+" + convert.str();

				}

				if (constant == 0) {

					if (range)
						b = std::string("j") + std::string(" <=") + other_global;
					else
						b = std::string("i") + std::string(" <=") + other_global;
				} else if (constant == -1) {
					if (range)
						b = std::string("j") + std::string(" < ") + other_global;
					else
						b = std::string("i") + std::string(" < ") + other_global;

				} else {

					if (range)
						b = std::string("j") + std::string(" <=") + other_global;
					else
						b = std::string("i") + std::string(" <=") + other_global;

				}

				return b;
			}
		}

	}
	return b;
}

std::pair<std::vector<std::string>,
		std::pair<
				std::vector<std::pair<bool, std::pair<std::string, std::string> > >,
				std::vector<std::pair<bool, std::pair<std::string, std::string> > > > > determineUFsForIegen(
		Relation &result, IR_Code *ir, std::vector<Free_Var_Decl *> freevar) {

	std::vector<std::pair<bool, std::pair<std::string, std::string> > > to_return;
	std::vector<std::pair<bool, std::pair<std::string, std::string> > > to_return2;
	//outer most function call Eg. rowptr(colidx(k)) would be just rowptr
	//would be stored as rowptr_colidx__ so just parse till first '_' for base UF name
	std::set<std::string> outer_most_ufs2;

	for (DNF_Iterator di(const_cast<Relation &>(result).query_DNF()); di; di++) {
		for (Constraint_Iterator gi((*di)->constraints()); gi; gi++) {
			for (Constr_Vars_Iter cvi(*gi); cvi; cvi++) {
				Variable_ID v = cvi.curr_var();

				switch (v->kind()) {
				case Global_Var: {

					Global_Var_ID g = v->get_global_var();
					if (g->arity() > 0) {
						std::string name = g->base_name();
						char * name_str = new char[name.length() + 1];
						std::strcpy(name_str, name.c_str());
						//char *conj_str = conjunct.c_str();
						char *pch2 = strtok(name_str, "_");
						if (pch2) {
							pch2 = strtok(NULL, "_");
							//if nothing after '_'
							if (!pch2)
								outer_most_ufs2.insert(name);
						}
					}
					break;
				}
				default:
					break;
				}
			}
		}
	}
	std::vector<std::string> outer_most_ufs;
	for (std::set<string>::iterator i = outer_most_ufs2.begin();
			i != outer_most_ufs2.end(); i++) {

		outer_most_ufs.push_back(*i);

	}

	std::vector<std::pair<std::string, std::pair<std::string, std::string> > > monotonic;
	for (int i = 0; i < outer_most_ufs.size(); i++)
		for (int j = i + 1; j < outer_most_ufs.size(); j++) {
			for (DNF_Iterator di(const_cast<Relation &>(result).query_DNF()); di;
					di++) {
				for (GEQ_Iterator gi((*di)->GEQs()); gi; gi++) {
					bool found_c = false;
					bool dont_consider = false;
					std::pair<int, int> coefs = std::pair<int, int>(0, 0);
					int constant = (*gi).get_const();
					for (Constr_Vars_Iter cvi(*gi); cvi; cvi++) {
						Variable_ID v = cvi.curr_var();

						switch (v->kind()) {
						case Global_Var: {

							Global_Var_ID g = v->get_global_var();
							if (g->arity() > 0) {
								std::string name = g->base_name();
								if (name == outer_most_ufs[i])
									coefs.first = cvi.curr_coef();
								else if (name == outer_most_ufs[j])
									coefs.second = cvi.curr_coef();

							} else
								dont_consider = true;

							break;
						}
						default:
							dont_consider = true;
							break;
						}
					}
					if (!dont_consider) {
						if (coefs.first == -1 && coefs.second == 1) {
							if (constant == -1)
								monotonic.push_back(
										std::pair<std::string,
												std::pair<std::string, std::string> >("<",
												std::pair<std::string, std::string>(
														outer_most_ufs[i],
														outer_most_ufs[j])));
							else if (constant == 0)
								monotonic.push_back(
										std::pair<std::string,
												std::pair<std::string, std::string> >("<=",
												std::pair<std::string, std::string>(
														outer_most_ufs[i],
														outer_most_ufs[j])));

						} else if (coefs.first == 1 && coefs.second == -1) {

							if (constant == -1)
								monotonic.push_back(
										std::pair<std::string,
												std::pair<std::string, std::string> >("<",
												std::pair<std::string, std::string>(
														outer_most_ufs[j],
														outer_most_ufs[i])));
							else if (constant == 0)
								monotonic.push_back(
										std::pair<std::string,
												std::pair<std::string, std::string> >("<=",
												std::pair<std::string, std::string>(
														outer_most_ufs[j],
														outer_most_ufs[i])));

						}

					}
				}
			}

		}

	std::set<std::string> monotonic2;
	std::set<std::string> not_monotonic2;
	std::vector<std::pair<std::string, std::string> > monotonic3;
	std::set<std::string> other_constraints;

	for (int i = 0; i < monotonic.size(); i++) {
		std::string first = monotonic[i].second.first;
		std::string second = monotonic[i].second.second;

		std::string a = first.substr(0, first.find("_"));
		std::string b = second.substr(0, second.find("_"));
		if (a == b) {
			CG_outputRepr *rep = ir->RetrieveMacro(first);
			std::vector<IR_PointerArrayRef *> refs = ir->FindPointerArrayRef(rep);
			rep = NULL;
			if (refs.size() > 0) {
				rep = refs[0]->index(0);
			}
			CG_outputRepr *rep2 = ir->RetrieveMacro(second);
			std::vector<IR_PointerArrayRef *> refs2 = ir->FindPointerArrayRef(
					rep2);
			rep2 = NULL;
			if (refs2.size() > 0) {
				rep2 = refs2[0]->index(0);
			}

			if (ir->QueryExpOperation(rep) == IR_OP_VARIABLE
					|| ir->QueryExpOperation(rep) == IR_OP_MULTIPLY) {
				std::string one;
				if (ir->QueryExpOperation(ir->QueryExpOperand(rep)[0])
						== IR_OP_VARIABLE)
					one = ir->Repr2Ref(ir->QueryExpOperand(rep)[0])->name();
				if (ir->QueryExpOperation(ir->QueryExpOperand(rep)[1])
						== IR_OP_VARIABLE)
					one = ir->Repr2Ref(ir->QueryExpOperand(rep)[1])->name();

				if (ir->QueryExpOperation(rep2) == IR_OP_PLUS) {
					std::string two;

					if (ir->QueryExpOperation(ir->QueryExpOperand(rep2)[0])
							== IR_OP_MULTIPLY) {
						CG_outputRepr *tmp = ir->QueryExpOperand(rep2)[0];
						if (ir->QueryExpOperation(ir->QueryExpOperand(tmp)[0])
								== IR_OP_VARIABLE)
							two = ir->Repr2Ref(ir->QueryExpOperand(tmp)[0])->name();
						else
							two = ir->Repr2Ref(ir->QueryExpOperand(tmp)[1])->name();
					} else if (ir->QueryExpOperation(ir->QueryExpOperand(rep)[0])
							== IR_OP_VARIABLE) {
						two = ir->Repr2Ref(ir->QueryExpOperand(rep2)[0])->name();
					}
					if (ir->QueryExpOperation(ir->QueryExpOperand(rep2)[1])
							== IR_OP_MULTIPLY) {
						CG_outputRepr *tmp = ir->QueryExpOperand(rep2)[1];
						if (ir->QueryExpOperation(ir->QueryExpOperand(tmp)[0])
								== IR_OP_VARIABLE)
							two = ir->Repr2Ref(ir->QueryExpOperand(tmp)[0])->name();
						else
							two = ir->Repr2Ref(ir->QueryExpOperand(tmp)[1])->name();
					} else if (ir->QueryExpOperation(ir->QueryExpOperand(rep2)[1])
							== IR_OP_VARIABLE) {
						two = ir->Repr2Ref(ir->QueryExpOperand(rep2)[1])->name();

					}

					if (one == two) {

						if (ir->QueryExpOperation(ir->QueryExpOperand(rep2)[1])
								== IR_OP_CONSTANT) {
							if (static_cast<IR_ConstantRef *>(ir->Repr2Ref(
									ir->QueryExpOperand(rep2)[1]))->integer() == 1) {

								monotonic2.insert(a);
								monotonic3.push_back(
										std::pair<std::string, std::string>(first,
												second));

							}
						} else if (ir->QueryExpOperation(ir->QueryExpOperand(rep2)[0])
								== IR_OP_CONSTANT) {
							if (static_cast<IR_ConstantRef *>(ir->Repr2Ref(
									ir->QueryExpOperand(rep2)[0]))->integer() == 1) {

								monotonic2.insert(a);
								monotonic3.push_back(
										std::pair<std::string, std::string>(first,
												second));
							}
						}
					}
				}
			} else if (ir->QueryExpOperation(rep2) == IR_OP_VARIABLE
					|| ir->QueryExpOperation(rep2) == IR_OP_MULTIPLY) {
				std::string one;
				if (ir->QueryExpOperation(ir->QueryExpOperand(rep2)[0])
						== IR_OP_VARIABLE)
					one = ir->Repr2Ref(ir->QueryExpOperand(rep2)[0])->name();
				if (ir->QueryExpOperation(ir->QueryExpOperand(rep2)[1])
						== IR_OP_VARIABLE)
					one = ir->Repr2Ref(ir->QueryExpOperand(rep2)[1])->name();

				if (ir->QueryExpOperation(rep) == IR_OP_PLUS) {
					std::string two;

					if (ir->QueryExpOperation(ir->QueryExpOperand(rep)[0])
							== IR_OP_MULTIPLY) {
						CG_outputRepr *tmp = ir->QueryExpOperand(rep)[0];
						if (ir->QueryExpOperation(ir->QueryExpOperand(tmp)[0])
								== IR_OP_VARIABLE)
							two = ir->Repr2Ref(ir->QueryExpOperand(tmp)[0])->name();
						else
							two = ir->Repr2Ref(ir->QueryExpOperand(tmp)[1])->name();
					} else if (ir->QueryExpOperation(ir->QueryExpOperand(rep)[0])
							== IR_OP_VARIABLE) {
						two = ir->Repr2Ref(ir->QueryExpOperand(rep)[0])->name();
					}
					if (ir->QueryExpOperation(ir->QueryExpOperand(rep)[1])
							== IR_OP_MULTIPLY) {
						CG_outputRepr *tmp = ir->QueryExpOperand(rep)[1];
						if (ir->QueryExpOperation(ir->QueryExpOperand(tmp)[0])
								== IR_OP_VARIABLE)
							two = ir->Repr2Ref(ir->QueryExpOperand(tmp)[0])->name();
						else
							two = ir->Repr2Ref(ir->QueryExpOperand(tmp)[1])->name();
					} else if (ir->QueryExpOperation(ir->QueryExpOperand(rep)[1])
							== IR_OP_VARIABLE) {
						two = ir->Repr2Ref(ir->QueryExpOperand(rep)[1])->name();

					}

					if (one == two) {

						if (ir->QueryExpOperation(ir->QueryExpOperand(rep)[1])
								== IR_OP_CONSTANT) {
							if (static_cast<IR_ConstantRef *>(ir->Repr2Ref(
									ir->QueryExpOperand(rep)[1]))->integer() == 1) {

								monotonic2.insert(a);
								monotonic3.push_back(
										std::pair<std::string, std::string>(first,
												second));

							}
						} else if (ir->QueryExpOperation(ir->QueryExpOperand(rep)[0])
								== IR_OP_CONSTANT) {
							if (static_cast<IR_ConstantRef *>(ir->Repr2Ref(
									ir->QueryExpOperand(rep)[0]))->integer() == 1) {

								monotonic2.insert(a);
								monotonic3.push_back(
										std::pair<std::string, std::string>(first,
												second));
							}
						}
					}
				}

				/*	if (ir->QueryExpOperation(rep2) == IR_OP_VARIABLE) {
				 std::string one =
				 ir->Repr2Ref(ir->QueryExpOperand(rep2)[0])->name();
				 if (ir->QueryExpOperation(rep) == IR_OP_PLUS) {
				 std::string two =
				 ir->Repr2Ref(ir->QueryExpOperand(rep)[0])->name();
				 if (one == two)
				 if (static_cast<IR_ConstantRef *>(ir->Repr2Ref(
				 ir->QueryExpOperand(rep)[1]))->integer() == 1) {

				 monotonic2.insert(a);
				 monotonic3.push_back(
				 std::pair<std::string, std::string>(second,
				 first));
				 }
				 }
				 }
				 }*/

			}
		}

		else {
			other_constraints.insert(a + " " + monotonic[i].first + " " + b);

		}
	}
	for (int i = 0; i < outer_most_ufs.size(); i++) {
		bool found = false;
		for (int j = 0; j < monotonic3.size(); j++) {
			if (monotonic3[j].first == outer_most_ufs[i]
					|| monotonic3[j].second == outer_most_ufs[i]) {
				found = true;
				break;
			}

		}
		if (!found) {
			//std::string a = outer_most_ufs[i].substr(0,outer_most_ufs[i].find("_") );
			not_monotonic2.insert(outer_most_ufs[i]);

		}

	}

	//range info
	for (std::set<std::string>::iterator i = not_monotonic2.begin();
			i != not_monotonic2.end(); i++) {
		std::string lb, ub;
		Free_Var_Decl *d = 0;
		for (int j = 0; j < freevar.size(); j++)
			if (freevar[j]->base_name() == *i) {
				d = freevar[j];
				break;

			}

		assert(d);
		Variable_ID v = result.get_local(d, Input_Tuple);

		lb = get_lb_string(result, v);
		ub = get_ub_string(result, v);

		to_return.push_back(
				std::pair<bool, std::pair<std::string, std::string> >(false,
						std::pair<std::string, std::string>(
								(*i).substr(0, (*i).find("_")), lb + " &&" + ub)));
		int arity = d->arity();
		Variable_ID var = result.set_var(arity);
		lb = get_lb_string(result, var, false);
		ub = get_ub_string(result, var, false);

		to_return2.push_back(
				std::pair<bool, std::pair<std::string, std::string> >(false,
						std::pair<std::string, std::string>(
								(*i).substr(0, (*i).find("_")), lb + " &&" + ub)));
	}
	//range info
	for (int i = 0; i < monotonic3.size(); i++) {
		Free_Var_Decl *d = 0;
		Free_Var_Decl *e = 0;
		for (int j = 0; j < freevar.size(); j++) {
			if (freevar[j]->base_name() == monotonic3[i].first) {
				d = freevar[j];
				//break;

			} else if (freevar[j]->base_name() == monotonic3[i].second) {
				e = freevar[j];
				//break;

			}
			if (e != NULL && d != NULL)
				break;
		}

		assert(d);
		assert(e);
		Variable_ID v = result.get_local(d, Input_Tuple);
		Variable_ID v2 = result.get_local(e, Input_Tuple);
		std::string a = get_lb_string(result, v);
		std::string b = get_ub_string(result, v2);
		to_return.push_back(
				std::pair<bool, std::pair<std::string, std::string> >(true,
						std::pair<std::string, std::string>(
								monotonic3[i].first.substr(0,
										monotonic3[i].first.find("_")), a + " &&" + b)));

		int arity = d->arity();
		Variable_ID var = result.set_var(arity);
		std::string lb = get_lb_string(result, var, false);
		std::string ub = get_ub_string(result, var, false);
		//  ub = ub + "+ 1";
		ub = ub.replace(ub.find("<"), 2, "<=");

		to_return2.push_back(
				std::pair<bool, std::pair<std::string, std::string> >(true,
						std::pair<std::string, std::string>(
								monotonic3[i].first.substr(0,
										(monotonic3[i].first).find("_")),
								lb + " &&" + ub)));
	}
	//domain
	std::vector<string> other_constraints2;
	for (std::set<string>::iterator i = other_constraints.begin();
			i != other_constraints.end(); i++)
		other_constraints2.push_back(*i);
	return (std::pair<std::vector<std::string>,
			std::pair<
					std::vector<std::pair<bool, std::pair<std::string, std::string> > >,
					std::vector<std::pair<bool, std::pair<std::string, std::string> > > > >(
			other_constraints2,
			std::pair<
					std::vector<std::pair<bool, std::pair<std::string, std::string> > >,
					std::vector<std::pair<bool, std::pair<std::string, std::string> > > >(
					to_return, to_return2)));

}

Relation simple_xform(int lex) {

	Relation xform(1, 3);

	F_And *root2 = xform.add_and();
	EQ_Handle e = root2->add_EQ();
	e.update_coef(xform.input_var(1), 1);
	e.update_coef(xform.output_var(2), -1);
	EQ_Handle e2 = root2->add_EQ();
	e2.update_coef(xform.output_var(3), 1);
	EQ_Handle e3 = root2->add_EQ();
	e3.update_coef(xform.output_var(1), 1);
	e3.update_const(lex);
	return xform;
}

Relation flip_var_exclusive(Relation &r1, Variable_ID &v1) {
	Relation r;
	if (r1.is_set())
		r = Relation(r1.n_set());
	else
		r = Relation(r1.n_inp(), r1.n_out());

	r.copy_names(r1);
	r.setup_names();
	F_Exists *f_exists = r.add_and()->add_exists();
	F_And *f_root = f_exists->add_and();
	std::map<Variable_ID, Variable_ID> exists_mapping;
	bool found = false;
	for (DNF_Iterator di(const_cast<Relation &>(r1).query_DNF()); di; di++) {

		for (GEQ_Iterator gi((*di)->GEQs()); gi; gi++) {
			if ((*gi).get_coef(v1) != 0) {
				GEQ_Handle h = f_root->add_GEQ();
				found = true;
				//&& (*gi).is_const_except_for_global(
				//		const_cast<Relation &>(old_relation).set_var(
				//				old_pos))) {
				bool not_const = true;
				for (Constr_Vars_Iter cvi(*gi); cvi; cvi++) {
					Variable_ID v = cvi.curr_var();
					switch (v->kind()) {
					case Output_Var: {

						if (v->get_position() == v1->get_position())
							h.update_coef(r.output_var(v->get_position()),
									-cvi.curr_coef());
						else
							h.update_coef(r.output_var(v->get_position()),
									cvi.curr_coef());
						/*else if (v->get_position() < old_pos)
						 h.update_coef(r.input_var(v->get_position()),
						 cvi.curr_coef());   //Anand: hack//
						 else
						 not_const = true;
						 */
						//else
						//	throw omega_error(
						//			"relation contains set vars other than that to be replicated!");
						break;

					}
					case Input_Var: {

						if (v->get_position() == v1->get_position())
							h.update_coef(r.input_var(v->get_position()),
									-cvi.curr_coef());
						else
							h.update_coef(r.input_var(v->get_position()),
									cvi.curr_coef());
						/*else if (v->get_position() < old_pos)
						 h.update_coef(r.input_var(v->get_position()),
						 cvi.curr_coef());   //Anand: hack//
						 else
						 not_const = true;
						 */
						//else
						//	throw omega_error(
						//			"relation contains set vars other than that to be replicated!");
						break;

					}

					case Wildcard_Var: {
						Variable_ID v2 = replicate_floor_definition(r1, v, r,
								f_exists, f_root, exists_mapping);
						h.update_coef(v2, cvi.curr_coef());
						break;
					}
					case Global_Var: {
						Global_Var_ID g = v->get_global_var();
						Variable_ID v2;
						if (g->arity() == 0)
							v2 = r.get_local(g);
						else
							v2 = r.get_local(g, v->function_of());
						h.update_coef(v2, cvi.curr_coef());
						break;
					}
					default:
						assert(false);
					}
				}
				h.update_const((*gi).get_const());
			}
		}
		for (EQ_Iterator ei((*di)->EQs()); ei; ei++) {
			if ((*ei).get_coef(v1) != 0) {
				found = true;
				EQ_Handle h = f_root->add_EQ();

				for (Constr_Vars_Iter cvi(*ei); cvi; cvi++) {
					Variable_ID v = cvi.curr_var();
					switch (v->kind()) {

					case Output_Var: {

						if (v->get_position() == v1->get_position())
							h.update_coef(r.output_var(v->get_position()),
									-cvi.curr_coef());
						else
							h.update_coef(r.output_var(v->get_position()),
									cvi.curr_coef());
						/*else if (v->get_position() < old_pos)
						 h.update_coef(r.input_var(v->get_position()),
						 cvi.curr_coef());   //Anand: hack//
						 else
						 not_const = true;
						 */
						//else
						//	throw omega_error(
						//			"relation contains set vars other than that to be replicated!");
						break;

					}
					case Input_Var: {

						if (v->get_position() == v1->get_position())
							h.update_coef(r.input_var(v->get_position()),
									-cvi.curr_coef());
						else
							h.update_coef(r.input_var(v->get_position()),
									cvi.curr_coef());
						//	else
						//		throw omega_error(
						//				"relation contains set vars other than that to be replicated!");
						break;

					}
					case Wildcard_Var: {
						Variable_ID v2 = replicate_floor_definition(r1, v, r,
								f_exists, f_root, exists_mapping);
						h.update_coef(v2, cvi.curr_coef());
						break;
					}
					case Global_Var: {
						Global_Var_ID g = v->get_global_var();
						Variable_ID v2;
						if (g->arity() == 0)
							v2 = r.get_local(g);
						else
							v2 = r.get_local(g, v->function_of());
						h.update_coef(v2, cvi.curr_coef());
						break;
					}
					default:
						assert(false);
					}
				}
				h.update_const((*ei).get_const());
			}

		}
	}

	r.simplify();
	if (found)
		return r;
	return r1;
}

Relation flip_var(Relation &r1, Variable_ID &v1) {
	Relation r;
	if (r1.is_set())
		r = Relation(r1.n_set());
	else
		r = Relation(r1.n_inp(), r1.n_out());

	r.copy_names(r1);
	r.setup_names();
	F_Exists *f_exists = r.add_and()->add_exists();
	F_And *f_root = f_exists->add_and();
	std::map<Variable_ID, Variable_ID> exists_mapping;

	for (DNF_Iterator di(const_cast<Relation &>(r1).query_DNF()); di; di++) {
		for (GEQ_Iterator gi((*di)->GEQs()); gi; gi++) {
			GEQ_Handle h = f_root->add_GEQ();

			//&& (*gi).is_const_except_for_global(
			//		const_cast<Relation &>(old_relation).set_var(
			//				old_pos))) {
			bool not_const = true;
			for (Constr_Vars_Iter cvi(*gi); cvi; cvi++) {
				Variable_ID v = cvi.curr_var();
				switch (v->kind()) {
				case Output_Var: {

					if (v->get_position() == v1->get_position())
						h.update_coef(r.output_var(v->get_position()),
								-cvi.curr_coef());
					else
						h.update_coef(r.output_var(v->get_position()),
								cvi.curr_coef());
					/*else if (v->get_position() < old_pos)
					 h.update_coef(r.input_var(v->get_position()),
					 cvi.curr_coef());   //Anand: hack//
					 else
					 not_const = true;
					 */
					//else
					//	throw omega_error(
					//			"relation contains set vars other than that to be replicated!");
					break;

				}
				case Input_Var: {

					if (v->get_position() == v1->get_position())
						h.update_coef(r.input_var(v->get_position()),
								-cvi.curr_coef());
					else
						h.update_coef(r.input_var(v->get_position()),
								cvi.curr_coef());
					/*else if (v->get_position() < old_pos)
					 h.update_coef(r.input_var(v->get_position()),
					 cvi.curr_coef());   //Anand: hack//
					 else
					 not_const = true;
					 */
					//else
					//	throw omega_error(
					//			"relation contains set vars other than that to be replicated!");
					break;

				}

				case Wildcard_Var: {
					Variable_ID v2 = replicate_floor_definition(r1, v, r, f_exists,
							f_root, exists_mapping);
					h.update_coef(v2, cvi.curr_coef());
					break;
				}
				case Global_Var: {
					Global_Var_ID g = v->get_global_var();
					Variable_ID v2;
					if (g->arity() == 0)
						v2 = r.get_local(g);
					else
						v2 = r.get_local(g, v->function_of());
					h.update_coef(v2, cvi.curr_coef());
					break;
				}
				default:
					assert(false);
				}
			}
			h.update_const((*gi).get_const());

		}
		for (EQ_Iterator ei((*di)->EQs()); ei; ei++) {
			EQ_Handle h = f_root->add_EQ();

			for (Constr_Vars_Iter cvi(*ei); cvi; cvi++) {
				Variable_ID v = cvi.curr_var();
				switch (v->kind()) {

				case Output_Var: {

					if (v->get_position() == v1->get_position())
						h.update_coef(r.output_var(v->get_position()),
								-cvi.curr_coef());
					else
						h.update_coef(r.output_var(v->get_position()),
								cvi.curr_coef());
					/*else if (v->get_position() < old_pos)
					 h.update_coef(r.input_var(v->get_position()),
					 cvi.curr_coef());   //Anand: hack//
					 else
					 not_const = true;
					 */
					//else
					//	throw omega_error(
					//			"relation contains set vars other than that to be replicated!");
					break;

				}
				case Input_Var: {

					if (v->get_position() == v1->get_position())
						h.update_coef(r.input_var(v->get_position()),
								-cvi.curr_coef());
					else
						h.update_coef(r.input_var(v->get_position()),
								cvi.curr_coef());
					//	else
					//		throw omega_error(
					//				"relation contains set vars other than that to be replicated!");
					break;

				}
				case Wildcard_Var: {
					Variable_ID v2 = replicate_floor_definition(r1, v, r, f_exists,
							f_root, exists_mapping);
					h.update_coef(v2, cvi.curr_coef());
					break;
				}
				case Global_Var: {
					Global_Var_ID g = v->get_global_var();
					Variable_ID v2;
					if (g->arity() == 0)
						v2 = r.get_local(g);
					else
						v2 = r.get_local(g, v->function_of());
					h.update_coef(v2, cvi.curr_coef());
					break;
				}
				default:
					assert(false);
				}
			}
			h.update_const((*ei).get_const());
		}

	}

	r.simplify();
	return r;

}

Statement construct_statement(int stmt_num, Relation IS, Relation xform,
		CG_outputRepr *code,
		std::vector<std::map<std::string, std::vector<omega::CG_outputRepr *> > > &uninterpreted_symbols,
		std::vector<std::map<std::string, std::vector<omega::CG_outputRepr *> > > &uninterpreted_symbols_stringrepr,
		std::vector<std::map<std::string, std::vector<omega::Relation> > > & unin_rel,
		DependenceGraph &dep) {

	Statement s1;
	//Relation IS(1);
	//Relation xform(1,3);

	/*F_And *root = IS.add_and();
	 //IS = replace_set_var_as_another_set_var(IS, copy(stmt[stmt_num].IS), 1,1);
	 F_And *root2 = xform.add_and();
	 EQ_Handle e = root2->add_EQ();
	 e.update_coef(xform.input_var(1), 1);
	 e.update_coef(xform.output_var(2), -1);
	 EQ_Handle e2 = root2->add_EQ();
	 e.update_coef(xform.input_var(3), 1);
	 EQ_Handle e3 = root2->add_EQ();
	 e.update_coef(xform.output_var(1), 1);
	 e.update_coef(xform.output_var(1), -lex[0]-1);
	 */

	s1.xform = xform;
	s1.IS = IS;

	s1.code = code;
	std::vector<LoopLevel> loops;
	for (int i = 0; i < IS.n_set(); i++) {
		LoopLevel ll;
		ll.made_dense = false;
		ll.parallel_level = 0;
		ll.payload = i + 1;
		ll.type = LoopLevelOriginal;
		ll.segment_descriptor = "";
		ll.segreducible = false;
		loops.push_back(ll);
	}
	s1.loop_level = loops;
	s1.has_inspector = true;
	s1.ir_stmt_node = NULL;
	s1.reduction = 0;
	s1.reductionOp = IR_OP_UNKNOWN;
//			new_stmt_num = stmt.size();
//			stmt.push_back(s1);

	std::map<std::string, std::vector<CG_outputRepr *> > new_unin;

	for (std::map<std::string, std::vector<CG_outputRepr *> >::iterator it =
			uninterpreted_symbols[stmt_num].begin();
			it != uninterpreted_symbols[stmt_num].end(); it++) {
		std::vector<CG_outputRepr *> a;

		for (int i = 0; i < it->second.size(); i++)
			a.push_back(it->second[i]->clone());

		new_unin.insert(
				std::pair<std::string, std::vector<CG_outputRepr *> >(it->first,
						a));

	}

	uninterpreted_symbols.push_back(new_unin);
	uninterpreted_symbols_stringrepr.push_back(
			uninterpreted_symbols_stringrepr[stmt_num]);
	unin_rel.push_back(unin_rel[stmt_num]);
	dep.insert();

	return s1;

}

void Loop::reorder_by_inspector(int stmt_num, int level, std::string perm,
		std::string dep, std::string dep_graph_constructor_name,
		std::string perm_func_name) {

	if (level != 1)
		throw loop_error(
				"Sparse partition only supports outer most level for now!");

//Construct dependence Relation for IeGenLib
        std::set<std::string> ufs_before;
        std::set<std::string> ufs_after;


//construct Relation from, Iteration space
	Relation R = stmt[stmt_num].IS;

        //for (std::map<std::string, std::vector<CG_outputRepr *> >::iterator it =
       //                                               uninterpreted_symbols[stmt_num].begin();  it != uninterpreted_symbols[stmt_num].end(); it++)
         //                                                      ufs_before.insert(it->first);



	Relation r = parseExpWithWhileToRel(stmt[stmt_num].code, R, stmt_num);
	r.simplify();
        //   for (std::map<std::string, std::vector<CG_outputRepr *> >::iterator it =
        //                                              uninterpreted_symbols[stmt_num].begin();  it != uninterpreted_symbols[stmt_num].end(); it++)
          //                                                     ufs_after.insert(it->first);

	Relation result;
	if (known.n_set() < r.n_set()) {
		omega::Relation known = omega::Extend_Set(omega::copy(this->known),
				r.n_set() - this->known.n_set());

		result = omega::Intersection(copy(known), copy(r));

	} else {

		Relation tmp = omega::Extend_Set(omega::copy(r),
				this->known.n_set() - r.n_set());

		result = omega::Intersection(tmp, copy(known));

	}
	result.simplify();
//r.print();

	bool is_negative = false;

	Variable_ID v2 = r.set_var(1);
	//need to correct following logic
	for (DNF_Iterator di(const_cast<Relation &>(r).query_DNF()); di; di++)
		for (GEQ_Iterator gi((*di)->GEQs()); gi; gi++)
			if ((*gi).is_const(v2))
				if ((*gi).get_coef(v2) < 0 && (*gi).get_const() == 0)
					is_negative = true;

	if (is_negative) {
		r = flip_var(r, v2);	//convert to positive_IS
		Variable_ID v3 = result.set_var(1);
		result = flip_var(result, v3);
	}

  /*                   if (is_negative) {
                                         Variable_ID v1, v2;

                                       if(ufs_before.size() != ufs_after.size()){
                                          for(std::set<std::string>::iterator it = ufs_after.begin(); it != ufs_after.end();it++)
                                                if (ufs_before.find(*it) == ufs_before.end()){
                                                   std::vector<CG_outputRepr *> vec = uninterpreted_symbols[stmt_num].find(*it)->second;

                                                   CG_outputRepr *repl = ir->builder()->CreateIdent(R.input_var(1)->name());
                                                   repl = ir->builder()->CreateMinus(NULL, repl);
                                                   std::vector<std::string>tmp2;
                                                   tmp2.push_back(R.input_var(1)->name());
                                                   std::vector<CG_outputRepr *> reverse_expr;
                                                   reverse_expr.push_back(repl->clone());
                                                   vec[0] =  ir->builder()->CreateSubstitutedStmt(0,
                                                                      vec[0]->clone(), tmp2, reverse_expr);

                                                   uninterpreted_symbols[stmt_num].find(*it)->second = vec;

                                                }

                                       }

                                }
*/



	std::pair<std::vector<std::string>,
			std::pair<
					std::vector<std::pair<bool, std::pair<std::string, std::string> > >,
					std::vector<std::pair<bool, std::pair<std::string, std::string> > > > > syms =
			determineUFsForIegen(result, ir, freevar);

	std::vector<int> lex_ = getLexicalOrder(stmt_num);
	std::set<int> same_loop_ = getStatements(lex_, level - 1);
	std::vector<IR_ArrayRef *> access;
//get the equality, and_with_EQ,

	for (std::set<int>::iterator i = same_loop_.begin(); i != same_loop_.end();
			i++) {

		std::vector<IR_ArrayRef *> access2 = ir->FindArrayRef(stmt[*i].code,
				false);
		for (int j = 0; j < access2.size(); j++)
			access.push_back(access2[j]);
	}

	std::vector<std::pair<omega::Relation, omega::Relation> > dep_relation_;
	std::map<int, Relation> rels;
	std::vector<std::pair<Relation, Relation> > rels2;
	for (int i = 0; i < access.size(); i++) {
		IR_ArrayRef *a = access[i];

		IR_ArraySymbol *sym_a = a->symbol();

		for (int j = i + 1; j < access.size(); j++) {
			IR_ArrayRef *b = access[j];
			IR_ArraySymbol *sym_b = b->symbol();

			if (*sym_a == *sym_b && (a->is_write() || b->is_write())) {

				Relation r1(r.n_set(), r.n_set());

				for (int i = 1; i <= r.n_set(); i++)
					r1.name_input_var(i, r.set_var(i)->name());

				for (int i = 1; i <= r.n_set(); i++)
					r1.name_output_var(i, r.set_var(i)->name() + "p");

				IR_Symbol *sym_src;	// = a->symbol();
				IR_Symbol *sym_dst;	// = b->symbol();

				if (!a->is_write()) {
					sym_src = a->symbol();
					sym_dst = b->symbol();

				} else {
					sym_src = b->symbol();
					sym_dst = a->symbol();
				}

				if (*sym_src != *sym_dst) {
					r1.add_or(); // False Relation
					delete sym_src;
					delete sym_dst;
					//return r;
				} else {
					delete sym_src;
					delete sym_dst;
				}

				F_And *f_root = r1.add_and();

				for (int i = 0; i < a->n_dim(); i++) {
					F_Exists *f_exists = f_root->add_exists();
					Variable_ID e1 = f_exists->declare(tmp_e());
					Variable_ID e2 = f_exists->declare(tmp_e());
					F_And *f_and = f_exists->add_and();

					CG_outputRepr *repr_src; // = a->index(i);
					CG_outputRepr *repr_dst; // = b->index(i);
					if (!a->is_write()) {
						repr_src = a->index(i);
						repr_dst = b->index(i);

					} else {
						repr_src = b->index(i);
						repr_dst = a->index(i);
					}
					bool has_complex_formula = false;

					if (ir->QueryExpOperation(repr_src) == IR_OP_ARRAY_VARIABLE
							|| ir->QueryExpOperation(repr_dst) == IR_OP_ARRAY_VARIABLE)
						; //has_complex_formula = true;

					//if (!has_complex_formula) {
                          //               std::set<std::string> ufs_before;
                          //              std::set<std::string> ufs_after;  
					try {
                      			  // std::set<std::string> ufs_before;
                                         //  std::set<std::string> ufs_after;
	
                                           //     for (std::map<std::string, std::vector<CG_outputRepr *> >::iterator it =
                                           //           uninterpreted_symbols[stmt_num].begin();  it != uninterpreted_symbols[stmt_num].end(); it++) 
                                             //                  ufs_before.insert(it->first);          


                                 		exp2formula(this, ir, r1, f_and, freevar, repr_src, e1, 'w',
								IR_COND_EQ, false, uninterpreted_symbols[stmt_num],
								uninterpreted_symbols_stringrepr[stmt_num],
								unin_rel[stmt_num]);
						exp2formula(this, ir, r1, f_and, freevar, repr_dst, e2, 'r',
								IR_COND_EQ, false, uninterpreted_symbols[stmt_num],
								uninterpreted_symbols_stringrepr[stmt_num],
								unin_rel[stmt_num]);

                                                 //for (std::map<std::string, std::vector<CG_outputRepr *> >::iterator it =
                                                  //    uninterpreted_symbols[stmt_num].begin();  it != uninterpreted_symbols[stmt_num].end(); it++)
                                                    //           ufs_after.insert(it->first);
/*
                     if (is_negative) {
                                         Variable_ID v1, v2;

                                       if(ufs_before.size() != ufs_after.size()){
                                          for(std::set<std::string>::iterator it = ufs_after.begin(); it != ufs_after.end();it++)
                                                if (ufs_before.find(*it) == ufs_before.end()){
                                                   std::vector<CG_outputRepr *> vec = uninterpreted_symbols[stmt_num].find(*it)->second;

                                                   CG_outputRepr *repl = ir->builder()->CreateIdent(r1.input_var(1)->name());
                                                   repl = ir->builder()->CreateMinus(NULL, repl);
                                                   std::vector<std::string>tmp2;
                                                   tmp2.push_back(r1.input_var(1)->name());
                                                   std::vector<CG_outputRepr *> reverse_expr;
                                                   reverse_expr.push_back(repl->clone());
                                                   vec[0] =  ir->builder()->CreateSubstitutedStmt(0,
                                                                      vec[0]->clone(), tmp2, reverse_expr);

                                                   uninterpreted_symbols[stmt_num].find(*it)->second = vec;

                                                }

                                       }

                                }
*/
					} catch (const ir_exp_error &e) {
						has_complex_formula = true;
					}

					if (!has_complex_formula) {
						EQ_Handle h = f_and->add_EQ();
						h.update_coef(e1, 1);
						h.update_coef(e2, -1);
					}
					//}
					repr_src->clear();
					repr_dst->clear();
					delete repr_src;
					delete repr_dst;
				}
				r1.simplify();
				if (is_negative) {
					Variable_ID v1, v2;

					v1 = r1.input_var(1);
					r1 = flip_var_exclusive(r1, v1);
					v2 = r1.output_var(1);
					r1 = flip_var_exclusive(r1, v2);

				}

				rels.insert(
						std::pair<int, Relation>(dep_relation_.size(), copy(r1)));
				dep_relation_.push_back(
						std::pair<omega::Relation, omega::Relation>(r, r1));

			}
		}
	}

// Call arrays2Relation

	for (int i = 0; i < dep_relation_.size(); i++) {
		Relation t(dep_relation_[i].first.n_set());
		for (int j = 1; j <= t.n_set(); j++)
			t.name_set_var(j, dep_relation_[i].first.set_var(j)->name() + "p");
		F_And *f_root = t.add_and();

		std::map<int, int> pos_map;
		for (int j = 1; j <= t.n_set(); j++) {
			pos_map.insert(std::pair<int, int>(i, i));

		}

		for (int j = 1; j <= t.n_set(); j++) {

			t = replace_set_var_as_another_set_var(t, dep_relation_[i].first, j, j,
					pos_map);

		}
		t.simplify();

		std::string initial = "{[";

		for (int j = 1; j <= dep_relation_[i].first.n_set(); j++) {
			if (j > 1)
				initial += ",";

			initial += dep_relation_[i].first.set_var(j)->name();
			initial += ",";
			initial += dep_relation_[i].first.set_var(j)->name() + "p";
		}

		initial += "]: ";

		std::string s1 = t.set_var(1)->name() + " <  "
				+ dep_relation_[i].first.set_var(1)->name();
		std::string s2 = t.set_var(1)->name() + " >  "
				+ dep_relation_[i].first.set_var(1)->name();

		Relation s_ = copy(dep_relation_[i].first);
		Relation t_ = copy(dep_relation_[i].second);
		/*for (DNF_Iterator di(const_cast<Relation &>(this->known).query_DNF());
		 di; di++) {
		 for (GEQ_Iterator e((*di)->GEQs()); e; e++) {
		 s_.and_with_GEQ(*e);
		 t.and_with_GEQ(*e);

		 }
		 }*/

		std::set<std::string> global_vars = get_global_vars(s_);
		std::set<std::string> global_vars2 = get_global_vars(t);

		std::string start = "[";

		for (std::set<std::string>::iterator j = global_vars.begin();
				j != global_vars.end(); j++) {
			if (j != global_vars.begin())
				start += ",";
			start += *j;

		}

		std::string start2 = "[";

		for (std::set<std::string>::iterator j = global_vars2.begin();
				j != global_vars2.end(); j++) {
			if (j != global_vars2.begin())
				start2 += ",";
			start2 += *j;

		}
		start += "] ->";
		start2 += "] ->";
		std::string s = " && " + print_to_iegen_string(s_);
		s += " && " + print_to_iegen_string(t);
		s += " && " + print_to_iegen_string(t_);
//std::cout<<s<<std::endl<<std::endl;
		for (std::map<std::string, std::string>::iterator a =
				unin_symbol_for_iegen.begin(); a != unin_symbol_for_iegen.end();
				a++) {
			std::size_t found = s.find(a->first);
			while (found != std::string::npos) {
				if (s.substr(found - 1, 1) != "_")
					s.replace(found, a->first.length(), a->second);

				found = s.find(a->first, found + 1);

			}
		}
		s1 = start + initial + s1 + s + "}";
		s2 = start2 + initial + s2 + s + "}";
		rels2.push_back(std::pair<Relation, Relation>(s_, t));
		std::cout << s1 << std::endl;
		std::cout << s2 << std::endl;
		std::cout << std::endl;
		dep_rel_for_iegen.push_back(std::pair<std::string, std::string>(s1, s2));
	}

	std::map<int, omega::Relation> for_codegen;
	std::map<int, omega::Relation> for_codegen2;
	int count = 0;
	rels = removeRedundantConstraints(rels);

	/*std::string try_ =
	 "[ m, nnz ] -> { [i, ip, k] : ip - colidx(k) = 0 && colidx(k) >= 0 && rowptr(i) >= 0 && rowptr(ip) >= 0 && rowptr(colidx(k)) >= 0  && k - rowptr(i) >= 0 && nnz - diagptr(colidx(k) + 1) >= 0  && nnz - rowptr(colidx(k) + 1) >= 0 && diagptr(i + 1) - rowptr(i + 1) >= 0 && diagptr(ip + 1) - rowptr(ip + 1) >= 0 && diagptr(colidx(k)) - rowptr(colidx(k)) >= 0 && diagptr(colidx(k) + 1) + 1 >= 0 && rowptr(colidx(k) + 1) + 1 >= 0  && -i + m - 2 >= 0 && i - colidx(k) - 1 >= 0 && -k + diagptr(i) - 1 >= 0 && -k + rowptr(i + 1) - 2 >= 0 && nnz - diagptr(i) - 1 >= 0 && nnz - diagptr(i + 1) - 1 >= 0 && nnz - diagptr(ip) - 1 >= 0 && nnz - diagptr(ip + 1) - 1 >= 0 && -diagptr(colidx(k)) + rowptr(ip + 1) - 2 >= 0 && -diagptr(colidx(k)) + rowptr(colidx(k) + 1) + 2 >= 0 && diagptr(colidx(k) + 1) - rowptr(colidx(k) + 1) + 8 >= 0}";
	 Relation a = parseISLStringToOmegaRelation(try_, rels2[0].first,
	 rels2[0].second, unin_symbol_for_iegen, freevar, ir);
	 std::cout << "First Result\n\n";
	 copy(a).print();
	 */

	//get IS intersect Known
	// check for domain and range info"
	// if rowptr_ < rowptr__  add monotonic info
	//
	for (int j = 0; j < dep_rel_for_iegen.size(); j++) {

		if (rels.find(j) != rels.end()) {

			iegenlib::setCurrEnv();
			for (int k = 0; k < syms.second.first.size(); k++) {
				std::string symbol = syms.second.first[k].second.first;
				std::string range_ = syms.second.first[k].second.second;
				std::string domain_ = syms.second.second[k].second.second;

				if (syms.second.first[k].first == true) {
					iegenlib::appendCurrEnv(symbol,
							new iegenlib::Set("{[i]:" + domain_ + "}"), // Domain
							new iegenlib::Set("{[j]:" + range_ + "}"),  // Range
							false,                              // Bijective?!
							iegenlib::Monotonic_Increasing       // monotonicity
							);

				} else {
					iegenlib::appendCurrEnv(symbol,
							new iegenlib::Set("{[i]:" + domain_ + "}"), // Domain
							new iegenlib::Set("{[j]:" + range_ + "}"),  // Range
							false,                              // Bijective?!
							iegenlib::Monotonic_NONE            // monotonicity
							);

				}

			}

			;

			/*		iegenlib::appendCurrEnv("colidx",
			 new iegenlib::Set("{[i]:0<=i &&i<nnz}"), // Domain
			 new iegenlib::Set("{[j]:0<=j &&j<m}"),        // Range
			 false,                              // Bijective?!
			 iegenlib::Monotonic_NONE            // monotonicity
			 );
			 iegenlib::appendCurrEnv("rowptr",
			 new iegenlib::Set("{[i]:0<=i &&i<=m}"),
			 new iegenlib::Set("{[j]:0<=j &&j<nnz}"), false,
			 iegenlib::Monotonic_Increasing);
			 iegenlib::appendCurrEnv("diagptr",
			 new iegenlib::Set("{[i]:0<=i &&i<=m}"),
			 new iegenlib::Set("{[j]:0<=j &&j<nnz}"), false,
			 iegenlib::Monotonic_Increasing);
			 */
// (2)
// Putting constraints in an iegenlib::Set
// Data access dependence from ILU CSR code
			iegenlib::Set *A1 = new iegenlib::Set(dep_rel_for_iegen[j].first);

// (3)
// How to add user defined constraint
			//iegenlib::Set* A1_extend;
			std::cout << dep_rel_for_iegen[j].first << std::endl;
			for (int i = 0; i < syms.first.size(); i++) {
				//		cout << syms.first[i] << endl;
				std::string comparator_str;
				if (syms.first[i].find("<=") != std::string::npos) {
					comparator_str = "<=";
				} else if (syms.first[i].find("<") != std::string::npos)
					comparator_str = "<";
				else
					assert(false);

				char *copy_str = new char[syms.first[i].length() + 1];
				;

				strcpy(copy_str, syms.first[i].c_str());
				//	char *first_part = part1.c_str();
				char *pch3 = strtok(copy_str, " ><=");
				std::string token1(pch3);

				pch3 = strtok(NULL, " ><=");
				std::string token2(pch3);
				iegenlib::Set *tmp = A1->addUFConstraints(token1, comparator_str,
						token2);
				delete A1;
				A1 = tmp;
			}
// (4)
// Specify loops that are going to be parallelized, so we are not going to
// project them out. Here "i" and "ip"
			std::set<int> parallelTvs;
			parallelTvs.insert(0);
			parallelTvs.insert(1);

// expected output  (for testing purposes)
//std::string ex_A1_str("Not Satisfiable");
// (5)
// Simplifyng the constraints set

			iegenlib::Set* A1_sim = A1->simplifyForPartialParallel(parallelTvs);

// (6)
// Print out results
// If set is not satisfiable simplifyForPartialParallel is going to return
// NULL, we should check this before getting result's string with
// toISLString.
			std::string A1_sim_str("Not Satisfiable");

			omega::Relation r, r2;
			//r = Relation::False(1);
			//r2 = Relation::False(1);
			if (A1_sim) {
				A1_sim_str = A1_sim->toISLString();
				r = parseISLStringToOmegaRelation(A1_sim_str, rels2[j].first,
						rels2[j].second, unin_symbol_for_iegen, freevar, ir);
				std::cout << "Result\n\n";
				copy(r).print();

				for_codegen.insert(std::pair<int, Relation>(count++, r));
			}
			std::cout << "\n\nA1 simplified = " << A1_sim_str << "\n\n";

			iegenlib::Set *A2 = new iegenlib::Set(dep_rel_for_iegen[j].second);
			for (int i = 0; i < syms.first.size(); i++) {
				//cout << syms.first[i] << endl;
				std::string comparator_str;
				if (syms.first[i].find("<=") != std::string::npos) {
					comparator_str = "<=";
				} else if (syms.first[i].find("<") != std::string::npos)
					comparator_str = "<";
				else
					assert(false);

				char *copy_str = new char[syms.first[i].length() + 1];
				;

				strcpy(copy_str, syms.first[i].c_str());
				//	char *first_part = part1.c_str();
				char *pch3 = strtok(copy_str, " ><=");
				std::string token1(pch3);

				pch3 = strtok(NULL, " ><=");
				std::string token2(pch3);

				iegenlib::Set *tmp = A2->addUFConstraints(token1, comparator_str,
						token2);
				delete A2;
				A2 = tmp;
			}
// (3)
// How to add user defined constraint
			//	iegenlib::Set* A2_extend = A2->addUFConstraints("rowptr", "<=",
			//			"diagptr");

// (4)
// Specify loops that are going to be parallelized, so we are not going to
// project them out. Here "i" and "ip"
			std::set<int> parallelTvs2;
			parallelTvs2.insert(0);
			parallelTvs2.insert(1);

// expected output  (for testing purposes)
//std::string ex_A1_str("Not Satisfiable");
// (5)
// Simplifyng the constraints set

			iegenlib::Set* A2_sim = A2->simplifyForPartialParallel(parallelTvs2);

// (6)
// Print out results
// If set is not satisfiable simplifyForPartialParallel is going to return
// NULL, we should check this before getting result's string with
// toISLString.
			std::string A2_sim_str("Not Satisfiable");
			if (A2_sim) {
				A2_sim_str = A2_sim->toISLString();
				r2 = parseISLStringToOmegaRelation(A2_sim_str, rels2[j].first,
						rels2[j].second, unin_symbol_for_iegen, freevar, ir);
				std::cout << "Result\n\n";
				copy(r2).print();

				for_codegen2.insert(std::pair<int, Relation>(count, r2));
			}

			std::cout << "\n\nA2 simplified = " << A2_sim_str << "\n\n";
// For testing purposes
//EXPECT_EQ(ex_A1_str, A1_sim_str);
			delete A1;
			delete A2;
			//delete A1_extend;
			delete A1_sim;
			//delete A2_extend;
			delete A2_sim;
		}
	}
	std::map<int, Relation> codegen_rel = removeRedundantConstraints(
			for_codegen);
	for_codegen = codegen_rel;

	codegen_rel = removeRedundantConstraints(for_codegen2);
	for_codegen2 = codegen_rel;
	std::vector<int> lex0 = getLexicalOrder(stmt_num);
	std::set<int> same_loop = getStatements(lex0, level - 1);

	CG_outputBuilder *ocg = ir->builder();

//1. Statement that constructs Dependence graph
	Statement s1, s2;

	std::vector<CG_outputRepr *> args;
//std::string dep = "A";
	std::string freevar_name;
	for (std::map<std::string, std::string>::iterator a =
			unin_symbol_for_iegen.begin(); a != unin_symbol_for_iegen.end(); a++)
		if (a->second.find("colidx(") != std::string::npos) {

			freevar_name = a->first.substr(0, a->first.find("("));
			break;
		}
	Free_Var_Decl *t = NULL;
	for (int i = 0; i < freevar.size(); i++) {
		if (freevar[i]->base_name() == freevar_name) {
			t = freevar[i];
			break;
		}

	}
	assert(t);

	CG_outputRepr *vertex_count = NULL;

	for (DNF_Iterator di(const_cast<Relation &>(stmt[stmt_num].IS).query_DNF());
			di; di++)
		for (GEQ_Iterator gi((*di)->GEQs()); gi; gi++)
			if ((*gi).is_const_except_for_global(v2))
				if ((*gi).get_coef(v2) < 0) {

					for (Constr_Vars_Iter cvi(*gi); cvi; cvi++) {
						Variable_ID v = cvi.curr_var();

						switch (v->kind()) {
						case Global_Var: {

							Global_Var_ID g = v->get_global_var();
							assert(g->arity() == 0);
							vertex_count = ocg->CreatePlus(vertex_count,
									ocg->CreateIdent(g->base_name()));

							break;

						}
						default:
							break;
						}

					}
					if ((*gi).get_const() == 0)
						vertex_count = ocg->CreatePlus(vertex_count,
								ocg->CreateInt(1));
					else if ((*gi).get_const() < -1)
						vertex_count = ocg->CreateMinus(vertex_count,
								ocg->CreateInt(-(*gi).get_const() - 1));
					else if ((*gi).get_const() >= 1)

						vertex_count = ocg->CreatePlus(vertex_count,
								ocg->CreateInt(1 + (*gi).get_const()));

					break;

				}

	assert(vertex_count);

	if (is_negative) {
		vertex_count = NULL;
		for (DNF_Iterator di(const_cast<Relation &>(r).query_DNF()); di; di++)
			for (GEQ_Iterator gi((*di)->GEQs()); gi; gi++)
				if ((*gi).is_const_except_for_global(v2))
					if ((*gi).get_coef(v2) < 0) {

						for (Constr_Vars_Iter cvi(*gi); cvi; cvi++) {
							Variable_ID v = cvi.curr_var();

							switch (v->kind()) {
							case Global_Var: {

								Global_Var_ID g = v->get_global_var();
								assert(g->arity() == 0);
								vertex_count = ocg->CreatePlus(vertex_count,
										ocg->CreateIdent(g->base_name()));

								break;

							}
							default:
								break;
							}

						}
						if ((*gi).get_const() == 0)
							vertex_count = ocg->CreatePlus(vertex_count,
									ocg->CreateInt(1));
						else if ((*gi).get_const() < -1)
							vertex_count = ocg->CreateMinus(vertex_count,
									ocg->CreateInt(-(*gi).get_const() - 1));
						else if ((*gi).get_const() >= 1)

							vertex_count = ocg->CreatePlus(vertex_count,
									ocg->CreateInt(1 + (*gi).get_const()));

						break;

					}
	}

	wfdg = new wavefront_intel_dep_graph(this->ir, this->ir->builder(), dep,
			vertex_count, this, inspection_code());
	wfsc = new wavefront_intel_schedule(this->ir, this->ir->builder(),
			"schedule", is_negative);
	/*	Relation IS(3), IS_(3), IS__(3);
	 IS.name_set_var(1, "i");
	 IS.name_set_var(2, "k");
	 IS.name_set_var(3, "i'");

	 IS_.name_set_var(1, "i");
	 IS_.name_set_var(2, "k");
	 IS_.name_set_var(3, "i'");

	 IS__.name_set_var(1, "i");
	 IS__.name_set_var(2, "k");
	 IS__.name_set_var(3, "i'");

	 IS.get_local(t, Input_Tuple);

	 Variable_ID v = IS_.get_local(t, Input_Tuple);
	 Variable_ID v1 = IS__.get_local(t, Input_Tuple);

	 F_And *f_root = IS.add_and();
	 F_And *f_root_ = IS_.add_and();
	 F_And *f_root__ = IS__.add_and();

	 EQ_Handle e = f_root_->add_EQ();
	 EQ_Handle e1 = f_root__->add_EQ();

	 GEQ_Handle g = f_root_->add_GEQ();
	 GEQ_Handle g1 = f_root__->add_GEQ();

	 if (!is_negative) {
	 e.update_coef(IS_.set_var(3), 1);
	 e.update_coef(v, -1);

	 e1.update_coef(IS__.set_var(3), 1);
	 e1.update_coef(v1, -1);

	 g.update_coef(IS_.set_var(1), 1);
	 g.update_coef(IS_.set_var(3), -1);
	 g.update_const(-1);

	 g1.update_coef(IS__.set_var(3), 1);
	 g1.update_coef(IS__.set_var(1), -1);
	 g1.update_const(-1);
	 } else {

	 e.update_coef(IS_.set_var(3), 1);
	 e.update_coef(v, 1);

	 e1.update_coef(IS__.set_var(3), 1);
	 e1.update_coef(v1, 1);

	 g.update_coef(IS_.set_var(1), -1);
	 g.update_coef(IS_.set_var(3), 1);
	 g.update_const(-1);

	 g1.update_coef(IS__.set_var(3), -1);
	 g1.update_coef(IS__.set_var(1), 1);
	 g1.update_const(-1);

	 }

	 IS_.simplify();
	 for (int i = 1; i <= 2; i++) {

	 IS = replace_set_var_as_another_set_var(IS, stmt[stmt_num].IS, i, i);
	 }

	 IS = replace_set_var_as_another_set_var(IS, stmt[stmt_num].IS, 3, 1);

	 IS.simplify();
	 Relation IS2 = copy(IS);
	 IS.and_with_EQ(e);
	 IS.and_with_GEQ(g);
	 IS.print();

	 IS2.simplify();
	 IS__.simplify();
	 IS2.and_with_EQ(e1);
	 IS2.and_with_GEQ(g1);
	 IS2.print();


	 args.push_back(ocg->CreateIdent(dep));
	 if (is_negative) {
	 args.push_back(
	 ocg->CreateMinus(NULL,
	 ocg->CreateIdent(
	 stmt[stmt_num].IS.set_var(1)->name() + "p")));
	 args.push_back(
	 ocg->CreateMinus(NULL,
	 ocg->CreateIdent(stmt[stmt_num].IS.set_var(1)->name())));
	 } else {
	 args.push_back(
	 //	ocg->CreateMinus(NULL,
	 ocg->CreateIdent(stmt[stmt_num].IS.set_var(1)->name() + "p"));
	 args.push_back(
	 //ocg->CreateMinus(NULL,
	 ocg->CreateIdent(stmt[stmt_num].IS.set_var(1)->name()));

	 }
	 */
	CG_outputRepr *found = ocg->CreateAssignment(0, ocg->CreateIdent("found"),
			ocg->CreateInt(1));
	std::vector<int> lex = getLexicalOrder(stmt_num);
	shiftLexicalOrder(lex, 0, 24);

	std::vector<Relation> xforms;
	std::vector<CG_outputRepr *> codes;

	/*s1.code = found->clone();//wfdg->dep_graph_connect_local(args[1]->clone(), args[2]->clone());
	 s2.code = found->clone(); //wfdg->dep_graph_connect_remote(args[2], args[1]); //ocg->CreateInvoke(dep_graph_constructor_name, args);


	 s1.IS = for_codegen.begin()->second; //IS1
	 s1.xform = xform;
	 s2.IS = for_codegen2.begin()->second; //IS2;
	 s2.xform = copy(xform);
	 */
	int new_stmt_num;
	Relation name_ = for_codegen.begin()->second;

	CG_outputRepr *ident = ocg->CreateIdent(name_.set_var(1)->name());
        CG_outputRepr *if_body = wfdg->dep_graph_initialize();
        init_code = ocg->StmtListAppend(init_code, ocg->CreateIf(0, ocg->CreateEQ(inspection_code()->clone(), ocg->CreateInt(0)),if_body, NULL)); 
        cleanup_code = ocg->StmtListAppend(cleanup_code, ocg->CreateIf(0, ocg->CreateEQ(inspection_code(), ocg->CreateInt(0)),ir->CreateDelete(ocg->CreateIdent(dep)), NULL));
        cleanup_code = ocg->StmtListAppend(cleanup_code,  ocg->CreateAssignment(0, inspection_code(), ocg->CreateInt(1)));
        cleanup_code = ocg->StmtListAppend(cleanup_code, ir->CreateReturn(ocg->CreateAddressOf(ocg->CreateIdent(wfsc->name())))); 
 
	//init_code = ocg->StmtListAppend(init_code, wfdg->dep_graph_initialize());
	//init_code = ocg->StmtListAppend(init_code, wfsc->schedule_initialize());
	//init_code = ocg->StmtListAppend(init_code,
//			wfdg->initialization_code(ident));

	{

		Relation IS(1);
		IS.name_set_var(1, name_.set_var(1)->name());
		Relation IS2(1);
		IS2.name_set_var(1, name_.set_var(1)->name());
		F_And *root = IS.add_and();
		std::map<int, int> pos_map;
		pos_map.insert(std::pair<int, int>(1, 1));
		IS = replace_set_var_as_another_set_var(IS, copy(stmt[stmt_num].IS), 1, 1,
				pos_map);
		if (is_negative) {
			Variable_ID v = IS.set_var(1);
			IS = flip_var(IS, v);
		}
		F_And *root_ = IS2.add_and();

		GEQ_Handle e = root_->add_GEQ();
		e.update_coef(IS2.input_var(1), 1);

		GEQ_Handle e2 = root_->add_GEQ();
		e2.update_coef(IS2.input_var(1), -1);

		//    * 3. aggregate_rows(Loop NO OMP)

		Statement s1;
		Relation xform = simple_xform(-lex[0]);

		s1 = construct_statement(stmt_num, copy(IS), xform,
				wfdg->initialization_code(ident), uninterpreted_symbols,
				uninterpreted_symbols_stringrepr, unin_rel, this->dep);

		new_stmt_num = stmt.size();
		std::vector<int> loop;
		loop.push_back(0);
		mark_omp_threads(new_stmt_num, loop);
		stmt.push_back(s1);

		xform = simple_xform(-lex[0] - 1);

		s1 = construct_statement(stmt_num, copy(IS2), xform,
				wfdg->initialization_code2(), uninterpreted_symbols,
				uninterpreted_symbols_stringrepr, unin_rel, this->dep);

		new_stmt_num = stmt.size();

		//mark_omp_threads(new_stmt_num, loop);
		stmt.push_back(s1);

	}

	for (std::map<int, Relation>::iterator k = for_codegen.begin();
			k != for_codegen.end(); k++) {

		Relation r = k->second;
		if (is_negative) {
			Variable_ID v = r.set_var(1);
			r = flip_var(r, v);
		}
		Relation xform(r.n_set(), 2 * r.n_set() + 1);
		F_And *root2 = xform.add_and();
		for (int i = 2; i < 2 * r.n_set() + 1; i += 2) {
			EQ_Handle h = root2->add_EQ();
			h.update_coef(xform.output_var(i + 1), 1);
			EQ_Handle h2 = root2->add_EQ();
			h2.update_coef(xform.output_var(i), 1);
			h2.update_coef(xform.input_var(i / 2), -1);
		}

		EQ_Handle h = root2->add_EQ();
		h.update_coef(xform.output_var(1), 1);
		h.update_const(-lex[0] - 2);
		int dim = -1;
		for (int i = 1; i <= r.n_set(); i++)
			if (r.set_var(i)->name() == "ip") {
				dim = i;
				break;
			}
		Relation gather;
		CG_outputRepr *gather_code = ocg->CreateAssignment(0,
				ocg->CreateIdent("found"), ocg->CreateInt(0));

		std::set<int> single_iteration_loops;

                std::vector<std::string> omp_prv_str;
                omp_prv_str.push_back(std::string("found"));  

		for (int i = 1; i <= r.n_set(); i++)
			if (is_single_loop_iteration(copy(r), i, copy(this->known))){
			      single_iteration_loops.insert(i);
                              omp_prv_str.push_back(std::string("t") + to_string(2*i));            
            
                        }

		int tmp = dim;

		while (single_iteration_loops.find(tmp) != single_iteration_loops.end()) {

			tmp--;
		}

		//if(single_iteration_loops.find(dim) != single_iteration_loops.end()){
		Relation gather_IS(tmp);
		for (int i = 1; i <= tmp; i++)
			gather_IS.name_set_var(i, r.set_var(i)->name());

		F_And * root__ = gather_IS.add_and();
		std::map<int, int> pos_map2;

		for (int i = 1; i <= tmp; i++)
			pos_map2.insert(std::pair<int, int>(i, i));

		for (int i = 1; i <= tmp; i++)
			gather_IS = replace_set_var_as_another_set_var(gather_IS, copy(r), i,
					i, pos_map2);

		Relation gather_xform(tmp, 2 * tmp + 1);
		F_And * root_ = gather_xform.add_and();
		for (int i = 2; i <= 2 * tmp; i += 2) {

			EQ_Handle e = root_->add_EQ();
			e.update_coef(gather_xform.input_var(i / 2), 1);
			e.update_coef(gather_xform.output_var(i), -1);
			EQ_Handle e2 = root_->add_EQ();
			e2.update_coef(gather_xform.output_var(i - 1), 1);
		}
		assign_const(gather_xform, 2 * tmp, 0);

		assign_const(gather_xform, 0, get_const(copy(xform), 0, Output_Var));

		Statement s1;
		s1.xform = gather_xform;
		s1.IS = gather_IS;

		s1.code = gather_code;
		std::vector<LoopLevel> loops;
		for (int i = 0; i < r.n_set(); i++) {
			LoopLevel ll;
			ll.made_dense = false;
			ll.parallel_level = 0;
			ll.payload = i + 1;
			ll.type = LoopLevelOriginal;
			ll.segment_descriptor = "";
			ll.segreducible = false;
			loops.push_back(ll);
		}
		s1.loop_level = loops;
		s1.has_inspector = true;
		s1.ir_stmt_node = NULL;
		s1.reduction = 0;
		s1.reductionOp = IR_OP_UNKNOWN;
		new_stmt_num = stmt.size();
		std::vector<int> loop;
		loop.push_back(0);
	       mark_omp_threads(new_stmt_num, loop);
                mark_omp_private(new_stmt_num, omp_prv_str);   
		stmt.push_back(s1);

		std::map<std::string, std::vector<CG_outputRepr *> > new_unin;

		for (std::map<std::string, std::vector<CG_outputRepr *> >::iterator it =
				uninterpreted_symbols[stmt_num].begin();
				it != uninterpreted_symbols[stmt_num].end(); it++) {
			std::vector<CG_outputRepr *> a;

			for (int i = 0; i < it->second.size(); i++)
				a.push_back(it->second[i]->clone());

			new_unin.insert(
					std::pair<std::string, std::vector<CG_outputRepr *> >(it->first,
							a));

		}

		uninterpreted_symbols.push_back(new_unin);
		uninterpreted_symbols_stringrepr.push_back(
				uninterpreted_symbols_stringrepr[stmt_num]);
		
               //if(!is_negative)
                  unin_rel.push_back(unin_rel[stmt_num]);
               

               /*else
                  for(std::map<std::string, std::vector<omega::Relation > > >iterator it =  unin_rel[stmt_num].begin(); it != unin_rel
                */  
   
		this->dep.insert();

	}
	for (std::map<int, Relation>::iterator k = for_codegen2.begin();
			k != for_codegen2.end(); k++) {
		Relation r = k->second;
		if (is_negative) {
			Variable_ID v = r.set_var(1);
			r = flip_var(r, v);
		}
		Relation xform(r.n_set(), 2 * r.n_set() + 1);
		F_And *root2 = xform.add_and();
		for (int i = 2; i < 2 * r.n_set() + 1; i += 2) {
			EQ_Handle h = root2->add_EQ();
			h.update_coef(xform.output_var(i + 1), 1);
			EQ_Handle h2 = root2->add_EQ();
			h2.update_coef(xform.output_var(i), 1);
			h2.update_coef(xform.input_var(i / 2), -1);
		}

		EQ_Handle h = root2->add_EQ();
		h.update_coef(xform.output_var(1), 1);
		h.update_const(-lex[0] - 2);
		Relation gather;
		CG_outputRepr *gather_code = ocg->CreateAssignment(0,
				ocg->CreateIdent("found"), ocg->CreateInt(0));

		std::set<int> single_iteration_loops;
		int dim = -1;
		for (int i = 1; i <= r.n_set(); i++)
			if (r.set_var(i)->name() == "ip") {
				dim = i;
				break;
			}
		for (int i = 1; i <= r.n_set(); i++)
			if (is_single_loop_iteration(copy(r), i, copy(this->known)))
				single_iteration_loops.insert(i);
		int tmp = dim;

		while (single_iteration_loops.find(tmp) != single_iteration_loops.end()) {

			tmp--;
		}

		//if(single_iteration_loops.find(dim) != single_iteration_loops.end()){
		Relation gather_IS(tmp);
		for (int i = 1; i <= tmp; i++)
			gather_IS.name_set_var(i, r.set_var(i)->name());
		F_And * root__ = gather_IS.add_and();

		std::map<int, int> pos_map3;

		for (int i = 1; i <= tmp; i++)
			pos_map3.insert(std::pair<int, int>(i, i));

		for (int i = 1; i <= tmp; i++)
			gather_IS = replace_set_var_as_another_set_var(gather_IS, copy(r), i,
					i, pos_map3);

		Relation gather_xform(tmp, 2 * tmp + 1);
		F_And * root_ = gather_xform.add_and();
		for (int i = 2; i <= 2 * tmp; i += 2) {

			EQ_Handle e = root_->add_EQ();
			e.update_coef(gather_xform.input_var(i / 2), 1);
			e.update_coef(gather_xform.output_var(i), -1);
			EQ_Handle e2 = root_->add_EQ();
			e2.update_coef(gather_xform.output_var(i - 1), 1);
		}
		assign_const(gather_xform, 2 * tmp, 0);
		//		get_const(copy(xform), 2 * tmp, Output_Var) + 1);

		assign_const(gather_xform, 0, get_const(copy(xform), 0, Output_Var));

		Statement s1;
		s1.xform = gather_xform;
		s1.IS = gather_IS;

		s1.code = gather_code;
		std::vector<LoopLevel> loops;
		for (int i = 0; i < r.n_set(); i++) {
			LoopLevel ll;
			ll.made_dense = false;
			ll.parallel_level = 0;
			ll.payload = i + 1;
			ll.type = LoopLevelOriginal;
			ll.segment_descriptor = "";
			ll.segreducible = false;
			loops.push_back(ll);
		}
		s1.loop_level = loops;
		s1.has_inspector = true;
		s1.ir_stmt_node = NULL;
		s1.reduction = 0;
		s1.reductionOp = IR_OP_UNKNOWN;
		new_stmt_num = stmt.size();
		stmt.push_back(s1);

		std::map<std::string, std::vector<CG_outputRepr *> > new_unin;

		for (std::map<std::string, std::vector<CG_outputRepr *> >::iterator it =
				uninterpreted_symbols[stmt_num].begin();
				it != uninterpreted_symbols[stmt_num].end(); it++) {
			std::vector<CG_outputRepr *> a;

			for (int i = 0; i < it->second.size(); i++)
				a.push_back(it->second[i]->clone());

			new_unin.insert(
					std::pair<std::string, std::vector<CG_outputRepr *> >(it->first,
							a));

		}

		uninterpreted_symbols.push_back(new_unin);
		uninterpreted_symbols_stringrepr.push_back(
				uninterpreted_symbols_stringrepr[stmt_num]);
		unin_rel.push_back(unin_rel[stmt_num]);

		this->dep.insert();

	}

	for (std::map<int, Relation>::iterator k = for_codegen.begin();
			k != for_codegen.end(); k++) {

		int dim = -1;
		for (int i = 1; i <= r.n_set(); i++)
			if (r.set_var(i)->name() == "ip") {
				dim = i;
				break;
			}

		Relation r = k->second;
		if (is_negative) {
			Variable_ID v = r.set_var(1);
			r = flip_var(r, v);
		}
		Relation xform(r.n_set(), 2 * r.n_set() + 1);
		F_And *root2 = xform.add_and();
		for (int i = 2; i < 2 * r.n_set() + 1; i += 2) {
			EQ_Handle h = root2->add_EQ();
			h.update_coef(xform.output_var(i + 1), 1);
			EQ_Handle h2 = root2->add_EQ();
			h2.update_coef(xform.output_var(i), 1);
			h2.update_coef(xform.input_var(i / 2), -1);
		}

		EQ_Handle h = root2->add_EQ();
		h.update_coef(xform.output_var(1), 1);
		h.update_const(-lex[0] - 2);

		Statement s1;

		s1.IS = r;
		s1.xform = copy(xform);
		s1.code = found->clone();
		std::vector<LoopLevel> loops;
		for (int i = 0; i < r.n_set(); i++) {
			LoopLevel ll;
			ll.made_dense = false;
			ll.parallel_level = 0;
			ll.payload = i + 1;
			ll.type = LoopLevelOriginal;
			ll.segment_descriptor = "";
			ll.segreducible = false;
			loops.push_back(ll);
		}
		s1.loop_level = loops;
		s1.has_inspector = true;
		s1.ir_stmt_node = NULL;
		s1.reduction = 0;
		s1.reductionOp = IR_OP_UNKNOWN;
		new_stmt_num = stmt.size();
		stmt.push_back(s1);

		std::map<std::string, std::vector<CG_outputRepr *> > new_unin;

		for (std::map<std::string, std::vector<CG_outputRepr *> >::iterator it =
				uninterpreted_symbols[stmt_num].begin();
				it != uninterpreted_symbols[stmt_num].end(); it++) {
			std::vector<CG_outputRepr *> a;

			for (int i = 0; i < it->second.size(); i++)
				a.push_back(it->second[i]->clone());

			new_unin.insert(
					std::pair<std::string, std::vector<CG_outputRepr *> >(it->first,
							a));

		}

		uninterpreted_symbols.push_back(new_unin);
		uninterpreted_symbols_stringrepr.push_back(
				uninterpreted_symbols_stringrepr[stmt_num]);
		unin_rel.push_back(unin_rel[stmt_num]);
		this->dep.insert();
		clean_unin_relation(s1.IS, unin_rel[new_stmt_num]);
		apply_xform(new_stmt_num);

	}

	for (std::map<int, Relation>::iterator k = for_codegen2.begin();
			k != for_codegen2.end(); k++) {
		Relation r = k->second;
		if (is_negative) {
			Variable_ID v = r.set_var(1);
			r = flip_var(r, v);
		}
		Relation xform(r.n_set(), 2 * r.n_set() + 1);
		F_And *root2 = xform.add_and();
		for (int i = 2; i < 2 * r.n_set() + 1; i += 2) {
			EQ_Handle h = root2->add_EQ();
			h.update_coef(xform.output_var(i + 1), 1);
			EQ_Handle h2 = root2->add_EQ();
			h2.update_coef(xform.output_var(i), 1);
			h2.update_coef(xform.input_var(i / 2), -1);
		}

		EQ_Handle h = root2->add_EQ();
		h.update_coef(xform.output_var(1), 1);
		h.update_const(-lex[0] - 2);

		Statement s1;
		int dim = -1;
		for (int i = 1; i <= r.n_set(); i++)
			if (r.set_var(i)->name() == "ip") {
				dim = i;
				break;
			}

		s1.IS = r;
		s1.xform = copy(xform);
		s1.code = found->clone();
		std::vector<LoopLevel> loops;
		for (int i = 0; i < r.n_set(); i++) {
			LoopLevel ll;
			ll.made_dense = false;
			ll.parallel_level = 0;
			ll.payload = i + 1;
			ll.type = LoopLevelOriginal;
			ll.segment_descriptor = "";
			ll.segreducible = false;
			loops.push_back(ll);
		}
		s1.loop_level = loops;
		s1.has_inspector = true;
		s1.ir_stmt_node = NULL;
		s1.reduction = 0;
		s1.reductionOp = IR_OP_UNKNOWN;
		new_stmt_num = stmt.size();
		stmt.push_back(s1);

		std::map<std::string, std::vector<CG_outputRepr *> > new_unin;

		for (std::map<std::string, std::vector<CG_outputRepr *> >::iterator it =
				uninterpreted_symbols[stmt_num].begin();
				it != uninterpreted_symbols[stmt_num].end(); it++) {
			std::vector<CG_outputRepr *> a;

			for (int i = 0; i < it->second.size(); i++)
				a.push_back(it->second[i]->clone());

			new_unin.insert(
					std::pair<std::string, std::vector<CG_outputRepr *> >(it->first,
							a));

		}

		uninterpreted_symbols.push_back(new_unin);
		uninterpreted_symbols_stringrepr.push_back(
				uninterpreted_symbols_stringrepr[stmt_num]);
		unin_rel.push_back(unin_rel[stmt_num]);
		this->dep.insert();
		clean_unin_relation(s1.IS, unin_rel[new_stmt_num]);
		apply_xform(new_stmt_num);

	}

	for (std::map<int, Relation>::iterator k = for_codegen.begin();
			k != for_codegen.end(); k++) {
		Relation r = k->second;
		if (is_negative) {
			Variable_ID v = r.set_var(1);
			r = flip_var(r, v);
		}
		Relation new_xform;
		Relation xform(r.n_set(), 2 * r.n_set() + 1);
		F_And *root2 = xform.add_and();
		for (int i = 2; i < 2 * r.n_set() + 1; i += 2) {
			EQ_Handle h = root2->add_EQ();
			h.update_coef(xform.output_var(i + 1), 1);
			EQ_Handle h2 = root2->add_EQ();
			h2.update_coef(xform.output_var(i), 1);
			h2.update_coef(xform.input_var(i / 2), -1);
		}

		EQ_Handle h = root2->add_EQ();
		h.update_coef(xform.output_var(1), 1);
		h.update_const(-lex[0] - 2);
		std::set<int> single_iteration_loops;
		int dim = -1;
		for (int i = 1; i <= r.n_set(); i++)
			if (r.set_var(i)->name() == "ip") {
				dim = i;
				break;
			}
		for (int i = 1; i <= r.n_set(); i++)
			if (is_single_loop_iteration(copy(r), i, copy(this->known)))
				single_iteration_loops.insert(i);

		if (2 * dim + 3 <= 2 * r.n_set() + 1) {
			for (int i = 2 * r.n_set() + 1; i >= 2 * dim + 3; i = i - 2) {

				if (single_iteration_loops.find((i - 1) / 2)
						== single_iteration_loops.end()) {
					new_xform = copy(xform);
					for (int j = i + 1; j <= xform.n_out(); j += 2) {
						assign_const(new_xform, j - 1, 0);

					}

					assign_const(new_xform, i - 1,
							get_const(copy(xform), i - 1, Output_Var) + 1);

				}
			}

			Statement s1;
			s1.xform = new_xform;
			s1.IS = r;

			s1.code = ocg->CreateIf(0,
					ocg->CreateEQ(ocg->CreateIdent("found"), ocg->CreateInt(1)),
					ocg->CreateBreakStatement(), NULL);
			std::vector<LoopLevel> loops;
			for (int i = 0; i < r.n_set(); i++) {
				LoopLevel ll;
				ll.made_dense = false;
				ll.parallel_level = 0;
				ll.payload = i + 1;
				ll.type = LoopLevelOriginal;
				ll.segment_descriptor = "";
				ll.segreducible = false;
				loops.push_back(ll);
			}
			s1.loop_level = loops;
			s1.has_inspector = true;
			s1.ir_stmt_node = NULL;
			s1.reduction = 0;
			s1.reductionOp = IR_OP_UNKNOWN;
			new_stmt_num = stmt.size();
			stmt.push_back(s1);

			std::map<std::string, std::vector<CG_outputRepr *> > new_unin;

			for (std::map<std::string, std::vector<CG_outputRepr *> >::iterator it =
					uninterpreted_symbols[stmt_num].begin();
					it != uninterpreted_symbols[stmt_num].end(); it++) {
				std::vector<CG_outputRepr *> a;

				for (int i = 0; i < it->second.size(); i++)
					a.push_back(it->second[i]->clone());

				new_unin.insert(
						std::pair<std::string, std::vector<CG_outputRepr *> >(
								it->first, a));

			}

			uninterpreted_symbols.push_back(new_unin);
			uninterpreted_symbols_stringrepr.push_back(
					uninterpreted_symbols_stringrepr[stmt_num]);
			unin_rel.push_back(unin_rel[stmt_num]);
			this->dep.insert();
			clean_unin_relation(s1.IS, unin_rel[new_stmt_num]);
			apply_xform(new_stmt_num);

		}
	}

	for (std::map<int, Relation>::iterator k = for_codegen2.begin();
			k != for_codegen2.end(); k++) {
		Relation r = k->second;
		if (is_negative) {
			Variable_ID v = r.set_var(1);
			r = flip_var(r, v);
		}
		Relation xform(r.n_set(), 2 * r.n_set() + 1);
		F_And *root2 = xform.add_and();
		for (int i = 2; i < 2 * r.n_set() + 1; i += 2) {
			EQ_Handle h = root2->add_EQ();
			h.update_coef(xform.output_var(i + 1), 1);
			EQ_Handle h2 = root2->add_EQ();
			h2.update_coef(xform.output_var(i), 1);
			h2.update_coef(xform.input_var(i / 2), -1);
		}

		EQ_Handle h = root2->add_EQ();
		h.update_coef(xform.output_var(1), 1);
		h.update_const(-lex[0] - 2);
		Relation new_xform;
		int dim = -1;
		for (int i = 1; i <= r.n_set(); i++)
			if (r.set_var(i)->name() == "ip") {
				dim = i;
				break;
			}
		std::set<int> single_iteration_loops;

		for (int i = 1; i <= r.n_set(); i++)
			if (is_single_loop_iteration(copy(r), i, copy(this->known)))
				single_iteration_loops.insert(i);
		if (2 * dim + 3 <= 2 * r.n_set() + 1) {
			for (int i = 2 * r.n_set() + 1; i >= 2 * dim + 3; i = i - 2) {

				if (single_iteration_loops.find((i - 1) / 2)
						== single_iteration_loops.end()) {
					new_xform = copy(xform);
					for (int j = i + 1; j <= xform.n_out(); j += 2) {
						assign_const(new_xform, j - 1, 0);

					}

					assign_const(new_xform, i - 1,
							get_const(copy(xform), i - 1, Output_Var) + 1);

				}
			}

			Statement s1;
			s1.xform = new_xform;
			s1.IS = r;

			s1.code = ocg->CreateIf(0,
					ocg->CreateEQ(ocg->CreateIdent("found"), ocg->CreateInt(1)),
					ocg->CreateBreakStatement(), NULL);
			std::vector<LoopLevel> loops;
			for (int i = 0; i < r.n_set(); i++) {
				LoopLevel ll;
				ll.made_dense = false;
				ll.parallel_level = 0;
				ll.payload = i + 1;
				ll.type = LoopLevelOriginal;
				ll.segment_descriptor = "";
				ll.segreducible = false;
				loops.push_back(ll);
			}
			s1.loop_level = loops;
			s1.has_inspector = true;
			s1.ir_stmt_node = NULL;
			s1.reduction = 0;
			s1.reductionOp = IR_OP_UNKNOWN;
			new_stmt_num = stmt.size();
			stmt.push_back(s1);

			std::map<std::string, std::vector<CG_outputRepr *> > new_unin;

			for (std::map<std::string, std::vector<CG_outputRepr *> >::iterator it =
					uninterpreted_symbols[stmt_num].begin();
					it != uninterpreted_symbols[stmt_num].end(); it++) {
				std::vector<CG_outputRepr *> a;

				for (int i = 0; i < it->second.size(); i++)
					a.push_back(it->second[i]->clone());

				new_unin.insert(
						std::pair<std::string, std::vector<CG_outputRepr *> >(
								it->first, a));

			}

			uninterpreted_symbols.push_back(new_unin);
			uninterpreted_symbols_stringrepr.push_back(
					uninterpreted_symbols_stringrepr[stmt_num]);
			unin_rel.push_back(unin_rel[stmt_num]);
			this->dep.insert();
			clean_unin_relation(s1.IS, unin_rel[new_stmt_num]);
			apply_xform(new_stmt_num);
		}
	}

	for (std::map<int, Relation>::iterator k = for_codegen.begin();
			k != for_codegen.end(); k++) {
		Relation r = k->second;
		if (is_negative) {
			Variable_ID v = r.set_var(1);
			r = flip_var(r, v);
		}
		Relation xform(r.n_set(), 2 * r.n_set() + 1);
		F_And *root2 = xform.add_and();
		for (int i = 2; i < 2 * r.n_set() + 1; i += 2) {
			EQ_Handle h = root2->add_EQ();
			h.update_coef(xform.output_var(i + 1), 1);
			EQ_Handle h2 = root2->add_EQ();
			h2.update_coef(xform.output_var(i), 1);
			h2.update_coef(xform.input_var(i / 2), -1);
		}

		EQ_Handle h = root2->add_EQ();
		h.update_coef(xform.output_var(1), 1);
		h.update_const(-lex[0] - 2);

		int dim = -1;
		for (int i = 1; i <= r.n_set(); i++)
			if (r.set_var(i)->name() == "ip") {
				dim = i;
				break;
			}
		Relation gather;

		CG_outputRepr *id = ocg->CreateIdent(r.set_var(1)->name());
		if (is_negative)
			id = ocg->CreateMinus(NULL, id);
		CG_outputRepr *gather_code = wfdg->increment_row_count(id);

		std::set<int> single_iteration_loops;

		for (int i = 1; i <= r.n_set(); i++)
			if (is_single_loop_iteration(copy(r), i, copy(this->known)))
				single_iteration_loops.insert(i);

		int tmp = dim;

		while (single_iteration_loops.find(tmp) != single_iteration_loops.end()) {

			tmp--;
		}

		//if(single_iteration_loops.find(dim) != single_iteration_loops.end()){
		Relation gather_IS(tmp);
		for (int i = 1; i <= tmp; i++)
			gather_IS.name_set_var(i, r.set_var(i)->name());
		F_And * root__ = gather_IS.add_and();
		std::map<int, int> pos_map4;

		for (int i = 1; i <= tmp; i++)
			pos_map4.insert(std::pair<int, int>(i, i));
		for (int i = 1; i <= tmp; i++)
			gather_IS = replace_set_var_as_another_set_var(gather_IS, copy(r), i,
					i, pos_map4);

		Relation gather_xform(tmp, 2 * tmp + 1);
		F_And * root_ = gather_xform.add_and();
		for (int i = 2; i <= 2 * tmp; i += 2) {

			EQ_Handle e = root_->add_EQ();
			e.update_coef(gather_xform.input_var(i / 2), 1);
			e.update_coef(gather_xform.output_var(i), -1);
			EQ_Handle e2 = root_->add_EQ();
			e2.update_coef(gather_xform.output_var(i - 1), 1);
		}
		assign_const(gather_xform, 2 * tmp,
				get_const(copy(xform), 2 * tmp, Output_Var) + 1);

		assign_const(gather_xform, 0, get_const(copy(xform), 0, Output_Var));

		Statement s1;
		s1.xform = gather_xform;
		s1.IS = gather_IS;

		s1.code = gather_code;
		std::vector<LoopLevel> loops;
		for (int i = 0; i < r.n_set(); i++) {
			LoopLevel ll;
			ll.made_dense = false;
			ll.parallel_level = 0;
			ll.payload = i + 1;
			ll.type = LoopLevelOriginal;
			ll.segment_descriptor = "";
			ll.segreducible = false;
			loops.push_back(ll);
		}
		s1.loop_level = loops;
		s1.has_inspector = true;
		s1.ir_stmt_node = NULL;
		s1.reduction = 0;
		s1.reductionOp = IR_OP_UNKNOWN;
		new_stmt_num = stmt.size();
		stmt.push_back(s1);

		std::map<std::string, std::vector<CG_outputRepr *> > new_unin;

		for (std::map<std::string, std::vector<CG_outputRepr *> >::iterator it =
				uninterpreted_symbols[stmt_num].begin();
				it != uninterpreted_symbols[stmt_num].end(); it++) {
			std::vector<CG_outputRepr *> a;

			for (int i = 0; i < it->second.size(); i++)
				a.push_back(it->second[i]->clone());

			new_unin.insert(
					std::pair<std::string, std::vector<CG_outputRepr *> >(it->first,
							a));

		}

		uninterpreted_symbols.push_back(new_unin);
		uninterpreted_symbols_stringrepr.push_back(
				uninterpreted_symbols_stringrepr[stmt_num]);
		unin_rel.push_back(unin_rel[stmt_num]);
		this->dep.insert();
	}
	for (std::map<int, Relation>::iterator k = for_codegen2.begin();
			k != for_codegen2.end(); k++) {
		Relation r = k->second;
		if (is_negative) {
			Variable_ID v = r.set_var(1);
			r = flip_var(r, v);
		}
		Relation xform(r.n_set(), 2 * r.n_set() + 1);
		F_And *root2 = xform.add_and();
		for (int i = 2; i < 2 * r.n_set() + 1; i += 2) {
			EQ_Handle h = root2->add_EQ();
			h.update_coef(xform.output_var(i + 1), 1);
			EQ_Handle h2 = root2->add_EQ();
			h2.update_coef(xform.output_var(i), 1);
			h2.update_coef(xform.input_var(i / 2), -1);
		}

		EQ_Handle h = root2->add_EQ();
		h.update_coef(xform.output_var(1), 1);
		h.update_const(-lex[0] - 2);

		Relation gather;

		std::set<int> single_iteration_loops;
		int dim = -1;
		for (int i = 1; i <= r.n_set(); i++)
			if (r.set_var(i)->name() == "ip") {
				dim = i;
				break;
			}
		for (int i = 1; i <= r.n_set(); i++)
			if (is_single_loop_iteration(copy(r), i, copy(this->known)))
				single_iteration_loops.insert(i);
		int tmp = dim;

		while (single_iteration_loops.find(tmp) != single_iteration_loops.end()) {

			tmp--;
		}
		CG_outputRepr *id = ocg->CreateIdent(r.set_var(1)->name());
		if (is_negative)
			id = ocg->CreateMinus(NULL, id);
		CG_outputRepr *gather_code = wfdg->increment_row_count(id);	//wfdg->increment_and_add_to_row(ocg->CreateIdent(r.set_var(1)->name()),ocg->CreateIdent(r.set_var(dim)->name()) );
		//if(single_iteration_loops.find(dim) != single_iteration_loops.end()){
		Relation gather_IS(tmp);
		for (int i = 1; i <= tmp; i++)
			gather_IS.name_set_var(i, r.set_var(i)->name());
		F_And * root__ = gather_IS.add_and();

		std::map<int, int> pos_map5;

		for (int i = 1; i <= tmp; i++)
			pos_map5.insert(std::pair<int, int>(i, i));

		for (int i = 1; i <= tmp; i++)
			gather_IS = replace_set_var_as_another_set_var(gather_IS, copy(r), i,
					i, pos_map5);

		Relation gather_xform(tmp, 2 * tmp + 1);
		F_And * root_ = gather_xform.add_and();
		for (int i = 2; i <= 2 * tmp; i += 2) {

			EQ_Handle e = root_->add_EQ();
			e.update_coef(gather_xform.input_var(i / 2), 1);
			e.update_coef(gather_xform.output_var(i), -1);
			EQ_Handle e2 = root_->add_EQ();
			e2.update_coef(gather_xform.output_var(i - 1), 1);
		}
		assign_const(gather_xform, 2 * tmp,
				get_const(copy(xform), 2 * tmp, Output_Var) + 1);

		assign_const(gather_xform, 0, get_const(copy(xform), 0, Output_Var));

		Statement s1;
		s1.xform = gather_xform;
		s1.IS = gather_IS;

		s1.code = gather_code;
		std::vector<LoopLevel> loops;
		for (int i = 0; i < r.n_set(); i++) {
			LoopLevel ll;
			ll.made_dense = false;
			ll.parallel_level = 0;
			ll.payload = i + 1;
			ll.type = LoopLevelOriginal;
			ll.segment_descriptor = "";
			ll.segreducible = false;
			loops.push_back(ll);
		}
		s1.loop_level = loops;
		s1.has_inspector = true;
		s1.ir_stmt_node = NULL;
		s1.reduction = 0;
		s1.reductionOp = IR_OP_UNKNOWN;
		new_stmt_num = stmt.size();
		stmt.push_back(s1);

		std::map<std::string, std::vector<CG_outputRepr *> > new_unin;

		for (std::map<std::string, std::vector<CG_outputRepr *> >::iterator it =
				uninterpreted_symbols[stmt_num].begin();
				it != uninterpreted_symbols[stmt_num].end(); it++) {
			std::vector<CG_outputRepr *> a;

			for (int i = 0; i < it->second.size(); i++)
				a.push_back(it->second[i]->clone());

			new_unin.insert(
					std::pair<std::string, std::vector<CG_outputRepr *> >(it->first,
							a));

		}

		uninterpreted_symbols.push_back(new_unin);
		uninterpreted_symbols_stringrepr.push_back(
				uninterpreted_symbols_stringrepr[stmt_num]);
		unin_rel.push_back(unin_rel[stmt_num]);
		this->dep.insert();
	}

	for (std::map<int, Relation>::iterator k = for_codegen.begin();
			k != for_codegen.end(); k++) {

		Relation r = k->second;
		if (is_negative) {
			Variable_ID v = r.set_var(1);
			r = flip_var(r, v);
		}
		Relation xform(r.n_set(), 2 * r.n_set() + 1);
		F_And *root2 = xform.add_and();
		for (int i = 2; i < 2 * r.n_set() + 1; i += 2) {
			EQ_Handle h = root2->add_EQ();
			h.update_coef(xform.output_var(i + 1), 1);
			EQ_Handle h2 = root2->add_EQ();
			h2.update_coef(xform.output_var(i), 1);
			h2.update_coef(xform.input_var(i / 2), -1);
		}

		EQ_Handle h = root2->add_EQ();
		h.update_coef(xform.output_var(1), 1);
		h.update_const(-lex[0] - 2);
		int dim = -1;
		for (int i = 1; i <= r.n_set(); i++)
			if (r.set_var(i)->name() == "ip") {
				dim = i;
				break;
			}
		Relation gather;
		CG_outputRepr *gather_code = ocg->CreateAssignment(0,
				ocg->CreateIdent("found"), ocg->CreateInt(0));

		std::set<int> single_iteration_loops;

		for (int i = 1; i <= r.n_set(); i++)
			if (is_single_loop_iteration(copy(r), i, copy(this->known)))
				single_iteration_loops.insert(i);

		int tmp = dim;

		while (single_iteration_loops.find(tmp) != single_iteration_loops.end()) {

			tmp--;
		}

		//if(single_iteration_loops.find(dim) != single_iteration_loops.end()){
		Relation gather_IS(tmp);
		for (int i = 1; i <= tmp; i++)
			gather_IS.name_set_var(i, r.set_var(i)->name());
		F_And * root__ = gather_IS.add_and();
		std::map<int, int> pos_map6;

		for (int i = 1; i <= tmp; i++)
			pos_map6.insert(std::pair<int, int>(i, i));
		for (int i = 1; i <= tmp; i++)
			gather_IS = replace_set_var_as_another_set_var(gather_IS, copy(r), i,
					i, pos_map6);

		Relation gather_xform(tmp, 2 * tmp + 1);
		F_And * root_ = gather_xform.add_and();
		for (int i = 2; i <= 2 * tmp; i += 2) {

			EQ_Handle e = root_->add_EQ();
			e.update_coef(gather_xform.input_var(i / 2), 1);
			e.update_coef(gather_xform.output_var(i), -1);
			EQ_Handle e2 = root_->add_EQ();
			e2.update_coef(gather_xform.output_var(i - 1), 1);
		}
		assign_const(gather_xform, 2 * tmp, 0);

		assign_const(gather_xform, 0, get_const(copy(xform), 0, Output_Var));

		Statement s1;
		s1.xform = gather_xform;
		s1.IS = gather_IS;

		s1.code = gather_code;
		std::vector<LoopLevel> loops;
		for (int i = 0; i < r.n_set(); i++) {
			LoopLevel ll;
			ll.made_dense = false;
			ll.parallel_level = 0;
			ll.payload = i + 1;
			ll.type = LoopLevelOriginal;
			ll.segment_descriptor = "";
			ll.segreducible = false;
			loops.push_back(ll);
		}
		s1.loop_level = loops;
		s1.has_inspector = true;
		s1.ir_stmt_node = NULL;
		s1.reduction = 0;
		s1.reductionOp = IR_OP_UNKNOWN;
		new_stmt_num = stmt.size();
		stmt.push_back(s1);

		std::map<std::string, std::vector<CG_outputRepr *> > new_unin;

		for (std::map<std::string, std::vector<CG_outputRepr *> >::iterator it =
				uninterpreted_symbols[stmt_num].begin();
				it != uninterpreted_symbols[stmt_num].end(); it++) {
			std::vector<CG_outputRepr *> a;

			for (int i = 0; i < it->second.size(); i++)
				a.push_back(it->second[i]->clone());

			new_unin.insert(
					std::pair<std::string, std::vector<CG_outputRepr *> >(it->first,
							a));

		}

		uninterpreted_symbols.push_back(new_unin);
		uninterpreted_symbols_stringrepr.push_back(
				uninterpreted_symbols_stringrepr[stmt_num]);
		unin_rel.push_back(unin_rel[stmt_num]);
		this->dep.insert();
	}
	for (std::map<int, Relation>::iterator k = for_codegen2.begin();
			k != for_codegen2.end(); k++) {
		Relation r = k->second;
		if (is_negative) {
			Variable_ID v = r.set_var(1);
			r = flip_var(r, v);
		}
		Relation xform(r.n_set(), 2 * r.n_set() + 1);
		F_And *root2 = xform.add_and();
		for (int i = 2; i < 2 * r.n_set() + 1; i += 2) {
			EQ_Handle h = root2->add_EQ();
			h.update_coef(xform.output_var(i + 1), 1);
			EQ_Handle h2 = root2->add_EQ();
			h2.update_coef(xform.output_var(i), 1);
			h2.update_coef(xform.input_var(i / 2), -1);
		}

		EQ_Handle h = root2->add_EQ();
		h.update_coef(xform.output_var(1), 1);
		h.update_const(-lex[0] - 2);
		Relation gather;
		CG_outputRepr *gather_code = ocg->CreateAssignment(0,
				ocg->CreateIdent("found"), ocg->CreateInt(0));

		std::set<int> single_iteration_loops;
		int dim = -1;
		for (int i = 1; i <= r.n_set(); i++)
			if (r.set_var(i)->name() == "ip") {
				dim = i;
				break;
			}
		for (int i = 1; i <= r.n_set(); i++)
			if (is_single_loop_iteration(copy(r), i, copy(this->known)))
				single_iteration_loops.insert(i);
		int tmp = dim;

		while (single_iteration_loops.find(tmp) != single_iteration_loops.end()) {

			tmp--;
		}

		//if(single_iteration_loops.find(dim) != single_iteration_loops.end()){
		Relation gather_IS(tmp);
		for (int i = 1; i <= tmp; i++)
			gather_IS.name_set_var(i, r.set_var(i)->name());

		std::map<int, int> pos_map7;

		for (int i = 1; i <= tmp; i++)
			pos_map7.insert(std::pair<int, int>(i, i));
		F_And * root__ = gather_IS.add_and();
		for (int i = 1; i <= tmp; i++)
			gather_IS = replace_set_var_as_another_set_var(gather_IS, copy(r), i,
					i, pos_map7);

		Relation gather_xform(tmp, 2 * tmp + 1);
		F_And * root_ = gather_xform.add_and();
		for (int i = 2; i <= 2 * tmp; i += 2) {

			EQ_Handle e = root_->add_EQ();
			e.update_coef(gather_xform.input_var(i / 2), 1);
			e.update_coef(gather_xform.output_var(i), -1);
			EQ_Handle e2 = root_->add_EQ();
			e2.update_coef(gather_xform.output_var(i - 1), 1);
		}
		assign_const(gather_xform, 2 * tmp, 0);
		//		get_const(copy(xform), 2 * tmp, Output_Var) + 1);

		assign_const(gather_xform, 0, get_const(copy(xform), 0, Output_Var));

		Statement s1;
		s1.xform = gather_xform;
		s1.IS = gather_IS;

		s1.code = gather_code;
		std::vector<LoopLevel> loops;
		for (int i = 0; i < r.n_set(); i++) {
			LoopLevel ll;
			ll.made_dense = false;
			ll.parallel_level = 0;
			ll.payload = i + 1;
			ll.type = LoopLevelOriginal;
			ll.segment_descriptor = "";
			ll.segreducible = false;
			loops.push_back(ll);
		}
		s1.loop_level = loops;
		s1.has_inspector = true;
		s1.ir_stmt_node = NULL;
		s1.reduction = 0;
		s1.reductionOp = IR_OP_UNKNOWN;
		new_stmt_num = stmt.size();
		stmt.push_back(s1);

		std::map<std::string, std::vector<CG_outputRepr *> > new_unin;

		for (std::map<std::string, std::vector<CG_outputRepr *> >::iterator it =
				uninterpreted_symbols[stmt_num].begin();
				it != uninterpreted_symbols[stmt_num].end(); it++) {
			std::vector<CG_outputRepr *> a;

			for (int i = 0; i < it->second.size(); i++)
				a.push_back(it->second[i]->clone());

			new_unin.insert(
					std::pair<std::string, std::vector<CG_outputRepr *> >(it->first,
							a));

		}

		uninterpreted_symbols.push_back(new_unin);
		uninterpreted_symbols_stringrepr.push_back(
				uninterpreted_symbols_stringrepr[stmt_num]);
		unin_rel.push_back(unin_rel[stmt_num]);
		this->dep.insert();
	}

	/*Order:
	 * 1. Initialization code (loop, OMP)
	 * 2. Loop nest 1
	 * 3. aggregate_rows(Loop NO OMP)
	 * 4. clear_counter (Loop, OMP)
	 * 5. resize_and_initialize_columns (0, NO OMP)
	 * 6. loop nest 2
	 * 7. initialize sum;(0, NO OMP)
	 *	8.aggregate_count(loop, NO OMP)
	 * 9.realloc_col (0, NO OMP)
	 *10. initialize_offset (0, NO_OMP)
	 *11. populate_col2 (loop, NO OMP)
	 *12. free_and_reassign_col(no loop, NO OMP)
	 *13. sort_by_col (loop,OMP)
	 *14. clear_counter2 	(loop, OMP)
	 *15. symmetrify (loop, OMP)
	 *16. initialize_sum (no loop, no OMP)
	 *17. aggregate count (loop, NO OMP)
	 *18. realloc_col2 (no loop, NO OMP)
	 *19.initialize_offset (no loop, NO OMP)
	 *20.populate_col3 (loop, NO OMP)
	 *21.free_and_reassign_col2(no loop, NO OMP)
	 *22.symmetrify2  (loop, OMP)
	 *23.sort_by_col_final(loop, OMP)
	 */
	{

		Relation IS(1);
		IS.name_set_var(1, name_.set_var(1)->name());
		Relation IS2(1);
		IS2.name_set_var(1, name_.set_var(1)->name());
		F_And *root = IS.add_and();
		std::map<int, int> pos_map8;

		for (int i = 1; i <= 1; i++)
			pos_map8.insert(std::pair<int, int>(i, i));
		IS = replace_set_var_as_another_set_var(IS, copy(stmt[stmt_num].IS), 1, 1,
				pos_map8);
		if (is_negative) {
			Variable_ID v = IS.set_var(1);
			IS = flip_var(IS, v);
		}
		F_And *root_ = IS2.add_and();

		GEQ_Handle e = root_->add_GEQ();
		e.update_coef(IS2.input_var(1), 1);

		GEQ_Handle e2 = root_->add_GEQ();
		e2.update_coef(IS2.input_var(1), -1);

		//    * 3. aggregate_rows(Loop NO OMP)

		Statement s1;
		Relation xform = simple_xform(-lex[0] - 3);
		std::vector<int> loop;
		s1 = construct_statement(stmt_num, copy(IS), xform,
				wfdg->aggregate_rows(ident), uninterpreted_symbols,
				uninterpreted_symbols_stringrepr, unin_rel, this->dep);
		new_stmt_num = stmt.size();
		stmt.push_back(s1);

		xform = simple_xform(-lex[0] - 4);
		// 4.  clear_counter (Loop, OMP)
		s1 = construct_statement(stmt_num, copy(IS), xform,
				wfdg->clear_counter(ident), uninterpreted_symbols,
				uninterpreted_symbols_stringrepr, unin_rel, this->dep);

		new_stmt_num = stmt.size();
		loop.push_back(1);
		mark_omp_threads(new_stmt_num, loop);
		stmt.push_back(s1);

		xform = simple_xform(-lex[0] - 5);
		s1 = construct_statement(stmt_num, copy(IS2), xform,
				wfdg->resize_and_initialize_columns(ident), uninterpreted_symbols,
				uninterpreted_symbols_stringrepr, unin_rel, this->dep);
		new_stmt_num = stmt.size();
		stmt.push_back(s1);
	}

	for (std::map<int, Relation>::iterator k = for_codegen.begin();
			k != for_codegen.end(); k++) {


		Relation r = k->second;
		if (is_negative) {
			Variable_ID v = r.set_var(1);
			r = flip_var(r, v);
		}

                int dim = -1;
                for (int i = 1; i <= r.n_set(); i++)
                        if (r.set_var(i)->name() == "ip") {
                                dim = i;
                                break;
                        }


            std::vector<std::string>omp_prv_str;
             for (int i = 1; i <= r.n_set(); i++)
                        if (is_single_loop_iteration(copy(r), i, copy(this->known))){
                              //single_iteration_loops.insert(i);
                              omp_prv_str.push_back(std::string("t") + to_string(2*i));

                        }




		Relation xform(r.n_set(), 2 * r.n_set() + 1);
		F_And *root2 = xform.add_and();
		for (int i = 2; i < 2 * r.n_set() + 1; i += 2) {
			EQ_Handle h = root2->add_EQ();
			h.update_coef(xform.output_var(i + 1), 1);
			EQ_Handle h2 = root2->add_EQ();
			h2.update_coef(xform.output_var(i), 1);
			h2.update_coef(xform.input_var(i / 2), -1);
		}

		EQ_Handle h = root2->add_EQ();
		h.update_coef(xform.output_var(1), 1);
		h.update_const(-lex[0] - 6);

		Statement s1;

		s1.IS = r;
		s1.xform = copy(xform);
		s1.code = found->clone();
		std::vector<LoopLevel> loops;
		for (int i = 0; i < r.n_set(); i++) {
			LoopLevel ll;
			ll.made_dense = false;
			ll.parallel_level = 0;
			ll.payload = i + 1;
			ll.type = LoopLevelOriginal;
			ll.segment_descriptor = "";
			ll.segreducible = false;
			loops.push_back(ll);
		}
		s1.loop_level = loops;
		s1.has_inspector = true;
		s1.ir_stmt_node = NULL;
		s1.reduction = 0;
		s1.reductionOp = IR_OP_UNKNOWN;
		new_stmt_num = stmt.size();
		std::vector<int> loop;
		loop.push_back(0);
		mark_omp_threads(new_stmt_num, loop);
                //std::vector<std::string>omp_prv_str;
                omp_prv_str.push_back("found");
                mark_omp_private(new_stmt_num,omp_prv_str);                  
  
		stmt.push_back(s1);

		std::map<std::string, std::vector<CG_outputRepr *> > new_unin;

		for (std::map<std::string, std::vector<CG_outputRepr *> >::iterator it =
				uninterpreted_symbols[stmt_num].begin();
				it != uninterpreted_symbols[stmt_num].end(); it++) {
			std::vector<CG_outputRepr *> a;

			for (int i = 0; i < it->second.size(); i++)
				a.push_back(it->second[i]->clone());

			new_unin.insert(
					std::pair<std::string, std::vector<CG_outputRepr *> >(it->first,
							a));

		}

		uninterpreted_symbols.push_back(new_unin);
		uninterpreted_symbols_stringrepr.push_back(
				uninterpreted_symbols_stringrepr[stmt_num]);
		unin_rel.push_back(unin_rel[stmt_num]);
		this->dep.insert();

	}

	for (std::map<int, Relation>::iterator k = for_codegen2.begin();
			k != for_codegen2.end(); k++) {
		Relation r = k->second;
		if (is_negative) {
			Variable_ID v = r.set_var(1);
			r = flip_var(r, v);
		}
		Relation xform(r.n_set(), 2 * r.n_set() + 1);
		F_And *root2 = xform.add_and();
		for (int i = 2; i < 2 * r.n_set() + 1; i += 2) {
			EQ_Handle h = root2->add_EQ();
			h.update_coef(xform.output_var(i + 1), 1);
			EQ_Handle h2 = root2->add_EQ();
			h2.update_coef(xform.output_var(i), 1);
			h2.update_coef(xform.input_var(i / 2), -1);
		}

		EQ_Handle h = root2->add_EQ();
		h.update_coef(xform.output_var(1), 1);
		h.update_const(-lex[0] - 6);

		Statement s1;
		int dim = -1;
		for (int i = 1; i <= r.n_set(); i++)
			if (r.set_var(i)->name() == "ip") {
				dim = i;
				break;
			}

		s1.IS = r;
		s1.xform = copy(xform);
		s1.code = found->clone();
		std::vector<LoopLevel> loops;
		for (int i = 0; i < r.n_set(); i++) {
			LoopLevel ll;
			ll.made_dense = false;
			ll.parallel_level = 0;
			ll.payload = i + 1;
			ll.type = LoopLevelOriginal;
			ll.segment_descriptor = "";
			ll.segreducible = false;
			loops.push_back(ll);
		}
		s1.loop_level = loops;
		s1.has_inspector = true;
		s1.ir_stmt_node = NULL;
		s1.reduction = 0;
		s1.reductionOp = IR_OP_UNKNOWN;
		new_stmt_num = stmt.size();
		stmt.push_back(s1);

		std::map<std::string, std::vector<CG_outputRepr *> > new_unin;

		for (std::map<std::string, std::vector<CG_outputRepr *> >::iterator it =
				uninterpreted_symbols[stmt_num].begin();
				it != uninterpreted_symbols[stmt_num].end(); it++) {
			std::vector<CG_outputRepr *> a;

			for (int i = 0; i < it->second.size(); i++)
				a.push_back(it->second[i]->clone());

			new_unin.insert(
					std::pair<std::string, std::vector<CG_outputRepr *> >(it->first,
							a));

		}

		uninterpreted_symbols.push_back(new_unin);
		uninterpreted_symbols_stringrepr.push_back(
				uninterpreted_symbols_stringrepr[stmt_num]);
		unin_rel.push_back(unin_rel[stmt_num]);
		this->dep.insert();

	}

	for (std::map<int, Relation>::iterator k = for_codegen.begin();
			k != for_codegen.end(); k++) {
		Relation r = k->second;
		if (is_negative) {
			Variable_ID v = r.set_var(1);
			r = flip_var(r, v);
		}
		Relation new_xform;
		Relation xform(r.n_set(), 2 * r.n_set() + 1);
		F_And *root2 = xform.add_and();
		for (int i = 2; i < 2 * r.n_set() + 1; i += 2) {
			EQ_Handle h = root2->add_EQ();
			h.update_coef(xform.output_var(i + 1), 1);
			EQ_Handle h2 = root2->add_EQ();
			h2.update_coef(xform.output_var(i), 1);
			h2.update_coef(xform.input_var(i / 2), -1);
		}

		EQ_Handle h = root2->add_EQ();
		h.update_coef(xform.output_var(1), 1);
		h.update_const(-lex[0] - 6);
		std::set<int> single_iteration_loops;
		int dim = -1;
		for (int i = 1; i <= r.n_set(); i++)
			if (r.set_var(i)->name() == "ip") {
				dim = i;
				break;
			}
		for (int i = 1; i <= r.n_set(); i++)
			if (is_single_loop_iteration(copy(r), i, copy(this->known)))
				single_iteration_loops.insert(i);
		if (2 * dim + 3 <= 2 * r.n_set() + 1) {
			for (int i = 2 * r.n_set() + 1; i >= 2 * dim + 3; i = i - 2) {

				if (single_iteration_loops.find((i - 1) / 2)
						== single_iteration_loops.end()) {
					new_xform = copy(xform);
					for (int j = i + 1; j <= xform.n_out(); j += 2) {
						assign_const(new_xform, j - 1, 0);

					}

					assign_const(new_xform, i - 1,
							get_const(copy(xform), i - 1, Output_Var) + 1);

				}
			}

			Statement s1;
			s1.xform = new_xform;
			s1.IS = r;

			s1.code = ocg->CreateIf(0,
					ocg->CreateEQ(ocg->CreateIdent("found"), ocg->CreateInt(1)),
					ocg->CreateBreakStatement(), NULL);
			std::vector<LoopLevel> loops;
			for (int i = 0; i < r.n_set(); i++) {
				LoopLevel ll;
				ll.made_dense = false;
				ll.parallel_level = 0;
				ll.payload = i + 1;
				ll.type = LoopLevelOriginal;
				ll.segment_descriptor = "";
				ll.segreducible = false;
				loops.push_back(ll);
			}
			s1.loop_level = loops;
			s1.has_inspector = true;
			s1.ir_stmt_node = NULL;
			s1.reduction = 0;
			s1.reductionOp = IR_OP_UNKNOWN;
			new_stmt_num = stmt.size();
			stmt.push_back(s1);

			std::map<std::string, std::vector<CG_outputRepr *> > new_unin;

			for (std::map<std::string, std::vector<CG_outputRepr *> >::iterator it =
					uninterpreted_symbols[stmt_num].begin();
					it != uninterpreted_symbols[stmt_num].end(); it++) {
				std::vector<CG_outputRepr *> a;

				for (int i = 0; i < it->second.size(); i++)
					a.push_back(it->second[i]->clone());

				new_unin.insert(
						std::pair<std::string, std::vector<CG_outputRepr *> >(
								it->first, a));

			}

			uninterpreted_symbols.push_back(new_unin);
			uninterpreted_symbols_stringrepr.push_back(
					uninterpreted_symbols_stringrepr[stmt_num]);
			unin_rel.push_back(unin_rel[stmt_num]);
			this->dep.insert();
		}
	}

	for (std::map<int, Relation>::iterator k = for_codegen2.begin();
			k != for_codegen2.end(); k++) {
		Relation r = k->second;
		if (is_negative) {
			Variable_ID v = r.set_var(1);
			r = flip_var(r, v);
		}
		Relation xform(r.n_set(), 2 * r.n_set() + 1);
		F_And *root2 = xform.add_and();
		for (int i = 2; i < 2 * r.n_set() + 1; i += 2) {
			EQ_Handle h = root2->add_EQ();
			h.update_coef(xform.output_var(i + 1), 1);
			EQ_Handle h2 = root2->add_EQ();
			h2.update_coef(xform.output_var(i), 1);
			h2.update_coef(xform.input_var(i / 2), -1);
		}

		EQ_Handle h = root2->add_EQ();
		h.update_coef(xform.output_var(1), 1);
		h.update_const(-lex[0] - 6);
		Relation new_xform;
		int dim = -1;
		for (int i = 1; i <= r.n_set(); i++)
			if (r.set_var(i)->name() == "ip") {
				dim = i;
				break;
			}
		std::set<int> single_iteration_loops;

		for (int i = 1; i <= r.n_set(); i++)
			if (is_single_loop_iteration(copy(r), i, copy(this->known)))
				single_iteration_loops.insert(i);
		if (2 * dim + 3 <= 2 * r.n_set() + 1) {
			for (int i = 2 * r.n_set() + 1; i >= 2 * dim + 3; i = i - 2) {

				if (single_iteration_loops.find((i - 1) / 2)
						== single_iteration_loops.end()) {
					new_xform = copy(xform);
					for (int j = i + 1; j <= xform.n_out(); j += 2) {
						assign_const(new_xform, j - 1, 0);

					}

					assign_const(new_xform, i - 1,
							get_const(copy(xform), i - 1, Output_Var) + 1);

				}
			}

			Statement s1;
			s1.xform = new_xform;
			s1.IS = r;

			s1.code = ocg->CreateIf(0,
					ocg->CreateEQ(ocg->CreateIdent("found"), ocg->CreateInt(1)),
					ocg->CreateBreakStatement(), NULL);
			std::vector<LoopLevel> loops;
			for (int i = 0; i < r.n_set(); i++) {
				LoopLevel ll;
				ll.made_dense = false;
				ll.parallel_level = 0;
				ll.payload = i + 1;
				ll.type = LoopLevelOriginal;
				ll.segment_descriptor = "";
				ll.segreducible = false;
				loops.push_back(ll);
			}
			s1.loop_level = loops;
			s1.has_inspector = true;
			s1.ir_stmt_node = NULL;
			s1.reduction = 0;
			s1.reductionOp = IR_OP_UNKNOWN;
			new_stmt_num = stmt.size();
			stmt.push_back(s1);

			std::map<std::string, std::vector<CG_outputRepr *> > new_unin;

			for (std::map<std::string, std::vector<CG_outputRepr *> >::iterator it =
					uninterpreted_symbols[stmt_num].begin();
					it != uninterpreted_symbols[stmt_num].end(); it++) {
				std::vector<CG_outputRepr *> a;

				for (int i = 0; i < it->second.size(); i++)
					a.push_back(it->second[i]->clone());

				new_unin.insert(
						std::pair<std::string, std::vector<CG_outputRepr *> >(
								it->first, a));

			}

			uninterpreted_symbols.push_back(new_unin);
			uninterpreted_symbols_stringrepr.push_back(
					uninterpreted_symbols_stringrepr[stmt_num]);
			unin_rel.push_back(unin_rel[stmt_num]);
			this->dep.insert();
		}
	}

	for (std::map<int, Relation>::iterator k = for_codegen.begin();
			k != for_codegen.end(); k++) {
		Relation r = k->second;
		if (is_negative) {
			Variable_ID v = r.set_var(1);
			r = flip_var(r, v);
		}

		Relation xform(r.n_set(), 2 * r.n_set() + 1);
		F_And *root2 = xform.add_and();
		for (int i = 2; i < 2 * r.n_set() + 1; i += 2) {
			EQ_Handle h = root2->add_EQ();
			h.update_coef(xform.output_var(i + 1), 1);
			EQ_Handle h2 = root2->add_EQ();
			h2.update_coef(xform.output_var(i), 1);
			h2.update_coef(xform.input_var(i / 2), -1);
		}

		EQ_Handle h = root2->add_EQ();
		h.update_coef(xform.output_var(1), 1);
		h.update_const(-lex[0] - 6);

		int dim = -1;
		for (int i = 1; i <= r.n_set(); i++)
			if (r.set_var(i)->name() == "ip") {
				dim = i;
				break;
			}
		Relation gather;
		CG_outputRepr *id = ocg->CreateIdent(r.set_var(1)->name());
		if (is_negative)
			id = ocg->CreateMinus(NULL, id);

		CG_outputRepr *gather_code = wfdg->increment_and_add_to_row(id,
				ocg->CreateIdent("t6"));

		std::set<int> single_iteration_loops;

		for (int i = 1; i <= r.n_set(); i++)
			if (is_single_loop_iteration(copy(r), i, copy(this->known)))
				single_iteration_loops.insert(i);

		int tmp = dim;

		while (single_iteration_loops.find(tmp) != single_iteration_loops.end()) {

			tmp--;
		}

		//if(single_iteration_loops.find(dim) != single_iteration_loops.end()){
		Relation gather_IS(tmp);
		for (int i = 1; i <= tmp; i++)
			gather_IS.name_set_var(i, r.set_var(i)->name());
		F_And * root__ = gather_IS.add_and();

		std::map<int, int> pos_map9;

		for (int i = 1; i <= tmp; i++)
			pos_map9.insert(std::pair<int, int>(i, i));
		for (int i = 1; i <= tmp; i++)
			gather_IS = replace_set_var_as_another_set_var(gather_IS, copy(r), i,
					i, pos_map9);

		Relation gather_xform(tmp, 2 * tmp + 1);
		F_And * root_ = gather_xform.add_and();
		for (int i = 2; i <= 2 * tmp; i += 2) {

			EQ_Handle e = root_->add_EQ();
			e.update_coef(gather_xform.input_var(i / 2), 1);
			e.update_coef(gather_xform.output_var(i), -1);
			EQ_Handle e2 = root_->add_EQ();
			e2.update_coef(gather_xform.output_var(i - 1), 1);
		}
		assign_const(gather_xform, 2 * tmp,
				get_const(copy(xform), 2 * tmp, Output_Var) + 1);

		assign_const(gather_xform, 0, get_const(copy(xform), 0, Output_Var));

		Statement s1;
		s1.xform = gather_xform;
		s1.IS = gather_IS;

		s1.code = gather_code;
		std::vector<LoopLevel> loops;
		for (int i = 0; i < r.n_set(); i++) {
			LoopLevel ll;
			ll.made_dense = false;
			ll.parallel_level = 0;
			ll.payload = i + 1;
			ll.type = LoopLevelOriginal;
			ll.segment_descriptor = "";
			ll.segreducible = false;
			loops.push_back(ll);
		}
		s1.loop_level = loops;
		s1.has_inspector = true;
		s1.ir_stmt_node = NULL;
		s1.reduction = 0;
		s1.reductionOp = IR_OP_UNKNOWN;
		new_stmt_num = stmt.size();
		stmt.push_back(s1);

		std::map<std::string, std::vector<CG_outputRepr *> > new_unin;

		for (std::map<std::string, std::vector<CG_outputRepr *> >::iterator it =
				uninterpreted_symbols[stmt_num].begin();
				it != uninterpreted_symbols[stmt_num].end(); it++) {
			std::vector<CG_outputRepr *> a;

			for (int i = 0; i < it->second.size(); i++)
				a.push_back(it->second[i]->clone());

			new_unin.insert(
					std::pair<std::string, std::vector<CG_outputRepr *> >(it->first,
							a));

		}

		uninterpreted_symbols.push_back(new_unin);
		uninterpreted_symbols_stringrepr.push_back(
				uninterpreted_symbols_stringrepr[stmt_num]);
		unin_rel.push_back(unin_rel[stmt_num]);
		this->dep.insert();
	}
	for (std::map<int, Relation>::iterator k = for_codegen2.begin();
			k != for_codegen2.end(); k++) {
		Relation r = k->second;
		if (is_negative) {
			Variable_ID v = r.set_var(1);
			r = flip_var(r, v);
		}
		Relation xform(r.n_set(), 2 * r.n_set() + 1);
		F_And *root2 = xform.add_and();
		for (int i = 2; i < 2 * r.n_set() + 1; i += 2) {
			EQ_Handle h = root2->add_EQ();
			h.update_coef(xform.output_var(i + 1), 1);
			EQ_Handle h2 = root2->add_EQ();
			h2.update_coef(xform.output_var(i), 1);
			h2.update_coef(xform.input_var(i / 2), -1);
		}

		EQ_Handle h = root2->add_EQ();
		h.update_coef(xform.output_var(1), 1);
		h.update_const(-lex[0] - 6);

		Relation gather;

		std::set<int> single_iteration_loops;
		int dim = -1;
		for (int i = 1; i <= r.n_set(); i++)
			if (r.set_var(i)->name() == "ip") {
				dim = i;
				break;
			}
		for (int i = 1; i <= r.n_set(); i++)
			if (is_single_loop_iteration(copy(r), i, copy(this->known)))
				single_iteration_loops.insert(i);
		int tmp = dim;

		while (single_iteration_loops.find(tmp) != single_iteration_loops.end()) {

			tmp--;
		}
		CG_outputRepr *id = ocg->CreateIdent(r.set_var(1)->name());
		if (is_negative)
			id = ocg->CreateMinus(NULL, id);

		CG_outputRepr *gather_code = wfdg->increment_and_add_to_row(id,
				ocg->CreateIdent("t6"));
		//if(single_iteration_loops.find(dim) != single_iteration_loops.end()){
		Relation gather_IS(tmp);
		for (int i = 1; i <= tmp; i++)
			gather_IS.name_set_var(i, r.set_var(i)->name());
		F_And * root__ = gather_IS.add_and();
		std::map<int, int> pos_map10;

		for (int i = 1; i <= tmp; i++)
			pos_map10.insert(std::pair<int, int>(i, i));
		for (int i = 1; i <= tmp; i++)
			gather_IS = replace_set_var_as_another_set_var(gather_IS, copy(r), i,
					i, pos_map10);

		Relation gather_xform(tmp, 2 * tmp + 1);
		F_And * root_ = gather_xform.add_and();
		for (int i = 2; i <= 2 * tmp; i += 2) {

			EQ_Handle e = root_->add_EQ();
			e.update_coef(gather_xform.input_var(i / 2), 1);
			e.update_coef(gather_xform.output_var(i), -1);
			EQ_Handle e2 = root_->add_EQ();
			e2.update_coef(gather_xform.output_var(i - 1), 1);
		}
		assign_const(gather_xform, 2 * tmp,
				get_const(copy(xform), 2 * tmp, Output_Var) + 1);

		assign_const(gather_xform, 0, get_const(copy(xform), 0, Output_Var));

		Statement s1;
		s1.xform = gather_xform;
		s1.IS = gather_IS;

		s1.code = gather_code;
		std::vector<LoopLevel> loops;
		for (int i = 0; i < r.n_set(); i++) {
			LoopLevel ll;
			ll.made_dense = false;
			ll.parallel_level = 0;
			ll.payload = i + 1;
			ll.type = LoopLevelOriginal;
			ll.segment_descriptor = "";
			ll.segreducible = false;
			loops.push_back(ll);
		}
		s1.loop_level = loops;
		s1.has_inspector = true;
		s1.ir_stmt_node = NULL;
		s1.reduction = 0;
		s1.reductionOp = IR_OP_UNKNOWN;
		new_stmt_num = stmt.size();
		stmt.push_back(s1);

		std::map<std::string, std::vector<CG_outputRepr *> > new_unin;

		for (std::map<std::string, std::vector<CG_outputRepr *> >::iterator it =
				uninterpreted_symbols[stmt_num].begin();
				it != uninterpreted_symbols[stmt_num].end(); it++) {
			std::vector<CG_outputRepr *> a;

			for (int i = 0; i < it->second.size(); i++)
				a.push_back(it->second[i]->clone());

			new_unin.insert(
					std::pair<std::string, std::vector<CG_outputRepr *> >(it->first,
							a));

		}

		uninterpreted_symbols.push_back(new_unin);
		uninterpreted_symbols_stringrepr.push_back(
				uninterpreted_symbols_stringrepr[stmt_num]);
		unin_rel.push_back(unin_rel[stmt_num]);
		this->dep.insert();
	}

	for (std::map<int, Relation>::iterator k = for_codegen.begin();
			k != for_codegen.end(); k++) {

		Relation r = k->second;
		if (is_negative) {
			Variable_ID v = r.set_var(1);
			r = flip_var(r, v);
		}
		Relation xform(r.n_set(), 2 * r.n_set() + 1);
		F_And *root2 = xform.add_and();
		for (int i = 2; i < 2 * r.n_set() + 1; i += 2) {
			EQ_Handle h = root2->add_EQ();
			h.update_coef(xform.output_var(i + 1), 1);
			EQ_Handle h2 = root2->add_EQ();
			h2.update_coef(xform.output_var(i), 1);
			h2.update_coef(xform.input_var(i / 2), -1);
		}

		EQ_Handle h = root2->add_EQ();
		h.update_coef(xform.output_var(1), 1);
		h.update_const(-lex[0] - 6);
		int dim = -1;
		for (int i = 1; i <= r.n_set(); i++)
			if (r.set_var(i)->name() == "ip") {
				dim = i;
				break;
			}
		Relation gather;
		CG_outputRepr *gather_code = ocg->CreateAssignment(0,
				ocg->CreateIdent("found"), ocg->CreateInt(0));

		std::set<int> single_iteration_loops;

		for (int i = 1; i <= r.n_set(); i++)
			if (is_single_loop_iteration(copy(r), i, copy(this->known)))
				single_iteration_loops.insert(i);

		int tmp = dim;

		while (single_iteration_loops.find(tmp) != single_iteration_loops.end()) {

			tmp--;
		}

		//if(single_iteration_loops.find(dim) != single_iteration_loops.end()){
		Relation gather_IS(tmp);
		for (int i = 1; i <= tmp; i++)
			gather_IS.name_set_var(i, r.set_var(i)->name());
		F_And * root__ = gather_IS.add_and();
		std::map<int, int> pos_map11;

		for (int i = 1; i <= tmp; i++)
			pos_map11.insert(std::pair<int, int>(i, i));

		for (int i = 1; i <= tmp; i++)
			gather_IS = replace_set_var_as_another_set_var(gather_IS, copy(r), i,
					i, pos_map11);

		Relation gather_xform(tmp, 2 * tmp + 1);
		F_And * root_ = gather_xform.add_and();
		for (int i = 2; i <= 2 * tmp; i += 2) {

			EQ_Handle e = root_->add_EQ();
			e.update_coef(gather_xform.input_var(i / 2), 1);
			e.update_coef(gather_xform.output_var(i), -1);
			EQ_Handle e2 = root_->add_EQ();
			e2.update_coef(gather_xform.output_var(i - 1), 1);
		}
		assign_const(gather_xform, 2 * tmp, 0);

		assign_const(gather_xform, 0, get_const(copy(xform), 0, Output_Var));

		Statement s1;
		s1.xform = gather_xform;
		s1.IS = gather_IS;

		s1.code = gather_code;
		std::vector<LoopLevel> loops;
		for (int i = 0; i < r.n_set(); i++) {
			LoopLevel ll;
			ll.made_dense = false;
			ll.parallel_level = 0;
			ll.payload = i + 1;
			ll.type = LoopLevelOriginal;
			ll.segment_descriptor = "";
			ll.segreducible = false;
			loops.push_back(ll);
		}
		s1.loop_level = loops;
		s1.has_inspector = true;
		s1.ir_stmt_node = NULL;
		s1.reduction = 0;
		s1.reductionOp = IR_OP_UNKNOWN;
		new_stmt_num = stmt.size();
		stmt.push_back(s1);

		std::map<std::string, std::vector<CG_outputRepr *> > new_unin;

		for (std::map<std::string, std::vector<CG_outputRepr *> >::iterator it =
				uninterpreted_symbols[stmt_num].begin();
				it != uninterpreted_symbols[stmt_num].end(); it++) {
			std::vector<CG_outputRepr *> a;

			for (int i = 0; i < it->second.size(); i++)
				a.push_back(it->second[i]->clone());

			new_unin.insert(
					std::pair<std::string, std::vector<CG_outputRepr *> >(it->first,
							a));

		}

		uninterpreted_symbols.push_back(new_unin);
		uninterpreted_symbols_stringrepr.push_back(
				uninterpreted_symbols_stringrepr[stmt_num]);
		unin_rel.push_back(unin_rel[stmt_num]);
		this->dep.insert();
	}
	for (std::map<int, Relation>::iterator k = for_codegen2.begin();
			k != for_codegen2.end(); k++) {
		Relation r = k->second;
		if (is_negative) {
			Variable_ID v = r.set_var(1);
			r = flip_var(r, v);
		}
		Relation xform(r.n_set(), 2 * r.n_set() + 1);
		F_And *root2 = xform.add_and();
		for (int i = 2; i < 2 * r.n_set() + 1; i += 2) {
			EQ_Handle h = root2->add_EQ();
			h.update_coef(xform.output_var(i + 1), 1);
			EQ_Handle h2 = root2->add_EQ();
			h2.update_coef(xform.output_var(i), 1);
			h2.update_coef(xform.input_var(i / 2), -1);
		}

		EQ_Handle h = root2->add_EQ();
		h.update_coef(xform.output_var(1), 1);
		h.update_const(-lex[0] - 6);
		Relation gather;
		CG_outputRepr *gather_code = ocg->CreateAssignment(0,
				ocg->CreateIdent("found"), ocg->CreateInt(0));

		std::set<int> single_iteration_loops;
		int dim = -1;
		for (int i = 1; i <= r.n_set(); i++)
			if (r.set_var(i)->name() == "ip") {
				dim = i;
				break;
			}
		for (int i = 1; i <= r.n_set(); i++)
			if (is_single_loop_iteration(copy(r), i, copy(this->known)))
				single_iteration_loops.insert(i);
		int tmp = dim;

		while (single_iteration_loops.find(tmp) != single_iteration_loops.end()) {

			tmp--;
		}

		//if(single_iteration_loops.find(dim) != single_iteration_loops.end()){
		Relation gather_IS(tmp);
		for (int i = 1; i <= tmp; i++)
			gather_IS.name_set_var(i, r.set_var(i)->name());
		F_And * root__ = gather_IS.add_and();
		std::map<int, int> pos_map12;

		for (int i = 1; i <= tmp; i++)
			pos_map12.insert(std::pair<int, int>(i, i));

		for (int i = 1; i <= tmp; i++)
			gather_IS = replace_set_var_as_another_set_var(gather_IS, copy(r), i,
					i, pos_map12);

		Relation gather_xform(tmp, 2 * tmp + 1);
		F_And * root_ = gather_xform.add_and();
		for (int i = 2; i <= 2 * tmp; i += 2) {

			EQ_Handle e = root_->add_EQ();
			e.update_coef(gather_xform.input_var(i / 2), 1);
			e.update_coef(gather_xform.output_var(i), -1);
			EQ_Handle e2 = root_->add_EQ();
			e2.update_coef(gather_xform.output_var(i - 1), 1);
		}
		assign_const(gather_xform, 2 * tmp, 0);
		//		get_const(copy(xform), 2 * tmp, Output_Var) + 1);

		assign_const(gather_xform, 0, get_const(copy(xform), 0, Output_Var));

		Statement s1;
		s1.xform = gather_xform;
		s1.IS = gather_IS;

		s1.code = gather_code;
		std::vector<LoopLevel> loops;
		for (int i = 0; i < r.n_set(); i++) {
			LoopLevel ll;
			ll.made_dense = false;
			ll.parallel_level = 0;
			ll.payload = i + 1;
			ll.type = LoopLevelOriginal;
			ll.segment_descriptor = "";
			ll.segreducible = false;
			loops.push_back(ll);
		}
		s1.loop_level = loops;
		s1.has_inspector = true;
		s1.ir_stmt_node = NULL;
		s1.reduction = 0;
		s1.reductionOp = IR_OP_UNKNOWN;
		new_stmt_num = stmt.size();
		stmt.push_back(s1);

		std::map<std::string, std::vector<CG_outputRepr *> > new_unin;

		for (std::map<std::string, std::vector<CG_outputRepr *> >::iterator it =
				uninterpreted_symbols[stmt_num].begin();
				it != uninterpreted_symbols[stmt_num].end(); it++) {
			std::vector<CG_outputRepr *> a;

			for (int i = 0; i < it->second.size(); i++)
				a.push_back(it->second[i]->clone());

			new_unin.insert(
					std::pair<std::string, std::vector<CG_outputRepr *> >(it->first,
							a));

		}

		uninterpreted_symbols.push_back(new_unin);
		uninterpreted_symbols_stringrepr.push_back(
				uninterpreted_symbols_stringrepr[stmt_num]);
		unin_rel.push_back(unin_rel[stmt_num]);
		this->dep.insert();
	}

	{
		/*
		 * 7. initialize sum;(0, NO OMP)
		 *	8.aggregate_count(loop, NO OMP)
		 * 9.realloc_col (0, NO OMP)
		 *10. initialize_offset (0, NO_OMP)
		 *11. populate_col2 (loop, NO OMP)
		 *12. free_and_reassign_col(no loop, NO OMP)
		 *13. sort_by_col (loop,OMP)
		 *14. clear_counter2 	(loop, OMP)
		 *15. symmetrify (loop, OMP)
		 *16. initialize_sum (no loop, no OMP)
		 *17. aggregate count (loop, NO OMP)
		 *18. realloc_col2 (no loop, NO OMP)
		 *19.initialize_offset (no loop, NO OMP)
		 *20.populate_col3 (loop, NO OMP)
		 *21.free_and_reassign_col2(no loop, NO OMP)
		 *22.symmetrify2  (loop, OMP)
		 *23.sort_by_col_final(loop, OMP)
		 */

		Relation IS(1);
		IS.name_set_var(1, name_.set_var(1)->name());
		Relation IS2(1);
		IS2.name_set_var(1, name_.set_var(1)->name());
		F_And *root = IS.add_and();

		std::map<int, int> pos_map13;

		for (int i = 1; i <= 1; i++)
			pos_map13.insert(std::pair<int, int>(i, i));
		IS = replace_set_var_as_another_set_var(IS, copy(stmt[stmt_num].IS), 1, 1,
				pos_map13);
		if (is_negative) {
			Variable_ID v = IS.set_var(1);
			IS = flip_var(IS, v);
		}

		F_And *root_ = IS2.add_and();

		GEQ_Handle e = root_->add_GEQ();
		e.update_coef(IS2.input_var(1), 1);

		GEQ_Handle e2 = root_->add_GEQ();
		e2.update_coef(IS2.input_var(1), -1);

		//    * 3. aggregate_rows(Loop NO OMP)

		Statement s1;
		Relation xform = simple_xform(-lex[0] - 7);
		std::vector<int> loop;
		s1 = construct_statement(stmt_num, copy(IS2), xform,
				wfdg->initialize_sum(), uninterpreted_symbols,
				uninterpreted_symbols_stringrepr, unin_rel, this->dep);
		new_stmt_num = stmt.size();
		stmt.push_back(s1);

		xform = simple_xform(-lex[0] - 8);

		s1 = construct_statement(stmt_num, copy(IS), xform,
				wfdg->aggregate_count(ident), uninterpreted_symbols,
				uninterpreted_symbols_stringrepr, unin_rel, this->dep);
		new_stmt_num = stmt.size();
		stmt.push_back(s1);

		xform = simple_xform(-lex[0] - 9);

		s1 = construct_statement(stmt_num, copy(IS2), xform, wfdg->realloc_col(),
				uninterpreted_symbols, uninterpreted_symbols_stringrepr, unin_rel,
				this->dep);
		new_stmt_num = stmt.size();
		stmt.push_back(s1);

		xform = simple_xform(-lex[0] - 10);

		s1 = construct_statement(stmt_num, copy(IS2), xform,
				wfdg->initialize_offset(), uninterpreted_symbols,
				uninterpreted_symbols_stringrepr, unin_rel, this->dep);
		new_stmt_num = stmt.size();
		stmt.push_back(s1);

		xform = simple_xform(-lex[0] - 11);

		s1 = construct_statement(stmt_num, copy(IS), xform,
				wfdg->populate_col2(ident), uninterpreted_symbols,
				uninterpreted_symbols_stringrepr, unin_rel, this->dep);
		new_stmt_num = stmt.size();
		stmt.push_back(s1);

		xform = simple_xform(-lex[0] - 12);

		s1 = construct_statement(stmt_num, copy(IS2), xform,
				wfdg->free_and_reassign_col(), uninterpreted_symbols,
				uninterpreted_symbols_stringrepr, unin_rel, this->dep);
		new_stmt_num = stmt.size();
		stmt.push_back(s1);

		xform = simple_xform(-lex[0] - 13);

		s1 = construct_statement(stmt_num, copy(IS), xform,
				wfdg->sort_by_col(ident), uninterpreted_symbols,
				uninterpreted_symbols_stringrepr, unin_rel, this->dep);
		loop.push_back(1);
		new_stmt_num = stmt.size();
		mark_omp_threads(new_stmt_num, loop);
		stmt.push_back(s1);

		xform = simple_xform(-lex[0] - 14);

		s1 = construct_statement(stmt_num, copy(IS), xform,
				wfdg->clear_counter2(ident), uninterpreted_symbols,
				uninterpreted_symbols_stringrepr, unin_rel, this->dep);
		//loop.push_back(1);
		new_stmt_num = stmt.size();
		mark_omp_threads(new_stmt_num, loop);
		stmt.push_back(s1);

		xform = simple_xform(-lex[0] - 15);

		s1 = construct_statement(stmt_num, copy(IS), xform,
				wfdg->symmetrify(ident), uninterpreted_symbols,
				uninterpreted_symbols_stringrepr, unin_rel, this->dep);
		//loop.push_back(1);
		new_stmt_num = stmt.size();
		mark_omp_threads(new_stmt_num, loop);
	        std::vector<std::string> omp_prv_str1;
                omp_prv_str1.push_back("c");
                //omp_prv_str1.push_back("cnt");
                mark_omp_private(new_stmt_num, omp_prv_str1);
  
         	stmt.push_back(s1);

		xform = simple_xform(-lex[0] - 16);

		s1 = construct_statement(stmt_num, copy(IS2), xform,
				wfdg->initialize_sum(), uninterpreted_symbols,
				uninterpreted_symbols_stringrepr, unin_rel, this->dep);
		//loop.push_back(1);
		new_stmt_num = stmt.size();
		//mark_omp_threads(new_stmt_num, loop);
		stmt.push_back(s1);

		xform = simple_xform(-lex[0] - 17);

		s1 = construct_statement(stmt_num, copy(IS), xform,
				wfdg->aggregate_count2(ident), uninterpreted_symbols,
				uninterpreted_symbols_stringrepr, unin_rel, this->dep);
		//loop.push_back(1);
		new_stmt_num = stmt.size();
		//mark_omp_threads(new_stmt_num, loop);
		stmt.push_back(s1);
		xform = simple_xform(-lex[0] - 18);

		s1 = construct_statement(stmt_num, copy(IS2), xform, wfdg->realloc_col2(),
				uninterpreted_symbols, uninterpreted_symbols_stringrepr, unin_rel,
				this->dep);
		//loop.push_back(1);
		new_stmt_num = stmt.size();
		//mark_omp_threads(new_stmt_num, loop);
		stmt.push_back(s1);

		xform = simple_xform(-lex[0] - 19);

		s1 = construct_statement(stmt_num, copy(IS2), xform,
				wfdg->initialize_offset(), uninterpreted_symbols,
				uninterpreted_symbols_stringrepr, unin_rel, this->dep);
		//loop.push_back(1);
		new_stmt_num = stmt.size();
		//mark_omp_threads(new_stmt_num, loop);
		stmt.push_back(s1);

		xform = simple_xform(-lex[0] - 20);

		s1 = construct_statement(stmt_num, copy(IS), xform,
				wfdg->populate_col3(ident), uninterpreted_symbols,
				uninterpreted_symbols_stringrepr, unin_rel, this->dep);
		//loop.push_back(1);
		new_stmt_num = stmt.size();
		//mark_omp_threads(new_stmt_num, loop);
		stmt.push_back(s1);

		xform = simple_xform(-lex[0] - 21);

		s1 = construct_statement(stmt_num, copy(IS2), xform,
				wfdg->free_and_reassign_col2(), uninterpreted_symbols,
				uninterpreted_symbols_stringrepr, unin_rel, this->dep);
		//loop.push_back(1);
		new_stmt_num = stmt.size();
		//mark_omp_threads(new_stmt_num, loop);
		stmt.push_back(s1);

		xform = simple_xform(-lex[0] - 22);

		s1 = construct_statement(stmt_num, copy(IS), xform,
				wfdg->symmetrify2(ident), uninterpreted_symbols,
				uninterpreted_symbols_stringrepr, unin_rel, this->dep);
		//loop.push_back(1);
		new_stmt_num = stmt.size();
		mark_omp_threads(new_stmt_num, loop);
		stmt.push_back(s1);
                std::vector<std::string> omp_prv_str2;
                omp_prv_str2.push_back("c");
                omp_prv_str2.push_back("cnt");

                mark_omp_private(new_stmt_num, omp_prv_str2);


		xform = simple_xform(-lex[0] - 23);

		s1 = construct_statement(stmt_num, copy(IS), xform,
				wfdg->sort_by_col_final(ident), uninterpreted_symbols,
				uninterpreted_symbols_stringrepr, unin_rel, this->dep);
		//loop.push_back(1);
		new_stmt_num = stmt.size();
		mark_omp_threads(new_stmt_num, loop);
		stmt.push_back(s1);

	}

	/*	std::vector<LoopLevel> loops;
	 for (int i = 0; i < 3; i++) {
	 LoopLevel ll;
	 ll.made_dense = false;/
	 ll.parallel_level = 0;
	 ll.payload = i + 1;
	 ll.type = LoopLevelOriginal;
	 ll.segment_descriptor = "";
	 ll.segreducible = false;
	 loops.push_back(ll);
	 }
	 s1.loop_level = loops;
	 s1.has_inspector = true;
	 s1.ir_stmt_node = NULL;
	 s1.reduction = 0;
	 s1.reductionOp = IR_OP_UNKNOWN;
	 int new_stmt_num = stmt.size();
	 stmt.push_back(s1);

	 std::map<std::string, std::vector<CG_outputRepr *> > new_unin;

	 for (std::map<std::string, std::vector<CG_outputRepr *> >::iterator it =
	 uninterpreted_symbols[stmt_num].begin();
	 it != uninterpreted_symbols[stmt_num].end(); it++) {
	 std::vector<CG_outputRepr *> a;

	 for (int i = 0; i < it->second.size(); i++)
	 a.push_back(it->second[i]->clone());

	 new_unin.insert(
	 std::pair<std::string, std::vector<CG_outputRepr *> >(it->first,
	 a));

	 }

	 uninterpreted_symbols.push_back(new_unin);
	 uninterpreted_symbols_stringrepr.push_back(
	 uninterpreted_symbols_stringrepr[stmt_num]);
	 unin_rel.push_back(unin_rel[stmt_num]);
	 s2.loop_level = loops;
	 s2.has_inspector = true;
	 s2.ir_stmt_node = NULL;
	 s2.reduction = 0;
	 s2.reductionOp = IR_OP_UNKNOWN;
	 stmt.push_back(s2);

	 std::map<std::string, std::vector<CG_outputRepr *> > new_unin2;

	 for (std::map<std::string, std::vector<CG_outputRepr *> >::iterator it =
	 uninterpreted_symbols[stmt_num].begin();
	 it != uninterpreted_symbols[stmt_num].end(); it++) {
	 std::vector<CG_outputRepr *> a;

	 for (int i = 0; i < it->second.size(); i++)
	 a.push_back(it->second[i]->clone());

	 new_unin2.insert(
	 std::pair<std::string, std::vector<CG_outputRepr *> >(it->first,
	 a));

	 }

	 uninterpreted_symbols.push_back(new_unin2);
	 uninterpreted_symbols_stringrepr.push_back(
	 uninterpreted_symbols_stringrepr[stmt_num]);
	 unin_rel.push_back(unin_rel[stmt_num]);
	 Statement s2_;
	 s2_.loop_level = loops;
	 s2_.has_inspector = true;
	 s2_.ir_stmt_node = NULL;
	 s2_.reduction = 0;
	 s2_.reductionOp = IR_OP_UNKNOWN;
	 s2_.IS = copy(s1.IS);
	 s2_.code = codes[0]->clone();
	 s2_.xform = xforms[0];

	 stmt.push_back(s2_);

	 std::map<std::string, std::vector<CG_outputRepr *> > new_unin2_;

	 for (std::map<std::string, std::vector<CG_outputRepr *> >::iterator it =
	 uninterpreted_symbols[stmt_num].begin();
	 it != uninterpreted_symbols[stmt_num].end(); it++) {
	 std::vector<CG_outputRepr *> a;

	 for (int i = 0; i < it->second.size(); i++)
	 a.push_back(it->second[i]->clone());

	 new_unin2_.insert(
	 std::pair<std::string, std::vector<CG_outputRepr *> >(it->first,
	 a));

	 }

	 uninterpreted_symbols.push_back(new_unin2_);
	 uninterpreted_symbols_stringrepr.push_back(
	 uninterpreted_symbols_stringrepr[stmt_num]);
	 unin_rel.push_back(unin_rel[stmt_num]);

	 Statement s2__;
	 s2__.loop_level = loops;
	 s2__.has_inspector = true;
	 s2__.ir_stmt_node = NULL;
	 s2__.reduction = 0;
	 s2__.reductionOp = IR_OP_UNKNOWN;
	 s2__.IS = copy(s2.IS);
	 s2__.code = codes[0]->clone();
	 s2__.xform = xforms[0];

	 stmt.push_back(s2__);

	 std::map<std::string, std::vector<CG_outputRepr *> > new_unin2__;

	 for (std::map<std::string, std::vector<CG_outputRepr *> >::iterator it =
	 uninterpreted_symbols[stmt_num].begin();
	 it != uninterpreted_symbols[stmt_num].end(); it++) {
	 std::vector<CG_outputRepr *> a;

	 for (int i = 0; i < it->second.size(); i++)
	 a.push_back(it->second[i]->clone());

	 new_unin2__.insert(
	 std::pair<std::string, std::vector<CG_outputRepr *> >(it->first,
	 a));

	 }

	 uninterpreted_symbols.push_back(new_unin2__);
	 uninterpreted_symbols_stringrepr.push_back(
	 uninterpreted_symbols_stringrepr[stmt_num]);
	 unin_rel.push_back(unin_rel[stmt_num]);

	 Statement s2a;
	 s2a.loop_level = loops;
	 s2a.has_inspector = true;
	 s2a.ir_stmt_node = NULL;
	 s2a.reduction = 0;
	 s2a.reductionOp = IR_OP_UNKNOWN;
	 s2a.IS = gather_IS;
	 s2a.code = gather_code;
	 s2a.xform = gather_xform;

	 stmt.push_back(s2a);

	 std::map<std::string, std::vector<CG_outputRepr *> > new_unin2a;

	 for (std::map<std::string, std::vector<CG_outputRepr *> >::iterator it =
	 uninterpreted_symbols[stmt_num].begin();
	 it != uninterpreted_symbols[stmt_num].end(); it++) {
	 std::vector<CG_outputRepr *> a;

	 for (int i = 0; i < it->second.size(); i++)
	 a.push_back(it->second[i]->clone());

	 new_unin2a.insert(
	 std::pair<std::string, std::vector<CG_outputRepr *> >(it->first,
	 a));

	 }

	 uninterpreted_symbols.push_back(new_unin2a);
	 uninterpreted_symbols_stringrepr.push_back(
	 uninterpreted_symbols_stringrepr[stmt_num]);
	 unin_rel.push_back(unin_rel[stmt_num]);
	 */
	/*if (is_negative) {

	 //stmt[new_stmt_num].code = ocg->CreateSubstitutedStmt(0, stmt[new_stmt_num].code, vars_to_be_reversed,
	 //		reverse1);
	 //stmt[new_stmt_num+1].code = ocg->CreateSubstitutedStmt(0, stmt[new_stmt_num+1].code, vars_to_be_reversed,
	 //		reverse2);
	 for (int k = new_stmt_num; k <= new_stmt_num + 1; k++)
	 for (std::map<std::string, std::vector<CG_outputRepr *> >::iterator iter =
	 uninterpreted_symbols[k].begin();
	 iter != uninterpreted_symbols[k].end(); iter++) {
	 if (iter->second.size() > 0) {
	 for (int l = 0; l < freevar.size(); l++) {
	 int arity;
	 if (freevar[l]->base_name() == iter->first) {
	 arity = freevar[l]->arity();
	 if (arity >= 1
	 && freevar[l]->base_name()
	 == freevar_name) {
	 std::vector<CG_outputRepr *> tmp = iter->second;
	 std::vector<CG_outputRepr *> reverse;
	 CG_outputRepr *repl = ocg->CreateIdent(
	 s1.IS.set_var(1)->name());
	 repl = ocg->CreateMinus(NULL, repl);
	 reverse.push_back(repl->clone());
	 std::vector<std::string> tmp2;
	 tmp2.push_back(s1.IS.set_var(1)->name());
	 tmp[0] = ocg->CreateSubstitutedStmt(0,
	 tmp[0]->clone(), tmp2, reverse);

	 iter->second[0] = tmp[0];
	 break;

	 }
	 }
	 }
	 }
	 }

	 }

	 */

/*
{
                Statement s2;
                Relation IS2(1);
                Relation xform2(1, 3);
                IS2.name_set_var(1, "i");

                F_And *root3 = IS2.add_and();
                EQ_Handle h5 = root3->add_EQ();
                h5.update_coef(IS2.set_var(1), 1);

                F_And *root4 = xform2.add_and();
                EQ_Handle h6 = root4->add_EQ();

                h6.update_coef(xform2.output_var(2), 1);
                h6.update_coef(xform2.input_var(1), -1);

                EQ_Handle h7 = root4->add_EQ();
                h7.update_coef(xform2.output_var(3), 1);
                std::vector<int> lex2 = getLexicalOrder(stmt_num);
                shiftLexicalOrder(lex2, 0, 1);
                EQ_Handle h8 = root4->add_EQ();
                h8.update_coef(xform2.output_var(1), 1);
                h8.update_const(-lex2[0]);


                s2.IS = IS2;
                s2.xform = xform2;
                std::vector<LoopLevel> loops2;

                LoopLevel ll;
                ll.made_dense = false;
                ll.parallel_level = 0;
                ll.payload = 1;
                ll.type = LoopLevelOriginal;
                ll.segment_descriptor = "";
                ll.segreducible = false;
                loops2.push_back(ll);

                s2.loop_level = loops2;
                s2.has_inspector = true;
                s2.ir_stmt_node = NULL;
                s2.reduction = 0;
                s2.reductionOp = IR_OP_UNKNOWN;
                s2.code = ocg->CreateAssignment(0, inspection_code(), ocg->CreateInt(1));
                stmt.push_back(s2);
                uninterpreted_symbols.push_back(uninterpreted_symbols[stmt_num]);
                uninterpreted_symbols_stringrepr.push_back(
                                uninterpreted_symbols_stringrepr[stmt_num]);
                unin_rel.push_back(unin_rel[stmt_num]);

}
*/
	{	//2. Statement that constructs task graph
		Statement s2;
		Relation IS2(1);
		Relation xform2(1, 3);
		IS2.name_set_var(1, "i");

		F_And *root3 = IS2.add_and();
		EQ_Handle h5 = root3->add_EQ();
		h5.update_coef(IS2.set_var(1), 1);

		F_And *root4 = xform2.add_and();
		EQ_Handle h6 = root4->add_EQ();

		h6.update_coef(xform2.output_var(2), 1);
		h6.update_coef(xform2.input_var(1), -1);

		EQ_Handle h7 = root4->add_EQ();
		h7.update_coef(xform2.output_var(3), 1);
		std::vector<int> lex2 = getLexicalOrder(stmt_num);
		shiftLexicalOrder(lex2, 0, 1);
		EQ_Handle h8 = root4->add_EQ();
		h8.update_coef(xform2.output_var(1), 1);
		h8.update_const(-lex2[0]);

		std::vector<CG_outputRepr *> args2;
//std::string schedule = "schedule";
		args2.push_back(ocg->CreateIdent(dep));
//args2.push_back(ocg->CreateIdent(schedule));
		s2.code = wfsc->build_task_graph(wfdg); //ocg->CreateInvoke(perm_func_name, args2);

		s2.IS = IS2;
		s2.xform = xform2;
		std::vector<LoopLevel> loops2;

		LoopLevel ll;
		ll.made_dense = false;
		ll.parallel_level = 0;
		ll.payload = 1;
		ll.type = LoopLevelOriginal;
		ll.segment_descriptor = "";
		ll.segreducible = false;
		loops2.push_back(ll);

		s2.loop_level = loops2;
		s2.has_inspector = true;
		s2.ir_stmt_node = NULL;
		s2.reduction = 0;
		s2.reductionOp = IR_OP_UNKNOWN;

		stmt.push_back(s2);
		uninterpreted_symbols.push_back(uninterpreted_symbols[stmt_num]);
		uninterpreted_symbols_stringrepr.push_back(
				uninterpreted_symbols_stringrepr[stmt_num]);

		unin_rel.push_back(unin_rel[stmt_num]);

		permutation_symbols.push_back(
				std::pair<std::string, std::string>(perm, perm + "_inv"));
	}
	std::vector<CG_outputRepr *> dummy, dummy2;
	dummy.push_back(ir->builder()->CreateInt(0));
	dummy2.push_back(ir->builder()->CreateInt(0));
	IR_PointerSymbol *p = ir->CreatePointerSymbol(IR_CONSTANT_INT, dummy, perm);
	IR_PointerSymbol *p2 = ir->CreatePointerSymbol(IR_CONSTANT_INT, dummy2,
			perm + "_inv");
	CG_outputRepr *perm_stmt = ocg->CreateAssignment(0, ocg->CreateIdent(perm),
			wfsc->perm());
	CG_outputRepr *inv_perm_stmt = ocg->CreateAssignment(0,
			ocg->CreateIdent(p2->name()), wfsc->inv_perm());
	perm_stmt = ocg->StmtListAppend(perm_stmt, inv_perm_stmt);
	for (std::map<std::string, std::vector<CG_outputRepr *> >::iterator i =
			uninterpreted_symbols[stmt_num].begin();
			i != uninterpreted_symbols[stmt_num].end(); i++) {
		std::vector<CG_outputRepr *> args = i->second;

		for (int j = 0; j < args.size(); j++) {

			std::vector<IR_ScalarRef *> refs = ir->FindScalarRef(args[j]);
			bool found = false;
			for (int k = 0; k < refs.size(); k++)
				if (refs[k]->name() == stmt[0].IS.set_var(1)->name())
					found = true;

			if (found) {
				std::vector<std::string> loop_vars;
				std::vector<CG_outputRepr *> subs;
				//for (int j = 1; j <= stmt[*i].IS.n_set(); j++)
				loop_vars.push_back(stmt[0].IS.set_var(1)->name());
				if (!is_negative)
					subs.push_back(
							ir->builder()->CreateArrayRefExpression(p->name(),
									ocg->CreateIdent(
											stmt[stmt_num].IS.set_var(1)->name())));
				else
					subs.push_back(
							ocg->CreateMinus(NULL,
									ir->builder()->CreateArrayRefExpression(p->name(),
											ocg->CreateMinus( NULL,
													ocg->CreateIdent(
															stmt[stmt_num].IS.set_var(1)->name())))));

				args[j] = ir->builder()->CreateSubstitutedStmt(0, args[j],
						loop_vars, subs, false);
				std::map<std::string, std::vector<Relation> >::iterator iter =
						unin_rel[stmt_num].find(i->first);
				std::vector<Relation> to_push = iter->second;
				Relation r(stmt[stmt_num].IS.n_set(), 1);
				F_And *f_root = r.add_and();
				EQ_Handle e = f_root->add_EQ();
				e.update_coef(r.input_var(1), 1);
				e.update_coef(r.output_var(1), -1);
				r.simplify();
				to_push.push_back(r);
				iter->second = to_push;

			}
		}

		i->second = args;

	}

	std::vector<std::string> loop_vars;
	std::vector<CG_outputRepr *> subs;
//for (int j = 1; j <= stmt[*i].IS.n_set(); j++)
	loop_vars.push_back(stmt[0].IS.set_var(1)->name());

	subs.push_back(
			ir->builder()->CreateArrayRefExpression(p->name(),
					ocg->CreateIdent(stmt[stmt_num].IS.set_var(1)->name())));
	stmt[stmt_num].code = ir->builder()->CreateSubstitutedStmt(0,
			stmt[stmt_num].code, loop_vars, subs, false);

	{
		Statement s2;
		Relation IS2(1);
		Relation xform2(1, 3);
		IS2.name_set_var(1, "i");

		F_And *root3 = IS2.add_and();
		EQ_Handle h5 = root3->add_EQ();
		h5.update_coef(IS2.set_var(1), 1);

		F_And *root4 = xform2.add_and();
		EQ_Handle h6 = root4->add_EQ();

		h6.update_coef(xform2.output_var(2), 1);
		h6.update_coef(xform2.input_var(1), -1);

		EQ_Handle h7 = root4->add_EQ();
		h7.update_coef(xform2.output_var(3), 1);
		std::vector<int> lex2 = getLexicalOrder(stmt_num);
		shiftLexicalOrder(lex2, 0, 1);
		EQ_Handle h8 = root4->add_EQ();
		h8.update_coef(xform2.output_var(1), 1);
		h8.update_const(-lex2[0]);

		s2.IS = IS2;
		s2.xform = xform2;
		std::vector<LoopLevel> loops2;

		LoopLevel ll;
		ll.made_dense = false;
		ll.parallel_level = 0;
		ll.payload = 1;
		ll.type = LoopLevelOriginal;
		ll.segment_descriptor = "";
		ll.segreducible = false;
		loops2.push_back(ll);

		s2.loop_level = loops2;
		s2.has_inspector = false;
		s2.ir_stmt_node = NULL;
		s2.reduction = 0;
		s2.reductionOp = IR_OP_UNKNOWN;

		uninterpreted_symbols.push_back(uninterpreted_symbols[stmt_num]);
		uninterpreted_symbols_stringrepr.push_back(
				uninterpreted_symbols_stringrepr[stmt_num]);
		unin_rel.push_back(unin_rel[stmt_num]);
		s2.code = perm_stmt;
		stmt.push_back(s2);
	}

	this->dep.insert();
	this->dep.insert();
	//this->dep.insert();
}

void Loop::sparse_wavefront(int stmt_num, int level,
		bool parallel_threaded_execution) {

	std::string obj_name = wfsc->name();
	std::string name = wfsc->wavefront_boundaries();

	CG_outputRepr *def =
			static_cast<CG_roseBuilder *>(ir->builder())->CreateDotExpression(
					(ir->builder())->CreateIdent(wfsc->name()),
					(ir->builder())->CreateArrayRefExpression(
							static_cast<CG_roseBuilder *>(ir->builder())->lookup_member_data(
									dynamic_cast<wavefront_intel_schedule *>(wfsc)->repr2,
									name, (ir->builder())->CreateIdent(wfsc->name())),
							ir->builder()->CreateIdent("i")));
	CG_outputRepr *def2 =
			static_cast<CG_roseBuilder *>(ir->builder())->CreateDotExpression(
					(ir->builder())->CreateIdent(wfsc->name()),
					(ir->builder())->CreateArrayRefExpression(
							static_cast<CG_roseBuilder *>(ir->builder())->lookup_member_data(
									dynamic_cast<wavefront_intel_schedule *>(wfsc)->repr2,
									name, (ir->builder())->CreateIdent(wfsc->name())),
							ir->builder()->CreatePlus(ir->builder()->CreateIdent("i"),
									ir->builder()->CreateInt(1))));
	dynamic_cast<IR_roseCode *>(ir)->CreateDefineMacro(name + "_", "(i)",
			def->clone());
	dynamic_cast<IR_roseCode *>(ir)->CreateDefineMacro(name + "__", "(i)",
			def2->clone());
	sparse_partition(stmt_num, level, name);

	if (parallel_threaded_execution) {

		std::string name = wfsc->threadBoundaries();

		CG_outputRepr *def =
				static_cast<CG_roseBuilder *>(ir->builder())->CreateDotExpression(
						(ir->builder())->CreateIdent(wfsc->name()),
						(ir->builder())->CreateArrayRefExpression(
								static_cast<CG_roseBuilder *>(ir->builder())->lookup_member_data(
										dynamic_cast<wavefront_intel_schedule *>(wfsc)->repr2,
										name, (ir->builder())->CreateIdent(wfsc->name())),
								ir->builder()->CreateIdent("i")));
		CG_outputRepr *def2 =
				static_cast<CG_roseBuilder *>(ir->builder())->CreateDotExpression(
						(ir->builder())->CreateIdent(wfsc->name()),
						(ir->builder())->CreateArrayRefExpression(
								static_cast<CG_roseBuilder *>(ir->builder())->lookup_member_data(
										dynamic_cast<wavefront_intel_schedule *>(wfsc)->repr2,
										name, (ir->builder())->CreateIdent(wfsc->name())),
								ir->builder()->CreatePlus(
										ir->builder()->CreateIdent("i"),
										ir->builder()->CreateInt(1))));
		dynamic_cast<IR_roseCode *>(ir)->CreateDefineMacro(name + "_", "(i)",
				def->clone());
		dynamic_cast<IR_roseCode *>(ir)->CreateDefineMacro(name + "__", "(i)",
				def2->clone());
		sparse_partition(stmt_num, level, name, true);

	}

}

void Loop::sparse_partition(int stmt_num, int level, std::string name,
		bool suppress_negative) {

// get stmt IS and XFORM

// check that level is the first loop level carrying a dependence
	if (level != 1)
		throw loop_error(
				"Sparse partition only supports outer most level for now!");

//
	std::vector<int> lex0 = getLexicalOrder(stmt_num);
	std::set<int> same_loop = getStatements(lex0, level - 1);

	/*for(std::map< std::string, std::vector<Relation > >::iterator iter  = unin_rel[0].begin(); iter != unin_rel[0].end(); iter ++){
	 std::cout<<iter->first<<std::endl;
	 std::vector<Relation> to_push = iter->second;
	 for(int l=0; l < to_push.size(); l++){
	 copy(to_push[l]).print();
	 }
	 }
	 */

	bool is_negative = false;

	Variable_ID v2 = stmt[stmt_num].IS.set_var(1);
	for (DNF_Iterator di(const_cast<Relation &>(stmt[stmt_num].IS).query_DNF());
			di; di++)
		for (GEQ_Iterator gi((*di)->GEQs()); gi; gi++)
			if ((*gi).is_const_except_for_global(v2))
				if ((*gi).get_coef(v2) < 0 && (*gi).get_const() == 0)
					is_negative = true;

	/*
	 CG_outputBuilder *ocg = ir->builder();

	 //1. Statement that constructs Dependence graph
	 Statement s1;

	 std::vector<CG_outputRepr *> args;
	 std::string dep = "A";

	 Relation IS(3);
	 IS.name_set_var(1, "i");
	 IS.name_set_var(2, "j");
	 IS.name_set_var(3, "i_");

	 {
	 int ub = -1, lb = -1, n = -1;
	 for (int i = 0; i < freevar.size(); i++) {
	 if (freevar[i]->base_name() == std::string("rowptr_"))
	 lb = i;
	 else if (freevar[i]->base_name() == std::string("diagptr_"))
	 ub = i;
	 else if (freevar[i]->base_name() == std::string("m"))
	 n = i;
	 }

	 assert((ub >= 0 && lb >= 0) && n >= 0);
	 F_And *root = IS.add_and();

	 GEQ_Handle h = root->add_GEQ();
	 h.update_coef(IS.set_var(1), 1);

	 GEQ_Handle h2 = root->add_GEQ();
	 h2.update_coef(IS.set_var(1), -1);
	 Variable_ID ub1 = IS.get_local(freevar[n]);
	 h2.update_coef(ub1, 1);
	 h2.update_const(-1);

	 GEQ_Handle h9 = root->add_GEQ();
	 h9.update_coef(IS.set_var(3), 1);

	 GEQ_Handle h3 = root->add_GEQ();
	 h3.update_coef(IS.set_var(3), -1);
	 //Variable_ID ub = IS.get_local(freevar[n]);
	 h3.update_coef(ub1, 1);
	 h3.update_const(-1);

	 GEQ_Handle h4 = root->add_GEQ();
	 h4.update_coef(IS.set_var(2), -1);
	 Variable_ID ub2 = IS.get_local(freevar[ub], Input_Tuple);
	 h4.update_coef(ub2, 1);
	 h4.update_const(-1);

	 GEQ_Handle h5 = root->add_GEQ();
	 h5.update_coef(IS.set_var(2), 1);
	 Variable_ID lb1 = IS.get_local(freevar[lb], Input_Tuple);
	 h5.update_coef(lb1, -1);

	 }
	 args.push_back(ocg->CreateIdent(dep));
	 args.push_back(ocg->CreateIdent(stmt[stmt_num].IS.set_var(1)->name()));
	 args.push_back(
	 ocg->CreateIdent(stmt[stmt_num].IS.set_var(1)->name() + "_"));

	 s1.code = ocg->CreateInvoke("connect", args);

	 std::vector<int> lex = getLexicalOrder(stmt_num);
	 shiftLexicalOrder(lex, 0, 1);
	 Relation xform(3,7);
	 F_And *root2 = xform.add_and();
	 for (int i = 2; i < 7; i += 2) {
	 EQ_Handle h = root2->add_EQ();
	 h.update_coef(xform.output_var(i+1), 1);
	 EQ_Handle h2 = root2->add_EQ();
	 h2.update_coef(xform.output_var(i), 1);
	 h2.update_coef(xform.input_var(i / 2), -1);
	 }

	 EQ_Handle h = root2->add_EQ();
	 h.update_coef(xform.output_var(1), 1);
	 h.update_const(-lex[0]);
	 s1.IS = IS;
	 s1.xform = xform;
	 std::vector<LoopLevel> loops;
	 for (int i = 0; i < 3; i++) {
	 LoopLevel ll;
	 ll.made_dense = false;
	 ll.parallel_level = 0;
	 ll.payload = i + 1;
	 ll.type = LoopLevelOriginal;
	 ll.segment_descriptor = "";
	 ll.segreducible = false;
	 loops.push_back(ll);
	 }
	 s1.loop_level = loops;
	 s1.has_inspector = true;
	 s1.ir_stmt_node = NULL;
	 s1.reduction = 0;
	 s1.reductionOp = IR_OP_UNKNOWN;

	 //2. Statement that constructs task graph
	 Statement s2;
	 Relation IS2(1);
	 Relation xform2(1,3);
	 IS2.name_set_var(1, "i");

	 F_And *root3 = IS2.add_and();
	 EQ_Handle h5 = root3->add_EQ();
	 h5.update_coef(IS2.set_var(1), 1);

	 F_And *root4 = xform2.add_and();
	 EQ_Handle h6 = root4->add_EQ();

	 h6.update_coef(xform2.output_var(2), 1);
	 h6.update_coef(xform2.input_var(1), -1);

	 EQ_Handle h7 = root4->add_EQ();
	 h7.update_coef(xform2.output_var(3), 1);
	 std::vector<int> lex2 = getLexicalOrder(stmt_num);
	 shiftLexicalOrder(lex2, 0, 1);
	 EQ_Handle h8 = root4->add_EQ();
	 h8.update_coef(xform2.output_var(1), 1);
	 h8.update_const(-lex2[0]);

	 std::vector<CG_outputRepr *> args2;
	 std::string schedule = "schedule";
	 args2.push_back(ocg->CreateIdent(dep));
	 args2.push_back(ocg->CreateIdent(schedule));
	 s2.code = ocg->CreateInvoke("create_task_graph", args2);

	 s2.IS = IS2;
	 s2.xform = xform2;
	 std::vector<LoopLevel> loops2;

	 LoopLevel ll;
	 ll.made_dense = false;
	 ll.parallel_level = 0;
	 ll.payload = 1;
	 ll.type = LoopLevelOriginal;
	 ll.segment_descriptor = "";
	 ll.segreducible = false;
	 loops2.push_back(ll);

	 s2.loop_level = loops;
	 s2.has_inspector = true;
	 s2.ir_stmt_node = NULL;
	 s2.reduction = 0;
	 s2.reductionOp = IR_OP_UNKNOWN;


	 stmt.push_back(s1);
	 uninterpreted_symbols.push_back(
	 uninterpreted_symbols[stmt_num]);
	 uninterpreted_symbols_stringrepr.push_back(
	 uninterpreted_symbols_stringrepr[stmt_num]);
	 unin_rel.push_back(unin_rel[stmt_num]);
	 stmt.push_back(s2);
	 uninterpreted_symbols.push_back(
	 uninterpreted_symbols[stmt_num]);
	 uninterpreted_symbols_stringrepr.push_back(
	 uninterpreted_symbols_stringrepr[stmt_num]);
	 unin_rel.push_back(unin_rel[stmt_num]);
	 this->dep.insert();
	 this->dep.insert();
	 */

//3. Executor, define task and threadboundaries
	std::vector<int> lex2 = getLexicalOrder(stmt_num);
	Free_Var_Decl *ub = new Free_Var_Decl("ub");
	Free_Var_Decl *ub2 = new Free_Var_Decl(name + "_", 1);
	Free_Var_Decl *ub3 = new Free_Var_Decl(name + "__", 1);
	for (std::set<int>::iterator it = same_loop.begin(); it != same_loop.end();
			it++) {

		apply_xform(*it);

//Relation new_IS(stmt[stmt_num].IS.n_set() + 1);
//Relation new_xform(stmt[stmt_num].xform.n_inp() + 1, stmt[stmt_num].xform.n_out() + 2);

		Relation outer_loop(1);

		if (!is_negative) {
			F_And *f_root = outer_loop.add_and();
			GEQ_Handle h = f_root->add_GEQ();
			h.update_coef(outer_loop.set_var(1), 1);
			GEQ_Handle h2 = f_root->add_GEQ();
			h2.update_coef(outer_loop.set_var(1), -1);
			Variable_ID csr_ub = outer_loop.get_local(ub);

			h2.update_coef(csr_ub, 1);
			h2.update_const(1);
		} else {
			F_And *f_root = outer_loop.add_and();
			GEQ_Handle h = f_root->add_GEQ();
			h.update_coef(outer_loop.set_var(1), -1);
			GEQ_Handle h2 = f_root->add_GEQ();
			h2.update_coef(outer_loop.set_var(1), 1);
			Variable_ID csr_ub = outer_loop.get_local(ub);

			h2.update_coef(csr_ub, 1);
			h2.update_const(-1);
		}

		stmt[*it].IS.get_local(ub);
		Relation partial_IS = replicate_IS_and_add_at_pos(stmt[*it].IS, 1,
				outer_loop);

		Relation outer_loops(2);
		F_And *f_root2 = outer_loops.add_and();
		if (!is_negative) {

			GEQ_Handle h3 = f_root2->add_GEQ();
			h3.update_coef(outer_loops.set_var(1), 1);
			GEQ_Handle h4 = f_root2->add_GEQ();
			h4.update_coef(outer_loops.set_var(1), -1);
			Variable_ID csr_ub2 = outer_loops.get_local(ub);

			h4.update_coef(csr_ub2, 1);
			h4.update_const(-1);
		} else {

			GEQ_Handle h3 = f_root2->add_GEQ();
			h3.update_coef(outer_loops.set_var(1), -1);
			GEQ_Handle h4 = f_root2->add_GEQ();
			h4.update_coef(outer_loops.set_var(1), 1);
			Variable_ID csr_ub2 = outer_loops.get_local(ub);

			h4.update_coef(csr_ub2, 1);
			h4.update_const(-1);

		}

		Variable_ID insp = outer_loops.get_local(ub2, Input_Tuple);

		Variable_ID insp2 = outer_loops.get_local(ub3, Input_Tuple);

		partial_IS.get_local(ub2, Input_Tuple);
		partial_IS.get_local(ub3, Input_Tuple);

		if (is_negative) {
			GEQ_Handle h5 = f_root2->add_GEQ();
			h5.update_coef(outer_loops.set_var(2), 1);
			h5.update_coef(insp2, 1);
			h5.update_const(-1);

			GEQ_Handle h6 = f_root2->add_GEQ();
			h6.update_coef(outer_loops.set_var(2), -1);
			h6.update_coef(insp, -1);
			//h6.update_const(-1);
		} else {

			GEQ_Handle h5 = f_root2->add_GEQ();
			h5.update_coef(outer_loops.set_var(2), 1);
			h5.update_coef(insp, -1);

			GEQ_Handle h6 = f_root2->add_GEQ();
			h6.update_coef(outer_loops.set_var(2), -1);
			h6.update_coef(insp2, 1);
			h6.update_const(-1);

		}
		Relation new_IS = and_with_relation_and_replace_var(partial_IS,
				partial_IS.set_var(2), outer_loops);

		Relation new_xform(stmt[*it].xform.n_inp() + 1,
				stmt[*it].xform.n_out() + 2);
		F_And *froot4 = new_xform.add_and();

		for (int i = 1; i <= new_IS.n_set(); i++) {
			EQ_Handle h = froot4->add_EQ();
			h.update_coef(new_xform.input_var(i), 1);
			h.update_coef(new_xform.output_var(2 * i), -1);
		}

		for (int i = 1; i <= new_xform.n_out(); i += 2) {
			EQ_Handle h = froot4->add_EQ();

			if (i > 1)
				h.update_coef(new_xform.output_var(i), 1);
			else {
				h.update_coef(new_xform.output_var(1), 1);
				h.update_const(-lex2[0]);
			}
		}

		new_IS.get_local(ub2, Input_Tuple);
		new_IS.get_local(ub3, Input_Tuple);

		this->known.get_local(ub2, Input_Tuple);
		this->known.get_local(ub3, Input_Tuple);

		stmt[*it].IS = new_IS;
		stmt[*it].xform = new_xform;

		freevar.push_back(ub2);
		freevar.push_back(ub3);
		LoopLevel ll;
		ll.type = LoopLevelOriginal;
		ll.payload = 1;
		ll.parallel_level = 0;
		ll.segreducible = false;
		ll.made_dense = false;

		std::vector<LoopLevel>::iterator it2 = stmt[*it].loop_level.begin();
		stmt[*it].loop_level.insert(it2, ll);
		std::vector<omega::CG_outputRepr *> reprs;
		std::vector<omega::CG_outputRepr *> reprs_;
		std::vector<omega::CG_outputRepr *> reprs2;
		std::vector<omega::Relation> reprs3;
		omega::CG_outputRepr *temp;

		if (is_negative && !suppress_negative)
			temp = ir->builder()->CreateMinus( NULL,
					ir->builder()->CreateIdent(stmt[*it].IS.set_var(1)->name()));
		else
			temp = ir->builder()->CreateIdent(stmt[*it].IS.set_var(1)->name());

		omega::CG_outputRepr *temp2 = ir->builder_s().CreateIdent(
				stmt[*it].IS.set_var(1)->name());

		reprs.push_back(temp->clone());
		reprs_.push_back(temp->clone());
		reprs2.push_back(temp2);
		Relation mapping = copy(stmt[*it].xform);
		omega::Relation mapping1(mapping.n_out(), 1);
		omega::F_And *f_root_ = mapping1.add_and();
		omega::EQ_Handle h_ = f_root_->add_EQ();
		h_.update_coef(mapping1.output_var(1), 1);
		h_.update_coef(mapping1.input_var(2), -1);
		omega::Relation r = Composition(mapping1, copy(mapping));
		r.simplify();

		omega::Variable_ID v = r.output_var(1);

		std::vector<std::pair<omega::CG_outputRepr *, int> > atof = std::vector<
				std::pair<omega::CG_outputRepr *, int> >(stmt[*it].xform.n_out(),
				std::make_pair(static_cast<omega::CG_outputRepr *>(NULL), 0));
		std::pair<omega::EQ_Handle, int> result1 = find_simplest_assignment(r, v,
				atof);

		omega::Relation c = omega::copy(r);
		reprs3.push_back(c);

		uninterpreted_symbols[*it].insert(
				std::pair<std::string, std::vector<omega::CG_outputRepr *> >(
						name + "_", reprs));
		uninterpreted_symbols_stringrepr[*it].insert(
				std::pair<std::string, std::vector<omega::CG_outputRepr *> >(
						name + "_", reprs2));
		unin_rel[*it].insert(
				std::pair<std::string, std::vector<omega::Relation> >(name + "_",
						reprs3));

		uninterpreted_symbols[*it].insert(
				std::pair<std::string, std::vector<omega::CG_outputRepr *> >(
						name + "__", reprs_));
		uninterpreted_symbols_stringrepr[*it].insert(
				std::pair<std::string, std::vector<omega::CG_outputRepr *> >(
						name + "__", reprs2));
		unin_rel[*it].insert(
				std::pair<std::string, std::vector<omega::Relation> >(name + "__",
						reprs3));

//	}

	}

	for (std::map<std::string, std::vector<Relation> >::iterator iter =
			unin_rel[0].begin(); iter != unin_rel[0].end(); iter++) {
		std::vector<Relation> to_push = iter->second;
		for (int l = 0; l < to_push.size(); l++) {

			if (to_push[l].n_out() > 0) {
				omega::Variable_ID v = to_push[l].output_var(1);

				std::vector<std::pair<omega::CG_outputRepr *, int> > atof =
						std::vector<std::pair<omega::CG_outputRepr *, int> >(
								to_push[l].n_inp(),
								std::make_pair(
										static_cast<omega::CG_outputRepr *>(NULL), 0));
				std::pair<omega::EQ_Handle, int> result1 = find_simplest_assignment(
						to_push[l], v, atof);

				Relation s(to_push[l].n_inp() + 1, 1);
				F_And * f_and = s.add_and();
				EQ_Handle h = f_and->add_EQ();
				h.update_coef(s.output_var(1), -1);

				for (Constr_Vars_Iter cvi(result1.first); cvi; cvi++) {
					Variable_ID v = cvi.curr_var();

					switch (v->kind()) {
					case Input_Var: {
						int coef = cvi.curr_coef();
						if (coef < 0)
							coef = (-1) * coef;
						h.update_coef(s.input_var(v->get_position() + 1), coef);

						break;
					}
					default:
						break;
					}
				}

				to_push[l] = s;

			}
		}
		iter->second = to_push;
	}

}
